<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="../../images/favicon.ico" type="image/ico" />
	<title>MK Sistemas Biomedicos</title>

	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet"  href="../../assets/css/bootstrap.min.css" />
	<link rel="stylesheet"  href="../../assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- page specific plugin styles -->

	<!-- text fonts -->
	<link rel="stylesheet"  href="../../assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet"  href="../../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

	<!--[if lte IE 9]>
	<link rel="stylesheet"  href="../../assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
	<![endif]-->
	<link rel="stylesheet"  href="../../assets/css/ace-skins.min.css" />
	<link rel="stylesheet"  href="../../assets/css/ace-rtl.min.css" />

	<!--[if lte IE 9]>
	<link rel="stylesheet"  href="../../assets/css/ace-ie.min.css" />
	<![endif]-->

	<!-- inline styles related to this page -->

	<!-- ace settings handler -->
	<script src="../../assets/js/ace-extra.min.js"></script>

	<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

	<!--[if lte IE 8]>
	<script src="../../assets/js/html5shiv.min.js"></script>
	<script src="../../assets/js/respond.min.js"></script>
	<![endif]-->
	<style>
	body {font-family: Arial;}

	/* Style the tab */
	.tab {
		overflow: hidden;
		border: 1px solid #ccc;
		background-color: #f1f1f1;
	}

	/* Style the buttons inside the tab */
	.tab button {
		background-color: inherit;
		float: left;
		border: none;
		outline: none;
		cursor: pointer;
		padding: 14px 16px;
		transition: 0.3s;
		font-size: 17px;
	}

	/* Change background color of buttons on hover */
	.tab button:hover {
		background-color: #ddd;
	}

	/* Create an active/current tablink class */
	.tab button.active {
		background-color: #ccc;
	}

	/* Style the tab content */
	.tabcontent {
		display: none;
		padding: 6px 12px;
		border: 1px solid #ccc;
		border-top: none;
	}
</style>

</head>
<body class="no-skin">
	<div id="navbar" class="navbar navbar-default          ace-save-state">
		<div class="navbar-container ace-save-state" id="navbar-container">
			<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
				<span class="sr-only">Toggle sidebar</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>
			</button>

			<div class="navbar-header pull-left">
				<a href="dashboard" class="navbar-brand">
					<small>

						MK Sistemas Biomedicos    Data: <script> document.write(new Date().toLocaleDateString()); </script>
					</small>
				</a>
			</div>

		</ul>
	</div>
</div><!-- /.navbar-container -->
</div>

<div class="main-container ace-save-state" id="main-container">
	<script type="text/javascript">
	try{ace.settings.loadState('main-container')}catch(e){}
	</script>



	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">


				<div class="nav-search" id="nav-search">
					<form class="form-search">

					</form>
				</div><!-- /.nav-search -->
			</div>

			<div class="page-content">
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="ace-icon fa fa-cog bigger-130"></i>
					</div>

					<div class="ace-settings-box clearfix" id="ace-settings-box">
						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<div class="pull-left">
									<select id="skin-colorpicker" class="hide">
										<option data-skin="no-skin" value="#438EB9">#438EB9</option>
										<option data-skin="skin-1" value="#222A2D">#222A2D</option>
										<option data-skin="skin-2" value="#C6487E">#C6487E</option>
										<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
									</select>
								</div>
								<span>&nbsp; Choose Skin</span>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
								<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
								<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
								<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
								<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
								<label class="lbl" for="ace-settings-add-container">
									Inside
									<b>.container</b>
								</label>
							</div>
						</div><!-- /.pull-left -->

						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
								<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
								<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
								<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
							</div>
						</div><!-- /.pull-left -->
					</div><!-- /.ace-settings-box -->
				</div><!-- /.ace-settings-container -->



				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->



						<div id="top-menu" class="modal aside" data-fixed="true" data-placement="top" data-background="true" data-backdrop="invisible" tabindex="-1">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body container">
										<?php include 'api/api-tools-top.php';?>
									</div>
								</div><!-- /.modal-content -->

								<button class="btn btn-inverse btn-app btn-xs ace-settings-btn aside-trigger" data-target="#top-menu" data-toggle="modal" type="button">
									<i data-icon1="fa-chevron-down" data-icon2="fa-chevron-up" class="ace-icon fa fa-chevron-down bigger-110 icon-only"></i>
								</button>
							</div><!-- /.modal-dialog -->
						</div>

						<div id="bottom-menu" class="modal aside" data-fixed="true" data-placement="bottom" data-background="true" tabindex="-1">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body container">
										<?php include 'api/api-tools-down.php';?>
									</div>
								</div><!-- /.modal-content -->

								<button class="btn btn-yellow btn-app btn-xs ace-settings-btn aside-trigger" data-target="#bottom-menu" data-toggle="modal" type="button">
									<i data-icon2="fa-chevron-down" data-icon1="fa-chevron-up" class="ace-icon fa fa-chevron-up bigger-110 icon-only"></i>
								</button>
							</div><!-- /.modal-dialog -->
						</div>

						<div id="right-menu" class="modal aside" data-body-scroll="false" data-offset="true" data-placement="right" data-fixed="true" data-backdrop="false" tabindex="-1">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header no-padding">
										<div class="table-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
												<span class="white">&times;</span>
											</button>
											Recursos &amp; Ferramentas
										</div>
									</div>

									<?php include 'api/api-tools.php';?>
								</div><!-- /.modal-content -->

								<button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#right-menu" data-toggle="modal" type="button">
									<i data-icon1="fa-plus" data-icon2="fa-minus" class="ace-icon fa fa-plus bigger-110 icon-only"></i>
								</button>
							</div><!-- /.modal-dialog -->
						</div>


						<div id="my-modal" class="modal fade" tabindex="-1">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h3 class="smaller lighter blue no-margin">A modal with a slider in it!</h3>
									</div>

									<div class="modal-body">
										Some content
										<br />
										<br />
										<br />
										<br />
										<br />
										1
										<br />
										<br />
										<br />
										<br />
										<br />
										2
									</div>

									<div class="modal-footer">
										<button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
											<i class="ace-icon fa fa-times"></i>
											Close
										</button>
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>

						<div id="aside-inside-modal" class="modal" data-placement="bottom" data-background="true" data-backdrop="false" tabindex="-1">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-body">
										<div class="row">
											<div class="col-xs-12 white">
												<h3 class="lighter no-margin">Inside another modal</h3>

												<br />
												<br />
											</div>
										</div>
									</div>
								</div><!-- /.modal-content -->

								<button class="btn btn-default btn-app btn-xs ace-settings-btn aside-trigger" data-target="#aside-inside-modal" data-toggle="modal" type="button">
									<i data-icon2="fa-arrow-down" data-icon1="fa-arrow-up" class="ace-icon fa fa-arrow-up bigger-110 icon-only"></i>
								</button>
							</div><!-- /.modal-dialog -->
						</div>

						<br />

						<!-- PAGE CONTENT BEGINS -->
						<div class="row">
							<div class="col-xs-6 col-sm-3 pricing-box">
								<div class="widget-box widget-color-dark">
									<div class="widget-header">
										<h5 class="widget-title bigger lighter">Leito 1</h5>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<ul class="list-unstyled spaced2">
												<img id="leito_1" height="100" align ="right" src="../../alerta.png" />
												<audio id="alarme_leito_1" src="../../alarm/a1.mp3" autoplay></audio>

												<strong >Tempo 	<span id="counter_1">00:00:00</span> </strong>
												<br />
												<strong>Ultimo Alarme 	<br /><span id="date_alarme1"></span></strong>






											</ul>


											<hr />

											<input type="button" value="Parar" onclick="para_1();"> <input type="button" value="Iniciar" onclick="inicia_1();"> <input type="button" value="Zerar" onclick="zera_1();">

										</div>

										<div>
											<!-- dados -->
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-6 col-sm-3 pricing-box">
								<div class="widget-box widget-color-orange">
									<div class="widget-header">
										<h5 class="widget-title bigger lighter">Leito 2</h5>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<ul class="list-unstyled spaced2">
												<img id="leito_2" height="100" align ="right" src="../../alerta.png" />
												<audio id="alarme_leito_2" src="../../alarm/a1.mp3" autoplay></audio>

												<strong >Tempo 	<span id="counter_2">00:00:00</span> </strong>
												<br />
												<strong>Ultimo Alarme 	<br /><span id="date_alarme2"></span></strong>






											</ul>


											<hr />

											<input type="button" value="Parar" onclick="para_2();"> <input type="button" value="Iniciar" onclick="inicia_2();"> <input type="button" value="Zerar" onclick="zera_2();">

										</div>

										<div>
											<!-- dados -->
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-6 col-sm-3 pricing-box">
								<div class="widget-box widget-color-blue">
									<div class="widget-header">
										<h5 class="widget-title bigger lighter">Leito 3</h5>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<ul class="list-unstyled spaced2">
												<img id="leito_3" height="100" align ="right" src="../../alerta.png" />
												<audio id="alarme_leito_3" src="../../alarm/a1.mp3" autoplay></audio>

												<strong >Tempo 	<span id="counter_3">00:00:00</span> </strong>
												<br />
												<strong>Ultimo Alarme 	<br /><span id="date_alarme3"></span></strong>






											</ul>


											<hr />

											<input type="button" value="Parar" onclick="para_3();"> <input type="button" value="Iniciar" onclick="inicia_3();"> <input type="button" value="Zerar" onclick="zera_3();">

										</div>

										<div>
											<!-- dados -->
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-6 col-sm-3 pricing-box">
								<div class="widget-box widget-color-green">
									<div class="widget-header">
										<h5 class="widget-title bigger lighter">Leito 4</h5>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<ul class="list-unstyled spaced2">
												<img id="leito_4" height="100" align ="right" src="../../alerta.png" />
												<audio id="alarme_leito_4" src="../../alarm/a1.mp3" autoplay></audio>

												<strong >Tempo 	<span id="counter_4">00:00:00</span> </strong>
												<br />
												<strong>Ultimo Alarme 	<br /><span id="date_alarme4"></span></strong>






											</ul>


											<hr />

											<input type="button" value="Parar" onclick="para_4();"> <input type="button" value="Iniciar" onclick="inicia_4();"> <input type="button" value="Zerar" onclick="zera_4();">

										</div>

										<div>
											<!-- dados -->
										</div>
									</div>
								</div>
							</div>
							<!-- PAGE CONTENT BEGINS -->
							<div class="row">
								<div class="col-xs-6 col-sm-3 pricing-box">
									<div class="widget-box widget-color-dark">
										<div class="widget-header">
											<h5 class="widget-title bigger lighter">Leito 5</h5>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												<ul class="list-unstyled spaced2">
													<img id="leito_5" height="100" align ="right" src="../../alerta.png" />
													<audio id="alarme_leito_5" src="../../alarm/a1.mp3" autoplay></audio>

													<strong >Tempo 	<span id="counter_5">00:00:00</span> </strong>
													<br />
													<strong>Ultimo Alarme 	<br /><span id="date_alarme5"></span></strong>






												</ul>


												<hr />

												<input type="button" value="Parar" onclick="para_5();"> <input type="button" value="Iniciar" onclick="inicia_5();"> <input type="button" value="Zerar" onclick="zera_5();">

											</div>

											<div>
												<!-- dados -->
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-6 col-sm-3 pricing-box">
									<div class="widget-box widget-color-orange">
										<div class="widget-header">
											<h5 class="widget-title bigger lighter">Leito 6</h5>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												<ul class="list-unstyled spaced2">
													<img id="leito_6" height="100" align ="right" src="../../alerta.png" />
													<audio id="alarme_leito_6" src="../../alarm/a1.mp3" autoplay></audio>

													<strong >Tempo 	<span id="counter_6">00:00:00</span> </strong>
													<br />
													<strong>Ultimo Alarme 	<br /><span id="date_alarme6"></span></strong>






												</ul>


												<hr />

												<input type="button" value="Parar" onclick="para_6();"> <input type="button" value="Iniciar" onclick="inicia_6();"> <input type="button" value="Zerar" onclick="zera_6();">

											</div>

											<div>
												<!-- dados -->
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-6 col-sm-3 pricing-box">
									<div class="widget-box widget-color-blue">
										<div class="widget-header">
											<h5 class="widget-title bigger lighter">Leito 7</h5>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												<ul class="list-unstyled spaced2">
													<img id="leito_7" height="100" align ="right" src="../../alerta.png" />
													<audio id="alarme_leito_7" src="../../alarm/a1.mp3" autoplay></audio>

													<strong >Tempo 	<span id="counter_7">00:00:00</span> </strong>
													<br />
													<strong>Ultimo Alarme 	<br /><span id="date_alarme7"></span></strong>






												</ul>


												<hr />

												<input type="button" value="Parar" onclick="para_7();"> <input type="button" value="Iniciar" onclick="inicia_7();"> <input type="button" value="Zerar" onclick="zera_7();">

											</div>

											<div>
												<!-- dados -->
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-6 col-sm-3 pricing-box">
									<div class="widget-box widget-color-green">
										<div class="widget-header">
											<h5 class="widget-title bigger lighter">Leito 8</h5>
										</div>

										<div class="widget-body">
											<div class="widget-main">
												<ul class="list-unstyled spaced2">
													<img id="leito_8" height="100" align ="right" src="../../alerta.png" />
													<audio id="alarme_leito_8" src="../../alarm/a1.mp3" autoplay></audio>

													<strong >Tempo 	<span id="counter_8">00:00:00</span> </strong>
													<br />
													<strong>Ultimo Alarme 	<br /><span id="date_alarme8"></span></strong>






												</ul>


												<hr />

												<input type="button" value="Parar" onclick="para_8();"> <input type="button" value="Iniciar" onclick="inicia_8();"> <input type="button" value="Zerar" onclick="zera_8();">

											</div>

											<div>
												<!-- dados -->
											</div>
										</div>
									</div>
								</div>

								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-dark">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Leito 9</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">

														<img id="leito_9" height="100" align ="right" src="../../alerta.png" />
														<audio id="alarme_leito_9" src="../../alarm/a1.mp3" autoplay></audio>

														<strong >Tempo 	<span id="counter_9">00:00:00</span> </strong>
														<br />
														<strong>Ultimo Alarme 	<br /><span id="date_alarme9"></span></strong>






													</ul>


													<hr />

													<input type="button" value="Parar" onclick="para_9();"> <input type="button" value="Iniciar" onclick="inicia_9();"> <input type="button" value="Zerar" onclick="zera_9();">

												</div>

												<div>
													<!-- dados -->
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-orange">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Leito 10</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">
														<img id="leito_10" height="100" align ="right" src="../../alerta.png" />
														<audio id="alarme_leito_10" src="../../alarm/a1.mp3" autoplay></audio>

														<strong >Tempo 	<span id="counter_10">00:00:00</span> </strong>
														<br />
														<strong>Ultimo Alarme 	<br /><span id="date_alarme10"></span></strong>






													</ul>


													<hr />

													<input type="button" value="Parar" onclick="para_10();"> <input type="button" value="Iniciar" onclick="inicia_10();"> <input type="button" value="Zerar" onclick="zera_10();">

												</div>

												<div>
													<!-- dados -->
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-blue">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Leito 11</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">

														<img id="leito_11" height="100" align ="right" src="../../alerta.png" />
														<audio id="alarme_leito_11" src="../../alarm/a1.mp3" autoplay></audio>

														<strong >Tempo 	<span id="counter_11">00:00:00</span> </strong>
														<br />
														<strong>Ultimo Alarme 	<br /><span id="date_alarme11"></span></strong>






													</ul>


													<hr />

													<input type="button" value="Parar" onclick="para_11();"> <input type="button" value="Iniciar" onclick="inicia_11();"> <input type="button" value="Zerar" onclick="zera_11();">

												</div>

												<div>
													<!-- dados -->
												</div>
											</div>
										</div>
									</div>

									<div class="col-xs-6 col-sm-3 pricing-box">
										<div class="widget-box widget-color-green">
											<div class="widget-header">
												<h5 class="widget-title bigger lighter">Leito 12</h5>
											</div>

											<div class="widget-body">
												<div class="widget-main">
													<ul class="list-unstyled spaced2">

														<img id="leito_12" height="100" align ="right" src="../../alerta.png" />
														<audio id="alarme_leito_12" src="../../alarm/a1.mp3" autoplay></audio>

														<strong >Tempo 	<span id="counter_12">00:00:00</span> </strong>
														<br />
														<strong>Ultimo Alarme 	<br /><span id="date_alarme12"></span></strong>






													</ul>


													<hr />

													<input type="button" value="Parar" onclick="para_12();"> <input type="button" value="Iniciar" onclick="inicia_12();"> <input type="button" value="Zerar" onclick="zera_12();">

												</div>

												<div>
													<!-- dados -->
												</div>
											</div>
										</div>
									</div>
									</div>
									<!-- /page content -->

									<!-- footer content -->
									<div class="footer">

										<div class="footer-inner">

											<div class="footer-content">

												<span class="bigger-120">

													<span class="blue bolder">IRIS</span>

													MK Sistemas Biomédicos &copy; 2021
												</span>

												&nbsp; &nbsp;
												<!--	<span class="action-buttons">
												<a href="#">
												<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
											</a>

											<a href="#">
											<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
										</a>

										<a href="#">
										<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
									</a>
								</span> -->
							</div>
						</div>
					</div>
					<!-- /footer content -->
				</div>
			</div>
			<!-- basic scripts -->

			<!--[if !IE]> -->
			<script src="../../assets/js/jquery-2.1.4.min.js"></script>

			<!-- <![endif]-->

			<!--[if IE]>
			<script src="../../assets/js/jquery-1.11.3.min.js"></script>
			<![endif]-->
			<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='../../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script>
			<script src="../../assets/js/bootstrap.min.js"></script>

			<!-- page specific plugin scripts -->

			<!-- ace scripts -->
			<script src="../../assets/js/ace-elements.min.js"></script>
			<script src="../../assets/js/ace.min.js"></script>

			<!-- inline scripts related to this page -->
			<script>
			function openCity(evt, cityName) {
				var i, tabcontent, tablinks;
				tabcontent = document.getElementsByClassName("tabcontent");
				for (i = 0; i < tabcontent.length; i++) {
					tabcontent[i].style.display = "none";
				}
				tablinks = document.getElementsByClassName("tablinks");
				for (i = 0; i < tablinks.length; i++) {
					tablinks[i].className = tablinks[i].className.replace(" active", "");
				}
				document.getElementById(cityName).style.display = "block";
				evt.currentTarget.className += " active";
			}
		</script>
		<script>
		function myFunction() {
			var x = document.getElementById("myTopnav");
			if (x.className === "topnav") {
				x.className += " responsive";
			} else {
				x.className = "topnav";
			}
		}
	</script>
</body>

<script type="text/javascript">
if('ontouchstart' in document.documentElement) document.write("<script src='../../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<scriptsrc="../../assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!-- ace scripts -->
<scriptsrc="../../assets/js/ace-elements.min.js"></script>
<scriptsrc="../../assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
jQuery(function($) {
	$('.modal.aside').ace_aside();

	$('#aside-inside-modal').addClass('aside').ace_aside({container: '#my-modal > .modal-dialog'});

	//$('#top-menu').modal('show')

	$(document).one('ajaxloadstart.page', function(e) {
		//in ajax mode, remove before leaving page
		$('.modal.aside').remove();
		$(window).off('.aside')
	});


	//make content sliders resizable using jQuery UI (you should include jquery ui files)
	//$('#right-menu > .modal-dialog').resizable({handles: "w", grid: [ 20, 0 ], minWidth: 200, maxWidth: 600});
})

function formatatempo(segs) {
	min = 0;
	hr = 0;
	/*
	if hr < 10 then hr = "0"&hr
	if min < 10 then min = "0"&min
	if segs < 10 then segs = "0"&segs
	*/
	while(segs>=60) {
		if (segs >=60) {
			segs = segs-60;
			min = min+1;
		}
	}

	while(min>=60) {
		if (min >=60) {
			min = min-60;
			hr = hr+1;
		}
	}

	if (hr < 10) {hr = "0"+hr}
	if (min < 10) {min = "0"+min}
	if (segs < 10) {segs = "0"+segs}
	fin = hr+":"+min+":"+segs
	return fin;
}
var segundos_1 = 0; //inicio do cronometro
var segundos_2 = 0; //inicio do cronometro
var segundos_3 = 0; //inicio do cronometro
var segundos_4 = 0; //inicio do cronometro
var segundos_5 = 0; //inicio do cronometro
var segundos_6 = 0; //inicio do cronometro
var segundos_7 = 0; //inicio do cronometro
var segundos_8 = 0; //inicio do cronometro
var segundos_9 = 0; //inicio do cronometro
var segundos_10 = 0; //inicio do cronometro
var segundos_11 = 0; //inicio do cronometro
var segundos_12 = 0; //inicio do cronometro
var segundos_13 = 0; //inicio do cronometro
var segundos_14 = 0; //inicio do cronometro
var segundos_15 = 0; //inicio do cronometro
var segundos_16 = 0; //inicio do cronometro
var alerta_tempo_1 =0;
var alerta_tempo_2=0;
var alerta_tempo_3 =0;
var alerta_tempo_4 =0;
var alerta_tempo_5 =0;
var alerta_tempo_6 =0;
var alerta_tempo_7 =0;
var alerta_tempo_8 =0;
var alerta_tempo_9 =0;
var alerta_tempo_10 =0;
var alerta_tempo_11 =0;
var alerta_tempo_12 =0;
var alerta_tempo_13 =0;
var alerta_tempo_14 =0;
var alerta_tempo_15 =0;
var alerta_tempo_16 =0;
var date_alarme1;
var date_alarme2;
var date_alarme3;
var date_alarme4;
var date_alarme5;
var date_alarme6;
var date_alarme7;
var date_alarme8;
var date_alarme9;
var date_alarme10;
var date_alarme11;
var date_alarme12;
var date_alarme13;
var date_alarme14;
var date_alarme15;
var date_alarme16;





function conta_1() {
	segundos_1++;
	document.getElementById("counter_1").innerHTML = formatatempo(segundos_1);
}

function inicia_1(){
	interval_1 = setInterval("conta_1();",1000);
	document.getElementById('alarme_leito_1').play()
	alerta_tempo_1 = window.setInterval(function(){


		if(alerta_1.style.visibility == 'hidden'){
			alerta_1.style.visibility = 'visible';
		}else{
			alerta_1.style.visibility = 'hidden';
		}
	}, 700);
}

function para_1(){
	clearInterval(interval_1);
	segundos_1 = 0;
	clearInterval(alerta_tempo_1);
	alerta_tempo_1= 0;
	alerta_1.style.visibility = 'hidden';
	document.getElementById('alarme_leito_1').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme1").innerHTML = fecha.toISOString();
}

function zera_1(){
	clearInterval(interval_1);
	segundos_1 = 0;
	document.getElementById("counter_1").innerHTML = formatatempo(segundos_1);
}


var alerta_1 = document.getElementById('leito_1');
// inicio contador 2


function conta_2() {
	segundos_2++;
	document.getElementById("counter_2").innerHTML = formatatempo(segundos_2);
}

function inicia_2(){
	interval_2 = setInterval("conta_2();",1000);
	document.getElementById('alarme_leito_2').play()
	alerta_tempo_2 = window.setInterval(function(){


		if(alerta_2.style.visibility == 'hidden'){
			alerta_2.style.visibility = 'visible';
		}else{
			alerta_2.style.visibility = 'hidden';
		}
	}, 700);
}

function para_2(){
	clearInterval(interval_2);
	segundos_2 = 0;
	clearInterval(alerta_tempo_2);
	alerta_tempo_2= 0;
	alerta_2.style.visibility = 'hidden';
	document.getElementById('alarme_leito_2').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme2").innerHTML = fecha.toISOString();
}

function zera_2(){
	clearInterval(interval_2);
	segundos_2 = 0;
	document.getElementById("counter_2").innerHTML = formatatempo(segundos_2);
}


var alerta_2 = document.getElementById('leito_2');
// inicio contador 3


function conta_3() {
	segundos_3++;
	document.getElementById("counter_3").innerHTML = formatatempo(segundos_3);
}

function inicia_3(){
	interval_3 = setInterval("conta_3();",1000);
	document.getElementById('alarme_leito_3').play()
	alerta_tempo_3 = window.setInterval(function(){


		if(alerta_3.style.visibility == 'hidden'){
			alerta_3.style.visibility = 'visible';
		}else{
			alerta_3.style.visibility = 'hidden';
		}
	}, 700);
}

function para_3(){
	clearInterval(interval_3);
	segundos_3 = 0;
	clearInterval(alerta_tempo_3);
	alerta_tempo_3= 0;
	alerta_3.style.visibility = 'hidden';
	document.getElementById('alarme_leito_3').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme3").innerHTML = fecha.toISOString();
}

function zera_3(){
	clearInterval(interval_3);
	segundos_3 = 0;
	document.getElementById("counter_3").innerHTML = formatatempo(segundos_3);
}


var alerta_3 = document.getElementById('leito_3');
// inicio contador 4

function conta_4() {
	segundos_4++;
	document.getElementById("counter_4").innerHTML = formatatempo(segundos_4);
}

function inicia_4(){
	interval_4 = setInterval("conta_4();",4000);
	document.getElementById('alarme_leito_4').play()
	alerta_tempo_4 = window.setInterval(function(){


		if(alerta_4.style.visibility == 'hidden'){
			alerta_4.style.visibility = 'visible';
		}else{
			alerta_4.style.visibility = 'hidden';
		}
	}, 700);
}

function para_4(){
	clearInterval(interval_4);
	segundos_4 = 0;
	clearInterval(alerta_tempo_4);
	alerta_tempo_4= 0;
	alerta_4.style.visibility = 'hidden';
	document.getElementById('alarme_leito_4').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme4").innerHTML = fecha.toISOString();
}

function zera_4(){
	clearInterval(interval_4);
	segundos_4 = 0;
	document.getElementById("counter_4").innerHTML = formatatempo(segundos_4);
}


var alerta_4 = document.getElementById('leito_4');
// inicio contador 5

function conta_5() {
	segundos_5++;
	document.getElementById("counter_5").innerHTML = formatatempo(segundos_5);
}

function inicia_5(){
	interval_5 = setInterval("conta_5();",5000);
	document.getElementById('alarme_leito_5').play()
	alerta_tempo_5 = window.setInterval(function(){


		if(alerta_5.style.visibility == 'hidden'){
			alerta_5.style.visibility = 'visible';
		}else{
			alerta_5.style.visibility = 'hidden';
		}
	}, 700);
}

function para_5(){
	clearInterval(interval_5);
	segundos_5 = 0;
	clearInterval(alerta_tempo_5);
	alerta_tempo_5= 0;
	alerta_5.style.visibility = 'hidden';
	document.getElementById('alarme_leito_5').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme5").innerHTML = fecha.toISOString();
}

function zera_5(){
	clearInterval(interval_5);
	segundos_5 = 0;
	document.getElementById("counter_5").innerHTML = formatatempo(segundos_5);
}


var alerta_5 = document.getElementById('leito_5');
// inicio contador 6

function conta_6() {
	segundos_6++;
	document.getElementById("counter_6").innerHTML = formatatempo(segundos_6);
}

function inicia_6(){
	interval_6 = setInterval("conta_6();",6000);
	document.getElementById('alarme_leito_6').play()
	alerta_tempo_6 = window.setInterval(function(){


		if(alerta_6.style.visibility == 'hidden'){
			alerta_6.style.visibility = 'visible';
		}else{
			alerta_6.style.visibility = 'hidden';
		}
	}, 700);
}

function para_6(){
	clearInterval(interval_6);
	segundos_6 = 0;
	clearInterval(alerta_tempo_6);
	alerta_tempo_6= 0;
	alerta_6.style.visibility = 'hidden';
	document.getElementById('alarme_leito_6').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme6").innerHTML = fecha.toISOString();
}

function zera_6(){
	clearInterval(interval_6);
	segundos_6 = 0;
	document.getElementById("counter_6").innerHTML = formatatempo(segundos_6);
}


var alerta_6 = document.getElementById('leito_6');
// inicio contador 7

function conta_7() {
	segundos_7++;
	document.getElementById("counter_7").innerHTML = formatatempo(segundos_7);
}

function inicia_7(){
	interval_7 = setInterval("conta_7();",7000);
	document.getElementById('alarme_leito_7').play()
	alerta_tempo_7 = window.setInterval(function(){


		if(alerta_7.style.visibility == 'hidden'){
			alerta_7.style.visibility = 'visible';
		}else{
			alerta_7.style.visibility = 'hidden';
		}
	}, 700);
}

function para_7(){
	clearInterval(interval_7);
	segundos_7 = 0;
	clearInterval(alerta_tempo_7);
	alerta_tempo_7= 0;
	alerta_7.style.visibility = 'hidden';
	document.getElementById('alarme_leito_7').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme7").innerHTML = fecha.toISOString();
}

function zera_7(){
	clearInterval(interval_7);
	segundos_7 = 0;
	document.getElementById("counter_7").innerHTML = formatatempo(segundos_7);
}


var alerta_7 = document.getElementById('leito_7');
// inicio contador 8

function conta_8() {
	segundos_8++;
	document.getElementById("counter_8").innerHTML = formatatempo(segundos_8);
}

function inicia_8(){
	interval_8 = setInterval("conta_8();",8000);
	document.getElementById('alarme_leito_8').play()
	alerta_tempo_8 = window.setInterval(function(){


		if(alerta_8.style.visibility == 'hidden'){
			alerta_8.style.visibility = 'visible';
		}else{
			alerta_8.style.visibility = 'hidden';
		}
	}, 800);
}

function para_8(){
	clearInterval(interval_8);
	segundos_8 = 0;
	clearInterval(alerta_tempo_8);
	alerta_tempo_8= 0;
	alerta_8.style.visibility = 'hidden';
	document.getElementById('alarme_leito_8').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme8").innerHTML = fecha.toISOString();
}

function zera_8(){
	clearInterval(interval_8);
	segundos_8 = 0;
	document.getElementById("counter_8").innerHTML = formatatempo(segundos_8);
}


var alerta_8 = document.getElementById('leito_8');
// inicio contador 9

function conta_9() {
	segundos_9++;
	document.getElementById("counter_9").innerHTML = formatatempo(segundos_9);
}

function inicia_9(){
	interval_9 = setInterval("conta_9();",9000);
	document.getElementById('alarme_leito_9').play()
	alerta_tempo_9 = window.setInterval(function(){


		if(alerta_9.style.visibility == 'hidden'){
			alerta_9.style.visibility = 'visible';
		}else{
			alerta_9.style.visibility = 'hidden';
		}
	}, 900);
}

function para_9(){
	clearInterval(interval_9);
	segundos_9 = 0;
	clearInterval(alerta_tempo_9);
	alerta_tempo_9= 0;
	alerta_9.style.visibility = 'hidden';
	document.getElementById('alarme_leito_9').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme9").innerHTML = fecha.toISOString();
}

function zera_9(){
	clearInterval(interval_9);
	segundos_9 = 0;
	document.getElementById("counter_9").innerHTML = formatatempo(segundos_9);
}


var alerta_9 = document.getElementById('leito_9');
// inicio contador 10

function conta_10() {
	segundos_10++;
	document.getElementById("counter_10").innerHTML = formatatempo(segundos_10);
}

function inicia_10(){
	interval_10 = setInterval("conta_10();",10000);
	document.getElementById('alarme_leito_10').play()
	alerta_tempo_10 = window.setInterval(function(){


		if(alerta_10.style.visibility == 'hidden'){
			alerta_10.style.visibility = 'visible';
		}else{
			alerta_10.style.visibility = 'hidden';
		}
	}, 1000);
}

function para_10(){
	clearInterval(interval_10);
	segundos_10 = 0;
	clearInterval(alerta_tempo_10);
	alerta_tempo_10= 0;
	alerta_10.style.visibility = 'hidden';
	document.getElementById('alarme_leito_10').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme10").innerHTML = fecha.toISOString();
}

function zera_10(){
	clearInterval(interval_10);
	segundos_10 = 0;
	document.getElementById("counter_10").innerHTML = formatatempo(segundos_10);
}


var alerta_10 = document.getElementById('leito_10');
// inicio contador 11

function conta_11() {
	segundos_11++;
	document.getElementById("counter_11").innerHTML = formatatempo(segundos_11);
}

function inicia_11(){
	interval_11 = setInterval("conta_11();",11000);
	document.getElementById('alarme_leito_11').play()
	alerta_tempo_11 = window.setInterval(function(){


		if(alerta_11.style.visibility == 'hidden'){
			alerta_11.style.visibility = 'visible';
		}else{
			alerta_11.style.visibility = 'hidden';
		}
	}, 1100);
}

function para_11(){
	clearInterval(interval_11);
	segundos_11 = 0;
	clearInterval(alerta_tempo_11);
	alerta_tempo_11= 0;
	alerta_11.style.visibility = 'hidden';
	document.getElementById('alarme_leito_11').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme11").innerHTML = fecha.toISOString();
}

function zera_11(){
	clearInterval(interval_11);
	segundos_11 = 0;
	document.getElementById("counter_11").innerHTML = formatatempo(segundos_11);
}


var alerta_11 = document.getElementById('leito_11');
// inicio contador 12

function conta_12() {
	segundos_12++;
	document.getElementById("counter_12").innerHTML = formatatempo(segundos_12);
}

function inicia_12(){
	interval_12 = setInterval("conta_12();",12000);
	document.getElementById('alarme_leito_12').play()
	alerta_tempo_12 = window.setInterval(function(){


		if(alerta_12.style.visibility == 'hidden'){
			alerta_12.style.visibility = 'visible';
		}else{
			alerta_12.style.visibility = 'hidden';
		}
	}, 1200);
}

function para_12(){
	clearInterval(interval_12);
	segundos_12 = 0;
	clearInterval(alerta_tempo_12);
	alerta_tempo_12= 0;
	alerta_12.style.visibility = 'hidden';
	document.getElementById('alarme_leito_12').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme12").innerHTML = fecha.toISOString();
}

function zera_12(){
	clearInterval(interval_12);
	segundos_12 = 0;
	document.getElementById("counter_12").innerHTML = formatatempo(segundos_12);
}


var alerta_12 = document.getElementById('leito_12');
// inicio contador 13

function conta_13() {
	segundos_13++;
	document.getElementById("counter_13").innerHTML = formatatempo(segundos_13);
}

function inicia_13(){
	interval_13 = setInterval("conta_13();",13000);
	document.getElementById('alarme_leito_13').play()
	alerta_tempo_13 = window.setInterval(function(){


		if(alerta_13.style.visibility == 'hidden'){
			alerta_13.style.visibility = 'visible';
		}else{
			alerta_13.style.visibility = 'hidden';
		}
	}, 1300);
}

function para_13(){
	clearInterval(interval_13);
	segundos_13 = 0;
	clearInterval(alerta_tempo_13);
	alerta_tempo_13= 0;
	alerta_13.style.visibility = 'hidden';
	document.getElementById('alarme_leito_13').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme13").innerHTML = fecha.toISOString();
}

function zera_13(){
	clearInterval(interval_13);
	segundos_13 = 0;
	document.getElementById("counter_13").innerHTML = formatatempo(segundos_13);
}


var alerta_13 = document.getElementById('leito_13');
// inicio contador 14

function conta_14() {
	segundos_14++;
	document.getElementById("counter_14").innerHTML = formatatempo(segundos_14);
}

function inicia_14(){
	interval_14 = setInterval("conta_14();",14000);
	document.getElementById('alarme_leito_14').play()
	alerta_tempo_14 = window.setInterval(function(){


		if(alerta_14.style.visibility == 'hidden'){
			alerta_14.style.visibility = 'visible';
		}else{
			alerta_14.style.visibility = 'hidden';
		}
	}, 1400);
}

function para_14(){
	clearInterval(interval_14);
	segundos_14 = 0;
	clearInterval(alerta_tempo_14);
	alerta_tempo_14= 0;
	alerta_14.style.visibility = 'hidden';
	document.getElementById('alarme_leito_14').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme14").innerHTML = fecha.toISOString();
}

function zera_14(){
	clearInterval(interval_14);
	segundos_14 = 0;
	document.getElementById("counter_14").innerHTML = formatatempo(segundos_14);
}


var alerta_14 = document.getElementById('leito_14');
// inicio contador 2

function conta_15() {
	segundos_15++;
	document.getElementById("counter_15").innerHTML = formatatempo(segundos_15);
}

function inicia_15(){
	interval_15 = setInterval("conta_15();",15000);
	document.getElementById('alarme_leito_15').play()
	alerta_tempo_15 = window.setInterval(function(){


		if(alerta_15.style.visibility == 'hidden'){
			alerta_15.style.visibility = 'visible';
		}else{
			alerta_15.style.visibility = 'hidden';
		}
	}, 1500);
}

function para_15(){
	clearInterval(interval_15);
	segundos_15 = 0;
	clearInterval(alerta_tempo_15);
	alerta_tempo_15= 0;
	alerta_15.style.visibility = 'hidden';
	document.getElementById('alarme_leito_15').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme15").innerHTML = fecha.toISOString();
}

function zera_15(){
	clearInterval(interval_15);
	segundos_15 = 0;
	document.getElementById("counter_15").innerHTML = formatatempo(segundos_15);
}


var alerta_15 = document.getElementById('leito_15');
// inicio contador 16

function conta_16() {
	segundos_16++;
	document.getElementById("counter_16").innerHTML = formatatempo(segundos_16);
}

function inicia_16(){
	interval_16 = setInterval("conta_16();",16000);
	document.getElementById('alarme_leito_16').play()
	alerta_tempo_16 = window.setInterval(function(){


		if(alerta_16.style.visibility == 'hidden'){
			alerta_16.style.visibility = 'visible';
		}else{
			alerta_16.style.visibility = 'hidden';
		}
	}, 1600);
}

function para_16(){
	clearInterval(interval_16);
	segundos_16 = 0;
	clearInterval(alerta_tempo_16);
	alerta_tempo_16= 0;
	alerta_16.style.visibility = 'hidden';
	document.getElementById('alarme_leito_16').pause()

	var fecha = new Date();
	var options = { year: 'numeric', month: 'numeric', day: 'numeric',hour: 'numeric', minute: 'numeric', second:'numeric' };
	document.getElementById("date_alarme16").innerHTML = fecha.toISOString();
}

function zera_16(){
	clearInterval(interval_16);
	segundos_16 = 0;
	document.getElementById("counter_16").innerHTML = formatatempo(segundos_16);
}


var alerta_16 = document.getElementById('leito_16');



</script>
</html>
