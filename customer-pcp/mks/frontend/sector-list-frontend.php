<?php
include("database/database.php");
$query = "SELECT setor.codigo,setor.id,setor.id_unidade,setor.custo,setor.nome,setor.upgrade,setor.reg_date, unidade.id, unidade.instituicao FROM setor INNER JOIN unidade ON setor.id_unidade = unidade.id where setor.trash = 1";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($codigo,$id, $id_unidade,$custo,$nome,$upgrade,$reg_date,$id2,$instituicao);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Unidade</th>
                          <th>Centro de Custo</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Update</th>
                          <th>Cadastro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($instituicao); ?> </td>
                          <td><?php printf($custo); ?></td>
                          <td><?php printf($nome); ?></td>
                            <td><?php printf($codigo); ?></td>
                          <td><?php printf($upgrade); ?></td>
                          <td><?php printf($reg_date); ?></td>

                          <td>
                   <a class="btn btn-app"  href="sector-edit?area=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>

              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                      <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/sector-trash-backend?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
