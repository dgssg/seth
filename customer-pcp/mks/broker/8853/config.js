// host = '172.16.153.122';	// hostname or IP address
host = 'mqtt.eclipseprojects.io';	// hostname or IP address
// host = '172.16.153.110';	// hostname or IP address
port = 443;
lwt_availability = '/dingtian/relay8853/out/lwt_availability';
r1 = '/dingtian/relay8853/out/r1';		// topic to subscribe to
r2 = '/dingtian/relay8853/out/r2';
r3 = '/dingtian/relay8853/out/r3';
r4 = '/dingtian/relay8853/out/r4';
relay1 = '/dingtian/relay8853/out/relay1';		// topic to subscribe to
relay2 = '/dingtian/relay8853/out/relay2';
relay3 = '/dingtian/relay8853/out/relay3';
relay4 = '/dingtian/relay8853/out/relay4';
i1 = '/dingtian/relay8853/out/i1';		// topic to subscribe to
i2 = '/dingtian/relay8853/out/i2';
i3 = '/dingtian/relay8853/out/i3';
i4 = '/dingtian/relay8853/out/i4';
input1 = '/dingtian/relay8853/out/input1';		// topic to subscribe to
input2 = '/dingtian/relay8853/out/input2';
input3 = '/dingtian/relay8853/out/input3';
input4 = '/dingtian/relay8853/out/input4';
ip = '/dingtian/relay8853/out/ip';
sn = '/dingtian/relay8853/out/sn';
mac = '/dingtian/relay8853/out/mac';
input_cnt = '/dingtian/relay8853/out/input_cnt';
relay_cnt = '/dingtian/relay8853/out/relay_cnt';
r1_in = '/dingtian/relay8853/in/r1';		// topic to subscribe to
r2_in = '/dingtian/relay8853/in/r2';
r3_in = '/dingtian/relay8853/in/r3';
r4_in = '/dingtian/relay8853/in/r4';
pub_qos = '0';
r_on='ON';
r_off='OFF';


useTLS = true;
username = null;
password = null;
// username = "jjolie";
// password = "aa";

// path as in "scheme:[//[user:password@]host[:port]][/]path[?query][#fragment]"
//    defaults to "/mqtt"
//    may include query and fragment
//
 //path = "/mqtt";
// path = "/data/cloud?device=12345";

cleansession = true;
