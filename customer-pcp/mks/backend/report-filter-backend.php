<?php


include("../database/database.php");

$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];
$nivel=$_POST['nivel'];
$id_device=$_POST['id_device'];
$id_user=$_POST['id_user'];
 
 
$nivel=trim($nivel);
$date_end=trim($date_end);
$date_start=trim($date_start);
 
if($nivel == ""){
  $nivel = "1,2,3,4";
}


$query = " SELECT nivel.nivel_pcp,  report.reg_date , report_device.nome,usuario.nome AS 'usuario',usuario.sobrenome FROM report  INNER JOIN nivel ON nivel.id = report.id_nivel LEFT JOIN report_device ON report.id_device = report_device.id  LEFT JOIN usuario ON usuario.id = report.id_user where report.id_nivel  IN ($nivel) ";

 

if($date_start==!""){
    $query = " $query and  report.reg_date > '$date_start'";
}

if($date_end==!""){
    $query = " $query and  report.reg_date < '$date_end'";
}
 
if($id_device==!""){
  $query = " $query and  report.id_device = $id_device";
}

if($id_user==!""){
  $query = " $query and  report.id_user = $id_user";
}


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_nivel,$reg_date,$device,$usuario,$sobrenome );



//header('Location: ../report-report-mc');
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>APOLLO | <small>Análise e Planejamento Operacional para Lotação de Leitos e Ocupação</small></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style>
    body {
    font-size: 12pt;
    font-family: Calibri;
    padding : 10px;
}
table {
    border: 1px solid black;
}
th {
    border: 1px solid black;
    padding: 5px;
    background-color:grey;
    color: white;
}
td {
    border: 1px solid black;
    padding: 5px;
}
input {
    font-size: 12pt;
    font-family: Calibri;
}
</style>
  </head>
  <body>
 
                  <a  class="btn btn-secondary btn-xs"  id="btnExport"> Excel
                    
                  </a>
                  <script type="text/javascript" language="JavaScript">

function printPage() {

  if (window.print) {

    agree = confirm("Deseja imprimir essa pagina ?");

    if (agree) {
      window.print();

      if (_GET("pg") != null)
      location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
      else
      history.go(-1);


      window.close();
    }



  }
}

function noPrint() {
  try {

    if (_GET("pg") != null)
    location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
    else
    history.go(-1);

    window.close();

  }
  catch (e) {
    alert();
    document.write("Error Message: " + e.message);
  }
}

function _GET(name) {
  var url = window.location.search.replace("?", "");
  var itens = url.split("&");

  for (n in itens) {
    if (itens[n].match(name)) {
      return decodeURIComponent(itens[n].replace(name + "=", ""));
    }
  }
  return null;
}





function visualizarImpressao() {

  var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

  document.body.insertAdjacentHTML("beforeEnd", Navegador);

  Navegador1.ExecWB(7, 1);

  Navegador1.outerHTML = "";

}



(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
    (i[r].q = i[r].q || []).push(arguments)
  }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-11247563-6', 'auto');
ga('send', 'pageview');

// -->
</script>

<style type="text/css">
page {
  /* background: white;*/
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  text-align: left;
}

page[size="A4"] {
  width: 21cm;
  /*height: 29.7cm;                */
}

page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /* height: 21cm; */
}

@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
    page-break-after:always;
  }

  .noprint {
    display: none;
  }
}




body {
  font-size: 11px;
  font-family: sans-serif;
}



.pagina_retrato {
  width: 720px;
}

.pagina_paisagem {
  width: 1025px;
}

.relatorio_filtro {
  padding: 10px 0px;
  border-collapse: collapse;
}

.relatorio_filtro tr td {
  border: 1px solid #000;
}


.relatorio_titulo {
  font-family: Times New Roman;
  font-size: 15px;
  color: Black;
  font-weight: bold;
  text-transform: uppercase;
}

.borda_celula {
  border: 1px solid #000;
  background-color: #fff;
}

.noprint {
  padding-bottom: 10px;
}
.right{
  float:right;
}

.left{
  float:left;
}
</style>

<style type="text/css" media="print">
/*
@page {
margin: 0.8cm;
}
*/
.pagina_retrato, .pagina_paisagem {
width: 100%;
}
 
</style>

<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script>
window.onload = function () {
JsBarcode(".barcode").init();
}
</script>

   <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <style>
    /* Estilos para o cabeçalho */
    header {
      padding: 20px;
      background-color: #f5f5f5;
      border-bottom: 1px solid #ddd;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      padding: 5px;
      border: 1px solid #ddd;
    } 

    #logo {
      width: 100px;
    }

    h1 {
      margin: 0;
      font-size: 24px;
    }

    #info {
      font-size: 14px;
    }
  </style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />
<link href="../App_Themes/Tema1/StyleSheet.css" type="text/css" rel="stylesheet" />

<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEwODA5NzUyMjMPZBYCZg9kFgICAw9kFgICAQ9kFggCAw8PFgIeCEltYWdlVXJsBUd+L2ltYWdlcy9ob3NwaXRhaXMvaV9jcnV6dmVybWVsaGFicmFzaWxlaXJhLWZpbGlhbGRvZXN0YWRvZG9wYXJhbsOhLnBuZ2RkAgUPDxYCHgRUZXh0BQcwMS8yMDE5ZGQCCw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIeCkhlYWRlclRleHQFBzAxLzIwMTg8KwAFAQAWAh8EBQcwMi8yMDE4PCsABQEAFgIfBAUHMDMvMjAxODwrAAUBABYCHwQFBzA0LzIwMTg8KwAFAQAWAh8EBQcwNS8yMDE4PCsABQEAFgIfBAUHMDYvMjAxODwrAAUBABYCHwQFBzA3LzIwMTg8KwAFAQAWAh8EBQcwOC8yMDE4PCsABQEAFgIfBAUHMDkvMjAxODwrAAUBABYCHwQFBzEwLzIwMTg8KwAFAQAWAh8EBQcxMS8yMDE4PCsABQEAFgIfBAUHMTIvMjAxODwrAAUBABYCHwQFBzAxLzIwMTkWDQIGAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGDBQrAABkAg8PPCsAEQMADxYEHwJnHwNmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIfBAUHMDEvMjAxODwrAAUBABYCHwQFBzAyLzIwMTg8KwAFAQAWAh8EBQcwMy8yMDE4PCsABQEAFgIfBAUHMDQvMjAxODwrAAUBABYCHwQFBzA1LzIwMTg8KwAFAQAWAh8EBQcwNi8yMDE4PCsABQEAFgIfBAUHMDcvMjAxODwrAAUBABYCHwQFBzA4LzIwMTg8KwAFAQAWAh8EBQcwOS8yMDE4PCsABQEAFgIfBAUHMTAvMjAxODwrAAUBABYCHwQFBzExLzIwMTg8KwAFAQAWAh8EBQcxMi8yMDE4PCsABQEAFgIfBAUHMDEvMjAxORYNAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGAgYMFCsAAGQYAgUeY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSRncnYyDzwrAAwBCGZkBR5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJGdydjEPPCsADAEIZmS9tcluC/wCTfFh0PbdSxSO8nxQJa5LJqUDKFT3sJzurQ==" />
</div>

<div>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3164E9EE" />
</div>

<div class="noprint">
<center>
  <input type="button" value="Não Imprimir" onclick="noPrint();">
  <input type="button" value="Imprimir" onclick="printPage();" >
  <input type="button" onclick="visualizarImpressao();" value="Visualizar Impressão" >
</center>
</div>

  <header>
    <table>
      <tr>
      <td><img src="../dropzone/logo/<?php printf($logo)?>" alt="Logo da empresa" id="logo"></td>
        <td>
          <center>
          <h1>Relatório de Análise e Planejamento Operacional para Lotação de Leitos e Ocupação</h1>
        
          <p>Data da impressão: <?php printf($today); ?> <small>Sistema APOLLO</small></p>
          </center>
        </td>
        <td><img src="../logo/clientelogo.png" alt="Logo da empresa" id="logo"></td>
      </tr>
      <tr>
        <td colspan="3" id="info">
          <table>
            <tr>
              <td>Unidade:</td>
              <td><?php printf($unidade); ?></td>
              <td>Setor:</td>
              <td><?php printf($setor_titulo); ?></td>
              <td>Area:</td>
              <td><?php printf($area_titulo); ?></td>
            </tr>
            <tr>
              <td>CEP:</td>
              <td><?php printf($cep); ?></td>
              <td>Rua:</td>
              <td><?php printf($rua); ?></td>
              <td>Bairro:</td>
              <td><?php printf($bairro); ?></td>
            </tr>
            <tr>
              <td>Cidade:</td>
              <td><?php printf($cidade); ?></td>
              <td>Estado:</td>
              <td><?php printf($estado); ?></td>
              <td>CNPJ:</td>
              <td><?php printf($cnpj); ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </header>
                    <div id="dvData">
 <table >
                      <thead>
                        <tr>
                         <th>Nivel</th>
                         <th>Registro</th>
                         <th>Plataforma</th>
                          <th>Usuario</th>
 

                        </tr>
                      </thead>


                      <tbody>
                        <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                        <td><?php printf($id_nivel); ?> </td>
 
 <td><?php printf($reg_date); ?></td>
 <td><?php printf($device); ?></td>
 <td><?php printf($usuario); ?> <?php printf($sobrenome); ?></td>



                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                    </div>
                     
                    </div>
                     <!-- jQuery 2.1.4 -->
    <script src="../../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../../framework/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="jquery.btechco.excelexport.js"></script>
<script src="jquery.base64.js"></script>
<script>
$(document).ready(function () {
 $("#btnExport").click(function (e) {
      e.preventDefault();
      var table_div = document.getElementById('dvData');
      // esse "\ufeff" é importante para manter os acentos
      var blobData = new Blob(['\ufeff'+table_div.outerHTML], { type: 'application/vnd.ms-excel' });
      var url = window.URL.createObjectURL(blobData);
      var a = document.createElement('a');
      a.href = url;
      a.download = 'SETH_APOLLO'
            a.click();
        });
    });
</script>
	</body>

 </html>

              