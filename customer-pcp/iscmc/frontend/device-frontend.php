<?php
include("database/database.php");
$query = "SELECT device.sn,device.ip,nivel.color_pcp,nivel.nivel_pcp,device.id,  unidade.instituicao, setor.nome, area.nome, device.upgrade, device.reg_date FROM device INNER JOIN area ON device.id_area = area.id INNER JOIN setor ON setor.id = area.id_area INNER JOIN unidade ON unidade.id = setor.id_unidade INNER JOIN nivel on nivel.id = device.id_nivel where device.trash = 0 and device.status = 1";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($sn,$ip,$color,$nivel,$id, $unidade,$setor,$area,$update,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }

?>

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Registro  &amp; Consulta <small>Dispositivo</small></h3>
      </div>


    </div>

    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="userstable_filter" style="column-count:3; -moz-column-count:3; -webkit-column-count:3; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>        
                   
                  </div>
                </div>
              </div>
            </div>



    <div class="clearfix"></div>

    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Lista <small>Dispositivo</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                  </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>ID</th>

                          <th>Update</th>
                          <th>Nº Serie</th>
                          <th>IP</th>
                          <th>Cadastro</th>
                          <th>Dados</th>



                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                              <td><?php printf($update); ?></td>
                              <td><?php printf($sn); ?></td>
                              <td><?php printf($ip); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td>  <a class="btn btn-app"  href="device-dados?sn=<?php printf($sn); ?>"  onclick="new PNotify({
          																title: 'Dados',
          																text: 'Dados',
          																type: 'info',
          																styling: 'bootstrap3'
          														});">
                              <i class="fa fa-database"></i> Dados</td>

                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

              <!--    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Mover para lixeira</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <h4>Aviso!</h4>
                            <div class="alert alert-danger alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Importante!</strong> A partir dessa decisão essa informação não estar disponível para registro!
                  </div>
                          <p>Toda informação movida para lixeira pode ser consultada ou recuperada acessando a lixeira <?php printf($id); ?></p>
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <a  class="btn btn-primary"  href="backend/register-location-unit-trash-backend?id=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Excluir',
																text: 'Exluindo Informação',
																type: 'error',
																styling: 'bootstrap3'
														});">Salvar informação</a>
                        </div>

                      </div>
                    </div>
                  </div> -->
                </div>
              </div>
              </div>
              </div>
            </div>
            </div>
            <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
        initComplete: function () {
            this.api()
                .columns([1,2,3,4])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text(); 
                    var select = $('<select  class="form-control my-1"><option value="">' +
        theadname +'</option></select>')
                        .appendTo( '#userstable_filter'  ).select2()
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
 
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });

                
        },
    });
});
</script>
