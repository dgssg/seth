<?php


include("../database/database.php");

$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];
$nivel=$_POST['nivel'];
$id_device=$_POST['id_device'];
$id_user=$_POST['id_user'];
 
 
$nivel=trim($nivel);
$date_end=trim($date_end);
$date_start=trim($date_start);
 
if($nivel == ""){
  $nivel = "1,2,3,4";
}


$query = " SELECT nivel.nivel_pcp,  report.reg_date , report_device.nome,usuario.nome AS 'usuario',usuario.sobrenome FROM report  INNER JOIN nivel ON nivel.id = report.id_nivel LEFT JOIN report_device ON report.id_device = report_device.id  LEFT JOIN usuario ON usuario.id = report.id_user where report.id_nivel  IN ($nivel) ";

 

if($date_start==!""){
    $query = " $query and  report.reg_date > '$date_start'";
}

if($date_end==!""){
    $query = " $query and  report.reg_date < '$date_end'";
}
 
if($id_device==!""){
  $query = " $query and  report.id_device = $id_device";
}

if($id_user==!""){
  $query = " $query and  report.id_user = $id_user";
}


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_nivel,$reg_date,$device,$usuario,$sobrenome );



//header('Location: ../report-report-mc');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      <style>
    body {
    font-size: 12pt;
    font-family: Calibri;
    padding : 10px;
}
table {
    border: 1px solid black;
}
th {
    border: 1px solid black;
    padding: 5px;
    background-color:grey;
    color: white;
}
td {
    border: 1px solid black;
    padding: 5px;
}
input {
    font-size: 12pt;
    font-family: Calibri;
}
</style>
  </head>
  <body>
 <a class="btn btn-app"  href="../report-filter-viwer.php?query=<?php printf($query); ?>" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> PDF
                  </a>
                   <a class="btn btn-app"  id="btnExport">
                    <i class="fa fa-file-excel-o"></i> Excel
                  </a>

                    <div id="dvData">
<table >
                      <thead>
                        <tr>
                         <th>Nivel</th>
                         <th>Registro</th>
                         <th>Plataforma</th>
                          <th>Usuario</th>
 

                        </tr>
                      </thead>


                      <tbody>
                        <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                        <td><?php printf($id_nivel); ?> </td>
 
 <td><?php printf($reg_date); ?></td>
 <td><?php printf($device); ?></td>
 <td><?php printf($usuario); ?> <?php printf($sobrenome); ?></td>



                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                    </div>
                     <!-- jQuery 2.1.4 -->
    <script src="../../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../../framework/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="jquery.btechco.excelexport.js"></script>
<script src="jquery.base64.js"></script>
<script>
$(document).ready(function () {
 $("#btnExport").click(function (e) {
      e.preventDefault();
      var table_div = document.getElementById('dvData');
      // esse "\ufeff" é importante para manter os acentos
      var blobData = new Blob(['\ufeff'+table_div.outerHTML], { type: 'application/vnd.ms-excel' });
      var url = window.URL.createObjectURL(blobData);
      var a = document.createElement('a');
      a.href = url;
      a.download = 'SETH_PCP'
            a.click();
        });
    });
</script>
	</body>

 </html>
