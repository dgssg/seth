//
//  main.m
//  mks-seth
//
//  Created by GE on 12/02/24.
//  Copyright © 2024 Douglas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
