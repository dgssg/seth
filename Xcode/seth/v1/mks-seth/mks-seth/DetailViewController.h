//
//  DetailViewController.h
//  mks-seth
//
//  Created by GE on 12/02/24.
//  Copyright © 2024 Douglas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "mks_seth+CoreDataModel.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Event *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

