  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Manutenção <small>Preventiva</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>





            <div class="clearfix"></div>




              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

              
                                                 
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                  <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive' ? 'active' : ''; ?>" href="maintenance-preventive">
             <i class="glyphicon glyphicon-open"></i> Aberta
              <span class="badge bg-green">  <h3> <?php
                $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 1 ");
                $stmt->execute();
                $stmt->bind_result($result);
                while ( $stmt->fetch()) {
                echo "$result" ;
              }?> </h3></span>

          </a>

     
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-before' ? 'active' : ''; ?>" href="maintenance-preventive-before">

            <i class="glyphicon glyphicon-export"></i> Atrasada
            <span class="badge bg-red">  <h3> <?php
              $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 2 ");
              $stmt->execute();
              $stmt->bind_result($result);
              while ( $stmt->fetch()) {
              echo "$result" ;
            }?> </h3></span>
          </a>

        
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-close' ? 'active' : ''; ?>" href="maintenance-preventive-close">

            <i class="glyphicon glyphicon-save"></i> Fechada
          </a>

         
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-print' ? 'active' : ''; ?>" href="maintenance-preventive-print">

            <i class="glyphicon glyphicon-print"></i> Impressão

          </a>

                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-print-register' ? 'active' : ''; ?>" href="maintenance-preventive-print-register">

            <i class="glyphicon glyphicon-folder-open"></i> Registro de Impressão
          </a>

                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-control' ? 'active' : ''; ?>" href="maintenance-preventive-control">

            <i class="fa fa-check-square-o"></i> Controle
          </a>
          
                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-control-return' ? 'active' : ''; ?>" href="maintenance-preventive-control-return">

            <i class="fa fa-pencil-square-o"></i> Retorno
          </a>

                </div>
              </div>
<div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
               <div class="x_panel">
                <div class="x_title">
                  <h2>Lista MP Concluido</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <?php include 'frontend/maintenance-preventive-close-list-frontend.php';?>

                </div>
              </div>




                </div>
              </div>
 
