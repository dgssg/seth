<?php
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
// Create connection

//$conn_procedure = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_seth_iscmc");
include("../database/database.php");
include("../database/database-procedure.php");
$id_status=1;
$status=1;
$procedure=1;
$query = "SELECT equipamento.data_calibration_end,calibration_control.procedure_mp,calibration_control.data_after,calibration_control.id_routine,calibration_control.data_start ,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,calibration_routine.id,calibration_routine.data_start, calibration_routine.periodicidade,calibration_routine.reg_date,calibration_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM calibration_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN calibration_control on calibration_control.id_routine =  calibration_routine.id WHERE calibration_control.data_after <= '$today ' and calibration_control.status = 0 and calibration_routine.habilitado = 0 ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($data_calibration_end,$procedure,$data_after,$routine_control,$date_control,$instituicao,$area,$setor,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
  while ($stmt->fetch()) {

    if($data_calibration_end != ""){
      if($data_calibration_end < $data_after){
        $procedure=1;
      }
    }

    $stmt_procedure = $conn_procedure->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
    $stmt_procedure->bind_param("ssss",$rotina,$data_after,$id_status,$procedure);
    $execval = $stmt_procedure->execute();
    $last_id = $conn_procedure->insert_id;
    $stmt_procedure->close();
    $stmt_procedure = $conn_procedure->prepare("INSERT INTO calibration_print( id_routine) VALUES (?)");
    $stmt_procedure->bind_param("s",$rotina);

    $execval = $stmt_procedure->execute();
    $stmt_procedure->close();

    $stmt_procedure = $conn_procedure->prepare("UPDATE calibration_control SET status = ? WHERE id_routine= ?");
    $stmt_procedure->bind_param("ss",$status,$rotina);
    $execval = $stmt_procedure->execute();
    $stmt_procedure->close();

  }
}

 ?>
<script type="text/javascript">
       window.open('../flow-calibration-viwer-all', '_blank');
</script>
<script type="text/javascript">
       window.open('../flow-calibration-open-all', '_blank');
</script>
 <script type="text/javascript">
setTimeout(function(){var ww = window.open(window.location, '_self'); ww.close(); }, 1000);
</script>
