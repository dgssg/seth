<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;
	
	require '../../framework/PHPMailer/src/PHPMailer.php';
	require '../../framework/PHPMailer/src/SMTP.php';
	require '../../framework/PHPMailer/src/Exception.php';
	
	
	session_start();

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$setor = $_SESSION['setor'];
	$usuario = $_SESSION['id_usuario'];
	
	$codigoget = ($_GET["id"]);
	$id = ($_GET["routine"]);
	$id_mod = 2;
	// Create connection
	include("database/database.php");// remover ../
	$query = "SELECT email_adm FROM tools";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($email_adm);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET assessment = 0 WHERE id= ?");
	$stmt->bind_param("s",$id);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("INSERT INTO assessment_send (id_mod, id_number, id_usuario) VALUES (?,?,?)");
	$stmt->bind_param("sss",$id_mod,$id,$usuario);
	$execval = $stmt->execute();
	$last_id = $conn->insert_id;
	$stmt->close();

	$query="SELECT email FROM usuario WHERE id like '$usuario'";
	 
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($email_usuario);
	}
	$stmt->close();
	
	$query = " SELECT fornecedor.id,fornecedor_assessment.id,fornecedor.empresa,fornecedor_assessment.reg_date, fornecedor_question.question, fornecedor_assessment_question.id_question,fornecedor_assessment_question.avaliable, fornecedor_service.service, fornecedor_assessment.id_service,fornecedor_assessment.assessment,fornecedor_assessment.number_assessment, fornecedor_assessment.data_assessment, fornecedor_assessment.obs FROM fornecedor_assessment LEFT JOIN fornecedor_service ON fornecedor_service.id = fornecedor_assessment.id_service LEFT JOIN fornecedor_assessment_question ON fornecedor_assessment_question.id_assessment = fornecedor_assessment.id LEFT JOIN fornecedor_question ON fornecedor_question.id = fornecedor_assessment_question.id_question LEFT JOIN fornecedor ON fornecedor.id = fornecedor_assessment.id_fornecedor WHERE fornecedor_assessment.id = $codigoget ";
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($fornecedor,$id,$empresa,$reg_date,$question,$id_question,$avaliable,$service,$id_service,$assessment,$number_assessment,$data_assessment,$obs);
		
		while ($stmt->fetch()) { 
			
			switch ($avaliable) {
				case '1':
					$avaliable="Bom";
					break;
				case '2':
					$avaliable="Regular";
					break;
				case '3':
					$avaliable="Ruim";
					break; }
			switch ($assessment) {
				case '1':
					$assessment="Ordem de Serviço";
					$quer_assment="1";
					break;
				case '2':
					$assessment="Manutenção Preventiva";
					$quer_assment="2";
					break;
				case '3':
					$assessment="Calibração";
					$quer_assment="3";
					break;
				
				
			}
			$descricao="$descricao \n Fornecedor: $empresa\nFornecimento: $service \n Data avaliação: $reg_date\n Data do Serviço avaliado:$data_assessment \n Tipo do serviço:$assessment\nNumero do serviço:$number_assessment\n Avaliação:$question\n Conceito:$avaliable\nObservação:$obs \n";
			
		}
	}
	 
	switch ($quer_assment) {
		case '1':
			 $query = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM os INNER JOIN equipamento ON equipamento.id = os.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE os.id like  '$number_assessment'";
			break;
		case '2':
			$query = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM maintenance_preventive INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE maintenance_preventive.id  like '$number_assessment'";
			break;
		case '3':
			$query = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM calibration_preventive INNER JOIN calibration_routine ON calibration_routine.id = calibration_preventive.id_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE calibration_preventive.id  like '$number_assessment'";
			break;
		
		
	}
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($unidade, $unidade_cnpj, $unidade_adress, $unidade_cep, $unidade_bairro, $unidade_city);
		
		
		
		while ($stmt->fetch()) {
			
		}
	}
		$query = "SELECT id, empresa, cnpj, seguimento, adress, city, state, cep, contato, email, telefone_1, telefone_2, observacao, bairro, ibge, reg_date, upgrade,trash,assessment FROM fornecedor WHERE id like '$fornecedor'";
		if ($stmt = $conn->prepare($query)) {
			$stmt->execute();
			$stmt->bind_result($id_fornecedor, $empresa, $cnpj, $seguimento, $adress, $city, $state, $cep, $contato, $email, $telefone_1, $telefone_2, $observacao, $bairro, $ibge, $reg_date, $upgrade,$trash,$assessment);

		
		while ($stmt->fetch()) {
			
		}
	}
	$descricao=" 
Unidade: $unidade\n
CNPJ: $unidade_cnpj\n
Endereço: $unidade_adress $unidade_cep $unidade_bairro $unidade_city\n

Nome: $usuariologado
Email: $email_usuario
-------------------------------------------------------------------
\n\n $descricao \n\n\n\n  

Enviado por: $usuariologado
Contato para retorno: $email_usuario
-------------------------------------------------------------------
Mensagem automática enviada via sistema SETH. Não responda. MK Sistemas Biomedicos";
	$titulo="Avaliacao de Fornecedor $number_assessment";
	// send email
	$mail = new PHPMailer(true);
	
	try {
		// Configurações do servidor SMTP
		$mail->isSMTP();
		$mail->Host       = 'seth.mksistemasbiomedicos.com.br'; // Substitua pelo host do seu provedor
		$mail->SMTPAuth   = true;
		$mail->Username   = 'seth@seth.mksistemasbiomedicos.com.br'; // Substitua pelo seu endereço de e-mail
		$mail->Password   = 'mks@2024'; // Substitua pela sua senha
		$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Use 'tls' ou 'ssl' se necessário
		$mail->Port       = 587; // Porta do servidor SMTP
		
		// Configurações do e-mail
		$mail->setFrom('seth@seth.mksistemasbiomedicos.com.br', $usuariologado);
		$mail->addAddress($email, $empresa); // Substitua pelo endereço do destinatário
		$mail->addCC($email_adm); // Adicionar um e-mail em cópia (CC)

		$mail->Subject = $titulo;
		$mail->Body    = $descricao;
		
		// Enviar e-mail
		$mail->send();
		$email_grupo = 1; //Avaliação
		$email_mod = 2; // Preventiva
		$email_status =1; //Enviado
		$stmt = $conn->prepare("INSERT INTO email_control (email_grupo,email_mod, email_status, id_number,id_ususario) VALUES (?,?,?,?,?)");
		$stmt->bind_param("sssss",$email_grupo,$email_mod,$email_status,$id,$usuario);
		$execval = $stmt->execute();
		$last_id = $conn->insert_id;
		$stmt->close();
		echo "<script>document.location='maintenance-preventive-shut-down?sweet_salve=1&routine=$id'</script>";
		

	} catch (Exception $e) {
		$email_grupo = 1; //Avaliação
		$email_mod = 2; // Preventiva
		$email_status =2; // Não Enviado
		$stmt = $conn->prepare("INSERT INTO email_control (email_grupo,email_mod, email_status, id_number,id_ususario) VALUES (?,?,?,?,?)");
		$stmt->bind_param("sssss",$email_grupo,$email_mod,$email_status,$id,$usuario);
		$execval = $stmt->execute();
		$last_id = $conn->insert_id;
		$stmt->close();
		echo "<script>document.location='maintenance-preventive-shut-down?sweet_delet=1&routine=$id'</script>";

	}
	
	
?>
