<?php
// Include the main TCPDF library (search for installation path).
require_once('../../framework/TCPDF/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('MK Sistemas Biomedicos');
$pdf->SetTitle('Sistema SETH');
$pdf->SetSubject('CERTIFICADO');
$pdf->SetKeywords('CERTIFICADO');

// set default header data
$pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH, 'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$codigoget = $_GET["id"];
  include("database/database.php");
  $print= date(DATE_RFC2822);
  
  $query = "SELECT user_training_attempts.id,usuario.nome,usuario.sobrenome,user_training_attempts.attempt_date,user_training_attempts.score,user_training_attempts.status,documentation_graduation.titulo,documentation_graduation.ativo,category.nome AS 'categoria',equipamento_familia.nome AS 'equipamento',equipamento_familia.fabricante,equipamento_familia.modelo,documentation_graduation.dropzone FROM user_training_attempts LEFT JOIN  documentation_graduation ON user_training_attempts.id_training = documentation_graduation.id LEFT JOIN category ON category.id = documentation_graduation.id_categoria LEFT JOIN equipamento_familia ON equipamento_familia.id = documentation_graduation.id_equipamento_familia LEFT JOIN usuario ON usuario.id = user_training_attempts.id_user WHERE user_training_attempts.id = $codigoget ";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$nome,$sobrenome,$attempt_date,$score,$status,$titulo,$ativo,$categoria,$equipamento,$fabricante,$modelo,$dropzone);
    
    
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  
   
$html = <<<EOD
<style>
    body {
    font-size: 12pt;
    font-family: Calibri;
    padding : 10px;
}
table {
    border: 1px solid black;
}
th {
    border: 1px solid black;
    padding: 5px;
    background-color:grey;
    color: white;
}
td {
    border: 1px solid black;
    padding: 5px;
}
input {
    font-size: 12pt;
    font-family: Calibri;
}
</style>
  

<style type="text/css">
page {
  /* background: white;*/
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  text-align: left;
}

page[size="A4"] {
  width: 21cm;
  /*height: 29.7cm;                */
}

page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /* height: 21cm; */
}

@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
    page-break-after:always;
  }

  .noprint {
    display: none;
  }
}




body {
  font-size: 11px;
  font-family: sans-serif;
}



.pagina_retrato {
  width: 720px;
}

.pagina_paisagem {
  width: 1025px;
}

.relatorio_filtro {
  padding: 10px 0px;
  border-collapse: collapse;
}

.relatorio_filtro tr td {
  border: 1px solid #000;
}


.relatorio_titulo {
  font-family: Times New Roman;
  font-size: 15px;
  color: Black;
  font-weight: bold;
  text-transform: uppercase;
}

.borda_celula {
  border: 1px solid #000;
  background-color: #fff;
}

.noprint {
  padding-bottom: 10px;
}
.right{
  float:right;
}

.left{
  float:left;
}
</style>

<style type="text/css" media="print">
/*
@page {
margin: 0.8cm;
}
*/
.pagina_retrato, .pagina_paisagem {
width: 100%;
}
 
</style>
 

   <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <style>
    /* Estilos para o cabeçalho */
    header {
      padding: 20px;
      background-color: #f5f5f5;
      border-bottom: 1px solid #ddd;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      padding: 5px;
      border: 1px solid #ddd;
    } 

    #logo {
      width: 100px;
    }

    h1 {
      margin: 0;
      font-size: 24px;
    }

    #info {
      font-size: 14px;
    }
  </style>
  <style>
    body {
      font-family: Arial, sans-serif;
      text-align: center;
      background-color: #f3f3f3;
    }
    .certificate-container {
      width: 80%;
      margin: 50px auto;
      padding: 30px;
      background: white;
      border: 5px solid #333;
      text-align: center;
      box-shadow: 5px 5px 15px rgba(0,0,0,0.2);
    }
    h1 {
      font-size: 40px;
      color: #2C3E50;
    }
    .user-name {
      font-size: 30px;
      font-weight: bold;
      color: #16A085;
    }
    .training-title {
      font-size: 24px;
      margin: 15px 0;
    }
    .score {
      font-size: 20px;
      color: #E74C3C;
    }
    .date {
      font-size: 18px;
      margin-top: 10px;
    }
    .signature {
      margin-top: 50px;
      font-size: 18px;
      font-style: italic;
    }
  </style>

<div class="conteudo" style="min-height:900px; font-size:17px;">
<table style="border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
    <tr>
   
 <td style="width: 80%; text-align: center;">
                        
 <h7>Certificado Educação Continuada</h7><br>
       
            <h9>$titulo</h9>
  

            </td>
        
        <td style="width: 20%;">
       <div>
        <img src="logo/clientelogo.png" alt="Logo da empresa" id="logo" width="60">
  </div>
        </td>
    </tr>
    
</table>

<br>
  <div class="certificate-container">
    <h1>Certificado de Conclusão</h1>
    <p>Certificamos que</p>
    <p class="user-name">$nome $sobrenome </p>
    <p>concluiu com êxito o treinamento</p>
    <p class="training-title">$titulo</p>
    <p>com uma pontuação de</p>
    <p class="score">$score</p>
    <p class="date">Data: $attempt_date</p>
    <p class="signature">Assinatura do Instrutor</p>
  </div>

 <br>
 <p><b><center><h2><small></small></h2></center></b></p>
           
       
<!-- Rodapé -->
<table width="100%" style="vertical-align: top;font-family:'Times New Roman',Times,serif; font-size: 11px;">
    <tr>
        <td align="center" colspan="2">
            <div style="padding-top: 20px;"><strong>Copyright © 2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados.</strong><br>www.mksistemasbiomedicos.com.br</div>
        </td>
    </tr>
    <tr>
        <td style="font-size: 9px" align="left">MK-01-ra-1.0</td>
        <td align="right"><!-- pagina --></td>
    </tr>
</table>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// Close and output PDF document
$pdf->Output('Certificado.pdf', 'I');
?>
