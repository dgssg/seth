<?php
include("../database/database.php");
$query = "SELECT
    db.id,
    db.nome,
    db.data_now,
    db.data_before,
    db.upgrade,
    db.reg_date,
    db.view,
    rdz.file AS dropzone
FROM
    documentation_bussines db
LEFT JOIN (
    SELECT
        id_bussines,
        file,
        reg_date,
        @rn := IF(@prev_id = id_bussines, @rn + 1, 1) AS rn,
        @prev_id := id_bussines
    FROM (
        SELECT
            id_bussines,
            file,
            reg_date
        FROM
            regdate_bussines_dropzone
        ORDER BY
            id_bussines, reg_date DESC
    ) AS subquery,
    (SELECT @prev_id := NULL, @rn := 0) AS init
    ORDER BY
        id_bussines, reg_date DESC
) AS rdz ON rdz.id_bussines = db.id AND rdz.rn = 1
ORDER BY
    db.id DESC;
";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
