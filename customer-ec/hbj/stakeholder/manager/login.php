<?php
session_id($usuario);
include("../../database/database.php");
    session_start();
    
   if (isset($_SESSION['session_token'])) {
    // Preparar a consulta para atualizar o token no banco de dados
    $stmt = $conn->prepare("UPDATE usuario SET session_token = NULL WHERE session_token = ?");
    
    if ($stmt) {
        // Associar o parâmetro e executar a consulta
        $stmt->bind_param('s', $_SESSION['session_token']);
        $stmt->execute();
        $stmt->close();
    } else {
        // Registrar erro se a preparação falhar
        error_log("Erro ao preparar a consulta para limpar o session_token: " . $conn->error);
    }
}

    
    session_destroy();
session_commit();
header ("location: ../../index.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Meu Redirect</title>

    <meta http-equiv="refresh" content="1; URL='https://www.seth.mksistemasbiomedicos.com.br/'"/>
</head>
<body>
</body>

</html> 

