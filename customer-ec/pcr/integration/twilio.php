<?php
// Credenciais Twilio
$account_sid = "AC62155517daedd8ca9ad15bb41a57502e";
$auth_token  = "44c2ae1babd010704ea46ad2e09da632";
$twilio_number = "+15802048400";
$to_number = "+5541998945903"; // Ex: +5511999999999

// Mensagem
$message = "Olá, este é um SMS enviado via Twilio e PHP!";

// Enviar via cURL
$url = "https://api.twilio.com/2010-04-01/Accounts/$account_sid/Messages.json";

$data = [
    'From' => $twilio_number,
    'To'   => $to_number,
    'Body' => $message
];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_USERPWD, "$account_sid:$auth_token");

$response = curl_exec($ch);
curl_close($ch);

echo "Resposta: " . $response;
?>
