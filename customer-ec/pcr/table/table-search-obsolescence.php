<?php
include("../database/database.php");
$query=" SELECT instituicao.instituicao, instituicao_area.nome AS 'setor',instituicao_localizacao.nome AS 'area',equipamento.codigo,equipamento.serie,equipamento.id,equipamento_familia.nome AS 'equipamento',equipamento_familia.fabricante,equipamento_familia.modelo,obsolescence_year.yr,obsolescence_rooi.rooi,obsolescence.id, obsolescence.id_equipamento, obsolescence.id_year, obsolescence.id_c1, obsolescence.id_c2, obsolescence.id_c3, obsolescence.id_c4, obsolescence.id_c5, obsolescence.id_c6, obsolescence.id_c7, obsolescence.id_rooi, obsolescence.score, obsolescence.obs, obsolescence.vlr_market, obsolescence.vlr_purchases, obsolescence.obs_purchases, obsolescence.upgrade, obsolescence.reg_date FROM obsolescence INNER JOIN obsolescence_rooi on obsolescence_rooi.id = obsolescence.id_rooi INNER JOIN obsolescence_year on obsolescence_year.id = obsolescence.id_year INNER JOIN equipamento on equipamento.id = obsolescence.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade ORDER BY obsolescence.id,obsolescence_year.yr,equipamento_familia.nome DESC ";
// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
