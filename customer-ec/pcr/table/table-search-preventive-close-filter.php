<?php
include("../database/database.php");

// Inicialize a variável da query com a consulta base
$query = "
SELECT maintenance_group.nome AS 'grupo', equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome AS 'setor',instituicao_localizacao.nome AS 'area', maintenance_preventive.id_routine ,maintenance_routine.id AS 'rotina',maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome AS 'equipamento',equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN maintenance_group ON maintenance_group.id = maintenance_routine.group_routine WHERE maintenance_preventive.id_status like '3' and maintenance_routine.trash = 1 
";

// Adicione os filtros se os parâmetros estiverem definidos
$filters = [];
if (isset($_GET['rotina'])) {
    $filters[] = "maintenance_routine.id = " . intval($_GET['rotina']);
}
    
    if (isset($_GET['unidade'])) {
        $filters[] = "instituicao.instituicao LIKE '%" . $conn->real_escape_string($_GET['unidade']) . "%'";
    }
    if (isset($_GET['setor'])) {
        $filters[] = "instituicao_area.nome LIKE '%" . $conn->real_escape_string($_GET['setor']) . "%'";
    }
    if (isset($_GET['area'])) {
        $filters[] = "instituicao_localizacao.nome LIKE '%" . $conn->real_escape_string($_GET['area']) . "%'";
    }
    if (isset($_GET['familia'])) {
        $filters[] = "equipamento_familia.nome LIKE '%" . $conn->real_escape_string($_GET['familia']) . "%'";
    }
    if (isset($_GET['fabricante'])) {
        $filters[] = "equipamento_familia.fabricante LIKE '%" . $conn->real_escape_string($_GET['fabricante']) . "%'";
    }
    if (isset($_GET['modelo'])) {
        $filters[] = "equipamento_familia.modelo LIKE '%" . $conn->real_escape_string($_GET['modelo']) . "%'";
    }
    if (isset($_GET['codigo'])) {
        $filters[] = "equipamento.codigo LIKE '%" . $conn->real_escape_string($_GET['codigo']) . "%'";
    }
    if (isset($_GET['patrimonio'])) {
        $filters[] = "equipamento.patrimonio LIKE '%" . $conn->real_escape_string($_GET['patrimonio']) . "%'";
    }
    if (isset($_GET['serie'])) {
        $filters[] = "equipamento.serie LIKE '%" . $conn->real_escape_string($_GET['serie']) . "%'";
    }
if (isset($_GET['grupo'])) {
        $filters[] = "equipamento_grupo.nome LIKE '%" . $conn->real_escape_string($_GET['grupo']) . "%'";
}
    

if (isset($_GET['ativo'])) {
    $filters[] = "equipamento.ativo = " . intval($_GET['ativo']);
}
    if (isset($_GET['baixa'])) {
        $filters[] = "equipamento.baixa = " . intval($_GET['baixa']);
    }
    if (isset($_GET['comodato'])) {
        $filters[] = "equipamento.comodato = " . intval($_GET['comodato']);
    }
// Se houver filtros, adicione-os na query
if (count($filters) > 0) {
    $query .= " AND " . implode(" AND ", $filters);
}

$query .= "GROUP BY maintenance_preventive.id_routine order by maintenance_routine.id DESC";

// Execute a consulta e retorne os resultados como JSON
$resultados = $conn->query($query);

if (!$resultados) {
    die("Erro na consulta ao banco de dados: " . $conn->error);
}

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

// Configurar o cabeçalho para JSON
header('Content-Type: application/json');
echo json_encode($rows);

$conn->close();
?>
