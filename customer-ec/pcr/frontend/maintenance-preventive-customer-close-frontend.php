
<style>
  .btn.btn-app {
    border: 2px solid transparent; /* Define a borda como transparente por padrão */
    padding: 5px;
    position: relative; /* Necessário para posicionar o pseudo-elemento */
  }
  
  .btn.btn-app.active {
    border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
  }
  
  .btn.btn-app.active::after {
    content: "";
    position: absolute;
    left: 0;
    bottom: 0; /* Posição da linha no final do elemento */
    width: 100%;
    height: 3px; /* Espessura da linha */
    background-color: #ffcc00; /* Cor da linha */
  }
  
</style>
<?php
  $current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Fechada  <small> Manutenção Preventiva </small> Cliente</h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                  
                    <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-customer-register' ? 'active' : ''; ?>" href="maintenance-preventive-customer-register">
                      <i class="glyphicon glyphicon-plus"></i> Cadastro
                    </a>   
                    <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-customer-open' ? 'active' : ''; ?>" href="maintenance-preventive-customer-open">
                      <i class=" glyphicon glyphicon-open"></i> Aberto
                    </a> 
                    <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-customer-close' ? 'active' : ''; ?>" href="maintenance-preventive-customer-close">
                      <i class="glyphicon glyphicon-save"></i> Fechada
                    </a> 
                    


                  </div>
                </div>
              </div>
	       </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="userstable_filter" style="column-count:2; -moz-column-count:2; -webkit-column-count:2; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                    
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fechada  <small> Manutenção Preventiva </small> Cliente</h2>                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
  
         
 
                    <?php
                      date_default_timezone_set('America/Sao_Paulo');
                      $today = date("Y-m-d");
               
                      
                      
                      $query = "SELECT regdate_mp_dropzone_customer.file,maintenance_customer.id,maintenance_customer.equipamento, maintenance_customer.serie,maintenance_customer.reg_date,customer.empresa,customer.cnpj,maintenance_customer.reg_date,maintenance_customer.data,maintenance_customer.validade FROM maintenance_customer INNER JOIN customer ON customer.id = maintenance_customer.id_customer  LEFT JOIN regdate_mp_dropzone_customer ON regdate_mp_dropzone_customer.id_mp = maintenance_customer.id WHERE maintenance_customer.status like '2' ORDER BY maintenance_customer.id DESC ";
                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($file,$id,$equipameto,$serie,$reg_date,$empresa,$cnpj,$reg_date,$data,$validade);
                        
                        
                        
                    ?>
                    
                    
                    <div class="col-md-11 col-sm-11 ">
                      <div class="x_panel">
                        
                        <div class="x_content">
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="card-box table-responsive">
                                
                                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th></th>
                                      <th><input type="checkbox" id="check_all"></th>
                                       <th>Data</th>
                                      <th>Empresa</th>
                                      <th>CNPJ</th>
                                      <th>Equipamento</th>
                                      <th>Serie</th>
                                     
                                      <th>Validade</th>
                                      <th>Registro</th>
                                      <th>Ação</th>
                                      
                                    </tr>
                                  </thead>
                                  
                                  
                                  <tbody>
                                    <?php   while ($stmt->fetch()) {   ?>
                                    <tr>
                                      <td > </td>
                                     
                                      <td > <input type="checkbox" class="checkbox" value="<?php printf($id); ?>"> </td>
                                       <td><?php printf($data); ?></td>                               
                                      <td><?php printf($empresa); ?></td>
                                      <td><?php printf($cnpj); ?></td>
                                      <td><?php printf($equipameto); ?></td>
                                      <td><?php printf($serie); ?></td>
                                    
                                      <td><?php printf($validade); ?></td>
                                      <td><?php printf($reg_date); ?></td>
                                      
                                      <td>
                                        
                                       
                                        
                                        <a class="btn btn-app"  href="maintenance-preventive-open-tag-customer?id=<?php printf($id); ?>" target="_blank"   onclick="new PNotify({
                                          title: 'Visualizar',
                                          text: 'Visualizar Etiqueta',
                                          type: 'info',
                                          styling: 'bootstrap3'
                                        });">
                                          <i class="fa fa-qrcode"></i> Etiqueta
                                        </a>
                                        
                                       
                                        <a class="btn btn-app"  href="maintenance-preventive-viwer-print-customer?id=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
                                          title: 'Imprimir',
                                          text: 'Imprimir Rotina',
                                          type: 'info',
                                          styling: 'bootstrap3'
                                        });">
                                          <i class="fa fa-print"></i> Imprimir
                                        </a>
                                     
                                        <a class="btn btn-app"    onclick="
                                          Swal.fire({
                                            title: 'Tem certeza?',
                                            text: 'Você não será capaz de reverter isso!',
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Sim, Deletar!'
                                          }).then((result) => {
                                            if (result.isConfirmed) {
                                              Swal.fire(
                                                'Deletando!',
                                                'Seu arquivo será excluído.',
                                                'success'
                                              ),
                                              window.location = 'backend/maintenance-preventive-trash-customer-backend?id=<?php printf($id); ?>';
                                            }
                                          })
                                        ">
                                          <i class="fa fa-trash"></i> Excluir</a>
                                        <a class="btn btn-app" onclick="
                                          Swal.fire({
                                            title: 'Tem certeza?',
                                            text: 'Você não será capaz de reverter isso!',
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Sim, Enviar Manutenção Preventiva!'
                                          }).then((result) => {
                                            if (result.isConfirmed) {
                                              Swal.fire(
                                                'Enviando!',
                                                'Seu arquivo será enviado.',
                                                'success'
                                              ),
                                              window.location = 'api-assessment-manufacture-send-mp-customer?id=<?php printf($id); ?> ';
                                            }
                                          })
                                        ">
                                          <i class="fa fa-at"></i> Email
                                        </a>
                                        <a class="btn btn-app"  href="maintenance-preventive-viwer-digital-print-customer?id=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
                                          title: 'Visualizar',
                                          text: 'Visualizar procediemnto',
                                          type: 'info',
                                          styling: 'bootstrap3'
                                        });">
                                          <i class="fa fa-file-pdf-o"></i> Visualizar Digital
                                        </a>
                                        <?php if($file != "") {  ?>
                                        <a class="btn btn-app" href="dropzone/mp-dropzone/<?php printf($file); ?>" download><i class="fa fa-paperclip"></i></a>
                                        <?php }  ?>
                                      </td>
                                    </tr>
                                    <?php   } }  ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>




                  </div>
                </div>
              </div>
	       </div>
	       
	       
          

            
	        
                  
                  
                </div>
              </div>
              
              
              <script type="text/javascript">
                $(document).ready(function () {
                  $('#check_all').click(function() {
                    $('input[type=checkbox]').prop('checked', this.checked); // Seleciona todas as checkboxes
                  });
                  $('#datatable').DataTable({
                    
                    "processing": true,
                    "stateSave": true,
                    responsive: true,
                    
                    
                    
                    
                    "language": {
                      "loadingRecords": "Carregando dados...",
                      "processing": "Processando  dados...",
                      "infoEmpty": "Nenhum dado a mostrar",
                      "emptyTable": "Sem dados disponíveis na tabela",
                      "zeroRecords": "Não há registros a serem exibidos",
                      "search": "Filtrar registros:",
                      "info": "Mostrando página _PAGE_ de _PAGES_",
                      "infoFiltered": " - filtragem de _MAX_ registros",
                      "lengthMenu": "Mostrar _MENU_ registros",
                      
                      "paginate": {
                        "previous": "Página anterior",
                        "next": "Próxima página",
                        "last": "Última página",
                        "first": "Primeira página",
                        
                        
                        
                      }
                    },
                    initComplete: function () {
                      this.api()
                      .columns([2,3,4,5,6,7])
                      .every(function (d) {
                        
                        
                        var column = this;
                        var theadname = $("#datatable th").eq([d]).text();
                        
                        // Container para o título, o select e o alerta de filtro
                        var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
                        
                        // Título acima do select
                        var title = $('<label>' + theadname + '</label>').appendTo(container);
                        
                        // Container para o select
                        var selectContainer = $('<div></div>').appendTo(container);
                        
                        var select = $('<select class="form-control my-1"><option value="">' +
                          theadname + '</option></select>').appendTo(selectContainer).select2()
                        .on('change', function() {
                          var val = $.fn.dataTable.util.escapeRegex($(this).val());
                          column.search(val ? '^' + val + '$' : '', true, false).draw();
                          
                          // Remove qualquer alerta existente
                          container.find('.filter-alert').remove();
                          
                          // Se um valor for selecionado, adicionar o alerta de filtro
                          if (val) {
                            $('<div class="filter-alert">' +
                              '<span class="filter-active-indicator">&#x25CF;</span>' +
                              '<span class="filter-active-message">Filtro ativo</span>' +
                              '</div>').appendTo(container);
                          }
                          
                          // Remove o indicador do título da coluna
                          $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
                          
                          // Se um valor for selecionado, adicionar o indicador no título da coluna
                          if (val) {
                            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
                          }
                        });
                        
                        column.data().unique().sort().each(function(d, j) {
                          select.append('<option value="' + d + '">' + d + '</option>');
                        });
                        var filterValue = column.search();
                        if (filterValue) {
                          select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
                        }
                        
                        
                      });
                      // Adicionar a configuração inicial da DataTable
                      table.on('init.dt', function() {
                        // Verifica se há filtros aplicados ao carregar a página
                        table.columns().every(function() {
                          var column = this;
                          var searchValue = column.search();
                          if (searchValue) {
                            var select = $('#userstable_filter select').eq(column.index());
                            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
                          }
                        });
                      }); 
                      column
                      .data()
                      .unique()
                      .sort()
                      .each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                      });
                      
                      
                    },
                  });
                });
                
                
                function takeAction() {
                  var selectedRows = [];
                  $('.checkbox:checked').each(function() {
                    selectedRows.push($(this).val());
                  });
                  
                  // Verifica se pelo menos um item foi selecionado
                  if (selectedRows.length > 0) {
                    // Constrói o URL com os IDs dos itens selecionados
                    var url = 'maintenance-preventive-viwer-print-selected-customer?id=' + selectedRows.join(',');
                    // Abre o URL em uma nova aba
                    window.open(url, '_blank');
                    var url = 'maintenance-preventive-open-tag-selected-customer?id=' + selectedRows.join(',');
                    // Abre o URL em uma nova aba
                    window.open(url, '_blank');
                  } else {
                    // Se nenhum item foi selecionado, mostra uma mensagem
                    alert('Por favor, selecione pelo menos um item.');
                  }
                }
                
                             
                
              </script>
              