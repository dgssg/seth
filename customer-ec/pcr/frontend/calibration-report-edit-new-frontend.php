  <?php
$codigoget = ($_GET["laudo"]);

// Create connection

include("database/database.php");
$query = "SELECT equipamento_familia.id_equipamento_grupo,calibration.temp,calibration.hum,calibration.obs,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade,calibration.procedure_cal,calibration_parameter_manufacture.nome,calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration INNER JOIN equipamento on equipamento.id = calibration.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN calibration_parameter_manufacture on calibration_parameter_manufacture.id =  calibration.id_manufacture INNER JOIN colaborador on colaborador.id = calibration.id_colaborador WHERE calibration.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_equipamento_grupo,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  }
 
}

$query = "SELECT colaborador.primeironome,colaborador.ultimonome,colaborador.entidade FROM calibration INNER JOIN colaborador on colaborador.id = calibration.id_responsavel WHERE calibration.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($primeironome_responsavel,$ultimonome_responsavel,$entidade_responsavel );
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  }
 
}

?>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Laudos de <small>Calibração</small></h3>
              </div>

           
            </div>
            <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="calibration-report">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
             
               
            
                  
                </div>
              </div>
        <div class="x_panel">
                <div class="x_title">
                  <h2>Alerta</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
             <div class="alert alert-info alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Area de Elaboração!</strong> Todo alteração é integrada.
                  </div>
                
                 
                  
                </div>
              </div>
       
              
              
              
              
        
              
                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Edição de Laudo </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Laudo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($id); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Validade <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($val); ?> Meses" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Data Realização <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($data_start); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
                    
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Empresa <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($manufacture); ?> " readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
                    
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> -  <?php printf($fabricante); ?> - <?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
                      
                        
                    
                    
                    
                    
                    
                </div>
              </div>  
            
             <div class="x_panel">
                <div class="x_title">
                  <h2>Procedimento</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                <?php


$query = "SELECT   pop, titulo, codigo FROM documentation_pop WHERE id_equipamento_grupo like '$id_equipamento_grupo' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($pop, $titulo,$codigo);
  while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
 

?>
                   
                 <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Procedimento</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"   value="<?php printf($pop); ?> - <?php printf($titulo); ?> - <?php printf($codigo); ?>" readonly="readonly">
                        </div>
                      </div>
             
               
            
                  
                </div>
              </div>  
              
              <div class="x_panel">
                <div class="x_title">
                  <h2>Executantes</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
               
                   <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Executante <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-4 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($primeironome); ?> <?php printf($ultimonome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                         <label class="col-form-label col-md-2 col-sm-2 label-align" for="nome">CREA/CFT <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-2 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($entidade); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
             <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Responsavel Técnico <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-4 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($primeironome_responsavel); ?> <?php printf($ultimonome_responsavel); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                        <label class="col-form-label col-md-2 col-sm-2 label-align" for="nome">CREA/CFT  <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-2 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($entidade_responsavel); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
               
            
                  
                </div>
              </div>  
              
              <div class="x_panel">
                <div class="x_title">
                  <h2>Condições de Calibração</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
               
                   <div class="item form-group">
                         <label class="col-form-label col-md-2 col-sm-2 label-align"  for="nome">Temperatura <span class="required">(ºC)</span>
                        </label>
                       <div class="col-md-2 col-sm-2 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($temperatura); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                         <label class="col-form-label col-md-2 col-sm-2 label-align" for="nome">Umidade <span class="required">(%)</span>
                        </label>
                        <div class="col-md-2 col-sm-2 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($umidade); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div> 
            <label for="message_ne">Observação:</label>
                          <textarea id="message_ne"  class="form-control" name="message_ne" data-parsley-trigger="keyup"  
                             placeholder="<?php printf($obs); ?>"readonly="readonly"><?php printf($obs); ?></textarea>
               
            
                  
                </div>
              </div>
              
                   <div class="x_panel">
                <div class="x_title">
                  <h2>Registrar ensaio</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <form  action="backend/calibration-report-edit-backend.php?laudo=<?php printf($codigoget);?>" method="post">
                  
             
                 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Parâmetro de Ensaio  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="parametro" id="parametro"  placeholder="parametro">
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  calibration_parameter_dados_parameter.id, calibration_parameter_dados_parameter.nome, calibration_parameter_dados_parameter.unidade,calibration_parameter_dados.id FROM calibration_parameter_dados_parameter INNER JOIN calibration_parameter_dados on calibration_parameter_dados.id = calibration_parameter_dados_parameter.id_calibration_parameter_dados WHERE calibration_parameter_dados.id_equipamento_grupo = '$id_equipamento_grupo'";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome,$unidade,$id_equipamento_grupo_row);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($nome);?>-<?php printf($unidade);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
									 
										
								      	</div>	
								      	</div>
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#parametro').select2();
                                      });
                                 </script>
                                 
                                 	     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Equipamento &amp; Analisador  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="tools" id="tools"  placeholder="Equipamento">
										    	
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT calibration_parameter_tools_dados.id, calibration_parameter_tools_dados.nome,calibration_parameter_tools_dados.unidade,calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo FROM calibration_parameter_tools  INNER JOIN calibration_parameter_tools_dados on calibration_parameter_tools_dados.id_calibration_parameter_tools = calibration_parameter_tools.id";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$parametro, $unidade, $equipamento,$fabricante,$modelo);
     while ($stmt->fetch()) {
     ?>
    
<option value="<?php printf($id);?>	"> <?php printf($equipamento);?>	<?php printf($fabricante);?> <?php printf($modelo);?> <?php printf($parametro);?> <?php printf($unidade);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
											
	                                     	</select>  
										 
										
										
								      	</div>	
								      	</div>	
								 <script>
                                    $(document).ready(function() {
                                    $('#tools').select2();
                                      });
                                 </script>
                                 
                            <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="k">FATOR DE ABRANGÊNCIA (K) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="k" name="k" required="required" class="form-control ">
                        </div>
                      </div> 
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Valor de Referência  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="ve" name="ve"  required="required" class="form-control ">
                        </div>
                      </div> 
                      
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_1">1º Ensaio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="vl_1" name="vl_1"  required="required" class="form-control ">
                        </div>
                      </div> 
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_2">2º Ensaioo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="vl_2" name="vl_2"  required="required" class="form-control ">
                        </div>
                      </div> 
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_3">3º Ensaio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="vl_3" name="vl_3" required="required" class="form-control ">
                        </div>
                      </div> 
               <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_3"> <span class="required">*</span>
                        </label>           
            <div class="col-md-6 col-sm-6 ">
                         
                           	<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" />
                        
                        </div>
                        </form>
                  
                </div>
              </div> 
              
     <!--          <div class="x_panel">
                <div class="x_title">
                  <h2>Ensaio</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  
           <?php 
           $query="SELECT  id FROM calibration_parameter_dados  WHERE id_equipamento_grupo = '$id_equipamento_grupo '";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id);
   while ($stmt->fetch()) {
       $id=$id;
   }
}

$query="SELECT  calibration_parameter_tools_dados.nome,calibration_parameter_tools_dados.unidade,calibration_parameter_tools_dados.i_h,calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo,calibration_report.id, IFNULL(calibration_report.ve,0),IFNULL(calibration_report.vl_1,0),IFNULL(calibration_report.vl_2,0),IFNULL(calibration_report.vl_3,0),IFNULL(calibration_report.vl_avg,0),IFNULL(calibration_report.erro,0),IFNULL(calibration_report.dp,0),IFNULL(calibration_report.ie,0),IFNULL(calibration_report.ia,0),IFNULL(calibration_report.k,0),IFNULL(calibration_report.ib,0),IFNULL(calibration_report.ic,0),calibration_parameter_dados_parameter.nome, calibration_parameter_dados_parameter.unidade FROM calibration_parameter_dados_parameter INNER JOIN calibration_report on calibration_report.id_calibration_parameter_dados_parameter = calibration_parameter_dados_parameter.id  AND IFNULL(calibration_report.id_calibration = '$codigoget',true) INNER JOIN calibration_parameter_tools on calibration_parameter_tools.id = calibration_report.id_calibration_parameter_tools INNER JOIN calibration_parameter_tools_dados on calibration_parameter_tools_dados.id = calibration_report.id_calibration_parameter_tools_dados WHERE calibration_parameter_dados_parameter.id_calibration_parameter_dados = '$id '  ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($tools_dados_nome,$tools_dados_unidade,$tools_dados_ih,$tools_nome,$tools_fabricante,$tools_modelo,$id,$ve, $vl_1, $vl_2, $vl_3, $vl_avg, $erro, $dp, $ie,$ia,$k,$ib,$ic,$nome,$unidade);
  
?>
 <?php  while ($stmt->fetch()) { ?>
 
  <td><?php printf($nome); ?></td> <td><?php printf($unidade); ?></td><br>
  <td>Analisador &amp; Simulador :</td> <td><?php printf($tools_nome); ?> <?php printf($tools_fabricante); ?> <?php printf($tools_modelo); ?> </td><br>
  <td>Parametro:</td> <td> <?php printf($tools_dados_nome); ?> <?php printf($tools_dados_unidade); ?></td><br>
  <td>Incerteza Herdada:</td> <td> <?php printf($tools_dados_ih); ?></td><br>
                    <table class="table table-striped">
                      <thead>
                          
                        <tr>
                          <th>#</th>
                          <th>VE</th>
                          <th>VL-1</th>
                          <th>VL-2</th>
                          <th>VL-3</th>
                          <th>VL-AVG</th>
                          <th>Erro</th>
                          <th>DP</th>
                          <th>IA</th>
                          <th>IB</th>
                          <th>IC</th>
                          <th>K</th>
                          <th>IE</th>
                        
                          <th>Ação</th>
                          
                          </tr>
                      </thead>
                      <tbody>
                            
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <th ><?php printf($ve); ?></th>
                          <th ><?php printf($vl_1); ?></th>
                          <th ><?php printf($vl_2); ?></th>
                          <th ><?php printf($vl_3); ?></th>
                          <th ><?php printf($vl_avg); ?></th>
                          <th ><?php printf($erro); ?></th>
                          <th ><?php printf($dp); ?></th>
                          <th ><?php printf($ia); ?></th>
                          <th ><?php printf($ib); ?></th>
                          <th ><?php printf($ic); ?></th>
                           <th ><?php printf($k); ?></th>
                          <th ><?php printf($ie); ?></th>
                           <th >
                               <center><a  class="btn btn-round "   onclick="open('calibration-report-edit-itens?id=<?php printf($id); ?>','calibration-report-edit-itens?id=<?php printf($id); ?>','status=no,Width=320,Height=285');"><i class="fa fa-edit "></i></a>  
                                <a   class="btn btn-round " onclick="open(' calibration-report-drop-itens?id=<?php printf($id); ?>','calibration-report-drop-itens?id=<?php printf($id); ?>','status=no,Width=320,Height=285');"><i class="fa fa-trash"></i></a>
                                	<a  class="btn btn-round "  value="Refresh Page" onClick="window.location.href=window.location.href" > <i class="fa fa-calculator"></i></a>
                                </center>
                           </th>
                          
                        </tr>
                        
                  
                      </tbody>
                    </table>

               <?php  $row=$row+1; }}   ?>  
              
                
                </div>
              </div> ->
              
              <!-- Posicionamento -->
           <!--    <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Posicionamento</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                           <form action="backend/os-progress-upgrade-position-backend.php?os=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>
                             <div class="item form-group">
                             <label class="col-form-label col-md-3 col-sm-3 label-align" for="posicionamento" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Posicionamento ">Posicionamento <span class="required" >*</span>
                             </label>
                             <div class="col-md-6 col-sm-6 ">
                         	 <select type="text" class="form-control has-feedback-right" name="posicionamento" id="posicionamento"  placeholder="posicionamento" value="<?php printf($id_tecnico); ?>">
										   
										  	<?php
										     $sql = "SELECT  id, status FROM os_posicionamento  ";
                                            if ($stmt = $conn->prepare($sql)) {
	                                    	$stmt->execute();
                                            $stmt->bind_result($id,$status);
                                             while ($stmt->fetch()) {
                                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($status);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                            </div>
                            </div>
                            <script>
                                    $(document).ready(function() {
                                    $('#usuario').select2();
                                      });
                            </script>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form> -->
              <!-- Posicionamento -->
              
              <!-- Posicionamento -->
         <!--      <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Posicionamento</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                           <form action="backend/os-progress-upgrade-position-backend.php?os=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>
                             <div class="item form-group">
                             <label class="col-form-label col-md-3 col-sm-3 label-align" for="posicionamento" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Posicionamento ">Posicionamento <span class="required" >*</span>
                             </label>
                             <div class="col-md-6 col-sm-6 ">
                         	 <select type="text" class="form-control has-feedback-right" name="posicionamento" id="posicionamento"  placeholder="posicionamento" value="<?php printf($id_tecnico); ?>">
										   
										  	<?php
										     $sql = "SELECT  id, status FROM os_posicionamento  ";
                                            if ($stmt = $conn->prepare($sql)) {
	                                    	$stmt->execute();
                                            $stmt->bind_result($id,$status);
                                             while ($stmt->fetch()) {
                                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($status);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                            </div>
                            </div>
                            <script>
                                    $(document).ready(function() {
                                    $('#usuario').select2();
                                      });
                            </script>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>  -->
              <!-- Posicionamento -->  
         
               
            
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ensaio </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
           
           
                        
           <?php 
           $result_cat_post  = "SELECT  id, instituicao FROM instituicao" ;
            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
 
           $query_text=0;
           $query_table=0;
           $block = 1;
           $query="SELECT  id FROM calibration_parameter_dados  WHERE id_equipamento_grupo = '$id_equipamento_grupo '";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id);
   while ($stmt->fetch()) {
       $id=$id;
   }
}

$query="SELECT   calibration_report.id_calibration_parameter_dados_parameter,calibration_report.id_calibration_parameter_tools_dados,calibration_report.id_calibration_parameter_tools,calibration_report.id_calibration,calibration_parameter_dados_parameter.erro,calibration_report.status,calibration_parameter_dados_parameter.d_1,calibration_parameter_dados_parameter.d_2,calibration_parameter_dados_parameter.id,calibration_parameter_tools_dados.nome,calibration_parameter_tools_dados.unidade,calibration_parameter_tools_dados.i_h,calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo,calibration_report.id, IFNULL(calibration_report.ve,0),IFNULL(calibration_report.vl_1,0),IFNULL(calibration_report.vl_2,0),IFNULL(calibration_report.vl_3,0),IFNULL(calibration_report.vl_avg,0),IFNULL(calibration_report.erro,0),IFNULL(calibration_report.dp,0),IFNULL(calibration_report.ie,0),IFNULL(calibration_report.ia,0),IFNULL(calibration_report.k,0),IFNULL(calibration_report.ib,0),IFNULL(calibration_report.ic,0),calibration_parameter_dados_parameter.nome, calibration_parameter_dados_parameter.unidade FROM calibration_parameter_dados_parameter INNER JOIN calibration_report on calibration_report.id_calibration_parameter_dados_parameter = calibration_parameter_dados_parameter.id  AND IFNULL(calibration_report.id_calibration = '$codigoget',true) INNER JOIN calibration_parameter_tools on calibration_parameter_tools.id = calibration_report.id_calibration_parameter_tools INNER JOIN calibration_parameter_tools_dados on calibration_parameter_tools_dados.id = calibration_report.id_calibration_parameter_tools_dados WHERE calibration_parameter_dados_parameter.id_calibration_parameter_dados = '$id '  ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_calibration_parameter_dados_parameter,$id_calibration_parameter_tools_dados,$id_calibration_parameter_tools,$id_calibration,$erro_ac,$status,$d_1,$d_2,$calibration_parameter_dados_parameter,$tools_dados_nome,$tools_dados_unidade,$tools_dados_ih,$tools_nome,$tools_fabricante,$tools_modelo,$id,$ve, $vl_1, $vl_2, $vl_3, $vl_avg, $erro, $dp, $ie,$ia,$k,$ib,$ic,$nome,$unidade);
  
?>
 <?php  while ($stmt->fetch()) {
 $calibration_parameter_dados_parameter=$calibration_parameter_dados_parameter;
 
 ?>

<?php if($calibration_parameter_dados_parameter  !== $query_text){ 
$query_text=$calibration_parameter_dados_parameter;
?>
 </tbody>
                    </table>
  <td><h2>  <?php printf($nome); ?></td> <td><?php printf($unidade); ?> </h2></td><br>
  <td>Analisador &amp; Simulador :</td>  <td><?php printf($tools_nome); ?> <?php printf($tools_fabricante); ?> <?php printf($tools_modelo); ?> </td> 
<!--<select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">

  <?php
 
 while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
					}
     ?>

	                                     	</select>  
									  	
								 <script>
                                    $(document).ready(function() {
                                    $('#instituicao').select2();
                                      });
                                 </script> -->
  <br>
  <td>Parametro:</td> <td> <?php printf($tools_dados_nome); ?> <?php printf($tools_dados_unidade); ?></td><br>
  <td>Incerteza Herdada:</td> <td> <?php printf($tools_dados_ih); ?></td><br>
   <td>Erro Aceitável:</td> <td> <?php if($erro_ac=="0"){printf("<input name='erro_vl' value='$d_1'>"); }?></td><br>
  <td>Erro Aceitável (%):</td> <td> <?php if($erro_ac=="1"){printf("<input name='erro_vl' value='$d_2'>"); }?></td><br>
  <?php //} ?>
  
     <?php //if((($calibration_parameter_dados_parameter  ==! $query_table and  //$block == 1))){ 
  //$query_table=$calibration_parameter_dados_parameter;
 //$block = 0;
 
 
?> 
 <a class="btn btn-app"  href="backend/calibration-report-edit-new-add-backend.php?id_calibration=<?php printf($id_calibration); ?>&id_calibration_parameter_tools=<?php printf($id_calibration_parameter_tools); ?>&$id_calibration_parameter_tools_dados=<?php printf($id_calibration_parameter_tools_dados); ?>&id_calibration_parameter_dados_parameter=<?php printf($id_calibration_parameter_dados_parameter); ?>">
                    <i class="glyphicon glyphicon-plus"></i> Linha
                  </a>
              <!--   <a class="btn btn-app"  href="#analisador=<?php printf($tools_dados_nome); ?>">
                    <i class="glyphicon glyphicon-floppy-saved"></i> Salvar
                  </a>-->
                   <a class="btn btn-app"  href="" value="Refresh Page" onClick="window.location.href=window.location.href" >
                    <i class="fa fa-calculator"></i> Calcular
                  </a>  
      
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                          
                        <tr>
                          <th>#</th>
                         
                          <th>VE</th>
                          <th>VL-1</th>
                          <th>VL-2</th>
                          <th>VL-3</th>
                          <th>VL-AVG</th>
                          <th>Erro</th>
                          <th>DP</th>
                          <th>IA</th>
                          <th>IB</th>
                          <th>IC</th>
                          <th>K</th>
                          <th>IE</th>
                          <th>Status</th>
                     <th>Ação</th> 
                          
                          </tr>
                      </thead>
                      
                      <tbody>
                        <?php  }  ?> 
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                           <form action="backend/calibration-report-edit-itens-backend.php?id=<?php printf($id);?>&k=<?php printf($k); ?>&d_1=<?php printf($d_1); ?>&d_2=&d_1=<?php printf($d_2); ?>&erro=<?php printf($erro_ac); ?>&tools=<?php printf($id_calibration_parameter_tools_dados); ?>" method="post" target="_blank">
                          <th ><input name="ve"value="<?php printf($ve); ?>" size="2"></th>
                          <th ><input name="vl_1"value="<?php printf($vl_1); ?>"size="2"></th>
                          <th ><input name="vl_2"value="<?php printf($vl_2); ?>"size="2"></th>
                          <th ><input name="vl_3"value="<?php printf($vl_3); ?>"size="2"></th>
                          <th ><?php printf($vl_avg); ?> </th>
                          <th ><?php printf($erro); ?> </th>
                          <th ><?php printf($dp); ?> </th>
                          <th ><?php printf($ia); ?> </th>
                          <th ><?php printf($ib); ?> </th>
                          <th ><?php printf($ic); ?> </th>
                           <th ><?php printf($k); ?> </th>
                          <th ><?php printf($ie); ?> </th>
                            <th ><?php if($status=="0"){ printf("Aprovado");}  if($status=="1"){ printf("Reprovado");}?></th>
                            <th >
                            <center>  
                               <button type="submit" class="btn btn-primary">Salvar</button>
                            </form>                             <a   class="btn btn-round " onclick="open(' calibration-report-drop-itens?id=<?php printf($id); ?>','calibration-report-drop-itens?id=<?php printf($id); ?>','status=no,Width=320,Height=285');"><i class="fa fa-trash"></i></a>
                                	
                                </center> 
                           </th> 
                        </tr>
                        
                         <?php if($calibration_parameter_dados_parameter  !== $query_text){ 
$query_text=$calibration_parameter_dados_parameter;
?>
                  
                      </tbody>
                    </table>
             <?php  }    ?>  
               <?php  $row=$row+1; }}   ?>  
           </tbody>
                    </table>
           
                
                </div>
              </div>      
           
              
              
            <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="calibration-report">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
             
               
            
                  
                </div>
              </div>
                  
                
	       
	        
                  
                  
                </div>
              </div>
              
  	 
