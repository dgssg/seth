  <?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../

$query = "SELECT os.id_cancelamento,os.id_servico,os.id_category,os.restrictions,os.id_fornecedor,os.id_prioridade,os.id_observacao, os.id_chamado,os.id_tecnico,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN usuario on usuario.id = os.id_usuario INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area INNER JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento INNER JOIN os_carimbo ON os_carimbo.id = os.id_carimbo INNER JOIN os_workflow on os_workflow.id = os.id_workflow INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia WHERE os.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id_cancelamento,$id_service,$id_category,$restrictions,$id_fornecedor,$id_prioridade,$id_observacao,$id_chamado,$id_tecnico,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);
 


while ($stmt->fetch()) {

}
}
?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ordem de Serviço</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Descrição <small>da Solicitação</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                    
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os">OS <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="os" name="os" value="<?php printf($id_os); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="solicitante">Solicitante<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="solicitante" name="solicitante" required="required" class="form-control"  value="<?php printf($id_usuario_nome); ?> <?php printf($id_usuario_sobrenome); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="setor" class="form-control" type="text" name="setor"  value="<?php printf($id_area); ?> <?php printf($id_localizacao); ?>" readonly="readonly">
                        </div>
                      </div>
                     
                     
                     
                      <div class="ln_solid"></div>
                    
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento">Equipamento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="equipamento" name="equipamento" value="<?php printf($equipamento_nome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="modelo">Modelo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="modelo" name="modelo" required="required" class="form-control"  value="<?php printf($equipamento_modelo); ?> " readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($equipamento_codigo); ?> " readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Numero de Serie</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="numeroserie" class="form-control" type="text" name="numeroserie"  value="<?php printf($equipamento_numeroserie); ?> " readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Patrimonio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="patromonio" class="form-control" type="text" name="patromonio"  value="<?php printf($equipamento_patrimonio); ?> " readonly="readonly">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Solicitação</label>
                <div class="col-md-6 col-sm-6 ">
                          <textarea id="solicitacao" class="form-control" name="solicitacao" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($id_solicitacao); ?>" value="<?php printf($id_solicitacao); ?>" readonly="readonly"></textarea>

                </div>
              </div>
              <div class="ln_solid"></div>
              <label for="message">Restrição:</label>
             <textarea id="message" required="required" class="form-control" name="restrictions"  placeholder="<?php printf($restrictions); ?>"readonly="readonly"></textarea>
 
             <div class="ln_solid"></div>
             <div class="item form-group">
               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da Solicitação</label>
               <div class="col-md-6 col-sm-6 ">
                 <input id="reg_date" class="form-control" type="text" name="reg_date"  value="<?php printf($reg_date); ?> " readonly="readonly">
               </div>
             </div>
 
                      
                    </form>
                  </div>
                </div>
              </div>
            </div>
         
            
             <!-- page content -->
       
           

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Anexo <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="dropzone/os/<?php printf($id_anexo); ?>" alt="image" />
                            <div class="mask">
                              <p>Anexo</p>
                              <div class="tools tools-bottom">
                                <a href="dropzone/os/<?php printf($id_anexo); ?>" download><i class="fa fa-download"></i></a>
                                
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>Anexo ordem de serviço</p>
                          </div>
                        </div>
                      </div>
                     
                    
                   
                  

                   
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
            
              <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cancelamento <small>da Solicitação</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="backend/os-opened-close-info-backend.php?os=<?php printf($codigoget);?>" method="post">

                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Colaborador <span class="required" >*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                           	<select type="text" class="form-control has-feedback-right" name="colaborador" id="colaborador"  placeholder="Colaborador" value="<?php printf($id_tecnico); ?>">
										 <option value=" ">Selecione um colaborador	</option>
										   	<?php
										  	
										  
										  	
										   $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE trash != 0 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$primeironome_query,$ultimonome_query);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($primeironome_query);?>	    <?php printf($ultimonome_query);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE trash = 1 and id_unidade like '%$id_instituicao%'";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$primeironome,$ultimonome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                      
     
                     
									
									
										
									
								     
								 <script>
                                    $(document).ready(function() {
                                    $('#colaborador').select2();
                                      });
                                 </script>
                      
                     
                     
            <!--      <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                         <button id="compose" class="btn btn-sm btn-success btn-block" class="form-control "type="button">Justificativa</button>
                         </center>
                        </div>
                      </div> -->
                      
                      

                      <div class="ln_solid"></div>
              <label for="message">Justificativa:</label>
             <textarea id="id_cancelamento" required="required" class="form-control" name="id_cancelamento"  placeholder="<?php printf($id_cancelamento); ?>" ><?php printf($id_cancelamento); ?></textarea>
 
             <div class="ln_solid"></div>
                         
                  
                  </div>
                </div>
              </div>
            </div>
         
            
            
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="os-opened">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
             
               
                  <button class="btn btn-app"  type="submit" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </button>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  
                </div>
              </div>

            
  </form>
            
        </div>
            </div>
        <!-- /page content -->
<!-- compose -->
    <div class="compose col-md-6  ">
      <div class="compose-header">
        Justificativa
        <button type="button" class="close compose-close">
          <span>×</span>
        </button>
      </div>
 <form  action="backend/os-opened-close-comment-backend.php?os=<?php printf($codigoget);?>" method="post">
      <div class="compose-body">
        <div id="alerts"></div>

        
        <input size="90" type="text" id="editor" name="editor" class="editor-wrapper">
            
        
      </div>

      <div class="compose-footer">
        <button class="btn btn-sm btn-success" type="submit">Salvar</button>
      </div>
      </form>
    </div>
    <!-- /compose -->

