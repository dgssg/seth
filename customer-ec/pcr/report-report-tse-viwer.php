<?php


include_once("database/database.php");
  $query = "SELECT assistence FROM tools";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($assistence);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
date_default_timezone_set('America/Sao_Paulo');
$codigoget = ($_GET["query"]);
$print= date(DATE_RFC2822);
$html = '<table border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:80%;border-collapse:collapse;">';
$html .= '<thead>';
$html .= '<tr>';
$html .= '<th>Laudo</th>';
$html .= '<th>Equipamento</th>';
$html .= '<th>Realização</th>';
$html .= '<th>Validade</th>';
$html .= '<th>vencido</th>';
$html .= '<th>Status</th>';

$html .= '<th>Unidade</th>';


$html .= '</tr>';
$html .= '</thead>';
$html .= '<tbody>';

$result_transacoes = "$codigoget";
$resultado_trasacoes = mysqli_query($conn, $result_transacoes);
while($row_transacoes = mysqli_fetch_assoc($resultado_trasacoes)){
  $data_now=date("Y-m-d");
 $new_date=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));

if ( $new_date < $data_now ) {
 $ven = 1;

}

 if($row_transacoes['ven'] ==0) {$ven=("Não");} if($row_transacoes['ven'] ==1) {$ven=("Sim");}

   if($row_transacoes['status'] ==0) {$status=("Aprovado");} if($row_transacoes['status'] ==1) {$status=("Reprovado");}
  $html .= '<tr><td><small>'.$row_transacoes['id'] . "</small></td>";
  $html .= '<td><small>'.$row_transacoes['codigo'] . '-'.$row_transacoes['nome'] .'-'.$row_transacoes['modelo'] . '-'.$row_transacoes['fabricante'] .  "</small></td>";
  $html .= '<td><small>'.$row_transacoes['data_start'] . "</small></td>";
  $html .= '<td><small>'.$row_transacoes['val'] . "</small></td>";
  $html .= '<td><small>'.$ven. "</small></td>";
  $html .= '<td><small>'.$status. "</small></td>";

  $html .= '<td><small>'. $row_transacoes['instituicao'] . "</small></td></tr>";
}

$os = 0;
$query = "$codigoget";


if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$codigo,$nome,$modelo,$fabricante,$data_start,$val,$status,$unidade);
  while ($stmt->fetch()) {
    $os = $os + 1;
  }
  $stmt->close();
}


$html .= '</tbody>';
$html .= '</table>';


//referenciar o DomPDF com namespace
use Dompdf\Dompdf;

// include autoloader
require_once("../../framework/dompdf/autoload.inc.php");

//Criando a Instancia
$dompdf = new DOMPDF();

// Carrega seu HTML
$dompdf->load_html('
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 30px;
  text-align: center;
  font-size: 35px;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 33.33%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

.footer {
  position: fixed;
  left: 0;
  bottom: 10;
  width: 100%;

  color: black;
  text-align: center;
}



/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
  .column {
    width: 100%;
  }
}


</style>

<style>
/*-----------------------------------------------
/*    COMUM
------------------------------------------------*/
* {
  margin: 0;
  padding: 0;
  border: 0;
}

html {
  line-height: 1.15;
  /* 1 */
  -ms-text-size-adjust: 100%;
  /* 2 */
  -webkit-text-size-adjust: 100%;
  /* 2 */
}

body {
  margin: 0;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  color: #333;
}

article,
aside,
footer,
header,
nav,
section,
figure,
main {
  display: block;
}

h1 {
  font-size: 1.8em;
  margin: 0;
}

a {
  background-color: transparent;
  /* 1 */
  -webkit-text-decoration-skip: objects;
  /* 2 */
}

b,
span {
  font-weight: 500 !important;
}

small {
  font-size: 80%;
}

sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sub {
  bottom: -0.25em;
}

sup {
  top: -0.5em;
}

img {
  border-style: none;
}

.wraper {
  width: 26.7cm;
  /* height: 18cm; */
  margin: 0 auto;
}

.uppercase {
  text-transform: uppercase
}

.small-font {
  font-size: .6em
}

.weight-normal {
  font-weight: normal
}

.weight-bold {
  font-weight: bold !important;
}

.text-left {
  text-align: left;
}

.text-right {
  text-align: right;
}

.width-10-percent {
  width: 10%;
}

.width-15-percent {
  width: 15%;
}

.width-20-percent {
  width: 20%;
}

.width-25-percent {
  width: 25%;
}

.width-30-percent {
  width: 30%;
}

.width-35-percent {
  width: 35%;
}

.width-40-percent {
  width: 40%;
}

.width-45-percent {
  width: 45%;
}

.width-50-percent {
  width: 50%;
}

.width-55-percent {
  width: 55%;
}

.width-60-percent {
  width: 60%;
}

.width-65-percent {
  width: 65%;
}

.width-100-percent {
  width: 100%;
}

.base-background {
  background: #e5efea;
}

.atv-principal {
  padding-top: 10px !important;
}

@media print {
  .break-page {
    page-break-before: always;
  }
}

/*-----------------------------------------------
/*    HEADER
------------------------------------------------*/

.header-logo {
  width: 30%;
  background: #FFFFFF !important;
  border: 0 !important;
  margin: 0 !important;
}

#header-logo {
  height: 12em !important;
  border: 0 !important;
}

.header-content-block {
  color: #000;
  width: 100%;
  padding: 0 1em;
}

.header-content-block-title {
  font-size: 2em !important;
  white-space: nowrap;
}

.header-content-block-col1 {
  text-transform: uppercase;
  white-space: nowrap;
}

.header-content-block-col2 {
  text-align: right;
  border: 0 !important;
  white-space: nowrap;
}

.header table {
  width: 100%;
}

/*-----------------------------------------------
/*    MAIN
------------------------------------------------*/
.table-documento td {
  border: 0 !important;
}

.content-block {
  padding: 1em;
  margin-top: .5em;
  font-size: 12px;
  background: #f9fafa;
  border: .1em solid #bebebe
}

.content-block-title {
  top: -1em;
  position: relative;
  margin: 0 auto;
  padding: .2em 0;
  width: 33em;
  background: #6c6b6c;
  color: #fff;
  text-transform: uppercase;
  text-align: center;
  font-weight: normal;
  font-size: 17px;
}

.lista-atividades-aviso {
  margin-bottom: 2em;
  width: 100%;
  background: #e5e8e7;
}

.lista-atividades-aviso-col1 {
  width: 25%
}

.lista-atividades-aviso-col2 b {
  padding-left: 1.5em;
}

.lista-atividades {
  width: 100%;
  font-size: 1em;
}

.lista-atividades thead th {
  border-bottom: .1em solid #333
}

.lista-atividades thead th:first-child {
  border-right: .1em solid #333;
  text-align: center !important;
}

.lista-atividades thead th:last-child {
  text-align: center !important;
}

.lista-atividades thead th,
.lista-atividades tbody td {
  padding: 0 0 .5em .7em;
}

.lista-atividades thead th,
.lista-atividades tbody td {
  padding: 0 0 .5em .7em;
}

/*-----------------------------------------------
/*    FOOTER
------------------------------------------------*/
.footer {
  margin-top: 0.0em;

}

.footer table {
  width: 100%
}

.footer-info {
  width: 1300px !important;
  padding: 10px 10px;
  height: 100px !important;
  color: #000;
}

.observacoes-lista {
  padding: 10px;
  font-size: 13px;
  word-break: break-word;
}

.footer-info h5 {
  padding-bottom: 0px;
}

.observacoes-lista li {
  list-style-type: none;
  padding-right: 1em;
}

.observacoes-lista li::before {
  display: inline-block;
  padding-right: .0em;
  font-size: 10px;
  content: "•";
}

.obsevacoes-link {
  color: #fff;
  font-weight: bold
}

.qr-code {
  margin-left: 1em;
  vertical-align: top
}

.p-bottom-190 {
  padding-bottom: 8.5em !important;
}

.p-bottom-130 {
  padding-bottom: 4em !important;
}

.p-bottom-40 {
  padding-bottom: 2em !important;
}

.p-top-50 {
  padding-top: 4em !important;
}

.p-left-30 {
  padding-left: 30px;
}

.table-veiculos {
  border-collapse: collapse;
  width: 100%;
  font-size: 13px;
  margin-bottom: 45px;
}


.table-veiculos tr,
.table-veiculos td,
.table-veiculos th {
  border: solid 1px #000 !important;
}

.table-veiculos,
.table-veiculos th,
.table-veiculos td {
  word-break: break-all;
  text-align: center;
  padding: 5px;
}

.tv-title-carga {
  width: 175px;
}
img {
  border: 3;
}
.div-top {
  float: center;
  width: 900px;
  margin: auto;
}
.div-top img {
  margin-top: 10px;
}
.escola_logo {
  float: left;
}
.academus_logo {
  float: right;
}

.left-side-text {
  float: left;
  max-width: 300px;
  margin-left: 10px;
}
.left-side-text * {
  width: 100%;
  float: left;
}
.right-side-text {
  float: right;
  max-width: 300px;
  margin-right: 10px;
  text-align: right;
}
.right-side-text * {
  float: right;
  width: 100%;
}


h4 {
  color: rgb(127, 127, 127);
  font-size: 14px;
}
h5 {
  color: rgb(191, 191, 191);
  font-size: 10px;
}
.titulo-relatorio {
  text-align: center;
  font-size: 20px;
  font-family: sans-serif;
  font-weight: lighter;
}
.box-externa {
  float: center;
  border: 1px solid rgb(217, 217, 217);
  width: 900px;
  margin: auto;
}
.box-interna {
  border: 1px solid rgb(166, 166, 166);
  width: 850px;
  margin: 15px auto 0 auto;
  float: center;
  padding: 10px;
}
.box-interna:last-of-type {
  margin-bottom: 15px;
}
.box-grafico {
  width: 850px;
  margin: 15px auto 0 auto;
  float: center;
  padding: 10px;
}
body {
  font-family: open sans;
}
p {
  margin: 8px 0 8px 0;
}

.arrow-down {
  width: 0;
  height: 0;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-top: 10px solid gray;
  margin: auto;
}
.grey-box {
  background-color: gray;
  width: 400px;
  margin: auto;
}
.indicador-titulo {
  margin-bottom: 0;
  margin-top: 10px;
}
.indicador-valores {
  float: left;
  width: 800px;
  margin-left: 25px;
}
</style>
<div class="wraper">
<header class="header table-documento base-background">
<table cellspacing="0" cellpadding="0">
<tr>
<td class="header-logo" rowspan="2">
<h1>

</h1>
</td>


</tr>
</table>
</header>
<br>
<!-- Início do Cabeçalho -->
<div class="div-top">

<div class="center-side-text">
<small><h3>Sistema SETH</h3></small>

</div>
<div class="center-side">

<div class="center-side-text">


<br>

</div>
</div>
</div>
<div style="clear: both;"></div>
<hr style="border-top: dashed 1px; width: 900px; color: gray; clear: both;">
<!-- Fim do Cabeçalho -->
<!-- Início do Título -->
<br>
<center>
<h2 class="titulo-relatorio"> Teste de Segurança Elétrica </h2>

</center>
<br>
<!-- Fim do Título -->
<main>
<div class="container">

<!--./content-block-->
<br>

<br>


<br>

<br>
<section class="content-block">
<header>
<h3 class="content-block-title">T.S.E</h3>
</header>
'. $html .'
</section>
<br>
<section class="content-block">
<header>
<h3 class="content-block-title">Dados</h3>
</header>
<table class="table-documento lista-atividades">
<thead>
<tr>
<th class="width-10-percent text-left">Total:</th>

</tr>
</thead>
<tbody>
<tr class="width-percent">
<td colspan="3" class="atv-principal">
'.$os.'
</td>
</tr>

</tbody>
</table>
</section>

<br>

<br>

<br>

<!--./content-block-->
</div>
<!--/.container-->
</main>

<footer class="footer">

<table>
<tr>
<td class="footer-info base-background">


</td>

</tr>

</table>
</footer>
<div class="footer">

Copyright ©2021 MK Sistemas Biomédicos- by MK Sistemas Biomédicos. Todos os direitos reservados.

</div>
</div>



<!-- /.col -->');

//Renderizar o html
$dompdf->render();

//Exibibir a página
$dompdf->stream(
  "relatorio.pdf",
  array(
    "Attachment" => false //Para realizar o download somente alterar para true
  )
);
?>
