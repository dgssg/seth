  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Laudos de <small>Calibração</small></h3>
              </div>

           
            </div>
       <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="calibration-report">
                    <i class="fa fa-file"></i> Laudos
                  </a>
                  
                  <a class="btn btn-app"  href="calibration-report-new">
                    <i class="fa fa-plus-square"></i> Novo
                  </a>
                    <a class="btn btn-app"  href="calibration-report-new-v2">
                    <i class="fa fa-plus-square"></i> Novo v2
                </a>
                
                  <a class="btn btn-app"  href="calibration-report-single">
                    <i class="fa fa-file-o"></i> Laudos único
                  </a>
                  <a class="btn btn-app"  href="calibration-report-new-single-wizard">
                    <i class="fa fa-plus-square-o"></i> Novo único
                  </a>
                <a class="btn btn-app"  href="calibration-report-new-single-wizard-v2">
                    <i class="fa fa-plus-square-o"></i> Novo único v2
                </a>
               
                  
                </div>
              </div>
              
              
              
              
        
              
                <div class="clearfix"></div>
                <div class="x_panel">
                <div class="x_title">
                  <h2>Status</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

             <div class="alert alert-success alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                <strong style="color: green;">AP</strong> Aprovado <br>
                <strong style="color: red;">RP</strong> Reprovado <br>
                <strong style="color: blue;">ER</strong> Erro por falta de parametrização<br>

                    <strong>Exclusão automática</strong> 1- Para valores de VE nulo ou 0. <br>
                    <strong>Exclusão automática</strong> 2- Para não seleção de equipamento com parâmetro
                
                  </div>


                  
                </div>
              </div>







                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Cadastro de Laudos </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                <?php include 'frontend/calibration-report-new-wizard-register-frontend.php';?>
                  
                </div>
              </div> 
	       
	        
                  
                  
                </div>
              </div>
              
              
              <?php 
                
                
                $query = "SELECT menu FROM tools";
                
                
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($menu);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                  }
                }
                if($menu == "0"){ ?>
              <script type="text/javascript">
                window.onload = function()
                {
                  document.getElementById("menu_toggle").click();
                }
              </script>
              <?php  } ?>