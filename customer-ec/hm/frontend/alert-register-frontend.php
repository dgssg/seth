
<?php


// Create connection
include("database/database.php");// remover ../


?>    
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
   
   <div class="right_col" role="main">
       
       <div class="">
           
         <div class="page-title">
           <div class="title_left">
             <h3>Eventos Sentinela</h3>
           </div>

           <div class="title_right">
             <div class="col-md-5 col-sm-5  form-group pull-right top_search">
               <div class="input-group">

                 <span class="input-group-btn">

                 </span>
               </div>
             </div>
           </div>
         </div>
         <div class="clearfix"></div>
         <div class="x_panel">
             <div class="x_title">
               <h2>Menu</h2>
               <ul class="nav navbar-right panel_toolbox">
                 <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                 </li>
                 <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                       class="fa fa-wrench"></i></a>
                   <ul class="dropdown-menu" role="menu">
                     <li><a href="#">Settings 1</a>
                     </li>
                     <li><a href="#">Settings 2</a>
                     </li>
                   </ul>
                 </li>
                 <li><a class="close-link"><i class="fa fa-close"></i></a>
                 </li>
               </ul>
               <div class="clearfix"></div>
             </div>
             <div class="x_content">

                <a class="btn btn-app"  href="alert">
                 <i class="glyphicon glyphicon-list-alt"></i> Consulta
               </a>
                 <a class="btn btn-app"  href="alert-register">
                 <i class="glyphicon glyphicon-plus"></i> Cadastro
               </a>
               

              

             </div>
           </div>






         <div class="clearfix"></div>
    
      <div class="x_panel">
             <div class="x_title">
               <h2>Sentinela</h2>
               <ul class="nav navbar-right panel_toolbox">
                 <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                 </li>
                 <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                       class="fa fa-wrench"></i></a>
                   <ul class="dropdown-menu" role="menu">
                     <li><a href="#">Settings 1</a>
                     </li>
                     <li><a href="#">Settings 2</a>
                     </li>
                   </ul>
                 </li>
                 <li><a class="close-link"><i class="fa fa-close"></i></a>
                 </li>
               </ul>
               <div class="clearfix"></div>
             </div>
             <div class="x_content">
               
       <!--   <div class="alert alert-primary alert-dismissible " role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                 </button>
                 <strong>Area de Alertas!</strong> Todo alteração é integrada.
               </div> -->
                <div class="alert alert-primary alert-dismissible " role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                 </button>
                 <strong>MC</strong> Manutenção Corretiva.<br>
                 <strong>MP</strong> Manutenção Preventiva.<br>
                 <strong>Ee</strong> Evento Externo.
               </div>
             
              
               
             </div>
           </div>
    
     
         
          <!-- page content -->
    
        
          <div class="x_panel">
                <div class="x_title">
                  <h2>Tecnovigilancia</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                       <form  action="backend/technological-surveillance-upgrade-single-backend.php" method="post">
                       <div class="form-group row">
    <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Empresa <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="unidade" id="unidade"   placeholder="Unidade">
  <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
        <?php



        $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#unidade').select2();
  });
  </script>



  

  <div class="form-group row">
      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Cliente <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
        <option value="">Selecione uma Opção</option>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#setor').select2();
  });
  </script>

<div class="form-group row">
      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Localização <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Area <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
        <option value="">Selecione uma Opção</option>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#area').select2();
  });
  </script>



  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Equipamento</label>
    <div class="col-md-6 col-sm-6 ">
      <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
        <option value="	">Selecione o equipamento</option>
        <?php



        $query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao";

        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
        if ($stmt = $conn->prepare($query)) {
          $stmt->execute();
          $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?>  - <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
            <?php
            // tira o resultado da busca da memória
          }

        }
        $stmt->close();

        ?>
      </select>
      <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#equipamento').select2();
  });
  </script> 
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Descrição</label>
                        <div class="col-md-6 col-sm-6 ">
                         <textarea  rows="5" cols="5" id="descricao"  class="form-control" name="descricao" data-parsley-trigger="keyup"  
                            data-parsley-validation-threshold="2" placeholder="<?php printf($descricao); ?>"  value="<?php printf($descricao); ?>"><?php printf($descricao); ?></textarea>
                        </div>
                      </div>
                      
                   <div class="ln_solid"></div>
                   
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
                        <div class="col-md-6 col-sm-6 ">
                         <div class="">
                            <label>
                              <input name="status"type="checkbox" class="js-switch" > 
                            </label>
                          </div>
                        </div>
                      </div>
                   
                          <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="technological_surveillance_list" class="docs-tooltip" data-toggle="tooltip" title="Selecione um Risco ">Risco <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="technological_surveillance_list" id="technological_surveillance_list"   >
							
										  	<?php
										  	
										  
										  	
										    $sql = "SELECT  id, codigo,nome FROM technological_surveillance_list ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$codigo,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	" <?php if($id==$risco){printf("selected");} ?>><?php printf($codigo);?> - <?php printf($nome);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                      
     
                     
									
									
										
									
								     
								 <script>
                                    $(document).ready(function() {
                                    $('#technological_surveillance_list').select2();
                                      });
                                 </script>
      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Inicio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_inicio" type="date" name="data_inicio"  value="<?php printf($data_inicio);?>"  class="form-control">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Fim</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_fim"  type="date" name="data_fim"   value="<?php printf($data_fim);?>" class="form-control">
                        </div>
                      </div>
                       <div class="ln_solid"></div>
                        <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ação</label>
                        <div class="col-md-6 col-sm-6 ">
                         <textarea  rows="5" cols="5" id="acao"  class="form-control" name="acao" data-parsley-trigger="keyup"  
                            data-parsley-validation-threshold="2" placeholder="<?php printf($acao); ?>"  value="<?php printf($acao); ?>"><?php printf($acao); ?></textarea>
                        </div>
                      </div>
                   <div class="ln_solid"></div>
                    <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observação</label>
                        <div class="col-md-6 col-sm-6 ">
                          <textarea  rows="5" cols="5" id="obs"  class="form-control" name="obs" data-parsley-trigger="keyup"  
                            data-parsley-validation-threshold="2" placeholder="<?php printf($obervacao); ?>"  value="<?php printf($obervacao); ?>"><?php printf($obervacao); ?></textarea>
                        </div>
                      </div>
                  
    
                   <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>
     
     
                                  </form>
                  
                </div>
              </div>
              
         
    <div class="clearfix"></div>

         
        <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
         

          <div class="clearfix"></div>
         
         
          
      
         
         
        

         

         
     </div>
         </div>
     <!-- /page content -->
    
 <!-- /compose -->
 
 <script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>
    								<script>
function clean() {
  $('#unidade').select2().load('reset_unidade.php');
  $('#area').select2().load('reset_area.php');
  $('#setor').select2().load('reset_setor.php');
  $('#equipamento').select2().load('reset_equipamento.php');


}

</script>
<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.FROMCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>  