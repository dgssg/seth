  <?php
$codigoget = ($_GET["id"]);

// Create connection

include("database/database.php");
$query_1="SELECT equipamento_grupo.nome,equipamento_grupo.yr,anvisa_class.nome, COUNT(equipamento.id) FROM equipamento_grupo LEFT JOIN anvisa_class ON equipamento_grupo.id_class = anvisa_class.id LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id where equipamento.trash != 0 and equipamento.ativo = 0 and equipamento.baixa != 0 group by equipamento_grupo.id";
$query_2="SELECT equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo,equipamento_familia.endoflife,equipamento_familia.aquisicion,equipamento_familia.preventive, COUNT(equipamento.id) FROM equipamento_grupo LEFT JOIN anvisa_class ON equipamento_grupo.id_class = anvisa_class.id LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id where equipamento.trash != 0 and equipamento.ativo = 0 and equipamento.baixa != 0 group by equipamento_familia.id";
$query_3="SELECT equipamento.codigo,equipamento.serie,equipamento.patrimonio,equipamento.data_fab,MAX(obsolescence.score),MAX(obsolescence_rooi.rooi),MAX(obsolescence_year.yr) FROM equipamento_grupo LEFT JOIN anvisa_class ON equipamento_grupo.id_class = anvisa_class.id LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id LEFT JOIN obsolescence ON obsolescence.id_equipamento = equipamento.id LEFT JOIN obsolescence_rooi ON obsolescence_rooi.id = obsolescence.id_rooi LEFT JOIN obsolescence_year ON obsolescence_year.id = obsolescence.id_year where equipamento.trash != 0 and equipamento.ativo = 0 and equipamento.baixa != 0 group by obsolescence.id_equipamento ";

$query=" SELECT id,nome,ano,bi,upgrade,reg_date FROM obsolescence_bi where id = $codigoget ";
      if ($stmt = $conn->prepare($query)) {
         $stmt->execute();
         $stmt->bind_result($id,$nome,$yr,$bi,$upgrade,$reg_date );
         while ($stmt->fetch()) {
            //printf("%s, %s\n", $solicitante, $equipamento);
            //  }
         }
      }
?>
<style>
   /* Estilos para os modais */
   .modal {
      display: none;
      position: fixed;
      z-index: 1;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      overflow: auto;
      background-color: rgba(0, 0, 0, 0.4);
   }
   
   .modal-content {
      background-color: #fefefe;
      margin: 15% auto;
      padding: 20px;
      border: 1px solid #888;
      width: 80%;
   }
   
   /* Estilos para as tabelas */
   table {
      border-collapse: collapse;
      width: 100%;
   }
   
   table, th, td {
      border: 1px solid black;
      padding: 8px;
   }
   
   th {
      background-color: #f2f2f2;
   }
</style>
<link rel="stylesheet" href="https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.css">
    <style>
        .main-container {
            width: 795px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Planejamento Estrategico</h3>
              </div>


            </div>
   

            <div class="x_panel">
               <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                           class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                           <li><a href="#">Settings 1</a>
                           </li>
                           <li><a href="#">Settings 2</a>
                           </li>
                        </ul>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  
                                  
                  <a class="btn btn-app"  href="obsolescence-prevision-provision">
                     <i class="glyphicon  glyphicon-list-alt"></i> Consulta
                  </a>
                  
                  <a class="btn btn-app"  href="obsolescence-prevision-provision-new">
                     <i class="glyphicon glyphicon-plus"></i> Registro
                     
                  </a>
                  
                                  
                  
                  
               </div>
            </div>


                <div class="x_panel">
                       <div class="x_title">
                         <h2>Editar Planejamento Estrategico</h2>
                         <ul class="nav navbar-right panel_toolbox">
                           <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                           </li>
                           <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                 class="fa fa-wrench"></i></a>
                             <ul class="dropdown-menu" role="menu">
                               <li><a href="#">Settings 1</a>
                               </li>
                               <li><a href="#">Settings 2</a>
                               </li>
                             </ul>
                           </li>
                           <li><a class="close-link"><i class="fa fa-close"></i></a>
                           </li>
                         </ul>
                         <div class="clearfix"></div>
                       </div>
                       <div class="x_content">

                           <form  action="backend/obsolescence-prevision-provision-edit-backend.php" method="post">
                              


                 <!-- PAGE CONTENT BEGINS -->
<input id="codigoget" class="form-control" name="codigoget" type="hidden" value="<?php printf($id); ?>"  >
                           
                           <div class="item form-group">
                              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                              <div class="col-md-6 col-sm-6 ">
                                 <input id="nome" class="form-control" name="nome" type="text" value="<?php printf($nome); ?>"  >
                              </div>
                           </div>
                           <div class="item form-group">
                              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                              <div class="col-md-6 col-sm-6 ">
                                 <input  id="yr" class="form-control" name="yr" type="number" value="<?php printf($yr); ?>"  >
                              </div>
                           </div>	
                              
                              <div class="item form-group">
                                 <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                                 <div class="col-md-6 col-sm-6 ">
                                    <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                                 </div>
                              </div>
                              <div class="item form-group">
                                 <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                                 <div class="col-md-6 col-sm-6 ">
                                    <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                                 </div>
                              </div>                              
                              <div class="ln_solid"></div>
                              <div class="ln_solid"></div>
                              
                              <textarea name="editor" id="editor"  placeholder="" value="" rows="100" cols="800"><?php echo htmlspecialchars_decode($bi);?>
                                 
                                 
                                 
                                 
                              </textarea>
                              <script type="importmap">
                                 {
                                    "imports": {
                                       "ckeditor5": "https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.js",
                                       "ckeditor5/": "https://cdn.ckeditor.com/ckeditor5/42.0.0/"
                                    }
                                 }
                              </script>
                              <script type="module">
                                 import {
                                    ClassicEditor,
                                    Essentials,
                                    Paragraph,
                                    Bold,
                                    Italic,
                                    Font,
                                    Table,
                                    TableToolbar,
                                    PasteFromOffice,
                                    Image,
                                    ImageToolbar,
                                    ImageCaption,
                                    ImageStyle,
                                    ImageResize,
                                    ImageUpload,
                                    ImageInsert,
                                    List,
                                    Alignment,
                                    Heading,
                                    FontSize,
                                    WordCount
                                    
                                    
                                 } from 'ckeditor5';
                                 
                                 ClassicEditor
                                 .create(document.querySelector('#editor'), {
                                    plugins: [
                                       Essentials,
                                       Paragraph,
                                       Bold,
                                       Italic,
                                       Font,
                                       Table,
                                       TableToolbar,
                                       PasteFromOffice,
                                       Image,
                                       ImageToolbar,
                                       ImageCaption,
                                       ImageStyle,
                                       ImageResize,
                                       ImageUpload,
                                       ImageInsert,
                                       List,
                                       Alignment,
                                       Heading,
                                       FontSize,
                                       WordCount
                                       
                                    ],
                                    toolbar: [
                                       'undo', 'redo', '|',
                                       'heading', '|',
                                       'bold', 'italic', '|',
                                       'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', '|',
                                       'alignment', '|',
                                       'numberedList', 'bulletedList', '|',
                                       'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
                                       'imageUpload', 'imageInsert', '|', // Adicionando o botão ImageInsert na toolbar
                                       'outdent', 'indent'
                                    ],
                                    fontSize: {
                                       options: [
                                          'tiny',
                                          'small',
                                          'default',
                                          'big',
                                          'huge',
                                          '10',
                                          '12',
                                          '14',
                                          '16',
                                          '18',
                                          '20',
                                          '22',
                                          '24',
                                          '26',
                                          '28',
                                          '36',
                                          '48',
                                          '72'
                                       ],
                                       
                                    }, 
                                    image: {
                                       toolbar: [
                                          'imageTextAlternative', '|',
                                          'imageStyle:full', 'imageStyle:side', '|',
                                          'imageResize'
                                       ]
                                    },
                                    table: {
                                       contentToolbar: [
                                          'tableColumn', 'tableRow', 'mergeTableCells'
                                       ]
                                    }
                                 })
                                 .then(editor => {
                                    window.editor = editor;
                                 })
                                 .catch(error => {
                                    console.error(error);
                                 });
                              </script>
                              <!-- A friendly reminder to run on a server, remove this during the integration. -->
                              <script>
                                 window.onload = function() {
                                    if (window.location.protocol === "file:") {
                                       alert("This sample requires an HTTP server. Please serve this file with a web server.");
                                    }
                                 };
                              </script>
                              
                              <script>
                                 // Replace the <textarea id="editor1"> with a CKEditor
                                 // instance, using default configuration.
                                 //	CKEDITOR.replace( 'editor1' );
                              </script>

                              
                              
                              
                              
                              
                              <div class="ln_solid"></div>
                              
                              <input type="submit" class="btn btn-primary" onclick="new PNotify({
                                 title: 'Registrado',
                                 text: 'Informações registrada!',
                                 type: 'success',
                                 styling: 'bootstrap3'
                              });" />
                              
                           </form>
                           
                           <!-- PAGE CONTENT ENDS -->




                       </div>
                     </div>



                    <!-- page content -->
















            
            
            <!-- /page content -->
            
            <?php
               // Coloque suas consultas SQL aqui
               $query_1 = "SELECT equipamento_grupo.nome AS 'grupo', equipamento_grupo.yr, anvisa_class.nome, COUNT(equipamento.id) FROM equipamento_grupo LEFT JOIN anvisa_class ON equipamento_grupo.id_class = anvisa_class.id LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id where equipamento.trash != 0 and equipamento.ativo = 0 and equipamento.baixa != 0 group by equipamento_grupo.id";
               $query_2 = "SELECT equipamento_familia.nome, equipamento_familia.fabricante, equipamento_familia.modelo, equipamento_familia.endoflife, equipamento_familia.aquisicion, equipamento_familia.preventive, COUNT(equipamento.id) FROM equipamento_grupo LEFT JOIN anvisa_class ON equipamento_grupo.id_class = anvisa_class.id LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id where equipamento.trash != 0 and equipamento.ativo = 0 and equipamento.baixa != 0 group by equipamento_familia.id";
               $query_3 = "SELECT equipamento.codigo, equipamento.serie, equipamento.patrimonio, equipamento.data_fab, MAX(obsolescence.score), MAX(obsolescence_rooi.rooi), MAX(obsolescence_year.yr) FROM equipamento_grupo LEFT JOIN anvisa_class ON equipamento_grupo.id_class = anvisa_class.id LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id LEFT JOIN obsolescence ON obsolescence.id_equipamento = equipamento.id LEFT JOIN obsolescence_rooi ON obsolescence_rooi.id = obsolescence.id_rooi LEFT JOIN obsolescence_year ON obsolescence_year.id = obsolescence.id_year where equipamento.trash != 0 and equipamento.ativo = 0 and equipamento.baixa != 0 group by obsolescence.id_equipamento ";
               $query_4 = "SELECT equipamento.codigo, equipamento.serie, equipamento.patrimonio,equipament_disable.date_disable,equipament_disable_status.nome,equipament_disable.des  FROM equipamento_grupo LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id LEFT JOIN  equipament_disable ON equipament_disable.id_equipamento = equipamento.id LEFT JOIN equipament_disable_status ON equipament_disable_status.id = equipament_disable.id_disable where  equipamento.baixa = 0 group by equipament_disable.id_equipamento order by equipament_disable.date_disable DESC";
               $query_5 = "SELECT 
               equipamento.codigo, 
               equipamento.serie, 
               equipamento.patrimonio,
               purchases_itens.qtd,
               REPLACE(REPLACE(purchases_itens.vlr, '.', ''), ',', '.') AS vlr_decimal,
               purchases_itens.qtd * REPLACE(REPLACE(purchases_itens.vlr, '.', ''), ',', '.') AS total_valor
               FROM 
               equipamento_grupo 
               LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id 
               LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id 
               LEFT JOIN purchases_itens ON purchases_itens.id_equipamento = equipamento.id 
               WHERE  
               equipamento.trash != 0 
               AND equipamento.ativo = 0 
               AND equipamento.baixa != 0 
               GROUP BY 
               purchases_itens.id_equipamento
";
               $query_6="SELECT 
               equipamento.codigo, 
               equipamento.serie, 
               equipamento.patrimonio,
               count(os.id) AS 'os'
               
               FROM 
               equipamento_grupo 
               LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id 
               LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id 
               LEFT JOIN os ON os.id_equipamento = equipamento.id 
               WHERE  
               equipamento.trash != 0 
               AND equipamento.ativo = 0 
               AND equipamento.baixa != 0 
               
               GROUP BY 
               equipamento.id
               HAVING
               count(os.id) > 0;";
               
               // Executar consultas SQL e montar as tabelas correspondentes
               $result_1 = mysqli_query($conn, $query_1);
               $result_2 = mysqli_query($conn, $query_2);
               $result_3 = mysqli_query($conn, $query_3);
               $result_4 = mysqli_query($conn, $query_4);
               $result_5 = mysqli_query($conn, $query_5);
               $result_6 = mysqli_query($conn, $query_6);
               
               
               
               
               
            ?>
            
            
            <!-- /compose -->
            <!-- Modal para a Tabela Grupo -->
            <div id="modal1" class="modal">
               <div class="modal-content">
                  <span class="close" onclick="closeModal('modal1')">&times;</span>
                  <?php
                     echo "<h2>Tabela Grupo</h2>";
                     echo "<table id='table1' class='table table-striped table-bordered dt-responsive nowrap' style='width:100%'>";
                     echo "<tr><th>Nome</th><th>YR</th><th>Anvisa</th><th>Count</th></tr>";
                     while ($row = mysqli_fetch_array($result_1)) {
                        echo "<tr><td>" . $row['grupo'] . "</td><td>" . $row['yr'] . "</td><td>" . $row['nome'] . "</td><td>" . $row['COUNT(equipamento.id)'] . "</td></tr>";
                     }
                     echo "</table>";
                     
                     
                  ?>
                  <button onclick="copyTable('table1')">Copiar Tabela Grupo</button>
               </div>
            </div>
            
            <!-- Modal para a Tabela Familia -->
            <div id="modal2" class="modal">
               <div class="modal-content">
                  <span class="close" onclick="closeModal('modal2')">&times;</span>
                  <?php
                     echo "<h2>Tabela Familia</h2>";
                     echo "<table id='table2' class='table table-striped table-bordered dt-responsive nowrap' style='width:100%'>";
                     echo "<tr><th>Nome</th><th>Fabricante</th><th>Modelo</th><th>End of Life</th><th>Aquisição</th><th>Preventive</th><th>Count</th></tr>";
                     while ($row = mysqli_fetch_array($result_2)) {
                        echo "<tr><td>" . $row['nome'] . "</td><td>" . $row['fabricante'] . "</td><td>" . $row['modelo'] . "</td><td>" . $row['endoflife'] . "</td><td>" . $row['aquisicion'] . "</td><td>" . $row['preventive'] . "</td><td>" . $row['COUNT(equipamento.id)'] . "</td></tr>";
                     }
                     echo "</table>";
                  ?>
                  <button onclick="copyTable('table2')">Copiar Tabela Familia</button>
               </div>
            </div>
            
            <!-- Modal para a Tabela Equipamento -->
            <div id="modal3" class="modal">
               <div class="modal-content">
                  <span class="close" onclick="closeModal('modal3')">&times;</span>
                  <?php
                     echo "<h2>Tabela Equipamento</h2>";
                     echo "<table id='table3' class='table table-striped table-bordered dt-responsive nowrap' style='width:100%'>";
                     echo "<tr><th>Código</th><th>Série</th><th>Patrimônio</th><th>Data de Fabricação</th><th>Score</th><th>Rooi</th><th>YR</th></tr>";
                     while ($row = mysqli_fetch_array($result_3)) {
                        echo "<tr><td>" . $row['codigo'] . "</td><td>" . $row['serie'] . "</td><td>" . $row['patrimonio'] . "</td><td>" . $row['data_fab'] . "</td><td>" . $row['MAX(obsolescence.score)'] . "</td><td>" . $row['MAX(obsolescence_rooi.rooi)'] . "</td><td>" . $row['MAX(obsolescence_year.yr)'] . "</td></tr>";
                     }
                     echo "</table>";
                     
                  ?>
                  <button onclick="copyTable('table3')">Copiar Tabela Equipamento</button>
               </div>
            </div>
            <div id="modal4" class="modal">
               <div class="modal-content">
                  <span class="close" onclick="closeModal('modal4')">&times;</span>
                  <?php
                     echo "<h2>Tabela Equipamento Baixa</h2>";
                     echo "<table id='table4' class='table table-striped table-bordered dt-responsive nowrap' style='width:100%'>";
                     echo "<tr><th>Código</th><th>Série</th><th>Patrimônio</th><th>Data de Baixa</th><th>Motivo</th><th>Descrição</th></tr>";
                     while ($row = mysqli_fetch_array($result_4)) {
                        echo "<tr><td>" . $row['codigo'] . "</td><td>" . $row['serie'] . "</td><td>" . $row['patrimonio'] . "</td><td>" . $row['date_disable'] . "</td><td>" . $row['nome'] . "</td><td>" . $row['des'] . "</td> </tr>";
                     }
                     echo "</table>";
                     
                  ?>
                  <button onclick="copyTable('table4')">Copiar Tabela Equipamento Baixa</button>
               </div>
            </div>
            <div id="modal5" class="modal">
               <div class="modal-content">
                  <span class="close" onclick="closeModal('modal5')">&times;</span>
                  <?php
                     echo "<h2>Tabela Equipamento Custo</h2>";
                     echo "<table id='table5' class='table table-striped table-bordered dt-responsive nowrap' style='width:100%'>";
                     echo "<tr><th>Código</th><th>Série</th><th>Patrimônio</th><th>Valor</th></tr>";
                     while ($row = mysqli_fetch_array($result_5)) {
                        echo "<tr><td>" . $row['codigo'] . "</td><td>" . $row['serie'] . "</td><td>" . $row['patrimonio'] . "</td><td>" . $row['total_valor'] . "</td> </tr>";
                     }
                     echo "</table>";
                     
                  ?>
                  <button onclick="copyTable('table5')">Copiar Tabela Equipamento Custo</button>
               </div>
            </div>
            <div id="modal6" class="modal">
               <div class="modal-content">
                  <span class="close" onclick="closeModal('modal6')">&times;</span>
                  <?php
                     echo "<h2>Tabela Equipamento O.S</h2>";
                     echo "<table id='table6' class='table table-striped table-bordered dt-responsive nowrap' style='width:100%'>";
                     echo "<tr><th>Código</th><th>Série</th><th>Patrimônio</th><th>O.S</th></tr>";
                     while ($row = mysqli_fetch_array($result_6)) {
                        echo "<tr><td>" . $row['codigo'] . "</td><td>" . $row['serie'] . "</td><td>" . $row['patrimonio'] . "</td><td>" . $row['os'] . "</td> </tr>";
                     }
                     echo "</table>";
                     
                  ?>
                  <button onclick="copyTable('table6')">Copiar Tabela Equipamento O.S</button>
               </div>
            </div>
            
            <!-- Botões para abrir os modais -->
            <button onclick="openModal('modal1')">Abrir Tabela Grupo</button>
            <button onclick="openModal('modal2')">Abrir Tabela Familia</button>
            <button onclick="openModal('modal3')">Abrir Tabela Equipamento</button>
            <button onclick="openModal('modal4')">Abrir Tabela Equipamento Baixa</button>
            <button onclick="openModal('modal5')">Abrir Tabela Equipamento Custo</button>
            <button onclick="openModal('modal6')">Abrir Tabela Equipamento O.S</button>
            
            <script>
               // Função para abrir um modal específico
               function openModal(modalId) {
                  document.getElementById(modalId).style.display = "block";
               }
               
               // Função para fechar um modal específico
               function closeModal(modalId) {
                  document.getElementById(modalId).style.display = "none";
               }
               
               // Função para copiar uma tabela para a área de transferência
               function copyTable(tableId) {
                  var range, selection;
                  
                  if (document.body.createTextRange) {
                     range = document.body.createTextRange();
                     range.moveToElementText(document.getElementById(tableId));
                     range.select();
                     document.execCommand("Copy");
                     alert("Tabela copiada para a área de transferência");
                  } else if (window.getSelection) {
                     selection = window.getSelection();
                     range = document.createRange();
                     range.selectNodeContents(document.getElementById(tableId));
                     selection.removeAllRanges();
                     selection.addRange(range);
                     document.execCommand("Copy");
                     alert("Tabela copiada para a área de transferência");
                  }
               }
            </script>
         </div>
         </div>

