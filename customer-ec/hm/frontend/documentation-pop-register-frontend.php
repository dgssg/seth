	<?php
include("database/database.php");

?>
	 <form  action="backend/documentation-pop-edit-register-backend.php" method="post">
	     
	     	 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Grupo Equipamento<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="Grupo Equipamento">
										    	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo WHERE id like '$id_equipamento_grupo'";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
                                            }
											$stmt->close();
											?>
										  	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo  WHERE trash = 1 ";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
                                            }
											$stmt->close();
											?>
	                                     	</select>  
 <span class="input-group-btn">
                                          
                                          
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um grupo de equipamento ">
										    </span>
										</span>										 </div>	
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>
								  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Grupo POP<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="group_pop" id="group_pop"  placeholder="Grupo POP">
									
										  	<?php
										  $sql = "SELECT  id, nome FROM documentation_pop_goup ";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$nome);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
                                            }
											$stmt->close();
											?>
	                                     	</select>  
 <span class="input-group-btn">
                                          
                                          
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">
										    </span>
										</span>										 </div>	
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#group_pop').select2();
                                      });
                                 </script>
                                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="titulo">Titulo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="titulo" name="titulo" value="<?php printf($titulo); ?>"  required="required" class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"  required="required" class="form-control ">
                        </div>
                      </div>
				
						
									
										
											<button type="reset" class="btn btn-primary"onclick="new PNotify({
																title: 'Limpado',
																text: 'Todos os Campos Limpos',
																type: 'info',
																styling: 'bootstrap3'
														});" />Limpar</button>
											<input type="submit" class="btn btn-primary" value="Salvar Informações" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" />
													
												</form>
														<div class="ln_solid"></div>