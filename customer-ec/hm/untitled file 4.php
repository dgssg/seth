tenho um sistema de gestão de engenharia clinica, esse e um exemplo do mapa do sistema em relação aos recursos


            <br />

<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>Geral</h3>
							<ul class="nav side-menu">
								<li><a><i class="fa fa-home"></i> Início <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="dashboard" title="Painel de supervisão">Dashboard</a></li>
										<!--<li><a href="dashboard-rfid" title="Painel de supervisão RFID">Dashboard RFID</a></li> -->
									 									</ul>
								</li>
								<li><a><i class="fa fa-edit"></i> Ordem de Serviço <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="os-open" title="Abrir nova ordem de serviço">Abrir OS </a></li>
										<li><a href="os-opened" title="Gerenciar ordem de serviço solicitadas">Aberto </a></li>
										<!--      <li><a href="form_validation.php">Buscar</a></li> -->
										<li><a href="os-progress" title="Gerenciar ordem de serviço em andamento">Em processo</a></li>
										<!--  <li><a href="os-concluded">Fechada</a></li> -->
										<li><a href="os-signature" title="Gerenciar ordem de serviço para assinatura eletrônica">Assinatura</a></li>
										<li><a href="os-signature-user" title="Gerenciar ordem de serviço para assinatura eletrônica do solicitante">Solicitante</a></li>
										<li><a href="os-finished" title="Gerenciar ordem de serviço concluida">Concluida</a></li>
										<li><a href="os-canceled" title="Gerenciar ordem de serviço cancelada">Canceleda</a></li>
										<li><a href="os-integration" title="Gerenciar ordem de Integração">Integração</a></li>
								
									</ul>
								</li>
								<li><a><i class="fa fa-desktop"></i> Cadastro &amp; Consulta <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<!--<li><a href="general_elements.php">Instituição</a></li>-->

										<li><a>	<?php if($assistence == "0"){  ?> Local de Atendimento <?php }else {   ?>  Localização<?php }  ?>	<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												
											
												<?php if($assistence == "0"){  ?>
												<li class="sub_menu"><a href="register-location-unit" title="Cadastrar ou gerenciar Empresa">Empresa</a>
												</li>
												<?php }else {   ?>
												<li class="sub_menu"><a href="register-location-unit" title="Cadastrar ou gerenciar Unidades">Unidade</a>
												</li>
													<?php }  ?>	
												<?php if($assistence == "0"){  ?>
												<li><a href="register-location-area" title="Cadastrar ou gerenciar Cliente">Cliente</a>
												</li>
												<?php }else {   ?>
												<li><a href="register-location-area" title="Cadastrar ou gerenciar Setores">Setor</a>
												</li>
												<?php }  ?>	
											
													<?php if($assistence == "0"){  ?>
												<li><a href="register-location-sector" title="Cadastrar ou gerenciar Area">Localização</a>
												</li>
													<?php }else {   ?>
													<li><a href="register-location-sector" title="Cadastrar ou gerenciar Area">Area</a>
													</li>
														<?php }  ?>	
											</ul>
										</li>

										<li><a>Equipamento<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-equipament-group" title="Cadastrar ou gerenciar Grupo de Equipamento">Grupo de Equipamento</a>
												</li>
												<li><a href="register-equipament-family" title="Cadastrar ou gerenciar Familia de Equipamento">Familia de Equipamento</a>
												</li>
												<li><a href="register-equipament" title="Cadastrar ou gerenciar Equipamento">Equipamento</a>
												</li>
												<li><a>Movimentação de Equipamento<span class="fa fa-chevron-down"></span></a>
													<ul class="nav child_menu">
														<li class="sub_menu"><a href="register-equipament-family-movement" title="Cadastrar ou Saída de Equipamento">Saída de Equipamento</a>
														</li>
														<li><a href="register-equipament-family-movement-list" title="Cadastrar ou gerenciar Histórico de Movimentação de Equipamento">Histórico de Movimentação de Equipamento</a>
														</li>
														<li><a href="register-equipament-form" title="Cadastrar ou gerenciar Formulario">Formulario</a>
														</li>
														<li><a href="register-equipament-location-register" title="Consulta Localização de Equipamento">Consulta de Localização</a>
														</li>
														
													</ul>
												</li>
											</ul>
										</li>

										<li><a href="register-user" title="Cadastrar ou gerenciar Colaborador">Colaborador</a></li>
										<li><a href="register-manufacture" title="Cadastrar ou gerenciar Fornecedor">Fornecedor</a></li>
										<?php if($budget_status == "0"){  ?>
 										<li><a href="register-customer" title="Cadastrar ou gerenciar Clientes Orçamento">Clientes Orçamento</a></li>
										<?php   }?>
										<li><a href="register-account" title="Cadastrar ou gerenciar Usuário">Usuário</a></li>
									
										<li><a href="calendar" title="Cadastrar ou gerenciar Datas">Calendario</a></li>
									</ul>
								</li>
							
							<li><a><i class="fa fa-table"></i> Manutenção Preventiva <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a>Preventiva<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="maintenance-preventive" title="Gerenciar Manutenção Preventiva Aberta">M.P. Aberta</a>
											</li>
											<li><a href="maintenance-preventive-before" title="Gerenciar Manutenção Preventiva Atrasada">M.P. Atrasada</a>	</li>
											<li><a href="maintenance-preventive-close" title="Gerenciar Manutenção Preventiva Fechada">M.P. Fechada</a>	</li>
											<li><a href="maintenance-preventive-print" title="Gerenciar Impressão de Manutenção Preventiva">M.P. Impressão</a>	</li>
											<li><a href="maintenance-preventive-print-register" title="Gerenciar Registro de Impressão de Manutenção Preventiva">M.P. Registro Impressão</a>	</li>
											<li><a href="maintenance-preventive-control" title="Gerenciar Manutenção Preventiva">M.P. Controle </a>	</li>
											<li><a href="maintenance-preventive-control-return" title="Gerenciar Retorno de Manutenção Preventiva">M.P. Retorno </a>	</li>
										</ul>
									</li>
									
									<?php if($assistence == "0"){  ?>
								 
									<li><a>Preventiva Cliente<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="maintenance-preventive-customer-register" title="Gerenciar Manutenção Preventiva Cliente Cadastro">M.P. Cadastro</a>
											</li>
											<li><a href="maintenance-preventive-customer-open" title="Gerenciar Manutenção Preventiva Cliente Aberta">M.P. Aberta</a>	</li>
											<li><a href="maintenance-preventive-customer-close" title="Gerenciar Manutenção Preventiva Fechada">M.P. Fechada</a>	</li>
											
											
										</ul>
									</li>
									
									<?php }  ?>	



								
									<li><a href="maintenance-procedures" title="Cadastrar ou gerenciar Procedimento">Procedimento</a></li>
									<li><a href="maintenance-routine" title="Cadastrar ou gerenciar Rotinas">Rotina</a></li>
								
								</ul>
							</li>
							<li><a><i class="fa fa-bar-chart-o"></i> Indicador &amp; Relatório <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a href="report-indicator-metas" title="Gerenciamento e visualizar Metas Indicador ">Metas</a></li>
									<li><a>Indicador<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-indicator-mc" title="Gerenciamento e visualizar Indicador Manutenção Corretiva">Manutenção Corretiva</a></li>
											<li><a href="report-indicator-mp" title="Gerenciamento e visualizar Indicador Manutenção Preventiva">Manutenção Preventiva </a>	</li>
											<li><a href="report-indicator-calibration" title="Gerenciamento e visualizar Indicador Calibração">Calibração </a>	</li>
												<li><a href="report-indicator-mc-sla" title="Gerenciamento e visualizar Indicador Prazo Atendimento de Manutenção Corretiva ">Prazo Atendimento de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-first-sla" title="Gerenciamento e visualizar Indicador Primeiro Atendimento de Manutenção Corretiva">Primeiro Atendimento de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-assessment" title="Gerenciamento e visualizar Indicador Pesquisa de Atendimento de Manutenção Corretiva">Pesquisa de Atendimento de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-prioridade" title="Gerenciamento e visualizar Indicador Prioridade de Manutenção Corretiva">Prioridade de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-service" title="Gerenciamento e visualizar Indicador Serviço de Manutenção Corretiva ">Serviço de Manutenção Corretiva  </a>	</li>
											<li><a href="report-indicator-mc-category" title="Gerenciamento e visualizar Indicador Categoria de Manutenção Corretiva ">Categoria de Manutenção Corretiva  </a>	</li>
											<li><a href="report-indicator-mp-category" title="Gerenciamento e visualizar Indicador Categoria de Manutenção Preventiva ">Categoria de Manutenção Preventiva  </a>	</li>
											<li><a href="report-indicator-mc-defeito" title="Gerenciamento e visualizar Indicador Defeito de Manutenção Preventiva ">Defeito de Manutenção Preventiva </a>	</li>
											<li><a href="report-indicator" title="Gerenciamento e visualizar Indicador">Indicadores </a>	</li>
											 </ul>
									</li>
									<li><a>Relatórios<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-report-mc" title="Gerenciamento e visualizar Relatório Manutenção Corretiva">Manutenção Corretiva</a></li>
											<li><a href="report-report-mp" title="Gerenciamento e visualizar Relatório Manutenção Preventiva">Manutenção Preventiva </a>	</li>
											<li><a href="report-report-calibration" title="Gerenciamento e visualizar Relatório Calibração">Calibração</a>	</li>
											<li><a href="report-report-equipament" title="Gerenciamento e visualizar Relatório Inventario">Inventario</a>	</li>
											<li><a href="report-report-horaire" title="Gerenciamento e visualizar Relatório Cronograma Preventiva">Cronograma Preventiva</a>	</li>
											<li><a href="report-report-horaire-calibration" title="Gerenciamento e visualizar Relatório Cronograma Calibração">Cronograma Calibração</a>	</li>
											<li><a href="report-report-hierarchy" title="Gerenciamento e visualizar Relatório Diagrama de Setor">Diagrama de Setor</a>	</li>
											<li><a href="report-report-manufacture" title="Gerenciamento e visualizar Relatório  Avaliação de Fornecedor">Avaliação de Fornecedor</a>	</li>
											<li><a href="report-report-tse" title="Gerenciamento e visualizar Relatório Teste de Segurança Elétrica">Teste de Segurança Elétrica</a>	</li>
											<li><a href="report-report-technological-surveillance" title="Gerenciamento e visualizar Relatório Tecnovigilância">Tecnovigilância</a>	</li>
											<li><a href="report-report-purchases" title="Gerenciamento e visualizar Relatório Compras">Compras</a>	</li>
											<li><a href="report-report-obsolescence" title="Gerenciamento e visualizar Relatório Relatório de Avaliação de Equipamento">Relatório de Avaliação de Equipamento</a>	</li>
											<li><a href="report-report-routine" title="Gerenciamento e visualizar Relatório Rotina">Rotina</a>	</li>
											<li><a href="report-report-calibration-report" title="Gerenciamento e visualizar Relatório Laudos de Calibração">Laudos de Calibração</a>	</li>
											<li><a href="register-equipament-family-movement-list" title="Gerenciamento e visualizar Historico de Saida de Equipamento">Historico de Saida de Equipamento</a>	</li>
<li><a href="register-equipament-form" title="Gerenciamento e visualizar Formularios">Formularios</a>	</li>
<li><a href="report-report-bi" title="Gerenciamento e visualizar Business Intelligence">Business Intelligence</a>	</li>
<li><a href="report-report-costs" title="Gerenciamento e visualizar Custos">Custos</a>	</li>
<li><a href="report-report-pdca-quarterly-analysis" title="Gerenciamento e visualizar Analise Trimestral Avaliação">Analise Trimestral Avaliação</a>	</li>
<li><a href="report-report-pdca-contracts" title="Gerenciamento e visualizar Contratos">Contratos</a>	</li>
<li><a href="report-report-complice" title="Gerenciamento e visualizar Não Conformidade">Não Conformidade</a>	</li>
<li><a href="report-report-pdca-analysis" title="Gerenciamento e visualizar Analise Trimestral O.S">Analise Trimestral O.S</a>	</li>
<li><a href="report-report-anvisa-analysis" title="Gerenciamento e visualizar Relatório Anvisa">Relatório Anvisa</a>	</li>
<li><a href="report-report-annvisa-analysis-equipament" title="Gerenciamento e visualizar Relatório Anvisa Parque Tecnológico">Relatório Anvisa Parque Tecnológico</a>	</li>
<li><a href="report-report-pdca-quarterly-analysis-purchases" title="Gerenciamento e visualizar Analise Trimestral Compras">Analise Trimestral Compras</a>	</li>
<li><a href="report-report-disable-equipament" title="Gerenciamento e visualizar Relatório Baixa Equipamento">Relatório Baixa Equipamento</a>	</li>
<li><a href="report-report-report-equipament" title="Gerenciamento e visualizar Relatório Parecer Tecnico de Equipamento">Relatório Parecer Tecnico de Equipamento</a>	</li>
<li><a href="report-report-assessment-os" title="Gerenciamento e visualizar Relatório de Avaliação de Ordem de Serviço">Relatório de Avaliação de Ordem de Serviço</a>	</li>
<li><a href="report-report-analysis-purchases" title="Gerenciamento e visualizar Analise Compras">Analise Compras</a>	</li>
<li><a href="report-report-analysis-os" title="Gerenciamento e visualizar Analise O.S">Analise O.S</a>	</li>
<li><a href="report-report" title="Gerenciamento e visualizar Relatório">Relatório</a>	</li>


										
										
										</ul>
									</li>
							
								</ul>
							</li>
							<li><a><i class="fa fa-cart-arrow-down"></i> Compras <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a>Solicitação<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-acquisition" title="Abertura de Solititação com Ordem de Serviço">Solicitação com O.S</a>
											</li>
											<li><a href="purchases-acquisition-single" title="Abertura de Solititação Sem Ordem de Serviço">Solicitação Sem O.S</a>
											</li>

										</ul>
									</li>
									<li><a>Inventario<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-inventory" title="Gerenciamento e visualizar Entrada de Material">Entrada de Material</a></li>
											<li><a href="purchases-inventory-output" title="Gerenciamento e visualizar Saida de Material">Saida de Material</a></li>
											<li><a href="purchases-inventory-stock" title="Gerenciamento e visualizar Estoque de Material">Estoque de Material</a></li>
											<li><a>Gerenciar<span class="fa fa-chevron-down"></span></a>
												<ul class="nav child_menu">
													<li class="sub_menu"><a href="purchases-inventory-manager-categoria" title="Gerenciamento e visualizar Categoria de Material">Categoria de Material</a></li>
													<li><a href="purchases-inventory-manager-grupo" title="Gerenciamento e visualizar Grupo de Material">Grupo de Material</a></li>
													<li><a href="purchases-inventory-manager-familia" title="Gerenciamento e visualizar Familia de Material">Familia de Material</a></li>
													<li><a href="purchases-inventory-manager" title="Gerenciamento e visualizar Material">Material</a></li>
												</ul>
											</li>
										</ul>
									</li>

									<li><a>Solicitação<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-progress" title="Gerenciamento de Solititação em Aberto">Solicitação Aberto</a></li>
											<li><a href="purchases-progress" title="Gerenciamento de Solititação em Aprovação">Solicitação Aprovação</a></li>
											<li><a href="purchases-progress-progress" title="Gerenciamento de Solititação em Andamento">Solicitação Andamento</a></li>
											<li><a href="purchases-progress-concluded" title="Gerenciamento de Solititação em Concluido">Solicitação Concluido</a></li>
										</ul>
									</li>

								</ul>
							</li>
					
				</ul>
			</div>
					<?php if($budget_status == "0"){  ?>

					<div class="menu_section">
				<ul class="nav side-menu">
		        	<li><a  href="budget" title="Cadastro e Gerenciamento de Orçamentos"><i class="fa fa-calculator"></i> Orçamentos <span class=""></span>
					</a> </li>
				 </ul>
			</div>
						<?php }  ?>
			<div class="menu_section">
				<h3>Calibração &amp; Obsolescência</h3>
				<ul class="nav side-menu">
					<li><a><i class="fa fa-bug"></i> Calibração <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a>Laudos<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="calibration-report" title="Gerenciamento de laudos">Lista de Laudos</a></li>
									<li><a href="calibration-report-new" title="Cadastro de novo laudo">Novo Laudo</a></li>
									<li><a href="calibration-report-single" title="Gerenciamento de laudos únicos">Lista de Laudos Único</a></li>
								<li><a href="calibration-report-new-single-wizard" title="Cadastro de novo laudo único">Novo Laudo Único</a></li>
								</ul>
							</li>

							<li><a href="calibration-parameter-equipament" title="Cadastro e gerenciamento de equipamento teste">Analisador &amp; Simulador</a></li>
							<li><a>T.S.E<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="calibration-equipament" title="Gerenciamento de Teste de Segurança Elétrica">Lista de T.S.E</a></li>
									<li><a href="calibration-equipament-new" title="Cadastro de novo Teste de Segurança Elétrica">Novo T.S.E</a></li>
									<li><a href="calibration-equipament-procedure" title="Gerenciamento de procedimento de Teste de Segurança Elétrica">Procedimento de T.S.E</a></li>

								</ul>
							</li>
							<li><a>Parametrização<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="calibration-parameter" title="Cadastro e Gerenciamento de Empresa">Empresa</a></li>
									<li><a href="calibration-parameter-dados" title="Cadastro e gerenciamento de Parametrização de Laudo">Parâmetro Laudo</a></li>
									<li><a href="calibration-parameter-equipament" title="Cadastro e gerenciamento de equipamento teste">Analisador &amp; Simulador</a></li>

								</ul>
							</li>
							<li><a>Calibração<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a>Calibração<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="flow-calibration" title="Gerenciar Calibração Aberta">Cal. Aberta</a>
											</li>
											<li><a href="flow-calibration-before" title="Gerenciar Calibração Atrasada">Cal. Atrasada</a>	</li>
											<li><a href="flow-calibration-close" title="Gerenciar Calibração Fechada">Cal. Fechada</a>	</li>
											<li><a href="flow-calibration-print" title="Gerenciar Impressão de Calibração">Cal. Impressão</a>	</li>
											<li><a href="flow-calibration-print-register" title="Gerenciar Registro de Impressão de Calibração">Cal. Reg. Impressão</a>	</li>
											<li><a href="flow-calibration-control" title="Gerenciar Calibração">Cal. Controle </a>	</li>
										</ul>
									</li>

								</li>
								<li><a href="flow-calibration-procedures" title="Gerenciar Procedimento de Calibração">Procedimento</a>
								</li>
								<li><a href="flow-calibration-routine" title="Gerenciar Rotinas de alibração">Rotina</a>
								</li>
							</ul>
						</li>
					
					</ul>
				</li>
				<li><a><i class="fa fa-windows"></i> Obsolescência <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="obsolescence-analysis" title="Elaboração de plano de Obsolescência">Analise</a></li>
						<li><a href="obsolescence-report" title="Relatório de plano de Obsolescência">Relatório</a></li>
					<li><a href="obsolescence-acquisition" title="Relatório de Aquisição de Obsolescência">Aquisição</a></li>
						
						
					</ul>
				</li>

			

</div>
	<?php if($assistence == "0"){  ?>
					
					<?php }else {   ?>
					<div class="menu_section">
						<h3>Planejamento Estratégico</h3>
						<ul class="nav side-menu">
							<li>
								<a href="obsolescence-prevision-provision" title="Cadastro e Gerenciamento de Planejamento Estratégico">
									<i class="fa fa-calendar"></i> Planejamento Estratégico <span class=""></span>
								</a>
							</li>
							
							
						</ul>
					</div>
					<?php }  ?>	
					 <?php if($assistence == "0"){  ?>
					
					<?php }else {   ?>
					<div class="menu_section">
						<h3>Depreciação</h3>
						<ul class="nav side-menu">
							<li>
								<a href="obsolescence-depreciation" title="Cadastro e Gerenciamento de Planejamento Depreciação">
									<i class="fa fa-line-chart"></i> Planejamento Depreciação <span class=""></span>
								</a>
							</li>
							
							
						</ul>
					</div>
					<?php }  ?>	
<div class="menu_section">
	<h3>Melhoria contínua  </h3>
	<ul class="nav side-menu">
		<?php if($assistence == "0"){  ?>
		
		<?php }else {   ?>
		<li><a><i class="fa fa-cube"></i> Análise Trimestral <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="pdca-quarterly-analysis" title="Elaboração de Análise Trimestral">Análise</a></li>
				<li><a href="pdca-quarterly-report" title="Relatorio de Análise Trimestral">Relatorio</a></li>
				
				<!--     <li><a href="index3.php">Dashboard3</a></li>  -->
			</ul>
		</li>
		<?php }  ?>	
		<li><a><i class="fa fa-database"></i> Contratos <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="pdca-contracts" title="Cadastro de Contratos">Contratos</a></li>
				<li><a href="pdca-contracts-configure" title="Gerenciamento de Contratos">Configuracoes</a></li>
				
				<!--     <li><a href="index3.php">Dashboard3</a></li>  -->
			</ul>
		</li>
	 
		
	</ul>
</div>
<div class="menu_section">
	<h3>Tecnovigilância & Alerta </h3>
	<ul class="nav side-menu">
		<li><a  href="technological-surveillance" title="Cadastro e Gerenciamento de Alertas da ANVISA"><i class="fa fa-exclamation"></i> Alertas <span class=""></span></a> </li>
		<?php if($assistence == "0"){  ?>
		
		<?php }else {   ?>
	<li><a  href="alert" title="Cadastro e Gerenciamento de Eventos Sentinelas"><i class="fa fa-exclamation-triangle" ></i> Eventos Sentinelas <span class=""></span></a> </li>
		<?php }  ?>	
			</ul>
</div>
<div class="menu_section">
	<!-- <h3>Rastreabilidade & Lean </h3> -->

	<ul class="nav side-menu">
		<li><a><i class="fa fa-tags"></i> RFID <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="rfid-tags" title="Cadastro e Gerenciamento de Tags">Tags </a></li>
				<li><a href="rfid-logs" title="Cadastro e Gerenciamento de Logs">Logs </a></li>
				<li><a href="rfid-point" title="Cadastro e Gerenciamento de Point">Point </a></li>


			</ul>
		</li>

	

		<li><a><i class="fa fa-heart"></i> Lean Healthcare <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="lean-define" title="Cadastro e Gerenciamento de Lean Healthcare Definir">Definir</a></li>
				<li><a href="lean-measure" title="Cadastro e Gerenciamento de Lean Healthcare Medir">Medir</a></li>
				<li><a href="lean-analyse" title="Cadastro e Gerenciamento de Lean Healthcare Analisar">Analisar </a></li>
				<li><a href="lean-improve" title="Cadastro e Gerenciamento de Lean Healthcare Melhorar">Melhorar </a></li>
				<li><a href="lean-control" title="Cadastro e Gerenciamento de Lean Healthcare Controlar">Controlar </a></li>

		
			</ul>
		</li>
		<!-- <li><a  href="artificial-intelligence"title="Gerenciamento de Inteligência artificial"><i class="fa fa-android"></i> I.A <span class=""></span></a> </li> -->


	</div>

</div>
<!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                <script>
                var goFS = document.getElementById("goFS");
                goFS.addEventListener("click", function() {
                  document.body.requestFullscreen();
                }, false);
              </script>

              </a>
              <a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="lockscreen">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
                    <a data-toggle="tooltip" data-placement="top" title="Parecer Tecnico" href="parecer"target="_blank">
                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Descritivo Tecnico" href="descritivo"target="_blank">
                <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
              </a>
                <a data-toggle="tooltip" data-placement="top" title="Manuais Tecnico" href="tecnico"target="_blank">
                <span class="glyphicon glyphicon glyphicon-folder-close" aria-hidden="true"></span>
              </a>
                <a data-toggle="tooltip" data-placement="top" title="Manuais Usuário" href="usuario"target="_blank">
                <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
              </a>
        

            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                        <img src="logo/img.jpg" alt=""><?php printf($usuariologado) ?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="profile">   <i class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></i> Perfil </a>
                        <a class="dropdown-item"  href="documentation-bussines">   <i class="glyphicon glyphicon-file pull-right" aria-hidden="true"></i> Documentos </a>
                        	   <a class="dropdown-item"  href="documentation-event-task">
						<span class="badge bg-red pull-right"><?php include 'api/api-event-task.php';?></span>
						<span>Eventos e Tarefas</span>
					</a>
				   
						<?php if($assistence == "0"){  ?>
						
						<?php }else {   ?>
						<a class="dropdown-item"  href="documentation-widget"> <i class="fa fa-desktop pull-right" aria-hidden="true"></i>Widget</a>
						<?php }  ?>	
						
				
					
						<a class="dropdown-item"  href="documentation">   <i class="fa fa-code pull-right" aria-hidden="true"></i> Sistema </a>
						<a class="dropdown-item"  href="documentation-pop">   <i class="fa fa-book pull-right" aria-hidden="true"></i> POP </a>
						<a class="dropdown-item"  href="documentation-update">   <i class="fa fa-history pull-right" aria-hidden="true"></i> Atualização </a>
						<?php if($assistence == "0"){  ?>
						
						<?php }else {   ?>
						<a class="dropdown-item"  href="documentation-hfmea">   <i class="fa fa-map pull-right" aria-hidden="true"></i> HFMEA </a>
						<?php }  ?>	
					
						<a class="dropdown-item"  href="documentation-label">   <i class="fa fa-fax pull-right" aria-hidden="true"></i> Rotuladora </a>
						<?php if($assistence == "0"){  ?>
						
						<?php }else {   ?>
					<a class="dropdown-item"  href="documentation-support">   <i class="fa fa-support pull-right" aria-hidden="true"></i> Contigência </a>
						<?php }  ?>	
						
						<a class="dropdown-item"  href="documentation-bell">   <i class="fa fa-bell pull-right" aria-hidden="true"></i> Central de Notificação </a>
						<?php if($assistence == "0"){  ?>
						
						<?php }else {   ?>
						<a class="dropdown-item"  href="documentation-graduation">   <i class="fa fa-graduation-cap pull-right" aria-hidden="true"></i> Educação Continuada </a>
						
						<a class="dropdown-item"  href="documentation-connectivity">   <i class="fa fa-code-fork pull-right" aria-hidden="true"></i> Conectividade e Recursos </a>
						<?php }  ?>	

             

                <li role="presentation" class="nav-item dropdown open">
					<a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false" title="Novas Notificações">
						<i class="fa fa-bell-o"></i>
						
						<span class="badge badge-dark"> <?php include 'api/api-bell-notification.php';?> </span>
					</a>

                 <li role="presentation" class="nav-item dropdown open">
						<a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false" title="Novas Ordem de Serviço de Integração">
							<i class="fa fa-code-fork"></i>
							
							<span class="badge bg-blue"> <?php include 'api/api-os-integration.php';?> </span>
						</a>


                   <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false" title="Novas Alertas Tecnovigilância Anvisa">
                      <i class="fa fa-exclamation-triangle"></i>

                      <span class="badge bg-orange"> <?php include 'api/api-anvisa-alert.php';?> </span>
                    </a>

                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false" title="Novas Ordem de Serviços">
						<i class="fa fa-envelope-o"></i>

                      <span class="badge bg-green"> <?php include 'workflow/envelope.php';?> </span>
                    </a>


                    </li>
                      <li role="presentation" class="nav-item dropdown open">
                   <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false" title="Novas Assinaturas de Ordem de Serviços">
						<i class="fa fa-envelope-o"></i>

						<span class="badge bg-red"> <?php include 'workflow/envelope-signature.php';?> </span>
                    </a>


       


me da sugestao de produtividade para implementar um aplicativo e quais recursos e modulos  sao interessante adicionar visando mobile e poder utilizar o que o celular ou tablet oferece em relação a camera e ao display touch