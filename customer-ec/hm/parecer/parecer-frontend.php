
<?php
include("../database/database.php");
$query = "SELECT parecer.id, parecer.titulo, parecer.upgrade,parecer.reg_date, equipamento_grupo.nome FROM parecer INNER JOIN equipamento_grupo ON parecer.id_equipamento_grupo = equipamento_grupo.id  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$titulo,$upgrade,$reg_date,$grupo);



?>


        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Parecer Tecnico</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista <small> de Parecer Tecnico</small></h2>
                    <ul  class="nav navbar-right panel_toolbox">
                         <li><a href="parecer-add" class=""><i class="fa fa-plus"></i></a>
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">








<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Titulo</th>
                          <th>Grupo</th>
                          <th>Atualização</th>
                          <th>Cadastro</th>
                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($grupo); ?></td>
                            <td><?php printf($upgrade); ?></td>
                              <td><?php printf($reg_date); ?></td>

                          <td>  <a  class="btn btn-app" href="parecer-viewer?id=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Parecer',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                   <a class="btn btn-app" href="parecer-edit?id=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Abrir Parecer',
																text: 'Receber Parecer',
																type: 'success',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-folder-open"></i> Editar
                  </a>
                   <a class="btn btn-app"  href="parecer-version?id=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Cancelamento de Parecer',
																text: 'Cancelamento de Abertura de Parecer',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-info"></i> Versão
                  </a>
              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


							</div>
						</div>
					</div>
				</div>
					</div>


             </div>
