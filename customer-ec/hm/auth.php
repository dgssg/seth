<?php
require 'database/database.php';
session_start();

if (!isset($_SESSION['session_token'])) {

     
     // Preparar a consulta para atualizar o token no banco de dados
    $stmt = $conn->prepare("UPDATE usuario SET session_token = NULL WHERE session_token = ?");
    
    if ($stmt) {
        // Associar o parâmetro e executar a consulta
        $stmt->bind_param('s', $_SESSION['session_token']);
        $stmt->execute();
        $stmt->close();
    } else {
        // Registrar erro se a preparação falhar
        error_log("Erro ao preparar a consulta para limpar o session_token: " . $conn->error);
    }


    
    session_destroy();
session_commit();
header ("location: ../../index.php");


}


    // Valida o token de sessão
    $stmt = $conn->prepare("SELECT * FROM usuario WHERE session_token = ? AND is_active = TRUE");
    $stmt->bind_param('s', $_SESSION['session_token']);
    $stmt->execute();
    $result = $stmt->get_result();
    $user = $result->fetch_assoc();


if (!$user) {
     
     // Preparar a consulta para atualizar o token no banco de dados
    $stmt = $conn->prepare("UPDATE usuario SET session_token = NULL WHERE session_token = ?");
    
    if ($stmt) {
        // Associar o parâmetro e executar a consulta
        $stmt->bind_param('s', $_SESSION['session_token']);
        $stmt->execute();
        $stmt->close();
    } else {
        // Registrar erro se a preparação falhar
        error_log("Erro ao preparar a consulta para limpar o session_token: " . $conn->error);
    }


    
    session_destroy();
session_commit();
header ("location: ../../index.php");
}

// Define o nível de acesso do usuário
define('USER_LEVEL', $user['user_level']);
?>
