<?php
include("../database/database.php");

// Verifique se o método de requisição é POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    // Verifique se o parâmetro 'id' foi enviado
    if (isset($_GET['id'])) {
        
        // Sanitize e obtenha o ID
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
        
        // Aqui você pode realizar as operações de atualização no banco de dados usando o $id
        // ...

        // Exemplo: Atualize os dados na tabela regdate_mp_registro
        $stmt = $conn->prepare("UPDATE regdate_mp_registro SET date_start=?, date_end=?, time=? WHERE id=?");
        $stmt->bind_param("sssi", $_POST['date_start'], $_POST['date_end'], $_POST['time'], $id);
        $execval = $stmt->execute();
        $stmt->close();

        // Após a atualização, você pode retornar uma resposta para o JavaScript
        // Se a atualização for bem-sucedida, você pode retornar algo como:
        echo json_encode(array('success' => true, 'message' => 'Registro atualizado com sucesso.'));
        exit;
    }
}

// Se o script chegou aqui, algo deu errado
echo json_encode(array('success' => false, 'message' => 'Erro ao processar a atualização.'));
exit;
?>
