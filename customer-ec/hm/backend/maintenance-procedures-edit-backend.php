<?php

include("../database/database.php");

$codigoget= $_POST['id'];
$nome= $_POST['nome'];
$info= $_POST['info'];
$habilitado= $_POST['habilitado'];
$codigo= $_POST['codigo'];



$stmt = $conn->prepare("UPDATE maintenance_procedures SET name = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE maintenance_procedures SET info = ? WHERE id= ?");
$stmt->bind_param("ss",$info,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE maintenance_procedures SET habilitado = ? WHERE id= ?");
$stmt->bind_param("ss",$habilitado,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE maintenance_procedures SET codigo = ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../maintenance-procedures-edit?procedures='.$codigoget);
?>