  <?php


// Create connection

include("../../database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3> Equipamentos <small>Analisador &amp; Simulador</small></h3>
              </div>

           
            </div>
       
              
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                    <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                 
                   
                  </div>
                </div>
              </div>
            </div>
              
        
              
                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Laudos </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
          <?php


date_default_timezone_set('America/Sao_Paulo');
$ano=date("Y");
$today=date("Y-m-d");
$mes=date("m");
$query = "SELECT   calibration_parameter_tools_laudos.file,calibration_parameter_tools.id, calibration_parameter_tools.nome, calibration_parameter_tools.fabricante, calibration_parameter_tools.modelo, calibration_parameter_tools.serie, calibration_parameter_tools.patrimonio,calibration_parameter_tools.calibration,calibration_parameter_tools.date_calibration, calibration_parameter_tools.date_validation FROM calibration_parameter_tools LEFT JOIN calibration_parameter_tools_laudos ON calibration_parameter_tools_laudos.id_calibration_parameter_tools = calibration_parameter_tools.id and calibration_parameter_tools_laudos.date_validation =  calibration_parameter_tools.date_validation  GROUP BY calibration_parameter_tools.id ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($file,$id, $nome, $fabricante, $modelo, $serie, $patrimonio,$calibration, $date_calibration, $date_validation);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  } calibration_parameter_tools_laudos.file,


?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
         
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Laudo</th>
                          <th>Nome</th>
                          <th>Fabricante</th>
                          <th>Modelo</th>
                          <th>Nº Serie</th>
                          <th>Patrimonio</th>
                          <th>Nº Certificado</th>
                          <th>Calibração</th>
                          <th>Validação</th>
                          <th>Status</th>
                          <th>Ação</th>
                           </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {  if($today < $date_validation ){  ?>

                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td> <?php if($file==!""){?><a href="../../dropzone/analisador_simulador/<?php printf($file); ?> " target="_blank" ><i class="fa fa-paperclip"></i> </a>     <?php  }  ?></td>
                          <td><?php printf($nome); ?> </td>
                          <td><?php printf($fabricante); ?> </td>
                          <td><?php printf($modelo); ?> </td>
                          <td><?php printf($serie); ?> </td>
                           <td><?php printf($patrimonio); ?> </td>
                          <td><?php printf($calibration); ?> </td>
                           <td><?php printf($date_calibration); ?> </td>
                            <td><?php printf($date_validation); ?> </td>
                            <td><?php if($today > $date_validation ){ printf("Fora da Validade");}  if($today < $date_validation ){ printf("Dentro da Validade");}?></td> 




                          <td>
                 
                  <a class="btn btn-app"  href="../../calibration-parameter-equipament-viewer?tools=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar procediemnto',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-file-pdf-o"></i> Prontuario
                  </a>
                  <?php if($file==!""){?>
                   <a class="btn btn-app"   href="../../dropzone/analisador_simulador/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar procediemnto',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-file"></i> Laudo Físico
                  </a>
                  <?php  }  ?>
                  </td>
                        </tr>
                        <?php   }} }  ?>
                      </tbody>
                   
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

                  
                </div>
              </div> 
	       
	        
                  
                  
                </div>
              </div>
              
              <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
        initComplete: function () {
            this.api()
                .columns([2,3,4,5,10])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
                    var container = $('<div class="filter-title"></div>')
                                    .appendTo('#userstable_filter');
                    var label = $('<label>'+theadname+': </label>')
                                .appendTo(container); 
                    var select = $('<select  class="form-control my-1"><option value="">' +
        theadname +'</option></select>')
                        .appendTo( '#userstable_filter'  ).select2()
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
 
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });

                
        },
    });
});
</script>


  