<?php
include("../database/database.php");

// Inicialize a variável da query com a consulta base
$query = "
SELECT regdate_os_dropzone.file, custom_service.nome AS 'custom_service',category.nome AS 'category',equipamento_familia.fabricante,os_sla.sla_verde,os_sla.sla_amarelo,os.sla_term,os_grupo_sla.grupo,os_posicionamento.cor,os_prioridade.prioridade,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome AS 'usuario',usuario.sobrenome AS 'sobrenome',instituicao_localizacao.nome AS 'area', instituicao_area.nome AS 'setor', os_status.status AS 'os_status', os_posicionamento.status AS 'os_posicionamento',os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome AS 'equipamento', equipamento_familia.modelo,os.assessment FROM os LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN os_prioridade on os_prioridade.id = os.id_prioridade LEFT JOIN os_sla on os_sla.id = os.id_sla LEFT JOIN os_grupo_sla on os_grupo_sla.id = os_sla.os_grupo_sla LEFT JOIN custom_service on custom_service.id = os.id_servico LEFT JOIN category on category.id = os.id_category LEFT JOIN regdate_os_dropzone ON regdate_os_dropzone.id_os = os.id WHERE os.id_status like '6'
";

// Adicione os filtros se os parâmetros estiverem definidos
$filters = [];
if (isset($_GET['os'])) {
    $filters[] = "os.id = " . intval($_GET['os']);
}
    if (isset($_GET['data_start'])) {
        $filters[] = "os.reg_date >= " . intval($_GET['data_start']);
    }
    if (isset($_GET['data_end'])) {
        $filters[] = "os.reg_date <= " . intval($_GET['data_end']);
    }
    if (isset($_GET['unidade'])) {
        $filters[] = "instituicao.instituicao LIKE '%" . $conn->real_escape_string($_GET['unidade']) . "%'";
    }
    if (isset($_GET['setor'])) {
        $filters[] = "instituicao_area.nome LIKE '%" . $conn->real_escape_string($_GET['setor']) . "%'";
    }
    if (isset($_GET['area'])) {
        $filters[] = "instituicao_localizacao.nome LIKE '%" . $conn->real_escape_string($_GET['area']) . "%'";
    }
    if (isset($_GET['familia'])) {
        $filters[] = "equipamento_familia.nome LIKE '%" . $conn->real_escape_string($_GET['familia']) . "%'";
    }
    if (isset($_GET['fabricante'])) {
        $filters[] = "equipamento_familia.fabricante LIKE '%" . $conn->real_escape_string($_GET['fabricante']) . "%'";
    }
    if (isset($_GET['modelo'])) {
        $filters[] = "equipamento_familia.modelo LIKE '%" . $conn->real_escape_string($_GET['modelo']) . "%'";
    }
    if (isset($_GET['codigo'])) {
        $filters[] = "equipamento.codigo LIKE '%" . $conn->real_escape_string($_GET['codigo']) . "%'";
    }
    if (isset($_GET['patrimonio'])) {
        $filters[] = "equipamento.patrimonio LIKE '%" . $conn->real_escape_string($_GET['patrimonio']) . "%'";
    }
    if (isset($_GET['serie'])) {
        $filters[] = "equipamento.serie LIKE '%" . $conn->real_escape_string($_GET['serie']) . "%'";
    }
if (isset($_GET['grupo'])) {
        $filters[] = "equipamento_grupo.nome LIKE '%" . $conn->real_escape_string($_GET['grupo']) . "%'";
}
    

if (isset($_GET['ativo'])) {
    $filters[] = "equipamento.ativo = " . intval($_GET['ativo']);
}
    if (isset($_GET['baixa'])) {
        $filters[] = "equipamento.baixa = " . intval($_GET['baixa']);
    }
    if (isset($_GET['comodato'])) {
        $filters[] = "equipamento.comodato = " . intval($_GET['comodato']);
    }
// Se houver filtros, adicione-os na query
if (count($filters) > 0) {
    $query .= " AND " . implode(" AND ", $filters);
}

$query .= " GROUP BY os.id order by os.id DESC ";

// Execute a consulta e retorne os resultados como JSON
$resultados = $conn->query($query);

if (!$resultados) {
    die("Erro na consulta ao banco de dados: " . $conn->error);
}

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

// Configurar o cabeçalho para JSON
header('Content-Type: application/json');
echo json_encode($rows);

$conn->close();
?>
