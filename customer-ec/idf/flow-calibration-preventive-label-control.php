<?php
include("database/database.php");
session_start();
if(!isset($_SESSION['usuario'])){
  header ("Location: ../index.php");
}
if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
}
$usuariologado=$_SESSION['usuario'];
$instituicaologado=$_SESSION['instituicao'];
$setorlogado=$_SESSION['setor'];
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=seth_data.csv');
$saida = fopen('php://output', 'w');
fputcsv($saida, array('id', 'rotina', 'realizado', 'periodicidade', 'codigo','equipamento','proxima'));

$codigoget = ($_GET["routine"]);
$query = "SELECT id FROM calibration_preventive WHERE id_routine = '$codigoget'  ORDER BY id DESC limit 1  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id_equipamento);

  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
    $codigoget =$id_equipamento;
  }
}

$query = "SELECT CONCAT('https://seth.mksistemasbiomedicos.com.br/customer-ec/','$instituicaologado','/flow-calibration-preventive-viwer?id=',calibration_preventive.id) AS 'id', calibration_preventive.id_routine, calibration_preventive.date_start,calibration_routine.periodicidade,equipamento.codigo,equipamento_familia.nome, IF( calibration_routine.periodicidade = 730, (date_add(calibration_preventive.date_start, INTERVAL 2 year  )) ,IF( calibration_routine.periodicidade = 365, (date_add(calibration_preventive.date_start, INTERVAL 1 year  )) ,IF( calibration_routine.periodicidade = 180, (date_add(calibration_preventive.date_start, INTERVAL 6 month   )) ,IF( calibration_routine.periodicidade = 120, (date_add(calibration_preventive.date_start, INTERVAL 4 month   )) ,IF( calibration_routine.periodicidade = 90, (date_add(calibration_preventive.date_start, INTERVAL 3 month   )) ,IF( calibration_routine.periodicidade = 60, (date_add(calibration_preventive.date_start, INTERVAL 2 month   )) ,IF( calibration_routine.periodicidade = 30, (date_add(calibration_preventive.date_start, INTERVAL 1 month   )) ,IF( calibration_routine.periodicidade = 28, (date_add(calibration_preventive.date_start, INTERVAL 28 day   )) ,IF( calibration_routine.periodicidade = 21, (date_add(calibration_preventive.date_start, INTERVAL 21 day   )) ,IF( calibration_routine.periodicidade = 14, (date_add(calibration_preventive.date_start, INTERVAL 14 day   )) ,IF( calibration_routine.periodicidade = 7, (date_add(calibration_preventive.date_start, INTERVAL 7 day   )) ,IF( calibration_routine.periodicidade = 1 OR calibration_routine.periodicidade = 5, (date_add(calibration_preventive.date_start, INTERVAL 1 day   )) ,1 ) ) ) ) ) ) ) ) ) ) ) )  AS 'proxima' FROM calibration_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN calibration_preventive ON calibration_preventive.id_routine = calibration_routine.id WHERE calibration_preventive.id LIKE '$codigoget'";

//if ($stmt = $conn->prepare($query)) {
//    $stmt->execute();
//    $stmt->bind_result($id,$nome,$codigo,$instituicao_localizacao, $instituicao_area);
//   while ($stmt->fetch()) {

//   }
// }
 $linhas = mysqli_query($conn, $query);
while($linha = mysqli_fetch_assoc($linhas)) {
  fputcsv($saida, $linha);
};
//echo "<script>window.close();</script>";
 ?>
