<?php

include("../database/database.php");

// Verifica se o ID foi enviado
if (!isset($_GET['id']) || empty($_GET['id'])) {
    die("Erro: ID não recebido.");
}

$codigoget = $_GET['id'];

// Atualiza a resposta como correta
$stmt = $conn->prepare("UPDATE user_training_responses SET is_correct = 0 WHERE id = ?");
$stmt->bind_param("i", $codigoget);
$execval = $stmt->execute();

if (!$execval) {
    die("Erro ao atualizar resposta: " . $stmt->error);
}

// Obtém o id_attempt associado
$query = "SELECT id_attempt FROM user_training_responses WHERE id = ?";
$stmt = $conn->prepare($query);
$stmt->bind_param("i", $codigoget);
$stmt->execute();
$stmt->bind_result($id_attempt);
$stmt->fetch();
$stmt->close();

// Verifica se um id_attempt foi retornado
if (empty($id_attempt)) {
    die("Erro: Nenhum id_attempt encontrado.");
}

// Redireciona para a próxima página
echo "<script>alert('Adicionado!');document.location='../documentation-graduation-training-feedback-assessment-check?id=$id_attempt'</script>";

?>
