<?php


include("../database/database.php");


$id_pop = $_GET['id_pop'];

$titulo = $_POST['titulo'];
$anexo=$_COOKIE['anexo'];

$stmt = $conn->prepare("INSERT INTO regdate_pop_img_dropzone (id_pop, file,titulo) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$id_pop,$anexo,$titulo);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
header('Location: ../documentation-pop-edit.php?pop='.$id_pop);
?>