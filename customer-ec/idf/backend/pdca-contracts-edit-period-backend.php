<?php


include("../database/database.php");


$id_contracts = $_GET['id_contracts'];

$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];
$percentage = $_POST['percentage'];
$percentage_applied = $_POST['percentage_applied'];
$vlr = $_POST['vlr'];
$obs = $_POST['obs'];


$stmt = $conn->prepare("INSERT INTO contracts_period (id_contracts,date_start,date_end,percentage,percentage_applied,vlr,obs) VALUES (?, ?, ?,?, ?, ?, ?)");
$stmt->bind_param("sssssss",$id_contracts,$date_start,$date_end,$percentage,$percentage_applied,$vlr,$obs);
$execval = $stmt->execute();
$stmt->close();


echo "<script>alert('Adicionado!');document.location='../pdca-contracts-edit?id=$id_contracts'</script>";
?>
