<?php
include("../database/database.php");
$query = "SELECT equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome AS 'setor',instituicao_localizacao.nome AS 'area', maintenance_preventive.id_routine ,maintenance_routine.id AS 'rotina',maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome AS 'equipamento',equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade where maintenance_preventive.id_status like '3' and maintenance_routine.trash = 1 group by maintenance_preventive.id_routine order by maintenance_routine.id DESC ";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
