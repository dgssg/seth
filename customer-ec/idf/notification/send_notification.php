<?php
// Conexão com o banco de dados
    $conn = new mysqli("localhost", "cvheal47_root", "cvheal47_root", "cvheal47_seth_mks");

// Verifica a conexão
if ($conn->connect_error) {
    die("Conexão falhou: " . $conn->connect_error);
}

// Consulta para obter as novas notificações
$sql = "SELECT title, notif_msg FROM notif WHERE read = 1";
$result = $conn->query($sql);

// Enviar notificações para o Service Worker
if ($result->num_rows > 0) {
    $notifications = array();
    while($row = $result->fetch_assoc()) {
        $notifications[] = $row;
    }
    echo json_encode($notifications);
} else {
    echo "Sem novas notificações.";
}

// Fecha conexão
$conn->close();
?>
