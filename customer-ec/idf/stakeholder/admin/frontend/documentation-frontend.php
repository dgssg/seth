 <?php

include("../../database/database.php");


//$con->close();


?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Documentos <small></small></h3>
              </div>

           
            </div>

            <div class="clearfix"></div>
            
             

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabela <small>de Documentos</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- start accordion -->
                    <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                      <div class="panel">
                        <a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                          <h4 class="panel-title">Ordem de serviço</h4>
                        </a>
                        <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Relatório</th>
                                  <th>Nome</th>
                                  <th>Codigo</th>
                                  <th>Versão</th>
                                  <th>Visualizar</th>
                                 
                                  
                               
                                </tr>
                              </thead>
                              <tbody>
                                  <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date FROM  documentation WHERE typology like 'mc' ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
 
    ?>
                                <tr>
                                    
                                  <th scope="row"> <?php   printf($id);   ?></th>
                                  <td><?php   printf($report);   ?></td>
                                  <td><?php   printf($name);   ?></td>
                                   <td><?php   printf($codigo);   ?></td>
                                    <td><?php   printf($version);   ?></td>
                                     <td><a target="_blank" href="<?php   printf($link);   ?>"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                                  
                                 
                            
                                </tr>
                                <?php       }
}
    ?>
                               
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      
                      
          
                    </div>
                    <!-- end of accordion -->


                  </div>
                </div>
              </div>
	       </div>
	       
	       
                  
                  
                </div>
              </div>
              
            
        <!-- /page content -->