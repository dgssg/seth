<?php
// Conecta ao banco de dados (substitua pelas suas credenciais)
include("database/database.php");

// Recebe os dados da solicitação AJAX
$tabelaHTML = $_POST['tabelaHTML'];

// Atualiza o valor no banco de dados
$query = "UPDATE maintenance_preventive SET digital = ? WHERE id = ?";

// Convertendo o JSON recebido de volta para um array PHP associativo
$data = json_decode($tabelaHTML, true);

foreach ($data as $row) {
    $id = $row['id'];
    $value = $row['digital'];

    if ($stmt = $conn->prepare($query)) {
        $stmt->bind_param("ss", $value, $id);
        $stmt->execute();
        $stmt->close();
    } else {
        echo "Erro na preparação da consulta: " . $conn->error;
        exit; // Sai do script em caso de erro
    }
}

echo "Valores atualizados com sucesso!";

$conn->close();
?>
