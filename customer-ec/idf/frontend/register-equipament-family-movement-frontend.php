<?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Saida Equipamento</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 <div class="clearfix"></div>


         <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Saida <small>de Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                  <form  action="backend/register-equipament-exit-out-backend.php" method="post">


                  <div class="ln_solid"></div>
                  <div class="item form-group">
             <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ordem de Serviço</label>
                        <div class="col-md-4 col-sm-4 ">
										<select type="text" class="form-control has-feedback-left" name="id_os" id="id_os"  >
										     <option value="">Selecione uma Ordem de Serviço</option>
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  os.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM os INNER JOIN equipamento ON equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE os.id_status like '2' ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><b>  O.S:</b> <?php printf($id);?>	 <b> Equipamento: </b> <?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($modelo);?></option>
										  	<?php
											// tira o resultado da busca da mem贸ria
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-cog form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Ordem de serviço "></span>
										
										
								      	</div>	
								      </div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#id_os').select2();
                                      });
                                 </script>
                                 

<div class="field item form-group">
                      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Empresa <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="unidade" id="unidade"  placeholder="Unidade">
										<?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                   <?php
                      

   $result_cat_post  = "SELECT instituicao.id, instituicao.instituicao FROM instituicao WHERE trash = 1 ";

   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
             while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
               echo '<option></option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
             }
                    ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma unidade "></span>

                  </div>

                  </div>

								 <script>
                                    $(document).ready(function() {
                                    $('#unidade').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Cliente <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Setor">
										  <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Setor "></span>

                  </div>
                  </div>
                            

								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>

<div class="field item form-group">
                      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Localização <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Area <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
                    <option value="">Selecione uma Opção</option>   
                  </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Area "></span>

                    </div>

                    </div>
                                
								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>
                                 

                                 <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Equipamento<span
                       >*</span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
										  <option value="">Selecione o equipamento</option>
                      <?php


					$query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
                                       	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>
                  </div>


								      		    
                  </div>
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento').select2();
                                      });
                                 </script>

                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Lista de Equipamento</label>
                      <div class="col-md-4 col-sm-4 ">
                        <select multiple="multiple" type="text" class="form-control has-feedback-right" name="id_equipamento[]" id="id_equipamento">
                          <option value="">Selecione um Equipamento</option>
                          <?php
                            $stmt = $conn->prepare("SELECT id FROM equipamento");
                            $stmt->execute();
                            $stmt->bind_result($result);
                            $stmt->fetch();
                            $stmt->close();
                            
                            $sql = "SELECT equipamento.id, equipamento.codigo, equipamento_familia.nome, equipamento_familia.fabricante, equipamento_familia.modelo FROM equipamento LEFT JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id";
                            
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                            $stmt->bind_result($id, $codigo, $nome, $fabricante, $modelo);
                            
                            while ($stmt->fetch()) {
                              $selected = (is_array($id_categoria) && in_array($id, $id_categoria)) ? 'selected' : '';
                              echo "<option value='$id' $selected>$codigo - $nome - $modelo - $fabricante</option>";
                            }
                            
                            $stmt->close();
                          ?>
                        </select>
                      </div>
                    </div>
                    
                    <script>
                      $(document).ready(function () {
                        $('#id_equipamento').select2();
                      });
                    </script>



<div class="ln_solid"></div>

                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Família de Equipamento</label>
                      <div class="col-md-4 col-sm-4 ">
                        <select type="text" class="form-control has-feedback-right" name="equipamento_familia" id="equipamento_familia">
                          <option value="">Selecione uma Família de Equipamento</option>
                          <?php
                            $sql = "SELECT id, nome, fabricante, modelo FROM equipamento_familia WHERE trash = 1";
                            
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                            $stmt->bind_result($id, $nome, $fabricante, $modelo);
                            
                            while ($stmt->fetch()) {
                              $selected = ($id == $id_familia) ? 'selected' : '';
                              echo "<option value='$id' $selected>$nome - $fabricante - $modelo</option>";
                            }
                            
                            $stmt->close();
                          ?>
                        </select>
                      </div>
                    </div>
                    
                    <script>
                      $(document).ready(function () {
                        $('#equipamento_familia').select2();
                      });
                    </script>
       
<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Movimentação<span
                        ></span></label>
                    <div class="col-md-6 col-sm-6">
                         	<select type="text" class="form-control has-feedback-right" name="id_equipament_exit_mov" id="id_equipament_exit_mov"   >

										  	<?php



										    $sql = "SELECT  id, nome FROM equipament_exit_mov ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_equipament_exit_mov,$mov);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_equipament_exit_mov);?>	"><?php printf($mov);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                                         <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Movimentação "></span>
                  </div>


								      		    
                  </div>








								 <script>
                                    $(document).ready(function() {
                                    $('#id_equipament_exit_mov').select2();
                                      });
                                 </script>
  

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_equipament_exit_reason" class="docs-tooltip" data-toggle="tooltip" title="Selecione uma motivo ">Motivo <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_equipament_exit_reason" id="id_equipament_exit_reason"   >

										  	<?php



										    $sql = "SELECT  id, nome FROM equipament_exit_reason ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_equipament_exit_reason,$reason);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_equipament_exit_reason);?>	"><?php printf($reason);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#id_equipament_exit_reason').select2();
                                      });
                                 </script>

                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="fornecedor" class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">Fornecedor <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="fornecedor" id="fornecedor"  placeholder="Fornecedor" >
                           <option  >Selecione um Fornecedor	</option>
										  	<?php



										   $sql = "SELECT  id, empresa FROM fornecedor  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_empresa,$empresa);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_empresa);?>	"><?php printf($empresa);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#fornecedor').select2();
                                      });
                                 </script>


                     <div class="ln_solid"></div>
                     <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Responsavel</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="responsavel" class="form-control" type="text" name="responsavel"   >
                        </div>
                      </div>

                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="telefone" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Telefone <span></span>
                      </label>
                      <div class="col-md-6 col-sm-6">
                        <input type="text" id="telefone" name="telefone" class="form-control" value="<?php echo $telefone; ?>" pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="(99)-99999-9999">
                      </div>
                    </div>
                    
                    <script type="text/javascript">
                      $(document).ready(function() {
                        $('#fornecedor').change(function() {
                          var selectedFornecedor = $('#fornecedor').val();
                          
                          $.ajax({
                            type: "POST",
                            url: "sub_fornecedor_telefone.php",
                            data: { id: selectedFornecedor },
                            success: function(telefone) {
                              // Atualize o campo "Telefone" com a resposta do servidor
                              $('#telefone').val(telefone);
                            }
                          });
                        });
                      });
                    </script>
                    <script type="text/javascript">
                      $(document).ready(function() {
                        $('#fornecedor').change(function() {
                          var selectedFornecedor = $('#fornecedor').val();
                          
                          $.ajax({
                            type: "POST",
                            url: "sub_fornecedor_name.php",
                            data: { id: selectedFornecedor },
                            success: function(response) {
                              // Atualize o campo "Contato" com a resposta do servidor
                              $('#responsavel').val(response);
                            }
                          });
                        });
                      });
                    </script>

                    


                    <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Envio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="date_send" class="form-control" type="date" name="date_send"  >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Retorno</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="date_return" class="form-control" type="date" name="date_return"   >
                        </div>
                      </div>

                        <div class="ln_solid"></div>
                          <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_equipament_exit_status" class="docs-tooltip" data-toggle="tooltip" title="Selecione um Status ">Status <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_equipament_exit_status" id="id_equipament_exit_status"   >

										  	<?php



										    $sql = "SELECT  id, nome FROM equipament_exit_status ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_equipament_exit_status,$status);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_equipament_exit_status);?>	"><?php printf($status);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#id_equipament_exit_status').select2();
                                      });
                                 </script>

                                  <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observação</label>
                        <div class="col-md-6 col-sm-6 ">
                        	<textarea  type="text" class="form-control has-feedback-left" id="obs" name="obs" placeholder="Observação" data-ls-module="charCounter" maxlength="1000" data-parsley-maxlength="1000" data-parsley-trigger="keyup" data-parsley-maxlength="1000" data-parsley-minlength-message="Vamos! Você precisa inserir um comentário de pelo menos 20 caracteres."
                            data-parsley-validation-threshold="20"> </textarea>
                        </div>
                      </div>






                    <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                                  </form>




                  </div>
                </div>
              </div>
            </div>

             <!-- page content -->



            <div class="clearfix"></div>







             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




             <!--    ref="backend/os-progress-upgrade-close-frontend.php?os=<?php printf($codigoget); ?>"

             <a class="btn btn-app"  href=""onclick="new PNotify({
																title: 'Atualização',
																text: 'Atualização de Abertura de O.S!',
																type: 'danger',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                  </a> -->





                </div>
              </div>







        </div>
            </div>
        <!-- /page content -->
         <!-- compose -->
         <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
         
<script type="text/javascript">
$(document).ready(function(){
  $('#id_os').change(function(){
    $('#equipamento').select2().load('sub_categorias_post_os_equipamento.php?id='+$('#id_os').val());
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
  $('#unidade').change(function(){
    $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#setor').change(function(){
    $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#area').change(function(){
    $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#equipamento').change(function(){
    $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
  
  });
});
$(document).ready(function(){
  $('#equipamento').change(function(){
    $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

  });
});
$(document).ready(function(){
  $('#equipamento').change(function(){
    $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

  });
});

</script>

<script>
function clean() {
$('#unidade').select2().load('reset_unidade.php');
$('#area').select2().load('reset_area.php');
$('#setor').select2().load('reset_setor.php');
$('#equipamento').select2().load('reset_equipamento.php');


}

</script>
