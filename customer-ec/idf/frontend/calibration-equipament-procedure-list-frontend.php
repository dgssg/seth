<?php
include("database/database.php");
$query = "SELECT tse_parameter_dados.id, tse_parameter_dados.name,tse_parameter_dados.cod,tse_parameter_dados.reg_date,tse_parameter_dados.upgrade FROM tse_parameter_dados WHERE tse_parameter_dados.trash = 1  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id, $name, $codigo, $reg_date, $upgrade);
  //while ($stmt->fetch()) {
  //printf("%s, %s\n", $solicitante, $equipamento);
  //  }


  ?>

  <div class="col-md-11 col-sm-11 ">
    <div class="x_panel">

      <div class="x_content">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box table-responsive">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Codigo</th>
                    <th>Atualização</th>
                    <th>Cadastro</th>


                    <th>Ação</th>

                  </tr>
                </thead>


                <tbody>
                  <?php   while ($stmt->fetch()) {   ?>
                    <tr>
                      <td><?php printf($id); ?> </td>
                      <td><?php printf($name); ?> </td>
                      <td><?php printf($codigo); ?></td>
                      <td><?php printf($upgrade); ?></td>
                      <td><?php printf($reg_date); ?></td>

                      <td>
                        <a class="btn btn-app"  href="calibration-equipament-procedure-edit?id=<?php printf($id); ?>" onclick="new PNotify({
                          title: 'Editar',
                          text: 'Abrindo Edição',
                          type: 'sucess',
                          styling: 'bootstrap3'
                        });">
                        <i class="fa fa-edit"></i> Editar
                      </a>

        <!--      <a class="btn btn-app"  href="calibration-equipament-procedure-viewer?id=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
                title: 'Visualizar',
                text: 'Visualizar procediemnto',
                type: 'info',
                styling: 'bootstrap3'
              });">
              <i class="fa fa-file-pdf-o"></i> Visualizar
            </a> -->
            <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/calibration-equipament-procedure-trash-backend?id=<?php printf($id); ?>';
  }
})
">
            <i class="fa fa-trash"></i> Excluir</a>
          </td>
        </tr>
      <?php   } }  ?>
    </tbody>
  </table>
</div>
</div>
</div>
</div>
</div>
</div>
