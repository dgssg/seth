<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Ordem de Serviço</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                    <div class="input-group">

                        <span class="input-group-btn">

                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
         <div class="clearfix"></div>
              <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                     <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

<!-- Menu -->
 
          
                <a class="btn btn-app <?php echo $current_page == 'os-open' ? 'active' : ''; ?>" href="os-open">
    <i class="fa fa-folder-open"></i> O.S Abertura
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-opened' ? 'active' : ''; ?>" href="os-opened">

    <i class="fa fa-play"></i>O.S Aberto 
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-progress' ? 'active' : ''; ?>" href="os-progress">
	 <i class="fa fa-cogs"></i>O.S Processo
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-signature' ? 'active' : ''; ?>" href="os-signature">

    <i class="fa fa-pencil"></i>O.S Assinatura
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-signature-user' ? 'active' : ''; ?>" href="os-signature-user">

    <i class="fa fa-user"></i>O.S Solicitante
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-finished' ? 'active' : ''; ?>" href="os-finished">

    <i class="fa fa-check-circle"></i>O.S Concluído
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-canceled' ? 'active' : ''; ?>" href="os-canceled">

    <i class="fa fa-times-circle"></i>O.S Cancelada
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-integration' ? 'active' : ''; ?>" href="os-integration">

    <i class="fa fa-code"></i>O.S Integração
</a>

                </div>
              </div>
              
              
              <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Filtro</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Settings 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="userstable_filter"
                            style="column-count:8; -moz-column-count:8; -webkit-column-count:8; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;">
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Legenda <small> de Ordem de Serviço</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Parametro 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Parametro 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div class="form-group ">
                        
                        <div class="col-md-2 col-sm-2  ">
                          <div class="input-group demo2">
                          
                          </div>
                        </div>
                      </div>
                        <?php include 'frontend/os-progress-legend-frontend.php';?>

                    </div>
                </div>
            </div>
        </div>

        <?php
include("database/database.php");
include("database/database_cal.php");

date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d H:i:s");   
$today_pm = date("Y-m-d");   
$query = "SELECT os_rasch_single FROM tools WHERE id = 1";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($os_rasch_single);
            while ($stmt->fetch()) {
               
            }
          }

if($os_rasch_single == "0"){
    function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
     $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
     $con = 1;
     $var = '';
     while($con < $QuantidadeDeCaracteresDaChave ){
         $n = rand(0, 57); 
         if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
     }else{
         $var = $var.$n.$res[$n];
         $con++;
         }
     }
         $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
     }
     //chamando a função.
    // echo chaveAlfaNumerica(5);
  //   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
 if(!self::is_valid($namespace)) return false;

 // Get hexadecimal components of namespace
 $nhex = str_replace(array('-','{','}'), '', $namespace);

 // Binary Value
 $nstr = '';

 // Convert Namespace UUID to bits
 for($i = 0; $i < strlen($nhex); $i+=2) {
   $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
 }

 // Calculate hash value
 $hash = md5($nstr . $name);

 return sprintf('%08s-%04s-%04x-%04x-%12s',

   // 32 bits for "time_low"
   substr($hash, 0, 8),

   // 16 bits for "time_mid"
   substr($hash, 8, 4),

   // 16 bits for "time_hi_and_version",
   // four most significant bits holds version number 3
   (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

   // 16 bits, 8 bits for "clk_seq_hi_res",
   // 8 bits for "clk_seq_low",
   // two most significant bits holds zero and one for variant DCE1.1
   (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

   // 48 bits for "node"
   substr($hash, 20, 12)
 );
}

public static function v4() {
 return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

   // 32 bits for "time_low"
   mt_rand(0, 0xffff), mt_rand(0, 0xffff),

   // 16 bits for "time_mid"
   mt_rand(0, 0xffff),

   // 16 bits for "time_hi_and_version",
   // four most significant bits holds version number 4
   mt_rand(0, 0x0fff) | 0x4000,

   // 16 bits, 8 bits for "clk_seq_hi_res",
   // 8 bits for "clk_seq_low",
   // two most significant bits holds zero and one for variant DCE1.1
   mt_rand(0, 0x3fff) | 0x8000,

   // 48 bits for "node"
   mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
 );
}

public static function v5($namespace, $name) {
 if(!self::is_valid($namespace)) return false;

 // Get hexadecimal components of namespace
 $nhex = str_replace(array('-','{','}'), '', $namespace);

 // Binary Value
 $nstr = '';

 // Convert Namespace UUID to bits
 for($i = 0; $i < strlen($nhex); $i+=2) {
   $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
 }

 // Calculate hash value
 $hash = sha1($nstr . $name);

 return sprintf('%08s-%04s-%04x-%04x-%12s',

   // 32 bits for "time_low"
   substr($hash, 0, 8),

   // 16 bits for "time_mid"
   substr($hash, 8, 4),

   // 16 bits for "time_hi_and_version",
   // four most significant bits holds version number 5
   (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

   // 16 bits, 8 bits for "clk_seq_hi_res",
   // 8 bits for "clk_seq_low",
   // two most significant bits holds zero and one for variant DCE1.1
   (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

   // 48 bits for "node"
   substr($hash, 20, 12)
 );
}

public static function is_valid($uuid) {
 return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                   '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();
//echo $v4uuid; //chave da assinatura
   
}

$query = "SELECT os.customer,MAX(regdate_os_posicionamento.data_end),os.sla_register,os.sla_pm,os.pm,regdate_os_dropzone.file, custom_service.nome,category.nome,equipamento_familia.fabricante,os_sla.sla_verde,os_sla.sla_amarelo,os.sla_term,os_grupo_sla.grupo,os_posicionamento.cor,os_prioridade.prioridade,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo,os.assessment,os.integration,os.id_integration FROM os LEFT JOIN regdate_os_posicionamento ON regdate_os_posicionamento.id_os = os.id LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN os_prioridade on os_prioridade.id = os.id_prioridade LEFT JOIN os_sla on os_sla.id = os.id_sla LEFT JOIN os_grupo_sla on os_grupo_sla.id = os_sla.os_grupo_sla LEFT JOIN custom_service on custom_service.id = os.id_servico LEFT JOIN category on category.id = os.id_category LEFT JOIN regdate_os_dropzone ON regdate_os_dropzone.id_os = os.id WHERE os.id_status like '2' GROUP BY os.id order by os.id DESC,regdate_os_posicionamento.data_end DESC ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($customer,$regdate_os_posicionamento,$sla_register,$sla_pm,$pm,$id_file,$custom_service,$category,$equipamento_fabricante,$sla_verde,$sla_amarelo,$sla_term,$grupo,$color,$prioridade,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo,$assessment,$integration,$id_integration);
 

?>

        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Solicitações <small> de Ordem de Serviço</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Parametro 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Parametro 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">








                        <div class="col-md-12 col-sm-12 ">
                            <div class="x_panel">

                                <div class="x_content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">

                                                <table id="datatable"
                                                    class="table table-striped table-bordered dt-responsive nowrap"
                                                    style="width:100%">
                                                    <thead>

                                                        <tr>
                                                            <th>#</th>
                                                        
                                                            <th>OS</th>
                                                                <th><input type="checkbox" id="check_all"></th>
                                                            <th>Anexo</th>
                                                            <th>1º SLA</th>
                                                            <th>2º SLA</th>
                                                            <th>Data da solicitação</th>
                                                            <th>Prioridade</th>
                                                            <th>Codigo</th>
                                                            <th>Equipamento</th>
                                                            <th>Modelo</th>
                                                            <th>Fabricante</th>
                                                            <th>N/S</th>
                                                              <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                                                              <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                                                              <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  
                                                            <th>Usuario</th>
                                                             <th>Solicitante</th>
                                                            <th>Solicitação</th>


                                                            <th>Posicionamento</th>
                                                            <th>Prazo 1º SLA</th>


                                                            <th>Categoria</th>
                                                            <th>Serviço</th>
                                                            <th>Alerta PM</th>
                                                            <th>Prazo 2º SLA PM</th>
                                                          <th>Avaliação Fornecedor</th>
<th>Integração</th>
<th>OS Integração</th>
                                                          
                                                            <th>Recursos</th>
                                                            <th>Ação</th>

                                                        </tr>
                                                    </thead>


                                                    <tbody>
                                                        <?php  
                                                          $linha = 0;
                                                          while ($stmt->fetch()) {  
                              $row=$row+1;
                                                          $linha++;
                            ?>
                                                        <tr >
                                                            <td style="background-color:<?php printf($color); ?>;color:white"> <?php printf($row); ?> <?php if($pm ==  "0"){ if(strtotime($sla_pm) < strtotime($today_pm)) { ?>
        <span class="badge badge-warning"> Prazo </span> <?php }; }; ?> </td>
                                                                                          

                                                            <td><?php printf($id_os); ?> </td>
    <td > <input type="checkbox" class="checkbox" value="<?php printf($id_os); ?>"> </td>
                                                            <td> <?php if($id_file==!""){?><a
                                                                    href="dropzone/mc-dropzone/<?php printf($id_file); ?>"
                                                                    download><i class="fa fa-paperclip"></i> </a>
                                                                <?php  }  ?><?php if($id_anexo==!""){?><a
                                                                    href="dropzone/os/<?php printf($id_anexo); ?>"
                                                                    download><i class="fa fa-paperclip"></i> </a>
                                                                <?php  }  ?></td>
                                                            <td>



                                                                <?php  


if ($pm == "0"){
    $sla_term_verde=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_verde.' minutes'));  


    $sla_term_amarelo=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_amarelo.' minutes'));  
      
      if($sla_term_verde > $sla_register){ ?>
                                                                    <span class="badge badge-success"> Verde </span> <?php }; 
    
      
       if(($sla_term_verde < $sla_register) && ($sla_term_amarelo > $sla_register )){ ?>
                                                                    <span class="badge badge-warning"> Amarelo </span> <?php }; 
    
       
       if($sla_term_amarelo < $sla_register ){ ?>
                                                                    <span class="badge badge-danger"> Vermelho </span> <?php }; 
    
}else{

    $sla_term_verde=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_verde.' minutes'));  


    $sla_term_amarelo=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_amarelo.' minutes'));  
      
      if($sla_term_verde > $today){ ?>
                                                                    <span class="badge badge-success"> Verde </span> <?php }; 
    
      
       if(($sla_term_verde < $today) && ($sla_term_amarelo > $today )){ ?>
                                                                    <span class="badge badge-warning"> Amarelo </span> <?php }; 
    
       
       if($sla_term_amarelo < $today ){ ?>
                                                                    <span class="badge badge-danger"> Vermelho </span> <?php }; 
    

}
   
  ?>
                                                            </td>
                                                          <td>
                                                            
                                                            
                                                            
                                                            <?php  
                                                              
                                                              
                                                              if ($pm == "0"){
                                                                $sla_term_verde=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_verde.' minutes'));  
                                                                
                                                                
                                                                $sla_term_amarelo=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_amarelo.' minutes'));  
                                                                
                                                              if($regdate_os_posicionamento > $today_pm){ ?>
                                                            <span class="badge badge-success"> Verde </span> <?php }; 
                                                              
                                                              
                                                             ?>
                                                        <?php 
                                                              
                                                              
                                                              if($regdate_os_posicionamento < $today_pm ){ ?>
                                                            <span class="badge badge-danger"> Vermelho </span> <?php }; 
                                                              
                                                              }else{
                                                                
                                                                $sla_term_verde=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_verde.' minutes'));  
                                                                
                                                                
                                                                $sla_term_amarelo=date('Y-m-d H:i:s', strtotime($sla_term.' + '.$sla_amarelo.' minutes'));  
                                                                
                                                              if($sla_term_verde > $today){ ?>
                                                            <span class="badge badge-success"> Verde </span> <?php }; 
                                                              
                                                              
                                                              if(($sla_term_verde < $today) && ($sla_term_amarelo > $today )){ ?>
                                                            <span class="badge badge-warning"> Amarelo </span> <?php }; 
                                                              
                                                              
                                                              if($sla_term_amarelo < $today ){ ?>
                                                            <span class="badge badge-danger"> Vermelho </span> <?php }; 
                                                              
                                                              
                                                              }
                                                              
                                                            ?>
                                                          </td>
                                                            <td><?php printf($reg_date); ?></td>
                                                            <td><?php printf($prioridade); ?></td>
                                                            <td><?php printf($equipamento_codigo); ?> </td>
                                                            <td><?php printf($equipamento_nome); ?> </td>
                                                            <td> <?php printf($equipamento_modelo); ?></td>
                                                            <td> <?php printf($equipamento_fabricante); ?></td>

                                                            <td><?php printf($equipamento_numeroserie); ?> </td>
                                                            <td><?php printf($id_instituicao); ?></td>
                                                            <td><?php printf($id_area); ?></td>

                                                            <td> <?php printf($id_localizacao); ?></td>
                                                            <td><?php printf($id_usuario_nome); ?>
                                                                <?php printf($id_usuario_sobrenome); ?></td>
  															 <td><?php printf($customer); ?></td>
                                                            <td><?php printf($id_solicitacao); ?></td>


                                                            <td><?php printf($id_posicionamento); ?> </td>
                                                            <td><?php printf($sla_term); ?> </td>


                                                            <td><?php printf($category); ?></td>

                                                            <td><?php printf($custom_service); ?></td>

                                                            <td><?php if($pm ==  "0"){ if(strtotime($sla_pm) < strtotime($today_pm)) { ?>
        <span class="badge badge-warning"> Prazo </span> <?php }; }; ?> </td>
                                                            <td><?php printf($sla_pm); ?></td>
<td><?php if($assessment ==  "0"){
  printf("sim"); 
}else {
  printf("Não"); }
                                                            ?></td>
                                                          <td><?php if($integration ==  "0"){
                                                            printf("sim"); 
                                                          }else {
                                                            printf("Não"); }
                                                          ?></td>
                                                          <td><?php printf($idintegration); ?></td>
                                                            <td>
                                                            <a class="btn btn-app"
                                                                    href="os-progress-upgrade-compliance?os=<?php printf($id_os); ?>"
                                                                    onclick="new PNotify({
                                              title: 'Nao Conformidade',
                                              text: 'Nao Conformidade!',
                                              type: 'info',
                                              styling: 'bootstrap3'
                                            });">
                                                                    <i class="fa fa-eject"></i> Não Conformidade
                                                                </a>

                                                                <a class="btn btn-app" data-toggle="modal"
                                                                    data-target=".bs-example-modal-lg<?php printf($id_os); ?>"
                                                                    honclick="new PNotify({
                                                      title: 'Fechamento',
                                                      text: 'Fechamento de O.S!',
                                                      type: 'success',
                                                      styling: 'bootstrap3'
                                                    });">
                                                                    <i class="glyphicon glyphicon-floppy-remove"></i>
                                                                    Fechar
                                                                </a>

                                                                <a class="btn btn-app"
                                                                    href="technological-surveillance-add?os=<?php printf($id_os); ?> "
                                                                    onclick="new PNotify({
                                                      title: 'Tecnovigilancia',
                                                      text: 'Registro Tecnovigilancia!',
                                                      type: 'Warning',
                                                      styling: 'bootstrap3'
                                                    });">
                                                                    <i class="fa fa-warning"></i>Tecnovigilancia
                                                                </a>
                                                                <a class="btn btn-app"
                                                                    href="os-progress-upgrade-out?os=<?php printf($id_os ); ?> "
                                                                    onclick="new PNotify({
                                              title: 'Saida',
                                              text: 'Saida de Equipamento!',
                                              type: 'info',
                                              styling: 'bootstrap3'
                                            });">
                                                                    <i class="fa fa-eject"></i> Saida de equipamento
                                                                </a>

                                                            </td>
                                                            <td>


                                                                <!--         <a class="btn btn-app"  href=""onclick="new PNotify({
																title: 'Fechamento',
																text: 'Fechamento de O.S!',
																type: 'success',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-remove"></i> Fechar
                  </a>  -->
                                                                <a class="btn btn-app"
                                                                    href="os-progress-upgrade?os=<?php printf($id_os); ?>"
                                                                    onclick="new PNotify({
																title: 'Atualização',
																text: 'Atualização de Abertura de O.S!',
																type: 'danger',
																styling: 'bootstrap3'
														});">
                                                                    <i class="glyphicon glyphicon-floppy-open"></i>
                                                                    Atualizar
                                                                </a>

                                                                <!--     <a class="btn btn-app"  href="" onclick="new PNotify({
																title: 'Material',
																text: 'Material',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-shopping-cart"></i> Material
                  </a> -->
                                                                <a class="btn btn-app"
                                                                    href="os-viewer?os=<?php printf($id_os); ?> "
                                                                    target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});">
                                                                    <i class="fa fa-file-pdf-o"></i> Visualizar
                                                                </a>
                                                                <a class="btn btn-app"
                                                                    href="os-opened-close?os=<?php printf($id_os); ?>"
                                                                    onclick="new PNotify({
																title: 'Cancelamento de O.S',
																text: 'Cancelamento de Abertura de O.S',
																type: 'error',
																styling: 'bootstrap3'
														});">
                                                                    <i class="fa fa-ban"></i> Cancelar
                                                                </a>
                                                                <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->

                                                                <a class="btn btn-app" onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/os-trash-backend?id=<?php printf($id_os); ?>';
  }
})
">
                                                                    <i class="fa fa-trash"></i> Excluir
                                                                </a>
                                                              <a class="btn btn-app" onclick="
                                                                Swal.fire({
                                                                  title: 'Tem certeza?',
                                                                  text: 'Você não será capaz de reverter isso!',
                                                                  icon: 'warning',
                                                                  showCancelButton: true,
                                                                  confirmButtonColor: '#3085d6',
                                                                  cancelButtonColor: '#d33',
                                                                  confirmButtonText: 'Sim, Enviar Ordem de Serviço!'
                                                                }).then((result) => {
                                                                  if (result.isConfirmed) {
                                                                    Swal.fire(
                                                                      'Enviando!',
                                                                      'Seu arquivo será enviado.',
                                                                      'success'
                                                                    ),
                                                                    window.location = 'api-assessment-manufacture-send-os-user?id=<?php printf($id_os); ?>&control=1 ';
                                                                  }
                                                                })
                                                              ">
                                                                <i class="fa fa-at"></i> Email
                                                              </a>
                                                                
 <a class="btn btn-app" onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Recalcular!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Calculando!',
      'Sua O.S será recalculada.',
      'success'
    ),
window.location = 'backend/os-sla?id=<?php printf($id_os); ?>';
  }
})
">
                                                                   <i class="fa fa-clock-o"></i> Calcular SLA
                                                                   
                                                                </a>


                                                            </td>
                                                        </tr>
                                                        <div class="modal fade bs-example-modal-lg<?php printf($id_os); ?>"
                                                            tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">

                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel4">
                                                                            Fechamento</h4>
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal"><span
                                                                                aria-hidden="true">×</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <h4>Dados Para Fechamento</h4>
                                                                        <!-- Registro forms-->
                                                                        <form
                                                                            action="backend/os-progress-upgrade-close-table-backend.php?os=<?php printf($id_os);?>"
                                                                            method="post">
                                                                            <div class="ln_solid"></div>
                                                                            <div class="item form-group">
                                                                                <label
                                                                                    class="col-form-label col-md-3 col-sm-3 label-align"
                                                                                    for="os">OS <span
                                                                                        class="required">*</span>
                                                                                </label>
                                                                                <div class="col-md-6 col-sm-6 ">
                                                                                    <input type="text" id="os" name="os"
                                                                                        value="<?php printf($id_os); ?>"
                                                                                        readonly="readonly"
                                                                                        required="required"
                                                                                        class="form-control ">
                                                                                </div>
                                                                            </div>
                                                                            <div class="ln_solid"></div>
                                                                            <div class="item form-group">
                                                                                <label
                                                                                    class="col-form-label col-md-3 col-sm-3 label-align"
                                                                                    for="causa" aria-hidden="true"
                                                                                    class="docs-tooltip"
                                                                                    data-toggle="tooltip"
                                                                                    title="Selecione o Causa ">Causa
                                                                                    <span class="required">*</span>
                                                                                </label>
                                                                                <div class="col-md-6 col-sm-6 ">
                                                                                    <select type="text"
                                                                                        class="form-control has-feedback-right"
                                                                                        name="causa" id="causa"
                                                                                        placeholder="causa">
  <option>Selecione uma opção</option>
                                                                                        <?php
                                                  $sql_cal = "SELECT  id, defeito FROM os_defeito  ";
                                                  if ($stmt_cal = $conn_cal->prepare($sql_cal)) {
                                                    $stmt_cal->execute();
                                                    $stmt_cal->bind_result($id_defeito_cal,$causa);
                                                    while ($stmt_cal->fetch()) {
                                                      ?>
                                                                                        <option
                                                                                            value="<?php printf($id_defeito_cal);?>	">
                                                                                            <?php printf($causa);?>
                                                                                        </option>
                                                                                        <?php
                                                      // tira o resultado da busca da memória
                                                    }

                                                  }
                                                  $stmt_cal->close();

                                                  ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <script>
                                                                            $(document).ready(function() {
                                                                                $('#usuario').select2();
                                                                            });
                                                                            </script>



                                                                            <label for="des">Defeito encontrado
                                                                                :</label>
                                                                            <textarea id="defeito" class="form-control"
                                                                                name="defeito"
                                                                                data-parsley-trigger="keyup"
                                                                                data-parsley-validation-threshold="10"></textarea>

                                                                            <label for="des">Serviço executado :</label>
                                                                            <textarea id="service" class="form-control"
                                                                                name="service"
                                                                                data-parsley-trigger="keyup"
                                                                                data-parsley-validation-threshold="10"></textarea>

                                                                            <div class="ln_solid"></div>
                                                                            <div class="field item form-group">

                                                                                <div class="col-md-6 col-sm-6">
                                                                                    <a class="btn btn-app"
                                                                                        href="technological-surveillance-add?os=<?php printf($id_os); ?> "
                                                                                        onclick="new PNotify({
                                                      title: 'Tecnovigilancia',
                                                      text: 'Registro Tecnovigilancia!',
                                                      type: 'Warning',
                                                      styling: 'bootstrap3'
                                                    });">
                                                                                        <i
                                                                                            class="fa fa-warning"></i>Tecnovigilancia
                                                                                    </a>
                                                                                </div>
                                                                            </div>








                                                                            <div class="field item form-group">
                                                                                <label
                                                                                    class="col-form-label col-md-3 col-sm-3  label-align">Date
                                                                                    Inicial<span
                                                                                        class="required">*</span></label>
                                                                                <div class="col-md-6 col-sm-6">
                                                                                    <input class="form-control"
                                                                                        class='date' type="date"
                                                                                        name="date_start"
                                                                                        required='required'>
                                                                                </div>
                                                                            </div>

                                                                            <div class="field item form-group">
                                                                                <label
                                                                                    class="col-form-label col-md-3 col-sm-3  label-align">Date
                                                                                    Final<span
                                                                                        class="required">*</span></label>
                                                                                <div class="col-md-6 col-sm-6">
                                                                                    <input class="form-control"
                                                                                        class='date' type="date"
                                                                                        name="date_end"
                                                                                        required='required'>
                                                                                </div>
                                                                            </div>

                                                                            <div class="item form-group">
                                                                                <label
                                                                                    class="col-form-label col-md-3 col-sm-3 label-align"
                                                                                    for="time" class="docs-tooltip"
                                                                                    data-toggle="tooltip"
                                                                                    title="Tempo">Tempo <span></span>
                                                                                </label>
                                                                                <div class="col-md-6 col-sm-6 ">
                                                                                    <input type="text" id="time"
                                                                                        name="time"
                                                                                        class="form-control "
                                                                                        pattern="\d{3}\-\d{3}\-\d{4}"
                                                                                        class="form-control telephone"
                                                                                        data-mask="99:99">
                                                                                </div>
                                                                            </div>
                                                                            <?php    if($os_rasch_single == "0"){ ?>
                                                                                <center>
                                                                            <div class="modal-body">
                                                                                <h4>Assinatura digital</h4>
                                                                                <p>Para Assinar digite o texto abaixo.
                                                                                </p>
                                                                                <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
                                                                                <p>
                                                                                <div class="item form-group">
                                                                                    <label
                                                                                        class="col-form-label col-md-3 col-sm-3 label-align"
                                                                                        for="assinature"> <span></span>
                                                                                    </label>
                                                                                    <div class="col-md-6 col-sm-6 ">
                                                                                        <input type="text"
                                                                                          id="assinatura_<?php echo $linha; ?>"
                                                                                            name="assinature"
                                                                                            class="form-control "
                                                                                            class="docs-tooltip"
                                                                                            data-toggle="tooltip"
                                                                                            title=" ">

                                                                                    </div>
                                                                                    <div class="form-group row">
                                                                                        <label
                                                                                            class="col-form-label col-md-3 col-sm-3 ">
                                                                                        </label>
                                                                                        <div class="col-md-9 col-sm-9 ">
                                                                                            <input type="hidden"
                                                                                                class="form-control"
                                                                                                value=" <?php  printf($v4uuid);?>"
                                                                                                id="v4uuid_<?php echo $linha; ?>"
                                                                                                name="v4uuid">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group row">
                                                                                        <label
                                                                                            class="col-form-label col-md-3 col-sm-3 ">
                                                                                        </label>
                                                                                        <div class="col-md-9 col-sm-9 ">
                                                                                            <input type="hidden"
                                                                                                class="form-control"
                                                                                                value=" <?php  printf($chave);?>"
                                                                                                id="chave_<?php echo $linha; ?>" name="chave">
                                                                                        </div>
                                                                                    </div>
                                                                                                                                                        </div>
                                                                                </p>
                                                                            </div>
                                                                            </center>
                                                                        
                                                                            <?php     } ?>



                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">Fechar</button>
                                                                        <button type="submit"
                                                                            class="btn btn-primary">Salvar
                                                                            Informações</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        </form>
                                                        <!-- Fechamen    to -->

                                                        <!-- compose -->

                                                        <script type="text/javascript">
                                                        $(document).ready(function() {
                                                            $('#fornecedor').change(function() {
                                                                $('#fornecimentoid').load(
                                                                    'api_assessment.php?id_fornecedor=' +
                                                                    $('#fornecedor').val());
                                                            });
                                                        });
                                                        </script>

                                                        <?php   } }  ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

                                                      <script>
                                                        // Iterar sobre cada linha da tabela
                                                        for (var i = 0; i < <?php echo $linha; ?>; i++) {
                                                          // Selecionar elementos de cada linha com base no identificador único
                                                          var assinaturaInput = document.getElementById("assinatura_" + i);
                                                          var chave = document.getElementById("chave_" + i).value;
                                                          
                                                          // Adicionar evento de validação para cada campo de assinatura
                                                          assinaturaInput.addEventListener("blur", function () {
                                                            var assinatura = this.value;
                                                            var linha = this.id.split("_")[1]; // Obter o número da linha a partir do ID
                                                            
                                                            // Comparar a assinatura com a chave da linha correspondente
                                                            if (assinatura !== chave) {
                                                              Swal.fire('A assinatura está incorreta na linha ' + linha + '!');
                                                            } else {
                                                              // Aqui você pode realizar ações caso a assinatura seja correta
                                                            }
                                                          });
                                                        }
                                                      
                                                      </script>   

<script type="text/javascript">
$(document).ready(function() {
  $('#check_all').click(function() {
          $('input[type=checkbox]').prop('checked', this.checked); // Seleciona todas as checkboxes
        });
    $('#datatable').DataTable({
        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    },
      
 initComplete: function () {
            this.api()
             .columns([1, 4, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 18, 19, 20, 21])
                .every(function (d) {
                   

                    var column = this;
                  var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      }); 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                
                
        },
    });
    
 });

 function takeAction() {
        var selectedRows = [];
        $('.checkbox:checked').each(function() {
          selectedRows.push($(this).val());
        });
        
        // Verifica se pelo menos um item foi selecionado
        if (selectedRows.length > 0) {
          // Constrói o URL com os IDs dos itens selecionados
          var url = 'os-viewer-selected?id=' + selectedRows.join(',');
          // Abre o URL em uma nova aba
          window.open(url, '_blank');
         // var url = 'maintenance-preventive-open-tag-selected?id=' + selectedRows.join(',');
          // Abre o URL em uma nova aba
        //  window.open(url, '_blank');
        } else {
          // Se nenhum item foi selecionado, mostra uma mensagem
          alert('Por favor, selecione pelo menos um item.');
        }
      }
     function closeAction() {
  var selectedRows = [];
  $('.checkbox:checked').each(function() {
    selectedRows.push($(this).val());
  });

  // Verifica se pelo menos um item foi selecionado
  if (selectedRows.length > 0) {
    // Constrói o URL com os IDs dos itens selecionados
    var url = 'os-close-selected?id=' + selectedRows.join(',');
    // Abre o URL em uma nova aba
    window.open(url);

    // Recarrega a página atual
    location.reload();
  } else {
    // Se nenhum item foi selecionado, mostra uma mensagem
    alert('Por favor, selecione pelo menos um item.');
  }
}

</script>

                                                         