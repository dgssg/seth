  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Abertura de <small>Solicitação</small></h3>
              </div>

           
            </div>
       <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="purchases-acquisition">
                    <i class="fa fa-cart-plus"></i> Solicitação OS
                  </a>
                  
                  <a class="btn btn-app"  href="purchases-acquisition-single">
                    <i class="fa fa-shopping-bag"></i> Solicitação
                  </a>
                    <a class="btn btn-app"  href="purchases-acquisition-wizard">
                    <i class="fa fa fa-shopping-basket"></i> Solicitação OS Digital
                  </a>
                  <a class="btn btn-app"  href="purchases-acquisition-wizard-single">
                    <i class="fa fa-suitcase"></i> Solicitação Sem OS Digital
                  </a>
               
               
                  
                </div>
              </div>
              
              
              
              
        
              
                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Solicitação de Compra  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                <?php include 'frontend/purchases-acquisition-register-single-frontend.php';?>
                  
                </div>
              </div> 
	       
	        
                  
                  
                </div>
              </div>
              
    <script type="text/javascript">
    $(document).ready(function(){
      $('#instituicao').change(function(){
        $('#area').select2().load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#setor').select2().load('sub_categorias_post_setor.php?area='+$('#area').val());
      });
    });
    </script>
    