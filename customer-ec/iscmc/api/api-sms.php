
// Update the path below to your autoload.php, 
// see https://getcomposer.org/doc/01-basic-usage.md 
require_once '/path/to/vendor/autoload.php'; 
 
use Twilio\Rest\Client; 
 
$sid    = "AC62155517daedd8ca9ad15bb41a57502e"; 
$token  = "[AuthToken]"; 
$twilio = new Client($sid, $token); 
 
$message = $twilio->messages 
                  ->create("+5541998945903", // to 
                           array(        
                               "body" => "Sistema SETH" 
                           ) 
                  ); 
 
print($message->sid);