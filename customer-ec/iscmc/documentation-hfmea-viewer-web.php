<?php
     $codigoget = ($_GET["id"]);

	include_once("database/database.php");
	date_default_timezone_set('America/Sao_Paulo');

  $query = "SELECT assistence FROM tools";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($assistence);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }



$print= date(DATE_RFC2822);


$query = "SELECT documentation_hfmea.id_unidade,documentation_hfmea.id_setor,documentation_hfmea.class,equipamento_grupo.nome, documentation_hfmea.id, documentation_hfmea.id_equipamento_grupo, documentation_hfmea.titulo, documentation_hfmea.ver, documentation_hfmea.file, documentation_hfmea.data_now, documentation_hfmea.data_before, documentation_hfmea.upgrade, documentation_hfmea.reg_date FROM documentation_hfmea  INNER JOIN equipamento_grupo ON equipamento_grupo.id = documentation_hfmea.id_equipamento_grupo WHERE documentation_hfmea.id = $codigoget ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id_unidade,$id_setor,$class,$nome,$id, $id_equipamento_grupo,$titulo,$ver,$file,$data_now,$data_before,$upgrade,$reg_date);


   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}

//border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:100%;border-collapse:collapse;

  $sql = "SELECT  instituicao,logo,endereco,cep,bairro,cidade,estado,logo,cnpj FROM instituicao WHERE id = $id_unidade ";
  
  
  if ($stmt = $conn->prepare($sql)) {
    $stmt->execute();
    $stmt->bind_result($unidade,$logo,$rua,$cep,$bairro,$cidade,$estado,$logo,$cnpj);
    while ($stmt->fetch()) {
    }
  }
  
  $sql = "SELECT  nome FROM instituicao_area WHERE id = $id_setor ";
  
  
  if ($stmt = $conn->prepare($sql)) {
    $stmt->execute();
    $stmt->bind_result($setor);
    while ($stmt->fetch()) {
    }
  }
  



?>


<style>
.

* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Style the header */
.header {
  background-color: #f1f1f1;
  padding: 30px;
  text-align: center;
  font-size: 35px;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 33.33%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

.footer {
  position: fixed;
  left: 10;
  bottom: 10;
  width: 100%;

  color: black;
  text-align: center;
}



/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
  .column {
    width: 100%;
  }
}


</style>

<style>

    /*-----------------------------------------------
  /*    COMUM
  ------------------------------------------------*/
    * {
      margin: 0;
      padding: 0;
      border: 0;
    }

    html {
      line-height: 1.15;
      /* 1 */
      -ms-text-size-adjust: 100%;
      /* 2 */
      -webkit-text-size-adjust: 100%;
      /* 2 */
    }

    body {
      margin: 0;
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      color: #333;
    }

    article,
    aside,
    footer,
    header,
    nav,
    section,
    figure,
    main {
      display: block;
    }

    h1 {
      font-size: 1.8em;
      margin: 0;
    }

    a {
      background-color: transparent;
      /* 1 */
      -webkit-text-decoration-skip: objects;
      /* 2 */
    }

    b,
    span {
      font-weight: 500 !important;
    }

    small {
      font-size: 80%;
    }

    sub,
    sup {
      font-size: 75%;
      line-height: 0;
      position: relative;
      vertical-align: baseline;
    }

    sub {
      bottom: -0.25em;
    }

    sup {
      top: -0.5em;
    }

    img {
      border-style: none;
    }

    .wraper {
      width: 26.7cm;
      /* height: 18cm; */
      margin: 0 auto;
    }

    .uppercase {
      text-transform: uppercase
    }

    .small-font {
      font-size: .6em
    }

    .weight-normal {
      font-weight: normal
    }

    .weight-bold {
      font-weight: bold !important;
    }

    .text-left {
      text-align: left;
    }

    .text-right {
      text-align: right;
    }

    .width-10-percent {
      width: 10%;
    }

    .width-15-percent {
      width: 15%;
    }

    .width-20-percent {
      width: 20%;
    }

    .width-25-percent {
      width: 25%;
    }

    .width-30-percent {
      width: 30%;
    }

    .width-35-percent {
      width: 35%;
    }

    .width-40-percent {
      width: 40%;
    }

    .width-45-percent {
      width: 45%;
    }

    .width-50-percent {
      width: 50%;
    }

    .width-55-percent {
      width: 55%;
    }

    .width-60-percent {
      width: 60%;
    }

    .width-65-percent {
      width: 65%;
    }

    .width-100-percent {
      width: 100%;
    }

    .base-background {
      background: #e5efea;
    }

    .atv-principal {
      padding-top: 10px !important;
    }

    @media print {
      .break-page {
        page-break-before: always;
      }
    }
    @page {
        counter-increment: page;
        counter-reset: page 1;
        @top-right {
            content: "Page " counter(page) " of " counter(pages);
        }
    }
    @page:right{
  @bottom-right {
    content: counter(page);
  }
}

@page:left{
  @bottom-left {
    content: counter(page);
  }
}
@page:left{
  @bottom-left {
    content: "Page " counter(page) " of " counter(pages);
  }
}
body {
  /* Set "my-sec-counter" to 0 */
  counter-reset: my-sec-counter;
}

h2::before {
  /* Increment "my-sec-counter" by 1 */
  counter-increment: my-sec-counter;
  content: "Section " counter(my-sec-counter) ". ";
}

#page-number::before {
  /* Increment "my-sec-counter" by 1 */
  counter-increment: my-sec-counter;
  content: "Page " counter(my-sec-counter) ". ";
}
    /*-----------------------------------------------
  /*    HEADER
  ------------------------------------------------*/

    .header-logo {
      width: 30%;
      background: #FFFFFF !important;
      border: 0 !important;
      margin: 0 !important;
    }

    #header-logo {
      height: 12em !important;
      border: 0 !important;
    }

    .header-content-block {
      color: #000;
      width: 100%;
      padding: 0 1em;
    }

    .header-content-block-title {
      font-size: 2em !important;
      white-space: nowrap;
    }

    .header-content-block-col1 {
      text-transform: uppercase;
      white-space: nowrap;
    }

    .header-content-block-col2 {
      text-align: right;
      border: 0 !important;
      white-space: nowrap;
    }

    .header table {
      width: 100%;
    }

    /*-----------------------------------------------
  /*    MAIN
  ------------------------------------------------*/
    .table-documento td {
      border: 0 !important;
    }

    .content-block {
      padding: 1em;
      margin-top: .5em;
      font-size: 12px;
      background: #f9fafa;
      border: .1em solid #bebebe
    }

    .content-block-title {
      top: -1em;
      position: relative;
      margin: 0 auto;
      padding: .2em 0;
      width: 33em;
      background: #6c6b6c;
      color: #fff;
      text-transform: uppercase;
      text-align: center;
      font-weight: normal;
      font-size: 17px;
    }

    .lista-atividades-aviso {
      margin-bottom: 2em;
      width: 100%;
      background: #e5e8e7;
    }

    .lista-atividades-aviso-col1 {
      width: 25%
    }

    .lista-atividades-aviso-col2 b {
      padding-left: 1.5em;
    }

    .lista-atividades {
      width: 100%;
      font-size: 1em;
    }

    .lista-atividades thead th {
      border-bottom: .1em solid #333
    }

    .lista-atividades thead th:first-child {
      border-right: .1em solid #333;
      text-align: center !important;
    }

    .lista-atividades thead th:last-child {
      text-align: center !important;
    }

    .lista-atividades thead th,
    .lista-atividades tbody td {
      padding: 0 0 .5em .7em;
    }

    .lista-atividades thead th,
    .lista-atividades tbody td {
      padding: 0 0 .5em .7em;
    }

    /*-----------------------------------------------
  /*    FOOTER
  ------------------------------------------------*/
    .footer {
      margin-top: 0.0em;

    }

    .footer table {
      width: 100%
    }

    .footer-info {
      width: 1300px !important;
      padding: 10px 10px;
      height: 100px !important;
      color: #000;
    }

    .observacoes-lista {
      padding: 10px;
      font-size: 13px;
      word-break: break-word;
    }

    .footer-info h5 {
      padding-bottom: 0px;
    }

    .observacoes-lista li {
      list-style-type: none;
      padding-right: 1em;
    }

    .observacoes-lista li::before {
      display: inline-block;
      padding-right: .0em;
      font-size: 10px;
      content: "•";
    }

    .obsevacoes-link {
      color: #fff;
      font-weight: bold
    }

    .qr-code {
      margin-left: 1em;
      vertical-align: top
    }

    .p-bottom-190 {
      padding-bottom: 8.5em !important;
    }

    .p-bottom-130 {
      padding-bottom: 4em !important;
    }

    .p-bottom-40 {
      padding-bottom: 2em !important;
    }

    .p-top-50 {
      padding-top: 4em !important;
    }

    .p-left-30 {
      padding-left: 30px;
    }

    .table-veiculos {
      border-collapse: collapse;
      width: 100%;
      font-size: 13px;
      margin-bottom: 45px;
    }


    .table-veiculos tr,
    .table-veiculos td,
    .table-veiculos th {
      border: solid 1px #000 !important;
    }

    .table-veiculos,
    .table-veiculos th,
    .table-veiculos td {
      word-break: break-all;
      text-align: center;
      padding: 5px;
    }

    .tv-title-carga {
      width: 175px;
    }
    img {
  border: 3;
}
.div-top {
  float: center;
  width: 900px;
  margin: auto;
}
.div-top img {
  margin-top: 10px;
}
.escola_logo {
  float: left;
}
.academus_logo {
  float: right;
}

.left-side-text {
  float: left;
  max-width: 300px;
  margin-left: 10px;
}
.left-side-text * {
  width: 100%;
  float: left;
}
.right-side-text {
  float: right;
  max-width: 300px;
  margin-right: 10px;
  text-align: right;
}
.right-side-text * {
  float: right;
  width: 100%;
}


h4 {
  color: rgb(127, 127, 127);
  font-size: 14px;
}
h5 {
  color: rgb(191, 191, 191);
  font-size: 10px;
}
.titulo-relatorio {
  text-align: center;
  font-size: 20px;
  font-family: sans-serif;
  font-weight: lighter;
}
.box-externa {
  float: center;
  border: 1px solid rgb(217, 217, 217);
  width: 900px;
  margin: auto;
}
.box-interna {
  border: 1px solid rgb(166, 166, 166);
  width: 850px;
  margin: 15px auto 0 auto;
  float: center;
  padding: 10px;
}
.box-interna:last-of-type {
  margin-bottom: 15px;
}
.box-grafico {
  width: 850px;
  margin: 15px auto 0 auto;
  float: center;
  padding: 10px;
}
body {
  font-family: open sans;
}
p {
  margin: 8px 0 8px 0;
}

.arrow-down {
  width: 0;
  height: 0;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-top: 10px solid gray;
  margin: auto;
}
.grey-box {
  background-color: gray;
  width: 400px;
  margin: auto;
}
.indicador-titulo {
  margin-bottom: 0;
  margin-top: 10px;
}
.indicador-valores {
  float: left;
  width: 800px;
  margin-left: 25px;
}
table-header-rotated {
  border-collapse: collapse;
  .csstransforms & td {
    width: 30px;
  }
  .no-csstransforms & th {
    padding: 5px 10px;
  }
  td {
    text-align: center;
    padding: 10px 5px;
    border: 1px solid #ccc;
  }
  .csstransforms & th.rotate {
    height: 140px;
    white-space: nowrap;
    // Firefox needs the extra DIV for some reason, otherwise the text disappears if you rotate
    > div {
      transform:
        // Magic Numbers
        translate(25px, 51px)
        // 45 is really 360-45
        rotate(315deg);
      width: 30px;
    }
    > div > span {
      border-bottom: 1px solid #ccc;
      padding: 5px 10px;
    }
  }
  th.row-header {
    padding: 0 10px;
    border-bottom: 1px solid #ccc;
  }
}
td {
    border: 1px black solid;
    padding: 5px;
}
.rotate {
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  width: 1.5em;
}
.rotate div {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
         margin-left: -10em;
         margin-right: -10em;
}
  </style>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style>
    body {
    font-size: 12pt;
    font-family: Calibri;
    padding : 10px;
}
table {
    border: 1px solid black;
}
th {
    border: 1px solid black;
    padding: 5px;
    background-color:grey;
    color: white;
}
td {
    border: 1px solid black;
    padding: 5px;
}
input {
    font-size: 12pt;
    font-family: Calibri;
}
</style>
  </head>
  <body>

                  <a  class="btn btn-secondary btn-xs"  id="btnExport"> Excel
                    
                  </a>
                  <script type="text/javascript" language="JavaScript">

function printPage() {

  if (window.print) {

    agree = confirm("Deseja imprimir essa pagina ?");

    if (agree) {
      window.print();

      if (_GET("pg") != null)
      location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
      else
      history.go(-1);


      window.close();
    }



  }
}

function noPrint() {
  try {

    if (_GET("pg") != null)
    location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
    else
    history.go(-1);

    window.close();

  }
  catch (e) {
    alert();
    document.write("Error Message: " + e.message);
  }
}

function _GET(name) {
  var url = window.location.search.replace("?", "");
  var itens = url.split("&");

  for (n in itens) {
    if (itens[n].match(name)) {
      return decodeURIComponent(itens[n].replace(name + "=", ""));
    }
  }
  return null;
}





function visualizarImpressao() {

  var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

  document.body.insertAdjacentHTML("beforeEnd", Navegador);

  Navegador1.ExecWB(7, 1);

  Navegador1.outerHTML = "";

}



(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
    (i[r].q = i[r].q || []).push(arguments)
  }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-11247563-6', 'auto');
ga('send', 'pageview');

// -->
</script>

<style type="text/css">
page {
  /* background: white;*/
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  text-align: left;
}

page[size="A4"] {
  width: 21cm;
  /*height: 29.7cm;                */
}

page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /* height: 21cm; */
}

@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
    page-break-after:always;
  }

  .noprint {
    display: none;
  }
}




body {
  font-size: 11px;
  font-family: sans-serif;
}



.pagina_retrato {
  width: 720px;
}

.pagina_paisagem {
  width: 1025px;
}

.relatorio_filtro {
  padding: 10px 0px;
  border-collapse: collapse;
}

.relatorio_filtro tr td {
  border: 1px solid #000;
}


.relatorio_titulo {
  font-family: Times New Roman;
  font-size: 15px;
  color: Black;
  font-weight: bold;
  text-transform: uppercase;
}

.borda_celula {
  border: 1px solid #000;
  background-color: #fff;
}

.noprint {
  padding-bottom: 10px;
}
.right{
  float:right;
}

.left{
  float:left;
}
</style>

<style type="text/css" media="print">
/*
@page {
margin: 0.8cm;
}
*/
.pagina_retrato, .pagina_paisagem {
width: 100%;
}
 
</style>

<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script>
window.onload = function () {
JsBarcode(".barcode").init();
}
</script>

   <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <style>
    /* Estilos para o cabeçalho */
    header {
      padding: 20px;
      background-color: #f5f5f5;
      border-bottom: 1px solid #ddd;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      padding: 5px;
      border: 1px solid #ddd;
    } 

    #logo {
      width: 100px;
    }

    h1 {
      margin: 0;
      font-size: 24px;
    }

    #info {
      font-size: 14px;
    }
  </style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />
<link href="App_Themes/Tema1/StyleSheet.css" type="text/css" rel="stylesheet" />

<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEwODA5NzUyMjMPZBYCZg9kFgICAw9kFgICAQ9kFggCAw8PFgIeCEltYWdlVXJsBUd+L2ltYWdlcy9ob3NwaXRhaXMvaV9jcnV6dmVybWVsaGFicmFzaWxlaXJhLWZpbGlhbGRvZXN0YWRvZG9wYXJhbsOhLnBuZ2RkAgUPDxYCHgRUZXh0BQcwMS8yMDE5ZGQCCw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIeCkhlYWRlclRleHQFBzAxLzIwMTg8KwAFAQAWAh8EBQcwMi8yMDE4PCsABQEAFgIfBAUHMDMvMjAxODwrAAUBABYCHwQFBzA0LzIwMTg8KwAFAQAWAh8EBQcwNS8yMDE4PCsABQEAFgIfBAUHMDYvMjAxODwrAAUBABYCHwQFBzA3LzIwMTg8KwAFAQAWAh8EBQcwOC8yMDE4PCsABQEAFgIfBAUHMDkvMjAxODwrAAUBABYCHwQFBzEwLzIwMTg8KwAFAQAWAh8EBQcxMS8yMDE4PCsABQEAFgIfBAUHMTIvMjAxODwrAAUBABYCHwQFBzAxLzIwMTkWDQIGAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGDBQrAABkAg8PPCsAEQMADxYEHwJnHwNmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIfBAUHMDEvMjAxODwrAAUBABYCHwQFBzAyLzIwMTg8KwAFAQAWAh8EBQcwMy8yMDE4PCsABQEAFgIfBAUHMDQvMjAxODwrAAUBABYCHwQFBzA1LzIwMTg8KwAFAQAWAh8EBQcwNi8yMDE4PCsABQEAFgIfBAUHMDcvMjAxODwrAAUBABYCHwQFBzA4LzIwMTg8KwAFAQAWAh8EBQcwOS8yMDE4PCsABQEAFgIfBAUHMTAvMjAxODwrAAUBABYCHwQFBzExLzIwMTg8KwAFAQAWAh8EBQcxMi8yMDE4PCsABQEAFgIfBAUHMDEvMjAxORYNAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGAgYMFCsAAGQYAgUeY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSRncnYyDzwrAAwBCGZkBR5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJGdydjEPPCsADAEIZmS9tcluC/wCTfFh0PbdSxSO8nxQJa5LJqUDKFT3sJzurQ==" />
</div>

<div>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3164E9EE" />
</div>

<div class="noprint">
<center>
  <input type="button" value="Não Imprimir" onclick="noPrint();">
  <input type="button" value="Imprimir" onclick="printPage();" >
  <input type="button" onclick="visualizarImpressao();" value="Visualizar Impressão" >
</center>
</div>

  <header>
    <table>
      <tr>
      <td><img src="dropzone/logo/<?php printf($logo)?>" alt="Logo da empresa" id="logo"></td>
        <td>
          <center>
          <h1>Análise do modo e efeito de falha na assistência médica</h1>

          <p>Data da impressão: <?php printf($today); ?> <small>Sistema SETH</small></p>
          </center>
        </td>
        <td><img src="logo/clientelogo.png" alt="Logo da empresa" id="logo"></td>
      </tr>
      <tr>
        <td colspan="3" id="info">
          <table>
            <tr>
              <td>Unidade:</td>
              <td><?php printf($unidade); ?></td>
              <td>Área:</td>
              <td><?php printf($area); ?></td>
              <td>Setor:</td>
              <td><?php printf($setor); ?></td>
            </tr>
            <tr>
              <td>CEP:</td>
              <td><?php printf($cep); ?></td>
              <td>Rua:</td>
              <td><?php printf($rua); ?></td>
              <td>Bairro:</td>
              <td><?php printf($bairro); ?></td>
            </tr>
            <tr>
              <td>Cidade:</td>
              <td><?php printf($cidade); ?></td>
              <td>Estado:</td>
              <td><?php printf($estado); ?></td>
              <td>CNPJ:</td>
              <td><?php printf($cnpj); ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </header>
  <div class="wraper">
    
  
 <!-- Início do Cabeçalho -->
  <div class="div-top">


    <div>


  <div style="clear: both;"></div>
  <hr style="border-top: dashed 1px; width: 900px; color: gray; clear: both;">
  <!-- Fim do Cabeçalho -->
  <!-- Início do Título -->
   <br>
  <center>
  <h2 class="titulo-relatorio">Modo de falha de assistência médica e análise de efeito Nº<?PHP PRINTF($codigoget); ?></h2>
  </center>
  <br>
  <!-- Fim do Título -->
    <main>
      <div class="container">
        <section class="content-block">

          <header>
            <h3 class="content-block-title">HFMEA</h3>
          </header>

          <table class="table-documento">
            <tbody>
              <tr>
                <td >
                  Titulo:
                  <span class="uppercase weight-bold"><?PHP PRINTF($titulo); ?></span>
                </td>

                <td class="p-left-30">
                  Grupo:
                  <span class="uppercase weight-bold"><?PHP PRINTF($nome); ?></span>
                </td>

                <td class="p-left-30">
                  Versão:
                  <span class="uppercase weight-bold"><?PHP PRINTF($ver); ?></span>
                </td>
              </tr>


              <tr>
                <td>
                  Data Revisão:
                  <span class="uppercase weight-bold"><?PHP PRINTF($data_now); ?> </span>
                </td>

                <td class="p-left-30">
                  Data proxima revisão:
                  <span class="uppercase weight-bold"><?PHP PRINTF($data_before); ?></span>
                </td>

                <td class="p-left-30">
                  Atualização:
                  <span class="uppercase weight-bold"><?PHP PRINTF($upgrade); ?></span>
                </td>
              </tr>

              <tr>
                <td>
                  Registro:
                  <span class="uppercase weight-bold"><?PHP PRINTF($reg_date); ?></span>
                </td>
                <td>
                Classificação da Falha:
                  <span class="uppercase weight-bold"><?PHP PRINTF($class); ?></span>
                </td>




              </tr>

              <tr>



              </tr>
            </tbody>
          </table>
        </section>
        <!--./content-block-->
<br>




        <section class="content-block">
          <header>
            <h3 class="content-block-title">MODOS DE FALHAS</h3>
          </header>

<table  class="table table-header-rotated"   border="1"  width="100%" height="100px" >
	<thead>
	<tr>
  
	<th>Falhas</th>
	<th>Causas</th>
  <th>Consequencia</th>
  <th>Prevenção</th>
  <th>Controle</th>
  <th class="rotate" height="100px"><div ><span><small>Sentinela</small></span></div></th>

	<th class="rotate" height="100px"><div ><span><small>Probabilidade</small></span></div></th>
	<th class="rotate" height="100px"><div ><span><small>Gravidade</small></span></div></th>
	<th class="rotate" height="100px"><div ><span><small>Abrangencia</small></span></div></th>
  <th class="rotate" height="100px"><div ><span><small>Situação atual</small></span></div></th>
  <th class="rotate" height="100px"><div ><span><small>Total</small></span></div></th>
  <th class="rotate" height="100px"><div ><span><small>Grau de Importância</small></span></div></th>

  <th>Contingência</th>
  <?PHP $row=1; ?>

	</tr>
	</thead>
	<tbody>
    <?PHP	$result_transacoes2 = "SELECT * FROM documentation_hfmea_itens   WHERE documentation_hfmea_itens.id_documentation_hfmea like '$codigoget' order by documentation_hfmea_itens.id ASC  ";
	$resultado_trasacoes2 = mysqli_query($conn, $result_transacoes2);
	while($row_transacoes2 = mysqli_fetch_assoc($resultado_trasacoes2)){ ?>
   
		<tr><td><small><?php printf($row_transacoes2['risco']); ?></small></td>

		<td><small><?php printf($row_transacoes2['causa']); ?></small></td>
    <td><small><?php printf($row_transacoes2['efeito']); ?></small></td>
    <td><small><?php printf($row_transacoes2['preven']); ?></small></td>
    <td><small><?php printf($row_transacoes2['controle']); ?></small></td>
    <th class="rotate" height="100px"><div><span><?php printf($row_transacoes2['sentinela']); ?></span></div></th>

	    <th class="rotate" height="100px"><div><span><?php printf($row_transacoes2['probabilidade']); ?></span></div></th>
		<th class="rotate" height="100px"><div><span><?php printf($row_transacoes2['gravidade']); ?></span></div></th>
		<th class="rotate" height="100px"><div><span><?php printf($row_transacoes2['abrangen']); ?></span></div></th>
    <th class="rotate" height="100px"><div><span><?php printf($row_transacoes2['situacao']); ?></span></div></th>
    <th class="rotate" height="100px"><div><span><?php printf($row_transacoes2['total']); ?></span></div></th>
<th class="rotate" height="100px" <?php if($row_transacoes2['total'] <= 6){ ?> style="background-color: #00b300; color: black;" <?php } ?>
<?php if($row_transacoes2['total'] > 6 && $row_transacoes2['total'] < 8){ ?> style="background-color: #f2ff00; color: black;" <?php } ?>
<?php if($row_transacoes2['total'] > 8){ ?> style="background-color: #ff0000; color: black;" <?php } ?>>
<div><span><?php printf($row_transacoes2['grau']); ?></span></div>
</th>


    <td><small><?php printf($row_transacoes2['conti']); ?> </small></td></tr>
    <?PHP  $row=$row+1;
	} ?>


	</tbody>
	</table>
          

        </section>




        <br>
        <section class="content-block">
        <header>
          <h4 class="content-block-title">legenda</h4>
        </header>
        <table class="table-documento lista-atividades">
          <thead>
            <tr>
              <th class="width-20-percent text-left" style="background-color: #ff0000; color: black; " >RISCO ALTO	: 8 a 12 pontos</th>
                 <th class="width-20-percent text-left" style="background-color: #f2ff00; color: black;">RISCO MEDIO	: 7 pontos</th>
                 <th class="width-20-percent text-left" style="background-color: #00b300; color: black;">RISCO BAIXO	: 0 a 6 pontos</th>

            </tr>
          </thead>
          <tbody>
            <tr class="width-percent">
              <td colspan="3" class="atv-principal">* O risco  alto deverá ser monitorado por indicador</td>
            </tr>
          </tbody>
         

          <tr>
         
          </tr>
        </table>



      </section>
<br>
<section class="content-block">
<header>
<h4 class="content-block-title">Assinatura</h4>
</header>
<?php
    $query="SELECT regdate_hfmea_signature.id_hfmea,regdate_hfmea_signature.id_user,regdate_hfmea_signature.id_signature,regdate_hfmea_signature.reg_date,usuario.nome FROM regdate_hfmea_signature INNER JOIN usuario ON regdate_hfmea_signature.id_user = usuario.id   WHERE regdate_hfmea_signature.id_hfmea like '$codigoget'";
    $row=1;
    if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($id_mp,$id_user,$id_signature,$reg_date,$id_user);
        
    ?>
    <table class="table-documento lista-atividades">
<thead>
<tr>
<th>#</th>

<th>Usuário</th>
<th>Assinatura</th>
<th>Registro</th>

</tr>
</thead>
<tbody>
<?php  while ($stmt->fetch()) { ?>

<tr>
<th scope="row"><?php printf($row); ?></th>
<td><?php printf($id_user); ?></td>
<td><?php printf($id_signature); ?></td>
<td><?php printf($reg_date); ?></td>



</tr>
<?php  $row=$row+1; }
    }   ?>
</tbody>
</table>
</section>

        <section class="content-block">


                 <div class="center-side">

                   <div class="center-side-text">


                       <small>HFMEA Nº<?PHP PRINTF($codigoget); ?></small>
                     
                     <br>

                   </div>
                 </div>

       </section>

        <!--./content-block-->
      </div>
      <!--/.container-->
    </main>

    

  </div>


 
