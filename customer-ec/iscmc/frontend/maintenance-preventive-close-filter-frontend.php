  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Manutenção <small>Preventiva</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>





            <div class="clearfix"></div>




              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

              
                                                 
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                  <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive' ? 'active' : ''; ?>" href="maintenance-preventive">
             <i class="glyphicon glyphicon-open"></i> Aberta
              <span class="badge bg-green">  <h3> <?php
                $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 1 ");
                $stmt->execute();
                $stmt->bind_result($result);
                while ( $stmt->fetch()) {
                echo "$result" ;
              }?> </h3></span>

          </a>

     
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-before' ? 'active' : ''; ?>" href="maintenance-preventive-before">

            <i class="glyphicon glyphicon-export"></i> Atrasada
            <span class="badge bg-red">  <h3> <?php
              $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 2 ");
              $stmt->execute();
              $stmt->bind_result($result);
              while ( $stmt->fetch()) {
              echo "$result" ;
            }?> </h3></span>
          </a>

        
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-close' ? 'active' : ''; ?>" href="maintenance-preventive-close">

            <i class="glyphicon glyphicon-save"></i> Fechada
          </a>

         
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-print' ? 'active' : ''; ?>" href="maintenance-preventive-print">

            <i class="glyphicon glyphicon-print"></i> Impressão

          </a>

                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-print-register' ? 'active' : ''; ?>" href="maintenance-preventive-print-register">

            <i class="glyphicon glyphicon-folder-open"></i> Registro de Impressão
          </a>

                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-control' ? 'active' : ''; ?>" href="maintenance-preventive-control">

            <i class="fa fa-check-square-o"></i> Controle
          </a>
          
                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-control-return' ? 'active' : ''; ?>" href="maintenance-preventive-control-return">

            <i class="fa fa-pencil-square-o"></i> Retorno
          </a>

                </div>
              </div>
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form id="filter-form" class="container">
                      <div class="form-row">
                      <?php if($assistence == "0"){  ?>
                        <div class="col-md-3">
                          <label for="unidade">Empresa</label>
                          <input type="text" id="unidade" name="unidade" class="form-control">
                        </div>
                        <?php }else {   ?>
                        <div class="col-md-3">
                          <label for="unidade">Unidade</label>
                          <input type="text" id="unidade" name="unidade" class="form-control">
                        </div>
                        <?php }  ?>
                        <?php if($assistence == "0"){  ?>
                        <div class="col-md-3">
                          <label for="setor">Cliente</label>
                          <input type="text" id="setor" name="setor" class="form-control">
                        </div>
                        <?php }else {   ?>
                        <div class="col-md-3">
                          <label for="setor">Setor</label>
                          <input type="text" id="setor" name="setor" class="form-control">
                        </div>
                        <?php }  ?>
                        <?php if($assistence == "0"){  ?>
                        <div class="col-md-3">
                          <label for="area">Localização</label>
                          <input type="text" id="area" name="area" class="form-control">
                        </div>
                        <?php }else {   ?>
                        <div class="col-md-3">
                          <label for="area">Área</label>
                          <input type="text" id="area" name="area" class="form-control">
                        </div>
                        <?php }  ?>
                        <div class="col-md-3">
                          <label for="grupo">Grupo de Equipamento</label>
                          <input type="text" id="grupo" name="grupo" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3">
                          <label for="familia">Família de Equipamento</label>
                          <input type="text" id="familia" name="familia" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="fabricante">Fabricante</label>
                          <input type="text" id="fabricante" name="fabricante" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="modelo">Modelo</label>
                          <input type="text" id="modelo" name="modelo" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="codigo">Código</label>
                          <input type="text" id="codigo" name="codigo" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3">
                          <label for="patrimonio">Patrimônio</label>
                          <input type="text" id="patrimonio" name="patrimonio" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="serie">Série</label>
                          <input type="text" id="serie" name="serie" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="serie">Rotina</label>
                          <input type="text" id="rotina" name="rotina" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                      
                        
                        
                        <div class="col-md-3">
                          <button type="button" class="btn btn-primary mt-4" onclick="fetchData()">Filtrar</button>
                        </div>
                        
                      </div>
                      
                    </form>
                    
                    
                  </div>
                </div>
              </div>
            </div>
            
            
            
            <div class="clearfix"></div>
            
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta <small>Manutenção Preventiva</small> Concluida</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      
                      
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    
                    <div class="col-md-11 col-sm-11 ">
                      <div class="x_panel">
                        
                        <div class="x_content">
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="card-box table-responsive">
                                
                                
                                <table id="result-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th><input type="checkbox" id="check_all"></th>           
                                      <th>Ação</th>
                                      <th>Rotina</th>
                                      <th>Grupo Rotina</th>
                                      <th>Periodicidade</th>
                                        <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                                        <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                                        <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  
                                      <th>Grupo</th>
                                      <th>Codigo</th>
                                      <th>Equipamento</th>
                                      <th>Modelo</th>
                                      <th>Fabricante</th>
                                      <th>Série</th>
                                      <th>Patrimônio</th>
                                      
                                      
                                      
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <!-- Os resultados serão inseridos aqui -->
                                  </tbody>
                                </table>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    
                    
                    <script>
                      let badgeHTML = '';
                      let badgeHTMLBaixa = '';
                      let badgeHTMLcheckist= '';
                      let badgeHTMLtreinamento= '';
                      let badgeHTMLparecer_1= '';
                      let badgeHTMLparecer_2= '';
                      let badgeHTMLsaida_1= '';
                      let badgeHTMLsaida_2= '';
                      let badgeHTMLconformidade= '';
                      
                      function fetchData() {
                        // Obtenha os valores dos filtros
                        
                        const unidade = document.getElementById('unidade').value;
                        const setor = document.getElementById('setor').value;
                        const area = document.getElementById('area').value;
                        const familia = document.getElementById('familia').value;
                        const fabricante = document.getElementById('fabricante').value;
                        const modelo = document.getElementById('modelo').value;
                        const serie = document.getElementById('serie').value;
                        const codigo = document.getElementById('codigo').value;
                        const patrimonio = document.getElementById('patrimonio').value;
                      
                        const grupo = document.getElementById('grupo').value;
                  
                        const rotina = document.getElementById('rotina').value;
                        
                        // Monta a URL com parâmetros
                        let url = 'table/table-search-preventive-close-filter.php?';
                        if (grupo) url += `grupo=${encodeURIComponent(grupo)}&`;  if (unidade) url += `unidade=${encodeURIComponent(unidade)}&`;
                        if (setor) url += `setor=${encodeURIComponent(setor)}&`;
                        if (area) url += `area=${encodeURIComponent(area)}&`;
                        if (familia) url += `familia=${encodeURIComponent(familia)}&`;  if (fabricante) url += `fabricante=${encodeURIComponent(fabricante)}&`;  if (modelo) url += `modelo=${encodeURIComponent(modelo)}&`;  if (serie) url += `serie=${encodeURIComponent(serie)}&`;  if (codigo) url += `codigo=${encodeURIComponent(codigo)}&`;  if (patrimonio) url += `patrimonio=${encodeURIComponent(patrimonio)}&`;  
                        if (rotina) url += `rotina=${encodeURIComponent(rotina)}`;
                        
                        // Faz a requisição
                        fetch(url)
                        .then(response => response.json())
                        .then(data => {
                          // Limpa a tabela de resultados
                          const tbody = document.getElementById('result-table').getElementsByTagName('tbody')[0];
                          tbody.innerHTML = '';
                          
                          // Insere as linhas na tabela
                          data.forEach(row => {
                            
                            const tr = document.createElement('tr');
                            tr.innerHTML = `

              <td>
  <input type="checkbox" class="checkbox" value="${row.id}">

  
</td>
<td><div class="btn-group-mosaic">

  <!-- Grupo 1: Funções 1 a 4 -->
  <div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 1
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="maintenance-preventive-close-edit?routine=${row.rotina}" onclick="new PNotify({ title: 'Editar', text: 'Abrindo Edição', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-database"></i> Visualizar MP's
      </a>
      <a class="dropdown-item" href="maintenance-preventive-viwer-control?routine=${row.rotina}" target="_blank" onclick="new PNotify({ title: 'Etiqueta', text: 'Visualizar', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-pdf-o"></i> Visualizar
      </a>
                      
    </div>
  </div>

  <!-- Grupo 2: Funções 5 a 8 -->
  <div class="btn-group">
    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 2
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="maintenance-preventive-viwer-print-control?routine=${row.rotina}" target="_blank"  onclick="new PNotify({
                                title: 'Imprimir',
                                text: 'Imprimir Rotina',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-print"></i> Imprimir
                  </a>    
<a class="dropdown-item"  href="maintenance-preventive-open-tag-control?routine=${row.rotina}" target="_blank"   onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar Etiqueta',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-qrcode"></i> Etiqueta
                  </a>
    </div>
  </div>

  <!-- Grupo 3: Funções 9 a 12 -->
  <div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 3
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="maintenance-preventive-label-control?routine=${row.rotina}" target="_blank" onclick="new PNotify({
                              title: 'Etiqueta',
                              text: 'Abrir Etiqueta',
                              type: 'sucess',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-fax"></i> Rótulo
                </a>
</div>
  </div>

  <!-- Grupo 4: Funções 13 a 16 -->
  <div class="btn-group">
    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 4
    </button>
    <div class="dropdown-menu">
    


    </div>
  </div>

</div>
</td>
  <td>${row.rotina}</td>
  <td>${row.grupo}</td>
  <td>${row.periodicidade}</td>
              <td>${row.instituicao}</td>
              <td>${row.area}</td>
              <td>${row.setor}</td>
            
              <td>${row.grupo}</td>
              <td>${row.codigo}</td>
              <td>${row.equipamento}</td>
              <td>${row.modelo}</td>
              <td>${row.fabricante}</td>
              <td>${row.serie}</td>
              <td>${row.patrimonio}</td>
            
              
  
            `;
                            tbody.appendChild(tr);
                          });
                        })
                        .catch(error => console.error('Erro ao buscar dados:', error));
                      }
                      function takeAction() {
                        var selectedRows = [];
                        $('.checkbox:checked').each(function() {
                          selectedRows.push($(this).val());
                        });
                        
                        // Verifica se pelo menos um item foi selecionado
                        if (selectedRows.length > 0) {
                          // Constrói o URL com os IDs dos itens selecionados
                          var url = 'os-viwer-selected=' + selectedRows.join(',');
                          // Abre o URL em uma nova aba
                          window.open(url, '_blank');
                        } else {
                          // Se nenhum item foi selecionado, mostra uma mensagem
                          alert('Por favor, selecione pelo menos um item.');
                        }
                      }
                      $('#check_all').click(function() {
                        $('input[type=checkbox]').prop('checked', this.checked); // Seleciona todas as checkboxes
                      });
                      $('#datatable').dataTable( {
                        "processing": true,
                        "stateSave": true,
                        responsive: true,
                        
                        
                        
                        "language": {
                          "loadingRecords": "Carregando dados...",
                          "processing": "Processando  dados...",
                          "infoEmpty": "Nenhum dado a mostrar",
                          "emptyTable": "Sem dados disponíveis na tabela",
                          "zeroRecords": "Não há registros a serem exibidos",
                          "search": "Filtrar registros:",
                          "info": "Mostrando página _PAGE_ de _PAGES_",
                          "infoFiltered": " - filtragem de _MAX_ registros",
                          "lengthMenu": "Mostrar _MENU_ registros",
                          
                          "paginate": {
                            "previous": "Página anterior",
                            "next": "Próxima página",
                            "last": "Última página",
                            "first": "Primeira página",
                            
                            
                            
                          }
                        }
                        
                        
                        
                      } );
                    </script>
                    
                    
                  </div>
                </div>
              </div>
            </div>
            
            
            
            
          </div>
        </div>

