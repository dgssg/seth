<?php
$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT kpi.interpretation,kpi.id,kpi.name, kpi.typology, kpi.meta,kpi.value  FROM kpi WHERE kpi.id = $codigoget ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($interpretation,$id,$name,$typology, $meta, $value);
  while ($stmt->fetch()) { 
  }
   
}
?>

 <div class="right_col" role="main">

        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Edição</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                <div class="input-group">

                  <span class="input-group-btn">

                  </span>
                </div>
              </div>
            </div>
          </div>


       <div class="x_panel">
              <div class="x_title">
                <h2>Alerta</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

           <div class="alert alert-success alert-dismissible " role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Area de Edição!</strong> Todo alteração não é irreversível.
                </div>



              </div>
            </div>



           <!-- page content -->









            <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edição <small>Metas do Indicador</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a class="dropdown-item" href="#">Settings 1</a>
                        </li>
                        <li><a class="dropdown-item" href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form action="backend/report-indicator-metas-upgrade-backend.php" method="post">

                  <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Indicador <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="nome" name="nome" value="<?php printf($name); ?>" class="form-control " readonly>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="typology">Tipologia<span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="typology" name="typology"  class="form-control"  value="<?php printf($typology); ?> " readonly>
                      </div>
                    </div>
                  

                    <div class="ln_solid"></div>


                     

           <label for="interpretation">Interpretação:</label>
<textarea id="interpretation" class="form-control" name="interpretation" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php echo htmlspecialchars_decode($interpretation); ?>" value="<?php echo htmlspecialchars_decode($interpretation); ?>"><?php echo htmlspecialchars_decode($interpretation); ?></textarea>
                   


                      <div class="ln_solid"></div>

                    <div class="ln_solid"></div>


                     

           <label for="meta">Meta:</label>
<textarea id="meta" class="form-control" name="meta" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php echo htmlspecialchars_decode($meta); ?>" value="<?php echo htmlspecialchars_decode($meta); ?>"><?php echo htmlspecialchars_decode($meta); ?></textarea>
                   


                      <div class="ln_solid"></div>

                         <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor (%)</label>
                      <div class="col-md-6 col-sm-6 ">
                       <input class="knob" data-width="100" data-height="120" data-angleOffset=-125 data-angleArc=250 data-fgColor="#34495E" data-rotation="anticlockwise" value="<?php printf($value); ?>" id="value" name="value">
                      </div>
                    </div>







                              <div class="form-group row">
                     <label class="col-form-label col-md-3 col-sm-3 "></label>
                      <div class="col-md-3 col-sm-3 ">
                          <center>
                         <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});">Salvar Informações</button>
                       </center>
                      </div>
                    </div>


                                </form>






                </div>
              </div>
            </div>
          </div>



           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="report-indicator-metas">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>




              </div>
            </div>




      </div>
          </div>
      <!-- /page content -->

  <!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
