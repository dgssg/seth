<?php
$codigoget = ($_GET["id"]);
  
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }

// Create connection
include("database/database.php");// remover ../


$row=1;
$query = "SELECT quarterly_analysis.anexo,quarterly_analysis.id_quarterly, quarterly_analysis.id_kpi_year, quarterly_analysis.id_colaborador, quarterly_analysis.id_instituicao, quarterly_analysis.id,instituicao.instituicao,colaborador.primeironome,colaborador.ultimonome, kpi_year.yr,quarterly.nome,quarterly_analysis.analysis,quarterly_analysis.action,quarterly_analysis.data_after,quarterly_analysis.data_execution,quarterly_analysis.upgrade,quarterly_analysis.reg_date FROM quarterly_analysis INNER JOIN quarterly ON quarterly.id = quarterly_analysis.id_quarterly INNER JOIN kpi_year ON kpi_year.id = quarterly_analysis.id_kpi_year INNER JOIN colaborador ON colaborador.id = quarterly_analysis.id_colaborador INNER JOIN instituicao ON instituicao.id = quarterly_analysis.id_instituicao WHERE quarterly_analysis.id = $codigoget";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($anexo,$id_quarterly,$id_kpi_year,$id_colaborador,$id_instituicao,$id,$instituicao,$primeironome,$ultimonome,$kpi_year,$quarterly,$analysis,$action,$data_after,$date_execution,$upgrade,$reg_date);

while ($stmt->fetch()) {
    //pintf("%s, %s\n", $solicitante, $equipamento);
  }
}
function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
  $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
  $con = 1;
  $var = '';
  while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
  }else{
      $var = $var.$n.$res[$n];
      $con++;
      }
  }
      $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
 // echo chaveAlfaNumerica(5);
//   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = md5($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 3
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function v4() {
return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

// 32 bits for "time_low"
mt_rand(0, 0xffff), mt_rand(0, 0xffff),

// 16 bits for "time_mid"
mt_rand(0, 0xffff),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand(0, 0x0fff) | 0x4000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand(0, 0x3fff) | 0x8000,

// 48 bits for "node"
mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
);
}

public static function v5($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = sha1($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function is_valid($uuid) {
return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();
//echo $v4uuid; //chave da assinatura
?>


                 <!-- 1 -->
                 <link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
<div class="right_col" role="main">

  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>Edição</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <!-- page content -->









      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edição <small>de Analise</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form action="backend/pdca-quarterly-analysis-edit-backend.php" method="post">

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                  </div>
                </div>


                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_quarterly">Trimestre <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_quarterly" id="id_quarterly"  placeholder="Trimestre">
                      <option value="">Selecione um Trimestre</option>
                      <?php



                      $result_cat_post  = "SELECT  id, nome FROM quarterly";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_quarterly ) {
                        echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Trimestre "></span>

                  </div>
                </div>

                <script>
                $(document).ready(function() {
                  $('#id_quarterly').select2();
                });
                </script>



                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_kpi_year">Ano <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_kpi_year" id="id_kpi_year"  placeholder="Ano">
                      <option value="">Selecione um Ano</option>
                      <?php



                      $result_cat_post  = "SELECT  id, yr FROM kpi_year ";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_kpi_year ) {
                        echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['yr'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['yr'].'  </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Ano "></span>

                  </div>
                </div>
                <script>
                $(document).ready(function() {
                  $('#id_kpi_year').select2();
                });
                </script>
                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_colaborador">colaborador <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador">
                      <option value="">Selecione um Colaborador</option>
                      <?php



                      $result_cat_post  = "SELECT  id, primeironome, ultimonome FROM colaborador  WHERE trash = 1 ";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_colaborador ) {
                            echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['primeironome'].' '.$row_cat_post['ultimonome'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['primeironome'].' '.$row_cat_post['ultimonome'].'  </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Colaborador "></span>

                  </div>
                </div>

                <script>
                $(document).ready(function() {
                  $('#id_colaborador').select2();
                });
                </script>



                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_instituicao">Unidade <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_instituicao" id="id_instituicao"  placeholder="Unidade">
                    <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                      <?php



                      $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_instituicao ) {
                          echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['instituicao'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'  </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Unidade "></span>

                  </div>
                </div>

                <script>
                $(document).ready(function() {
                  $('#id_instituicao').select2();
                });
                </script>

                          <label for="analysis">Análise :</label>
                          <textarea id="analysis" class="form-control" name="analysis" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="20" placeholder="<?php echo htmlspecialchars_decode($analysis); ?>" value="<?php echo htmlspecialchars_decode($analysis); ?>"><?php echo htmlspecialchars_decode($analysis); ?></textarea>
                          
                            <label for="action">Ação:</label>
                          <textarea id="action" class="form-control" name="action" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="20" placeholder="<?php echo htmlspecialchars_decode($action); ?>" value="<?php echo htmlspecialchars_decode($action); ?>"><?php echo htmlspecialchars_decode($action); ?></textarea>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Prevista</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="data_after"  type="date" name="data_after" value="<?php printf($data_after); ?>" class="form-control"  >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Execução</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="data_execution"  type="date" name="data_execution"  value="<?php printf($date_execution); ?>" class="form-control" >
                  </div>
                </div>



                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input readonly="readonly" type="text"  class="form-control"  value="<?php printf($reg_date); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                  </div>
                </div>



                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                  <div class="col-md-3 col-sm-3 ">
                    <center>
                      <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                    </center>
                  </div>
                </div>


              </form>






            </div>
          </div>
        </div>
      </div>


      <div class="clearfix"></div>
<!--
<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Anexo <small> </small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="row">
          <div class="col-md-55">
            <?php if($anexo ==! "" ){ ?>
              <div class="thumbnail">
                <div class="image view view-first">
                  <img style="width: 100%; display: block;" src="dropzone/quarterly/<?php printf($anexo); ?>" alt="image" />
                  <div class="mask">
                    <p>Anexo</p>
                    <div class="tools tools-bottom">
                      <a href="dropzone/quarterly/<?php printf($anexo); ?>" download><i class="fa fa-download"></i></a>

                    </div>
                  </div>
                </div>
                <div class="caption">
                  <p>Anexo</p>
                </div>
              </div>
            <?php }; ?>
          </div>







        </div>
      </div>
    </div>
  </div>
</div> 

<div class="x_panel">
                <div class="x_title">
                  <h2>Atualizar Anexo</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                 <form  class="dropzone" action="backend/pdca-quarterly-analysis-upload-backend.php" method="post">
    </form >

    <form action="backend/pdca-quarterly-analysis-update-backend.php?id=<?php printf($codigoget); ?>" method="post">
  <center>
  <button class="btn btn-sm btn-success" type="submit">Atualizar Anexo</button>
  </center>
  </form >

                </div>
              </div> -->


              <div class="clearfix"></div> 

<!-- Posicionamento -->

<div class="x_panel">
    <div class="x_title">
        <h2>Anexos Gerais</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="badge bg-green pull-right" data-toggle="modal"
                    data-target=".bs-example-modal-lg2"><i class="fa fa-plus"></i></a>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <!--arquivo -->
        <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_quartely_dropzone WHERE id_quartely like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Titulo</th>
                    <th>Registro</th>
                    <th>Atualização</th>
                    <th>Ação</th>

                </tr>
            </thead>
            <tbody>
                <?php  while ($stmt->fetch()) { ?>

                <tr>
                    <th scope="row"><?php printf($row); ?></th>
                    <td><?php printf($id); ?></td>
                    <td><?php printf($titulo); ?></td>
                    <td><?php printf($reg_date); ?></td>
                    <td><?php printf($upgrade); ?></td>
                    <td> <a class="btn btn-app" href="dropzone/quarterly/<?php printf($file); ?> "
                            target="_blank" onclick="new PNotify({
                      title: 'Visualizar',
                      text: 'Visualizar Anexo',
                      type: 'info',
                      styling: 'bootstrap3'
                  });">
                            <i class="fa fa-file"></i> Anexo
                        </a>
                        <a class="btn btn-app" onclick="
        Swal.fire({
title: 'Tem certeza?',
text: 'Você não será capaz de reverter isso!',
icon: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
if (result.isConfirmed) {
Swal.fire(
'Deletando!',
'Seu arquivo será excluído.',
'success'
),
window.location = 'backend/pdca-quartely-analysis-dropzone-trash.php?id=<?php printf($id); ?>&id_quartely=<?php printf($codigoget); ?> ';
}
})
">
                            <i class="fa fa-trash"></i> Excluir
                        </a>
                    </td>

                </tr>
                <?php  $row=$row+1; }
}   ?>
            </tbody>
        </table>




    </div>
</div>

<!-- Registro -->
<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <!-- Registro forms-->
                <form
                    action="backend/pdca-quartely-analysis-dropzone-backend.php?id=<?php printf($codigoget);?>"
                    method="post">
                    <div class="ln_solid"></div>


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                                class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" class='date' type="text" name="titulo"
                                required='required'>
                        </div>
                    </div>




                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar Informações</button>

                    </div>
            </div>
            </form>

            <form class="dropzone" action="backend/pdca-quartely-analysis-dropzone-upload-backend.php"
                method="post">
            </form>
        </div>
    </div>
</div>
<!-- Registro -->
<div class="clearfix"></div>



<div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Assinatura <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="backend/pdca-quarterly-signature-backend.php?id=<?php printf($codigoget);?>" method="post">
          <div class="item form-group">
                        <label for="middle-name"
                            class="col-form-label col-md-3 col-sm-3 label-align">Chave da Assinatura</label>
                        <div class="col-md-6 col-sm-6 ">
                            <input class="form-control" type="text" value="<?php printf($signature_alert); ?> "
                                readonly="readonly">
                        </div>
                    </div>

<!-- Large modal --> <center>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Assinatura</button>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Assinatura</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <h4>Assinatura digital</h4>
                <p>Para Assinar digite o texto abaixo.</p>
               <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
                <p> <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> <span ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="assinature" name="assinature"   class="form-control " class="docs-tooltip" data-toggle="tooltip" title=" ">
                
              </div>
              <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >
              </div>
            </div>
            <script>
var chave = "<?php echo $chave; ?>"; // Obtém a chave do PHP e armazena na variável JavaScript
var assinaturaInput = document.getElementById("assinature");

assinaturaInput.addEventListener("blur", function () {
var assinatura = this.value;

if (assinatura !== chave) {
Swal.fire('A assinatura está incorreta!');
  // Aqui você pode realizar a ação desejada caso a assinatura seja divergente, como exibir uma mensagem de erro ou realizar algum redirecionamento.
} else {

  // Aqui você pode realizar a ação desejada caso a assinatura seja correta.
}
});
</script>
            </div></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Assinar</button>
              </div>

            </div>
          </div>
        </div>
       </center>
       
          </form>
        </div>
      </div>
    </div>
  </div>


  <div class="clearfix"></div>
  <div class="clearfix"></div>


<div class="x_panel">
    <div class="x_title">
      <h2>Assinatura</h2>
      <ul class="nav navbar-right panel_toolbox">
         
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      
    <!--arquivo -->
<?php    
$query="SELECT regdate_quarterly_analysis_signature.id,regdate_quarterly_analysis_signature.id_regdate_quarterly_analysis_signature,regdate_quarterly_analysis_signature.id_user,regdate_quarterly_analysis_signature.id_signature,regdate_quarterly_analysis_signature.reg_date,usuario.nome FROM regdate_quarterly_analysis_signature INNER JOIN usuario ON regdate_quarterly_analysis_signature.id_user = usuario.id   WHERE regdate_quarterly_analysis_signature.id_regdate_quarterly_analysis_signature like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$id_mp,$id_user,$id_signature,$reg_date,$id_user);

?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              
              <th>Usuário</th>
              <th>Assinatura</th>
               <th>Registro</th>
                <th>Ação</th>
            
            </tr>
          </thead>
          <tbody>
                 <?php  while ($stmt->fetch()) { ?>
                 
            <tr>
              <th scope="row"><?php printf($row); ?></th>
              <td><?php printf($id_user); ?></td>
              <td><?php printf($id_signature); ?></td>
              <td><?php printf($reg_date); ?></td>
              <td>                                                                <a class="btn btn-app" onclick="
                Swal.fire({
                  title: 'Tem certeza?',
                  text: 'Você não será capaz de reverter isso!',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Sim, Deletar!'
                }).then((result) => {
                  if (result.isConfirmed) {
                    Swal.fire(
                      'Deletando!',
                      'Seu arquivo será excluído.',
                      'success'
                    ),
window.location = 'backend/signature-pdca-trash-backend.php?id=<?php printf($id); ?>&page=<?php printf($codigoget); ?>';                  }
                })
              ">
                <i class="fa fa-trash"></i> Excluir
              </a>
              </td>

             
              
            </tr>
       <?php  $row=$row+1; }
}   ?>
          </tbody>
        </table>

    
  
    
    </div>
  </div>  
    <!-- compose -->


<div class="clearfix"></div>

                                    <div class="x_panel">
                                      <div class="x_title">
                                        <h2>Ação</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                          </li>
                                          <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                              class="fa fa-wrench"></i></a>
                                              <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                              </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                          </ul>
                                          <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                          <a class="btn btn-app"  href="pdca-quarterly-analysis">
                                            <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                                          </a>




                                        </div>
                                      </div>




                                    </div>
                                  </div>
                                  <!-- /page content -->

                                  <!-- /compose -->

                                  </div>
                                  <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
