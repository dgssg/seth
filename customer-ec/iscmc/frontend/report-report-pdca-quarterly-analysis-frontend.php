<?php
$codigoget = ($_GET["os"]);
date_default_timezone_set('America/Sao_Paulo');
$ano=date("Y");
// Create connection
include("database/database.php");// remover ../

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Analise Trimestral</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 
           
         
            
             <!-- page content -->
       
           

           
            
            
            
            
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                  <h2>Filtro <small>do Relatório</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="backend/report-pdca-quarterly-analysis-backend.php" method="post"  target="_blank">
 
 <div class="ln_solid"></div>
 <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_quarterly">Trimestre <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_quarterly" id="id_quarterly"  placeholder="Trimestre">
                          <option value="">Selecione um Trimestre</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM quarterly";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Trimestre "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#id_quarterly').select2();
                    });
                    </script>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_kpi_year">Ano <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_kpi_year" id="id_kpi_year"  placeholder="Ano">
                          <option value="">Selecione um Ano</option>
                          <?php



$query = "SELECT id, yr FROM kpi_year";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id, $yr);
while ($stmt->fetch()) {
?>
<option value="<?php printf($yr);?>	"<?php if($yr == $ano){ printf("selected"); } ?>><?php printf($yr);?></option>
   <?php
 // tira o resultado da busca da memória
 }

                       }
 $stmt->close();

 ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Ano "></span>

                      </div>
                    </div>
                    <script>
                    $(document).ready(function() {
                      $('#id_kpi_year').select2();
                    });
                    </script>
 
 



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_instituicao">Unidade <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_instituicao" id="id_instituicao"  placeholder="Unidade">
                        <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                          <?php



                          $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Unidade "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#id_instituicao').select2();
                    });
                    </script>
                     
                      
                     <br>
                     
                    
                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatório</button>
                         </center>
                        </div>
                      </div>
     
                  
                    
                   
                  </div>
                </div>
              </div>
            </div>
         
         
         
         
        
            
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="report-report">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
              
                 
                  
                </div>
              </div>
              
              
              

            

            
        </div>
            </div>
        <!-- /page content -->
        <!-- /page content -->
     <script type="text/javascript">
    $(document).ready(function(){
        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });
    </script>
     <script type="text/javascript">
    $(document).ready(function(){
        $('#area').change(function(){
            $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
        });
    });
    </script>