self.addEventListener('push', function(event) {
    console.log('Push Notification received', event);

    var options = {
        body: event.data.text(),
        icon: 'path_to_your_icon.png', // Substitua 'path_to_your_icon.png' pelo caminho para o ícone da notificação
        badge: 'path_to_your_badge.png' // Substitua 'path_to_your_badge.png' pelo caminho para o badge da notificação
    };

    event.waitUntil(
        self.registration.showNotification('Notification Title', options)
    );
});
