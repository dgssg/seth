<?php

include("../database/database.php");

$codigoget= $_GET['id'];
$tools= $_GET['tools'];
$nome= $_POST['nome'];
$unidade= $_POST['unidade'];
$i_h= $_POST['i_h'];
$r_p= $_POST['r_p'];


$stmt = $conn->prepare("UPDATE calibration_parameter_tools_dados SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools_dados SET unidade = ? WHERE id= ?");
$stmt->bind_param("ss",$unidade,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools_dados SET i_h = ? WHERE id= ?");
$stmt->bind_param("ss",$i_h,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools_dados SET r_p = ? WHERE id= ?");
$stmt->bind_param("ss",$r_p,$codigoget);
$execval = $stmt->execute();

echo "<script>alert('Atualizado!');document.location='../calibration-parameter-equipament-parameter?tools=$tools'</script>";


?>
