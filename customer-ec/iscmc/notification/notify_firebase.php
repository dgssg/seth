<?php

// Configurações de conexão com o banco de dados
$servername = "localhost";
$username = "cvheal47_root";
$password = "cvheal47_root";
$database = "cvheal47_seth_mks";

// Conexão com o banco de dados
$conn = new mysqli($servername, $username, $password, $database);

// Verifica a conexão
if ($conn->connect_error) {
    die("Conexão falhou: " . $conn->connect_error);
}

// Consulta para obter os dados relevantes para a notificação
$sql = "SELECT title, notif_msg FROM notif WHERE read = 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Dados da notificação
    $row = $result->fetch_assoc();
    $titulo = $row["title"];
    $corpo = $row["notif_msg"];

    // Chave do servidor FCM
    define('FCM_API_KEY', 'SUA_CHAVE_DO_SERVIDOR_FCM');

    // Token do dispositivo móvel
    $deviceToken = 'TOKEN_DO_DISPOSITIVO';

    // Corpo da notificação
    $message = array(
        'title' => $titulo,
        'body' => $corpo
    );

    $fields = array(
        'to' => $deviceToken,
        'notification' => $message
    );

    $headers = array(
        'Authorization: key=' . FCM_API_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    $result = curl_exec($ch);

    curl_close($ch);

    echo $result;
} else {
    echo "Nenhuma notificação ativa encontrada.";
}

// Fechar conexão
$conn->close();

?>
