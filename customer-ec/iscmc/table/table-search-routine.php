<?php
include("../database/database.php");
$query = "SELECT maintenance_routine.digital,maintenance_routine.signature_digital,maintenance_routine.app,maintenance_group.nome AS 'grupo', maintenance_routine.habilitado,maintenance_routine.id_maintenance_procedures_before, equipamento_familia.fabricante,equipamento.serie,instituicao.instituicao, instituicao_area.nome AS 'setor',instituicao_localizacao.nome AS 'area',maintenance_routine.id,maintenance_routine.data_start AS 'data', maintenance_routine.periodicidade AS 'periodicidade',maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome AS 'equipamento',equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN maintenance_group ON maintenance_group.id = maintenance_routine.group_routine WHERE maintenance_routine.trash =1 order by maintenance_routine.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
