<?php
include("../database/database.php");
$query = "SELECT id, instituicao, codigo, endereco, site, upgrade, reg_date FROM instituicao WHERE trash = 1 ORDER by id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
