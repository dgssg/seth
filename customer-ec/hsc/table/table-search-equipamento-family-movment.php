<?php
include("../database/database.php");
$query = "SELECT equipamento.codigo,equipamento_familia.nome AS 'familia',equipamento_familia.modelo,equipamento_familia.fabricante,equipament_exit.id,fornecedor.empresa,equipament_exit_mov.nome AS 'mov', equipament_exit_reason.nome AS 'reason',equipament_exit.date_send, equipament_exit.date_return, equipament_exit_status.nome AS 'status' FROM equipament_exit INNER JOIN fornecedor on fornecedor.id = equipament_exit.id_fornecedor INNER JOIN equipament_exit_mov on equipament_exit_mov.id= equipament_exit.id_equipament_exit_mov INNER JOIN equipament_exit_reason on equipament_exit_reason.id = equipament_exit.id_equipament_exit_reason  INNER JOIN equipament_exit_status on equipament_exit_status.id = equipament_exit.id_equipament_exit_status LEFT JOIN os ON os.id =  equipament_exit.id_os LEFT JOIN equipamento ON equipamento.id = equipament_exit.id_equipamento OR equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia OR equipament_exit.id_equipamento_familia = equipamento_familia.id ORDER BY equipament_exit.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
