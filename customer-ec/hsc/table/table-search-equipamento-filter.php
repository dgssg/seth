<?php
include("../database/database.php");

// Inicialize a variável da query com a consulta base
$query = "
SELECT 
    equipament_compliance.id AS 'conformidade', 
    os.id AS 'saida_1',
    equipament_exit.id AS 'saida_2',
    parecer_equipamento.id AS 'parecer_1',
    equipament_report.id AS 'parecer_2',
    equipament_training.id_equipamento AS 'treinamento', 
    equipamento_grupo.nome AS 'grupo',
    equipamento.patrimonio,
    equipamento_familia.anvisa, 
    equipament_check.id AS 'checklist', 
    technological_surveillance.id AS 'technological',
    alert.id as 'alert', 
    equipamento.nf_dropzone,
    equipamento.serie,
    equipamento.comodato,
    equipamento.baixa,
    equipamento.ativo,
    instituicao.instituicao, 
    instituicao_area.nome AS 'area',
    instituicao_localizacao.nome AS 'setor', 
    equipamento.id, 
    equipamento_familia.nome AS 'equipamento', 
    equipamento_familia.modelo,
    equipamento_familia.fabricante, 
    equipamento.codigo 
FROM 
    equipamento 
INNER JOIN 
    equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia  
INNER JOIN 
    instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao 
INNER JOIN 
    instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  
INNER JOIN 
    instituicao ON instituicao.id = instituicao_area.id_unidade 
LEFT JOIN 
    alert on alert.equipamento_familia = equipamento.id_equipamento_familia 
LEFT JOIN 
    os on os.id_equipamento = equipamento.id 
LEFT JOIN 
    maintenance_routine on maintenance_routine.id_equipamento = equipamento.id 
LEFT JOIN 
    technological_surveillance on os.id = technological_surveillance.id_os and technological_surveillance.mp = 0  
LEFT JOIN 
    equipament_check ON equipament_check.id_equipamento = equipamento.id 
LEFT JOIN 
    equipamento_grupo ON equipamento_grupo.id = equipamento_familia.id_equipamento_grupo 
LEFT JOIN 
    equipament_training ON equipament_training.id_equipamento =  equipamento.id 
LEFT JOIN 
    equipament_report ON equipament_report.id_equipamento = equipamento.id 
LEFT JOIN 
    parecer_equipamento ON parecer_equipamento.id_equipamento = equipamento.id 
LEFT JOIN 
    equipament_exit ON equipament_exit.id_equipamento = equipamento.id OR os.id =  equipament_exit.id_os 
LEFT JOIN  
    equipament_compliance ON equipament_compliance.id_os = os.id 
WHERE 
    equipamento.trash = 1 
";

// Adicione os filtros se os parâmetros estiverem definidos
$filters = [];
if (isset($_GET['equipamento_id'])) {
    $filters[] = "equipamento.id = " . intval($_GET['equipamento_id']);
}
    if (isset($_GET['unidade'])) {
        $filters[] = "instituicao.instituicao LIKE '%" . $conn->real_escape_string($_GET['unidade']) . "%'";
    }
    if (isset($_GET['setor'])) {
        $filters[] = "instituicao_area.nome LIKE '%" . $conn->real_escape_string($_GET['setor']) . "%'";
    }
    if (isset($_GET['area'])) {
        $filters[] = "instituicao_localizacao.nome LIKE '%" . $conn->real_escape_string($_GET['area']) . "%'";
    }
    if (isset($_GET['familia'])) {
        $filters[] = "equipamento_familia.nome LIKE '%" . $conn->real_escape_string($_GET['familia']) . "%'";
    }
    if (isset($_GET['fabricante'])) {
        $filters[] = "equipamento_familia.fabricante LIKE '%" . $conn->real_escape_string($_GET['fabricante']) . "%'";
    }
    if (isset($_GET['modelo'])) {
        $filters[] = "equipamento_familia.modelo LIKE '%" . $conn->real_escape_string($_GET['modelo']) . "%'";
    }
    if (isset($_GET['codigo'])) {
        $filters[] = "equipamento.codigo LIKE '%" . $conn->real_escape_string($_GET['codigo']) . "%'";
    }
    if (isset($_GET['patrimonio'])) {
        $filters[] = "equipamento.patrimonio LIKE '%" . $conn->real_escape_string($_GET['patrimonio']) . "%'";
    }
    if (isset($_GET['serie'])) {
        $filters[] = "equipamento.serie LIKE '%" . $conn->real_escape_string($_GET['serie']) . "%'";
    }
if (isset($_GET['grupo'])) {
        $filters[] = "equipamento_grupo.nome LIKE '%" . $conn->real_escape_string($_GET['grupo']) . "%'";
}

if (isset($_GET['ativo'])) {
    $filters[] = "equipamento.ativo = " . intval($_GET['ativo']);
}
    if (isset($_GET['baixa'])) {
        $filters[] = "equipamento.baixa = " . intval($_GET['baixa']);
    }
    if (isset($_GET['comodato'])) {
        $filters[] = "equipamento.comodato = " . intval($_GET['comodato']);
    }
// Se houver filtros, adicione-os na query
if (count($filters) > 0) {
    $query .= " AND " . implode(" AND ", $filters);
}

$query .= " GROUP BY equipamento.id ORDER BY equipamento.id DESC";

// Execute a consulta e retorne os resultados como JSON
$resultados = $conn->query($query);

if (!$resultados) {
    die("Erro na consulta ao banco de dados: " . $conn->error);
}

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

// Configurar o cabeçalho para JSON
header('Content-Type: application/json');
echo json_encode($rows);

$conn->close();
?>
