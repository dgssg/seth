<!DOCTYPE html>
<html lang="en">
  <head>
  <head>
  <link rel="icon" type="image/x-icon" href="../../framework/images/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../../framework/images/favicon.ico" type="image/ico" />

    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>


   <!-- Bootstrap -->
   <link href="../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- iCheck -->
    <link href="../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- JQVMap -->
    <link href="../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    
    <!-- Custom Theme Style -->
    <link href="../../../framework/build/css/custom.min.css" rel="stylesheet">

      <!-- PNotify -->
  <link href="../../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
   <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-500px"></i> <span>SETH Clinico</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../logo/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bem vindo a</span>
                <h2>MK Sistemas</h2>
              
               <span>
              <script> document.write(new Date().toLocaleDateString()); </script>
              </span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Geral</h3>
                <ul class="nav side-menu">
             
                </div>
            

            </div>
          
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">


                  </li>

            
                  </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"  rel="stylesheet"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<?php clearstatcache(); ?>
<?php 	setcookie('anexo', null, -1); ?>
<script type="text/javascript">
$.ajaxSetup({ cache: false })
</script>

<script type="text/javascript">
$.ajaxSetup({ cache: false })

function limpaUrl() {     //função
    urlpg = $(location).attr('href');   //pega a url atual da página
    urllimpa = urlpg.split("?")[0]      //tira tudo o que estiver depois de '?'

    window.history.replaceState(null, null, urllimpa); //subtitui a url atual pela url limpa
  
}
limpaUrl();
</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


      <?php


// Create connection
include("../database/database.php");// remover ../
$query = "SELECT link_horaire FROM tools WHERE id = 1";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($link_horaire);
            while ($stmt->fetch()) {
               
            }
          }
          if($link_horaire !== 0 ){
            header ("Location: ../index.php");
          }
?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Cronograma</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 
           
         
            
             <!-- page content -->
       
           

           
            
            
            
            
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro <small>do Cronograma</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="../backend/report-indicator-horaire-backend.php" method="post"  target="_blank">
 
 <div class="ln_solid"></div>
 
<div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="yr" id="yr"  placeholder="equipamento">
										  <option value="">Selecione o ano</option>
										  	<?php
										  	
										  
										  	
										 $query = "SELECT id, yr FROM kpi_year";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $yr);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($yr);?>	"><?php printf($yr);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Ano "></span>
										
										</div>
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#yr').select2();
                                      });
                                 </script>
                    
                   <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Mês</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="month" id="month"  placeholder="equipamento">
										  <option value="">Selecione o Mês</option>
										  	<?php
										  	
										  
										  	
										 $query = "SELECT id, month FROM kpi_month";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $month);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($month);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Ano "></span>
										
										</div>
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#month').select2();
                                      });
                                 </script>
 
 <div class="item form-group">
                         <?php if($assistence == "0"){  ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Empresa <span class="required"></span>
        </label>
        <?php }else {   ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
        </label>            
        <?php }  ?>	
                          <div class="input-group col-md-6 col-sm-6">
										<select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">
										   <option>Selecione uma Instituição</option>
										  	<?php
										  	
										  
										  	
										   $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

$resultado_cat_post = mysqli_query($conn, $result_cat_post);
					while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
						echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
					}
     ?>

	                                     	</select>  
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
										
										</div>
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#instituicao').select2();
                                      });
                                 </script>
                                 
                                
                                
                                	 <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
                            </label>
                            <div class="input-group col-md-6 col-sm-6">
										<select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
										  	  <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	
	                                     	</select>  
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>
										
									
										</div>
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>
                                 
                                 <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required"></span>
                            </label>
                            <div class="input-group col-md-6 col-sm-6">
										<select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
										  	<option value="">Selecione uma Opção</option>
	                                     	</select>  
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>
										
									
										</div>
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>
 

                    
                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_status" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Status ">Status <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_status" id="id_status"  placeholder="Status">
								 <option value=""> Selecione um Status</option>
										  	<?php
										  	
										  
										  	
										  $query = "SELECT id, status,cor FROM maintenance_status ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $status,$cor);
   while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($status);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                       <script>
                                    $(document).ready(function() {
                                    $('#id_status').select2();
                                      });
                                 </script>
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_status" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Status ">Grupo <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="equipamento_grupo">
										     <option value=""> Selecione um Grupo</option>
										  	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo ";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
                                            }
											$stmt->close();
											?>
	                                     	</select>  
 <span class="input-group-btn">
                                          
                                          
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">
										    </span>
										</span>										 </div>	
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>
                      
     
                     
									
									
										
									
								     
								 <script>
                                    $(document).ready(function() {
                                    $('#os_status').select2();
                                      });
                                 </script>
                      
                      
                     <br>
                     
                    
                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatório</button>
                         </center>
                        </div>
                      </div>
     
     
                    
                   
                  </div>
                </div>
              </div>
            </div>
         
         
         
         
        
           
              

            

            
        </div>
            </div>
        <!-- /page content -->
        <!-- /page content -->
     <script type="text/javascript">
    $(document).ready(function(){
        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });
    </script>
     <script type="text/javascript">
    $(document).ready(function(){
        $('#area').change(function(){
            $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
        });
    });
    </script>




        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           ©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://colorlib.com"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
<!-- jQuery
    <script src="../../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../../framework/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../../framework/build/js/custom.min.js"></script>


    <!-- gauge.js -->
    <script src="../../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../../framework/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../../../framework/vendors/skycons/skycons.js"></script>

   <!-- JQVMap -->
    <script src="../../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
  
 <!-- PNotify -->
   <script src="../../../framework/vendors/pnotify/dist/pnotify.js"></script>
   <script src="../../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
   <script src="../../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>


      <!-- bootstrap-wysiwyg -->
    <script src="../../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../../../framework/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../../../framework/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../../../framework/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="../../../framework/assets/js/select2.min.js"></script>
    <!-- Parsley -->
    <script src="../../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../../../framework/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../../../framework/vendors/starrr/dist/starrr.js"></script>
   <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>



  </body>


</html>
