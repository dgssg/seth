

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Manutenção <small>Preventiva</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>





            <div class="clearfix"></div>




              <!-- Posicionamento -->



<div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:5; -moz-column-count:5; -webkit-column-count:5; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
               <div class="x_panel">
                <div class="x_title">
                  <h2>Lista MP Concluido Aguardando Assinatura</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  
                  
                  
                  <div class="col-md-11 col-sm-11 ">
                    <div class="x_panel">
                      
                      <div class="x_content">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                              
                              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Rotina</th>
                                    <th>Periodicidade</th>
                                    <th>Codigo</th>
                                    <th>Equipamento</th>
                                    <th>Modelo</th>
                                    <th>Fabricante</th>
                                    
                                    <th>N/S</th> 
                                    
                                    
                                      <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                                      <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                                      <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	   
                                    
                                    
                                    <th>Ação</th>
                                    
                                  </tr>
                                </thead>
                                
                                
                                <tbody>
                                  
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
          

                </div>
              </div>




                </div>
              </div>
 
  <script language="JavaScript">
    
    $(document).ready(function() {
      $('#datatable').dataTable( {
        "processing": true,
        "stateSave": true,
        responsive: true,
        
        
        
        "language": {
          "loadingRecords": "Carregando dados...",
          "processing": "Processando  dados...",
          "infoEmpty": "Nenhum dado a mostrar",
          "emptyTable": "Sem dados disponíveis na tabela",
          "zeroRecords": "Não há registros a serem exibidos",
          "search": "Filtrar registros:",
          "info": "Mostrando página _PAGE_ de _PAGES_",
          "infoFiltered": " - filtragem de _MAX_ registros",
          "lengthMenu": "Mostrar _MENU_ registros",
          
          "paginate": {
            "previous": "Página anterior",
            "next": "Próxima página",
            "last": "Última página",
            "first": "Primeira página",
            
            
            
          }
        }
        
        
        
      } );
      // Define a URL da API que retorna os dados da query em formato JSON
      const apiUrl = '../../table/table-search-preventive-control-return-user.php';
      
      // Usa a função fetch() para obter os dados da API em formato JSON
      fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        // Mapeia os dados para o formato esperado pelo DataTables
        const novosDados = data.map(item => [
          ``,
          item.rotina,
          
          item.periodicidade == 5 ? "Diaria [1 Dias Uteis]"
          : item.periodicidade == 365 ? "Anual [365 Dias]"
          : item.periodicidade == 180 ? "Semestral [180 Dias]"
          : item.periodicidade == 30 ? "Mensal [30 Dias]"
          : item.periodicidade == 1 ? "Diaria [1 Dias]"
          : item.periodicidade == 7 ? "Semanal [7 Dias]"
          : item.periodicidade == 14 ? "Bisemanal [14 Dias]"
          : item.periodicidade == 21 ? "Trisemanal [21 Dias]"
          : item.periodicidade == 28 ? "Quadrisemanal [28 Dias]"
          : item.periodicidade == 60 ? "Bimestral [60 Dias]"
          : item.periodicidade == 90 ? "Trimestral [90 Dias]"
          : item.periodicidade == 120 ? "Quadrimestral [120 Dias]"
          : item.periodicidade == 730 ? "Bianual [730 Dias]"
          : item.periodicidade == 1095 ? "Trianual [1095 Dias]"
          : item.periodicidade == 1460 ? "Quadrianual [1460 Dias]"
          
          : "",
          item.codigo,
          item.equipamento,
          item.modelo,
          item.fabricante,
          item.serie,
          
          item.instituicao,
          item.setor,
          item.area,
          
          
          
          `   
  <a class="btn btn-app"  href="mp-return-signature?id=${item.id}"  onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar Rotina',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-pencil"></i> Assinar
                  </a>

                                  
                 `
          
        ]);
        
        // Adiciona as novas linhas ao DataTables e desenha a tabela
        $('#datatable').DataTable().rows.add(novosDados).draw();
        
        
        // Cria os filtros após a tabela ser populada
        $('#datatable').DataTable().columns([1,2,3,4,5,6,7,8,9,10]).every(function (d) {
          var column = this;
          var theadname = $("#datatable th").eq([d]).text();
          var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
          var label = $('<label>'+theadname+': </label>').appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>').appendTo('#userstable_filter').select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column.data().unique().sort().each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
      });
    });
    
    
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#btn-carregar').click(function() {
        $.ajax({
          url: 'loading/load_equipamento.php', // substitua pelo nome do seu arquivo PHP que busca os itens restantes da tabela
          type: 'POST', // ou 'GET', dependendo da sua implementação
          data: {
            offset: 10 // substitua pelo número de itens que você já carregou
          },
          success: function(data) {
            $('#datatable').append(data); // adicione os novos itens à tabela
          }
        });
      });
    });
    
  </script>