<?php
include("database/database.php");

?>


<!--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->



<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Ordem de Serviço</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                    <div class="input-group">

                        <span class="input-group-btn">

                        </span>
                    </div>
                </div>
            </div>
        </div>
         <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
            <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

<!-- Menu -->
 
          
                <a class="btn btn-app <?php echo $current_page == 'os-open' ? 'active' : ''; ?>" href="os-open">
    <i class="fa fa-folder-open"></i> O.S Abertura
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-opened' ? 'active' : ''; ?>" href="os-opened">

    <i class="fa fa-play"></i>O.S Aberto 
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-progress' ? 'active' : ''; ?>" href="os-progress">
	 <i class="fa fa-cogs"></i>O.S Processo
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-signature' ? 'active' : ''; ?>" href="os-signature">

    <i class="fa fa-pencil"></i>O.S Assinatura
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-signature-user' ? 'active' : ''; ?>" href="os-signature-user">

    <i class="fa fa-user"></i>O.S Solicitante
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-finished' ? 'active' : ''; ?>" href="os-finished">

    <i class="fa fa-check-circle"></i>O.S Concluído
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-canceled' ? 'active' : ''; ?>" href="os-canceled">

    <i class="fa fa-times-circle"></i>O.S Cancelada
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-integration' ? 'active' : ''; ?>" href="os-integration">

    <i class="fa fa-code"></i>O.S Integração
</a>

                </div>
              </div>
              
              
              <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Solicitação <small> Descreva nos campos abaixo</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Parametro 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Parametro 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <div class="clearfix"></div>
                        <form class="dropzone" action="backend/os-upload-backend.php" method="post">
                        </form>
                        <div class="ln_solid"></div>
                        <!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
                        <form action="backend/os-open-backend.php?anexo=<?php printf($anexo);?>" method="post">




                            <div class="ln_solid"></div>

                            <div class="field item form-group">
                                <label
                                    class="col-form-label col-md-3 col-sm-3  label-align">Unidade<span></span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select type="text" class="form-control has-feedback-right" name="unidade"
                                        id="unidade" placeholder="Unidade">
                                      <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                                        <?php
                      

   $result_cat_post  = "SELECT instituicao.id, instituicao.instituicao FROM instituicao WHERE trash = 1 ";

   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
             while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
               echo '<option></option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
             }
                    ?>
                                    </select>
                                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                        class="docs-tooltip" data-toggle="tooltip"
                                        title="Selecione uma unidade "></span>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#unidade').select2();
                            });
                            </script>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Setor<span></span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select type="text" class="form-control has-feedback-right" name="setor" id="setor"
                                        placeholder="Setor">
                                          <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	  
                                    </select>
                                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                        class="docs-tooltip" data-toggle="tooltip" title="Selecione um Setor "></span>

                                </div>
                            </div>


                            <script>
                            $(document).ready(function() {
                                $('#setor').select2();
                            });
                            </script>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Area<span></span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select type="text" class="form-control has-feedback-right" name="area" id="area"
                                        placeholder="Area">
                                        <option value="">Selecione uma Opção</option>
                                    </select>
                                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                        class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Area "></span>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#area').select2();
                            });
                            </script>


                            <div class="field item form-group">
                                <label
                                    class="col-form-label col-md-3 col-sm-3  label-align">Equipamento<span>*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select type="text" class="form-control has-feedback-right" name="equipamento"
                                        id="equipamento" placeholder="equipamento" required>
                                        <option value="">Selecione o equipamento</option>
                                        <?php


					$query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.trash != 0 AND equipamento.ativo = 0 AND equipamento.baixa != 0";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
                                        <option value="<?php printf($id);?>	"><?php printf($codigo);?> -
                                            <?php printf($nome);?> - <?php printf($fabricante);?> - <?php printf($modelo);?> </option>
                                        <?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
                                    </select>
                                    <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"
                                        class="docs-tooltip" data-toggle="tooltip"
                                        title="Selecione o Equipamento "></span>
                                </div>



                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#equipamento').select2();
                            });
                            </script>

                            <div class="col-md-7 col-sm-7  form-group has-feedback">

                            </div>




                            <div class="ln_solid"></div>


                            <label for="middle-name">Solicitação</label>

                            <textarea id="solicitacao" class="form-control" name="solicitacao"
                                data-parsley-trigger="keyup" data-parsley-validation-threshold="10"
                                placeholder="<?php printf($id_solicitacao); ?>"
                                value="<?php printf($id_solicitacao); ?>"required ></textarea>



                            <div class="ln_solid"></div>

                            <label for="message">Restrição:</label>
                            <textarea id="message" class="form-control" name="restrictions"
                                placeholder="<?php printf($restrictions); ?>" ></textarea>

                            <div class="ln_solid"></div>



                            <div class="ln_solid"></div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3 col-sm-3 "></label>
                                <div class="col-md-9 col-sm-9 ">
                                    <center>

                                        <button type="reset" onclick="clean()" class="btn btn-primary" onclick="new PNotify({
																title: 'Limpado',
																text: 'Todos os Campos Limpos',
																type: 'info',
																styling: 'bootstrap3'
														});" />Limpar </button>
                                        <input type="submit" name="submit1" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar" />
                            <input type="submit" name="submit2" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar e Abrir" />
                             <input type="submit" name="submit3" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar e Fechar" />
                                    </center>
                                </div>
                            </div>

                    </div>
                    </form>




                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contador de caracter -->
<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    $('#unidade').change(function() {
        $('#setor').select2().load('sub_categorias_post.php?instituicao=' + $('#unidade').val());
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#setor').change(function() {
        $('#area').select2().load('sub_categorias_post_setor.php?area=' + $('#setor').val());
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#area').change(function() {
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor=' + $('#area')
    .val());
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#equipamento').change(function() {
        $('#area').select2().load('sub_setor.php?equipamento=' + $('#equipamento').val());

    });
});
$(document).ready(function() {
    $('#equipamento').change(function() {
        $('#setor').select2().load('sub_area.php?equipamento=' + $('#equipamento').val());

    });
});
$(document).ready(function() {
    $('#equipamento').change(function() {
        $('#unidade').select2().load('sub_unidade.php?equipamento=' + $('#equipamento').val());

    });
});
</script>

<script>
function clean() {
    $('#unidade').select2().load('reset_unidade.php');
    $('#area').select2().load('reset_area.php');
    $('#setor').select2().load('reset_setor.php');
    $('#equipamento').select2().load('reset_equipamento.php');


}
</script>


