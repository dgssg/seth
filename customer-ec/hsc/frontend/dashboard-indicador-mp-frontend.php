<?php
include("database/database.php");

$query = "SELECT id FROM os WHERE id_status like '1' ";

$aberto=0;
$andamento=0;
$fechado=0;
$cancelado=0;
$assinada_Executante=0;
$assinada_solicitante=0;
$concluido=0;

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
      $stmt->bind_result($id);
    while ($stmt->fetch()) {
     $aberto = $aberto + 1;
    }
  
}
$aberto=$aberto;
  
$query = "SELECT id FROM os WHERE id_status like '2' ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
      $stmt->bind_result($id);
    while ($stmt->fetch()) {
     $andamento = $andamento + 1;
    }
  
}
$andamento=$andamento;
  
$query = "SELECT id FROM os WHERE id_status like '3' ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
      $stmt->bind_result($id);
    while ($stmt->fetch()) {
     $fechado = $fechado + 1;
    }
  
}
$fechado=$fechado;
  
$query = "SELECT id FROM os WHERE id_status like '4' ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
      $stmt->bind_result($id);
    while ($stmt->fetch()) {
     $cancelado = $cancelado + 1;
    }
  
}
$cancelado=$cancelado;
  
$query = "SELECT id FROM os WHERE id_status like '5' ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
      $stmt->bind_result($id);
    while ($stmt->fetch()) {
     $assinada_Executante = $assinada_Executante + 1;
    }
  
}
$assinada_Executante=$assinada_Executante;
  



  
$query = "SELECT id FROM os WHERE id_status like '6' ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
      $stmt->bind_result($id);
    while ($stmt->fetch()) {
     $concluido = $concluido + 1;
    }
  
}
$concluido=$concluido;
  
?>

<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	exportEnabled: true,
	animationEnabled: true,
	title:{
		text: "Indicador OS"
	},
	legend:{
		cursor: "pointer",
		itemclick: explodePie
	},
	data: [{
		type: "pie",
		showInLegend: true,
		toolTipContent: "{name}: <strong>{y}%</strong>",
		indexLabel: "{name} - {y}%",
		dataPoints: [
		{ y: <?php print($aberto); ?>, name: "Aberto", exploded: true },
		{ y: <?php print($andamento); ?>, name: "Atrasado" },
		{ y: <?php print($fechado); ?>, name: "Fechado" },
		{ y: <?php print($cancelado); ?>, name: "Cencelado" }
	
		]
	}]
});
chart.render();
}

function explodePie (e) {
	if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
	} else {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
	}
	e.chart.render();

}
</script>
</head>
<body>

<div id="chartContainer" style="height: 100%; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>