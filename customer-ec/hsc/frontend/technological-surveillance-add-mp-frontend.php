  <?php
$codigoget = ($_GET["routine"]);

 

// Create connection

include("database/database.php");
$query = "SELECT maintenance_preventive.id_mp,maintenance_preventive.id_fornecedor,maintenance_preventive.date_end,maintenance_preventive.file,maintenance_preventive.time_mp,maintenance_preventive.obs_tc,maintenance_preventive.obs_mp,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine LEFT JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  LEFT JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id LEFT JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_tecnico,$id_fornecedor,$date_end,$id_anexo,$time_mp,$obs_tc,$obs_mp,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
}
}

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Evento Sentinela</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Descrição <small>da Solicitação</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                  
 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> " readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="categoria" name="categoria" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="info" class="form-control" type="text" name="info"  value="<?php printf($time_ms); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade (dias)</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($periodicidade); ?>" readonly="readonly">
                        </div>
                      </div>
                      
                     
                      
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
         
            
             <!-- page content -->
       
           

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Anexo <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">
                       <div class="col-md-55">
                          <?php if($id_anexo ==! "" ){ ?>
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="dropzone/os/<?php printf($id_anexo); ?>" alt="image" />
                            <div class="mask">
                              <p>Anexo</p>
                              <div class="tools tools-bottom">
                                <a href="dropzone/os/<?php printf($id_anexo); ?>" download><i class="fa fa-download"></i></a>
                                
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>Anexo ordem de serviço</p>
                          </div>
                        </div>
                          <?php }; ?>
                      </div>
                     
                     
                    
                   
                  

                   
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          
         <div class="x_panel">
                <div class="x_title">
                  <h2>Tecnovigilancia</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                     <form  action="backend/technological-surveillance-add-mp-backend.php?routine=<?php printf($codigoget); ?>" method="post">
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Descrição</label>
                        <div class="col-md-6 col-sm-6 ">
                        	<textarea  type="text" class="form-control has-feedback-left" id="descricao" name="descricao" placeholder="Descrição" data-ls-module="charCounter" maxlength="1000" data-parsley-maxlength="1000" data-parsley-trigger="keyup" data-parsley-maxlength="1000" data-parsley-minlength-message="Vamos! Você precisa inserir um comentário de pelo menos 20 caracteres."
                            data-parsley-validation-threshold="20"> </textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>

<label for="descricao">Descrição:</label>
<textarea id="descricao" class="form-control" name="descricao" ></textarea>

<div class="ln_solid"></div>

                   <div class="ln_solid"></div>
                   
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
                        <div class="col-md-6 col-sm-6 ">
                         <div class="">
                            <label>
                              <input name="status"type="checkbox" class="js-switch"  value="0"  /> 
                            </label>
                          </div>
                        </div>
                      </div>
                   
                          <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="technological_surveillance_list" class="docs-tooltip" data-toggle="tooltip" title="Selecione um Risco ">Risco <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="technological_surveillance_list" id="technological_surveillance_list"   >
							
										  	<?php
										  	
										  
										  	
										    $sql = "SELECT  id, codigo,nome FROM technological_surveillance_list ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$codigo,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                      
     
                     
									
									
										
									
								     
								 <script>
                                    $(document).ready(function() {
                                    $('#technological_surveillance_list').select2();
                                      });
                                 </script>
      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Inicio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_inicio"  type="date" name="data_inicio" class="form-control" >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Fim</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_fim"  type="date" name="data_fim"  class="form-control" >
                        </div>
                      </div>
                      

                      <div class="ln_solid"></div>

<label for="acao">Ação:</label>
<textarea id="acao" class="form-control" name="acao" ></textarea>


                      <div class="ln_solid"></div>

<label for="obs">Observação:</label>
<textarea id="obs" class="form-control" name="obs" ></textarea>

                   <div class="ln_solid"></div>
                  
    
                   <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>
     
     
                                 
                  
                </div>
                 </form>
              </div>
              
         
   <div class="clearfix"></div>

         
        <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- Posicionamento -->
    
	         
	       
	       
            <div class="clearfix"></div>
  
             
            
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="maintenance-preventive">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
              
                 
                   
             <!--    ref="backend/os-progress-upgrade-close-frontend.php?os="
             
             <a class="btn btn-app"  href=""onclick="new PNotify({
																title: 'Atualização',
																text: 'Atualização de Abertura de O.S!',
																type: 'danger',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                  </a> -->
                
               
                  
                </div>
              </div>
              
              
              

            

            
        </div>
            </div>
        <!-- /page content -->
     <!-- Switchery -->
    <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
      <!-- Switchery -->
    <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">