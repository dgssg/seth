  <?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Analisador e Simulador</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>




             <!-- page content -->








              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro <small>do Relatório</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="backend/report-report-equipament-calibration-backend.php" method="post"  target="_blank">

 <div class="ln_solid"></div>



  



 

                      
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Analisador/Simulador</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="tools" id="tools"  placeholder="Fabricante">
                            <option value="">Selecione um Analisador/Simulador</option>
                            <?php
                              $sql = "SELECT  id, nome FROM calibration_parameter_tools  WHERE trash = 1";
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id_tools,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id_tools);?>">  <?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              }
                              $stmt->close();
                            ?>
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Fabricante "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#tools').select2();
                        });
                      </script>           
 
<div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Fabricante</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="manufacture" id="manufacture"  placeholder="Fabricante">
										  <option value="">Selecione um Fabricante</option>
                      <?php
                        $sql = "SELECT  id, fabricante FROM calibration_parameter_tools  WHERE trash = 1";
                                        if ($stmt = $conn->prepare($sql)) {
                                    $stmt->execute();
                                        $stmt->bind_result($id_manufacture,$fabricante);
                                        while ($stmt->fetch()) {
                                              ?>
                                          <option value="<?php printf($id_manufacture);?>">  <?php printf($fabricante);?>	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }
                                          }
                    $stmt->close();
                    ?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Fabricante "></span>

                        </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#manufacture').select2();
                                      });
                                 </script>
                                  <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Modelo</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="model" id="model"  placeholder="Modelo">
										  <option value="">Selecione o Modelo</option>
                      <?php
                        $sql = "SELECT  id, modelo FROM calibration_parameter_tools  WHERE trash = 1";
                                        if ($stmt = $conn->prepare($sql)) {
                                    $stmt->execute();
                                        $stmt->bind_result($id_model,$modelo);
                                        while ($stmt->fetch()) {
                                              ?>
                                          <option value="<?php printf($id_model);?>"> <?php printf($modelo);?> 	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }
                                          }
                    $stmt->close();
                    ?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Modelo "></span>

                        </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#model').select2();
                                      });
                                 </script>


 
<div class="ln_solid"></div>
   
           
<div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Laudo Vencido</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="laudo" id="laudo" >
										  <option value="">Selecione o item</option>
                      <option value="0">Ambos</option>
                      <option value="1">Sim</option>
                      <option value="2">Não</option>
                    
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>

                        </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#laudo').select2();
                                      });
                                 </script>

             
 
                                        <div class="ln_solid"></div>
                                        <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ativo</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="ativo" id="ativo" >
										  <option value="">Selecione o item</option>
                      <option value="0">Ambos</option>
                      <option value="1">Sim</option>
                      <option value="2">Não</option>
                    
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>

                        </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#ativo').select2();
                                      });
                                 </script>


                 
                                        <div class="ln_solid"></div>
                                        <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Excluido</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="trash" id="trash" >
										  <option value="">Selecione o item</option>
                      <option value="0">Ambos</option>
                      <option value="1">Sim</option>
                      <option value="2">Não</option>
                    
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>

                        </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#trash').select2();
                                      });
                                 </script>

               

                                        <div class="ln_solid"></div>



                     <br>


                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatório</button>
                         </center>
                        </div>
                      </div>




                  </div>
                </div>
              </div>
            </div>






             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="report-report">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




                </div>
              </div>




              <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>


        </div>
            </div>
        <!-- /page content -->
     <script type="text/javascript">
    $(document).ready(function(){
        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });
    </script>
     <script type="text/javascript">
    $(document).ready(function(){
        $('#area').change(function(){
            $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
        });
    });
    </script>
