 <?php

include("../database/database.php");

$codigoget= $_GET['id'];

$ve= $_POST['ve'];

$vl_1= $_POST['vl_1'];
$vl_2= $_POST['vl_2'];
$vl_3= $_POST['vl_3'];
//$k= $_GET['k'];
$k= $_POST['k']; //A linha referente ao Grau de Liberdade Efetivo na coluna 0,025 da tabela T-Student, quando calculado.
//$tools = $_POST['tools']; 
$tools= $_GET['tools'];
//$d_1= $_POST['d_1'];
//$d_2= $_POST['d_2'];
//$erro_ld= $_POST['erro'];
$d_1= $_GET['d_1'];
$d_2= $_GET['d_2'];
$erro_ld= $_GET['erro'];
$id_tools=trim($tools);
$query="SELECT i_h,id,id_calibration_parameter_tools FROM calibration_parameter_tools_dados WHERE id like '$id_tools' ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($ih,$id_dados,$id_calibration_parameter_tools);
  

 while ($stmt->fetch()) {
     $ih;
     $id_calibration_parameter_tools=$id_calibration_parameter_tools;
 }
}
$ih;
$ibn=$ih;  //Esse cálculo só se aplica se a Incerteza for do tipo B, e o tipo for, (resolução ou outras), então é a estimativa convertida dividida pela distribuição

$vl_avg= ($vl_1 + $vl_2 + $vl_3)/3; //Valor medio

$erro=abs($ve-$vl_avg); //Erro

$dp=sqrt((pow(($vl_1-$vl_avg),2)+pow(($vl_2-$vl_avg),2)+pow(($vl_3-$vl_avg),2))/3); // Desvio Padrão

$ia=$dp/(sqrt(3)); // Incerteza do Tipo A / Repetitividade


$ib= $ibn; //É a raiz quadrada das somas das incertezas do tipo B ao quadrado.

$ic=sqrt((pow($ib,2)+pow($ia,2))); //É a raiz quadrada da soma da incerteza do tipo B ao quadrado mais a incerteza do tipo A ao quadrado.

$ie=$ic*$k;//É a Incerteza Combinada vezes o fator de Abrangência.
$erro_ld=trim($erro_ld);
//if($erro_ld=="0"){
  $ap= $ie+$vl_avg;
  $ac=$vl_avg+$d_1;
  if($ap<$ac){
      $status=1;
  }
  if($ap>$ac){
      $status=0;
  }
//}

$stmt = $conn->prepare("UPDATE calibration_report SET ve = ? WHERE id= ?");
$stmt->bind_param("ss",$ve,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET vl_1 = ? WHERE id= ?");
$stmt->bind_param("ss",$vl_1,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET vl_2 = ? WHERE id= ?");
$stmt->bind_param("ss",$vl_2,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET vl_3 = ? WHERE id= ?");
$stmt->bind_param("ss",$vl_3,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET vl_avg = ? WHERE id= ?");
$stmt->bind_param("ss",$vl_avg,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET erro = ? WHERE id= ?");
$stmt->bind_param("ss",$erro,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET dp = ? WHERE id= ?");
$stmt->bind_param("ss",$dp,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ia = ? WHERE id= ?");
$stmt->bind_param("ss",$ia,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ie = ? WHERE id= ?");
$stmt->bind_param("ss",$ie,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET k = ? WHERE id= ?");
$stmt->bind_param("ss",$k,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ib = ? WHERE id= ?");
$stmt->bind_param("ss",$ib,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ic = ? WHERE id= ?");
$stmt->bind_param("ss",$ic,$codigoget);
$execval = $stmt->execute();
$stmt->close();
//$stmt = $conn->prepare("UPDATE calibration_report SET id_calibration_parameter_tools = ? WHERE id= ?");
//$stmt->bind_param("ss",$id_calibration_parameter_tools,$codigoget);
//$execval = $stmt->execute();
//$stmt->close();
//$stmt = $conn->prepare("UPDATE calibration_report SET id_calibration_parameter_tools_dados = ? WHERE id= ?");
//$stmt->bind_param("ss",$tools,$codigoget);
//$execval = $stmt->execute();
//$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_report SET status = ? WHERE id= ?");
$stmt->bind_param("ss",$status,$codigoget);
$execval = $stmt->execute();
$stmt->close();

echo "<script>window.close();</script>";
//header('Location: ../calibration-report-edit-new?laudo='.$codigoget);
?>