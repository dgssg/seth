<?php
include("database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
	if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
} if( $_SESSION['mod'] != $mod ){
    header ("Location: ../index.php");
}
	require_once 'auth.php';
	require_once 'permissions.php';
	// Verificar se o usuário é admin
	checkPermission('1', $pdo);
	// Verificar se o usuário está autenticado
	if (!isset($_SESSION['session_token'])) {
		header('Location: ../../index.php');
		exit;
	}
	
	// Valida o token de sessão
	$stmt = $conn->prepare("SELECT * FROM usuario WHERE session_token = ? AND is_active = TRUE");
	$stmt->bind_param('s', $_SESSION['session_token']);
	$stmt->execute();
	$result = $stmt->get_result();
	$user = $result->fetch_assoc();

	if (!$user) {
		// Token inválido ou sessão expirada
		session_destroy();
		header('Location: ../../index.php');
		exit;
	}
	
	// Função para obter a saudação com base no horário do dia
	function saudacao() {
		$hora = date('H');
		if ($hora < 12) {
			return "Bom dia";
		} elseif ($hora < 18) {
			return "Boa tarde";
		} else {
			return "Boa noite";
		}
}	
	$query = "SELECT budget FROM tools";	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($budget_status);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$query = "SELECT assistence FROM tools";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($assistence);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];

	include("database/database.php");


	$codigoget = ($_GET["id"]);

	date_default_timezone_set('America/Sao_Paulo');
 
      $print= date(DATE_RFC2822);


$query = "SELECT maintenance_preventive.file,maintenance_preventive.id,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_anexo,$id,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }
}



?>



                              <!--  <img id="ctl00_ContentPlaceHolder1_lst_ctl00_imgQRCode" src="http://chart.apis.google.com/chart?cht=qr&amp;chl=http://www.manuteh.com.br/ipo/e/13216&amp;chs=82x82" style="border-width:0px;" /> -->




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>

</title>
    <script type="text/javascript" language="JavaScript">
<!--
    function printPage() {

        if (window.print) {

            agree = confirm("Deseja imprimir essa página ?");

            if (agree) {
                window.print();

                if (_GET("pg") != null)
                    location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
                else
                    history.go(-1);


                window.close();
            }



        }
    }

    function noPrint() {
        try {

            if (_GET("pg") != null)
                location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
            else
                history.go(-1);

            window.close();

        }
        catch (e) {
            alert();
            document.write("Error Message: " + e.message);
        }
    }

    function _GET(name) {
        var url = window.location.search.replace("?", "");
        var itens = url.split("&");

        for (n in itens) {
            if (itens[n].match(name)) {
                return decodeURIComponent(itens[n].replace(name + "=", ""));
            }
        }
        return null;
    }





    function visualizarImpressao() {

        var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

        document.body.insertAdjacentHTML("beforeEnd", Navegador);

        Navegador1.ExecWB(7, 1);

        Navegador1.outerHTML = "";

    }



    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-11247563-6', 'auto');
    ga('send', 'pageview');

    // -->
    </script>

    <style type="text/css">
        page {
            /* background: white;*/
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            text-align: left;
        }

            page[size="A4"] {
                width: 21cm;
                /*height: 29.7cm;                */
            }

                page[size="A4"][layout="portrait"] {
                    width: 29.7cm;
                    /* height: 21cm; */
                }

        @media print {
            body,
            page {
                margin: 0;
                box-shadow: 0;
                page-break-after:always;
            }

            .noprint {
                display: none;
            }

            .quebra {
                page-break-before: always;
            }
        }




        body {
            font-size: 11px;
            font-family: sans-serif;
        }



        .pagina_retrato {
            width: 720px;
        }

        .pagina_paisagem {
            width: 1025px;
        }

        .relatorio_filtro {
            padding: 10px 0px;
            border-collapse: collapse;
        }

            .relatorio_filtro tr td {
                border: 1px solid #000;
            }


        .relatorio_titulo {
            font-family: Times New Roman;
            font-size: 15px;
            color: Black;
            font-weight: bold;
            text-transform: uppercase;
        }

        .borda_celula {
            border: 1px solid #000;
            background-color: #fff;
        }

        .noprint {
            padding-bottom: 10px;
        }

          .table_collapse {
            border-collapse: collapse;
        }

            .table_collapse tr th {
                border: 1px solid black;
                padding: 3px;
                background-color: #f0f0f0;
            }

            .table_collapse tr td {
                border: 1px solid black;
                padding: 3px;
            }
    </style>

    <style type="text/css" media="print">
        /*
        @page {
            margin: 0.8cm;
        }
        */
        .pagina_retrato, .pagina_paisagem {
            width: 100%;
        }
    </style>



   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />

    <style>
        body {
            background: transparent !important;
            color: #000 !important;
            text-shadow: none !important;
            filter: none !important;
            -ms-filter: none !important;
            margin: 0;
            padding: 0;
            line-height: 1em;
        }

        @media print {
            .quebra {
                page-break-before: always;
            }
        }
    </style>
<link href="../../../App_Themes/Tema1/StyleSheet.css" type="text/css" rel="stylesheet" /></head>
<body>
    <form name="aspnetForm" method="post" action="./1676" id="aspnetForm">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTI2NzM0NjA5Mw9kFgJmD2QWAgIDD2QWAgIBD2QWAgIBDxYCHgtfIUl0ZW1Db3VudAIBFgJmD2QWCgIBDw8WBB4ISW1hZ2VVcmwFbGh0dHA6Ly9jaGFydC5hcGlzLmdvb2dsZS5jb20vY2hhcnQ/Y2h0PXFyJmNobD1odHRwOi8vd3d3Lm1hbnV0ZWguY29tLmJyL3NhbnRhY2FzYWN1cml0aWJhL2UvMTY3NiZjaHM9MTUweDE1MB4HVmlzaWJsZWdkZAIFDw8WAh4EVGV4dAUKRUxFVkFET1JFU2RkAgcPDxYCHwMFJ1RIWVNTRU5LUlVQUCBFTEVWQURPUkVTIFMuQS4gVEsgUFJFTUlVTWRkAgkPDxYCHwMFAzAwMWRkAgsPDxYCHwMFDsOBUkVBUyBDT01VTlMgZGRkPuF1RigL6VFnJEc3lOqgJmk9lyxKQu8FMaaF7n/baMs=" />
</div>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="63A739F7" />
</div>

        <div class="noprint">
            <center>
                <input type="button" value="Não Imprimir" onclick="noPrint();">
                <input type="button" value="Imprimir" onclick="printPage();" />
                <!-- <input type="button" onclick="visualizarImpressao();" value="Visualizar Impressão"  />-->
            </center>
        </div>


    <center>


                <div style="width: 7cm; height: 3.8cm; overflow: hidden; text-align: center; line-height: 12px; margin: 2px;" class="quebra">
                    <table style="width: 100%; height: 100%; font-size: 10px; font-weight: bold; font-family: Calibri,'Segoe UI','Trebuchet MS'; padding-top: 0px; padding-bottom: 0px; padding-left: 2px; padding-right: 10px;">
                        <tr>
 <span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblArea" style="font-size:15px;font-weight:bold;">  </span>
                            <td style="vertical-align: middle; margin: 1px; padding: 1px;">

                                 <img src="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=www.seth.mksistemasbiomedicos.com.br/customer-ec/<?php printf($instituicaologado); ?>/maintenance-preventive-viwer?id=<?php printf($codigoget); ?>" alt="MK Sistemas Biomedicos" width="100" />

													  </td>
														<td style="vertical-align: middle; text-align: center; margin: 0px; padding: 0px; padding-top: 0px;">
																<!--<br />
																<br />-->
																<!--<span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblDescricao" style="font-size:12px;font-weight:bold;"> <?php printf($nome)?></span><br />
																<span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblMarcaModelo" style="font-size:12px;font-weight:bold;"> <?php printf($modelo)?></span>
																<br />-->

																<br />
																	 <span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblMarcaModelo" style="font-size:18px;font-weight:bold;"> <?php printf($codigo)?></span>
																<br />
																<br />
																<span style="font-size: 15px; font-weight: bold;">Realizada:</span><br />
																<span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblCodigo" style="font-size:16px;font-weight:bold;"><?php 	 echo date("m-Y", strtotime($print)); ?></span><br />
																<span style="font-size: 15px; font-weight: bold;">Proxima:</span><br />
																<span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblCodigo" style="font-size:16px;font-weight:bold;"><?php

																if($periodicidade==365){

																	$data_after=date('Y-m-d', strtotime($programada.' + 12 months'));

																}

																if($periodicidade==180){

																	$data_after=date('Y-m-d', strtotime($programada.' + 6 months'));

																}

																if($periodicidade==30){

																	$data_after=date('Y-m-d', strtotime($programada.' + 1 months'));

																}
                                                                if($periodicidade==5){

                                                                    date_default_timezone_set('America/Sao_Paulo');
                                                                    function getListaDiasFeriado($ano = null) {
                                                                    
                                                                        if ($ano === null) {
                                                                            $ano = intval(date('Y'));
                                                                        }
                                                                    
                                                                        $pascoa = easter_date($ano); // retorna data da pascoa do ano especificado
                                                                        $diaPascoa = date('j', $pascoa);
                                                                        $mesPacoa = date('n', $pascoa);
                                                                        $anoPascoa = date('Y', $pascoa);
                                                                    
                                                                        $feriados = [
                                                                            // Feriados nacionais fixos
                                                                            mktime(0, 0, 0, 1, 1, $ano),   // Confraternização Universal
                                                                            mktime(0, 0, 0, 4, 21, $ano),  // Tiradentes
                                                                            mktime(0, 0, 0, 5, 1, $ano),   // Dia do Trabalhador
                                                                            mktime(0, 0, 0, 9, 7, $ano),   // Dia da Independência
                                                                            mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida
                                                                            mktime(0, 0, 0, 11, 2, $ano),  // Todos os santos
                                                                            mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica
                                                                            mktime(0, 0, 0, 12, 25, $ano), // Natal
                                                                            //
                                                                            // Feriados variaveis
                                                                            mktime(0, 0, 0, $mesPacoa, $diaPascoa - 48, $anoPascoa), // 2º feria Carnaval
                                                                            mktime(0, 0, 0, $mesPacoa, $diaPascoa - 47, $anoPascoa), // 3º feria Carnaval 
                                                                            mktime(0, 0, 0, $mesPacoa, $diaPascoa - 2, $anoPascoa),  // 6º feira Santa  
                                                                            mktime(0, 0, 0, $mesPacoa, $diaPascoa, $anoPascoa),      // Pascoa
                                                                            mktime(0, 0, 0, $mesPacoa, $diaPascoa + 60, $anoPascoa), // Corpus Christ
                                                                        ];
                                                                    
                                                                        sort($feriados);
                                                                    
                                                                        $listaDiasFeriado = [];
                                                                        foreach ($feriados as $feriado) {
                                                                            $data = date('Y-m-d', $feriado);
                                                                            $listaDiasFeriado[$data] = $data;
                                                                        }
                                                                    
                                                                        return $listaDiasFeriado;
                                                                    }
                                                                    
                                                                    function isFeriado($data) {
                                                                        $listaFeriado = getListaDiasFeriado(date('Y', strtotime($data)));
                                                                        if (isset($listaFeriado[$data])) {
                                                                            return true;
                                                                        }
                                                                    
                                                                        return false;
                                                                    }
                                                                    
                                                                    function getDiasUteis($aPartirDe, $quantidadeDeDias = 30) {
                                                                        $dateTime = new DateTime($aPartirDe);
                                                                    
                                                                        $listaDiasUteis = [];
                                                                        $contador = 0;
                                                                        while ($contador < $quantidadeDeDias) {
                                                                            $dateTime->modify('+1 weekday'); // adiciona um dia pulando finais de semana
                                                                            $data = $dateTime->format('Y-m-d');
                                                                            if (!isFeriado($data)) {
                                                                                $listaDiasUteis[] = $data;
                                                                                $contador++;
                                                                            }
                                                                        }
                                                                    
                                                                        return $listaDiasUteis;
                                                                    }
                                                                    $today = $programada;
                                                                    //$today = "2023-01-06";
                                                                    
                                                                    $listaDiasUteis = getDiasUteis($today, 15);
                                                                    $ultimoDia = end($listaDiasUteis);
                                                                    
                                                                    //echo "<pre>";
                                                                    //print_r($listaDiasUteis);
                                                                    //echo "</pre>";
                                                                    
                                                                    //echo "ULTIMO DIA: " . $ultimoDia;
                                                                    //print_r("\n");
                                                                    //echo " DIA: " . $today;
                                                                    //print_r("\n");
                                                                    $i=0;
                                                                    $key=true;
                                                                    do {
                                                                      if($listaDiasUteis[$i] > $today ){
                                                                      //	echo " DIA Proximo: " . $listaDiasUteis[$i] ;
                                                                        $DiasUteis = $listaDiasUteis[$i];
                                                                        $key=false;
                                                                      }
                                                                      else
                                                                      {
                                                                      $i	= $i +1;
                                                                      
                                                                      }
                                                                    } while($key);
                                                                    
                                                                      $data_after = $DiasUteis;
                                                                    
                                                                  }

																if($periodicidade==1){

																	$data_after=date('Y-m-d', strtotime($programada.' + 1 days'));

																}

																if($periodicidade==7){

																	$data_after=date('Y-m-d', strtotime($programada.' + 7 days'));


																}

																if($periodicidade==14){

																	$data_after=date('Y-m-d', strtotime($programada.' + 14 days'));


																}

																if($periodicidade==21){

																	$data_after=date('Y-m-d', strtotime($programada.' + 21 days'));


																}

																if($periodicidade==28){

																	$data_after=date('Y-m-d', strtotime($programada.' + 28 days'));


																}

																if($periodicidade==60){

																	$data_after=date('Y-m-d', strtotime($programada.' + 2 months'));


																}

																if($periodicidade==90){

																	$data_after=date('Y-m-d', strtotime($programada.' + 3 months'));


																}

																if($periodicidade==120){

																	$data_after=date('Y-m-d', strtotime($programada.' + 4 months'));


																}

																if($periodicidade==730){

																	$data_after=date('Y-m-d', strtotime($programada.' + 24 months'));


																}
																 echo date("m-Y", strtotime($data_after));
																  ?></span><br />
																 <br />
 <span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblArea" style="font-size:15px;font-weight:bold;"> MANUTENÇÃO PREVENTIVA <?php printf($rotina); ?> </span>
                            </td>
                        </tr>
                    </table>
                </div>


    </center>



    </form>





     <script type="text/javascript">

         //jQuery(document).ready(function () {
         function pageLoad() {





        }
        // });
    </script>
    <!-- END JAVASCRIPTS -->


</body>
</html>
