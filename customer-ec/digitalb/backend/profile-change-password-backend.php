<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
$codigoget = ($_GET["usuario"]);
//$codigoget=trim($codigoget);
include("../database/database.php");

$setor = $_SESSION['setor'];

$usuario = $_SESSION['id_usuario'];

$password= $_POST['password'];
$password=trim($password);
//$password = 'seth123'; // A senha que você deseja hashear
$hashedPassword = password_hash($password, PASSWORD_BCRYPT); //
$stmt = $conn->prepare("UPDATE usuario SET senha = ? WHERE id= ?");
$stmt->bind_param("ss",$password,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE usuario SET password_hash = ? WHERE id= ?");
$stmt->bind_param("ss",$hashedPassword,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../profile');

?>

