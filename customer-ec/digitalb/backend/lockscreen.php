<?php
session_start();

include("../database/database.php");

$database = $base . $_SESSION['instituicao'];
$usuariologado = $_SESSION['email'];
$instituicaologado =	$_SESSION['instituicao'];
$setor = 	$_SESSION['setor'];
 $session_token= $_SESSION['session_token'];

// Requer o arquivo para registro de auditoria
require_once '../../../connection2/audit_log.php';


// Recebendo os dados do formulário
$usuario = $_SESSION['usuario'];
$senha = $_POST['password'];

// Consultando o usuário no banco
$stmt = $conn->prepare('SELECT * FROM usuario WHERE username = ? AND is_active = 1');
if (!$stmt) {
    die('Erro na preparação da consulta: ' . $conn->error);
}
$stmt->bind_param('s', $usuario);
$stmt->execute();
$result = $stmt->get_result();
$user = $result->fetch_assoc();
$stmt->close();

// Verificando as credenciais do usuário
if ($user && password_verify($senha, $user['password_hash']) && $session_token == $user['session_token']) {
    // Gerar token de sessão único
    $sessionToken = bin2hex(random_bytes(32));

    // Atualizar o último acesso e token de sessão
    $updateStmt = $conn->prepare('UPDATE usuario SET last_acess = NOW(), session_token = ?, last_ip = ? WHERE id = ?');
    $updateStmt->bind_param('ssi', $sessionToken, $_SERVER['REMOTE_ADDR'], $user['id']);
    $updateStmt->execute();
    $updateStmt->close();

    // Registro de auditoria
    logAction($user['id'], 'Login bem-sucedido.');

    // Configurar as variáveis de sessão
    $_SESSION['session_token'] = $sessionToken;
    $_SESSION['id_user'] = $user['id'];
    $_SESSION['usuario'] = $user['username'];
    $_SESSION['id_usuario'] = $user['id'];
    $_SESSION['instituicao'] = $_SESSION['instituicao'];
    $_SESSION['setor'] = $user['id_setor'];
    $_SESSION['email'] = $user['email'];
    $_SESSION['id_nivel'] = $user['id_nivel'];
    $_SESSION['time'] = time() + 30;
    $_SESSION['mod'] = $mod;

    // Redirecionar com base no nível de acesso
    $nivel = $user['id_nivel'];
    $redirect_url = "../dashboard";
} else {
    // Login falhou
    logAction(null, 'Tentativa de login falhou para username: ' . $usuario);
    $_SESSION['error'] = 'Usuário ou senha inválidos.';
    $redirect_url = '../logout.php';
}

// Redirecionar para a página correspondente
header("Location: $redirect_url");
exit;
?>
