<?php
session_start();
if(!isset($_SESSION['usuario'])){
    header ("Location: ../index.php");
}
$usuariologado=$_SESSION['id_usuario'];
$instituicaologado=$_SESSION['instituicao'];
$setorlogado=$_SESSION['setor'];

include("../database/database.php");
date_default_timezone_set('America/Sao_Paulo');
$date= date(DATE_RFC2822);

$new_manufacture=$_COOKIE['new_manufacture'];

//include("equipament-group-upload-nf-backend.php");



$anexo=$_COOKIE['anexo'];


$id_equipamento_familia=$_POST['equipamento_familia'];
$codigo=$_POST['codigo'];
$patrimonio=$_POST['patrimonio'];
$serie=$_POST['numeroserie'];
$data_instal=$_POST['data_instal'];
$data_fab=$_POST['data_fab'];
$data_calibration_end=$_POST['data_calibration_end'];
$nf=$_POST['nf'];
$vlr=$_POST['vlr'];
$data_val=$_POST['data_val'];
$obs=$_POST['obs'];

$id_instituicao_localizacao=$_POST['setor'];
$id_fornecedor=$_POST['id_fornecedor'];

$baixa=$_POST['baixa'];
$ativo=$_POST['ativo'];
$inventario=$_POST['inventario'];
$comodato=$_POST['comodato'];
$duplicidade=$_POST['duplicidade'];
$preventive=$_POST['preventive'];
$predictive=$_POST['preditiva'];
$obsolecencia=$_POST['obsolecencia'];
$rfid=$_POST['rfid'];
$contrato=$_POST['contrato'];
$terceiro=$_POST['terceiro'];
$locacao= $_POST['locacao'];
$list=$_POST['list'];
$ass_adm=$_POST['ass_adm'];
$ass_user=$_POST['ass_user'];
$instal=$_POST['instal'];
$routine=$_POST['routine'];

$assinature = $_POST['assinature'];
$chave= $_POST['chave'];

if(trim("$chave") == trim("$assinature")){ 
    $v4uuid = $_POST['v4uuid'];
}




$default="Equipamento cadastrado no sistema em $date";
//$default=$default.$date;
if($new_manufacture != "" ){
$id_fornecedor =  $new_manufacture;
}
$stmt = $conn->prepare("INSERT INTO equipamento (id_equipamento_familia, codigo, patrimonio, serie, data_instal, data_fab, nf, data_val, obs, id_instituicao_localizacao, id_fornecedor, baixa, ativo, inventario, comodato, duplicidade, preventive, obsolescence, rfid,nf_dropzone,data_calibration_end,vlr,contrato,locacao,terceiro) VALUES ( ?,?,?,?, ?, ?, ?, ?, ?, ?, ?,?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)");

$stmt->bind_param("sssssssssssssssssssssssss",$id_equipamento_familia, $codigo, $patrimonio, $serie, $data_instal, $data_fab, $nf, $data_val, $obs, $id_instituicao_localizacao, $id_fornecedor, $baixa, $ativo, $inventario, $comodato, $duplicidade, $preventive, $obsolecencia, $rfid,$anexo,$data_calibration_end,$vlr,$contrato,$locacao,$terceiro);
$execval = $stmt->execute();
 $last_id = $conn->insert_id;
 $stmt->close();
//echo "New records created successfully";
 $stmt = $conn->prepare("INSERT INTO equipament_location (id_equipamento, id_localizacao, obs) VALUES (?, ?, ?)");
 $stmt->bind_param("sss",$last_id,$id_instituicao_localizacao,$default);
 $execval = $stmt->execute();
 $stmt->close();
 $id_equipamento = $last_id;

 if($list != "" ){

 $stmt = $conn->prepare("INSERT INTO equipament_check_install (id_equipamento, id_equipament_check_mod, signature_admin,list,ass_adm,ass_user) VALUES (?, ?,?, ?,?,?)");
$stmt->bind_param("ssssss",$id_equipamento,$instal,$v4uuid,$list,$ass_adm,$ass_user);
$execval = $stmt->execute();
$last_id = $conn->insert_id;
$stmt->close();

$stmt = $conn->prepare("INSERT INTO  regdate_equipament_check_install (id_equipamento_check_install, id_user, id_signature) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$last_id,$usuariologado,$v4uuid);
$execval = $stmt->execute();
$stmt->close();

$query = "SELECT  id FROM equipament_check_list ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
	$stmt->execute();
	$stmt->bind_result($id);
	while ($stmt->fetch()) {
	  $codigos[]  = $id;
    $count=$count+1;

	}
}

//echo implode( ', ', $codigos );

for ($i=0; $i < $count ; $i++) {
 $id_question=$codigos[$i];
 $avaliable=$_POST[$codigos[$i]];
 $obs=$_POST['t_'.$codigos[$i]];


$stmt = $conn->prepare("INSERT INTO equipament_check_list_iten (id_equipamento_check_install, id_equipament_check_list, avaliable,obs) VALUES (?,?, ?, ?)");
$stmt->bind_param("ssss",$last_id,$id_question,$avaliable,$obs);
$execval = $stmt->execute();
$stmt->close();
}


 }

setcookie('new_manufacture', null, -1);
if($routine != "" ){
header('Location: ../maintenance-routine-register?id_equipamento=' . $id_equipamento . '&fornecedor=' . $id_fornecedor);
}else{
header('Location: ../register-equipament');
}
//echo '<script type="text/javascript">';
//echo ' alert("JavaScript Alert Box by PHP ")';  //not showing an alert box.
//echo '</script>';
?>
