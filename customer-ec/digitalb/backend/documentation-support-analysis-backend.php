<?php


include("../database/database.php");


$id = $_GET['id'];

$titulo = $_POST['titulo'];
$anexo=$_COOKIE['anexo'];

$stmt = $conn->prepare("INSERT INTO regdate_suport_dropzone (id_suport, file,titulo) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$id,$anexo,$titulo);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
header('Location: ../documentation-suport-edit.php?id='.$id);
?>