
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Planejamento Depreciação</h3>
              </div>


            </div>
   

            <div class="x_panel">
               <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                           class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                           <li><a href="#">Settings 1</a>
                           </li>
                           <li><a href="#">Settings 2</a>
                           </li>
                        </ul>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  
                                  
                  <a class="btn btn-app"  href="obsolescence-depreciation-list">
                     <i class="glyphicon glyphicon-list-alt"></i> Consulta
                  </a>
                  
                  <a class="btn btn-app"  href="obsolescence-depreciation">
                     <i class="glyphicon glyphicon-plus"></i> Cadastro
                     
                  </a>
                  
                                  
                  
                  
               </div>
            </div>



  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Filtro</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
                            <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>

         
        </div>
      </div>
    </div>
  </div>
                <div class="x_panel">
                       <div class="x_title">
                         <h2>Planejamento Depreciação</h2>
                         <ul class="nav navbar-right panel_toolbox">
                           <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                           </li>
                           <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                 class="fa fa-wrench"></i></a>
                             <ul class="dropdown-menu" role="menu">
                               <li><a href="#">Settings 1</a>
                               </li>
                               <li><a href="#">Settings 2</a>
                               </li>
                             </ul>
                           </li>
                           <li><a class="close-link"><i class="fa fa-close"></i></a>
                           </li>
                         </ul>
                         <div class="clearfix"></div>
                       </div>
                       <div class="x_content">



                 <!-- PAGE CONTENT BEGINS -->
                           <div class="col-md-11 col-sm-11 ">
                              <div class="x_panel">
                                 
                                 <div class="x_content">
                                    <div class="row">
                                       <div class="col-sm-12">
                                          <div class="card-box table-responsive">
                                             
       							
                           <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>Codigo</th>
                                    <th>Equipamento</th>
                                    <th>Modelo</th>
                                    <th>Fabricante</th>
                                    
                                    <th>N/S</th>
                                      <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                                      <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                                      <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  
                                    
                                    <th>Ano</th>
                        
                                    <th>Ação</th>
                                    
                                 </tr>
                              </thead>
                              
                           </table>
                           
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           <!-- PAGE CONTENT ENDS -->




                       </div>
                     </div>



                    <!-- page content -->


















               </div>
                   </div>
               <!-- /page content -->

           <!-- /compose -->


        <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
         
         <script type="text/javascript">
            
            $(document).ready(function() {
               
               $('#datatable').dataTable( {
                  "processing": true,
                  "stateSave": true,
                  responsive: true,
                  
                  
                  
                  "language": {
                     "loadingRecords": "Carregando dados...",
                     "processing": "Processando  dados...",
                     "infoEmpty": "Nenhum dado a mostrar",
                     "emptyTable": "Sem dados disponíveis na tabela",
                     "zeroRecords": "Não há registros a serem exibidos",
                     "search": "Filtrar registros:",
                     "info": "Mostrando página _PAGE_ de _PAGES_",
                     "infoFiltered": " - filtragem de _MAX_ registros",
                     "lengthMenu": "Mostrar _MENU_ registros",
                     
                     "paginate": {
                        "previous": "Página anterior",
                        "next": "Próxima página",
                        "last": "Última página",
                        "first": "Primeira página",
                        
                        
                        
                     }
                  }
                  
                  
                  
               } );
               // Define a URL da API que retorna os dados da query em formato JSON
               const apiUrl = 'table/table-search-obsolescence-depreciation.php';
               
               // Usa a função fetch() para obter os dados da API em formato JSON
               fetch(apiUrl)
               .then(response => response.json())
               .then(data => {
                  // Mapeia os dados para o formato esperado pelo DataTables
                  const novosDados = data.map(item => [
                     ``,
                     item.codigo,
                     item.equipamento,
                     item.modelo,
                     item.fabricante,
                     item.serie,
                     item.instituicao,
                     item.setor,
                     item.area,
                     item.yr,                   
                     
                     `   <a class="btn btn-app"  href="obsolescence-depreciation-edit?id=${item.id}" onclick="new PNotify({
                                                title: 'Editar',
                                                text: 'Abrindo Edição',
                                                type: 'sucess',
                                                styling: 'bootstrap3'
                                          });">
                              <i class="fa fa-edit"></i> Editar
                           </a>
                           
                  <a  class="btn btn-app" href="obsolescence-depreciation-viwer?id=${item.id}"target="_blank" onclick="new PNotify({
                                                title: 'Visualizar',
                                                text: 'Visualizar O.S!',
                                                type: 'info',
                                                styling: 'bootstrap3'
                                          });" >
                              <i class="fa fa-file-pdf-o"></i> Visualizar
                           </a>
                  
                                                         
                           <a class="btn btn-app"     onclick="
                           Swal.fire({
   title: 'Tem certeza?',
   text: 'Você não será capaz de reverter isso!',
   icon: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#d33',
   confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
   if (result.isConfirmed) {
      Swal.fire(
         'Deletando!',
         'Seu arquivo será excluído.',
         'success'
      ),
window.location = 'backend/obsolescence-depreciation-backend?id=${item.id}';
   }
})
">
                                                   <i class="fa fa-trash"></i> Excluir
                                                </a>`
                  ]);
                  
                  // Adiciona as novas linhas ao DataTables e desenha a tabela
                  $('#datatable').DataTable().rows.add(novosDados).draw();
                  
                  
                  // Cria os filtros após a tabela ser populada
                  $('#datatable').DataTable().columns([1,2,3,4,5,6,7,8]).every(function (d) {
                     var column = this;
                     var theadname = $("#datatable th").eq([d]).text();
                     var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
                     var label = $('<label>'+theadname+': </label>').appendTo(container); 
                     var select = $('<select  class="form-control my-1"><option value="">' +
                        theadname +'</option></select>').appendTo('#userstable_filter').select2()
                     .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column.search(val ? '^' + val + '$' : '', true, false).draw();
                     });
                     
                     column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                     });
                  });
               });
            });
            
            
         </script>