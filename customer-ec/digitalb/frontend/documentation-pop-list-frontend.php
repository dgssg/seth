<?php
include("database/database.php");
$query = "SELECT 
    documentation_pop_goup.nome,
    latest_files.file,
    
    documentation_pop.titulo,
    documentation_pop.codigo,
    equipamento_grupo.nome,
    documentation_pop.id,
    documentation_pop.id_equipamento_grupo,
    documentation_pop.file,
    documentation_pop.pop,
    documentation_pop.data_now,
    documentation_pop.data_after,
    documentation_pop.upgrade,
    documentation_pop.reg_date
FROM 
    documentation_pop 
LEFT JOIN 
    equipamento_grupo ON equipamento_grupo.id = documentation_pop.id_equipamento_grupo 
LEFT JOIN 
    (SELECT 
         regdate_pop_dropzone.id_pop, 
         regdate_pop_dropzone.file, 
         regdate_pop_dropzone.reg_date
     FROM 
         regdate_pop_dropzone
     INNER JOIN 
         (SELECT 
              id_pop, 
              MAX(reg_date) as max_reg_date 
          FROM 
              regdate_pop_dropzone 
          GROUP BY 
              id_pop
         ) as latest 
     ON 
         regdate_pop_dropzone.id_pop = latest.id_pop 
         AND regdate_pop_dropzone.reg_date = latest.max_reg_date
    ) as latest_files 
ON 
    latest_files.id_pop = documentation_pop.id 
LEFT JOIN 
    documentation_pop_goup ON documentation_pop_goup.id = documentation_pop.group_pop 
GROUP BY 
    documentation_pop.id;
";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($documentation_pop_goup,$dropzone,$titulo,$codigo,$grupo,$id, $id_equipamento_grupo, $file, $pop, $data_now, $data_after, $upgrade, $reg_date);
  


?>

 
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nº</th>
                          <th>Codigo</th>
                          <th>Grupo</th>
                          <th>POP</th>
                          <th>Titulo</th>
                          <th>Revisão</th> 
                          <th>Proxima</th>
                          
                        
                         
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                         <td > </td>
                        <td ><?php printf($id); ?> </td>

                          <td ><?php printf($codigo); ?> </td>
                         
                            
                              <td><?php printf($grupo); ?></td>
                              <td><?php printf($documentation_pop_goup); ?></td>
                                <td><?php printf($titulo); ?></td>
                                
                          <td><?php printf($data_now); ?></td>
                            <td><?php printf($data_after); ?></td>
                      
                          <td>  
                 
                  <a class="btn btn-app"  href="documentation-pop-view?pop=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar POP',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> 
                   <a class="btn btn-app"  href="documentation-pop-edit.php?pop=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Editar',
																text: 'Editar POP',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a> 
                    <?php if($dropzone==!""){?>
                   <a class="btn btn-app"   href="dropzone/pop/<?php printf($dropzone); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar procediemnto',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Download
                  </a>
                  <?php  }  ?>
                    <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/documentation-pop-trash.php?pop=<?php printf($id); ?>';
  }
})
">
                    <i class="fa fa-trash"></i> Excluir
                  </a>
             
                
                
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
           