

 <!-- page content -->
 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Calibração <small>Procedimento</small></h3>
              </div>


            </div>

            
         <div class="clearfix"></div>

<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
      
<a class="btn btn-app"  href="flow-calibration-procedures">
<i class="glyphicon glyphicon-list-alt"></i> Procedimentos
</a>

<a class="btn btn-app"  href="flow-calibration-procedures-register">
<i class="glyphicon glyphicon-plus"></i> Cadastro
</a>

 
<a class="btn btn-app"  href="flow-calibration-procedures-incomplete-viewer" target="_blank" >
        <i class="fa fa-file-pdf-o"></i> Sem Procedimentos
      </a>
      

<?php //include 'frontend/maintenance-procedures-register-frontend.php';?>




      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
          
             
 

<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Cadastro<small>Procedimento</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

<?php include 'frontend/flow-calibration-procedures-register-frontend.php';?>




      </div>
    </div>
  </div>
</div>






                </div>
              </div>
            

