<?php
include("database/database.php");
$codigoget = ($_GET["id"]);
$query="SELECT purchases_out.id_purchases_status,purchases_out.service,colaborador.primeironome,colaborador.ultimonome,instituicao.instituicao,instituicao_area.nome,instituicao_localizacao.nome, purchases_out.id,purchases_service.nome,purchases_out.number_mp,purchases_out.id_setor,purchases_status.status,purchases_out.data_purchases,purchases_out.id_colaborador,purchases_out.upgrade,purchases_out.reg_date FROM purchases_out INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = purchases_out.id_setor INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  INNER JOIN colaborador ON colaborador.id =purchases_out.id_colaborador INNER JOIN purchases_status ON purchases_status.id = purchases_out.id_purchases_status INNER JOIN purchases_service ON purchases_service.id = purchases_out.service WHERE  purchases_out.id = $codigoget";
$row=1;
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result( $purchases_status,$id_service,$primeironome,$ultimonome,$instituicao,$setor,$area,$id,$service,$number,$id_setor,$id_purchases_status,$data_purchases,$id_colaborador,$upgrade,$reg_date);

  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}

?>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>  Alteração <small>Saida de Material</small></h3>
      </div>


    </div>

    <div class="clearfix"></div>

    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cabeçalho <small>Saida de Material</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings 1</a>
                  <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form action="backend/purchases-inventory-output-edit-backend.php?output=<?php printf($codigoget);?>" method="post">

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Manutenção <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <select type="text" class="form-control has-feedback-left" name="purchases_service" id="purchases_service"  placeholder="Manutenção" readonly="readonly">
                    <option value="">Selecione uma Manutenção</option>
                    <?php



                    $result_cat_post  = "SELECT  id, nome FROM purchases_service";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                    while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if($row_cat_post['id'] ==$id_service){
                      echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'</option>';
                    }
                      echo '<option value="'.$row_cat_post['id'].'" >'.$row_cat_post['nome'].'</option>';
                    }
                    ?>

                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Manutenção "></span>

                </div>
              </div>


          <!--    <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Nº <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <select type="text" class="form-control has-feedback-right" name="number" id="number"  placeholder="Nº">
                    <option value="">Selecione uma M.P</option>
                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Nº "></span>


                </div>
              </div> -->
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Nº <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <input id="number" class="form-control" name="number"  value="<?php printf($number); ?> " readonly="readonly" >
                </div>
              </div>
          <!--    <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <select type="text" class="form-control has-feedback-left" name="instituicao2" id="instituicao2"  placeholder="Unidade">
                  <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                    <?php



                    $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                    while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                      echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
                    }
                    ?>

                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#instituicao2').select2();
              });
              </script>



              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <select type="text" class="form-control has-feedback-right" name="area2" id="area2"  placeholder="Area">
                      <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	  
                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#area2').select2();
              });
              </script>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <select type="text" class="form-control has-feedback-right" name="setor" id="setor2"  placeholder="Area">
                    <option value="">Selecione uma Opção</option>
                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#setor2').select2();
              });
            </script> -->


              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Status <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <select type="text" class="form-control has-feedback-left" name="id_purchases_status" id="id_purchases_status"  placeholder="Status">
                    <option value="">Selecione uma Status</option>
                    <?php



                    $result_cat_post  = "SELECT  id, status FROM purchases_status";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                    while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if($row_cat_post['id'] ==$purchases_status){
                      echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['status'].'</option>';
                    }
                      echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['status'].'</option>';
                  }
                    ?>

                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Status "></span>

                </div>
              </div>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="data_purchases" class="form-control" type="date" name="data_purchases" value="<?php printf($data_purchases); ?>"  >
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Colaborador <span class="required"></span>
                </label>
                <div class="input-group col-md-6 col-sm-6">
                  <select type="text" class="form-control has-feedback-left" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador">
                    <option value="">Selecione uma Colaborador</option>
                    <?php



                    $result_cat_post  = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                    while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if($row_cat_post['id'] ==$id_colaborador){
                      echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['primeironome'].' - '.$row_cat_post['ultimonome'].'</option>';

                    }
                      echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['primeironome'].' - '.$row_cat_post['ultimonome'].'</option>';
                  }
                    ?>

                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Colaborador "></span>

                </div>
              </div>
              <script>
              $(document).ready(function() {
                $('#id_colaborador').select2();
              });
              </script>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 "></label>
                <div class="col-md-3 col-sm-3 ">
                  <center>
                    <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
                      title: 'Registrado',
                      text: 'Informações registrada!',
                      type: 'success',
                      styling: 'bootstrap3'
                    });">Salvar Informações</button>
                  </center>
                </div>
              </div>


            </form>



          </div>
        </div>
      </div>
    </div>


    <div class="clearfix"></div>



    <div class="x_panel">
      <div class="x_title">
        <h2>Itens de Saida</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <!--arquivo-->
            <?php
            $query="SELECT purchases_out_itens.id, purchases_out_itens.qtd,purchases_material.nome,purchases_material.unidade,purchases_material.cod FROM purchases_out_itens INNER JOIN purchases_material ON purchases_material.id = purchases_out_itens.id_purchases_material WHERE purchases_out_itens.id_purchases_out like '$codigoget'  ";
            $row=1;
            if ($stmt = $conn->prepare($query)) {
              $stmt->execute();
              $stmt->bind_result($id,$qtd,$nome,$unidade,$cod);

              ?>
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Cod</th>
                    <th>Nome</th>
                      <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                    <th>Qtd</th>
                    <th>Ação</th>

                  </tr>
                </thead>
                <tbody>
                  <?php  while ($stmt->fetch()) { ?>

                    <tr>
                      <th scope="row"><?php printf($row); ?></th>
                      <td><?php printf($cod); ?></td>
                      <td><?php printf($nome); ?></td>
                      <td><?php printf($unidade); ?></td>
                      <td><?php printf($qtd); ?></td>

                      <td>

                         <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/purchases-inventory-output-edit-itens-trash.php?id=<?php printf($id); ?>&output=<?php printf($id); ?>';
  }
})
">
                            <i class="glyphicon glyphicon-trash"></i> Excluir
                          </a>
                       </td>
                    </tr>
                    <?php  $row=$row+1; }
                  }   ?>
                </tbody>
              </table>





            </div>
          </div>


          <div  class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">

                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Itens</h4>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="backend/purchases-inventory-output-insert-itens-backend.php?output=<?php printf($codigoget);?>" method="post">
                    <div class="ln_solid"></div>
                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Material <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="purchases_material" id="purchases_material"  placeholder="Material">
                          <option value="">Selecione um Material</option>
                          <?php



                          $result_cat_post  = "SELECT  id, cod,nome,unidade FROM purchases_material";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['cod'].' - '.$row_cat_post['nome'].' - '.$row_cat_post['unidade'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Material "></span>

                      </div>
                    </div>
                    <script>
                      $(document).ready(function() {
                        $('#').select2({
                          dropdownParent: $("#myModal")
                        });
                      });
                    </script>


                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estoque Atual</label>
                      <div class="col-md-6 col-sm-6 ">
                        <p id="estoque" class="form-control" type="text" name="estoque"   readonly="readonly"></p>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="qtd"  class="form-control" class="number" type="number" name="qtd"  min="0"   >

                                            </div>
                                          </div>




                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                          <button type="submit" class="btn btn-primary" onclick="new PNotify({
                                            title: 'Registrado',
                                            text: 'Informações registrada!',
                                            type: 'success',
                                            styling: 'bootstrap3'
                                          });">Salvar Informações</button>
                                        </div>

                                      </div>
                                    </div>
                                  </div>
                                </form>

                                <!-- -->


                                <!-- Posicionamento -->



                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Ação</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                          class="fa fa-wrench"></i></a>
                                          <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                          </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                      </ul>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                      <a class="btn btn-app"  href="purchases-inventory-output">
                                        <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                                      </a>



                                      <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
                                      title: 'Cancelamento',
                                      text: 'Cancelamento de Abertura de O.S!',
                                      type: 'error',
                                      styling: 'bootstrap3'
                                    });">
                                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                                  </a>
                                  <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
                                  title: 'Visualizar',
                                  text: 'Visualizar O.S!',
                                  type: 'info',
                                  styling: 'bootstrap3'
                                });" >
                                <i class="fa fa-file-pdf-o"></i> Visualizar
                              </a> -->

                            </div>
                          </div>




                        </div>
                      </div>
                      <script type="text/javascript">
                      $(document).ready(function(){
                        $('#purchases_material').change(function(){
                          $('#estoque').load('sub_categorias_post_purchases_estoque.php?purchases_material='+$('#purchases_material').val());
                        });
                      });
                      </script>
                      <script type="text/javascript">
                      $(document).ready(function(){
                        $('#purchases_material').change(function(){
                          $('#qtd').load('sub_categorias_post_purchases_numeber_max.php?purchases_material='+$('#purchases_material').val());
                        });
                      });
                      </script>
                      <script type="text/javascript">
                      $(document).ready(function(){
                        $('#purchases_service').change(function(){
                          $('#number').select2().load('sub_categorias_post_purchases.php?purchases_service='+$('#purchases_service').val());
                        });
                      });
                      </script>
