<?php
include("../database/database.php");
$query = "SELECT instituicao.instituicao, instituicao_area.nome AS 'setor',instituicao_localizacao.nome AS 'area',equipament_compliance.id_os,compliance.nome AS 'compliance', equipament_compliance.id, equipament_compliance.id_os, equipament_compliance.file, equipament_compliance.titulo, equipament_compliance.upgrade, equipament_compliance.reg_date AS 'compliance_reg_date', equipament_compliance.date_compliance, equipament_compliance.id_compliance, equipament_compliance.when_compliance, equipament_compliance.how_compliance, equipament_compliance.obs_compliance, equipament_compliance.responsible_compliance, equipament_compliance.date_quality, equipament_compliance.analysis_quality, equipament_compliance.date_conclusion, equipament_compliance.conclusion_compliance, equipament_compliance.id_responsible FROM equipament_compliance LEFT JOIN os ON os.id = equipament_compliance.id_os LEFT JOIN compliance ON compliance.id = equipament_compliance.id_compliance LEFT JOIN instituicao_localizacao on instituicao_localizacao.id = equipament_compliance.id_area OR os.id_setor  LEFT JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade GROUP BY equipament_compliance.id ORDER by equipament_compliance.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
