<?php
include("../database/database.php");
$query = "SELECT instituicao_localizacao.id, instituicao.instituicao,instituicao_area.custo, instituicao_area.nome AS 'setor',instituicao_localizacao.codigo,instituicao_localizacao.andar,instituicao_localizacao.nome,instituicao_localizacao.observacao,instituicao_localizacao.upgrade,instituicao_localizacao.reg_date FROM instituicao_localizacao INNER JOIN instituicao ON instituicao.ID =   instituicao_localizacao.id_unidade INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area WHERE instituicao_localizacao.trash = 1 ORDER BY instituicao_localizacao.id  DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
