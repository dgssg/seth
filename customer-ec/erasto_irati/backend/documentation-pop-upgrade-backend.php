 <?php

include("../database/database.php");

$codigoget = ($_GET["pop"]);
$titulo= $_POST['titulo'];
$equipamento_grupo= $_POST['equipamento_grupo'];
$codigo= $_POST['codigo'];
$data_now= $_POST['date_now'];
$data_after= $_POST['date_after'];
$pop= $_POST['editor'];
$group_pop= $_POST['group_pop'];
$create= $_POST['create'];
$signature= $_POST['signature'];
$rev= $_POST['rev'];
$id_unidade= $_POST['id_unidade'];
$id_setor= $_POST['area'];
$id_resp= $_POST['id_colaborador'];

$stmt = $conn->prepare("UPDATE documentation_pop SET `create` = ? WHERE id= ?");
$stmt->bind_param("ss",$create,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_pop SET signature = ? WHERE id= ?");
$stmt->bind_param("ss",$signature,$codigoget);
$execval = $stmt->execute();
$stmt = $conn->prepare("UPDATE documentation_pop SET rev = ? WHERE id= ?");
$stmt->bind_param("ss",$rev,$codigoget);
$execval = $stmt->execute();
$stmt = $conn->prepare("UPDATE documentation_pop SET id_unidade = ? WHERE id= ?");
$stmt->bind_param("ss",$id_unidade,$codigoget);
$execval = $stmt->execute();
$stmt = $conn->prepare("UPDATE documentation_pop SET id_setor = ? WHERE id= ?");
$stmt->bind_param("ss",$id_setor,$codigoget);
$execval = $stmt->execute();
$stmt = $conn->prepare("UPDATE documentation_pop SET id_resp = ? WHERE id= ?");
$stmt->bind_param("ss",$id_resp,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE documentation_pop SET group_pop = ? WHERE id= ?");
$stmt->bind_param("ss",$group_pop,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE documentation_pop SET id_equipamento_grupo = ? WHERE id= ?");
$stmt->bind_param("ss",$equipamento_grupo,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_pop SET pop = ? WHERE id= ?");
$stmt->bind_param("ss",$pop,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_pop SET data_now = ? WHERE id= ?");
$stmt->bind_param("ss",$data_now,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_pop SET data_after = ? WHERE id= ?");
$stmt->bind_param("ss",$data_after,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_pop SET codigo = ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();


//$stmt = $conn->prepare("UPDATE documentation_pop SET titulo = ? WHERE id= ?");
//$stmt->bind_param("ss",$titulo,$codigoget);
//$execval = $stmt->execute();


echo "<script>alert('Atualizado!');document.location='../documentation-pop-edit.php?pop=$codigoget'</script>";


?>
