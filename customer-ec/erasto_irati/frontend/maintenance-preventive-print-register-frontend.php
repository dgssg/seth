  <?php
$codigoget = ($_GET["equipamento"]);
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               <h3>  Manutenção <small>Preventiva</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>





            <div class="clearfix"></div>




              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                
                                                 
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                  <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive' ? 'active' : ''; ?>" href="maintenance-preventive">
             <i class="glyphicon glyphicon-open"></i> Aberta
              <span class="badge bg-green">  <h3> <?php
                $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 1 ");
                $stmt->execute();
                $stmt->bind_result($result);
                while ( $stmt->fetch()) {
                echo "$result" ;
              }?> </h3></span>

          </a>

     
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-before' ? 'active' : ''; ?>" href="maintenance-preventive-before">

            <i class="glyphicon glyphicon-export"></i> Atrasada
            <span class="badge bg-red">  <h3> <?php
              $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 2 ");
              $stmt->execute();
              $stmt->bind_result($result);
              while ( $stmt->fetch()) {
              echo "$result" ;
            }?> </h3></span>
          </a>

        
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-close' ? 'active' : ''; ?>" href="maintenance-preventive-close">

            <i class="glyphicon glyphicon-save"></i> Fechada
          </a>

         
                            <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-print' ? 'active' : ''; ?>" href="maintenance-preventive-print">

            <i class="glyphicon glyphicon-print"></i> Impressão

          </a>

                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-print-register' ? 'active' : ''; ?>" href="maintenance-preventive-print-register">

            <i class="glyphicon glyphicon-folder-open"></i> Registro de Impressão
          </a>

                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-control' ? 'active' : ''; ?>" href="maintenance-preventive-control">

            <i class="fa fa-check-square-o"></i> Controle
          </a>
          
                             <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-control-return' ? 'active' : ''; ?>" href="maintenance-preventive-control-return">

            <i class="fa fa-pencil-square-o"></i> Retorno
          </a>

                </div>
              </div>




	       <div class="row" style="display: block;">
              <div class="col-md-6 col-sm-6  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista <small>MP</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-9 col-sm-9  form-group has-feedback">
                               <select type="text" class="form-control has-feedback-left" name="lista" id="lista"  placeholder="Lista">
                               <option value="">Selecione uma data</option>
                                   <?php



                                  $result_cat_post  = "SELECT DAY(reg_date) AS dia,  month(reg_date) AS mes,  year(reg_date) AS ano FROM maintenance_print GROUP by dia,mes,ano order by id DESC";

                $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                     while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
if($row_cat_post['mes'] < 10 && $row_cat_post['dia'] < 10){
                       echo '<option value="'.$row_cat_post['ano'].'-0'.$row_cat_post['mes'].'-0'.$row_cat_post['dia'].'">'.$row_cat_post['dia'].'-'.$row_cat_post['mes'].'-'.$row_cat_post['ano'].'</option>';
}
if($row_cat_post['mes'] < 10 && $row_cat_post['dia'] > 9){
  echo '<option value="'.$row_cat_post['ano'].'-0'.$row_cat_post['mes'].'-'.$row_cat_post['dia'].'">'.$row_cat_post['dia'].'-'.$row_cat_post['mes'].'-'.$row_cat_post['ano'].'</option>';
}
if($row_cat_post['mes'] > 9 && $row_cat_post['dia'] < 10){
  echo '<option value="'.$row_cat_post['ano'].'-'.$row_cat_post['mes'].'-0'.$row_cat_post['dia'].'">'.$row_cat_post['dia'].'-'.$row_cat_post['mes'].'-'.$row_cat_post['ano'].'</option>';
}
if($row_cat_post['mes'] > 9 && $row_cat_post['dia'] > 9){
  echo '<option value="'.$row_cat_post['ano'].'-'.$row_cat_post['mes'].'-'.$row_cat_post['dia'].'">'.$row_cat_post['dia'].'-'.$row_cat_post['mes'].'-'.$row_cat_post['ano'].'</option>';
}
                      }
                ?>

                                                   </select>
                               <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a data"></span>


                                   </div>

                            <script>
                                               $(document).ready(function() {
                                               $('#lista').select2();
                                                 });
                                            </script>

 
                  </div>
                </div>
              </div>


              <div class="col-md-6 col-sm-6  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cadastro <small>de MP</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                     <form  action="backend/maintenance-preventive-print-register-before-backend.php" method="post" target="_blank"  >
<input type="hidden" id="date" name="date">
                     <div class="col-md-9 col-sm-9 form-group has-feedback">
                   <select multiple="multiple" type="text" class="form-control has-feedback-right" name="registro[]" id="registro"  placeholder="Registro">
                   <option value="">Selecione um Registro</option>
                           </select>
                   <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Registro "></span>



                   </div>
                   <button onclick="selecionarTodos()">Selecionar Todos</button>
                   <input type="submit" class="btn btn-primary" onclick="new PNotify({
                              title: 'Registrado',
                              text: 'Informações registrada!',
                              type: 'success',
                              styling: 'bootstrap3'
                          });" value="Imprimir" />
                   </form>

                   <script>
                       $(document).ready(function() {
                       $('#registro').select2();
                         });
                    </script>


                  </div>
                </div>
              </div>
              
                 </div>



                 <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista Impressão</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                            class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <?php
                    include("database/database.php");
                    $query = "SELECT * FROM maintenance_print ";
                    if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id,$rotina,$reg_date,$upgrade);



                    ?>


                    <div class="col-md-11 col-sm-11 ">
                                    <div class="x_panel">

                                      <div class="x_content">
                                          <div class="row">
                                              <div class="col-sm-12">
                                                <div class="card-box table-responsive">

                                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                          <thead>
                                            <tr>
                                              <th>#</th>

                                              <th>Rotina</th>

                                              <th>Atualização</th>
                                              <th>Registro</th>


                                              <th>Ação</th>

                                            </tr>
                                          </thead>


                                          <tbody>
                                              <?php   while ($stmt->fetch()) {   ?>
                                            <tr>
                                              <td ><?php printf($id); ?> </td>


                                                  <td><?php printf($rotina); ?></td>
                                                    <td><?php printf($upgrade); ?></td>

                                              <td><?php printf($reg_date); ?></td>

                                              <td>

                                      <a class="btn btn-app"  href="maintenance-preventive-viwer-register?routine=<?php printf($rotina); ?>&registro=<?php printf($reg_date); ?>" target="_blank"  onclick="new PNotify({
                    																title: 'Visualizar',
                    																text: 'Visualizar Rotina',
                    																type: 'info',
                    																styling: 'bootstrap3'
                    														});">
                                        <i class="fa fa-file-pdf-o"></i> Visualizar
                                      </a>

                                       <a class="btn btn-app"  href="maintenance-preventive-open-tag-register?routine=<?php printf($rotina); ?>&registro=<?php printf($reg_date); ?>" target="_blank"   onclick="new PNotify({
                    																title: 'Visualizar',
                    																text: 'Visualizar Etiqueta',
                    																type: 'info',
                    																styling: 'bootstrap3'
                    														});">
                                        <i class="fa fa-qrcode"></i> Etiqueta
                                      </a>


                                       <a class="btn btn-app"  href="maintenance-preventive-viwer-print-register?routine=<?php printf($rotina); ?>&registro=<?php printf($reg_date); ?>" target="_blank"  onclick="new PNotify({
                    																title: 'Imprimir',
                    																text: 'Imprimir Rotina',
                    																type: 'info',
                    																styling: 'bootstrap3'
                    														});">
                                        <i class="fa fa-print"></i> Imprimir
                                      </a>
                                      <a class="btn btn-app"  href="maintenance-preventive-label-register?routine=<?php printf($rotina_id); ?>&registro=<?php printf($reg_date); ?>" target="_blank" onclick="new PNotify({
                                                   title: 'Etiqueta',
                                                   text: 'Abrir Etiqueta',
                                                   type: 'sucess',
                                                   styling: 'bootstrap3'
                                               });">
                                       <i class="fa fa-fax"></i> Rótulo
                                     </a>
                                      </td>
                                            </tr>
                                            <?php   } }  ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                    </div>
                                  </div>


                  </div>
                </div>





                </div>
              </div>

              <script type="text/javascript">
               $(document).ready(function(){
                   $('#lista').change(function(){
                       $('#registro').load('sub_categorias_post_registro.php?lista='+$('#lista').val());
                   });
               });
               $(document).ready(function(){
                   $('#lista').change(function(){
                    var lista = document.getElementById("lista").value;
 
                    $('#date').val(lista);
                   });
               });

               var date_end = document.getElementById("date_startsecond").value;
//  var  date_end = +$('#date_start').val(date_start);

  $('#date_endsecond').val(date_end);
               </script>
               <script>
        function selecionarTodos() {
            var selectElement = document.getElementById('registro');
            var options = selectElement.options;
            for (var i = 0; i < options.length; i++) {
                options[i].selected = true;
            }
        }
    </script>
 <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
