 <?php

include("database/database.php");


//$con->close();


?>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Documentos & POP <small></small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                                                
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                <a class="btn btn-app <?php echo $current_page == 'documentation' ? 'active' : ''; ?>" href="documentation">
                   <i class="fa fa-code"></i> Sistema
                 </a>
                  <a class="btn btn-app <?php echo $current_page == 'documentation-email' ? 'active' : ''; ?>" href="documentation-email">
                    <i class="fa fa-at"></i> Email
                  </a>

              
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-pop' ? 'active' : ''; ?>" href="documentation-pop">

                   <i class="fa fa-book"></i> POP
                 </a>
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-update' ? 'active' : ''; ?>" href="documentation-update">

                   <i class="fa fa-history"></i> Atualização
                 </a>
              
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-hfmea' ? 'active' : ''; ?>" href="documentation-hfmea">

                   <i class="fa fa-map"></i> HFMEA
                 </a>
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-label' ? 'active' : ''; ?>" href="documentation-label">

                   <i class="fa fa-fax"></i> Rotuladora
                 </a>
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-bussines' ? 'active' : ''; ?>" href="documentation-bussines">

                    <i class="fa fa-folder-open"></i> Documentação
                  </a>
                
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-support' ? 'active' : ''; ?>" href="documentation-support">

                    <i class="fa fa-support"></i> Contingência
                  </a>

               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-bell' ? 'active' : ''; ?>" href="documentation-bell">

                    <i class="fa fa-bell"></i> Central de Notificações
                  </a>
                
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-graduation' ? 'active' : ''; ?>" href="documentation-graduation">

                    <i class="fa fa-graduation-cap"></i> Educação Continuada
                  </a>
            
                                  <a class="btn btn-app <?php echo $current_page == 'documentation-library' ? 'active' : ''; ?>" href="documentation-library">

                    <i class="fa fa-book"></i> Biblioteca
                  </a> 
               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-connectivity' ? 'active' : ''; ?>" href="documentation-connectivity">

                    <i class="fa fa-code-fork"></i> Conectividade &amp; Recursos
                  </a>
                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-event-task' ? 'active' : ''; ?>" href="documentation-event-task">

                    <i class="fa fa-tasks"></i> Eventos &amp; Taferas
                  </a>                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-widget' ? 'active' : ''; ?>" href="documentation-widget">

                    <i class="fa fa-desktop"></i> Widget
                  </a>

<a class="btn btn-app" href="http://assinador.iti.br/" target="_blank" rel="noopener noreferrer">
                <i class="fa fa-external-link"></i> ITI Assinador 
            </a>


                </div>
              </div>
            
        <div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <a class="btn btn-app" href="documentation-graduation">
          <i class="fa fa-graduation-cap"></i> Educação Continuada
        </a>
        
        <a class="btn btn-app" href="documentation-graduation-training-questions">
          <i class="fa fa-question-circle"></i> Perguntas para o Treinamento
        </a>
        
        <a class="btn btn-app" href="documentation-graduation-training-application">
          <i class="fa fa-check"></i> Aplicação do Treinamento
        </a>
        
        <a class="btn btn-app" href="documentation-graduation-training-feedback">
          <i class="fa fa-comments"></i> Avaliação e Feedback
        </a>
        
       


<?php //include 'frontend/maintenance-routine-register-frontend.php';?>




      </div>
    </div>
  </div>
</div>
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="userstable_filter" style="column-count:3; -moz-column-count:3; -webkit-column-count:3; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                    
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            
            
            <div class="x_panel">
              <div class="x_title">
                <h2>Educação Continuada</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                    </ul>
                    <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="card-box table-responsive">
                            
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Pergunta</th>
                                  <th>Tipo</th>
                                  <th>Categoria</th>
                                  
                                  <th>Titulo</th>
                                  
                                  <th>Equipamento</th>
                                  <th>Fabricante</th>
                                  <th>Modelo</th>
                                  
                                
                                  <th>Ativo</th>
                                                                   
                                  
                                 
                                  
                                  <th>Ação</th>
                                  
                                </tr>
                              </thead>
                              
                              
                              <tbody>
                                
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      
                      <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal" >
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h4 class="modal-title" id="myModalLabel">Salvar Material</h4>
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="ln_solid"></div>
                           
<div class="ln_solid"></div>
                              <form action="backend/documentation-graduation-training-questions-backend.php" method="post">
                                
                                
                                <div class="ln_solid"></div>
                                
                                
                                
                                
                                <div class="item form-group">
                                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Pergutna</label>
                                  <div class="col-md-6 col-sm-6 ">
                                    <input id="question" class="form-control" type="text" name="question"   >
                                  </div>
                                </div>
                             
                                <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Educação Continuada<span
                                    class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                    <select type="text" class="form-control has-feedback-left" name="id_training"
                                      id="id_training" style="width:350px;" placeholder="Unidade" required="required">
                                      <option> Selecione uma opção</option>
                                      <?php
                                        
                                        
                                        
                                        $result_cat_post  = "SELECT  id, titulo FROM documentation_graduation ";
                                        
                                        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                                        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['titulo'].'</option>';
                                        }
                                      ?>
                                      
                                    </select>
                                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                      class="docs-tooltip" data-toggle="tooltip" ></span>
                                    
                                  </div>
                                </div>
                                
                              
                                
                                <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Tipo <span
                                    class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                    <select type="text" class="form-control has-feedback-left" name="type"
                                      id="type" style="width:350px;" placeholder="Unidade" required="required">
                                      <option> Selecione uma opção</option>
                                      <option value="multiple_choice"> Multipla Escolha</option>
                                      <option value="true_false"> Verdadeiro ou Falso</option>
                                      <option value="open"> Aberta</option>
                                                                  
                                    </select>
                                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                      class="docs-tooltip" data-toggle="tooltip"  ></span>
                                    
                                  </div>
                                </div>
                                
                                <script>
                                  $(document).ready(function() {
                                    $('#id_training').select2({
                                      dropdownParent: $("#myModal")
                                    });
                                    $('#type').select2({
                                      dropdownParent: $("#myModal")
                                    });
                                  });
                                </script>

                                
                                
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                  <button type="submit" class="btn btn-primary" onclick="new PNotify({
                                    title: 'Registrado',
                                    text: 'Informações registrada!',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                  });" >Salvar</button> 
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        
                        
                        
                        
                      </div>
                    </div>
                    
                    </div>
                    </div>
                    
                    
                    
                    <script language="JavaScript">
                      $(document).ready(function() {
                        
                        $('#datatable').dataTable( {
                          "processing": true,
                          "stateSave": true,
                          responsive: true,
                          
                          
                          
                          "language": {
                            "loadingRecords": "Carregando dados...",
                            "processing": "Processando  dados...",
                            "infoEmpty": "Nenhum dado a mostrar",
                            "emptyTable": "Sem dados disponíveis na tabela",
                            "zeroRecords": "Não há registros a serem exibidos",
                            "search": "Filtrar registros:",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoFiltered": " - filtragem de _MAX_ registros",
                            "lengthMenu": "Mostrar _MENU_ registros",
                            
                            "paginate": {
                              "previous": "Página anterior",
                              "next": "Próxima página",
                              "last": "Última página",
                              "first": "Primeira página",
                              
                              
                              
                            }
                          }
                          
                          
                          
                        } );
                        
                        
                        // Define a URL da API que retorna os dados da query em formato JSON
                        const apiUrl = 'table/table-search-graduation-training-questions.php';
                        
                        
                        
                        // Usa a função fetch() para obter os dados da API em formato JSON
                        fetch(apiUrl)
                        .then(response => response.json())
                        .then(data => {
                          // Mapeia os dados para o formato esperado pelo DataTables
                          const novosDados = data.map(item => [
                            `
`,
                            item.question, 
                            item.type, 
                            item.nome,    
                            item.titulo,
                            item.equipamento,
                            item.fabricante,
                            item.modelo,
                            item.active == "1" ? "Sim" : "Não",                             
                            
                                                      `
                          <a class="btn btn-app" onclick="Swal.fire({
      title: 'Tem certeza?',
      text: 'Você não será capaz de reverter isso!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, Deletar!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deletando!',
          'Seu arquivo será excluído.',
          'success'
        ),
        window.location = 'backend/documentation-graduation-training-questions-trash.php?id=${item.id}';
      }
    });">
      <i class="fa fa-trash"></i> Excluir
    </a>`
                          ]);
                          
                          // Inicializa o DataTables com os novos dados
                          const table = $('#datatable').DataTable();
                          table.clear().rows.add(novosDados).draw();
                          
                          
                          // Cria os filtros após a tabela ser populada
                          $('#datatable').DataTable().columns([1, 2,3,4,5,6]).every(function(d) {
                            var column = this;
                          var theadname = $("#datatable th").eq([d]).text();
        // Container para o título, o select e o alerta de filtro
        var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
        
        // Título acima do select
        var title = $('<label>' + theadname + '</label>').appendTo(container);
        
        // Container para o select
        var selectContainer = $('<div></div>').appendTo(container);
        
        var select = $('<select class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo(selectContainer).select2()
        .on('change', function() {
          var val = $.fn.dataTable.util.escapeRegex($(this).val());
          column.search(val ? '^' + val + '$' : '', true, false).draw();
          
          // Remove qualquer alerta existente
          container.find('.filter-alert').remove();
          
          // Se um valor for selecionado, adicionar o alerta de filtro
          if (val) {
            $('<div class="filter-alert">' +
              '<span class="filter-active-indicator">&#x25CF;</span>' +
              '<span class="filter-active-message">Filtro ativo</span>' +
              '</div>').appendTo(container);
          }
          
          // Remove o indicador do título da coluna
          $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
          
          // Se um valor for selecionado, adicionar o indicador no título da coluna
          if (val) {
            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
          }
        });
        
        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
        // Verificar se há filtros aplicados ao carregar a página
        var filterValue = column.search();
        if (filterValue) {
          select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
        }
      });
      
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
                          
                          var oTable = $('#datatable').dataTable();
                          
                          // Avança para a próxima página da tabela
                          //oTable.fnPageChange('next');
                          
                          
                          
                        });
                        
                        
                        // Armazenar a posição atual ao sair da página
                        $(window).on('beforeunload', function() {
                          var table = $('#datatable').DataTable();
                          var pageInfo = table.page.info();
                          localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
                        });
                        
                        // Restaurar a posição ao retornar à página
                        $(document).ready(function() {
                          var storedPosition = localStorage.getItem('paginationPosition');
                          if (storedPosition) {
                            var pageInfo = JSON.parse(storedPosition);
                            var table = $('#datatable').DataTable();
                            table.page(pageInfo.page).draw('page');
                          }
                        });
                        
                        
                      });
                      
                      
                    </script>   
                    
                    <!-- /page content -->
                    
        