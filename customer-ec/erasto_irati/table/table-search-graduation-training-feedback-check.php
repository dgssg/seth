<?php
include("../database/database.php");
$codigoget = ($_GET["id"]);
    $query = "SELECT 
    tq.question, 
    utr.id, 
    utr.user_response, 
    ta.answer, 
    utr.is_correct 
    FROM user_training_attempts uta
    INNER JOIN documentation_graduation dg ON uta.id_training = dg.id
    INNER JOIN category c ON c.id = dg.id_categoria
    INNER JOIN equipamento_familia ef ON ef.id = dg.id_equipamento_familia
    INNER JOIN usuario u ON u.id = uta.id_user
    INNER JOIN user_training_responses utr ON utr.id_attempt = uta.id
    INNER JOIN training_questions tq ON tq.id = utr.id_question
    LEFT JOIN training_answers ta ON ta.id = utr.id_answer -- Relacionando corretamente com a resposta do usuário
    WHERE uta.id = $codigoget
    GROUP BY utr.id, tq.question, ta.answer;
 ";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
