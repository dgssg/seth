<?php
    date_default_timezone_set('America/Sao_Paulo');
    $today = date("Y-m-d");
    $read_open = 1;
    $conn = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_seth_erasto_irati");
    $conn_crontrol = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_seth_erasto_irati");
    $conn_crontrol_bell = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_seth_erasto_irati");
    

    $query = "SELECT 
    results.id,
    results.data,
    results.origem,
    results.alert,
    results.ativo
    FROM (
        SELECT 
        regdate_os_posicionamento.id_os AS id,
        regdate_os_posicionamento.data_end AS data,
        'Posicionamento O.S' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(regdate_os_posicionamento.data_end, NOW()) AS alerta
        FROM 
        regdate_os_posicionamento,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 1) AS notification
        WHERE 
        regdate_os_posicionamento.data_end IS NOT NULL  
        
        
        UNION ALL
        
        SELECT
        documentation_bussines.nome AS id,
        documentation_bussines.data_before AS data,
        'Documentação Empresa' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(documentation_bussines.data_before, NOW()) AS alerta
        FROM 
        documentation_bussines,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 2) AS notification
        WHERE 
        documentation_bussines.data_before IS NOT NULL  
        
        
        UNION ALL
        
        SELECT
        documentation_hfmea.titulo AS id,
        documentation_hfmea.data_before AS data,
        'Documentação HFMEA' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(documentation_hfmea.data_before, NOW()) AS alerta
        FROM 
        documentation_hfmea,
        (SELECT * FROM documentation_notification WHERE ativo = 0 and id = 3) AS notification
        WHERE 
        documentation_hfmea.data_before IS NOT NULL  
        
        
        UNION ALL
        
        SELECT
        documentation_pop.titulo AS id,
        documentation_pop.data_after AS data,
        'Documentação POP' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(documentation_pop.data_after, NOW()) AS alerta
        FROM 
        documentation_pop,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 4) AS notification
        WHERE 
        documentation_pop.data_after IS NOT NULL 
        
        
        UNION ALL
        
        SELECT
        documentation_suport.titulo AS id,
        documentation_suport.data_before AS data,
        'Documentação Contingência' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(documentation_suport.data_before, NOW()) AS alerta
        FROM 
        documentation_suport,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 5) AS notification
        WHERE 
        documentation_suport.data_before IS NOT NULL 
        
        
        UNION ALL
        
        SELECT
        calibration_parameter_tools.nome AS id,
        calibration_parameter_tools.date_validation AS data,
        'Equipamento Calibração' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(calibration_parameter_tools.date_validation, NOW()) AS alerta
        FROM 
        calibration_parameter_tools,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 6) AS notification
        WHERE 
        calibration_parameter_tools.date_validation IS NOT NULL    
        
        
        UNION ALL
        
        SELECT
        quarterly_analysis.analysis AS id,
        quarterly_analysis.data_after AS data,
        'Analise Trimestral' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(quarterly_analysis.data_after, NOW()) AS alerta
        FROM 
        quarterly_analysis,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 7) AS notification
        WHERE 
        quarterly_analysis.data_after IS NOT NULL  
        
        
        UNION ALL
        
        SELECT
        contracts_period.id_contracts AS id,
        contracts_period.date_end AS data,
        'Contratos' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(contracts_period.date_end, NOW()) AS alerta
        FROM 
        contracts_period,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 8) AS notification
        WHERE 
        contracts_period.date_end IS NOT NULL  
        
        UNION ALL
        
        SELECT
        equipamento.codigo AS id,
        equipamento.data_val AS data,
        'Equipamento' AS origem,
        notification.alert,
        notification.ativo,
        DATEDIFF(equipamento.data_val, NOW()) AS alerta
        FROM 
        equipamento,
        (SELECT * FROM documentation_notification  WHERE ativo = 0 and id = 9) AS notification
        WHERE 
        equipamento.data_val IS NOT NULL 
        
    ) AS results
    WHERE 
    results.ativo = 0 AND results.alerta <= results.alert;

";

    $resultados = $conn->query($query);
    
    $rows = array();
    
    
    while ($r = mysqli_fetch_assoc($resultados)) {
        $rows[] = $r;
        $origem = $r['origem']; // Substitua 'origem' pelo nome correto da coluna na sua consulta
        $ref = $r['id']; // Substitua 'id' pelo nome correto da coluna na sua consulta
        $data = $r['data']; // Substitua 'data' pelo nome correto da coluna na sua consulta
        
        // Verifique se já existe uma entrada com os mesmos dados na tabela
        $query_select = "SELECT COUNT(*) AS count FROM notif WHERE origem = '$origem' AND ref = '$ref' AND data = '$data'";
        $result_select = $conn_crontrol->query($query_select);
        $row_select = mysqli_fetch_assoc($result_select);
        $count = $row_select['count'];
        
        // Se não existir, insira os dados na tabela
        if ($count == 0) {
            $stmt = $conn_crontrol_bell->prepare("INSERT INTO notif (origem, ref, data, read_open) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("sssi", $origem, $ref, $data, $read_open);
            $stmt->execute(); // Execute a declaração preparada para inserir os dados
            $stmt->close(); // Feche a declaração preparada
        }
    }
    
    $conn->close();
?>
