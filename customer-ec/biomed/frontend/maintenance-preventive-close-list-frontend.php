


<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>#</th>
                          <th>Grupo</th>
                    <th>Rotina</th>
                    <th>Periodicidade</th>
                    <th>Codigo</th>
                    <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                         
                          <th>N/S</th> 
                       
                           
                        <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                        <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                        <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	   
                          <th>Baixa</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                       
                     
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

              <script language="JavaScript">
 
 $(document).ready(function() {
  $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );
  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-preventive-close.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
  .then(data => {
    // Mapeia os dados para o formato esperado pelo DataTables
    const novosDados = data.map(item => [
      ``,
      item.grupo,
  item.rotina,

  item.periodicidade == 5 ? "Diaria [1 Dias Uteis]"
      : item.periodicidade == 365 ? "Anual [365 Dias]"
      : item.periodicidade == 180 ? "Semestral [180 Dias]"
      : item.periodicidade == 30 ? "Mensal [30 Dias]"
      : item.periodicidade == 1 ? "Diaria [1 Dias]"
      : item.periodicidade == 7 ? "Semanal [7 Dias]"
      : item.periodicidade == 14 ? "Bisemanal [14 Dias]"
      : item.periodicidade == 21 ? "Trisemanal [21 Dias]"
      : item.periodicidade == 28 ? "Quadrisemanal [28 Dias]"
      : item.periodicidade == 60 ? "Bimestral [60 Dias]"
      : item.periodicidade == 90 ? "Trimestral [90 Dias]"
      : item.periodicidade == 120 ? "Quadrimestral [120 Dias]"
      : item.periodicidade == 730 ? "Bianual [730 Dias]"
      : item.periodicidade == 1095 ? "Trianual [1095 Dias]"
      : item.periodicidade == 1460 ? "Quadrianual [1460 Dias]"
      
      : "",
      item.codigo,
  item.equipamento,
  item.modelo,
  item.fabricante,
  item.serie,
 
  item.instituicao,
  item.setor,
  item.area,
      item.baixa == "0" ? "Sim" : "Não",

 
  
  `    
                   <a class="btn btn-app"  href="maintenance-preventive-close-edit?routine=${item.rotina}"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar MP',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-database"></i> Visualizar MP
                  </a>
                   <a class="btn btn-app"  href="maintenance-preventive-viwer-print-control?routine=${item.rotina}" target="_blank"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
                  <a class="btn btn-app"  href="maintenance-preventive-open-tag-control?routine=${item.rotina}" target="_blank"   onclick="new PNotify({
                               title: 'Visualizar',
                               text: 'Visualizar Etiqueta',
                               type: 'info',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-qrcode"></i> Etiqueta
                 </a>
                 <a class="btn btn-app"  href="maintenance-preventive-label-control?routine=${item.rotina}" target="_blank" onclick="new PNotify({
                              title: 'Etiqueta',
                              text: 'Abrir Etiqueta',
                              type: 'sucess',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-fax"></i> Rótulo
                </a>`

]);

    // Adiciona as novas linhas ao DataTables e desenha a tabela
    $('#datatable').DataTable().rows.add(novosDados).draw();

 
// Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([1,2,3,4,5,6,7,8,9,10,11,12]).every(function (d) {
        var column = this;
      var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
    });
});


</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#btn-carregar').click(function() {
    $.ajax({
      url: 'loading/load_equipamento.php', // substitua pelo nome do seu arquivo PHP que busca os itens restantes da tabela
      type: 'POST', // ou 'GET', dependendo da sua implementação
      data: {
        offset: 10 // substitua pelo número de itens que você já carregou
      },
      success: function(data) {
        $('#datatable').append(data); // adicione os novos itens à tabela
      }
    });
  });
});

</script>