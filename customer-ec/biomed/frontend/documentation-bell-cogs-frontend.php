
<!-- page content -->
       <div class="right_col" role="main">
         <div class="">
           <div class="page-title">
             <div class="title_left">
               <h3>Central de Notificações <small></small></h3>
             </div>


           </div>

           <div class="clearfix"></div>

            <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                
                <style>
                  .btn.btn-app {
                    border: 2px solid transparent; /* Define a borda como transparente por padrão */
                    padding: 5px;
                    position: relative; /* Necessário para posicionar o pseudo-elemento */
                  }
                  
                  .btn.btn-app.active {
                    border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
                  }
                  
                  .btn.btn-app.active::after {
                    content: "";
                    position: absolute;
                    left: 0;
                    bottom: 0; /* Posição da linha no final do elemento */
                    width: 100%;
                    height: 3px; /* Espessura da linha */
                    background-color: #ffcc00; /* Cor da linha */
                  }
                  
                </style>
                <?php
                  $current_page = basename($_SERVER['REQUEST_URI'], ".php");
                ?>
                
                
                
                <a class="btn btn-app <?php echo $current_page == 'documentation' ? 'active' : ''; ?>" href="documentation">
                  <i class="fa fa-code"></i> Sistema
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-pop' ? 'active' : ''; ?>" href="documentation-pop">
                  
                  <i class="fa fa-book"></i> POP
                </a>
                <a class="btn btn-app <?php echo $current_page == 'documentation-update' ? 'active' : ''; ?>" href="documentation-update">
                  
                  <i class="fa fa-history"></i> Atualização
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-hfmea' ? 'active' : ''; ?>" href="documentation-hfmea">
                  
                  <i class="fa fa-map"></i> HFMEA
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-label' ? 'active' : ''; ?>" href="documentation-label">
                  
                  <i class="fa fa-fax"></i> Rotuladora
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-bussines' ? 'active' : ''; ?>" href="documentation-bussines">
                  
                  <i class="fa fa-folder-open"></i> Documentação
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-support' ? 'active' : ''; ?>" href="documentation-support">
                  
                  <i class="fa fa-support"></i> Contingência
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-bell' ? 'active' : ''; ?>" href="documentation-bell">
                  
                  <i class="fa fa-bell"></i> Central de Notificações
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-graduation' ? 'active' : ''; ?>" href="documentation-graduation">
                  
                  <i class="fa fa-graduation-cap"></i> Educação Continuada
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-library' ? 'active' : ''; ?>" href="documentation-library">
                  
                  <i class="fa fa-book"></i> Biblioteca
                </a> 
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-connectivity' ? 'active' : ''; ?>" href="documentation-connectivity">
                  
                  <i class="fa fa-code-fork"></i> Conectividade &amp; Recursos
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-event-task' ? 'active' : ''; ?>" href="documentation-event-task">
                  
                  <i class="fa fa-tasks"></i> Eventos &amp; Taferas
                </a>                 
                <a class="btn btn-app <?php echo $current_page == 'documentation-widget' ? 'active' : ''; ?>" href="documentation-widget">
                  
                  <i class="fa fa-desktop"></i> Widget
                </a>
                <a class="btn btn-app" href="http://assinador.iti.br/" target="_blank" rel="noopener noreferrer">
                <i class="fa fa-external-link"></i> ITI Assinador 
            </a>
                
                
                
                
                
              </div>
            </div>

             <div class="clearfix"></div>
            <div class="clearfix"></div>
            
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <a class="btn btn-app"  href="documentation-bell-alert">
                      <i class="glyphicon glyphicon-list-alt"></i> Historico
                    </a>
                    
                    <a class="btn btn-app"  href="documentation-bell">
                      <i class="glyphicon glyphicon-plus"></i> Notificações
                    </a>
                    
                        <a class="btn btn-app"  href="documentation-bell-cogs">
                      <i class="glyphicon glyphicon-cog"></i> Configuração
                    </a>           
                    
                    
                                 
                    
                    
                    
                  </div>
                </div>
              </div>
            </div>
            

           
            <div class="clearfix"></div>


             <div class="x_panel">
                <div class="x_title">
                  <h2>Configuração Central de Notifcaição</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                 <form action="backend/documentation-bell-cogs-backend.php" method="post" >
                 
                   <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align">Plano de Obsolescência</label>
            <div class="col-md-6 col-sm-6 ">
              <div class="">
                <label>
                  <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="bell_01"type="checkbox" class="js-switch" <?php if($bell_01 == "0"){printf("checked"); }?> />
                </label>
              </div>
            </div>
          </div>
                    <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Mês Plano de Obsolescência</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="month" id="month"  placeholder="equipamento">
										  <option value="">Selecione o Mês</option>
										  	<?php



										 $query = "SELECT id, month FROM kpi_month";

 if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $month);
     while ($stmt->fetch()) {
     
     ?>
<option value="<?php printf($id); ?>" <?php if($id == $month_01){ printf("selected"); }; ?> ><?php printf($month); ?></option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Ano "></span>

										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#month').select2();
                                      });
                                 </script>


                  
  <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align">Analise Trimenstral</label>
            <div class="col-md-6 col-sm-6 ">
              <div class="">
                <label>
                  <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="bell_02"type="checkbox" class="js-switch" <?php if($bell_02 == "0"){printf("checked"); }?> />
                </label>
              </div>
            </div>
          </div>




   <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit">Atualizar</button>
                         </center>
                        </div>
                      </div>
</form>
                      

       <!-- /page content -->
                        