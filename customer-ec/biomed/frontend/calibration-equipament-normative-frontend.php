

   <div class="right_col" role="main">

          <div class="">

            <div class="page-title">
              <div class="title_left">
                <h3>Calibração</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>






          <!-- PAGE CONTENT BEGINS -->







                   <div class="clearfix"></div>

          <div class="x_panel">
                   <div class="x_title">
                     <h2>Menu</h2>
                     <ul class="nav navbar-right panel_toolbox">
                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                       </li>
                       <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                             class="fa fa-wrench"></i></a>
                         <ul class="dropdown-menu" role="menu">
                           <li><a href="#">Settings 1</a>
                           </li>
                           <li><a href="#">Settings 2</a>
                           </li>
                         </ul>
                       </li>
                       <li><a class="close-link"><i class="fa fa-close"></i></a>
                       </li>
                     </ul>
                     <div class="clearfix"></div>
                   </div>
                   <div class="x_content">

                     <a class="btn btn-app"  href="calibration-equipament">
                       <i class="fa fa-file"></i> Laudos
                     </a>

                     <a class="btn btn-app"  href="calibration-equipament-new">
                       <i class="fa fa-plus-square"></i> Novo
                     </a>
                     <a class="btn btn-app"  href="calibration-equipament-procedure">
                       <i class="fa fa-list"></i> Procedimento
                     </a>
                      <a class="btn btn-app"  href="calibration-equipament-normative">
                        <i class="fa fa-list"></i> Norma IE 62353
                      </a>




                   </div>
                 </div>



                 <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Norma IEC 62353</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
               
                        
                        <table border="1">
                          <thead>
                            <tr>
                              <th rowspan="2">Corrente em mA</th>
                              <th colspan="2">Tipo B</th>
                              <th colspan="2">Tipo BF</th>
                              <th colspan="2">Tipo CF</th>
                            </tr>
                            <tr>
                              <th>N.C.</th>
                              <th>S.F.C.</th>
                              <th>N.C.</th>
                              <th>S.F.C.</th>
                              <th>N.C.</th>
                              <th>S.F.C.</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>E Corrente de Fuga para Terra Geral</td>
                              <td>0,5</td>
                              <td>1 a</td>
                              <td>0,5</td>
                              <td>1 a</td>
                              <td>0,5</td>
                              <td>1 a</td>
                            </tr>
                            <tr>
                              <td>E Corrente de Fuga para Terra de Equipamento (conforme notas b e d)</td>
                              <td>2,5</td>
                              <td>5 a</td>
                              <td>2,5</td>
                              <td>5 a</td>
                              <td>2,5</td>
                              <td>5 a</td>
                            </tr>
                            <tr>
                              <td>E Corrente de Fuga para Terra de Equipamento (conforme nota c)</td>
                              <td>5</td>
                              <td>10 a</td>
                              <td>5</td>
                              <td>10 a</td>
                              <td>5</td>
                              <td>10 a</td>
                            </tr>
                            <tr>
                              <td>E Corrente de Fuga da Carcaça</td>
                              <td>0,1</td>
                              <td>0,5</td>
                              <td>0,1</td>
                              <td>0,5</td>
                              <td>0,1</td>
                              <td>0,5</td>
                            </tr>
                            <tr>
                              <td>Corrente de Fuga para o Paciente (conforme Nota e)</td>
                              <td>a.c. 0,1</td>
                              <td>0,5</td>
                              <td>a.c. 0,1</td>
                              <td>0,5</td>
                              <td>a.c. 0,01</td>
                              <td>0,05</td>
                            </tr>
                            <tr>
                              <td>Corrente de Fuga para o Paciente (Tensão de REDE na parte de entrada ou saída do sinal)</td>
                              <td>-</td>
                              <td>5</td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
                            </tr>
                            <tr>
                              <td>Corrente de Fuga para o Paciente (Tensão de REDE na PART E APLICADA)</td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
                              <td>5</td>
                              <td>-</td>
                              <td>0,05</td>
                            </tr>
                            <tr>
                              <td>Corrente Auxiliar para o Paciente (conforme Nota e)</td>
                              <td>d.c. 0,01</td>
                              <td>0,05</td>
                              <td>d.c. 0,01</td>
                              <td>0,05</td>
                              <td>d.c. 0,01</td>
                              <td>0,05</td>
                            </tr>
                            <tr>
                              <td>Corrente Auxiliar para o Paciente (conforme Nota e)</td>
                              <td>a.c. 0,1</td>
                              <td>0,5</td>
                              <td>a.c. 0,1</td>
                              <td>0,5</td>
                              <td>a.c. 0,01</td>
                              <td>0,05</td>
                            </tr>
                          </tbody>
                        </table>
                        
                        <p>
                          N.C.: Condição Normal<br>
                          S.F.C.: Condição de Falha Única (Single Fault Condition)
                        </p>
                        
                        <p>
                          NOTAS sobre a Tabela IV da IEC 60601-1:1988<br>
                          a A única Condição de Falha Única para a Corrente de Fuga para Terra é a interrupção de um condutor de alimentação por vez .<br>
                          b PARTES ACESSÍVEIS do EQUIPAMENTO COM PROTEÇÃO TERRA e sem meios de aterramento protetor de outros equipamentos e que atende aos requisitos para a Corrente de Fuga para Carcaça e para a Corrente de Fuga para o Paciente (se aplicável). Exemplo: Alguns computadores com uma PARTE DE REDE telada.<br>
                          c Equipamentos especificados para serem INSTALADOS PERMANENTEMENTE com um condutor de terra protetor que está eletricamente conectado de tal forma que a conexão só pode ser solta com a ajuda de uma ferramenta e que está fixado ou de outra forma seguro mecanicamente em um local específico que só pode ser movido após o uso de uma ferramenta. Exemplos de tais equipamentos são: principais componentes de uma instalação de raios-X, como o gerador de raios-X, a mesa de exame ou tratamento; equipamentos com aquecedores isolados mineralmente; equipamentos com uma Corrente de Fuga para Terra maior do que a declarada na Tabela IV, primeira linha, que se deve ao cumprimento dos requisitos para a supressão de interferência de rádio.<br>
                          d Equipamentos de raios-X móveis e equipamentos móveis com isolamento mineral.<br>
                          e Os valores máximos para o componente a.c. da Corrente de Fuga para o Paciente e da corrente auxiliar do paciente especificados na Tabela IV referem-se ao componente a.c. dessas correntes.
                        </p>
                        
                   
                   
                  </div>
                </div>
              </div>
            </div>



                   <div class="clearfix"></div>

               

								<!-- PAGE CONTENT ENDS -->




                </div>
              </div>
            

             <!-- page content -->


















        <!-- /page content -->

    <!-- /compose -->


 <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
