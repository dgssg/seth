<?php
include("database/database.php");
$codigoget = ($_GET["id"]);
$query = "SELECT equipamento_grupo.nome, documentation_hfmea.id, documentation_hfmea.id_equipamento_grupo, documentation_hfmea.titulo, documentation_hfmea.ver, documentation_hfmea.file, documentation_hfmea.data_now, documentation_hfmea.data_before, documentation_hfmea.upgrade, documentation_hfmea.reg_date FROM documentation_hfmea  INNER JOIN equipamento_grupo ON equipamento_grupo.id = documentation_hfmea.id_equipamento_grupo WHERE documentation_hfmea.id = $codigoget ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($nome,$id, $id_equipamento_grupo,$titulo,$ver,$file,$data_now,$data_before,$upgrade,$reg_date);


   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}

?>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Troubleshooting <small>HFMEA</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>HFMEA</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                 <form action="backend/documentation-hfmea-edit-backend.php?hfmea=<?php printf($codigoget);?>" method="post">

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Grupo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($nome); ?>"  readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                   <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"  id="titulo" name="titulo"  value="<?php printf($titulo); ?>"  readonly="readonly">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Versão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="ver" class="form-control" type="text" name="ver"  value="<?php printf($ver); ?>"  readonly="readonly">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_now"  type="date" name="data_now"  value="<?php printf($data_now); ?>"  readonly="readonly" class="form-control">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da Proxima Revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_before"  type="date" name="data_before"  value="<?php printf($data_before); ?>"  readonly="readonly" class="form-control">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">

                        </div>
                      </div>


                                  </form>



                  </div>
                </div>
              </div>
	       </div>


            <div class="clearfix"></div>



          <div class="x_panel">
                <div class="x_title">
                  <h2>Modos de falhas</h2>
                  <ul class="nav navbar-right panel_toolbox">
                       <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo-->
       <?php
$query="SELECT documentation_hfmea_matrix.score,documentation_hfmea_prob.nome,documentation_hfmea_grav.nome,documentation_hfmea_itens.id, documentation_hfmea_itens.modo, documentation_hfmea_itens.causa, documentation_hfmea_itens.efeito,documentation_hfmea_itens.question_one,documentation_hfmea_itens.question_two,documentation_hfmea_itens.question_three,documentation_hfmea_itens.question_four FROM documentation_hfmea_itens INNER JOIN documentation_hfmea_grav ON documentation_hfmea_grav.id = documentation_hfmea_itens.id_documentation_hfmea_grav INNER JOIN documentation_hfmea_prob ON documentation_hfmea_prob.id = documentation_hfmea_itens.id_documentation_hfmea_prob INNER JOIN documentation_hfmea_matrix  ON documentation_hfmea_matrix.id_documentation_hfmea_grav = documentation_hfmea_itens.id_documentation_hfmea_grav and documentation_hfmea_matrix.id_documentation_hfmea_prob =  documentation_hfmea_itens.id_documentation_hfmea_prob WHERE documentation_hfmea_itens.id_documentation_hfmea like '$codigoget' order by modo ASC  ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($documentation_hfmea_matrix,$documentation_hfmea_prob,$documentation_hfmea_grav,$id,$modo,$causa,$efeito,$question_one,$question_two,$question_three,$question_four);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Modo</th>
                          <th>Causa</th>
                          <th>Efeito</th>
                          <th>Probalidade</th>
                          <th>Gravidade</th>
                          <th>Pontuação</th>
                          <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($modo); ?></td>
                          <td><?php printf($causa); ?></td>
                          <td><?php printf($efeito); ?></td>
                          <td><?php printf($documentation_hfmea_prob); ?></td>
                          <td><?php printf($documentation_hfmea_grav); ?></td>
                          <td><?php printf($documentation_hfmea_matrix); ?></td>
                          <td>

                             <a class="btn btn-app"  href="documentation-hfmea-troubleshooting-itens?id=<?php printf($id); ?>&hfmea=<?php printf($codigoget); ?>"onclick="new PNotify({
                                            title: 'Alteração',
                                            text: 'Alteração!',
                                            type: 'danger',
                                            styling: 'bootstrap3'
                                        });">
                                <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                              </a>
                          </td>
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>





                </div>
              </div>


               <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Itens</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                           <form action="backend/documentation-hfmea-insert-itens-backend.php?hfmea=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>

                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Modo</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="modo" class="form-control" type="text" name="modo"   >
                               </div>
                             </div>
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Causa</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="causa" class="form-control" type="text" name="causa"  >
                               </div>
                             </div>
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Efeito</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="efeito" class="form-control" type="text" name="efeito"  >
                               </div>
                             </div>

                             <div class="form-group row">
                               <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Gravidade <span class="required"></span>
                               </label>
                               <div class="input-group col-md-6 col-sm-6">
                                 <select type="text" class="form-control has-feedback-left" name="documentation_hfmea_grav" id="documentation_hfmea_grav" required="required" placeholder="Gravidade">
                               <option value="">Selecione uma Gravidade</option>
                                   <?php



                                   $result_cat_post  = "SELECT  id, nome FROM documentation_hfmea_grav";

                                   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                                   while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                     echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                                   }
                                   ?>

                                 </select>
                                 <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Gravidade "></span>

                               </div>
                             </div>



                             <div class="form-group row">
                               <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Probalidade <span class="required"></span>
                               </label>
                               <div class="input-group col-md-6 col-sm-6">
                                 <select type="text" class="form-control has-feedback-left" name="documentation_hfmea_prob" id="documentation_hfmea_prob" required="required" placeholder="Probalidade">
                               <option value="">Selecione uma Probalidade</option>
                                   <?php



                                   $result_cat_post  = "SELECT  id, nome FROM documentation_hfmea_prob";

                                   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                                   while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                     echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                                   }
                                   ?>

                                 </select>
                                 <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Probalidade "></span>

                               </div>
                             </div>



                             <div class="item form-group">
                       <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 1</label>
                       <div class="col-md-6 col-sm-6 ">

                           <label>
                            <input name="question_one"id="question_one"type="checkbox" class="js-switch" /> Sim
                          </label>

                       </div>
                     </div>
                     <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 2</label>
                      <div class="col-md-6 col-sm-6 ">

                          <label>
                              <input name="question_two"id="question_two" type="checkbox" class="js-switch" /> Sim
                            </label>

                      </div>
                    </div>
                    <div class="item form-group">
                     <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 3</label>
                     <div class="col-md-6 col-sm-6 ">

                         <label>
                             <input name="question_three"id="question_three"type="checkbox" class="js-switch" /> Sim
                           </label>

                     </div>
                   </div>
                   <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 4</label>
                    <div class="col-md-6 col-sm-6 ">

                        <label>
                            <input name="question_four"id="question_four"type="checkbox" class="js-switch" /> Sim
                          </label>

                    </div>
                  </div>







                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary" onclick="new PNotify({
                                    title: 'Registrado',
                                    text: 'Informações registrada!',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>

              <!-- -->


              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="documentation-hfmea">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
