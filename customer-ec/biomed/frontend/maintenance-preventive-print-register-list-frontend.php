<?php
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
include("database/database.php");



$query = "SELECT DAY(reg_date) AS dia,  month(reg_date) AS mes,  year(reg_date) AS ano FROM maintenance_print GROUP by dia";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($dia,$mes, $ano);



?>


<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Impressão</th>




                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td ><?php printf($dia); ?> - <?php printf($mes); ?> - <?php printf($ano); ?></td>




                          <td>

                  <a class="btn btn-app"  href="maintenance-preventive-print-register?dia=<?php printf($dia); ?>&mes=<?php printf($mes); ?>&ano=<?php printf($ano); ?>"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-database"></i> Visualizar
                  </a>



                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
