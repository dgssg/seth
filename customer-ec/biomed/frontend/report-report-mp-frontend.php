  <?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Manutenção Preventiva</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>




             <!-- page content -->








              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro <small>do Relatório</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="backend/report-report-mp-backend.php" method="post"  target="_blank">

 <div class="ln_solid"></div>

  <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Inicial<span
                          class="required"></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="date" name="date_start" ></div>
                    </div>

                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Final<span
                          class="required"></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="date" name="date_end" ></div>
                    </div>

 <div class="item form-group">
                         <?php if($assistence == "0"){  ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Empresa <span class="required"></span>
        </label>
        <?php }else {   ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
        </label>            
        <?php }  ?>	
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="instituicao" id="instituicao"  placeholder="Unidade" required>
										 <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	
										  	<?php



										   $sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$instituicao);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($instituicao);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
											</div>

								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#instituicao').select2();
                                      });
                                 </script>

                                                     	 <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
                            </label>
                            <div class="input-group col-md-6 col-sm-6">
										<select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
										  	  <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>

                                 <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required"></span>
                            </label>
                            <div class="input-group col-md-6 col-sm-6">
										<select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
										  	<option value="">Selecione uma Opção</option>
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>

<div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Equipamento <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
										  <option value="">Selecione o equipamento</option>
										  	<?php



										 $query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.trash = 1 ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?>- <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento').select2();
                                      });
                                 </script>

                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_status" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Status ">Status <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="os_status" id="os_status"  placeholder="Status">
	<option value="">Selecione um Status</option>
										  	<?php



										  $query = "SELECT * FROM maintenance_status ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $status,$cor);
   while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($status);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_status" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Status ">Grupo <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="equipamento_grupo">
										     <option value="">Selecione um grupo</option>
										  	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo WHERE trash = 1 ";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
	                                     	</select>
 <span class="input-group-btn">


										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">
										    </span>
										</span>										 </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>








								 <script>
                                    $(document).ready(function() {
                                    $('#os_status').select2();
                                      });
                                 </script>


                     <br>

                      
                      
                      <div class="ln_solid"></div>
                      
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Baixa</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="baixa" id="baixa" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#baixa').select2();
                        });
                      </script>
                      
                      <!--       <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Baixa</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="baixa" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="baixa" value="0" checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="baixa" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="baixa" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div>     --> 
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ativo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="ativo" id="ativo" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#ativo').select2();
                        });
                      </script>
                      
                      
                      <!--     <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Ativo</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="ativo" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="ativo" value="0"  checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="ativo" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="ativo" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div> -->
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Comodato</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="comodato" id="comodato" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#comodato').select2();
                        });
                      </script>
                      
                      
                      <!--  <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Comodato</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="comodato" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="comodato" value="0" checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="comodato" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="comodato" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div> -->
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Duplicidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="duplicidade" id="duplicidade" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#duplicidade').select2();
                        });
                      </script>
                      
                      <!--      <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Duplicidade</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="duplicidade" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="duplicidade" value="0" checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="duplicidade" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="duplicidade" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div> -->
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Preventiva</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="preventive" id="preventive" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#preventive').select2();
                        });
                      </script>
                      
                      <!--     <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Preventiva</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="preventive" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="preventive" value="0" checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="preventive" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="preventive" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div> -->
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Obsolecência</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="obsolecencia" id="obsolecencia" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#obsolecencia').select2();
                        });
                      </script>
                      
                      <!-- <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Obsolecencia</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="obsolecencia" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="obsolecencia" value="0" checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="obsolecencia" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="obsolecencia" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div> -->
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">RFID</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="rfid" id="rfid" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#rfid').select2();
                        });
                      </script>
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Contrato</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="contrato" id="contrato" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#contrato').select2();
                        });
                      </script>
                      
                      <!--  <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">RFID</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="rfid" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="rfid" value="0" checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="rfid" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="rfid" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div> -->
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Excluido</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="trash" id="trash" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#trash').select2();
                        });
                      </script>
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Locação</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="locacao" id="locacao" >
                            <option value="">Selecione o item</option>
                            <option value="0">Ambos</option>
                            <option value="1">Sim</option>
                            <option value="2">Não</option>
                            
                          </select>
                          <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Item "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#locacao').select2();
                        });
                      </script>
                      
                      <!--    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Excluido</label>
                      <div class="col-md-6 col-sm-6 ">
                      <div id="trash" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-secondary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="trash" value="0" checked="" class="flat">
                      &nbsp; Ambos &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="trash" value="1" class="flat">
                      Sim
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                      <input type="radio" name="trash" value="2" class="flat">
                      Nao
                      </label>
                      </div>
                      </div>
                      </div> -->
                      
                      <div class="ln_solid"></div>
                      

                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatório</button>
                         </center>
                        </div>
                      </div>




                  </div>
                </div>
              </div>
            </div>






             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="report-report">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




                </div>
              </div>







        </div>
            </div>
        <!-- /page content -->
   <script type="text/javascript">
    $(document).ready(function(){
        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });
    </script>
     <script type="text/javascript">
    $(document).ready(function(){
        $('#area').change(function(){
            $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
        });
    });
    </script>
                      