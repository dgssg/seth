<?php





namespace Phppot;

use Phppot\Model\FAQ;
$codigoget = ($_GET["laudo"]);
include("database/database.php");
$query = "SELECT calibration.val,calibration.id_responsavel,calibration.id_colaborador,calibration.codigo,calibration.id_equipamento,calibration.id_manufacture,equipamento_familia.id_equipamento_grupo,calibration.temp,calibration.hum,calibration.obs,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade,calibration.procedure_cal,calibration_parameter_manufacture.nome,calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration LEFT join equipamento on equipamento.id = calibration.id_equipamento LEFT join equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT join calibration_parameter_manufacture on calibration_parameter_manufacture.id =  calibration.id_manufacture LEFT join colaborador on colaborador.id = calibration.id_colaborador WHERE calibration.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($date_validation,$id_responsavel,$id_colaborador,$codigo_caliration,$id_equipamento,$id_manufacture,$id_equipamento_grupo,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}

$query = "SELECT colaborador.primeironome,colaborador.ultimonome,colaborador.entidade FROM calibration INNER JOIN colaborador on colaborador.id = calibration.id_responsavel WHERE calibration.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($primeironome_responsavel,$ultimonome_responsavel,$entidade_responsavel );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}

?>

<script src="./assets/js/inlineEdit.js"></script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>  Laudos de <small>Calibração</small></h3>
      </div>


    </div>
    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <div class="x_panel">
        <div class="x_title">
          <h2>Edição de Laudo </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <form  action="backend/calibration-report-update-report-copy-backend.php?laudo=<?php printf($codigoget);?>" method="post">

              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Laudo <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="id_laudo" name="id_laudo" value="<?php printf($id); ?>" readonly="readonly" required="required" class="form-control ">
                </div>
              </div>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Validade (Meses)</label>
                <div class="col-md-4 col-sm-4 ">
                  <input type="text" id="date_validation" name="date_validation"  value="<?php printf($date_validation); ?>"  class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Data Realização <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="date" id="data_start" name="data_start"readonly="readonly" value="<?php printf($data_start); ?>"  class="form-control ">
                </div>
              </div>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
                <div class="col-md-4 col-sm-4 ">
                  <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  >
                    <option value="	">Selecione a Empresa</option>
                    <?php



                    $sql = "SELECT  id, nome FROM calibration_parameter_manufacture ";


                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id,$nome);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>	"<?php if($id==$id_manufacture){printf("selected");};?>><?php printf($nome);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>


                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#id_manufacture').select2();
              });
              </script>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Equipamento</label>
                <div class="col-md-4 col-sm-4 ">
                  <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
                    <option value="	">Selecione o equipamento</option>
                    <?php



                    $query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao";

                    //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                    if ($stmt = $conn->prepare($query)) {
                      $stmt->execute();
                      $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>	"<?php if($id==$id_equipamento){printf("selected");};?>><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
                  </select>
                  <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#equipamento').select2();
              });
              </script>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo Externo</label>
                <div class="col-md-4 col-sm-4 ">
                  <input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($codigo_caliration);?>" >
                </div>
              </div>





            </div>
          </div>

          <div class="x_panel">
            <div class="x_title">
              <h2>Procedimento</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                    class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">


                <?php


                $query = "SELECT   pop, titulo, codigo FROM documentation_pop WHERE id_equipamento_grupo like '$id_equipamento_grupo' ";
                //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($pop, $titulo,$codigo);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                  }
                }


                ?>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Procedimento</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  class="form-control" type="text"   value="<?php printf($codigo); ?> - <?php printf($titulo); ?>"  readonly="readonly">
                  </div>
                </div>





              </div>
            </div>

            <div class="x_panel">
              <div class="x_title">
                <h2>Executantes</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <!--
                  <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Executante <span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 ">
                <input type="text" id="nome" name="nome" value="<?php printf($primeironome); ?> <?php printf($ultimonome); ?>"  class="form-control ">
              </div>
              <label class="col-form-label col-md-2 col-sm-2 label-align" for="nome">CREA/CFT <span class="required">*</span>
            </label>
            <div class="col-md-2 col-sm-2 ">
            <input type="text" id="nome" name="nome" value="<?php printf($entidade); ?>"  class="form-control ">
          </div>
        </div>
        <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Responsavel Técnico <span class="required">*</span>
      </label>
      <div class="col-md-4 col-sm-4 ">
      <input type="text" id="nome" name="nome" value="<?php printf($primeironome_responsavel); ?> <?php printf($ultimonome_responsavel); ?>"  class="form-control ">
    </div>
    <label class="col-form-label col-md-2 col-sm-2 label-align" for="nome">CREA/CFT  <span class="required">*</span>
  </label>
  <div class="col-md-2 col-sm-2 ">
  <input type="text" id="nome" name="nome" value="<?php printf($entidade_responsavel); ?>"  class="form-control ">
</div>
</div>

-->
<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Colaborador <span class="required" >*</span>
  </label>
  <div class="col-md-4 col-sm-4 ">
    <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador" value="<?php printf($id_tecnico); ?>">
      <option value="	">Selecione o Executante</option>


      <?php



      $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


      if ($stmt = $conn->prepare($sql)) {
        $stmt->execute();
        $stmt->bind_result($id,$primeironome,$ultimonome);
        while ($stmt->fetch()) {
          ?>
          <option value="<?php printf($id);?>	"<?php if($id==$id_colaborador){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
          <?php
          // tira o resultado da busca da mem��ria
        }

      }
      $stmt->close();

      ?>
    </select>
  </div>
</div>









<script>
$(document).ready(function() {
  $('#id_colaborador').select2();
});
</script>
<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Responsável ">Responsável Tecnico <span class="required" >*</span>
  </label>
  <div class="col-md-4 col-sm-4 ">
    <select type="text" class="form-control has-feedback-right" name="id_responsavel" id="id_responsavel"  placeholder="Responsavel" value="<?php printf($id_tecnico); ?>">
      <option value="	">Selecione o Responsável</option>


      <?php



      $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


      if ($stmt = $conn->prepare($sql)) {
        $stmt->execute();
        $stmt->bind_result($id,$primeironome,$ultimonome);
        while ($stmt->fetch()) {
          ?>
          <option value="<?php printf($id);?>	"<?php if($id==$id_responsavel){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
          <?php
          // tira o resultado da busca da mem��ria
        }

      }
      $stmt->close();

      ?>
    </select>
  </div>
</div>









<script>
$(document).ready(function() {
  $('#id_responsavel').select2();
});
</script>

</div>
</div>

<div class="x_panel">
  <div class="x_title">
    <h2>Condições de Calibração</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
          class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">


      <div class="item form-group">
        <label class="col-form-label col-md-2 col-sm-2 label-align"  for="nome">Temperatura <span class="required">(ºC)</span>
        </label>
        <div class="col-md-2 col-sm-2 ">
          <input type="text" id="temp" name="temp" value="<?php printf($temperatura); ?>"  class="form-control ">
        </div>
        <label class="col-form-label col-md-2 col-sm-2 label-align" for="nome">Umidade <span class="required">(%)</span>
        </label>
        <div class="col-md-2 col-sm-2 ">
          <input type="text" id="hum" name="hum" value="<?php printf($umidade); ?>"  class="form-control ">
        </div>
      </div>
      <label for="message_ne">Observação:</label>
      <textarea id="obs"  class="form-control" name="obs" value="<?php printf($obs); ?>"data-parsley-trigger="keyup"
      placeholder="<?php printf($obs); ?>" ><?php printf($obs); ?></textarea>



    </div>
  </div>

  <div class="x_panel">
    <div class="x_title">
      <h2>Atualizar</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
            class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <center>
          <div class="compose-footer">
            <button class="btn btn-sm btn-success" type="submit">Salvar</button>
          </div>
        </center>

      </form>

    </div>
  </div>

  <div class="x_panel">
    <div class="x_title">
      <h2>Parâmetro Geral</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
            class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <form  action="backend/calibration-report-update-all-k-copy-backend.php?laudo=<?php printf($codigoget);?>" method="post">



          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="k">FATOR DE ABRANGÊNCIA (K) <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="number" id="k" name="k" required="required" class="form-control " value="2">
            </div>
          </div>





          <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_3"> <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 ">

            <input type="submit" class="btn btn-primary" onclick="new PNotify({
              title: 'Registrado',
              text: 'Informações registrada!',
              type: 'success',
              styling: 'bootstrap3'
            });" value="Salvar" />

          </div>
        </form>

      </div>
    </div>

    <div class="clearfix"></div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Ensaio</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <?php


          $calibration_parameter_tools = "SELECT calibration_parameter_tools_dados.nome AS 'nome_unidade',calibration_parameter_tools_dados.unidade,calibration_parameter_tools.nome,calibration_parameter_tools.modelo,calibration_parameter_tools.fabricante,calibration_report.id_calibration_parameter_tools, calibration_report.id_calibration_parameter_tools_dados, calibration_report.id_calibration_parameter_dados_parameter FROM calibration_report INNER JOIN calibration_parameter_dados_parameter on calibration_parameter_dados_parameter.id = calibration_report.id_calibration_parameter_dados_parameter INNER JOIN calibration_parameter_tools on calibration_parameter_tools.id =  calibration_report.id_calibration_parameter_tools INNER JOIN calibration_parameter_tools_dados on calibration_parameter_tools_dados.id =  calibration_report.id_calibration_parameter_tools_dados WHERE calibration_report.id_calibration = '$codigoget' ORDER BY calibration_parameter_dados_parameter.nome ";

          $calibration_parameter_query= mysqli_query($conn, $calibration_parameter_tools);
          while($query_tools = mysqli_fetch_assoc($calibration_parameter_query) ) {
            $query_tools_nome_unidade=$query_tools['nome_unidade'];
            $query_tools_unidade=$query_tools['unidade'];
            $query_tools_nome=$query_tools['nome'];
            $query_tools_modelo=$query_tools['modelo'];
            $query_tools_fabricante=$query_tools['fabricante'];
            $query_tools_id_calibration_parameter_tools=$query_tools['id_calibration_parameter_tools'];
            $query_tools_id_calibration_parameter_tools_dados=$query_tools['id_calibration_parameter_tools_dados'];
            $query_tools_id_calibration_parameter_dados_parameter=$query_tools['id_calibration_parameter_dados_parameter'];
          }






          $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1" ;
          $resultado_cat_post = mysqli_query($conn, $result_cat_post);

          $query_text=0;
          $query_table=0;
          $block = 1;
          $query="SELECT  id FROM calibration_parameter_dados  WHERE id_equipamento_grupo = '$id_equipamento_grupo '";

          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id);
            while ($stmt->fetch()) {
              $id=$id;
            }
          }

          $calibration_parameter_dados_parameter  = "SELECT  id, nome, fabricante,modelo FROM calibration_parameter_tools";
          $resultado_calibration_parameter_dados_parameter = mysqli_query($conn, $calibration_parameter_dados_parameter);



          $query=" SELECT calibration_parameter_tools_dados.nome,calibration_parameter_tools_dados.unidade,calibration_parameter_tools.nome,calibration_parameter_tools.modelo,calibration_parameter_tools.fabricante,calibration_parameter_dados_parameter.erro,calibration_parameter_dados_parameter.d_1,calibration_parameter_dados_parameter.d_2,calibration_parameter_dados_parameter.unidade,calibration_parameter_dados_parameter.nome, calibration_report.id, calibration_report.id_calibration, calibration_report.id_calibration_parameter_tools, calibration_report.id_calibration_parameter_tools_dados, calibration_report.id_calibration_parameter_dados_parameter, calibration_report.ve, calibration_report.vl_1, calibration_report.vl_2, calibration_report.vl_3, calibration_report.vl_avg, calibration_report.erro, calibration_report.dp, calibration_report.ie, calibration_report.ia, calibration_report.k, calibration_report.ib, calibration_report.ic, calibration_report.status, calibration_report.upgrade, calibration_report.reg_date, calibration_report.trash FROM calibration_report INNER JOIN calibration_parameter_dados_parameter on calibration_parameter_dados_parameter.id = calibration_report.id_calibration_parameter_dados_parameter LEFT JOIN calibration_parameter_tools  ON calibration_parameter_tools.id = calibration_report.id_calibration_parameter_tools  LEFT JOIN calibration_parameter_tools_dados ON calibration_parameter_tools_dados.id = calibration_report.id_calibration_parameter_tools_dados WHERE calibration_report.id_calibration = '$codigoget' ORDER BY calibration_parameter_dados_parameter.nome ";
          $row=1;
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($unidade_nome,$unidade_unidade,$tools_nome,$tools_modelo,$tools_fabricante,$erro_ac,$d_1,$d_2,$unidade,$nome,$id, $id_calibration, $id_calibration_parameter_tools, $id_calibration_parameter_tools_dados, $id_calibration_parameter_dados_parameter, $ve, $vl_1, $vl_2, $vl_3, $vl_avg, $erro, $dp, $ie, $ia, $k, $ib, $ic, $status, $upgrade, $reg_date, $trash);

            while ($stmt->fetch()) {



              ?>
              <?php if($id_calibration_parameter_dados_parameter  !== $query_text){
                $query_text=$id_calibration_parameter_dados_parameter;
                ?>
              </tbody>
            </table>

            <td><center><h2>  <?php printf($nome); ?></td> <td><?php printf($unidade); ?> </h2> </center></td><br>

            <?php

            $tools_nome_select="";
            if($tools_nome==$tools_nome_select) {
              printf("  <div class='alert alert-danger alert-dismissible 'role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
              </button>
              <strong>Simulador ou Analisador Não Selecionado!</strong>
              </div>");
            };
            if($tools_nome != $tools_nome_select) {
              printf("  <div class='alert alert-success alert-dismissible 'role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
              </button>
              <strong>Simulador ou Analisador   Selecionado!</strong>
              </div>");
            };



            ?>

            <td>






              <div class="item form-group">


                <div class="col-md-4 col-sm-4 ">
                  <strong>  <td>Analisador &amp; Simulador :</td>  </strong>
                  <input  type="text" class="form-control"  name="tools_query" id="tools_query"   readonly="readonly"value="<?php printf($tools_nome); ?> - <?php printf($tools_modelo); ?> - <?php printf($tools_fabricante); ?>"  >





                </div>

                <div class="col-md-4 col-sm-4 ">
                  <strong>  <td>Parâmetro :</td>  </strong>
                  <input type="text" class="form-control"  name="parameter" id="parameter"  placeholder="Unidade" readonly="readonly" value=" <?php printf($unidade_nome); ?> - <?php printf($unidade_unidade); ?> " >




                </div>





                <div class="col-md-4 col-sm-4 ">
                  <td><strong>  <td>Incerteza Herdada:</td></strong> <input name="tools_dados_ih"  class="form-control"  value="<?php printf($tools_dados_ih); ?>" readonly="readonly" ></td>  </div>   </div><br>
                  <strong>  <td>Erro Aceitável (Unidade):</td></strong> <td> <?php if($erro_ac=="0"){printf("   <form  action='backend/calibration-report-update-erro-ap-copy-backend.php?laudo=$codigoget&class=$id_calibration_parameter_dados_parameter' method='post'> <input name='erro_vl' value='$d_1'> <input type='hidden' name='erro_ap' value='0'> 	<input type='submit' class='btn btn-primary' onclick='new PNotify({
                    title: 'Registrado',
                    text: 'Informações registrada!',
                    type: 'success',
                    styling: 'bootstrap3'
                  });'' value='Salvar' /></form>"); }?></td><br>
                  <strong> <td>Erro Aceitável~(Porcentagem):</td> </strong><td> <?php if($erro_ac=="1"){printf("<form  action='backend/calibration-report-update-erro-ap-copy-backend.php?laudo=$codigoget&class=$id_calibration_parameter_dados_parameter' method='post'> <input name='erro_vl' value='$d_2'> <input type='hidden' name='erro_ap' value='1'>  <input type='submit' class='btn btn-primary' onclick='new PNotify({
                    title: 'Registrado',
                    text: 'Informações registrada!',
                    type: 'success',
                    styling: 'bootstrap3'
                  });'' value='Salvar' /></form>"); }?></td><br></td>
                  <br>
                  <a class="btn btn-app"  href="backend/calibration-report-edit-new-add-copy-backend.php?id_calibration=<?php printf($id_calibration); ?>&id_calibration_parameter_dados_parameter=<?php printf($id_calibration_parameter_dados_parameter); ?>">
                    <i class="glyphicon glyphicon-plus"></i> Linha
                  </a>

                  <!--     <a class="btn btn-app"  href="" value="Refresh Page" onClick="window.location.href=window.location.href" >
                  <i class="fa fa-calculator"></i> Calcular
                </a> -->

                <!--     <a class="btn btn-app" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">
                <i class="fa fa-cogs"></i> Analisador &amp; Simulador
              </a> -->
              <!--    <a class="btn btn-app" onclick="open(' calibration-report-edit-upgrade-tools?id=<?php printf($id_calibration); ?>&class=<?php printf($id_calibration_parameter_dados_parameter); ?>','calibration-report-edit-upgrade-tools?id=<?php printf($id_calibration); ?>&class=<?php printf($id_calibration_parameter_dados_parameter); ?>','status=no,Width=320,Height=285');">
              <i class="fa fa-cogs"></i> Analisador &amp; Simulador
            </a> -->
            <a class="btn btn-app" href="calibration-report-edit-upgrade-tools?id=<?php printf($id_calibration); ?>&class=<?php printf($id_calibration_parameter_dados_parameter); ?>&grupo=<?php printf($id_equipamento_grupo); ?>&nome=<?php printf($nome); ?>">
              <i class="fa fa-cogs"></i> Analisador &amp; Simulador
            </a>
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Analisador &amp; Simulador</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <form action="backend/calibration-report-edit-upgrade-tools-backend.php?id=<?php printf($codigoget);?>&class=<?php printf($class);?>" method="post">
                      <div class="ln_solid"></div>

                      <div class="col-md-4 col-sm-4  form-group has-feedback">
                        <select type="text" class="form-control has-feedback-left" name="tools" id="tools"  placeholder="Unidade">

                          <?php



                          $result_cat_post  = "SELECT  id, nome,fabricante,modelo FROM calibration_parameter_tools  ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].' '.$row_cat_post['fabricante'].' '.$row_cat_post['modelo'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Analisador  "></span>


                      </div>

                      <script>
                      $(document).ready(function() {
                        $('#tools').select2();
                      });
                      </script>

                      <div class="col-md-4 col-sm-4  form-group has-feedback">
                        <select type="text" class="form-control has-feedback-right" name="parameter" id="parameter"  placeholder="Area">
                          <option value="">Selecione um Parametro</option>
                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>



                      </div>

                      <script>
                      $(document).ready(function() {
                        $('#parameter').select2();
                      });
                      </script>



                      <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Salvar Informações</button>
                      </div>

                    </form>

                    <script type="text/javascript">
                    $(document).ready(function(){
                      $('#tools').change(function(){
                        $('#parameter').load('sub_categorias_post_tools.php?tools='+$('#tools').val());
                      });
                    });
                    </script>

                  </div>
                  <div class="modal-footer">
                    <!--      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> -->
                  </div>

                </div>
              </div>
            </div>

            <a class="btn btn-app" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">
              <i class="glyphicon glyphicon-th-large"></i> Grau de Liberdade
            </a>

            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel2">Gau de Liberdade</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <form action="backend/calibration-report-edit-upgrade-k-copy-backend.php?class=<?php printf($class);?>&laudo=<?php printf($codigoget); ?>" method="post">
                      <div class="ln_solid"></div>


                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">K <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="number" value="2" id="k" name="k" class="form-control ">
                        </div>
                      </div>


                      <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Salvar Informações</button>
                      </div>

                    </form>

                  </div>
                  <div class="modal-footer">
                    <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> -->
                  </div>

                </div>
              </div>
            </div>

            <a class="btn btn-app"  href="backend/calibration-report-edit-trash-class.php?id=<?php printf($id_calibration); ?>&class=<?php printf($id_calibration_parameter_dados_parameter); ?>&laudo=<?php printf($codigoget); ?>" >
              <i class="glyphicon glyphicon-trash"></i> Excluir Classe
            </a>


            <table class="table table-striped jambo_table bulk_action">
              <thead>
                <tr>


                  <th class="table-header">VE</th>
                  <th class="table-header">VL-1</th>
                  <th class="table-header">VL-2</th>
                  <th class="table-header">VL-3</th>
                  <th class="table-header">VL-AVG</th>
                  <th class="table-header">Erro</th>
                  <th class="table-header">DP</th>
                  <th class="table-header">IA</th>
                  <th class="table-header">IB</th>
                  <th class="table-header">IC</th>
                  <th class="table-header">K</th>
                  <th class="table-header">IE</th>
                  <th class="table-header">Erro AC</th>
                  <th class="table-header">Status</th>
                  <th class="table-header">Ação</th>
                </tr>
              </thead>
              <tbody>
                <?php
                require_once ("Model/FAQ.php");
                $faq = new FAQ();
                $faqResult = $faq->getFAQ();

                foreach ($faqResult as $k => $v) {
                  if(($faqResult[$k]["id_calibration_parameter_dados_parameter"] == $id_calibration_parameter_dados_parameter) and ($faqResult[$k]["id_calibration"] == $codigoget)) {
                    ?>
                    <tr class="table-row">


                      <td bgcolor="" contenteditable="true"
                      onBlur="saveToDatabase(this,'ve','<?php echo $faqResult[$k]["id"]; ?>')"
                      onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["ve"]; ?></td>
                      <td bgcolor="" contenteditable="true"
                      onBlur="saveToDatabase(this,'vl_1','<?php echo $faqResult[$k]["id"]; ?>')"
                      onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["vl_1"]; ?></td>
                      <td bgcolor="" contenteditable="true"
                      onBlur="saveToDatabase(this,'vl_2','<?php echo $faqResult[$k]["id"]; ?>')"
                      onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["vl_2"]; ?></td>
                      <td bgcolor="" contenteditable="true"
                      onBlur="saveToDatabase(this,'vl_3','<?php echo $faqResult[$k]["id"]; ?>')"
                      onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["vl_3"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'vl_avg','<?php echo $faqResult[$k]["id"]; ?>')"onClick="showEdit(this);"><?php echo $faqResult[$k]["vl_avg"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'erro','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["erro"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'dp','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["dp"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'ia','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ia"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'ib','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ib"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'ic','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ic"]; ?></td>
                      <td bgcolor="" contenteditable="true" onBlur="saveToDatabase(this,'k','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["k"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'ie','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ie"]; ?></td>
                      <td bgcolor="" contenteditable="true" onBlur="saveToDatabase(this,'erro_ac','<?php echo $faqResult[$k]["id"]; ?>')"   onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["erro_ac"]; ?></td>
                      <td contenteditable="false" onBlur="saveToDatabase(this,'status','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);">  <?php if($faqResult[$k]["status"]=="0"){ printf("Aprovado");}  if($faqResult[$k]["status"]=="1"){ printf("Reprovado");} ?> </td>
                      <td > <center>
                        <a   class="btn btn-round " href="backend/calibration-report-drop-itens-copy.php?id=<?php echo $faqResult[$k]["id"];  ?>&laudo=<?php printf($codigoget); ?>"><i class="fa fa-trash"></i></a>
                        <a   class="btn btn-round " href="backend/calibration-report-calculator-itens-copy.php?id=<?php echo $faqResult[$k]["id"];  ?>"><i class="fa fa-calculator"></i></a></center></td>

                      </div>
                    </div>





                  </tr>
                  <?php
                } }
                ?>
              </tbody>
            </table>
          <?php  }} } ?>

        </div>
      </div>

      <div class="x_panel">
        <div class="x_title">
          <h2>Registrar ensaio</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form  action="backend/calibration-report-edit-copy-backend.php?laudo=<?php printf($codigoget);?>" method="post">


              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Parâmetro de Ensaio  <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <select type="text" class="form-control has-feedback-right" name="parametro" id="parametro"  placeholder="parametro">
                    <?php



                    $sql = "SELECT  calibration_parameter_dados_parameter.id, calibration_parameter_dados_parameter.nome, calibration_parameter_dados_parameter.unidade,calibration_parameter_dados.id FROM calibration_parameter_dados_parameter INNER JOIN calibration_parameter_dados on calibration_parameter_dados.id = calibration_parameter_dados_parameter.id_calibration_parameter_dados WHERE calibration_parameter_dados.id_equipamento_grupo = '$id_equipamento_grupo'";


                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id,$nome,$unidade,$id_equipamento_grupo_row);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>	"><?php printf($nome);?>-<?php printf($unidade);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
                  </select>
                  <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>


                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#parametro').select2();
              });
              </script>

              <!--	     <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Equipamento &amp; Analisador  <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
            <select type="text" class="form-control has-feedback-right" name="tools" id="tools"  placeholder="Equipamento">

            <?php



            $sql = "SELECT calibration_parameter_tools_dados.id, calibration_parameter_tools_dados.nome,calibration_parameter_tools_dados.unidade,calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo FROM calibration_parameter_tools  INNER JOIN calibration_parameter_tools_dados on calibration_parameter_tools_dados.id_calibration_parameter_tools = calibration_parameter_tools.id";


            if ($stmt = $conn->prepare($sql)) {
            $stmt->execute();
            $stmt->bind_result($id,$parametro, $unidade, $equipamento,$fabricante,$modelo);
            while ($stmt->fetch()) {
            ?>

            <option value="<?php printf($id);?>	"> <?php printf($equipamento);?>	<?php printf($fabricante);?> <?php printf($modelo);?> <?php printf($parametro);?> <?php printf($unidade);?></option>
            <?php
            // tira o resultado da busca da memória
          }

        }
        $stmt->close();

        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>


    </div>
  </div>
  <script>
  $(document).ready(function() {
  $('#tools').select2();
});
</script>  -->

<div class="ln_solid"></div>

<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="tools_rg">Equipamento &amp; Analisador  <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 ">
    <select type="text" class="form-control has-feedback-left" name="tools_rg" id="tools_rg"  placeholder="Equipamento e Analisador">

      <?php



      $result_cat_post  = "SELECT  id, nome,fabricante,modelo FROM calibration_parameter_tools WHERE id_equipamento_grupo  like '%$id_equipamento_grupo%' ";

      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
        echo '<option> </option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].' '.$row_cat_post['fabricante'].' '.$row_cat_post['modelo'].'</option>';
      }
      ?>

    </select>
    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Analisador  "></span>

  </div>
</div>

<script>
$(document).ready(function() {
  $('#tools_rg').select2();
});
</script>

<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="parameter_rg">Parâmetro <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 ">
    <select type="text" class="form-control has-feedback-right" name="parameter_rg" id="parameter_rg"  placeholder="Area">
      <option value="">Selecione um Parametro</option>
    </select>
    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


  </div>
</div>

<script>
$(document).ready(function() {
  $('#parameter_rg').select2();
});
</script>

<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="k">FATOR DE ABRANGÊNCIA (K) <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 ">
    <input type="text" id="k" name="k" required="required" class="form-control ">
  </div>
</div>
<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Valor de Referência  <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 ">
    <input type="text" id="ve" name="ve"  required="required" class="form-control ">
  </div>
</div>

<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_1">1º Ensaio <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 ">
    <input type="text" id="vl_1" name="vl_1"  required="required" class="form-control ">
  </div>
</div>
<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_2">2º Ensaioo <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 ">
    <input type="text" id="vl_2" name="vl_2"  required="required" class="form-control ">
  </div>
</div>
<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_3">3º Ensaio <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 ">
    <input type="text" id="vl_3" name="vl_3" required="required" class="form-control ">
  </div>
</div>
<label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_3"> <span class="required">*</span>
</label>
<div class="col-md-6 col-sm-6 ">

  <input type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informações registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" value="Salvar" />

</div>
</form>

</div>
</div>

<div class="x_panel">
  <div class="x_title">
    <h2>Ação</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
          class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

      <a class="btn btn-app"  href="calibration-report">
        <i class="glyphicon glyphicon-arrow-left"></i> Voltar
      </a>


      <a class="btn btn-app"  href="backend/pre-laudo-copy-backend.php?id_calibration=<?php printf($codigoget); ?>">
        <i class="glyphicon glyphicon-th-list"></i> Pré-Laudo
      </a>

      <a  class="btn btn-app" href="calibration-report-viewer-copy?laudo=<?php printf($codigoget); ?> "target="_blank" onclick="new PNotify({
        title: 'Visualizar',
        text: 'Visualizar Laudo!',
        type: 'info',
        styling: 'bootstrap3'
      });" >
      <i class="fa fa-file-pdf-o"></i> Visualizar
    </a>
    <!--       <a  class="btn btn-app" href="calibration-report-viewer-copy-2?laudo=<?php printf($codigoget); ?> "target="_blank" onclick="new PNotify({
    title: 'Visualizar',
    text: 'Visualizar Laudo!',
    type: 'info',
    styling: 'bootstrap3'
  });" >
  <i class="fa fa-file-pdf-o"></i> Visualizar H
</a> -->
<a class="btn btn-app"  href="backend/calibration-report-edit-calculator-backend.php?laudo=<?php printf($codigoget); ?>"  >
  <i class="fa fa-calculator"></i> Calcular
</a>
<a class="btn btn-app"  href="backend/calibration-report-trash-table-backend.php?laudo=<?php printf($codigoget); ?>"  >
  <i class="fa fa-recycle"></i> Exluir Ensaio
</a>

</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#tools_rg').change(function(){
    $('#parameter_rg').load('sub_categorias_post_tools_rg.php?tools_rg='+$('#tools_rg').val());
  });
});
</script>
