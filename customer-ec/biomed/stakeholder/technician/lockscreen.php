<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
	if($_SESSION['id_nivel'] != 2 ){
    header ("Location: ../index.php");
}
	$usuariologado=$_SESSION['usuario'];
	// Adiciona a Função display_campo($nome_campo, $tcruzvermelhacastro_campo)
	//require_once "personalizacao_display.php";
	//	if (!$PHPSESSID) {
	//	header ("Location: ../index.php");

	//}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href=""><b>SETH</b> System</a>
      </div>
      <!-- User name -->
      <div class="lockscreen-name"><?php printf($usuariologado) ?></div>

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <img src="../../logo/img.jpg" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class=""action="backend/lockscreen.php?" method="post">
          <div class="input-group">
            <input type="password" class="form-control" placeholder="password" name="password" id="password">

            <div class="input-group-btn">
              <button class="btn" ><i class="fa fa-arrow-right text-muted" ></i></button>
            </div>
          </div>
        </form><!-- /.lockscreen credentials -->

      </div><!-- /.lockscreen-item -->
      <div class="help-block text-center">
        Digite sua senha para recuperar sua sessão
      </div>
      <div class="text-center">
        <a href="login.php">Ou faça login como um usuário diferente</a>
      </div>
      <div class="lockscreen-footer text-center">
        Copyright &copy; 2020 <b><a href="http://mksistemasbiomedicos.com.br" class="text-black">MK Sistemas</a></b><br>
        All rights reserved
      </div>
    </div><!-- /.center -->

    <!-- jQuery 2.1.4 -->
    <script src="../../../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../../../framework/bootstrap/js/bootstrap.min.js"></script>
	</body>
  <script language="JavaScript">
 
 
 history.pushState(null, null, document.URL);
 window.addEventListener('popstate', function () {
     history.pushState(null, null, document.URL);
 });
 </script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;
  var timer;
  var interval;

  function countdown() {

      seconds--;
    if (seconds < 0) {
		clearInterval(interval);
      // Change your redirection link here
      window.location = "https://seth.mksistemasbiomedicos.com.br";
    } else if (seconds === 60) {
      // Show warning 60 seconds before redirect
      var warning = document.createElement("div");
      warning.innerHTML = "O sistema será desconectado por motivos de segurança em 60 segundos. Clique aqui para continuar no sistema e reiniciar o tempo.";
      warning.style.backgroundColor = "yellow";
      warning.style.padding = "10px";
      warning.style.position = "fixed";
      warning.style.bottom = "0";
      warning.style.right = "0";
      warning.style.zIndex = "999";
      warning.addEventListener("click", function() {
        warning.style.display = "none";
        clearTimeout(seconds);
		seconds = 1080;
        countdown();
      });
      document.body.appendChild(warning);
	  countdown();
    } else {
      // Update remaining seconds
      document.getElementById("countdown").innerHTML = seconds;
      // Count down using javascript
	
      timer = setTimeout(countdown, 1000);
    }
  }

  // Run countdown function
  countdown();
  
  
  function stopCountdown() {
    clearInterval(seconds);
  }
 
    // Reset countdown on user interaction
	document.addEventListener("click", function() {
    clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });
  document.addEventListener("mousemove", function() {
	seconds--;
    //clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });

</script>

 </html>
