<?php
include("../database/database.php");
// Parâmetro POST recebido da chamada AJAX
$offset = $_POST['offset'];

$query = "SELECT equipamento.serie,equipamento.comodato,equipamento.baixa,equipamento.ativo,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.trash = 1 OFFSET $offset";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($serie,$comodato,$baixa,$ativo,$instituicao,$area,$setor,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>
 
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($codigo); ?> </td>
                          <td><?php printf($nome); ?> </td>
                          <td> <?php printf($modelo); ?></td>
                          <td> <?php printf($fabricante); ?></td>
                         
                           <td><?php printf($serie); ?> </td>
                           <td><?php printf($instituicao); ?></td>
                           <td><?php printf($area); ?></td>
                           <td>  <?php printf($setor); ?></td>
      <td> <?php if($baixa=="0"){printf("Sim");}else{printf("Não");} ?> </td>
       <td> <?php if($ativo=="0"){printf("Sim");}else{printf("Não");} ?> </td>
       <td> <?php if($comodato=="0"){printf("Sim");} else{printf("Não");}?> </td>


                          <td>
                   <a class="btn btn-app"  href="register-equipament-edit?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>

                   <a class="btn btn-app"  href="register-equipament-tag?equipamento=<?php printf($id); ?>" target="_blank" onclick="new PNotify({
																title: 'Etiqueta',
																text: 'Abrir Etiqueta',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-qrcode"></i> Etiqueta
                  </a>
                  <a class="btn btn-app"  href="register-equipament-label?equipamento=<?php printf($id); ?>" target="_blank" onclick="new PNotify({
                               title: 'Etiqueta',
                               text: 'Abrir Etiqueta',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-fax"></i> Rótulo
                 </a>
                 <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-equipament-trash-backend?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                  <td>
                   <a class="btn btn-app"  href="register-equipament-record?equipamento=<?php printf($id); ?>" target="_blank" onclick="new PNotify({
																title: 'Ficha',
																text: 'Abrir Ficha',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Ficha
                  </a>
                   <a class="btn btn-app"  href="register-equipament-prontuario?equipamento=<?php printf($id); ?>" target="_blank" onclick="new PNotify({
																title: 'Prontuario',
																text: 'Abrir Prontuario',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-o"></i> Prontuario
                  </a>
                    <a class="btn btn-app"  href="register-equipament-disable?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Baixa',
																text: 'Baixa Equipamento',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-trash"></i> Baixa
                  </a>
                  </td>
                   <td>
                   <a class="btn btn-app"  href="register-equipament-check?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Check List',
																text: 'Abrir Check List',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> CheckList
                  </a>
                     <a class="btn btn-app"  href="register-equipament-training?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Treinamento',
																text: 'Abrir Treinamento',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> Treinamento
                  </a>
                     <a class="btn btn-app"  href="register-equipament-report?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Parecer',
																text: 'Abrir Parecer',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> Parecer
                  </a>
                     <a class="btn btn-app"  href="register-equipament-exit?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Saida',
																text: 'Abrir Saida',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> Saida
                  </a>
                     <a class="btn btn-app"  href="register-equipament-compliance?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Conformidade',
																text: 'Abrir Conformidade',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i>Conformidade
                  </a>

              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                  </td>

                  <td>
                         <a class="btn btn-app"  href="register-equipament-timeline?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Timeline',
																text: 'Abrir Timeline',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-hourglass"></i>Linha do Tempo
                  </a>
                  <a class="btn btn-app"  href="register-equipament-location?equipamento=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Localização',
																text: 'Abrir Localização',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-cogs"></i>Localização
                  </a>
             
                       </td>
                        </tr>
                        <?php   } }  ?>
                    