<?php

include("../database/database.php");

$codigo= $_POST['codigo'];
$id_equipamento= $_POST['equipamento'];
$data_start= $_POST['data_start'];
$val= $_POST['val'];
$ven= 0;
$status= 0;
$id_manufacture= $_POST['id_manufacture'];
$procedure_cal= $_POST['procedure'];
$id_colaborador= $_POST['id_colaborador'];
$id_responsavel= $_POST['id_responsavel'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];
$obs= $_POST['obs'];
if($obs==""){
    $obs="Este certificado &#233; v&#225;lido para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar.";
}

$stmt = $conn->prepare("INSERT INTO calibration (id_equipamento, data_start, val, ven, status, id_manufacture, procedure_cal, id_colaborador, id_responsavel, temp, hum, obs,codigo) VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?,?,?,?)");
$stmt->bind_param("sssssssssssss",$id_equipamento, $data_start, $val, $ven, $status, $id_manufacture, $procedure_cal, $id_colaborador, $id_responsavel, $temp, $hum, $obs,$codigo);
$execval = $stmt->execute();
 $last_id = $conn->insert_id;
 $stmt->close();


$query = "SELECT equipamento_familia.id_equipamento_grupo, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM equipamento   INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia  WHERE equipamento.id like '$id_equipamento' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_equipamento_grupo,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  }

}
 $stmt->close();
  $query="SELECT  id FROM calibration_parameter_dados  WHERE id_equipamento_grupo = '$id_equipamento_grupo '";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id);
   while ($stmt->fetch()) {
       $id=$id;
   }
}
 $stmt->close();

 $query="SELECT  id FROM calibration_parameter_dados_parameter  WHERE id_calibration_parameter_dados = '$id '";

$query_result = mysqli_query($conn, $query);
					while($row_cat_post = mysqli_fetch_assoc($query_result) ) {
						$row_cat=$row_cat_post['id'];
						$stmt = $conn->prepare("INSERT INTO calibration_report (id_calibration, id_calibration_parameter_dados_parameter) VALUES (?, ?)");
$stmt->bind_param("ss",$last_id, $row_cat);
$execval = $stmt->execute();
					}
 $stmt->close();

 $stmt = $conn->prepare("UPDATE  equipamento SET data_calibration = ? WHERE id= ?");
 $stmt->bind_param("ss",$data_start,$id_equipamento);
 $execval = $stmt->execute();
 $stmt->close();

 $stmt = $conn->prepare("UPDATE  equipamento SET id_calibration = ? WHERE id= ?");
 $stmt->bind_param("ss",$last_id,$id_equipamento);
 $execval = $stmt->execute();
 $stmt->close();

    $data_calibration_end=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));

 $stmt = $conn->prepare("UPDATE  equipamento SET data_calibration_end = ? WHERE id= ?");
 $stmt->bind_param("ss",$data_calibration_end,$id_equipamento);
 $execval = $stmt->execute();
 $stmt->close();


//echo "New records created successfully";

setcookie('anexo', null, -1);
header('Location: ../calibration-report-edit-copy?laudo='.$last_id);
?>
