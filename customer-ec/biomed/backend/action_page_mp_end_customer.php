<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;
	
	require '../../../framework/PHPMailer/src/PHPMailer.php';
	require '../../../framework/PHPMailer/src/SMTP.php';
	require '../../../framework/PHPMailer/src/Exception.php';
	
	include("../database/database.php");
	session_start();
	
	$codigoget = $_GET['codigoget'];
	$id_mp_reurn = $codigoget;
	$programada_data = $_GET['programada_data'];
	$assessment = $_GET['fornecedor'];
	$number_assessment = $_GET['data_assessment'];
	$data_assessment = $_GET['obs'];
	$id_fornecedor = $_GET['date_start'];
	$id_service = $_GET['date_end'];
	$obs_general = $_GET['obs'];
	$time_obs = $_GET['time'];
	$colaborador_obs = $_GET['colaborador'];
	$message_mp = $_GET['message_mp'];
	$message_tc = $_GET['message_tc'];
	$temp_obs = $_GET['temp'];
	$hum_obs = $_GET['hum'];
	$v4uuid_obs = $_GET['v4uuid'];
	$chave_obs = $_GET['chave'];
	$assinature_obs = $_GET['assinature'];

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$setor = $_SESSION['setor'];
	$usuario = $_SESSION['id_usuario'];
	
	$assessment=$_GET['assessment'];
	$number_assessment=$_GET['number_assessment'];
	$data_assessment=$_GET['data_assessment'];
	$id_fornecedor=$_GET['id_manufacture'];
	$id_service=$_GET['fornecedor_service'];
	$obs=$_GET['obs'];
	
	date_default_timezone_set('America/Sao_Paulo');
	//$codigoget = ($_GET["id"]);
	$fornecedor = $_GET['fornecedor'];
	$fornecedor_assement = $fornecedor;

	$date_mp_start = $_GET['date_start'];
	$date_mp_end = $_GET['date_end'];
	$time_mp = $_GET['time'];
	$colaborador = $_GET['colaborador'];
	$message_mp = $_GET['message_mp'];
	$message_tc = $_GET['message_tc'];
	$routine = $_GET['routine'];
	$periodicidade = $_GET['periodicidade'];
	$programada= $_GET['programada'];
	$temp= $_GET['temp'];
	$hum= $_GET['hum'];
	$table = $_GET['table'];
	$tabelaHTML = $_GET['tabelaHTML'];

	
	$anexo=$_COOKIE['anexo'];
	
	$status="3";
	$status2=0;
	$count=0;
	$id_mod=2;
	$mp_control = 2;
	
	
	$stmt = $conn->prepare("UPDATE maintenance_customer SET id_colaborador= ? WHERE id= ?");
	$stmt->bind_param("ss",$colaborador,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_customer SET status= ? WHERE id= ?");
	$stmt->bind_param("ss",$mp_control,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
		
	$stmt = $conn->prepare("UPDATE maintenance_customer SET obs_mp= ? WHERE id= ?");
	$stmt->bind_param("ss",$message_mp,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_customer SET obs_tec= ? WHERE id= ?");
	$stmt->bind_param("ss",$message_tc,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	 
	
	$stmt = $conn->prepare("UPDATE maintenance_customer SET temp= ? WHERE id= ?");
	$stmt->bind_param("ss",$temp,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_customer SET hum= ? WHERE id= ?");
	$stmt->bind_param("ss",$hum,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	 
	$stmt = $conn->prepare("UPDATE maintenance_customer SET id_signature= ? WHERE id= ?");
	$stmt->bind_param("ss",$v4uuid_obs,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_customer SET id_user= ? WHERE id= ?");
	$stmt->bind_param("ss",$usuario,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	
	
	
	 
	
	
	setcookie('anexo', null, -1);
	header('Location: ../maintenance-preventive-customer-open');
	
	
	
?>

