<?php
    include("database/database.php");
// Verifica se os dados foram enviados via POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Captura o ID e o conteúdo da tabela
    $codigoget = $_POST['id'];
    $tabelaHtml = $_POST['tabelaHtml'];
    
    $stmt = $conn->prepare("UPDATE maintenance_customer SET preventiva= ? WHERE id= ?");
    $stmt->bind_param("ss",$tabelaHtml,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();

} 
?>
