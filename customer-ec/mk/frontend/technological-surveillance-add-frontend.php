  <?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../

$query = "SELECT os.time_backlog,os.id_fornecedor,os.id_prioridade,os.id_observacao, os.id_chamado,os.id_tecnico,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia WHERE os.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($time_backlog,$id_fornecedor,$id_prioridade,$id_observacao,$id_chamado,$id_tecnico,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);
 


while ($stmt->fetch()) {

}
}
?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ordem de Serviço</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Descrição <small>da Solicitação</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os">OS <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="os" name="os" value="<?php printf($id_os); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="solicitante">Solicitante<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="solicitante" name="solicitante" required="required" class="form-control"  value="<?php printf($id_usuario_nome); ?> <?php printf($id_usuario_sobrenome); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="setor" class="form-control" type="text" name="setor"  value="<?php printf($id_area); ?> <?php printf($id_localizacao); ?>" readonly="readonly">
                        </div>
                      </div>
                     
                     
                      <div class="ln_solid"></div>
                    
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento">Equipamento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="equipamento" name="equipamento" value="<?php printf($equipamento_nome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="modelo">Modelo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="modelo" name="modelo" required="required" class="form-control"  value="<?php printf($equipamento_modelo); ?> " readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($equipamento_codigo); ?> " readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Numero de Serie</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="numeroserie" class="form-control" type="text" name="numeroserie"  value="<?php printf($equipamento_numeroserie); ?> " readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Patrimonio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="patromonio" class="form-control" type="text" name="patromonio"  value="<?php printf($equipamento_patrimonio); ?> " readonly="readonly">
                        </div>
                      </div>
                        <div class="ln_solid"></div>
                          <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Solicitação</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="solicitacao" class="form-control" type="text" name="solicitacao"  value="<?php printf($id_solicitacao); ?> " readonly="readonly">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         
            
             <!-- page content -->
       
           

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Anexo <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">
                       <div class="col-md-55">
                          <?php if($id_anexo ==! "" ){ ?>
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="dropzone/os/<?php printf($id_anexo); ?>" alt="image" />
                            <div class="mask">
                              <p>Anexo</p>
                              <div class="tools tools-bottom">
                                <a href="dropzone/os/<?php printf($id_anexo); ?>" download><i class="fa fa-download"></i></a>
                                
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>Anexo ordem de serviço</p>
                          </div>
                        </div>
                          <?php }; ?>
                      </div>
                     
                     
                    
                   
                  

                   
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
          
         <div class="x_panel">
                <div class="x_title">
                  <h2>Tecnovigilancia</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                       <form  action="backend/technological-surveillance-add-backend.php?os=<?php printf($codigoget); ?> " method="post">
                       <div class="ln_solid"></div>

<label for="descricao">Descrição:</label>
<textarea id="descricao" class="form-control" name="descricao" ></textarea>

<div class="ln_solid"></div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
                        <div class="col-md-6 col-sm-6 ">
                         <div class="">
                            <label>
                              <input name="status"type="checkbox" class="js-switch"  value="0"  /> 
                            </label>
                          </div>
                        </div>
                      </div>
                   
                          <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="technological_surveillance_list" class="docs-tooltip" data-toggle="tooltip" title="Selecione um Risco ">Risco <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="technological_surveillance_list" id="technological_surveillance_list"   >
							
										  	<?php
										  	
										  
										  	
										    $sql = "SELECT  id, codigo,nome FROM technological_surveillance_list ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$codigo,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                      
     
                     
									
									
										
									
								     
								 <script>
                                    $(document).ready(function() {
                                    $('#technological_surveillance_list').select2();
                                      });
                                 </script>
      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Inicio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_inicio"  type="date" name="data_inicio" class="form-control" >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Fim</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_fim"  type="date" name="data_fim"  class="form-control" >
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>

<label for="acao">Ação:</label>
<textarea id="acao" class="form-control" name="acao" ></textarea>


                      <div class="ln_solid"></div>

<label for="obs">Observação:</label>
<textarea id="obs" class="form-control" name="obs" ></textarea>

                   <div class="ln_solid"></div>
                  
    
                   <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>
     
     
                                  </form>
                  
                </div>
              </div>
              
         
    <div class="clearfix"></div>

         
        <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- Posicionamento -->
    
	          
              <!-- Registro -->	       
	       
	       
            <div class="clearfix"></div>
 
             
            
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="os-progress">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
              
                 
                   
             <!--    ref="backend/os-progress-upgrade-close-frontend.php?os=<?php printf($codigoget); ?>"
             
             <a class="btn btn-app"  href=""onclick="new PNotify({
																title: 'Atualização',
																text: 'Atualização de Abertura de O.S!',
																type: 'danger',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                  </a> -->
                
                
                  
                   
                  
                </div>
              </div>
              
              
              

            

            
        </div>
            </div>
        <!-- /page content -->
     <!-- Switchery -->
    <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
      <!-- Switchery -->
    <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">