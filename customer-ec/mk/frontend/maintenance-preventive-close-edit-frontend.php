  <?php
$codigoget = ($_GET["routine"]);

// Create connection

include("database/database.php");
$query = "SELECT maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id_routine like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
}
}

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Manutenção <small>Preventiva</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Procedimento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="rotina" name="rotina" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> " readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="categoria" name="categoria" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="info" class="form-control" type="text" name="info"  value="<?php printf($time_ms); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade (dias)</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($periodicidade); ?>" readonly="readonly">
                        </div>
                      </div>



                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>



                  </div>
                </div>
              </div>
	       </div>
<!-- Posicionamento -->
<div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>




<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>#</th>
                          <th>Data Programada</th>

                          <th>Data Abertura</th>
                          <th>Data Termino</th>

                          <th>Status</th>
                          <th>Anexo</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                        

                    

                
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>





 <!-- Posicionamento -->
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="maintenance-preventive-close">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
                </div>
                <script language="JavaScript">
 
 $(document).ready(function() {
  $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );
  var routine = document.getElementById("rotina").value;

  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-preventive-close-case.php?routine='+ routine;

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
  .then(data => {
    const novosDados = data.map(item => {
    // Separa as informações concatenadas em item.id_anexo
const id_anexoList = item.id_anexo2 ? item.id_anexo2.split(';') : [];

// Gera os links para cada item.id_anexo separado
const id_anexoLinks = id_anexoList.map(id_anexo2 => `<a href="dropzone/mp-dropzone//${id_anexo2}" download><i class="fa fa-paperclip"></i></a>`);

// Concatena os links gerados em uma única string
const id_anexoHtml = id_anexoLinks.join('');
    // Mapeia os dados para o formato esperado pelo DataTables
    // Retorna os dados no formato esperado pelo DataTables
    return [
      ``,
      item.date_start,
      item.reg_date = (item.reg_date ? new Date(item.reg_date.replace(/-/g, "/")).toLocaleDateString("pt-BR") : ""),
      item.date_mp_end,
      item.id_status == 1 ? "Aberto"
      : item.id_status == 2 ? "Atrasado"
      : item.id_status == 3 ? "Fechado"
      : item.id_status == 4 ? "Cancelado"
      : item.id_status == 5 ? "Reprogramado"
      : item.id_status == 6 ? "Antecipado"
      : item.id_status == 7 ? "Retroativo"
      : item.id_status == 8 ? "Avulso"
      : item.id_status == 9 ? "Excluido"
      : item.id_status == 10 ? "Programado"
    
      : "",
      (item.id_anexo || item.id_anexo2 || item.procedure_mp == 0) ? `
    ${item.id_anexo ? `<a href="dropzone/mp/${item.id_anexo}" download><i class="fa fa-paperclip"></i></a>` : ""}
    ${item.id_anexo2 ? `${id_anexoHtml}` : ""}
${item.procedure_mp == 0 ? `<a href="register-equipament-check-list-copy-print?id=${item.equipamento_id}" target="_blank"><i class="fa fa-file"></i></a>` : ""}
  ` : "",
  
  
  `    
  <a class="btn btn-app"  href="maintenance-preventive-close-edit-case?id=${item.maintenance_preventive}" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>

  <a class="btn btn-app"  href="maintenance-preventive-viwer-digital-print?id=${item.maintenance_preventive}" target="_blank"  onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar procediemnto',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-file-pdf-o"></i> Visualizar 
                  </a>
                   <a class="btn btn-app"  href="maintenance-preventive-close-edit-tag?id=${item.maintenance_preventive}" target="_blank"   onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Etiqueta',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-qrcode"></i> Etiqueta
                  </a>
                   
                  <a class="btn btn-app"  href="maintenance-preventive-viwer-print?id=${item.maintenance_preventive}" target="_blank"  onclick="new PNotify({
                               title: 'Imprimir',
                               text: 'Abrindo Impressão',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-print"></i> Imprimir
                 </a>
                 <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/maintenance-preventive-trash-backend?id=${item.maintenance_preventive}';
  }
})
">
                <i class="fa fa-trash"></i> Excluir</a>
                                                      <a class="btn btn-app" onclick="
                                                        Swal.fire({
                                                          title: 'Tem certeza?',
                                                          text: 'Você não será capaz de reverter isso!',
                                                          icon: 'warning',
                                                          showCancelButton: true,
                                                          confirmButtonColor: '#3085d6',
                                                          cancelButtonColor: '#d33',
                                                          confirmButtonText: 'Sim, Enviar Manutenção Preventiva!'
                                                        }).then((result) => {
                                                          if (result.isConfirmed) {
                                                            Swal.fire(
                                                              'Enviando!',
                                                              'Seu arquivo será enviado.',
                                                              'success'
                                                            ),
                                                            window.location = 'api-assessment-manufacture-send-mp-user?id=${item.maintenance_preventive} ';
                                                          }
                                                        })
                                                      ">
                                                        <i class="fa fa-at"></i> Email
                                                      </a>
`

              ];
              });

    // Adiciona as novas linhas ao DataTables e desenha a tabela
    $('#datatable').DataTable().rows.add(novosDados).draw();

 
// Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([1,2,3,4]).every(function (d) {
        var column = this;
      var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
    });
});


</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#btn-carregar').click(function() {
    $.ajax({
      url: 'loading/load_equipamento.php', // substitua pelo nome do seu arquivo PHP que busca os itens restantes da tabela
      type: 'POST', // ou 'GET', dependendo da sua implementação
      data: {
        offset: 10 // substitua pelo número de itens que você já carregou
      },
      success: function(data) {
        $('#datatable').append(data); // adicione os novos itens à tabela
      }
    });
  });
});

</script>
