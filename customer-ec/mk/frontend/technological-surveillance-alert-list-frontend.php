<?php
include("database/database.php");
$query = "SELECT alert.resum,alert.identification,alert.problem,alert.action,alert.history,alert.recomentation,alert.anexo,equipamento_grupo.nome,equipamento_familia.modelo,equipamento_familia.fabricante,alert.id, equipamento_familia.nome,alert.alert,alert.date_alert,alert.obs,alert.upgrade,alert.reg_date,alert.titulo,alert.planing FROM alert  LEFT JOIN equipamento_familia ON equipamento_familia.id = alert.equipamento_familia LEFT JOIN equipamento_grupo ON equipamento_grupo.id = alert.equipamento_grupo order by alert.id DESC  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($resum,$identification,$problem,$action,$history,$recomentation,$anexo,$equipamento_grupo,$modelo,$fabricante,$id, $equipamento_familia,$alert,$date_alert,$obs,$upgrade,$reg_date,$yr,$planing);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                           <th>Registro</th>
                          <th>Grupo</th>
                          <th>Familia</th>
                          <th>Fabricante</th>
                          <th>Modelo</th>
                          <th>Alerta</th>
                          <th>Titulo</th>
                          <th>Data</th>
                          <th>Resumo</th>
                          
                                    <th>Anexos</th>
                          <th>Atualização</th>
                          
                          <th>Cadastro</th>
                          <th>Plano de Ação</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td>  </td>
                           <td><?php printf($id); ?> </td>
                          <td><?php printf($equipamento_grupo); ?> </td>
                          <td><?php printf($equipamento_familia); ?> </td>
                          <td><?php printf($fabricante); ?> </td>
                          <td><?php printf($modelo); ?> </td>
                          <td><?php printf($alert); ?></td>
                            <td><?php printf($yr); ?></td>
                            <td><?php printf($date_alert); ?></td>
                            <td><?php printf($resum); ?> <br></td>
                                  
                                    <td>  <a  href="<?php echo $anexo ?>" target="_blank">Carta ao Cliente </a> </td>
                          <td><?php printf($upgrade); ?></td>
                            <td><?php printf($reg_date); ?></td>
                            <td><?php if($planing == "0") {  printf("Sim");}else{printf("Não");} ?></td>

                          <td>
                   <a class="btn btn-app"  href="technological-surveillance-edit?id=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>

          
                  <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/alert-trash.php?id=<?php printf($id); ?>';
  }
})
">
                     <i class="glyphicon glyphicon-trash"></i> Excluir
                   </a>
                   <a  class="btn btn-app" href="technological-surveillance-alert-viewer?id=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Analise!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
