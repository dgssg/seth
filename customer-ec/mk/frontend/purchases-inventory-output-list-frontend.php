<?php
include("database/database.php");
$query="SELECT colaborador.primeironome,colaborador.ultimonome,instituicao.instituicao,instituicao_area.nome,instituicao_localizacao.nome, purchases_out.id,purchases_service.nome,purchases_out.number_mp,purchases_out.id_setor,purchases_status.status,purchases_out.data_purchases,purchases_out.id_colaborador,purchases_out.upgrade,purchases_out.reg_date FROM purchases_out LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = purchases_out.id_setor LEFT JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  LEFT JOIN colaborador ON colaborador.id =purchases_out.id_colaborador LEFT JOIN purchases_status ON purchases_status.id = purchases_out.id_purchases_status LEFT JOIN purchases_service ON purchases_service.id = purchases_out.service WHERE  purchases_out.id > 0";
$row=1;


if($instituicao==!""){
    $query = " $query and  instituicao.id = '$instituicao'";

}
if($setor==!""){
    $query = " $query and  instituicao_area.id = '$setor'";

}
if($area==!""){
    $query = " $query and  instituicao_localizacao.id = '$area'";

}
if($mc==!""){
    $query = " $query and  purchases_out.service = 1";

}
if($mp==!""){
    $query = " $query and  purchases_out.service = 2";

}


if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result( $primeironome,$ultimonome,$instituicao,$setor,$area,$id,$service,$number,$id_setor,$id_purchases_status,$data_purchases,$id_colaborador,$upgrade,$reg_date);

?>
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Manutenção</th>
                  <th>Nº</th>
                  <th>Localização</th>
                  <th>Status</th>
                  <th>Data</th>
                  <th>Colaborador</th>
                  <th>Ação</th>


                 </tr>
               </thead>
               <tbody>
                      <?php  while ($stmt->fetch()) { ?>

                 <tr>
                   <th scope="row"><?php printf($row); ?></th>
                   <td><?php printf($service); ?></td>
                   <td><?php printf($number); ?></td>
                   <td><?php printf($instituicao); ?> - <?php printf($setor); ?> - <?php printf($area); ?></td>
                   <td><?php printf($id_purchases_status); ?></td>
                   <td><?php printf($data_purchases); ?></td>
                   <td><?php printf($primeironome); ?> - <?php printf($ultimonome); ?> </td>

                     <td>
                       <a class="btn btn-app"  href="purchases-inventory-output-edit?id=<?php printf($id); ?>"onclick="new PNotify({
      																title: 'Atualização',
      																text: 'Atualização de Alerta!',
      																type: 'danger',
      																styling: 'bootstrap3'
      														});">
                          <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                        </a>
                          <a  class="btn btn-app" href="purchases-inventory-output-viewer?id=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
      																title: 'Visualizar',
      																text: 'Visualizar Alerta!',
      																type: 'info',
      																styling: 'bootstrap3'
      														});" >
                          <i class="fa fa-file-pdf-o"></i> Visualizar
                        </a>
                        <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/purchases-inventory-output-edit-trash.php?id=<?php printf($id); ?>';
  }
})
">
                           <i class="glyphicon glyphicon-trash"></i> Excluir
                         </a>
                     </td>
                 </tr>
            <?php  $row=$row+1; }
}   ?>
               </tbody>
             </table>







        <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal">
             <div class="modal-dialog modal-lg">
               <div class="modal-content">

                 <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Itens</h4>
                   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                   </button>
                 </div>
                 <div class="modal-body">
                    <form action="backend/purchases-inventory-output-backend.php" method="post">
                      <div class="ln_solid"></div>

                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Manutenção <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-left" name="purchases_service" id="purchases_service" required="required" placeholder="Manutenção">
                        <option value="">Selecione uma Manutenção</option>
                            <?php



                            $result_cat_post  = "SELECT  id, nome FROM purchases_service";

                            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                            while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                              echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                            }
                            ?>

                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Manutenção "></span>

                        </div>
                      </div>
                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>

                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Nº <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-right" name="number" id="number"  placeholder="Nº">
                            <option value="">Selecione uma M.P</option>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Nº "></span>


                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-left" name="instituicao2" id="instituicao2" required="required" placeholder="Unidade">
                      <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                            <?php



                            $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                            while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                              echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
                            }
                            ?>

                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                        </div>
                      </div>

                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>


                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-right" name="area2" id="area2"  placeholder="Area">
                              <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	  
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


                        </div>
                      </div>

                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>

                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-right" name="setor" id="setor2"  placeholder="Area">
                            <option value="">Selecione uma Opção</option>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


                        </div>
                      </div>

                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>


                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Status <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-left" name="id_purchases_status" id="id_purchases_status" required="required" placeholder="Status">
                        <option value="">Selecione uma Status</option>
                            <?php



                            $result_cat_post  = "SELECT  id, status FROM purchases_status";

                            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                            while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                              echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['status'].'</option>';
                            }
                            ?>

                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Status "></span>

                        </div>
                      </div>
                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_purchases" class="form-control" type="date" name="data_purchases"   >
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Colaborador <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-left" name="id_colaborador" id="id_colaborador" required="required" placeholder="Colaborador">
                        <option value="">Selecione uma Colaborador</option>
                            <?php



                            $result_cat_post  = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1";

                            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                            while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                              echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['primeironome'].' - '.$row_cat_post['ultimonome'].'</option>';
                            }
                            ?>

                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Colaborador "></span>

                        </div>
                      </div>

                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>








                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                   <button type="submit" class="btn btn-primary" onclick="new PNotify({
                             title: 'Registrado',
                             text: 'Informações registrada!',
                             type: 'success',
                             styling: 'bootstrap3'
                         });" >Salvar Informações</button>
                 </div>

               </div>
             </div>
           </div>
       </form>

       <!-- -->
       <script type="text/javascript">
       $(document).ready(function(){
         $('#instituicao2').change(function(){
           $('#area2').load('sub_categorias_post.php?instituicao='+$('#instituicao2').val());
         });
       });
       </script>
       <script type="text/javascript">
       $(document).ready(function(){
         $('#area2').change(function(){
           $('#setor2').load('sub_categorias_post_setor.php?area='+$('#area2').val());
         });
       });
       </script>
       <script type="text/javascript">
       $(document).ready(function(){
         $('#purchases_service').change(function(){
           $('#number').select2().load('sub_categorias_post_purchases.php?purchases_service='+$('#purchases_service').val());
         });
       });
       </script>
