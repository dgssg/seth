<?php
include("database/database.php");
 
$query = "SELECT equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome, calibration_preventive.date_start,calibration_preventive.id,calibration_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN calibration_preventive ON calibration_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE calibration_preventive.id_status like '2'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($serie,$fabricante,$instituicao,$area,$setor,$programada,$id,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);



?>


<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>Rotina</th>
                         <th></th>
                          <th>Periodicidade</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>
                          <th>Programada</th>
                            <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                            <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                            <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  
                          <th>Atualização</th>
                          <th>Cadastro</th>

                        </tr>
                      </thead>


                      <tbody>
                      <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td ><?php printf($rotina); ?> </td>
                          <td >
                          <?php
                            switch($periodicidade){
    case 1:


    $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));

    if($today > $new_date){

        printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
    }



        break;
        case 5:


          $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
      
          if($today > $new_date){
      
              printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
          }
      
      
      
              break;
    case 7:


   $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
   if($today > $new_date){

       printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
   }



        break;
    case 14:


    $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
    if($today > $new_date){

        printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
    }



        break;
    case 21:


    $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
    if($today > $new_date){

        printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
    }



        break;
    case 28:


    $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
    if($today > $new_date){

        printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
    }


        break;
    case 30:



   $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
   if($today > $new_date){

       printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
   }

        break;
    case 60:


    $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
    if($today > $new_date){

        printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
    }


        break;
    case 90:


    $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
    if($today > $new_date){

        printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
    }


        break;
    case 120:


    $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
    if($today > $new_date){

        printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
    }



        break;
    case 180:
          $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
          if($today > $new_date){

              printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
          }




        break;
    case 365:


   $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));

   if($today > $new_date){

       printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
   }

        break;
    case 730:



   $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));


   if($today > $new_date){

       printf("<i class='fa fa-clock-o' aria-hidden='true'></i>");
   }
        break;
}

	$result_transacoes = "SELECT COUNT(id_routine) AS 'duplicidade',  id_routine FROM calibration_preventive WHERE id_status like '1' AND id_routine = id_routine GROUP by id_routine";
	if ($stmtquery = $conn_cal->prepare($result_transacoes)) {
		$stmtquery->execute();
     $stmtquery->bind_result($duplicidade,$id_routine_query);
     while ($stmtquery->fetch()) {

  if($id_routine_query==$rotina){
       if($duplicidade>1){
        printf("<br><i class='fa fa-files-o' aria-hidden='true'></i>");
       }
  }
	}
	}
	// for($i = 0; $i <= $result; $i++){ if($id_unidade[$i]==$rotina){printf("<br><i class='fa fa-files-o' aria-hidden='true'></i>");}}
        ?>

                          </td>
                          <td><?php if($periodicidade==5){printf("Diaria [1 Dias Uteis ]");}if($periodicidade==365){printf("Anual [365 Dias]");}if($periodicidade==180){printf("Semestral [180 Dias]");} if($periodicidade==30){printf("Mensal [30 Dias]");}if($periodicidade==1){printf("Diaria [1 Dias]");} if($periodicidade==7){printf("Semanal [7 Dias]");}if($periodicidade==14){printf("Bisemanal [14 Dias]");}if($periodicidade==21){printf("Trisemanal [21 Dias]");} if($periodicidade==28){printf("Quadrisemanal [28 Dias]");} if($periodicidade==60){printf("Bimestral [60 Dias]");}if($periodicidade==90){printf("Trimestral [90 Dias]");}if($periodicidade==120){printf("Quadrimestral [120 Dias]");}if($periodicidade==730){printf("Bianual [730 Dias]");}
                            if($periodicidade==1460){printf("Quadrianual [1460 Dias]");}if($periodicidade==1095){printf("Trianual [1095 Dias]");} ?></td>

                            <td><?php printf($nome); ?></td>
                              <td><?php printf($modelo); ?></td>
                            <td><?php printf($fabricante); ?></td>
                            <td><?php printf($codigo); ?></td>
                            <td><?php printf($serie); ?></td>
                              <td><?php printf($programada); ?></td>
                              <td><?php printf($instituicao); ?></td>
                                 <td><?php printf($area); ?></td>
                                 <td>  <?php printf($setor); ?></td>
                                <td><?php printf($upgrade); ?></td>

                          <td><?php printf($reg_date); ?></td>

                          <td>

                  <a class="btn btn-app"  href="flow-calibration-viwer?routine=<?php printf($rotina); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                   <a class="btn btn-app"  href="flow-calibration-shut-down?routine=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Fechar',
																text: 'Fechar MP',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-lock"></i> Fechar MP
                  </a>
                   <a class="btn btn-app"  href="flow-calibration-open-tag?id=<?php printf($rotina); ?>" target="_blank"   onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Etiqueta',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-qrcode"></i> Etiqueta
                  </a>
                   <a class="btn btn-app"  href="flow-calibration-ban?routine=<?php printf($id); ?>"    onclick="new PNotify({
																title: 'Execução',
																text: 'Não Execução',
																type: 'warning',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-ban"></i> Não execução
                  </a>
                  <a class="btn btn-app"  href="flow-calibration-times?routine=<?php printf($id); ?>"    onclick="new PNotify({
																title: 'Cancelar',
																text: 'Cancelar MP',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-times"></i> Cancelar
                  </a>
                   <a class="btn btn-app"  href="flow-calibration-viwer-print?routine=<?php printf($rotina); ?>" target="_blank"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
                  <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/flow-calibration-trash-backend?id=<?php printf($id); ?>';
  }
})
">
                 <i class="fa fa-trash"></i> Excluir</a>
                 <a class="btn btn-app"  href="flow-calibration-preventive-label?id=<?php printf($id); ?>" target="_blank" onclick="new PNotify({
                              title: 'Etiqueta',
                              text: 'Abrir Etiqueta',
                              type: 'sucess',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-fax"></i> Rótulo
                </a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
