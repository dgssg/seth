<?php
$hfmea = ($_GET["hfmea"]);
$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query="SELECT * FROM documentation_hfmea_itens WHERE id like '$codigoget' ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$id_documentation_hfmea,$risco,$causa,$efeito,$preven,$controle,$sentinela,$probabilidade,$gravidade,$abrangen,$upgrade,$reg_date,$situacao,$total,$grau,$conti);
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
?>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <div class="right_col" role="main">

        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Edição</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                <div class="input-group">

                  <span class="input-group-btn">

                  </span>
                </div>
              </div>
            </div>
          </div>


       <div class="x_panel">
              <div class="x_title">
                <h2>Alerta</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

           <div class="alert alert-success alert-dismissible " role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Area de Edição!</strong> Todo alteração não é irreversível.
                </div>



              </div>
            </div>



           <!-- page content -->









            <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edição <small>Modo de Falha</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a class="dropdown-item" href="#">Settings 1</a>
                        </li>
                        <li><a class="dropdown-item" href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form action="backend/documentation-hfmea-itens-edit-upgrade-backend.php" method="post">

                  <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                      </div>
                    </div>

                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="hfmea" class="form-control" name="hfmea" type="hidden" value="<?php printf($hfmea); ?> " >
                      </div>
                    </div>


                      <label for="risco">Risco:</label>
                          <textarea id="risco" class="form-control" name="risco" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"placeholder="<?php printf($risco); ?>"value="<?php printf($risco); ?>"><?php printf($risco); ?></textarea>



                             <label for="causa">Causa:</label>
                          <textarea id="causa" class="form-control" name="causa" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"placeholder="<?php printf($causa); ?>"value="<?php printf($causa); ?>"><?php printf($causa); ?></textarea>

                            

                             <label for="efeito">Consequencia:</label>
                          <textarea id="efeito" class="form-control" name="efeito" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"placeholder="<?php printf($efeito); ?>"value="<?php printf($efeito); ?>"><?php printf($efeito); ?></textarea>


                            <label for="preven">Prevencao:</label>
                          <textarea id="preven" class="form-control" name="preven" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"placeholder="<?php printf($preven); ?>"value="<?php printf($preven); ?>"><?php printf($preven); ?></textarea>


                            <label for="controle">Controle:</label>
                          <textarea id="controle" class="form-control" name="controle" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"placeholder="<?php printf($controle); ?>"value="<?php printf($controle); ?>"><?php printf($controle); ?></textarea>
                            
                            <div class="ln_solid"></div>  
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Sentinela</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="sentinela" class="form-control" type="text" name="sentinela"  placeholder="<?php printf($sentinela); ?>"value="<?php printf($sentinela); ?>" >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Probabilidade</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="probabilidade" class="form-control" type="text" name="probabilidade" placeholder="<?php printf($probabilidade); ?>"value="<?php printf($probabilidade); ?>"  >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Gravidade</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="gravidade" class="form-control" type="text" name="gravidade" placeholder="<?php printf($gravidade); ?>"value="<?php printf($gravidade); ?>"  >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Abrangencia</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="abrangen" class="form-control" type="text" name="abrangen"  placeholder="<?php printf($abrangen); ?>"value="<?php printf($abrangen); ?>" >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Situacao Total</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="situacao" class="form-control" type="text" name="situacao" placeholder="<?php printf($situacao); ?>"value="<?php printf($situacao); ?>"  >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Total</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="total" class="form-control" type="text" name="total" placeholder="<?php printf($total); ?>"value="<?php printf($total); ?>"  >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grau de improtancia</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="gau" class="form-control" type="text" name="grau"  placeholder="<?php printf($grau); ?>"value="<?php printf($grau); ?>" >
                               </div>
                             </div>
                            
                             <div class="ln_solid"></div>

                            <label for="conti">Contingência:</label>
                          <textarea id="conti" class="form-control" name="conti" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($conti); ?>"value="<?php printf($conti); ?>" ><?php printf($conti); ?></textarea>
                            
                             





                    <div class="ln_solid"></div>


                 
<!--  
                    <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 1</label>
              <div class="col-md-6 col-sm-6 ">

                  <label>
                   <input name="question_one"id="question_one"type="checkbox" class="js-switch" /> Sim
                 </label>

              </div>
            </div>
            <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 2</label>
             <div class="col-md-6 col-sm-6 ">

                 <label>
                     <input name="question_two"id="question_two" type="checkbox" class="js-switch" /> Sim
                   </label>

             </div>
           </div>
           <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 3</label>
            <div class="col-md-6 col-sm-6 ">

                <label>
                    <input name="question_three"id="question_three"type="checkbox" class="js-switch" /> Sim
                  </label>

            </div>
          </div>
          <div class="item form-group">
           <label class="col-form-label col-md-3 col-sm-3 label-align">Pergunta 4</label>
           <div class="col-md-6 col-sm-6 ">

               <label>
                   <input name="question_four"id="question_four"type="checkbox" class="js-switch" /> Sim
                 </label>

           </div>
         </div>
         -->

                      <div class="ln_solid"></div>
                         <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input  class="form-control" type="text"   value="<?php printf($upgrade); ?> " readonly="readonly">
                      </div>
                    </div>

                     <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input  class="form-control" type="text"   value="<?php printf($reg_date); ?> " readonly="readonly">
                      </div>
                    </div>





                              <div class="form-group row">
                     <label class="col-form-label col-md-3 col-sm-3 "></label>
                      <div class="col-md-3 col-sm-3 ">
                          <center>
                         <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});">Salvar Informações</button>
                       </center>
                      </div>
                    </div>


                                </form>






                </div>
              </div>
            </div>
          </div>



           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="documentation-hfmea-edit?id=<?php printf($hfmea);?>">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>




              </div>
            </div>




      </div>
          </div>
      <!-- /page content -->

  <!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
