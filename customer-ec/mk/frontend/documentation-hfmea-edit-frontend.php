<?php
include("database/database.php");
$codigoget = ($_GET["id"]);
$query = "SELECT documentation_hfmea.id_unidade,documentation_hfmea.id_setor,documentation_hfmea.id_colaborador,colaborador.primeironome,colaborador.ultimonome,instituicao_area.nome,documentation_hfmea.class, equipamento_grupo.nome, documentation_hfmea.id, documentation_hfmea.id_equipamento_grupo, documentation_hfmea.titulo, documentation_hfmea.ver, documentation_hfmea.file, documentation_hfmea.data_now, documentation_hfmea.data_before, documentation_hfmea.upgrade, documentation_hfmea.reg_date FROM documentation_hfmea  LEFT JOIN equipamento_grupo ON equipamento_grupo.id = documentation_hfmea.id_equipamento_grupo LEFT JOIN instituicao_area ON instituicao_area.id = documentation_hfmea.id_setor LEFT JOIN colaborador ON colaborador.id = documentation_hfmea.id_colaborador WHERE documentation_hfmea.id = $codigoget";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id_unidade,$id_setor,$id_colaborador,$primeironome,$ultimonome,$instituicao_area,$class,$nome,$id, $id_equipamento_grupo,$titulo,$ver,$file,$data_now,$data_before,$upgrade,$reg_date);


   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
  
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }

  
function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
  $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
  $con = 1;
  $var = '';
  while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
  }else{
      $var = $var.$n.$res[$n];
      $con++;
      }
  }
      $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
 // echo chaveAlfaNumerica(5);
//   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = md5($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 3
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function v4() {
return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

// 32 bits for "time_low"
mt_rand(0, 0xffff), mt_rand(0, 0xffff),

// 16 bits for "time_mid"
mt_rand(0, 0xffff),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand(0, 0x0fff) | 0x4000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand(0, 0x3fff) | 0x8000,

// 48 bits for "node"
mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
);
}

public static function v5($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = sha1($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function is_valid($uuid) {
return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();

//echo $v4uuid; //chave da assinatura
?>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>HFMEA</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>HFMEA</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                 <form action="backend/documentation-hfmea-edit-backend.php?hfmea=<?php printf($codigoget);?>" method="post">

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="hfmea" class="form-control" name="hfmea" type="hidden" value="<?php printf($codigoget); ?> " >
                        </div>
                      </div>

                      <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Grupo <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="id_equipamento_grupo" id="id_equipamento_grupo"  placeholder="Area">
        <option value="">Selecione um Grupo</option>
        <?php



$sql = "SELECT  id, nome FROM equipamento_grupo WHERE trash = 1";


if ($stmt = $conn->prepare($sql)) {
$stmt->execute();
$stmt->bind_result($id,$nome);
while ($stmt->fetch()) {
?>

<option value="<?php printf($id);?>	" <?php if($id == $id_equipamento_grupo){printf("selected");};?>><?php printf($nome);?> 	</option>

 <?php
// tira o resultado da busca da memória
}

                     }
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_equipamento_grupo').select2();
  });
  </script>
                   <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"  id="titulo" name="titulo"  value="<?php printf($titulo); ?>" >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Versão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="ver" class="form-control" type="text" name="ver"  value="<?php printf($ver); ?>" >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Classificação</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="class" class="form-control" type="text" name="class"  value="<?php printf($class); ?>" >
                        </div>
                      </div>
                      <div class="form-group row">
    <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Empresa <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="id_unidade" id="id_unidade"  placeholder="Setor">
      <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
        <?php



$sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";


if ($stmt = $conn->prepare($sql)) {
$stmt->execute();
$stmt->bind_result($id,$instituicao);
while ($stmt->fetch()) {
?>

<option value="<?php printf($id);?>	" <?php if($id == $id_unidade){printf("selected");} ?> > <?php printf($instituicao);?>	</option>
 <?php
// tira o resultado da busca da memória
}

                     }
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Unidade "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_unidade').select2();
  });
  </script>
                      <div class="form-group row">
      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Cliente <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Setor">
          <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	  
        <?php



$sql = "SELECT  id, codigo,nome FROM instituicao_area WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
  $stmt->bind_result($id,$codigo,$nome);
  while ($stmt->fetch()) {
    ?>
    <option value="<?php printf($id);?>	" <?php if($id == $id_setor){printf("selected");};?>><?php printf($codigo);?> <?php printf($nome);?>	</option>
    <?php
    // tira o resultado da busca da mem��ria
  }

}
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#area').select2();
  });
  </script>
<div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Responsável 
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Responsável" >
        <option value="	">Selecione o Responsável</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	" <?php if($id == $id_colaborador){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_colaborador').select2();
  });
  </script>
  
  

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_now"  type="date" name="data_now"  value="<?php printf($data_now); ?>" class="form-control" >
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da Proxima Revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_before"  type="date" name="data_before"  value="<?php printf($data_before); ?>" class="form-control" >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
                                     title: 'Registrado',
                                     text: 'Informações registrada!',
                                     type: 'success',
                                     styling: 'bootstrap3'
                                 });">Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                                  </form>



                  </div>
                </div>
              </div>
	       </div>


            <div class="clearfix"></div>



          <div class="x_panel">
                <div class="x_title">
                  <h2>Modos de falhas</h2>
                  <ul class="nav navbar-right panel_toolbox">
                       <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo-->
       <?php
$query="SELECT * FROM documentation_hfmea_itens WHERE id_documentation_hfmea like '$codigoget' order by id ASC  ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$id_documentation_hfmea,$risco,$causa,$efeito,$preven,$controle,$sentinela,$probabilidade,$gravidade,$abrangen,$upgrade,$reg_date,$situacao,$total,$grau,$conti);

?>
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Risco</th>
                          <th>Causa</th>
                          <th>Consequencia</th>
                          <th>Prevencao</th>
                          <th>Controle</th>
                          <th>Sentinela</th>
                          <th>Probabilidade</th>
                          <th>Gravidade</th>
                          <th>Abrangencia</th>
                          <th>Situacao Atual</th>
                          <th>Total</th>
                          <th>Grau de improtancia</th>
                          <th>Contingência</th>
                    
                          <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($risco); ?></td>
                          <td><?php printf($causa); ?></td>
                          <td><?php printf($efeito); ?></td>
                          <td><?php printf($preven); ?></td>
                          <td><?php printf($controle); ?></td>
                          <td><?php printf($sentinela); ?></td>
                          <td><?php printf($probabilidade); ?></td>
                          <td><?php printf($gravidade); ?></td>
                          <td><?php printf($abrangen); ?></td>
                          <td><?php printf($situacao); ?></td>
                          <td><?php printf($total); ?></td>
                          <td><?php printf($grau); ?></td>
                          <td><?php printf($conti); ?></td>
                          <td>
                            <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/documentation-hfmea-itens-trash.php?id=<?php printf($id); ?>&hfmea=<?php printf($codigoget); ?>';
  }
})
">
                               <i class="glyphicon glyphicon-trash"></i> Excluir
                             </a>
                             <a class="btn btn-app"  href="documentation-hfmea-itens-edit?id=<?php printf($id); ?>&hfmea=<?php printf($codigoget); ?>"onclick="new PNotify({
                                            title: 'Alteração',
                                            text: 'Alteração!',
                                            type: 'danger',
                                            styling: 'bootstrap3'
                                        });">
                                <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                              </a>
                          </td>
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>





                </div>
              </div>


               <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Itens</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                           <form action="backend/documentation-hfmea-insert-itens-backend.php?hfmea=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>

                             <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="hfmea" class="form-control" name="hfmea" type="hidden" value="<?php printf($codigoget); ?> " >
                        </div>
                      </div>

                             <label for="risco">Risco:</label>
                          <textarea id="risco" class="form-control" name="risco" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"></textarea>



                             <label for="causa">Causa:</label>
                          <textarea id="causa" class="form-control" name="causa" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"></textarea>

                            

                             <label for="efeito">Consequencia:</label>
                          <textarea id="efeito" class="form-control" name="efeito" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"></textarea>


                            <label for="preven">Prevencao:</label>
                          <textarea id="preven" class="form-control" name="preven" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"></textarea>


                            <label for="controle">Controle:</label>
                          <textarea id="controle" class="form-control" name="controle" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"></textarea>
                            
                            
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Sentinela</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="sentinela" class="form-control" type="text" name="sentinela"   >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Probabilidade</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="probabilidade" class="form-control" type="text" name="probabilidade"   >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Gravidade</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="gravidade" class="form-control" type="text" name="gravidade"   >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Abrangencia</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="abrangen" class="form-control" type="text" name="abrangen"   >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Situacao Total</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="situacao" class="form-control" type="text" name="situacao"   >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Total</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="total" class="form-control" type="text" name="total"   >
                               </div>
                             </div>
                            
                             <div class="item form-group">
                               <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grau de improtancia</label>
                               <div class="col-md-6 col-sm-6 ">
                                 <input id="gau" class="form-control" type="text" name="grau"   >
                               </div>
                             </div>
                            


                            <label for="conti">Contingência:</label>
                          <textarea id="conti" class="form-control" name="conti" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"></textarea>
                            
                             







                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary" onclick="new PNotify({
                                    title: 'Registrado',
                                    text: 'Informações registrada!',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>

              <!-- -->


              <!-- Posicionamento -->


              <div class="clearfix"></div>



<div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Assinatura <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="backend/documentation-hfmea-signature-backend.php?id=<?php printf($codigoget);?>" method="post">
          <div class="item form-group">
                        <label for="middle-name"
                            class="col-form-label col-md-3 col-sm-3 label-align">Chave da Assinatura</label>
                        <div class="col-md-6 col-sm-6 ">
                            <input class="form-control" type="text" value="<?php printf($signature_alert); ?> "
                                readonly="readonly">
                        </div>
                    </div>

<!-- Large modal --> <center>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Assinatura</button>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Assinatura</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <h4>Assinatura digital</h4>
                <p>Para Assinar digite o texto abaixo.</p>
               <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
                <p> <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> <span ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="assinature" name="assinature"   class="form-control " class="docs-tooltip" data-toggle="tooltip" title=" ">
                
              </div>
              <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >
              </div>
            </div>
            <script>
var chave = "<?php echo $chave; ?>"; // Obtém a chave do PHP e armazena na variável JavaScript
var assinaturaInput = document.getElementById("assinature");

assinaturaInput.addEventListener("blur", function () {
var assinatura = this.value;

if (assinatura !== chave) {
Swal.fire('A assinatura está incorreta!');
  // Aqui você pode realizar a ação desejada caso a assinatura seja divergente, como exibir uma mensagem de erro ou realizar algum redirecionamento.
} else {

  // Aqui você pode realizar a ação desejada caso a assinatura seja correta.
}
});
</script>
            </div></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Assinar</button>
              </div>

            </div>
          </div>
        </div>
       </center>
       
          </form>
        </div>
      </div>
    </div>
  </div>


  <div class="clearfix"></div>
  <div class="clearfix"></div>


<div class="x_panel">
    <div class="x_title">
      <h2>Assinatura</h2>
      <ul class="nav navbar-right panel_toolbox">
         
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      
    <!--arquivo -->
<?php    
$query="SELECT regdate_hfmea_signature.id, regdate_hfmea_signature.id_hfmea,regdate_hfmea_signature.id_user,regdate_hfmea_signature.id_signature,regdate_hfmea_signature.reg_date,usuario.nome FROM regdate_hfmea_signature INNER JOIN usuario ON regdate_hfmea_signature.id_user = usuario.id   WHERE regdate_hfmea_signature.id_hfmea like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$id_mp,$id_user,$id_signature,$reg_date,$id_user);

?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              
              <th>Usuário</th>
              <th>Assinatura</th>
               <th>Registro</th>
                <th>Ação</th>
            
            </tr>
          </thead>
          <tbody>
                 <?php  while ($stmt->fetch()) { ?>
                 
            <tr>
              <th scope="row"><?php printf($row); ?></th>
              <td><?php printf($id_user); ?></td>
              <td><?php printf($id_signature); ?></td>
              <td><?php printf($reg_date); ?></td>
              <td>                                                                <a class="btn btn-app" onclick="
                Swal.fire({
                  title: 'Tem certeza?',
                  text: 'Você não será capaz de reverter isso!',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Sim, Deletar!'
                }).then((result) => {
                  if (result.isConfirmed) {
                    Swal.fire(
                      'Deletando!',
                      'Seu arquivo será excluído.',
                      'success'
                    ),
                    window.location = 'backend/signature-hfmea-trash-backend.php?id=<?php printf($id); ?>&page=<?php printf($codigoget); ?>';
                  }
                })
              ">
                <i class="fa fa-trash"></i> Excluir
              </a>
</td>
             
              
            </tr>
       <?php  $row=$row+1; }
}   ?>
          </tbody>
        </table>

    
  
    
    </div>
  </div>  
    <!-- compose -->


<div class="clearfix"></div>
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="documentation-hfmea">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>

              <script type="text/javascript">
    $(document).ready(function(){
      $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );

        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });

    

    </script>