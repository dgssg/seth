<?php
  //============================================================+
  // File name   : example_001.php
  // Begin       : 2008-03-04
  // Last Update : 2013-05-14
  //
  // Description : Example 001 for TCPDF class
  //               Default Header and Footer
  //
  // Author: Nicola Asuni
  //
  // (c) Copyright:
  //               Nicola Asuni
  //               Tecnick.com LTD
  //               www.tecnick.com
  //               info@tecnick.com
  //============================================================+
  
  /**
  * Creates an example PDF TEST document using TCPDF
  * @package com.tecnick.tcpdf
  * @abstract TCPDF - Example: Default Header and Footer
  * @author Nicola Asuni
  * @since 2008-03-04
  */
  
  // Include the main TCPDF library (search for installation path).
  require_once('../../framework/TCPDF/tcpdf.php');
  
  // create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('MK Sistemas Biomedicos');
  $pdf->SetTitle('Sistema SETH');
  $pdf->SetSubject('Ordem de Serviço');
  $pdf->SetKeywords('Ordem de Serviço');
  
  // set default header data
  $pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH,'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
  $pdf->setFooterData(array(0,64,0), array(0,64,128));
  
  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  
  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  
  // set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  
  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  
  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
  }
  
  // ---------------------------------------------------------
  
  // set default font subsetting mode
  $pdf->setFontSubsetting(true);
  
  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage();
  
  // set text shadow effect
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
  
include("database/database.php");

$codigoget = ($_GET["routine"]);
$query = "SELECT id FROM maintenance_preventive WHERE id_routine = '$codigoget'  ORDER BY id DESC limit 1  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id_equipamento);

  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
    $codigoget =$id_equipamento;
  }
}

// Create connection
$mp=true;




$query = "SELECT maintenance_preventive.temp, maintenance_preventive.hum, maintenance_routine.digital,maintenance_routine.id_equipamento,maintenance_preventive.file,maintenance_preventive.id,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($temp,$hum,$digital,$id_equipamento,$id_anexo,$id,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);

  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}


$query = "SELECT instituicao.logo,instituicao_localizacao.nome,instituicao_area.nome,instituicao.instituicao,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento_familia.anvisa,equipamento_familia.fabricante,equipamento_familia.img,equipamento.obs,equipamento.data_instal,equipamento.data_fab,equipamento.patrimonio,equipamento.serie,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao ON instituicao.id = instituicao_localizacao.id_unidade INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area WHERE equipamento.id like '$id_equipamento'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($logo,$setor,$area,$instituicao,$data_val,$nf,$data_fab,$data_instal,$anvisa,$fab,$img,$obs,$data_fab,$data_fab,$patrimonio,$serie,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}

$query = "SELECT maintenance_preventive.digital,maintenance_preventive.procedure_mp,maintenance_preventive.id,maintenance_preventive.id_mp,maintenance_preventive.id_fornecedor,maintenance_preventive.date_end,maintenance_preventive.file,maintenance_preventive.time_mp,maintenance_preventive.obs_tc,maintenance_preventive.obs_mp,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($digital_mp,$procedure_mp,$id_maintenance_preventive,$id_tecnico,$id_fornecedor,$date_end,$id_anexo,$time_mp,$obs_tc,$obs_mp,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
    //  }
  }
}//referenciar o DomPDF com namespace

switch($procedure_mp){
    case 1 :
    
    $query = "SELECT maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_before WHERE maintenance_routine.id = '$rotina'";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
      while ($stmt->fetch()) {
        //printf("%s, %s\n", $solicitante, $equipamento);
      }
    } 
    break;

    case 2 :

        $query = "SELECT maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_after WHERE maintenance_routine.id = '$rotina'";
        if ($stmt = $conn->prepare($query)) {
          $stmt->execute();
          $stmt->bind_result($id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
          while ($stmt->fetch()) {
            //printf("%s, %s\n", $solicitante, $equipamento);
          }
        }
        break;
    };
    $query=" ";
    $row=1;
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($id,$id_maintenance_procedures,$class,$column,$procedures);
    }


	$html2 = '<table border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:100%;border-collapse:collapse;">';
	$html2 .= '<thead>';
	$html2 .= '<tr class="gridHeaderRelatorio">';
	$html2 .= '<th>#</th>';
	$html2 .= '<th>Procedimento</th>';


	$html2 .= '</tr>';
	$html2 .= '</thead>';
	$html2 .= '<tbody>';
		$result_transacoes2 = "SELECT maintenance_procedures_itens.id,maintenance_procedures_itens.id_maintenance_procedures,maintenance_procedures_itens.class,maintenance_procedures_itens.column,maintenance_procedures_itens.procedures FROM maintenance_procedures_itens WHERE maintenance_procedures_itens.id_maintenance_procedures like '$id_maintenance_procedures' order by maintenance_procedures_itens.position ASC";
	$resultado_trasacoes2 = mysqli_query($conn, $result_transacoes2);
	while($row_transacoes2 = mysqli_fetch_assoc($resultado_trasacoes2)){
    
		$html2 .= '<tr><td scope="row"><small> </small></td>';

        if($row_transacoes2['class']==1){
            $html2 .= '<td><center><small>'.$row_transacoes2['procedures'] . " </small></center></td></tr>";
        }
        if($row_transacoes2['class']==2){
            $html2 .= '<td><small>'.$row_transacoes2['procedures'] . " ";
            $html2 .= '<span class="right">';
            $html2 .= '  <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" size="5"> <small> OK</small>';
            $html2 .= '  <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" size="5"> <small> NOK</small>';
            $html2 .= '  <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" size="5"> <small> NA</small>';
            $html2 .= ' </small>';
            $html2 .= '  </span align="right">';
            $html2 .= '/td></tr>';
        }

        if($row_transacoes2['class']==3 ||$class==4 ){
            $html2 .= '<td><small>  <div  class="input-group input-group-sm  "> "';
            $html2 .= '   <label class="input-group-addon" for="dataScaleX"> '.$row_transacoes2['procedures'] .'</label>';
            $html2 .= '     <input type="text" class="form-control" id="dataScaleX"placeholder="" >';
            $html2 .= '   <label class="input-group-addon" for="dataScaleX"> '.$row_transacoes2['procedures'] .'</label>';
            $html2 .= '     <input type="text" class="form-control" id="dataScaleX"placeholder="" >';
            $html2 .= '   <label class="input-group-addon" for="dataScaleX"> '.$row_transacoes2['procedures'] .'</label>';
            $html2 .= '     <input type="text" class="form-control" id="dataScaleX"placeholder="" >';
            
            $html2 .= '  <label class="input-group-addon" for="dataScaleX">';
            $html2 .= '     <label> OK </label>';
            $html2 .= '     <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" size="5"';
            $html2 .= '     <label> OK </label>';
            $html2 .= '     <input align="right" type="radio" class="flat" name="iCheck2" disabled="disabled" size="5"';
            $html2 .= '     <label> OK </label>';
            $html2 .= '     <input align="right" type="radio" class="flat" name="iCheck3" disabled="disabled" size="5"   </label>';
            
            $html2 .= '  </div></small></td></tr>';
        }
	

    if($row_transacoes2['class']==5){
        $html2 .= '<td><small>'.$row_transacoes2['procedures'] . " </small></td></tr>";
    }
}

    $html2 .= '</tbody>';
	$html2 .= '</table>';



	$html3 = '<table border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:100%;border-collapse:collapse;">';
	$html3 .= '<thead>';
	$html3 .= '<tr class="gridHeaderRelatorio">';

	$html3 .= '<th>Execultante</th>';
    $html3 .= '<th>Data de inicio</th>';
    $html3 .= '<th>Data de Termino</th>';
    $html3 .= '<th>Tempo</th>';
	$html3 .= '</tr>';
	$html3 .= '</thead>';
	$html3 .= '<tbody>';

	$result_transacoes2 = "SELECT colaborador.ultimonome,regdate_mp_registro.id_mp, regdate_mp_registro.id_user,regdate_mp_registro.date_start,regdate_mp_registro.date_end,regdate_mp_registro.time,regdate_mp_registro.reg_date, regdate_mp_registro.upgrade, colaborador.id, colaborador.primeironome FROM regdate_mp_registro INNER JOIN colaborador on regdate_mp_registro.id_user = colaborador.id WHERE regdate_mp_registro.id_mp like '$id_maintenance_preventive'";
	$resultado_trasacoes2 = mysqli_query($conn, $result_transacoes2);
	while($row_transacoes2 = mysqli_fetch_assoc($resultado_trasacoes2)){

        $html3 .= '<td><small>'.$row_transacoes2['primeironome'] . " ";
        $html3 .= '<td><small>'.$row_transacoes2['date_start'] . " ";
        $html3 .= '<td><small>'.$row_transacoes2['date_end'] . " ";
        $html3 .= '<td><small>'.$row_transacoes2['time'] . " ";
        $html3 .= '  </div></small></td></tr>';
     



        }

        $html3 .= '</tbody>';
        $html3 .= '</table>';
  $html = <<<EOD
<div class="wraper">
 
<br>
<!-- Início do Cabeçalho -->
 
<div style="clear: both;"></div>
<hr style="border-top: dashed 1px; width: 900px; color: gray; clear: both;">
<!-- Fim do Cabeçalho -->
<!-- Início do Título -->
<br>
<center>
<h2 class="titulo-relatorio">ROTINA Nº $codigoget</h2>
</center>
<br>
<!-- Fim do Título -->
<main>
<div class="container">
  <section class="content-block">

    <header>
      <h4 class="content-block-title">Dados da Preventiva</h4>
    </header>
    <table class="table-documento">
      <tbody>
        <tr>
          <td >
          Programada:
            <span class="uppercase weight-bold"> $programada</span>
          </td>

          <td >
          Rotina:
            <span class="uppercase weight-bold"> $rotina</span>
          </td>

        

        


        </tr>
      </tbody>
    </table>
  </section>
  <!--./content-block-->

  <section class="content-block">
  <header>
    <h4 class="content-block-title">Dados do equipamento:</h4>
  </header>

   <table class="table-documento">
    <tbody>
      <tr>
        <td >
        Nome:
          <span class="uppercase weight-bold"> $nome</span>
        </td>

        <td >
        Modelo:
          <span class="uppercase weight-bold"> $modelo</span>
        </td>

        <td >
        N&ordm; Serie:
          <span class="uppercase weight-bold"> $serie</span>
        </td>

        <td>
          Patrimonio:
          <span class="uppercase weight-bold"> $patrimonio </span>
        </td>
        <td>
        Codigo:
        <span class="uppercase weight-bold"> $codigo </span>
      </td>

     
      </tr>






    </tbody>
  </table>
</section>
<section class="content-block">
<header>
  <h4 class="content-block-title">Localiza&ccedil;&atilde;o Atual:</h4>
</header>

 <table class="table-documento">
  <tbody>
    <tr>
      <td >
      Data instalação:
        <span class="uppercase weight-bold"> $data_instal</span>
      </td>

      <td >
      Instituição:
        <span class="uppercase weight-bold"> $instituicao</span>
      </td>

      <td >
        Setor:
        <span class="uppercase weight-bold"> $area</span>
      </td>

      <td>
        Area:
        <span class="uppercase weight-bold"> $setor </span>
      </td>

   
    </tr>






  </tbody>
</table>
</section>
<section class="content-block">
<header>
  <h4 class="content-block-title">Rotina do Equipamento:</h4>
</header>

 <table class="table-documento">
  <tbody>
    <tr>
      <td >
      Rotina:
        <span class="uppercase weight-bold"> $rotina</span>
      </td>

      <td >
      Periodicidade:
        <span class="uppercase weight-bold"> $periodicidade</span>
      </td>

      <td >
        Programada Inicial/Reprogramada:
        <span class="uppercase weight-bold"> $data_start</span>
      </td>

      <td>
      Inicial:
        <span class="uppercase weight-bold"> $reg_date </span>
      </td>
      <td>
      Atualização:
        <span class="uppercase weight-bold"> $upgrade </span>
      </td>
      <td>
      Procedimento $procedure_mp:
        <span class="uppercase weight-bold"> $procedimento_1 </span>
      </td>
      <td>
      Codigo:
        <span class="uppercase weight-bold"> $codigo_p </span>
      </td>

   
    </tr>






  </tbody>
</table>
</section>

          
<section class="content-block">
<header>
  <h4 class="content-block-title">Procedimento</h4>
</header>
  $html2

</section>

<section class="content-block">
<header>
  <h4 class="content-block-title">Dados do Ambiente:</h4>
</header>
<table class="table-documento lista-atividades">
  <thead>
    <tr>
      <th class="width-100-percent text-left">TEMPERATURA DO AR AMBIENTE (Temp °C), UMIDADE DO AR AMBIENTE (URa %):</th>
     
       


    </tr>
   
  </thead>



</table>
 <table class="table-documento">
  <tbody>
    <tr>
      <td >
      Temp:
        <span class="uppercase weight-bold"> $temp</span>
      </td>

      <td >
      URa:
        <span class="uppercase weight-bold"> $hum</span>
      </td>

     
   
    </tr>






  </tbody>
</table>
</section>

<section class="content-block">
<header>
  <h4 class="content-block-title">Execultante</h4>
</header>
  $html3

</section>
                  
      
        
<section class="content-block">
<header>
  <h4 class="content-block-title">Observações</h4>
</header>
<table class="table-documento lista-atividades">
  <thead>
    <tr>
      <th class="width-50-percent text-left">M.P: $obs_mp</th>
         <th class="width-50-percent text-left">Técnica: $obs_tec</th>
       
         
         

    </tr>
   
  </thead>



</table>

<table class="table-documento lista-atividades">
<thead>
<tr>
<th class="width-100-percent text-left">
Data Inicio:
<span class="uppercase weight-bold"  > $date_start_ms</span>  
<br>
Data Termino:
<span class="uppercase weight-bold" > $date_end_ms</span> 
<br>
Tempo Execução:
<span class="uppercase weight-bold" > $time_ms</span> 
</th>

</tr>
</thead>
</table>
<p><b><center> <h3><small>Declaro que os serviços constantes no presente relatório técnico foram efetivamente
prestados, sendo aceito por mim nesta data. </small></h3> </center></b></p>

</section>

         
<br>
             

           

        


     

            <br>
            <section class="content-block">
             <header>
               <h3 class="content-block-title">Assinatura</h3>
             </header>
              <table style="width: 100%; margin-right: 25px" class="table-documento lista-atividades">
                       <tbody style="text-align: left">
                         <tr>
                          <td class="width-10-percent text-left">_______________________________________________</td>
   
                           <td class="width-10-percent text-left">______________________________________________</td>
                         </tr>
                         <tr>
                           <td>Executante: $primeironome $ultimonome CREA/CFT: $entidade</td>
                            <td>Aprovado:</td>
                         </tr>
                         <tr>
                           <td class="width-10-percent text-left">______________________________________________</td>
                          <td class="width-10-percent text-left">_______________________________________________</td>
                         </tr>
                         <tr>
                           <td>Responsavel Técnico:  $primeironome_responsavel $ultimonome_responsavel CREA/CFT: $entidade_responsavel</td>
                            <td>Responsavel Area:</td>
                         </tr>
                       </tbody>
   
                     </table>
   
           </section>
           <br>
           <section class="content-block">
   
   
                    <div class="center-side">
   
                      <div class="center-side-text">
   
                        <small>$print</small>
                          <small>ROTINA Nº $rotina</small>
                        <br>
   
                      </div>
                    </div>
   
          </section>
   
           <!--./content-block-->
         </div>
         <!--/.container-->
       </main>
   
       <footer class="footer">
       <div class="">
   <center>
    Copyright ©2022 MK Sistemas Biomédicos- by MK Sistemas Biomédicos.
   </center>
   
          </br>
        </div>
         <table>
           <tr>
             <td class="footer-info base-background">
   
   
             </td>
   
           </tr>
   
         </table>
       </footer>
   
     </div>

EOD;
  // Print text using writeHTMLCell()
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  
  // ---------------------------------------------------------
  
  // Close and output PDF document
  // This method has several options, check the source code documentation for more information.
  $pdf->Output('Rotina.pdf', 'I');
   