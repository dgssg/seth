<?php

include("../database/database.php");


$codigo= $_POST['codigo'];
$id_equipamento= $_POST['equipamento'];
$data_start= $_POST['data_start'];
$val= $_POST['val'];
$ven= 0;
$status= 0;
$id_manufacture= $_POST['id_manufacture'];
$procedure_cal= $_POST['procedure'];
$id_colaborador= $_POST['id_colaborador'];
$id_responsavel= $_POST['id_responsavel'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];
$obs= $_POST['obs'];
if($obs==""){
    $obs="Este certificado &#233; v&#225;lido para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar.";
}

$stmt = $conn->prepare("INSERT INTO tse (id_equipamento, data_start, val, ven, status, id_manufacture, procedure_cal, id_colaborador, id_responsavel, temp, hum, obs,codigo) VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?,?,?,?)");
$stmt->bind_param("sssssssssssss",$id_equipamento, $data_start, $val, $ven, $status, $id_manufacture, $procedure_cal, $id_colaborador, $id_responsavel, $temp, $hum, $obs,$codigo);
$execval = $stmt->execute();
 $last_id = $conn->insert_id;
 $stmt->close();



$stmt = $conn->prepare("INSERT INTO tse_report (id_calibration) VALUES (?)");
$stmt->bind_param("s",$last_id);
$execval = $stmt->execute();
 $stmt->close();


//echo "New records created successfully";

setcookie('anexo', null, -1);
header('Location: ../calibration-equipament-edit?laudo='.$last_id);
?>
