<?php

include("../database/database.php");

$codigoget = ($_GET["id"]);
$fornecedor = $_POST['fornecedor'];
$date_mp_start = $_POST['date_start'];
$date_mp_end = $_POST['date_end'];
$time_mp = $_POST['time'];
$colaborador = $_POST['colaborador'];
$message_mp = $_POST['message_mp'];
$message_tc = $_POST['message_tc'];
$routine = $_POST['routine'];
$periodicidade = $_POST['periodicidade'];
$programada= $_POST['programada'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];


$anexo=$_COOKIE['anexo'];

$status="3";
$status2=0;

$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_fornecedor= ? WHERE id= ?");
$stmt->bind_param("ss",$fornecedor,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_start= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_start,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET time_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$time_mp,$codigoget);
$execval = $stmt->execute();
$stmt->close();


if ($anexo != "") {

$stmt = $conn->prepare("UPDATE maintenance_preventive SET file= ? WHERE id= ?");
$stmt->bind_param("ss",$anexo,$codigoget);
$execval = $stmt->execute();
$stmt->close();
}
$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$colaborador,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$message_mp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_tc= ? WHERE id= ?");
$stmt->bind_param("ss",$message_tc,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET temp= ? WHERE id= ?");
$stmt->bind_param("ss",$temp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET hum= ? WHERE id= ?");
$stmt->bind_param("ss",$hum,$codigoget);
$execval = $stmt->execute();
$stmt->close();



setcookie('anexo', null, -1);
echo "<script>alert('Atualizado!');document.location='../maintenance-preventive-close-edit?routine=$routine'</script>";

?>
