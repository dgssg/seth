<?php

include("../database/database.php");

$codigoget= $_POST['id'];

$id_documentation_hfmea= $_POST['hfmea'];
$risco = $_POST['risco'];
$causa = $_POST['causa'];
$efeito = $_POST['efeito'];
$preven = $_POST['preven'];
$controle = $_POST['controle'];
$sentinela = $_POST['sentinela'];
$probabilidade = $_POST['probabilidade'];
$gravidade = $_POST['gravidade'];
$abrangen = $_POST['abrangen'];
$situacao = $_POST['situacao'];
$total = $_POST['total'];
$grau = $_POST['grau'];
$conti = $_POST['conti'];

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET risco = ? WHERE id= ?");
$stmt->bind_param("ss",$risco,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET causa = ? WHERE id= ?");
$stmt->bind_param("ss",$causa,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET efeito = ? WHERE id= ?");
$stmt->bind_param("ss",$efeito,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET preven = ? WHERE id= ?");
$stmt->bind_param("ss",$preven,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET controle = ? WHERE id= ?");
$stmt->bind_param("ss",$controle,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET sentinela = ? WHERE id= ?");
$stmt->bind_param("ss",$sentinela,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET probabilidade = ? WHERE id= ?");
$stmt->bind_param("ss",$probabilidade,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET gravidade = ? WHERE id= ?");
$stmt->bind_param("ss",$gravidade,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET abrangen = ? WHERE id= ?");
$stmt->bind_param("ss",$abrangen,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET situacao = ? WHERE id= ?");
$stmt->bind_param("ss",$situacao,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET total = ? WHERE id= ?");
$stmt->bind_param("ss",$total,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET grau = ? WHERE id= ?");
$stmt->bind_param("ss",$grau,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE documentation_hfmea_itens SET conti = ? WHERE id= ?");
$stmt->bind_param("ss",$conti,$codigoget);
$execval = $stmt->execute();

echo "<script>alert('Atualizado!');document.location='../documentation-hfmea'</script>";

?>
