<?php
  //============================================================+
  // File name   : example_001.php
  // Begin       : 2008-03-04
  // Last Update : 2013-05-14
  //
  // Description : Example 001 for TCPDF class
  //               Default Header and Footer
  //
  // Author: Nicola Asuni
  //
  // (c) Copyright:
  //               Nicola Asuni
  //               Tecnick.com LTD
  //               www.tecnick.com
  //               info@tecnick.com
  //============================================================+
  
  /**
  * Creates an example PDF TEST document using TCPDF
  * @package com.tecnick.tcpdf
  * @abstract TCPDF - Example: Default Header and Footer
  * @author Nicola Asuni
  * @since 2008-03-04
  */
  
  // Include the main TCPDF library (search for installation path).
  require_once('../../framework/TCPDF/tcpdf.php');
  
  // create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('MK Sistemas Biomedicos');
  $pdf->SetTitle('Sistema SETH');
  $pdf->SetSubject('Ordem de Serviço');
  $pdf->SetKeywords('Ordem de Serviço');
  
  // set default header data
  $pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH,'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
  $pdf->setFooterData(array(0,64,0), array(0,64,128));
  
  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  
  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  
  // set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  
  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  
  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
  }
  
  // ---------------------------------------------------------
  
  // set default font subsetting mode
  $pdf->setFontSubsetting(true);
  
  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage();
  
  // set text shadow effect
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT os_rasch,workflow FROM tools";


if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($os_rasch,$workflow);
  while ($stmt->fetch()) {
	//printf("%s, %s\n", $solicitante, $equipamento);
  }
}

$query = "SELECT  custom_service.nome,category.nome,os.services,os.reg_date,os.upgrade,os.id_defeito,os.defeito,os.time,os.date_start,os.date_end,os.sla_term,instituicao.logo,os_status.status,os_posicionamento.status,os_carimbo.img,equipamento_familia.fabricante,os.time_backlog,os.id_fornecedor,os.id_prioridade,os.id_observacao, os.id_chamado,os.id_tecnico,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN usuario on usuario.id = os.id_usuario INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area INNER JOIN os_status on os_status.id = os.id_status INNER JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento INNER JOIN os_carimbo ON os_carimbo.id = os.id_carimbo INNER JOIN os_workflow on os_workflow.id = os.id_workflow INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN custom_service on custom_service.id = os.id_servico LEFT JOIN category on category.id = os.id_category WHERE os.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($custom_service,$category,$services,$os_reg_date,$os_upgrade,$id_defeito,$reparo_tecnico,$time_os,$date_start_os, $date_end_os,$sla_term,$logo,$os_status,$os_posicionamento,$carimbo_img,$equipamento_fabricante,$time_backlog,$id_fornecedor,$id_prioridade,$id_observacao,$id_chamado,$id_tecnico,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);




while ($stmt->fetch()) {

}
}
  if($id_tecnico !== ""){
    $sql = "SELECT  id, primeironome FROM colaborador WHERE id like '$id_tecnico' ";
    
    
    if ($stmt = $conn->prepare($sql)) {
      $stmt->execute();
      $stmt->bind_result($id,$primeironome_query);
      while ($stmt->fetch()) {
      }
    }
  }
  $sql = "SELECT  id, empresa FROM fornecedor WHERE id like '$id_fornecedor' ";
  
  
  if ($stmt = $conn->prepare($sql)) {
    $stmt->execute();
    $stmt->bind_result($id_empresa,$empresa_query);
    while ($stmt->fetch()) {
    }
  }
  $sql = "SELECT  id, prioridade FROM os_prioridade WHERE id like '$id_prioridade'  ";
  
  
  if ($stmt = $conn->prepare($sql)) {
    $stmt->execute();
    $stmt->bind_result($id,$prioridade_smt_query);
    while ($stmt->fetch()) {
    }
  }
  
  $query="SELECT regdate_os_posicionamento.data_end,regdate_os_posicionamento.detail,regdate_os_posicionamento.id_os, regdate_os_posicionamento.id_posicionamento, regdate_os_posicionamento.id_status, regdate_os_posicionamento.reg_date, os_posicionamento.id, os_posicionamento.status, os_status.id, os_status.status FROM  regdate_os_posicionamento INNER JOIN os_posicionamento ON regdate_os_posicionamento.id_posicionamento = os_posicionamento.id INNER JOIN os_status ON regdate_os_posicionamento.id_status = os_status.id WHERE regdate_os_posicionamento.id_os = '$codigoget '";
  $row=1;
  $tabela = '';
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($data_end, $detail, $id_os, $id_posicionamento, $id_status, $regdate, $id, $status_posicionamento, $id, $status_os);
    
    while ($stmt->fetch()) {
      $tabela .= '<tr>';
      $tabela .= '<th scope="row">' . $row . '</th>';
      $tabela .= '<td>' . $status_posicionamento . '</td>';
      $tabela .= '<td>' . $status_os . '</td>';
      $tabela .= '<td>' . $detail . '</td>';
      $tabela .= '<td>' . $data_end . '</td>';
      $tabela .= '<td>' . $regdate . '</td>';
      $tabela .= '</tr>';
      $row = $row + 1;
    }
  }
  $tabela1 = '';
  $query="SELECT regdate_os_registro.id_os, regdate_os_registro.id_user,regdate_os_registro.reparo,regdate_os_registro.date_start,regdate_os_registro.date_end,regdate_os_registro.time,regdate_os_registro.reg_date, regdate_os_registro.upgrade, colaborador.id, colaborador.primeironome FROM regdate_os_registro INNER JOIN colaborador on regdate_os_registro.id_user = colaborador.id WHERE regdate_os_registro.id_os like '$codigoget'";
  $row=1;
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_os,$id_user,$reparo,$date_start,$date_end,$time,$reg_date,$upgrade,$id,$usuario);
    while ($stmt->fetch()) {
      $tabela1 .= '<tr>';
      $tabela1 .= '<th scope="row">' . $row . '</th>';
      $tabela1 .= '<td>' . $usuario . '</td>';
      $tabela1 .= '<td>' . $date_start . '</td>';
      $tabela1 .= '<td>' . $date_end . '</td>';
      $tabela1 .= '<td>' . $time . '</td>';
      $tabela1 .= '<td>' . $reparo . '</td>';
      $tabela1 .= '</tr>';
      $row = $row + 1;
    }
  }
  $tabela2 = '';
  $query="SELECT regdate_os_carimbo.id_os, regdate_os_carimbo.id_carimbo,regdate_os_carimbo.reg_date, regdate_os_carimbo.upgrade, os_carimbo.id, os_carimbo.carimbo FROM regdate_os_carimbo INNER JOIN os_carimbo on regdate_os_carimbo.id_carimbo = os_carimbo.id WHERE regdate_os_carimbo.id_os like '$codigoget'";
  $row=1;
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_os,$id_carimbo,$reg_date,$upgrade,$id,$carimbo);
    while ($stmt->fetch()) {
      $tabela2 .= '<tr>';
      $tabela2 .= '<th scope="row">' . $row . '</th>';
      $tabela2 .= '<td>' . $carimbo . '</td>';
      $tabela2 .= '<td>' . $reg_date . '</td>';
      $tabela2 .= '<td>' . $upgrade . '</td>';
 
      $tabela2 .= '</tr>';
      $row = $row + 1;
    }
  }
  $tabela3 = '';
  $query="SELECT regdate_os_workflow.id_os, regdate_os_workflow.id_workflow,regdate_os_workflow.reg_date, regdate_os_workflow.upgrade, os_workflow.id, os_workflow.workflow FROM regdate_os_workflow INNER JOIN os_workflow on regdate_os_workflow.id_workflow = os_workflow.id WHERE regdate_os_workflow.id_os like '$codigoget'";
  $row=1;
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_os,$id_workflow,$reg_date,$upgrade,$id,$workflow);
    while ($stmt->fetch()) {
      $tabela3 .= '<tr>';
      $tabela3 .= '<th scope="row">' . $row . '</th>';
      $tabela3 .= '<td>' . $workflow . '</td>';
      $tabela3 .= '<td>' . $reg_date . '</td>';
      $tabela3 .= '<td>' . $upgrade . '</td>';
      
      $tabela3 .= '</tr>';
      $row = $row + 1;
    }
  }
  if($id_defeito > 0){
    $sql = "SELECT  id, defeito FROM os_defeito WHERE id = $id_defeito  ";
    
    
    if ($stmt = $conn->prepare($sql)) {
      $stmt->execute();
      $stmt->bind_result($id,$defeito);
      while ($stmt->fetch()) {
        
      }
    }
  }
  $tabela4 = '';
  $query="SELECT regdate_os_signature.id_os,regdate_os_signature.id_user,regdate_os_signature.id_signature,regdate_os_signature.reg_date,usuario.nome FROM regdate_os_signature INNER JOIN usuario ON regdate_os_signature.id_user = usuario.id   WHERE regdate_os_signature.id_os like '$codigoget'";
  $row=1;
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_os,$id_user,$id_signature,$reg_date,$id_user);
    while ($stmt->fetch()) {
      $tabela4 .= '<tr>';
      $tabela4 .= '<th scope="row">' . $row . '</th>';
      $tabela4 .= '<td>' . $id_user . '</td>';
      $tabela4 .= '<td>' . $id_signature . '</td>';
      $tabela4 .= '<td>' . $reg_date . '</td>';
      
      $tabela4 .= '</tr>';
      $row = $row + 1;
    }
  }
     
  $html = <<<EOD
 
<div class="conteudo" style="min-height:900px; font-size:17px;">
  <table style="border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
    <tbody>
      <tr>
        <td align="center"><strong>Ordem de Serviço N&ordm; $codigoget</strong> </td>
        <td align="center">
          <center>
            <img src="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=www.seth.mksistemasbiomedicos.com.br/customer-ec/$key/os-viewer?os=$codigoget" alt="MK Sistemas Biomedicos" width="70" />
          </center>
        </td>
      </tr>
    </tbody>
  </table>

<p><b><center> <h2><small>Dados do equipamento:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td width="50%"> <strong> Equipamento:</strong>$equipamento_nome </td>
<td><strong>Codigo:</strong>$equipamento_codigo</td>
</tr>
<tr>
<td><strong>Modelo:</strong>$equipamento_modelo</td>
<td><strong>Fabricante:</strong>$equipamento_fabricante</td>
</tr>
<tr>
<td><strong> N&ordm; Serie:</strong>$equipamento_numeroserie</td>
<td><strong>Patrimonio:</strong>$equipamento_patrimonio</td>
</tr>
</tbody>
</table>

<p><b><center> <h2><small>Dados da solicitação:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td><strong> Solicitante:</strong>$id_usuario_nome - $id_usuario_sobrenome</td>
<td><strong>Solicitação:</strong>$id_solicitacao</td>
<tr>
<tr>
<td><strong>Registro:</strong>$reg_date</td>
<?php if($assistence == "0"){  ?>
  <td><strong>Cliente:</strong>
  <?php }else {   ?>
<td>  <strong>Setor:</strong>           
  <?php }  ?>$id_area - $id_localizacao</td>
<tr>


</tr>
</tbody>
</table>
 
<p><b><center> <h2><small>Dados da O.S:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td width="50%"> <strong> Tecnico:</strong>$primeironome_query </td>
<td><strong>Observação:</strong>$id_observacao</td>
</tr>
<tr>
<td><strong>Fornecedor:</strong>$empresa_query</td>
<td><strong>Chamado:</strong>$id_chamado</td>
</tr>
<tr>
<td><strong>Prioridade:</strong>$prioridade_smt_query</td>
<td><strong>SLA:</strong>$sla_term</td>
</tr>
<tr>
<td><strong>Posicionamento:</strong>$os_posicionamento</td>
<td><strong>Status:</strong>$os_status</td>
</tr>
<tr>
<td><strong>Categoria:</strong>$category</td>
<td><strong>Serviço:</strong>$custom_service</td>
</tr>
</tbody>
</table>
<p><b><center> <h2><small>Posicionamento:</small></h2> </center></b></p>
 
                    <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Pocicionamento</th>
                          <th>Status</th>
                            <th>Detalhe</th>
                          <th>Prazo</th>
                          <th>Registro</th>
                        </tr>
                      </thead>
                      <tbody>
              $tabela
                      </tbody>
                    </table>
<p><b><center> <h2><small>Dados de Registro:</small></h2> </center></b></p>

                <!--arquivo -->
 
                    <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Executante</th>
                          <th>Data de inicio</th>
                          <th>Data de Termino</th>
                          <th>Tempo</th>
                          <th>Reparo</th>

                        </tr>
                      </thead>
                      <tbody>
                          $tabela1      
                      </tbody>
                    </table>

<p><b><center> <h2><small>Dados de Carimbo:</small></h2> </center></b></p>


                <!--arquivo -->
     
                      <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Carimbo</th>
                          <th>Registro</th>
                          <th>Atualização</th>


                        </tr>
                      </thead>
                      <tbody>
                          $tabela2     
</tbody>
                    </table>

<p><b><center> <h2><small>Dados de Fluxo de Trabalho:</small></h2> </center></b></p>


 
                    <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Workflow</th>
                          <th>Registro</th>
                          <th>Atualização</th>


                        </tr>
                      </thead>
                      <tbody>
                        $tabela3                             
</tbody>
                    </table>

  <p><b><center> <h2><small>Dados de Fechamento :</small></h2> </center></b></p>



  <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td width="50%"> <strong> Causa:</strong>$defeito </td>
<td><strong>Defeito Encontrado:</strong>$reparo_tecnico</td>
</tr>
<tr>
<td><strong>Data Inicial:</strong>$date_start_os</td>
<td><strong>Data Final:</strong>$date_end_os</td>
</tr>
<tr>
<td><strong>Tempo:</strong>$time_os</td>
<td><strong>Serviço:</strong>$services</td>
</tr>
<tr>
<td><strong>Data Registro:</strong>$os_reg_date</td>
<td><strong>Data Atualização:</strong>$os_upgrade</td>
</tr>
</tbody>
</table>
    <p><b><center> <h2><small>Dados de Assinatura Eletronica :</small></h2> </center></b></p>
              <!--arquivo -->
   
                      <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>

                          <th>Usuário</th>
                          <th>Assinatura</th>
                            <th>Registro</th>

                        </tr>
                      </thead>
                      <tbody>
      $tabela4
                      </tbody>
                    </table>

  <p><b><center> <h3><small>Declaro que os serviços constantes no presente relatório técnico foram efetivamente
prestados, sendo aceito por mim nesta data. </small></h3> </center></b></p>


                  <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>

                                <td valign="bottom" align="center">
                                </br>
                              </br>
                                    <span id="ctl00_ContentPlaceHolder1_repRelatorio_ctl00_lblSupervisorMatricula">________________________</span><br />

                                </td>
                                <td valign="bottom" align="center">
                                </br>
                              </br>
                                  <span id="ctl00_ContentPlaceHolder1_repRelatorio_ctl00_lblUsuárioFechamentoMatricula">________________________</span><br />
                                    </td>
                                <td valign="bottom" align="center">
                                </br>
                              </br>
                                    <span id="ctl00_ContentPlaceHolder1_repRelatorio_ctl00_responsavelAceiteMatricula">________________________</span><br />

                                </td>
                            </tr>
                            <tr>

                                <td valign="top" align="center">Supervisor de Manutenção</td>
                                <td valign="top" align="center">Executante</td>
                                <td valign="top" align="center">Matrícula / Resp. pelo aceite</td>
                            </tr>
                        </table>
  <!-- Restante do código continua aqui... -->

</div>

<!-- Rodapé -->
<table width="100%" style="vertical-align: top;font-family:'Times New Roman',Times,serif; font-size: 11px;">
  <tr>
    <td align="center" colspan="2">
      <div style="padding-top: 20px;"><strong>Copyright © 2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados.</strong><br>www.mksistemasbiomedicos.com.br</div>
    </td>
  </tr>
  <tr>
    <td style="font-size: 9px" align="left">MK-01-ra-1.0</td>
    <td align="right"><!-- pagina --></td>
  </tr>
</table>
EOD;
  // Print text using writeHTMLCell()
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  
  // ---------------------------------------------------------
  
  // Close and output PDF document
  // This method has several options, check the source code documentation for more information.
  $pdf->Output('Ordem de Serviço.pdf', 'I');
  
  //============================================================+
  // END OF FILE
  //============================================================+