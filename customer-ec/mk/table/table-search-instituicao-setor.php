<?php
include("../database/database.php");
$query = "SELECT instituicao_area.codigo,instituicao_area.id,instituicao_area.id_unidade,instituicao_area.custo,instituicao_area.nome,instituicao_area.upgrade,instituicao_area.reg_date, instituicao.instituicao FROM instituicao_area INNER JOIN instituicao ON instituicao_area.id_unidade = instituicao.id WHERE instituicao_area.trash = 1 ORDER by instituicao_area.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
