<?php
include("../database/database.php");
$query = "SELECT 
    email_control.id AS 'id',
    email_control.id_number AS 'documento',
    email_control.reg_date AS 'reg_date',
    email_grupo.nome AS 'grupo',
    email_mod.nome AS 'familia',
    email_status.nome AS 'status',
    usuario.nome AS 'usuario'
    
FROM 
    email_control 
LEFT JOIN 
    email_grupo ON email_grupo.id = email_control.email_grupo 
LEFT JOIN 
    email_mod ON email_mod.id = email_control.email_mod  
LEFT JOIN 
    email_status ON email_status.id = email_control.email_status 
LEFT JOIN 
    usuario  ON usuario.id = email_control.id_ususario 


ORDER BY 
    email_control.id DESC;
";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
