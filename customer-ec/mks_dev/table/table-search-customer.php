<?php
include("../database/database.php");
$query = "SELECT id, empresa, cnpj, seguimento, adress, city, state, cep, contato, email, telefone_1, telefone_2, observacao,  reg_date, upgrade FROM customer WHERE trash = 1 ORDER by id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
