<?php
include("../database/database.php");
$query = "SELECT 
    anvisa_class.nome AS 'classe',
    equipamento_grupo.id, 
    equipamento_grupo.nome, 
    equipamento_grupo.upgrade, 
    equipamento_grupo.reg_date, 
    equipamento_grupo.yr,
    equipamento_grupo.id_time AS 'id_time', 
    equipamento_grupo.id_periodicidade AS 'periodicidade',
    category.nome AS 'id_categoria', 
    maintenance_group.nome AS 'id_mp_group', 
    mp1.name AS 'nome_procedimento_1', 
    mp2.name AS 'nome_procedimento_2',
    colaborador.primeironome,
    colaborador.ultimonome,
    documentation_pop.titulo
FROM 
    equipamento_grupo 
LEFT JOIN 
    anvisa_class ON anvisa_class.id = equipamento_grupo.id_class 
LEFT JOIN 
    category ON category.id = equipamento_grupo.id_categoria  
LEFT JOIN 
    maintenance_group ON maintenance_group.id = equipamento_grupo.id_mp_group 
LEFT JOIN 
    maintenance_procedures AS mp1 ON mp1.id = equipamento_grupo.id_procedimento_1 
LEFT JOIN 
    maintenance_procedures AS mp2 ON mp2.id = equipamento_grupo.id_procedimento_2 
LEFT JOIN 
    colaborador ON colaborador.id = equipamento_grupo.id_colaborador 
LEFT JOIN 
    documentation_pop ON documentation_pop.id = equipamento_grupo.id_pop
WHERE 
    equipamento_grupo.trash = 1 
ORDER BY 
    equipamento_grupo.id DESC;
";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
