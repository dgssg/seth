<?php
session_id($usuario);
session_start();
session_destroy();
session_commit();
echo "<script>alert('Usuario deslogado!');document.location='../index.php'</script>";
header ("location: ../index.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title></title>

    <meta http-equiv="refresh" content="1; URL='https://www.seth.mksistemasbiomedicos.com.br/'"/>
</head>
<body>
</body>

</html>
