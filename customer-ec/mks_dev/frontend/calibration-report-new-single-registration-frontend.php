<?php


// Create connection
include("database/database.php");// remover ../


?>
<form action="backend/calibration-report-new-single-register-backend.php" method="post">

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Realizado</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="date_register" class="form-control" type="date" name="date_register"  >
    </div>
  </div>

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Validade (Meses)</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="date_validation" class="form-control" type="number" name="date_validation"  >
    </div>
  </div>



  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  >
        <option value="	">Selecione a Empresa</option>
        <?php



        $sql = "SELECT  id, nome FROM calibration_parameter_manufacture ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$nome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
            <?php
            // tira o resultado da busca da mem贸ria
          }

        }
        $stmt->close();

        ?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_manufacture').select2();
  });
  </script>



  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cliente</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="customer" class="form-control" type="text" name="customer"  >
    </div>
  </div>

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CPF/CNPJ</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="documentation" class="form-control" type="text" name="documentation"  >
    </div>
  </div>





  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Equipamento</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="equipament" class="form-control" type="text" name="equipament"  >
    </div>
  </div>


  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nº de Série:</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="serie_number" class="form-control" type="text" name="serie_number"  >
    </div>
  </div>



  <div class="ln_solid"></div>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento_grupo" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Grupo de Equipamento ">Grupo de Equipamento<span  ></span>
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="equipamento_grupo">
        <option value=""></option>
        <?php
        $sql = "SELECT  id, nome FROM equipamento_grupo  WHERE trash = 1 ";
        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$equipamento_grupo);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
            <?php
            // tira o resultado da busca da memória
          }
        }
        $stmt->close();
        ?>
      </select>
      <span class="input-group-btn">


        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">
        </span>
      </span>										 </div>
    </div>

    <script>
    $(document).ready(function() {
      $('#equipamento_grupo').select2();
    });
    </script>


    <div class="item form-group">
      <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Codigo Externo ">Codigo Externo <span  ></span>
      </label>
      <div class="col-md-4 col-sm-4 ">
        <input id="codigo" class="form-control" type="text" name="codigo"  >
      </div>
    </div>

    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observa&ccedil;&atilde;o de calibra&ccedil;&atilde;o</label>
      <div class="col-md-4 col-sm-4 ">


        <textarea  rows="5" cols="5" id="obs"  class="form-control" name="obs" data-parsley-trigger="keyup"
        data-parsley-validation-threshold="2" placeholder="Este certificado &#233; valido somente para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar."  value="Este certificado &#233; valido somente para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar."></textarea>

      </div>
    </div>


    <div class="item form-group">
      <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Colaborador <span class="required" >*</span>
      </label>
      <div class="col-md-4 col-sm-4 ">
        <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador" value="<?php printf($id_tecnico); ?>">
          <option value="	">Selecione o Executante</option>


          <?php



          $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1  ";


          if ($stmt = $conn->prepare($sql)) {
            $stmt->execute();
            $stmt->bind_result($id,$primeironome,$ultimonome);
            while ($stmt->fetch()) {
              ?>
              <option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
              <?php
              // tira o resultado da busca da mem��ria
            }

          }
          $stmt->close();

          ?>
        </select>
      </div>
    </div>









    <script>
    $(document).ready(function() {
      $('#id_colaborador').select2();
    });
    </script>
    <div class="item form-group">
      <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Responsável ">Responsável Tecnico <span class="required" >*</span>
      </label>
      <div class="col-md-4 col-sm-4 ">
        <select type="text" class="form-control has-feedback-right" name="id_responsavel" id="id_responsavel"  placeholder="Responsavel" value="<?php printf($id_tecnico); ?>">
          <option value="	">Selecione o Responsável</option>


          <?php



          $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


          if ($stmt = $conn->prepare($sql)) {
            $stmt->execute();
            $stmt->bind_result($id,$primeironome,$ultimonome);
            while ($stmt->fetch()) {
              ?>
              <option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
              <?php
              // tira o resultado da busca da mem��ria
            }

          }
          $stmt->close();

          ?>
        </select>
      </div>
    </div>









    <script>
    $(document).ready(function() {
      $('#id_responsavel').select2();
    });
    </script>

    <div class="ln_solid"></div>

    <div class="item form-group ">

      <label  >Temperatura  </label>

      <input name="temp" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#26B99A" data-skin="tron" data-thickness=".2" value="25.5" data-step=".1">
      <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($carga); ?> " > -->


      <label >Umidade  </label>

      <input name="hum" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#34495E" data-skin="tron" data-thickness=".2" value="65.5" data-step=".1">
      <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($carga); ?> " > -->

    </div>
    <div class="ln_solid"></div>
    <div class="form-group row">
      <label class="col-form-label col-md-3 col-sm-3 "></label>
      <div class="col-md-3 col-sm-3 ">
        <center>
          <button id="compose" class="btn btn-sm btn-success btn-block" class="form-control "type="button">Observa&ccedil;&atilde;o</button>
        </center>
      </div>
    </div>


    <button type="reset" class="btn btn-primary"onclick="new PNotify({
      title: 'Limpado',
      text: 'Todos os Campos Limpos',
      type: 'info',
      styling: 'bootstrap3'
    });" />Limpar</button>
    <input type="submit" class="btn btn-primary" onclick="new PNotify({
      title: 'Registrado',
      text: 'Informa&ccedil;&atilde;es registrada!',
      type: 'success',
      styling: 'bootstrap3'
    });" value="Prosseguir" />

  </form>
  <div class="ln_solid"></div>
  <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
  <!-- compose -->
  <div class="compose col-md-6  ">
    <div class="compose-header">
      Observa&ccedil;&atilde;o
      <button type="button" class="close compose-close">
        <span>X</span>
      </button>
    </div>
    <form  action="backend/" method="post">
      <div class="compose-body">
        <div id="alerts"></div>


        <input size="90" type="text" id="editor" name="editor" class="editor-wrapper" value="Este certificado &eacute; v&aacute;lido para o equipamento ensaiado, n&atilde;o sendo extensivo para quaisquer lotes, mesmo similar.">


      </div>

      <div class="compose-footer">
        <button class="btn btn-sm btn-success" type="submit">Salvar</button>
      </div>
    </form>
  </div>
  <!-- /compose -->
