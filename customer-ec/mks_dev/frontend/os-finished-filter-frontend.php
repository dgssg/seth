


        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ordem de Serviço</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
              <div class="clearfix"></div>
              <div class="clearfix"></div>
              <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
            <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

<!-- Menu -->
 
          
                <a class="btn btn-app <?php echo $current_page == 'os-open' ? 'active' : ''; ?>" href="os-open">
    <i class="fa fa-folder-open"></i> O.S Abertura
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-opened' ? 'active' : ''; ?>" href="os-opened">

    <i class="fa fa-play"></i>O.S Aberto 
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-progress' ? 'active' : ''; ?>" href="os-progress">
	 <i class="fa fa-cogs"></i>O.S Processo
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-signature' ? 'active' : ''; ?>" href="os-signature">

    <i class="fa fa-pencil"></i>O.S Assinatura
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-signature-user' ? 'active' : ''; ?>" href="os-signature-user">

    <i class="fa fa-user"></i>O.S Solicitante
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-finished' ? 'active' : ''; ?>" href="os-finished">

    <i class="fa fa-check-circle"></i>O.S Concluído
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-canceled' ? 'active' : ''; ?>" href="os-canceled">

    <i class="fa fa-times-circle"></i>O.S Cancelada
</a>

 
<a class="btn btn-app <?php echo $current_page == 'os-integration' ? 'active' : ''; ?>" href="os-integration">

    <i class="fa fa-code"></i>O.S Integração
</a>

                </div>
              </div>
              
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form id="filter-form" class="container">
                      <div class="form-row">
                      
                      
                        <div class="col-md-3">
                          <?php if($assistence == "0"){  ?>
                          <label for="unidade">Empresa</label>
                          <?php }else {   ?>
                          <label for="unidade">Unidade</label>
                          <?php }  ?>
                          <input type="text" id="unidade" name="unidade" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <?php if($assistence == "0"){  ?>
                          <label for="setor">Cliente</label>

                          <?php }else {   ?>
                          <label for="setor">Setor</label>
                          <?php }  ?>
                      <input type="text" id="setor" name="setor" class="form-control">
                    </div>
                        
                        <div class="col-md-3">
                          <?php if($assistence == "0"){  ?>
                          <label for="area">Localização</label>
                          
                          <?php }else {   ?>
                          <label for="area">Área</label>
                          <?php }  ?>
                          <input type="text" id="area" name="area" class="form-control">
                        </div>
                        
                                               
                        <div class="col-md-3">
                          <label for="grupo">Grupo de Equipamento</label>
                          <input type="text" id="grupo" name="grupo" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3">
                          <label for="familia">Família de Equipamento</label>
                          <input type="text" id="familia" name="familia" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="fabricante">Fabricante</label>
                          <input type="text" id="fabricante" name="fabricante" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="modelo">Modelo</label>
                          <input type="text" id="modelo" name="modelo" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="codigo">Código</label>
                          <input type="text" id="codigo" name="codigo" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3">
                          <label for="patrimonio">Patrimônio</label>
                          <input type="text" id="patrimonio" name="patrimonio" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="serie">Série</label>
                          <input type="text" id="serie" name="serie" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3">
                          <label for="serie">O.S</label>
                          <input type="text" id="os" name="os" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="serie">Data da Solicitação Inicial</label>
                          <input type="date" id="data_start" name="data_start" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="serie">Data da Solicitação Final</label>
                          <input type="date" id="data_end" name="data_end" class="form-control">
                        </div>
                        
                        <div class="col-md-3">
                          <button type="button" class="btn btn-primary mt-4" onclick="fetchData()">Filtrar</button>
                        </div>
                        
                      </div>
                      
                    </form>
                
                    
                  </div>
                </div>
              </div>
            </div>
            
            
            
            <div class="clearfix"></div>
            
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta <small> Ordem de Serviço</small> Concluida</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      
                      
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    
                    <div class="col-md-11 col-sm-11 ">
                      <div class="x_panel">
                        
                        <div class="x_content">
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="card-box table-responsive">
                                
                                
                                <table id="result-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th><input type="checkbox" id="check_all"></th>           
                                      <th>Ação</th>
                                      <th>O.S</th>
                                      <th>Data da solicitação</th>
                                      <th>Prioridade</th>
                                        <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                                        <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                                        <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  
                                      <th>Grupo</th>
                                      <th>Codigo</th>
                                      <th>Equipamento</th>
                                      <th>Modelo</th>
                                      <th>Fabricante</th>
                                      <th>Série</th>
                                      <th>Patrimônio</th>
                                    
                                                                            
                                      
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <!-- Os resultados serão inseridos aqui -->
                                  </tbody>
                                </table>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    
                    
                    <script>
                      let badgeHTML = '';
                      let badgeHTMLBaixa = '';
                      let badgeHTMLcheckist= '';
                      let badgeHTMLtreinamento= '';
                      let badgeHTMLparecer_1= '';
                      let badgeHTMLparecer_2= '';
                      let badgeHTMLsaida_1= '';
                      let badgeHTMLsaida_2= '';
                      let badgeHTMLconformidade= '';
                      
                      function fetchData() {
                        // Obtenha os valores dos filtros
                        
                        const unidade = document.getElementById('unidade').value;
                        const setor = document.getElementById('setor').value;
                        const area = document.getElementById('area').value;
                        const familia = document.getElementById('familia').value;
                        const fabricante = document.getElementById('fabricante').value;
                        const modelo = document.getElementById('modelo').value;
                        const serie = document.getElementById('serie').value;
                        const codigo = document.getElementById('codigo').value;
                        const patrimonio = document.getElementById('patrimonio').value;
                        const data_start = document.getElementById('data_start').value;
                        const grupo = document.getElementById('grupo').value;
                        const data_end = document.getElementById('data_end').value;
                        const os = document.getElementById('os').value;
                        
                        // Monta a URL com parâmetros
                        let url = 'table/table-search-os-finished-filter.php?';
                        if (grupo) url += `grupo=${encodeURIComponent(grupo)}&`;  if (unidade) url += `unidade=${encodeURIComponent(unidade)}&`;
                        if (setor) url += `setor=${encodeURIComponent(setor)}&`;
                        if (area) url += `area=${encodeURIComponent(area)}&`;
                        if (familia) url += `familia=${encodeURIComponent(familia)}&`;  if (fabricante) url += `fabricante=${encodeURIComponent(fabricante)}&`;  if (modelo) url += `modelo=${encodeURIComponent(modelo)}&`;  if (serie) url += `serie=${encodeURIComponent(serie)}&`;  if (codigo) url += `codigo=${encodeURIComponent(codigo)}&`;  if (patrimonio) url += `patrimonio=${encodeURIComponent(patrimonio)}&`;       if (os) url += `os=${encodeURIComponent(os)}&`; if (data_start) url += `data_start=${encodeURIComponent(data_start)}&`;
                        if (os) url += `os=${encodeURIComponent(os)}&`;
                        if (data_end) url += `data_end=${data_end}`;
                        
                        // Faz a requisição
                        fetch(url)
                        .then(response => response.json())
                        .then(data => {
                          // Limpa a tabela de resultados
                          const tbody = document.getElementById('result-table').getElementsByTagName('tbody')[0];
                          tbody.innerHTML = '';
                          
                          // Insere as linhas na tabela
                          data.forEach(row => {
                            
                            const tr = document.createElement('tr');
                            tr.innerHTML = `

              <td>
  <input type="checkbox" class="checkbox" value="${row.id}">

  
</td>
<td><div class="btn-group-mosaic">

  <!-- Grupo 1: Funções 1 a 4 -->
  <div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 1
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="os-progress-upgrade?os=${row.id}" onclick="new PNotify({ title: 'Editar', text: 'Abrindo Edição', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-edit"></i> Atualizar
      </a>
      <a class="dropdown-item" href="os-viewer?os=${row.id}" target="_blank" onclick="new PNotify({ title: 'Etiqueta', text: 'Visualizar', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-pdf-o"></i> Visualizar
      </a>
                  <a class="dropdown-item"      onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/os-trash-backend?id=${row.id}';
  }
})
">
                                  <i class="fa fa-trash"></i> Excluir
                                </a>
                                                              <a class="btn btn-app" onclick="
                                                                Swal.fire({
                                                                  title: 'Tem certeza?',
                                                                  text: 'Você não será capaz de reverter isso!',
                                                                  icon: 'warning',
                                                                  showCancelButton: true,
                                                                  confirmButtonColor: '#3085d6',
                                                                  cancelButtonColor: '#d33',
                                                                  confirmButtonText: 'Sim, Enviar Ordem de Serviço!'
                                                                }).then((result) => {
                                                                  if (result.isConfirmed) {
                                                                    Swal.fire(
                                                                      'Enviando!',
                                                                      'Seu arquivo será enviado.',
                                                                      'success'
                                                                    ),
                                                                    window.location = 'api-assessment-manufacture-send-os-user?id=${row.id}&control=2 ';
                                                                  }
                                                                })
                                                              ">
                                                                <i class="fa fa-at"></i> Email
                                                              </a>
      
    </div>
  </div>

  <!-- Grupo 2: Funções 5 a 8 -->
  <div class="btn-group">
    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 2
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="os-edit?os=${row.id}" onclick="new PNotify({ title: 'Editar', text: 'Editar', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-edit"></i> Editar 
      </a>
      <a class="dropdown-item" href="os-opened-close?os=${row.id}" onclick="new PNotify({ title: 'Check List', text: 'Abrir Check List', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-ban"></i>Cancelar  
      </a>
    </div>
  </div>

  <!-- Grupo 3: Funções 9 a 12 -->
  <div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 3
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="backend/os-assessment-backend.php?id=${row.id}" onclick="new PNotify({ title: 'Treinamento', text: 'Abrir Treinamento', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-question"></i> Avaliação 
       </a>
    </div>
  </div>

  <!-- Grupo 4: Funções 13 a 16 -->
  <div class="btn-group">
    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 4
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="#" onclick="openReopenModal(${row.id})" onclick="new PNotify({ title: 'Timeline', text: 'Abrir Timeline', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-hourglass"></i> Reabertura
      </a>

    </div>
  </div>

</div>
</td>
  <td>${row.id}</td>
  <td>${row.reg_date}</td>
  <td>${row.prioridade}</td>
              <td>${row.instituicao}</td>
              <td>${row.area}</td>
              <td>${row.setor}</td>
            
              <td>${row.grupo}</td>
              <td>${row.codigo}</td>
              <td>${row.equipamento}</td>
              <td>${row.modelo}</td>
              <td>${row.fabricante}</td>
              <td>${row.serie}</td>
              <td>${row.patrimonio}</td>
           
              
  
            `;
                            tbody.appendChild(tr);
                          });
                        })
                        .catch(error => console.error('Erro ao buscar dados:', error));
                      }
                      function takeAction() {
                        var selectedRows = [];
                        $('.checkbox:checked').each(function() {
                          selectedRows.push($(this).val());
                        });
                        
                        // Verifica se pelo menos um item foi selecionado
                        if (selectedRows.length > 0) {
                          // Constrói o URL com os IDs dos itens selecionados
                          var url = 'os-viwer-selected=' + selectedRows.join(',');
                          // Abre o URL em uma nova aba
                          window.open(url, '_blank');
                        } else {
                          // Se nenhum item foi selecionado, mostra uma mensagem
                          alert('Por favor, selecione pelo menos um item.');
                        }
                      }
                      $('#check_all').click(function() {
                        $('input[type=checkbox]').prop('checked', this.checked); // Seleciona todas as checkboxes
                      });
                      $('#datatable').dataTable( {
                        "processing": true,
                        "stateSave": true,
                        responsive: true,
                        
                        
                        
                        "language": {
                          "loadingRecords": "Carregando dados...",
                          "processing": "Processando  dados...",
                          "infoEmpty": "Nenhum dado a mostrar",
                          "emptyTable": "Sem dados disponíveis na tabela",
                          "zeroRecords": "Não há registros a serem exibidos",
                          "search": "Filtrar registros:",
                          "info": "Mostrando página _PAGE_ de _PAGES_",
                          "infoFiltered": " - filtragem de _MAX_ registros",
                          "lengthMenu": "Mostrar _MENU_ registros",
                          
                          "paginate": {
                            "previous": "Página anterior",
                            "next": "Próxima página",
                            "last": "Última página",
                            "first": "Primeira página",
                            
                            
                            
                          }
                        }
                        
                        
                        
                      } );
                    </script>
                    
                    
                  </div>
                </div>
              </div>
            </div>
            
            
            
            
          </div>
        </div>

