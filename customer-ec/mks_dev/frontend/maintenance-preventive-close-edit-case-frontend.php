  <?php
$codigoget = ($_GET["id"]);
$close = ($_GET["close"]);

// Create connection
      
      $sweet_salve = ($_GET["sweet_salve"]);
      $sweet_delet = ($_GET["sweet_delet"]);
      if ($sweet_delet == 1) {
         // Redirecionamento para a âncora "registro" usando JavaScript
         echo '<script>
      Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi deletado!\',
            showConfirmButton: false,
            timer: 1500
      });
</script>';
         
         
      }
      if ($sweet_salve == 1) {
         // Redirecionamento para a âncora "registro" usando JavaScript
         echo '<script>
            Swal.fire({
                  position: \'top-end\',
                  icon: \'success\',
                  title: \'Seu trabalho foi salvo!\',
                  showConfirmButton: false,
                  timer: 1500
            });
      </script>';
         
         
      }
include("database/database.php");

if ($close == "1") {

  $stmt = $conn->prepare("UPDATE maintenance_preventive SET id_status= 3 WHERE id= ?");
  $stmt->bind_param("s",$codigoget);
  $execval = $stmt->execute();
  $stmt->close();
  }


$query = "SELECT mp FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($mp);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }

$query = "SELECT maintenance_routine.id_maintenance_procedures_before,maintenance_routine.signature_digital, maintenance_routine.digital,maintenance_preventive.digital,maintenance_preventive.hum,maintenance_preventive.temp,maintenance_preventive.id_mp,maintenance_preventive.id_fornecedor,maintenance_preventive.date_end,maintenance_preventive.file,maintenance_preventive.time_mp,maintenance_preventive.obs_tc,maintenance_preventive.obs_mp,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_maintenance_procedures_before,$signature_digital,$digital,$tabela,$hum,$temp,$id_tecnico,$id_fornecedor,$date_end,$id_anexo,$time_mp,$obs_tc,$obs_mp,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
}
}

?>

 <!-- page content -->
 <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
<!-- page content -->
       <div class="right_col" role="main">
         <div class="">
           <div class="page-title">
             <div class="title_left">
             <h3>  Manutenção <small>Preventiva</small></h3>
             </div>


           </div>

           <div class="clearfix"></div>

           <div class="row" style="display: block;">
             <div class="col-md-12 col-sm-12  ">
               <div class="x_panel">
                 <div class="x_title">
                   <h2>Cabeçalho <small>Procedimento</small></h2>
                   <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="#">Settings 1</a>
                           <a class="dropdown-item" href="#">Settings 2</a>
                         </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                   </ul>
                   <div class="clearfix"></div>
                 </div>
                 <div class="x_content">

               <div class="item form-group">
                       <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                       </label>
                       <div class="col-md-6 col-sm-6 ">
                         <input type="text" id="nome" name="nome" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                       </div>
                     </div>

                     <div class="item form-group">
                       <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                       </label>
                       <div class="col-md-6 col-sm-6 ">
                         <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> " readonly="readonly" required="required" class="form-control ">
                       </div>
                     </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="categoria" name="categoria" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="time_ms"  class="form-control" type="text" name="time_ms"  value="<?php printf($time_ms); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                       <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade (dias)</label>
                       <div class="col-md-6 col-sm-6 ">
                         <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($periodicidade); ?>" readonly="readonly">
                       </div>
                     </div>



                      <div class="item form-group">
                       <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                       <div class="col-md-6 col-sm-6 ">
                         <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                       </div>
                     </div>
                      <div class="item form-group">
                       <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                       <div class="col-md-6 col-sm-6 ">
                         <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                       </div>
                     </div>



                 </div>
               </div>
             </div>
        </div>

<div class="x_panel">
               <div class="x_title">
                 <h2>Fechamento</h2>
                 <ul class="nav navbar-right panel_toolbox">
                     <li> <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg4" ><i class="fa fa-pencil"></i></a>
                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                   </li>
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                         class="fa fa-wrench"></i></a>
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Settings 1</a>
                       </li>
                       <li><a href="#">Settings 2</a>
                       </li>
                     </ul>
                   </li>
                   <li><a class="close-link"><i class="fa fa-close"></i></a>
                   </li>
                 </ul>
                 <div class="clearfix"></div>
               </div>
               <div class="x_content">


              <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Programada</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="programada_data" class="form-control" type="text" name="programada_data"  value="<?php printf($programada); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Abertura</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($date_start_ms); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Fechamento</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($date_end_ms); ?>" readonly="readonly">
                        </div>
                      </div>

                
                </div>
              </div>
            <!-- Registro -->
            <div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-hidden="true">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                     
                     <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel2">Registro</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                     </div>
                     <div class="modal-body">
                        <!-- Registro forms-->
                        <form action="backend/maintenance-preventive-shut-down-close-case-update-backend.php?routine=<?php printf($codigoget);?>" method="post">
                           
                           
                           <div class="item form-group">
                              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Programada</label>
                              <div class="col-md-6 col-sm-6 ">
                                 <input id="programada_data" class="form-control" type="date" name="programada_data"  value="<?php printf($programada); ?>"  >
                              </div>
                           </div>
                           
                           <div class="item form-group">
                              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Abertura</label>
                              <div class="col-md-6 col-sm-6 ">
                                 <input id="programada_open" class="form-control" type="date" name="programada_open"  value="<?php printf($date_start_ms); ?>" >
                              </div>
                           </div>
                           
                           <div class="item form-group">
                              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Fechamento</label>
                              <div class="col-md-6 col-sm-6 ">
                                 <input id="programada_close" class="form-control" type="date" name="programada_close"  value="<?php printf($date_end); ?>"  >
                              </div>
                           </div>
                                                
                           
                           
                           </div>
                           <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                              <button type="submit" class="btn btn-primary">Salvar Informações</button>
                           </div>
                           
                           </div>
                           </div>
                           </div>
                        </form>
                        <!-- Registro -->
              <div class="x_panel">
               <div class="x_title">
                 <h2>Cadastro</h2>
                 <ul class="nav navbar-right panel_toolbox">
                     <li> <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2" ><i class="fa fa-plus"></i></a>
                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                   </li>
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                         class="fa fa-wrench"></i></a>
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Settings 1</a>
                       </li>
                       <li><a href="#">Settings 2</a>
                       </li>
                     </ul>
                   </li>
                   <li><a class="close-link"><i class="fa fa-close"></i></a>
                   </li>
                 </ul>
                 <div class="clearfix"></div>
               </div>
               <div class="x_content">

               <!--arquivo -->
      <?php
$query="SELECT regdate_mp_registro.id,colaborador.ultimonome,regdate_mp_registro.id_mp, regdate_mp_registro.id_user,regdate_mp_registro.date_start,regdate_mp_registro.date_end,regdate_mp_registro.time,regdate_mp_registro.reg_date, regdate_mp_registro.upgrade, colaborador.id, colaborador.primeironome FROM regdate_mp_registro INNER JOIN colaborador on regdate_mp_registro.id_user = colaborador.id WHERE regdate_mp_registro.id_mp like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
   $stmt->execute();
   $stmt->bind_result($regdate_mp_registro,$ultimonome,$id_mp,$id_user,$date_start,$date_end,$time,$reg_date,$upgrade,$id,$usuario);

?>
                   <table class="table table-striped">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Executante</th>
                         <th>Data de inicio</th>
                         <th>Data de Termino</th>
                         <th>Tempo</th>
                         <th>Ação</th>


                       </tr>
                       </thead>
                       <tbody>
                          <?php  while ($stmt->fetch()) { ?>

                       <tr>
                       <th scope="row"><?php printf($row); ?></th>
                       <td><?php printf($usuario); ?> <?php printf($ultimonome); ?></td>
                       <td><?php printf($date_start); ?></td>
                       <td><?php printf($date_end); ?></td>
                        <td><?php printf($time); ?></td>
                        <td><a class="btn btn-app"      onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/maintenance-preventive-edit-case-register-drop-backend?id=<?php printf($regdate_mp_registro); ?>&routine=<?php printf($codigoget); ?> ';
  }
})
">
                       <i class="fa fa-trash"></i> Excluir
                       </a> </td>

                       </tr>
                  <?php  $row=$row+1; }
}   ?>
                     </tbody>
                   </table>




               </div>
             </div>

               <!-- Registro -->
              <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
                   <div class="modal-dialog modal-lg">
                     <div class="modal-content">

                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel2">Registro</h4>
                         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                         </button>
                       </div>
                       <div class="modal-body">
                       <!-- Registro forms-->
                         <form action="backend/maintenance-preventive-shut-down-register-case-backend.php?routine=<?php printf($codigoget);?>" method="post">



                             <div class="item form-group">
                       <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Colaborador</label>
                       <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="colaborador" id="colaborador"  placeholder="Colaborador"



                     ?>
                       <?php
                        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE  trash != 0 and  id_categoria like '%$id_category%' ";
                                           if ($stmt = $conn->prepare($sql)) {
                                       $stmt->execute();
                                           $stmt->bind_result($id,$primeironome,$ultimonome);
                                            while ($stmt->fetch()) {
                                           ?>
                           <option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
                       <?php
                     // tira o resultado da busca da memória
                     }

                                           }
                     $stmt->close();

                     ?>
                                       </select>
                           </div>
                           </div>
                           <script>
                                   $(document).ready(function() {
                                   $('#').select2();
                                     });
                           </script>

                          <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Data Inicial<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input   type="date" name="date_start"  id="date_start"required='required' class="form-control"></div>
                        <span class="input-group-btn">
                                             <button type="button" onclick="dateStartmp()"class="btn btn-primary">Utilizar Data de Abertura</button>
                                         </span>
                    </div>

                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Data Final<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input   type="date" name="date_end" id="date_end"required='required' class="form-control"></div>
                        <span class="input-group-btn">
                                             <button type="button" onclick="dateStart()"class="btn btn-primary">Utilizar Data de Inicio</button>
                                         </span>
                    </div>

                 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="time" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Tempo <span></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="time" name="time"  class="form-control "   pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99">
                          <span class="input-group-btn">
                                               <button type="button" onclick="datetimemp()"class="btn btn-primary">Utilizar Tempo da Rotina</button>
                                           </span>
                        </div>
                      </div>




                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                         <button type="submit" class="btn btn-primary">Salvar Informações</button>
                       </div>

                     </div>
                   </div>
                 </div>
             </form>
             <!-- Registro -->



   <div class="x_panel">
               <div class="x_title">
                 <h2>Anexos</h2>
                 <ul class="nav navbar-right panel_toolbox">
                     <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg3" ><i class="fa fa-plus"></i></a>
                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                   </li>
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                         class="fa fa-wrench"></i></a>
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Settings 1</a>
                       </li>
                       <li><a href="#">Settings 2</a>
                       </li>
                     </ul>
                   </li>
                   <li><a class="close-link"><i class="fa fa-close"></i></a>
                   </li>
                 </ul>
                 <div class="clearfix"></div>
               </div>
               <div class="x_content">

               <!--arquivo -->
      <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_mp_dropzone WHERE id_mp like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
   $stmt->execute();
   $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
                   <table class="table table-striped">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>#</th>
                         <th>Titulo</th>
                         <th>Registro</th>
                         <th>Atualização</th>
                        <th>Ação</th>

                       </tr>
                     </thead>
                     <tbody>
                            <?php  while ($stmt->fetch()) { ?>

                       <tr>
                         <th scope="row"><?php printf($row); ?></th>
                         <td><?php printf($id); ?></td>
                         <td><?php printf($titulo); ?></td>
                         <td><?php printf($reg_date); ?></td>
                         <td><?php printf($upgrade); ?></td>
                        <td> <a class="btn btn-app"   href="dropzone/mp-dropzone/<?php printf($file); ?> " download  onclick="new PNotify({
                               title: 'Visualizar',
                               text: 'Visualizar Anexo',
                               type: 'info',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-file"></i> Anexo
                 </a>  <a class="btn btn-app"      onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/maintenance-preventive-edit-case-dropzone-drop-backend?id=<?php printf($id); ?>&routine=<?php printf($codigoget); ?>';
  }
})
">
              <i class="fa fa-trash"></i> Excluir
            </a>
               </td>

                       </tr>
                  <?php  $row=$row+1; }
}   ?>
                     </tbody>
                   </table>




               </div>
             </div>

               <!-- Registro -->
              <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
                   <div class="modal-dialog modal-lg">
                     <div class="modal-content">

                       <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                         </button>
                       </div>

                       <div class="modal-body">

                       <!-- Registro forms-->
                         <form action="backend/regdate_mp_dropzone-case-backend.php?id_mp=<?php printf($codigoget);?>" method="post">
                            <div class="ln_solid"></div>


                       <div class="field item form-group">
                     <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                         class="required">*</span></label>
                     <div class="col-md-6 col-sm-6">
                       <input class="form-control"  type="text" name="titulo" required='required'></div>
                   </div>




                       <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                         <button type="submit" class="btn btn-primary">Salvar Informações</button>

                       </div>
                </div>
            </form>
             <form  class="dropzone" action="backend/regdate_mp_dropzone-upload-backend.php" method="post">
   </form >
    </div>
                   </div>
                 </div>
             <!-- Registro -->


<div class="clearfix"></div>

           <div class="row">
             <div class="col-md-12">
               <div class="x_panel">
                 <div class="x_title">
                   <h2>Anexo <small> </small></h2>
                   <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="#">Settings 1</a>
                           <a class="dropdown-item" href="#">Settings 2</a>
                         </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                   </ul>
                   <div class="clearfix"></div>
                 </div>
                 <div class="x_content">

                   <div class="row">



                  <form  class="dropzone" action="backend/maintenance-preventive-shut-down-upload-backend.php" method="post">
   </form >




                   </div>
                 </div>
               </div>
             </div>
           </div>

           <div class="row">
             <div class="col-md-12">
               <div class="x_panel">
                 <div class="x_title">
                   <h2>Anexo salvo<small> </small></h2>
                   <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="#">Settings 1</a>
                           <a class="dropdown-item" href="#">Settings 2</a>
                         </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                   </ul>
                   <div class="clearfix"></div>
                 </div>
                 <div class="x_content">

                   <div class="row">
                     <div class="col-md-55">
                         <?php if($id_anexo ==! ""){ ?>
                       <div class="thumbnail">
                         <div class="image view view-first">

                           <img style="width: 50%; display: block;" src="dropzone/mp/<?php printf($id_anexo); ?>" alt="image" />
                           <div class="mask">
                             <p>Anexo</p>
                             <div class="tools tools-bottom">
                               <a href="dropzone/mp/<?php printf($id_anexo); ?>" download><i class="fa fa-download"></i></a>

                             </div>
                           </div>
                         </div>
                         <div class="caption">
                           <p>Anexo </p>
                         </div>
                       </div>
                        <?php }; ?>
                     </div>







                   </div>
                 </div>
               </div>
             </div>
           </div>

           <div class="clearfix"></div>

             <div class="x_panel">
               <div class="x_title">
                 <h2>Fechamento de Manutenção preventiva</h2>
                 <ul class="nav navbar-right panel_toolbox">
                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                   </li>
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                         class="fa fa-wrench"></i></a>
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Settings 1</a>
                       </li>
                       <li><a href="#">Settings 2</a>
                       </li>
                     </ul>
                   </li>
                   <li><a class="close-link"><i class="fa fa-close"></i></a>
                   </li>
                 </ul>
                 <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  <form  action="backend/maintenance-preventive-shut-down-update-backend.php?id=<?php printf($codigoget);?>" method="post">

           <div class="item form-group">
                       <label class="col-form-label col-md-3 col-sm-3 label-align" for="fornecedor" class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">Fornecedor <span aria-hidden="true"></span>
                       </label>
                       <div class="col-md-6 col-sm-6 ">
                         <select type="text" class="form-control has-feedback-right" name="fornecedor" id="fornecedor"  placeholder="Fornecedor" value="<?php printf($id_fornecedor); ?>" >
       <option> Selecione um Fornecedor	</option>
                       <?php



                      $sql = "SELECT  id, empresa FROM fornecedor WHERE trash =1 ";


if ($stmt = $conn->prepare($sql)) {
   $stmt->execute();
    $stmt->bind_result($id_empresa,$empresa);
    while ($stmt->fetch()) {
    ?>
<option value="<?php printf($id_empresa);?>	"<?php if($id_empresa==$id_fornecedor){printf("selected");}; ?>><?php printf($empresa);?>	</option>
                       <?php
                     // tira o resultado da busca da memória
                     }

                                           }
                     $stmt->close();

                     ?>
                                       </select>
                       </div>
                     </div>









                <script>
                                   $(document).ready(function() {
                                   $('#fornecedor').select2();
                                     });
                                </script>




                               <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Inicial<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input   type="date" name="date_start" id="date_startsecond"required='required'value="<?php printf($date_start_ms); ?>" class="form-control"></div>
                        <span class="input-group-btn">
                                             <button type="button"  onclick="dateStartmpsecond()" class="btn btn-primary">Utilizar Data de Abertura</button>
                                         </span>
                    </div>

                      <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Final<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input   type="date" name="date_end" id="date_endsecond" required='required'value="<?php printf($date_end_ms); ?>" class="form-control"></div>
                        <span class="input-group-btn">
                                             <button type="button"  onclick="dateStartsecond()" class="btn btn-primary">Utilizar Data de Inicio</button>
                                         </span>
                    </div>

                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="backlog" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Tempo de Execução <span></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="timepicker1" name="time"  class="form-control " value="<?php printf($time_mp); ?>"  pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99">
                          <span class="input-group-btn">
                                               <button type="button" onclick="datetimempsecond()"class="btn btn-primary">Utilizar Tempo da Rotina</button>
                                           </span>
                        </div>
                      </div>



                     <div class="item form-group">
                       <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Colaborador</label>
                       <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="colaborador" id="colaborador2"  placeholder="Colaborador">



                       <option> Selecione um colaborador	</option>
                       <?php
                        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE trash =1 and id_categoria like '%$id_category%'";
                                           if ($stmt = $conn->prepare($sql)) {
                                       $stmt->execute();
                                           $stmt->bind_result($id,$primeironome,$ultimonome);
                                            while ($stmt->fetch()) {
                                           ?>
                           <option value="<?php printf($id);?>	" <?php if($id==$id_tecnico){printf("selected");}; ?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
                       <?php
                     // tira o resultado da busca da memória
                     }

                                           }
                     $stmt->close();

                     ?>
                                       </select>
                           </div>
                           </div>
                           <script>
                                   $(document).ready(function() {
                                   $('#colaborador2').select2();
                                     });
                           </script>




                         <label for="message_mp">Observação de MP :</label>
                         <textarea id="message_mp" class="form-control" name="message_mp" data-parsley-trigger="keyup"
                           data-parsley-validation-threshold="10" placeholder="<?php printf($obs_mp); ?>" value="<?php printf($obs_mp); ?>"><?php printf($obs_mp); ?><?php printf($obs_mp); ?></textarea>

                      <label for="message_tc">Observação Técnica :</label>
                         <textarea id="message_tc" class="form-control" name="message_tc" data-parsley-trigger="keyup"
                           data-parsley-validation-threshold="10" placeholder="<?php printf($obs_tc); ?>"value="<?php printf($obs_tc); ?>"><?php printf($obs_tc); ?><?php printf($obs_tc); ?></textarea>

                            <div class="item form-group">
                       <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome"> <span class="required"></span>
                       </label>
                       <div class="col-md-6 col-sm-6 ">
                         <input type="text" id="routine" name="routine" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control " hidden>
                       </div>
                     </div>
                     <div class="item form-group">
                       <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                       <div class="col-md-6 col-sm-6 ">
                         <input id="periodicidade" class="form-control" type="text" name="periodicidade"  value="<?php printf($periodicidade); ?>" readonly="readonly" hidden>
                       </div>
                     </div>

                   <div class="item form-group">
                       <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria"><span class="required"></span>
                       </label>
                       <div class="col-md-6 col-sm-6 ">
                         <input type="text" id="programada" name="programada" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly"hidden>
                       </div>
                     </div>

                     <div class="ln_solid"></div>

                     <?php if($mp == "0"){ ?> 

<div class="item form-group ">

<label  >Temperatura  </label>

    <input name="temp" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#26B99A" data-skin="tron" data-thickness=".2" value="<?php printf($temp); ?>" data-step=".1">
 <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($temp); ?> " > -->


<label >Umidade  </label>

    <input name="hum" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#34495E" data-skin="tron" data-thickness=".2" value="<?php printf($hum); ?>" data-step=".1">
 <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($hum); ?> " > -->

</div>

  <?php }?> 




            <div class="ln_solid"></div>
                     <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 "></label>
                       <div class="col-md-3 col-sm-3 ">
                           <center>
                          <button class="btn btn-sm btn-success" type="submit">Atualizar Informações</button>
                        </center>
                       </div>
                     </div>


                 </form>
               </div>
             </div>

             <?php if($digital == "0"){
?> 
<div class="x_panel">
<div class="x_title">
  <h2>Procedimento Digital</h2>
  <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    </li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
          class="fa fa-wrench"></i></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="#">Settings 1</a>
        </li>
        <li><a href="#">Settings 2</a>
        </li>
      </ul>
    </li>
    <li><a class="close-link"><i class="fa fa-close"></i></a>
    </li>
  </ul>
  <div class="clearfix"></div>
</div>
<div class="x_content">
<div class="ln_solid"></div>
<table class="table table-striped jambo_table bulk_action" id="tabela">

<?php echo $tabela;  ?> 
</tbody>
</table>
    <div class="ln_solid"></div>
</div>
</div>
    <?php
  }
  ?> 


             <?php if($signature_digital == "0"){ ?> 

              <div class="x_panel">
                <div class="x_title">
                  <h2>Assinatura</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                <!--arquivo -->
       <?php    
$query="SELECT regdate_mp_signature.id,regdate_mp_signature.id_mp,regdate_mp_signature.id_user,regdate_mp_signature.id_signature,regdate_mp_signature.reg_date,usuario.nome FROM regdate_mp_signature INNER JOIN usuario ON regdate_mp_signature.id_user = usuario.id   WHERE regdate_mp_signature.id_mp like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$id_mp,$id_user,$id_signature,$reg_date,$id_user);
  
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th>Usuário</th>
                          <th>Assinatura</th>
                           <th>Registro</th>
                     <th>Ação</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id_user); ?></td>
                          <td><?php printf($id_signature); ?></td>
                          <td><?php printf($reg_date); ?></td>
                           <td>                                                                <a class="btn btn-app" onclick="
                              Swal.fire({
                                 title: 'Tem certeza?',
                                 text: 'Você não será capaz de reverter isso!',
                                 icon: 'warning',
                                 showCancelButton: true,
                                 confirmButtonColor: '#3085d6',
                                 cancelButtonColor: '#d33',
                                 confirmButtonText: 'Sim, Deletar!'
                              }).then((result) => {
                                 if (result.isConfirmed) {
                                    Swal.fire(
                                       'Deletando!',
                                       'Seu arquivo será excluído.',
                                       'success'
                                    ),
window.location = 'backend/signature-maintenance-preventive-trash-backend.php?id=<?php printf($id); ?>&page=<?php printf($codigoget); ?>';
}
                              })
                           ">
                              <i class="fa fa-trash"></i> Excluir
                           </a>
                           </td>

                         
                          
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

                
              
                
                </div>
              </div>  
                <!-- compose -->
                <?php  
}   ?>
   <div class="x_panel">
                                                <div class="x_title">
                                                  <h2>Avaliação Fornecedor</h2>
                                                  <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                                        class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                          <li><a href="#">Settings 1</a>
                                                          </li>
                                                          <li><a href="#">Settings 2</a>
                                                          </li>
                                                        </ul>
                                                      </li>
                                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                      </li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                  </div>
                                                  <div class="x_content">

                                                  
                <!--arquivo -->
       <?php    
$query="SELECT fornecedor.empresa,fornecedor_assessment.reg_date, fornecedor_question.question, fornecedor_assessment_question.id_question,fornecedor_assessment_question.avaliable, fornecedor_service.service, fornecedor_assessment.id_service,fornecedor_assessment.assessment,fornecedor_assessment.number_assessment, fornecedor_assessment.data_assessment, fornecedor_assessment.obs FROM fornecedor_assessment LEFT JOIN fornecedor_service ON fornecedor_service.id = fornecedor_assessment.id_service LEFT JOIN fornecedor_assessment_question ON fornecedor_assessment_question.id_assessment = fornecedor_assessment.id LEFT JOIN fornecedor_question ON fornecedor_question.id = fornecedor_assessment_question.id_question LEFT JOIN fornecedor ON fornecedor.id = fornecedor_assessment.id_fornecedor WHERE fornecedor_assessment.number_assessment = $codigoget and  fornecedor_assessment.assessment = 2 ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($empresa,$reg_date,$question,$id_question,$avaliable,$service,$id_service,$assessment,$number_assessment,$data_assessment,$obs);
  
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th>Fornecedor</th>
            <th>Fornecimento</th>
            <th>Data avaliação</th>
            <th>Data do Serviço avaliado</th>
            <th>Tipo do serviço</th>
            <th>Numero do serviço</th>
            <th>Avaliação</th>
            <th>Conceito</th>
            <th>Observação</th>

                        
                        
                        
                        
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($empresa); ?> </td>
              <td><?php printf($service); ?></td>
              <td><?php printf($reg_date); ?> </td>
              <td><?php printf($data_assessment); ?> </td>
              <td><?php  switch ($assessment) {
                case '1':
                print("Ordem de Serviço");
                break;
                case '2':
                print("Manutenção Preventiva");
                break;
                case '3':
                print("Calibração");
                break;


              } ?></td>
              <td><?php printf($number_assessment); ?></td>
              <td><?php printf($question); ?></td>
              <td><?php switch ($avaliable) {
                case '1':
                print("Bom");
                break;
                case '2':
                print("Regular");
                break;
                case '3':
                print("Ruim");
                break; }
                ?></td>
                <td><?php printf($obs); ?> </td>
                         
                         
                          
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

                                          

                                        </div>
                                      </div>
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="maintenance-preventive-close-edit?routine=<?php printf($rotina); ?>">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




                </div>
              </div>




                </div>
              </div>
              <!-- Contador de caracter -->
          <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

          <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
                 <script type="text/javascript">
$("meuPrimeiroDropzone").dropzone({ url: "upload.php" });
		 Dropzone.options.meuPrimeiroDropzone = {
   paramName: "fileToUpload",
   dictDefaultMessage: "Arraste seus arquivos para cá!",
   maxFilesize: 300,
   accept: function(file, done) {
    if (file.name == "olamundo.png") {
       done("Arquivo não aceito.");
   } else {
     done();
   }
 }
 }
		</script>
    <script>
function dateStartmpsecond() {

  //document.getElementById("date_start").innerHTML ="2022-04-18";
  //  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
  //  $('#date_start').val(date_start);
   var date_start = document.getElementById("programada_data").value;
//  var  date_start = +$('#programada_data').val(programada_data);
  $('#date_startsecond').val(date_start);
      //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



}
function dateStartsecond() {
 var date_end = document.getElementById("date_startsecond").value;
//  var  date_end = +$('#date_start').val(date_start);

  $('#date_endsecond').val(date_end);
}
</script>
<script>
function dateStartmp() {

//document.getElementById("date_start").innerHTML ="2022-04-18";
//  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
//  $('#date_start').val(date_start);
var date_start = document.getElementById("programada_data").value;
//  var  date_start = +$('#programada_data').val(programada_data);
$('#date_start').val(date_start);
  //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



}
function dateStart() {
var date_end = document.getElementById("date_start").value;
//  var  date_end = +$('#date_start').val(date_start);

$('#date_end').val(date_end);
}
</script>
<script>
function datetimemp() {

//document.getElementById("date_start").innerHTML ="2022-04-18";
//  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
//  $('#date_start').val(date_start);
var time_ms = document.getElementById("time_ms").value;
//  var  date_start = +$('#programada_data').val(programada_data);
$('#time').val(time_ms);
  //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



}
function datetimempsecond() {
var time_ms = document.getElementById("time_ms").value;
//  var  date_end = +$('#date_start').val(date_start);

$('#timepicker1').val(time_ms);
}
</script>
