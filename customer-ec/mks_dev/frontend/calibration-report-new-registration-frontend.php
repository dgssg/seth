<?php


// Create connection
include("database/database.php");// remover ../


?>
<form action="backend/calibration-report-new-backend.php" method="post">

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Realizado</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="data_start" class="form-control" type="date" name="data_start"  >
    </div>
  </div>

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Validade (Meses)</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="date_validation" class="form-control" type="number" name="val"  >
    </div>
  </div>



  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  >
        <option value="	">Selecione a Empresa</option>
        <?php



        $sql = "SELECT  id, nome FROM calibration_parameter_manufacture ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$nome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
            <?php
            // tira o resultado da busca da memória
          }

        }
        $stmt->close();

        ?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_manufacture').select2();
  });
  </script>


<div class="ln_solid"></div>

<div class="field item form-group">
                      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Empresa <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="unidade" id="unidade"  placeholder="Unidade">
										<?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                   <?php
                      

   $result_cat_post  = "SELECT instituicao.id, instituicao.instituicao FROM instituicao WHERE trash = 1 ";

   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
             while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
               echo '<option></option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
             }
                    ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma unidade "></span>

                  </div>

                  </div>

								 <script>
                                    $(document).ready(function() {
                                    $('#unidade').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Cliente <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Setor">
										  <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Setor "></span>

                  </div>
                  </div>
                            

								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>

<div class="field item form-group">
                      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Localização <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Area <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
                    <option value="">Selecione uma Opção</option>   
                  </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Area "></span>

                    </div>

                    </div>
                                
								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>
                                 

                                 <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Equipamento<span
                       >*</span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
										  <option value="">Selecione o equipamento</option>
                      <?php


					$query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
                                       	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>
                  </div>


								      		    
                  </div>
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento').select2();
                                      });
                                 </script>


  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo Externo</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="codigo" class="form-control" type="text" name="codigo"  >
    </div>
  </div>






  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observa&ccedil;&atilde;o de calibra&ccedil;&atilde;o</label>
    <div class="col-md-4 col-sm-4 ">


      <textarea  rows="5" cols="5" id="obs"  class="form-control" name="obs" data-parsley-trigger="keyup"
      data-parsley-validation-threshold="2" placeholder="Este certificado &#233; valido somente para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar."  value="Este certificado &#233; valido somente para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar."></textarea>

    </div>
  </div>


  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Colaborador <span class="required" >*</span>
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador" value="<?php printf($id_tecnico); ?>">
        <option value="	">Selecione o Executante</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_colaborador').select2();
  });
  </script>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Responsável ">Responsável Tecnico <span class="required" >*</span>
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="id_responsavel" id="id_responsavel"  placeholder="Responsavel" value="<?php printf($id_tecnico); ?>">
        <option value="	">Selecione o Responsável</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_responsavel').select2();
  });
  </script>

  <div class="ln_solid"></div>

  <div class="item form-group ">

    <label  >Temperatura  </label>

    <input name="temp" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#26B99A" data-skin="tron" data-thickness=".2" value="25.5" data-step=".1">
    <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($carga); ?> " > -->


    <label >Umidade  </label>

    <input name="hum" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#34495E" data-skin="tron" data-thickness=".2" value="65.5" data-step=".1">
    <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($carga); ?> " > -->

  </div>
  <div class="ln_solid"></div>
  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 "></label>
    <div class="col-md-3 col-sm-3 ">
      <center>
        <button id="compose" class="btn btn-sm btn-success btn-block" class="form-control "type="button">Observa&ccedil;&atilde;o</button>
      </center>
    </div>
  </div>


  <button type="reset" onclick="clean()" class="btn btn-primary"onclick="new PNotify({
    title: 'Limpado',
    text: 'Todos os Campos Limpos',
    type: 'info',
    styling: 'bootstrap3'
  });" />Limpar</button>
  <input type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informa&ccedil;&atilde;es registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" value="Prosseguir"/>

</form>
<div class="ln_solid"></div>
<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<!-- compose -->
<div class="compose col-md-6  ">
  <div class="compose-header">
    Observa&ccedil;&atilde;o
    <button type="button" class="close compose-close">
      <span>X</span>
    </button>
  </div>
  <form  action="backend/" method="post">
    <div class="compose-body">
      <div id="alerts"></div>


      <input size="90" type="text" id="editor" name="editor" class="editor-wrapper" value="Este certificado &eacute; v&aacute;lido para o equipamento ensaiado, n&atilde;o sendo extensivo para quaisquer lotes, mesmo similar.">


    </div>

    <div class="compose-footer">
      <button class="btn btn-sm btn-success" type="submit">Salvar</button>
    </div>
  </form>
</div>
<!-- /compose -->
<script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>
    								<script>
function clean() {
  $('#unidade').select2().load('reset_unidade.php');
  $('#area').select2().load('reset_area.php');
  $('#setor').select2().load('reset_setor.php');
  $('#equipamento').select2().load('reset_equipamento.php');


}

</script>

