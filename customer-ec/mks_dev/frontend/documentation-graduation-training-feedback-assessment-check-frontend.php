<?php
  include("database/database.php");
  
  $codigoget = ($_GET["id"]);
  
  $query = "SELECT user_training_attempts.id,usuario.nome,usuario.sobrenome,user_training_attempts.attempt_date,user_training_attempts.score,user_training_attempts.status,documentation_graduation.titulo FROM user_training_attempts LEFT JOIN  documentation_graduation ON user_training_attempts.id_training = documentation_graduation.id LEFT JOIN category ON category.id = documentation_graduation.id_categoria LEFT JOIN equipamento_familia ON equipamento_familia.id = documentation_graduation.id_equipamento_familia LEFT JOIN usuario ON usuario.id = user_training_attempts.id_user where user_training_attempts.id = $codigoget ";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$nome,$sobrenome,$attempt_date,$score,$status,$titulo);
    
    
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }

$query = "SELECT documentation_graduation.assessment,documentation_graduation.id,documentation_graduation.titulo,documentation_graduation.ativo,documentation_graduation.upgrade,documentation_graduation.reg_date,category.id,equipamento_familia.id,documentation_graduation.dropzone  FROM documentation_graduation  LEFT JOIN category ON category.id = documentation_graduation.id_categoria LEFT JOIN equipamento_familia ON equipamento_familia.id = documentation_graduation.id_equipamento_familia WHERE documentation_graduation.id = $id";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($assessment,$id,$titulo,$ativo,$upgrade,$reg_date,$category,$equipamento_familia,$dropzone);


   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
  
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }

  
function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
  $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
  $con = 1;
  $var = '';
  while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
  }else{
      $var = $var.$n.$res[$n];
      $con++;
      }
  }
      $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
 // echo chaveAlfaNumerica(5);
//   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = md5($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 3
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function v4() {
return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

// 32 bits for "time_low"
mt_rand(0, 0xffff), mt_rand(0, 0xffff),

// 16 bits for "time_mid"
mt_rand(0, 0xffff),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand(0, 0x0fff) | 0x4000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand(0, 0x3fff) | 0x8000,

// 48 bits for "node"
mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
);
}

public static function v5($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = sha1($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function is_valid($uuid) {
return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();

//echo $v4uuid; //chave da assinatura
?>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Avaliação <small>Educação Continuada</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Educação <small>Continuada</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

 
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($codigoget); ?> " readonly >
                        </div>
                      </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input  class="form-control" type="text"  id="titulo" name="titulo"  value="<?php printf($titulo); ?>" readonly>
                      </div>
                    </div>
                                     
                   
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly" readonly>
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly" readonly>
                        </div>
                      </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Link</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input  class="form-control" type="text"  id="dropzone" name="dropzone"  value="<?php printf($dropzone); ?>" readonly >
                      </div>
                    </div>
                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                            
                         </center>
                        </div>
                      </div>


                                   



                  </div>
                </div>
              </div>
	       </div>


    
            <div class="clearfix"></div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Video</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div id="ytplayer"></div>
              
              <script>
                var video = document.getElementById('dropzone').value;
                
                // Load the IFrame Player API code asynchronously.
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/player_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                
                // Replace the 'ytplayer' element with an <iframe> and
                // YouTube player after the API code downloads.
                var player;
                function onYouTubePlayerAPIReady() {
                  player = new YT.Player('ytplayer', {
                    height: '360',
                    width: '640',
                    videoId: video
                  });
                }
              </script>
         
              

                              
              </div>
            </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Filtro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a class="dropdown-item" href="#">Settings 1</a>
                        </li>
                        <li><a class="dropdown-item" href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="userstable_filter" style="column-count:3; -moz-column-count:3; -webkit-column-count:3; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                  
                  
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="x_panel">
            <div class="x_title">
              <h2>Educação Continuada</h2>
              <ul class="nav navbar-right panel_toolbox">
                
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                    class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-box table-responsive">
                    
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Pergunta</th>
                          <th>Resposta</th>
                          <th>Aberta</th>
                          <th>Correto</th>
           
                          
                          
                          
                          
                          <th>Ação</th>
                          
                        </tr>
                      </thead>
                      
                      
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>   

<div class="clearfix"></div>
             



                </div>
              </div>

          <script language="JavaScript">
            $(document).ready(function() {
              
              $('#datatable').dataTable( {
                "processing": true,
                "stateSave": true,
                responsive: true,
                
                
                
                "language": {
                  "loadingRecords": "Carregando dados...",
                  "processing": "Processando  dados...",
                  "infoEmpty": "Nenhum dado a mostrar",
                  "emptyTable": "Sem dados disponíveis na tabela",
                  "zeroRecords": "Não há registros a serem exibidos",
                  "search": "Filtrar registros:",
                  "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoFiltered": " - filtragem de _MAX_ registros",
                  "lengthMenu": "Mostrar _MENU_ registros",
                  
                  "paginate": {
                    "previous": "Página anterior",
                    "next": "Próxima página",
                    "last": "Última página",
                    "first": "Primeira página",
                    
                    
                    
                  }
                }
                
                
                
              } );
              
              
              // Define a URL da API que retorna os dados da query em formato JSON
              const apiUrl = 'table/table-search-graduation-training-feedback-check.php?id=<?php printf($codigoget); ?>';
              
              
              
              // Usa a função fetch() para obter os dados da API em formato JSON
              fetch(apiUrl)
              .then(response => response.json())
              .then(data => {
                // Mapeia os dados para o formato esperado pelo DataTables
                const novosDados = data.map(item => [
                  `
`,
                  item.question, 
                  item.user_response, 
                  item.answer, 
                  item.is_correct == "1" ? "Sim" : "Não",

                  
                  
                  
                  
                  `
                                                        <a class="btn btn-app" href="backend/documentation-graduation-training-feedback-assessment-true-backend?id=${item.id}">
  <i class="fa fa-check-circle text-success"></i> Certo
</a>

<a class="btn btn-app" href="backend/documentation-graduation-training-feedback-assessment-false-backend?id=${item.id}">
  <i class="fa fa-times-circle text-danger"></i> Errado
</a>
    
                        `
                ]);
                
                // Inicializa o DataTables com os novos dados
                const table = $('#datatable').DataTable();
                table.clear().rows.add(novosDados).draw();
                
                
                // Cria os filtros após a tabela ser populada
                $('#datatable').DataTable().columns([1, 2,3]).every(function(d) {
                  var column = this;
                  var theadname = $("#datatable th").eq([d]).text();
                  // Container para o título, o select e o alerta de filtro
                  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
                  
                  // Título acima do select
                  var title = $('<label>' + theadname + '</label>').appendTo(container);
                  
                  // Container para o select
                  var selectContainer = $('<div></div>').appendTo(container);
                  
                  var select = $('<select class="form-control my-1"><option value="">' +
                    theadname + '</option></select>').appendTo(selectContainer).select2()
                  .on('change', function() {
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column.search(val ? '^' + val + '$' : '', true, false).draw();
                    
                    // Remove qualquer alerta existente
                    container.find('.filter-alert').remove();
                    
                    // Se um valor for selecionado, adicionar o alerta de filtro
                    if (val) {
                      $('<div class="filter-alert">' +
                        '<span class="filter-active-indicator">&#x25CF;</span>' +
                        '<span class="filter-active-message">Filtro ativo</span>' +
                        '</div>').appendTo(container);
                    }
                    
                    // Remove o indicador do título da coluna
                    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
                    
                    // Se um valor for selecionado, adicionar o indicador no título da coluna
                    if (val) {
                      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
                    }
                  });
                  
                  column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                  });
                  // Verificar se há filtros aplicados ao carregar a página
                  var filterValue = column.search();
                  if (filterValue) {
                    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
                  }
                });
                
                // Adicionar a configuração inicial da DataTable
                table.on('init.dt', function() {
                  // Verifica se há filtros aplicados ao carregar a página
                  table.columns().every(function() {
                    var column = this;
                    var searchValue = column.search();
                    if (searchValue) {
                      var select = $('#userstable_filter select').eq(column.index());
                      select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
                    }
                  });
                });
                
                var oTable = $('#datatable').dataTable();
                
                // Avança para a próxima página da tabela
                //oTable.fnPageChange('next');
                
                
                
              });
              
              
              // Armazenar a posição atual ao sair da página
              $(window).on('beforeunload', function() {
                var table = $('#datatable').DataTable();
                var pageInfo = table.page.info();
                localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
              });
              
              // Restaurar a posição ao retornar à página
              $(document).ready(function() {
                var storedPosition = localStorage.getItem('paginationPosition');
                if (storedPosition) {
                  var pageInfo = JSON.parse(storedPosition);
                  var table = $('#datatable').DataTable();
                  table.page(pageInfo.page).draw('page');
                }
              });
              
              
            });
            
            
          </script>   
          
          