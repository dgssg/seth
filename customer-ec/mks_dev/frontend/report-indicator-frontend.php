<?php

include("database/database.php");






?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Indicador <small> do sistema</small></h3>
            </div>


        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2>Menu</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <a class="btn btn-app" href="report-indicator">
                    <i class="fa fa-bar-chart"></i> Indicador
                </a>

                <a class="btn btn-app" href="report-indicator-metas">
                    <i class="fa fa-line-chart"></i> Metas
                </a>




            </div>
        </div>

        <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tabela <small>de Indicador</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Settings 1</a>
                                    <a class="dropdown-item" href="#">Settings 2</a>
                                </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!-- start accordion -->
                        <div class="accordion" aria-multiselectable="true">
                            <div class="panel">

                                <a class="panel-heading" href="report-indicator-mc">
                                    <h4 class="panel-title">Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>


                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mp">
                                    <h4 class="panel-title">Manuten&ccedil;&atilde;o Preventiva</h4>
                                </a>

                            </div>

                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-calibration">
                                    <h4 class="panel-title">Calibração</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mc-sla">
                                    <h4 class="panel-title">Prazo Atendimento de Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mc-first-sla">
                                    <h4 class="panel-title">Primeiro Atendimento de Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mc-assessment">
                                    <h4 class="panel-title">Pesquisa de Atendimento de Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mc-prioridade">
                                    <h4 class="panel-title">Prioridade de Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mc-service">
                                    <h4 class="panel-title">Serviço de Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mc-category">
                                    <h4 class="panel-title">Categoria de Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mp-category">
                                    <h4 class="panel-title">Categoria de Manuten&ccedil;&atilde;o Preventiva</h4>
                                </a>

                            </div>
                            <div class="panel">
                                <a class="panel-heading" href="report-indicator-mc-defeito">
                                    <h4 class="panel-title">Defeito de Manuten&ccedil;&atilde;o Corretiva</h4>
                                </a>

                            </div>


                            <!-- novos-->
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-1" >
                                    <h4 class="panel-title">Tempo de inatividade (tempo de não disponibilidade)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-2" >
                                    <h4 class="panel-title">Tempo de atividade (tempo de disponibilidade)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-3" >
                                    <h4 class="panel-title">MTTR (tempo médio para reparo)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-4" >
                                    <h4 class="panel-title">MTBF (tempo médio entre falhas)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-5" >
                                    <h4 class="panel-title">Fração de falha de classe (falha por classe)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-6" >
                                    <h4 class="panel-title">Taxa de falha global (defeito)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-7" >
                                    <h4 class="panel-title">AFR: taxa de falha por ano</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-8" >
                                    <h4 class="panel-title">Ações negligentes</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-9" >
                                    <h4 class="panel-title">Ações de 1 dia</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-10" >
                                    <h4 class="panel-title">Falha na SM </h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-11" >
                                    <h4 class="panel-title">Taxa cobertura de SM</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-12" >
                                    <h4 class="panel-title">Nenhum problema encontrado (falhas falsas) </h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-13" >
                                    <h4 class="panel-title">Quantidade de Equipamento por técnico (interno)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-14" >
                                    <h4 class="panel-title">Custo do tempo da força de trabalho</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-15" >
                                    <h4 class="panel-title">COSR ( relação custo do serviço = manutenção global ao custo de aquisição) </h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-16" >
                                    <h4 class="panel-title">Custo de manutenção externa ( em relação ao custo total de manutenção)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-17" >
                                    <h4 class="panel-title">Custo de manutenção interna (de respeito ao custo total de manutenção)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-18" >
                                    <h4 class="panel-title">Custo de manutenção corretiva (CM) ( de respeito à manutenção total)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-19" >
                                    <h4 class="panel-title">Custo de manutenção programado (SM) ( de respeito ao custo total de manutenção)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-20" >
                                    <h4 class="panel-title">Custo das peças de reposição ( consumíveis) ( de respeito à manutenção total)</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-21" >
                                    <h4 class="panel-title">Backlog Ordem de Serviço</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-22" >
                                    <h4 class="panel-title">Backlog Manutenção Preventiva</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-23" >
                                    <h4 class="panel-title">Tempo Previsto Mensal de Rotina</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-24" >
                                    <h4 class="panel-title">Tempo Previsto Mensal de em relação a tempo de fechamento Rotina</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-25" >
                                    <h4 class="panel-title">Tempo Previsto Mensal de fechamento Rotina</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-26" >
                                    <h4 class="panel-title">Acompanhamento de Manutenção Preventiva</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-kpi-27" >
                                    <h4 class="panel-title">Acompanhamento de Ordem de Serviço</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-mc-filter" >
                                    <h4 class="panel-title">Indicador de Ordem de Serviço com filtro</h4>
                                </a>
                                
                            </div>
                            <div class="panel">
                                <a class="panel-heading"  href="report-indicator-mp-filter" >
                                    <h4 class="panel-title">Indicador de Manutenção Preventiva com filtro</h4>
                                </a>
                                
                            </div>

                           <div class="panel">
                                <a class="panel-heading"  href="report-indicator-mc-action" >
                                    <h4 class="panel-title">Indicador de Manutenção Corretiva de Ação Tomada</h4>
                                </a>
                                
                            </div>



















                        </div>
                        <!-- end of accordion -->


                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tabela <small>de Indicador</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Settings 1</a>
                                    <a class="dropdown-item" href="#">Settings 2</a>
                                </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <!-- start accordion -->
                        <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                            <div class="panel">
                                <a data-toggle="modal" data-target=".bs-example-modal-lg1 " class="panel-heading"
                                    role="tab" id="headingOne1" href="#collapseOne1" aria-expanded="true"
                                    aria-controls="collapseOne">
                                    <h4 class="panel-title">Tenologico</h4>
                                </a>
                                <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel"
                                    aria-labelledby="headingOne">



                                </div>
                            </div>


                            <!-- compose -->

                            <div class="panel">
                                <a data-toggle="modal" data-target=".bs-example-modal-lg2 " class="panel-heading"
                                    role="tab" id="headingOne2" data-toggle="collapse" data-parent="#accordion1"
                                    href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Organizacional</h4>
                                </a>
                                <div id="collapseOne2" class="panel-collapse collapse in" role="tabpane1"
                                    aria-labelledby="headingOne">

                                </div>
                            </div>


                            <div class="panel">
                                <a data-toggle="modal" data-target=".bs-example-modal-lg3 " class="panel-heading"
                                    role="tab" id="headingOne3" data-toggle="collapse" data-parent="#accordion1"
                                    href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Financeiro</h4>
                                </a>
                                <div id="collapseOne3" class="panel-collapse collapse in" role="tabpane1"
                                    aria-labelledby="headingOne">

                                </div>
                            </div>


                            <div class="panel">
                                <a data-toggle="modal" data-target=".bs-example-modal-lg4 " class="panel-heading"
                                    role="tab" id="headingOne4" data-toggle="collapse" data-parent="#accordion1"
                                    href="#collapseOne4" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Backlog</h4>
                                </a>
                                <div id="collapseOne4" class="panel-collapse collapse in" role="tabpane1"
                                    aria-labelledby="headingOne">

                                </div>
                            </div>

                            <div class="panel">
                                <a data-toggle="modal" data-target=".bs-example-modal-lg5 " class="panel-heading"
                                    role="tab" id="headingOne5" data-toggle="collapse" data-parent="#accordion1"
                                    href="#collapseOne5" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Rotina</h4>
                                </a>
                                <div id="collapseOne5" class="panel-collapse collapse in" role="tabpane1"
                                    aria-labelledby="headingOne">

                                </div>
                            </div>

                            <div class="panel">
                                <a data-toggle="modal" data-target=".bs-example-modal-lg6 " class="panel-heading"
                                    role="tab" id="headingOne6" data-toggle="collapse" data-parent="#accordion1"
                                    href="#collapseOne6" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Manutenção Preventiva</h4>
                                </a>
                                <div id="collapseOne6" class="panel-collapse collapse in" role="tabpane1"
                                    aria-labelledby="headingOne">

                                </div>
                            </div>

                            <div class="panel">
                                <a data-toggle="modal" data-target=".bs-example-modal-lg7 " class="panel-heading"
                                    role="tab" id="headingOne7" data-toggle="collapse" data-parent="#accordion1"
                                    href="#collapseOne7" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Manutenção Corretiva</h4>
                                </a>
                                <div id="collapseOne7" class="panel-collapse collapse in" role="tabpane1"
                                    aria-labelledby="headingOne">

                                </div>
                            </div>



                        </div>
                        <!-- end of accordion -->


                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel4">Indicador</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Filtro</h4>
                        <!-- Registro forms-->
                        <form action="report/report-indicator.php" target="_blank" method="post">

                            <div class="item form-group">
                                <label for="middle-name"
                                    class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="yr" id="yr" placeholder="equipamento">
                                        <option value="">Selecione o ano</option>

                                    </select>


                                </div>
                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#yr').select2({
                                    dropdownParent: $("#myModal1")
                                });
                            });
                            </script>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="instituicao" id="instituicao" placeholder="Unidade" required>
                                       <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	

                                    </select>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#instituicao').select2({
                                    dropdownParent: $("#myModal1")
                                });
                            });
                            </script>

                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Familia de Equipamento <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="family" id="family" placeholder="Familia">
                                        <option value="">Selecione uma Familia</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#family').select2({
                                    dropdownParent: $("#myModal1")
                                });
                            });
                            </script>

                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kpi_tecnologico"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione o Causa ">Indicador <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="kpi_tecnologico" id="kpi_tecnologico" placeholder="kpi_tecnologico">
                                        <option value="">Selecione uma Indicador</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#kpi_tecnologico').select2({
                                    dropdownParent: $("#myModal1")
                                });
                            });
                            </script>




                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Visualizar</button>
                    </div>

                </div>
            </div>
        </div>
        </form>

        <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true" id="myModal2">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel4">Indicador</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Filtro</h4>
                        <!-- Registro forms-->
                        <form action="report/report-indicator.php" target="_blank" method="post">
                            <div class="item form-group">
                                <label for="middle-name"
                                    class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="yr" id="yr_1" placeholder="equipamento">
                                        <option value="">Selecione o ano</option>

                                    </select>


                                </div>
                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#yr_1').select2({
                                    dropdownParent: $("#myModal2")
                                });
                            });
                            </script>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="instituicao" id="instituicao_1" placeholder="Unidade" required>
                                       <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	

                                    </select>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#instituicao_1').select2({
                                    dropdownParent: $("#myModal2")
                                });
                            });
                            </script>
                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Familia de Equipamento <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="family" id="family_1" placeholder="Familia">
                                        <option value="">Selecione uma Familia</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#family_1').select2({
                                    dropdownParent: $("#myModal2")
                                });
                            });
                            </script>

                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kpi_organizacional"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione o Causa ">Indicador <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="kpi_organizacional" id="kpi_organizacional"
                                        placeholder="kpi_organizacional">
                                        <option value="">Selecione uma Indicador</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#kpi_organizacional').select2({
                                    dropdownParent: $("#myModal2")
                                });
                            });
                            </script>




                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Visualizar</button>
                    </div>

                </div>
            </div>
        </div>
        </form>


        <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true" id="myModal3">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel4">Indicador</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Filtro</h4>
                        <!-- Registro forms-->
                        <form action="report/report-indicator.php" target="_blank" method="post">
                            <div class="item form-group">
                                <label for="middle-name"
                                    class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="yr" id="yr_2" placeholder="equipamento">
                                        <option value="">Selecione o ano</option>

                                    </select>


                                </div>
                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#yr_2').select2({
                                    dropdownParent: $("#myModal3")
                                });
                            });
                            </script>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="instituicao" id="instituicao_2" placeholder="Unidade" required>
                                       <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	

                                    </select>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#instituicao_2').select2({
                                    dropdownParent: $("#myModal3")
                                });
                            });
                            </script>
                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Familia de Equipamento <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="family" id="family_2" placeholder="Familia">
                                        <option value="">Selecione uma Familia</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#family_2').select2({
                                    dropdownParent: $("#myModal3")
                                });
                            });
                            </script>

                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kpi_financeiro"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione o Causa ">Indicador <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="kpi_financeiro" id="kpi_financeiro" placeholder="kpi_financeiro">
                                        <option value="">Selecione uma Indicador</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#kpi_financeiro').select2({
                                    dropdownParent: $("#myModal3")
                                });
                            });
                            </script>




                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Visualizar</button>
                    </div>

                </div>
            </div>
        </div>
        </form>

        <div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-hidden="true" id="myModal4">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel4">Indicador</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Filtro</h4>
                        <!-- Registro forms-->
                        <form action="report/report-indicator.php" target="_blank" method="post">
                            <div class="item form-group">
                                <label for="middle-name"
                                    class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="yr" id="yr_3" placeholder="equipamento">
                                        <option value="">Selecione o ano</option>

                                    </select>


                                </div>
                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#yr_3').select2({
                                    dropdownParent: $("#myModal4")
                                });
                            });
                            </script>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="instituicao" id="instituicao_3" placeholder="Unidade" required>
                                       <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	

                                    </select>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#instituicao_3').select2({
                                    dropdownParent: $("#myModal4")
                                });
                            });
                            </script>
                            <div class="ln_solid"></div>



                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kpi_backlog"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione o Causa ">Indicador <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="kpi_backlog" id="kpi_backlog" placeholder="kpi_backlog">
                                        <option value="">Selecione uma Indicador</option>
                                        <option value="21">Ordem de Serviço</option>
                                        <option value="22">Manutenção Preventiva</option>
                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#kpi_backlog').select2({
                                    dropdownParent: $("#myModal4")
                                });
                            });
                            </script>




                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Visualizar</button>
                    </div>

                </div>
            </div>
        </div>
        </form>

        <div class="modal fade bs-example-modal-lg5" tabindex="-1" role="dialog" aria-hidden="true" id="myModal5">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel5">Indicador</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Filtro</h4>
                        <!-- Registro forms-->
                        <form action="report/report-indicator.php" target="_blank" method="post">
                            <div class="item form-group">
                                <label for="middle-name"
                                    class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="yr" id="yr_4" placeholder="equipamento">
                                        <option value="">Selecione o ano</option>

                                    </select>


                                </div>
                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#yr_4').select2({
                                    dropdownParent: $("#myModal5")
                                });
                            });
                            </script>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="instituicao" id="instituicao_4" placeholder="Unidade" required>
                                       <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	

                                    </select>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#instituicao_4').select2({
                                    dropdownParent: $("#myModal5")
                                });
                            });
                            </script>
                            <div class="ln_solid"></div>



                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kpi_backlog"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione o Causa ">Indicador <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="kpi_routine" id="kpi_routine" placeholder="kpi_routine">
                                        <option value="">Selecione uma Indicador</option>
                                        <option value="23">Tempo previsto mensal</option>
                                        <option value="24">Tempo da rotina com tempo do fechamento</option>
                                        <option value="25">Tempo da rotina fechada mensal</option>
                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#kpi_routine').select2({
                                    dropdownParent: $("#myModal5")
                                });
                            });
                            </script>




                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Visualizar</button>
                    </div>

                </div>
            </div>
        </div>
        </form>

        <div class="modal fade bs-example-modal-lg6" tabindex="-1" role="dialog" aria-hidden="true" id="myModal6">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel6">Indicador</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Filtro</h4>
                        <!-- Registro forms-->
                        <form action="report/report-indicator.php" target="_blank" method="post">
                            <div class="item form-group">
                                <label for="middle-name"
                                    class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="yr" id="yr_5" placeholder="equipamento">
                                        <option value="">Selecione o ano</option>

                                    </select>


                                </div>
                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#yr_5').select2({
                                    dropdownParent: $("#myModal6")
                                });
                            });
                            </script>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="instituicao" id="instituicao_5" placeholder="Unidade" required>
                                       <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	

                                    </select>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#instituicao_5').select2({
                                    dropdownParent: $("#myModal6")
                                });
                            });
                            </script>
                            <div class="ln_solid"></div>



                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kpi_mp"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione o Causa ">Indicador <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="kpi_mp" id="kpi_mp" placeholder="kpi_mp">
                                        <option value="">Selecione uma Indicador</option>
                                        <option value="26">Acompanhamento de M.P</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#kpi_mp').select2({
                                    dropdownParent: $("#myModal6")
                                });
                            });
                            </script>




                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Visualizar</button>
                    </div>

                </div>
            </div>
        </div>
        </form>

        <div class="modal fade bs-example-modal-lg7" tabindex="-1" role="dialog" aria-hidden="true" id="myModal7">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel7">Indicador</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Filtro</h4>
                        <!-- Registro forms-->
                        <form action="report/report-indicator.php" target="_blank" method="post">
                            <div class="item form-group">
                                <label for="middle-name"
                                    class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="yr" id="yr_6" placeholder="equipamento">
                                        <option value="">Selecione o ano</option>

                                    </select>


                                </div>
                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#yr_6').select2({
                                    dropdownParent: $("#myModal7")
                                });
                            });
                            </script>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="instituicao" id="instituicao_6" placeholder="Unidade" required>
                                       <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	

                                    </select>

                                </div>

                            </div>

                            <script>
                            $(document).ready(function() {
                                $('#instituicao_6').select2({
                                    dropdownParent: $("#myModal7")
                                });
                            });
                            </script>
                            <div class="ln_solid"></div>



                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kpi_mc"
                                    aria-hidden="true" class="docs-tooltip" data-toggle="tooltip"
                                    title="Selecione o Causa ">Indicador <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <select type="text" style="width:350px;" class="form-control has-feedback-right"
                                        name="kpi_mc" id="kpi_mc" placeholder="kpi_mc">
                                        <option value="">Selecione uma Indicador</option>
                                        <option value="27">Acompanhamento de M.C</option>

                                    </select>
                                </div>
                            </div>
                            <script>
                            $(document).ready(function() {
                                $('#kpi_mc').select2({
                                    dropdownParent: $("#myModal7")
                                });
                            });
                            </script>




                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Visualizar</button>
                    </div>

                </div>
            </div>
        </div>
        </form>


    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    $('#family').select2({
        dropdownParent: $("#myModal1")
    }).load('family.php');
    $('#kpi_tecnologico').select2({
        dropdownParent: $("#myModal1")
    }).load('kpi_tecnologico.php');

});
</script>
<script type="text/javascript">
$(document).ready(function() {

    $('#family_1').select2({
        dropdownParent: $("#myModal2")
    }).load('family_1.php');
    $('#kpi_organizacional').select2({
        dropdownParent: $("#myModal2")
    }).load('kpi_organizacional.php');

});
</script>
<script type="text/javascript">
$(document).ready(function() {

    $('#family_2').select2({
        dropdownParent: $("#myModal3")
    }).load('family_2.php');
    $('#kpi_financeiro').select2({
        dropdownParent: $("#myModal3")
    }).load('kpi_financeiro.php');

});
</script>
<script type="text/javascript">
$(document).ready(function() {

    $('#yr').select2({
        dropdownParent: $("#myModal1")
    }).load('kpi_yr.php');
    $('#instituicao').select2({
        dropdownParent: $("#myModal1")
    }).load('kpi_instituicao.php');

    $('#yr_1').select2({
        dropdownParent: $("#myModal2")
    }).load('kpi_yr.php');
    $('#instituicao_1').select2({
        dropdownParent: $("#myModal2")
    }).load('kpi_instituicao.php');

    $('#yr_2').select2({
        dropdownParent: $("#myModal3")
    }).load('kpi_yr.php');
    $('#instituicao_2').select2({
        dropdownParent: $("#myModal3")
    }).load('kpi_instituicao.php');

    $('#yr_3').select2({
        dropdownParent: $("#myModal4")
    }).load('kpi_yr.php');
    $('#instituicao_3').select2({
        dropdownParent: $("#myModal4")
    }).load('kpi_instituicao.php');

    $('#yr_4').select2({
        dropdownParent: $("#myModal5")
    }).load('kpi_yr.php');
    $('#instituicao_4').select2({
        dropdownParent: $("#myModal5")
    }).load('kpi_instituicao.php');

    $('#yr_5').select2({
        dropdownParent: $("#myModal6")
    }).load('kpi_yr.php');
    $('#instituicao_5').select2({
        dropdownParent: $("#myModal6")
    }).load('kpi_instituicao.php');

    $('#yr_6').select2({
        dropdownParent: $("#myModal7")
    }).load('kpi_yr.php');
    $('#instituicao_6').select2({
        dropdownParent: $("#myModal7")
    }).load('kpi_instituicao.php');

});
</script>

<!-- /page content -->