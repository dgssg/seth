<?php
$codigoget = ($_GET["routine"]);

// Create connection

include("database/database.php");
$query = "SELECT calibration_routine.time_ms,calibration_preventive.id_routine ,calibration_routine.id,calibration_routine.data_start, calibration_routine.periodicidade,calibration_routine.reg_date,calibration_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM calibration_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN calibration_preventive ON calibration_preventive.id_routine = calibration_routine.id WHERE calibration_preventive.id_routine like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
 while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
//  }
}
}

?>

<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>  Calibração <small></small></h3>
            </div>


          </div>

          <div class="clearfix"></div>

          <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Cabeçalho <small>Procedimento</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

              <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="nome" name="nome" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> " readonly="readonly" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio<span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="categoria" name="categoria" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempo</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="info" class="form-control" type="text" name="info"  value="<?php printf($time_ms); ?>" readonly="readonly">
                      </div>
                    </div>
                     <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade (dias)</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($periodicidade); ?>" readonly="readonly">
                      </div>
                    </div>



                     <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                      </div>
                    </div>
                     <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                      </div>
                    </div>



                </div>
              </div>
            </div>
       </div>
<!-- Posicionamento -->

<div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

<?php

$query = "SELECT calibration_preventive.file,calibration_preventive.id,calibration_preventive.date_start,calibration_preventive.upgrade,calibration_preventive.id_status,calibration_preventive.date_mp_end,calibration_preventive.date_mp_start,calibration_preventive.id_routine ,calibration_routine.id,calibration_routine.data_start, calibration_routine.periodicidade,calibration_preventive.reg_date,calibration_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM calibration_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN calibration_preventive ON calibration_preventive.id_routine = calibration_routine.id WHERE calibration_preventive.id_routine like '$rotina'   GROUP BY calibration_preventive.id ORDER BY calibration_preventive.date_start DESC";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id_anexo,$id,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);



?>


<div class="col-md-11 col-sm-11 ">
              <div class="x_panel">

                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                          <div class="card-box table-responsive">

                  <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                      <tr>
                      <th>#</th>
                        <th>Data Programada</th>

                        <th>Data Abertura</th>
                        <th>Data Termino</th>

                        <th>Status</th>
                        <th>Atualização</th>


                        <th>Ação</th>

                      </tr>
                    </thead>


                    <tbody>
                        <?php   while ($stmt->fetch()) { 
                            $row=$row+1;
                            $programada = date('d/m/Y', strtotime($programada));
                              $reg_date = date('d/m/Y', strtotime($reg_date));
                              if($date_end_ms != ""){ 
                                $date_end_ms = date('d/m/Y', strtotime($date_end_ms));
  
                              }
                              
                            ?>
                        <tr>
                        <td> <?php printf($row); ?> </td>
                        <td><?php printf($programada); ?> </td>

                        <td> <?php printf($reg_date); ?> </td>

                          <td> <?php printf($date_end_ms); ?>   </td>


                              <td><?php if($status == 1){ printf("Aberto");}if($status == 2){ printf("Atrasado");}
                                if($status == 3){ printf("Fechado");}
                                if($status == 4){ printf("Cancelado");}
                                ?></td>

                        <td><?php printf($ms_upgrade); ?></td>

                        <td>

                <a class="btn btn-app"  href="flow-calibration-viwer?id=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
                              title: 'Visualizar',
                              text: 'Visualizar procediemnto',
                              type: 'info',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-file-pdf-o"></i> Visualizar
                </a>
                 <a class="btn btn-app"  href="flow-calibration-open-tag?id=<?php printf($id); ?>" target="_blank"   onclick="new PNotify({
                              title: 'Visualizar',
                              text: 'Visualizar Etiqueta',
                              type: 'info',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-qrcode"></i> Etiqueta
                </a>
                 <a class="btn btn-app"  href="flow-calibration-close-edit-case?id=<?php printf($id); ?>" onclick="new PNotify({
                              title: 'Editar',
                              text: 'Abrindo Edição',
                              type: 'sucess',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-edit"></i> Editar
                </a>
                <a class="btn btn-app"  href="flow-calibration-viwer-print?id=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
                  title: 'Imprimir',
                  text: 'Imprimir Rotina',
                  type: 'info',
                  styling: 'bootstrap3'
                });">
                <i class="fa fa-print"></i> Imprimir
              </a>
              <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/flow-calibration-trash-backend?id=<?php printf($id); ?>';
  }
})
">
             <i class="fa fa-trash"></i> Excluir</a>
                  <?php if($id_anexo==!""){?>
                <a class="btn btn-app"  href="dropzone/calibration/<?php printf($id_anexo); ?>" download onclick="new PNotify({
                              title: 'Editar',
                              text: 'Abrindo Edição',
                              type: 'sucess',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-download"></i> Download
                </a>
                <?php    }  ?>
                </td>
                      </tr>
                      <?php   } }  ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
              </div>
            </div>





<!-- Posicionamento -->
           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="flow-calibration-close">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>



            <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
                              title: 'Cancelamento',
                              text: 'Cancelamento de Abertura de O.S!',
                              type: 'error',
                              styling: 'bootstrap3'
                          });">
                  <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                </a>
                  <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
                              title: 'Visualizar',
                              text: 'Visualizar O.S!',
                              type: 'info',
                              styling: 'bootstrap3'
                          });" >
                  <i class="fa fa-file-pdf-o"></i> Visualizar
                </a> -->

              </div>
            </div>




              </div>
            </div>
            <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    },
        initComplete: function () {
            this.api()
                .columns([1,2,3,4])
                .every(function (d) {
                    var column = this;
                  var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      }); 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                
                
        },
    });
});


</script>

