<?php
include("database/database.php");
date_default_timezone_set('America/Sao_Paulo');
$data= date(DATE_RFC2822);
$query = "SELECT equipamento_familia.nome,equipamento.serie,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,tse.id, tse.data_start, tse.val, tse.ven, tse.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM tse INNER JOIN equipamento on equipamento.id = tse.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE tse.trash =1";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($fabricante,$serie,$instituicao,$area,$setor,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }




?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Laudo</th>

                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>

                          <th>Realização</th>
                          <th>Validade</th>
                          <th>vencido</th>
                        
                             <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                             <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                             <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {
                            if ($ven == 0) {
                              $new_date=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));
                              $data_now=date("Y-m-d");
                            if ( $new_date < $data_now ) {
                              $ven = 1;
                            //  $stmt = $conn->prepare("UPDATE calibration SET ven = 1 WHERE id= ?");
                            //  $stmt->bind_param("s",$id);
                            //  $execval = $stmt->execute();
                            }
                            }
                              ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($nome); ?> </td>
                          <td> <?php printf($modelo); ?></td>
                          <td> <?php printf($fabricante); ?></td>
                           <td><?php printf($codigo); ?> </td>
                           <td><?php printf($serie); ?> </td>
                           <td><?php printf($data_start); ?></td>
                            <td><?php printf($val); ?> Meses</td>
                            <td><?php if($ven ==0) {printf("Não");} if($ven ==1) {printf("Sim");}  ?></td>
                           
                            <td><?php printf($instituicao); ?></td>
                               <td><?php printf($area); ?></td>
                               <td>  <?php printf($setor); ?></td>


                          <td>
              <!--     <a class="btn btn-app"  href="tse-report-edit?laudo=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>-->
                  <a class="btn btn-app"  href="calibration-equipament-edit?laudo=<?php printf($id); ?>" onclick="new PNotify({
                               title: 'Editar',
                               text: 'Abrindo Edição',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-edit"></i> Editar
                 </a>

    <!--      <a  class="btn btn-app" href="tse-report-viewer-page?laudo=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Laudo!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-o"></i> Visualizar v1
                  </a> -->
              <!--    <a  class="btn btn-app" href="tse-report-viewer?laudo=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
                                       title: 'Visualizar',
                                       text: 'Visualizar Laudo!',
                                       type: 'info',
                                       styling: 'bootstrap3'
                                   });" >
                           <i class="fa fa-file-pdf-o"></i> PDF
                         </a> -->
                    <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tse-report-trash-backend?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  <a  class="btn btn-app" href="tse-report-viewer-copy-print?laudo=<?php printf($id); ?>  "target="_blank" onclick="new PNotify({
                                       title: 'Visualizar',
                                       text: 'Visualizar Laudo!',
                                       type: 'info',
                                       styling: 'bootstrap3'
                                   });" >
                           <i class="fa fa-print"></i> Imprimir
                         </a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
