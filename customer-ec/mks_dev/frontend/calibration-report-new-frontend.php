  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Laudos de <small>Calibração</small></h3>
              </div>

           
            </div>
       <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                 <a class="btn btn-app"  href="calibration-report">
                    <i class="fa fa-file"></i> Laudos
                  </a>
                  
                  <a class="btn btn-app"  href="calibration-report-new-wizard">
                  <i class="fa fa-plus-square"></i> Novo
                  </a>
                   <a class="btn btn-app"  href="calibration-report-new-v2">
                    <i class="fa fa-plus-square"></i> Novo v2
                </a>
                  <a class="btn btn-app"  href="calibration-report-single">
                    <i class="fa fa-file-o"></i> Laudos único
                  </a>
                  <a class="btn btn-app"  href="calibration-report-new-single-wizard">
                    <i class="fa fa-plus-square-o"></i> Novo único
                  </a>
                <a class="btn btn-app"  href="calibration-report-new-single-wizard-v2">
                    <i class="fa fa-plus-square-o"></i> Novo único v2
                </a>
               
                  
                </div>
              </div>
              
              
              
              
        
              
                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Cadastro de Laudo </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                <?php include 'frontend/calibration-report-new-registration-frontend.php';?>
                  
                </div>
              </div> 
	       
	        
                  
                  
                </div>
              </div>
              
  