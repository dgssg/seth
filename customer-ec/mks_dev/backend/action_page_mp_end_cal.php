<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;
	
	require '../../../framework/PHPMailer/src/PHPMailer.php';
	require '../../../framework/PHPMailer/src/SMTP.php';
	require '../../../framework/PHPMailer/src/Exception.php';
	
	include("../database/database.php");
	session_start();
	
	$codigoget = $_GET['codigoget'];
	$id_mp_reurn = $codigoget;
	$programada_data = $_GET['programada_data'];
	$assessment = $_GET['fornecedor'];
	$number_assessment = $_GET['data_assessment'];
	$data_assessment = $_GET['obs'];
	$id_fornecedor = $_GET['date_start'];
	$id_service = $_GET['date_end'];
	$obs_general = $_GET['obs'];
	$time_obs = $_GET['time'];
	$colaborador_obs = $_GET['colaborador'];
	$message_mp = $_GET['message_mp'];
	$message_tc = $_GET['message_tc'];
	$temp_obs = $_GET['temp'];
	$hum_obs = $_GET['hum'];
	$v4uuid_obs = $_GET['v4uuid'];
	$chave_obs = $_GET['chave'];
	$assinature_obs = $_GET['assinature'];

	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$setor = $_SESSION['setor'];
	$usuario = $_SESSION['id_usuario'];
	
	$assessment=$_GET['assessment'];
	$number_assessment=$_GET['number_assessment'];
	$data_assessment=$_GET['data_assessment'];
	$id_fornecedor=$_GET['id_manufacture'];
	$id_service=$_GET['fornecedor_service'];
	$obs=$_GET['obs'];
	
	date_default_timezone_set('America/Sao_Paulo');
	//$codigoget = ($_GET["id"]);
	$fornecedor = $_GET['fornecedor'];
	$fornecedor_assement = $fornecedor;

	$date_mp_start = $_GET['date_start'];
	$date_mp_end = $_GET['date_end'];
	$time_mp = $_GET['time'];
	$colaborador = $_GET['colaborador'];
	$message_mp = $_GET['message_mp'];
	$message_tc = $_GET['message_tc'];
	$routine = $_GET['routine'];
	$periodicidade = $_GET['periodicidade'];
	$programada= $_GET['programada'];
	$temp= $_GET['temp'];
	$hum= $_GET['hum'];
	$table = $_GET['table'];
	$tabelaHTML = $_GET['tabelaHTML'];

	
	$anexo=$_COOKIE['anexo'];
	
	$status="3";
	$status2=0;
	$count=0;
	$id_mod=2;
	$mp_control = 1;
	
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET mp_control= ? WHERE id= ?");
	$stmt->bind_param("ss",$mp_control,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_fornecedor = ? WHERE id= ?");
	$stmt->bind_param("ss",$fornecedor,$number_assessment);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("INSERT INTO fornecedor_assessment (assessment, number_assessment, data_assessment, id_fornecedor, id_service, obs) VALUES (?, ?,?, ?, ?, ?)");
	$stmt->bind_param("ssssss",$id_mod,$number_assessment,$data_assessment,$fornecedor,$id_service,$obs);
	$execval = $stmt->execute();
	$last_id = $conn->insert_id;
	$stmt->close();
	if($id_service != "" ){
		$query = "SELECT  id FROM fornecedor_question WHERE id_fornecedor_service  = $id_service ";
		
		//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
		if ($stmt = $conn->prepare($query)) {
			$stmt->execute();
			$stmt->bind_result($id);
			while ($stmt->fetch()) {
				$codigos[]  = $id;
				$count=$count+1;
				
			}
		}
	}
	
	//echo implode( ', ', $codigos );
	
	for ($i=0; $i < $count ; $i++) {
		$id_question=$codigos[$i];
		$avaliable=$_GET[$codigos[$i]];
		
		
		$stmt = $conn->prepare("INSERT INTO fornecedor_assessment_question (id_assessment, id_question, avaliable) VALUES (?, ?, ?)");
		$stmt->bind_param("sss",$last_id,$id_question,$avaliable);
		$execval = $stmt->execute();
		$stmt->close();
	}
	
	$query = "SELECT assessment FROM fornecedor WHERE id = ?";
	if ($stmt = $conn->prepare($query)) {
		$stmt->bind_param("i", $fornecedor); // "i" indica que $fornecedor é um inteiro
		$stmt->execute();
		$stmt->bind_result($assessment_fornecedor);
		$stmt->fetch(); // Se você espera apenas uma linha de resultado
		$stmt->close();
	}

	
	if($assessment_fornecedor == 0){
		
		$stmt = $conn->prepare("UPDATE maintenance_preventive SET assessment = 0 WHERE id= ?");
		$stmt->bind_param("s",$codigoget);
		$execval = $stmt->execute();
		
		$stmt = $conn->prepare("INSERT INTO assessment_send (id_mod, id_number, id_usuario) VALUES (?,?,?)");
		$stmt->bind_param("sss",$id_mod,$codigoget,$usuario);
		$execval = $stmt->execute();
		$last_id = $conn->insert_id;
		$stmt->close();
		
		$query="SELECT email FROM usuario WHERE id like '$usuario'";
		
		if ($stmt = $conn->prepare($query)) {
			$stmt->execute();
			$stmt->bind_result($email_usuario);
		}
		$stmt->close();
		
		$query = " SELECT fornecedor.id,fornecedor_assessment.id,fornecedor.empresa,fornecedor_assessment.reg_date, fornecedor_question.question, fornecedor_assessment_question.id_question,fornecedor_assessment_question.avaliable, fornecedor_service.service, fornecedor_assessment.id_service,fornecedor_assessment.assessment,fornecedor_assessment.number_assessment, fornecedor_assessment.data_assessment, fornecedor_assessment.obs FROM fornecedor_assessment LEFT JOIN fornecedor_service ON fornecedor_service.id = fornecedor_assessment.id_service LEFT JOIN fornecedor_assessment_question ON fornecedor_assessment_question.id_assessment = fornecedor_assessment.id LEFT JOIN fornecedor_question ON fornecedor_question.id = fornecedor_assessment_question.id_question LEFT JOIN fornecedor ON fornecedor.id = fornecedor_assessment.id_fornecedor WHERE fornecedor_assessment.id = $last_id limit 1";
		
		if ($stmt = $conn->prepare($query)) {
			$stmt->execute();
			$stmt->bind_result($fornecedor,$id,$empresa,$reg_date,$question,$id_question,$avaliable,$service,$id_service,$assessment_fornecedor,$number_assessment,$data_assessment,$obs);
			
			while ($stmt->fetch()) { 
				
				switch ($avaliable) {
					case '1':
						$avaliable="Bom";
						break;
					case '2':
						$avaliable="Regular";
						break;
					case '3':
						$avaliable="Ruim";
						break; }
				switch ($assessment_fornecedor) {
					case '1':
						$assessment="Ordem de Serviço";
						$quer_assment=1;
						break;
					case '2':
						$assessment="Manutenção Preventiva";
						$quer_assment=2;
						break;
					case '3':
						$assessment="Calibração";
						$quer_assment=3;
						break;
					
					
				}
				$descricao="$descricao \n Fornecedor: $empresa\nFornecimento: $service \n Data avaliação: $reg_date\n Data do Serviço avaliado:$data_assessment \n Tipo do serviço:$assessment\nNumero do serviço:$number_assessment\n Avaliação:$question\n Conceito:$avaliable\nObservação:$obs \n";
				
			}
		}
		
		$query_assessment="";
		switch ($quer_assment) {
			
			case '1':
				$query_assessment = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM os INNER JOIN equipamento ON equipamento.id = os.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE os.id like  '$number_assessment'";
				break;
			case '2':
				$query_assessment = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM maintenance_preventive INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE maintenance_preventive.id  like '$number_assessment'";
				break;
			case '3':
				$query_assessment = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM calibration_preventive INNER JOIN calibration_routine ON calibration_routine.id = calibration_preventive.id_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE calibration_preventive.id  like '$number_assessment'";
				break;
			
			
		}
		if($query_assessment != ""){
			if ($stmt = $conn->prepare($query_assessment)) {
				$stmt->execute();
				$stmt->bind_result($unidade, $unidade_cnpj, $unidade_adress, $unidade_cep,$unidade_bairro, $unidade_city);
				
				
				
				while ($stmt->fetch()) {
					
				}
			}
		}		$query = "SELECT id, empresa, cnpj, seguimento, adress, city, state, cep, contato, email, telefone_1, telefone_2, observacao, bairro, ibge, reg_date, upgrade, trash, assessment FROM fornecedor WHERE id = ?";
		if ($stmt = $conn->prepare($query)) {
			// Vincula o parâmetro da consulta
			$stmt->bind_param("i", $fornecedor_assement); // Supondo que $fornecedor_assement seja um número inteiro
			// Executa a consulta
			$stmt->execute();
			// Vincula os resultados da consulta às variáveis
			$stmt->bind_result($id_fornecedor, $empresa, $cnpj, $seguimento, $adress, $city, $state, $cep, $contato, $email, $telefone_1, $telefone_2, $observacao, $bairro, $ibge, $reg_date, $upgrade, $trash, $assessment_fornecedor);
			// Itera sobre os resultados
			while ($stmt->fetch()) {
				// Manipula os dados aqui
			}
			// Fecha a declaração
			
		} 
		$descricao=" 
Unidade: $unidade\n
CNPJ: $unidade_cnpj\n
Endereço: $unidade_adress $unidade_cep $unidade_bairro $unidade_city\n

Nome: $usuariologado
Email: $email_usuario
-------------------------------------------------------------------
\n\n $descricao \n\n\n\n  

Enviado por: $usuariologado
Contato para retorno: $email_usuario
-------------------------------------------------------------------
Mensagem automática enviada via sistema SETH. Não responda. MK Sistemas Biomedicos";
		$titulo="Avaliação de Fornecedor $number_assessment";
		// send email
		
		
		
	}
	if($assessment_fornecedor == 0){
		
		$mail = new PHPMailer(true);
		
		try{
			// Configurações do servidor SMTP
			$mail->isSMTP();
			$mail->Host       = 'seth.mksistemasbiomedicos.com.br'; // Substitua pelo host do seu provedor
			$mail->SMTPAuth   = true;
			$mail->Username   = 'seth@seth.mksistemasbiomedicos.com.br'; // Substitua pelo seu endereço de e-mail
			$mail->Password   = 'mks@2024'; // Substitua pela sua senha
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Use 'tls' ou 'ssl' se necessário
			$mail->Port       = 587; // Porta do servidor SMTP
			
			// Configurações do e-mail
			$mail->setFrom('seth@seth.mksistemasbiomedicos.com.br', $usuariologado);
			$mail->addAddress($email, $empresa); // Substitua pelo endereço do destinatário
			$mail->Subject = $titulo;
			$mail->Body    = $descricao;
			
			// Enviar e-mail
			$mail->send();
			
		}catch (Exception $e) {
			//echo 'Erro no envio do e-mail: ', $mail->ErrorInfo;
		}
		
	}
	
	//$ano =date('Y', $programada);
	$carimbo_tempo = strtotime($programada);
	$ano = date('Y', $carimbo_tempo);
	//$partes = explode('-', $data);
	//$ano = $partes[0]; // O ano estará na primeira posição do array resultante
	
	function getListaDiasFeriado($ano = null) {
		
		if ($ano === null) {
			$ano = intval(date('Y'));
		}
		
		$pascoa = easter_date($ano); // retorna data da pascoa do ano especificado
		$diaPascoa = date('j', $pascoa);
		$mesPacoa = date('n', $pascoa);
		$anoPascoa = date('Y', $pascoa);
		
		$feriados = [
			// Feriados nacionais fixos
			mktime(0, 0, 0, 1, 1, $ano),   // Confraternização Universal
			mktime(0, 0, 0, 4, 21, $ano),  // Tiradentes
			mktime(0, 0, 0, 5, 1, $ano),   // Dia do Trabalhador
			mktime(0, 0, 0, 9, 7, $ano),   // Dia da Independência
			mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida
			mktime(0, 0, 0, 11, 2, $ano),  // Todos os santos
			mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica
			mktime(0, 0, 0, 12, 25, $ano), // Natal
			//   mktime(0, 0, 0, 1, 31, $ano), // Fim de mes
			//
			// Feriados variaveis
			mktime(0, 0, 0, $mesPacoa, $diaPascoa - 48, $anoPascoa), // 2º feria Carnaval
			mktime(0, 0, 0, $mesPacoa, $diaPascoa - 47, $anoPascoa), // 3º feria Carnaval 
			mktime(0, 0, 0, $mesPacoa, $diaPascoa - 2, $anoPascoa),  // 6º feira Santa  
			mktime(0, 0, 0, $mesPacoa, $diaPascoa, $anoPascoa),      // Pascoa
			mktime(0, 0, 0, $mesPacoa, $diaPascoa + 60, $anoPascoa), // Corpus Christ
		];
		
		sort($feriados);
		
		$listaDiasFeriado = [];
		foreach ($feriados as $feriado) {
			$data = date('Y-m-d', $feriado);
			$listaDiasFeriado[$data] = $data;
		}
		
		return $listaDiasFeriado;
	}
	
	function isFeriado($data) {
		$listaFeriado = getListaDiasFeriado(date('Y', strtotime($data)));
		if (isset($listaFeriado[$data])) {
			return true;
		}
		
		return false;
	}
	
	function getDiasUteis($aPartirDe, $quantidadeDeDias = 30) {
		$dateTime = new DateTime($aPartirDe);
		
		$listaDiasUteis = [];
		$contador = 0;
		while ($contador < $quantidadeDeDias) {
			
			$dateTime->modify('+1 weekday'); // adiciona um dia pulando finais de semana
			$data = $dateTime->format('Y-m-d');
			if (!isFeriado($data)) {
				$listaDiasUteis[] = $data;
				$contador++;
			}
		}
		
		return $listaDiasUteis;
	}
	$today = $programada;
	//$today = "2023-01-06";
	
	$listaDiasUteis = getDiasUteis($today, 15);
	$ultimoDia = end($listaDiasUteis);
	
	//echo "<pre>";
	//print_r($listaDiasUteis);
	//echo "</pre>";
	
	//echo "ULTIMO DIA: " . $ultimoDia;
	//print_r("\n");
	//echo " DIA: " . $today;
	//print_r("\n");
	$i=0;
	$key=true;
	do {
		if($listaDiasUteis[$i] > $today ){
			//	echo " DIA Proximo: " . $listaDiasUteis[$i] ;
			$DiasUteis = $listaDiasUteis[$i];
			$key=false;
		}
		else
			{
				$i	= $i +1;
				
			}
	} while($key);
	
	
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_fornecedor= ? WHERE id= ?");
	$stmt->bind_param("ss",$fornecedor,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_start= ? WHERE id= ?");
	$stmt->bind_param("ss",$date_mp_start,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_end= ? WHERE id= ?");
	$stmt->bind_param("ss",$date_mp_end,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET time_mp= ? WHERE id= ?");
	$stmt->bind_param("ss",$time_mp,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_status= ? WHERE id= ?");
	$stmt->bind_param("ss",$status,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET file= ? WHERE id= ?");
	$stmt->bind_param("ss",$anexo,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_mp= ? WHERE id= ?");
	$stmt->bind_param("ss",$colaborador,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_mp= ? WHERE id= ?");
	$stmt->bind_param("ss",$message_mp,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_tc= ? WHERE id= ?");
	$stmt->bind_param("ss",$message_tc,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_end= ? WHERE id= ?");
	$stmt->bind_param("ss",$date_mp_end,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET temp= ? WHERE id= ?");
	$stmt->bind_param("ss",$temp,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET hum= ? WHERE id= ?");
	$stmt->bind_param("ss",$hum,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	
	if($periodicidade==365){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 12 months'));
		
	}
	
	if($periodicidade==180){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 6 months'));
		
	}
	
	if($periodicidade==30){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 months'));
		
	}
	
	if($periodicidade==1){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 days'));
		
	}
	
	if($periodicidade==5){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 weekday'));
		
		
	}
	
	if($periodicidade==7){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 7 days'));
		
		
	}
	
	if($periodicidade==14){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 14 days'));
		
		
	}
	
	if($periodicidade==21){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 21 days'));
		
		
	}
	
	if($periodicidade==28){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 28 days'));
		
		
	}
	
	if($periodicidade==60){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 2 months'));
		
		
	}
	
	if($periodicidade==90){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 3 months'));
		
		
	}
	
	if($periodicidade==120){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 4 months'));
		
		
	}
	
	if($periodicidade==730){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 24 months'));
		
		
	}
	if($periodicidade==1095){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 36 months'));
		
		
	}
	if($periodicidade==1460){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 48 months'));
		
		
	}
	
	
	$stmt = $conn->prepare("UPDATE maintenance_control SET data_after = ? WHERE id_routine= ?");
	$stmt->bind_param("ss",$data_after,$routine);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_control SET status = ? WHERE id_routine= ?");
	$stmt->bind_param("ss",$status2,$routine);
	$execval = $stmt->execute();
	$stmt->close();
	
	

	$query = "SELECT date_start FROM maintenance_preventive WHERE id_routine = ? ORDER BY date_start DESC LIMIT 1";
	
	if ($stmt = $conn->prepare($query)) {
		// Substitua "i" pelo tipo de dado correto de $routine (se for um número inteiro)
		$stmt->bind_param("i", $routine);
		
		if ($stmt->execute()) {
			$stmt->bind_result($programada);
			
			// Use uma condição if para verificar se há resultados
			if ($stmt->fetch()) {
				// Faça algo com $programada, por exemplo, exibir ou processar
			//	echo "Última data de início programada: " . $programada;
			} else {
				// Não foram encontrados registros
			//	echo "Nenhum registro encontrado para a rotina $routine";
			}
		} else {
			// Tratar erro na execução da consulta
		//	echo "Erro na execução da consulta: " . $stmt->error;
		}
		
		$stmt->close(); // Fechar o statement
	} else {
		// Tratar erro na preparação da consulta
	//	echo "Erro na preparação da consulta: " . $conn->error;
	}
	
	 
	$query = "SELECT periodicidade FROM maintenance_routine WHERE id = ?";
	
	if ($stmt = $conn->prepare($query)) {
		// Substitua "i" pelo tipo de dado correto de $routine (se for um número inteiro)
		$stmt->bind_param("i", $routine);
		
		if ($stmt->execute()) {
			$stmt->bind_result($periodicidade);
			
			// Use uma condição if para verificar se há resultados
			if ($stmt->fetch()) {
				// Faça algo com $periodicidade, por exemplo, exibir ou processar
				//echo "Periodicidade da rotina: " . $periodicidade;
			} else {
				// Não foram encontrados registros
				//echo "Nenhuma periodicidade encontrada para a rotina $routine";
			}
		} else {
			// Tratar erro na execução da consulta
		//	echo "Erro na execução da consulta: " . $stmt->error;
		}
		
		$stmt->close(); // Fechar o statement
	} else {
		// Tratar erro na preparação da consulta
	//	echo "Erro na preparação da consulta: " . $conn->error;
	}
 	
	
	
	if($periodicidade==365){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 12 months'));
		
	}
	
	if($periodicidade==180){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 6 months'));
		
	}
	
	if($periodicidade==30){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 months'));
		
	}
	
	if($periodicidade==1){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 days'));
		
	}
	if($periodicidade==5){
		
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 weekday'));
		
	}
	
	if($periodicidade==7){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 7 days'));
		
		
	}
	
	if($periodicidade==14){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 14 days'));
		
		
	}
	
	if($periodicidade==21){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 21 days'));
		
		
	}
	
	if($periodicidade==28){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 28 days'));
		
		
	}
	
	if($periodicidade==60){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 2 months'));
		
		
	}
	
	if($periodicidade==90){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 3 months'));
		
		
	}
	
	if($periodicidade==120){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 4 months'));
		
		
	}
	
	if($periodicidade==730){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 24 months'));
		
		
	}
	if($periodicidade==1095){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 36 months'));
		
		
	}
	if($periodicidade==1460){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 48 months'));
		
		
	}
	
	
	
	$stmt = $conn->prepare("UPDATE maintenance_control SET data_after = ? WHERE id_routine= ?");
	$stmt->bind_param("ss",$data_after,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_control SET status = ? WHERE id_routine= ?");
	$stmt->bind_param("ss",$status2,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$codigoget = $routine;
	 
	// Buscar a última data de início da manutenção preventiva
	$queryPreventiva = "SELECT date_start FROM maintenance_preventive WHERE id_routine = ? ORDER BY date_start DESC LIMIT 1";
	if ($stmtPreventiva = $conn->prepare($queryPreventiva)) {
		$stmtPreventiva->bind_param("i", $codigoget); // Substitua "i" pelo tipo de dado correto de $codigoget
		
		if ($stmtPreventiva->execute()) {
			$stmtPreventiva->bind_result($programada);
			
			// Use uma condição if para verificar se há resultados
			if ($stmtPreventiva->fetch()) {
				// Faça algo com $programada, por exemplo, exibir ou processar
			//	echo "Última data de início da manutenção preventiva: " . $programada;
			} else {
				// Não foram encontrados registros
			//	echo "Nenhuma manutenção preventiva encontrada para o código $codigoget";
			}
		} else {
			// Tratar erro na execução da consulta
		//	echo "Erro na execução da consulta de manutenção preventiva: " . $stmtPreventiva->error;
		}
		
		$stmtPreventiva->close(); // Fechar o statement
	} else {
		// Tratar erro na preparação da consulta
	//	echo "Erro na preparação da consulta de manutenção preventiva: " . $conn->error;
	}
	
	// Buscar a periodicidade da rotina de manutenção
	$queryRotina = "SELECT periodicidade FROM maintenance_routine WHERE id = ?";
	if ($stmtRotina = $conn->prepare($queryRotina)) {
		$stmtRotina->bind_param("i", $codigoget); // Substitua "i" pelo tipo de dado correto de $codigoget
		
		if ($stmtRotina->execute()) {
			$stmtRotina->bind_result($periodicidade);
			
			// Use uma condição if para verificar se há resultados
			if ($stmtRotina->fetch()) {
				// Faça algo com $periodicidade, por exemplo, exibir ou processar
		//		echo "Periodicidade da rotina de manutenção: " . $periodicidade;
			} else {
				// Não foram encontrados registros
		//		echo "Nenhuma rotina de manutenção encontrada para o código $codigoget";
			}
		} else {
			// Tratar erro na execução da consulta
		//	echo "Erro na execução da consulta de rotina de manutenção: " . $stmtRotina->error;
		}
		
		$stmtRotina->close(); // Fechar o statement
	} else {
		// Tratar erro na preparação da consulta
	//	echo "Erro na preparação da consulta de rotina de manutenção: " . $conn->error;
	}
	
	
	
	
	
	if($periodicidade==365){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 12 months'));
		
	}
	
	if($periodicidade==180){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 6 months'));
		
	}
	
	if($periodicidade==30){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 months'));
		
	}
	
	if($periodicidade==1){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 days'));
		
	}
	if($periodicidade==5){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 1 weekday'));
		
	}
	
	if($periodicidade==7){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 7 days'));
		
		
	}
	
	if($periodicidade==14){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 14 days'));
		
		
	}
	
	if($periodicidade==21){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 21 days'));
		
		
	}
	
	if($periodicidade==28){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 28 days'));
		
		
	}
	
	if($periodicidade==60){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 2 months'));
		
		
	}
	
	if($periodicidade==90){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 3 months'));
		
		
	}
	
	if($periodicidade==120){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 4 months'));
		
		
	}
	
	if($periodicidade==730){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 24 months'));
		
		
	}
	if($periodicidade==1095){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 36 months'));
		
		
	}
	if($periodicidade==1460){
		
		$data_after=date('Y-m-d', strtotime($programada.' + 48 months'));
		
		
	}
	
	
	$stmt = $conn->prepare("UPDATE maintenance_control SET data_after = ? WHERE id_routine= ?");
	$stmt->bind_param("ss",$data_after,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE maintenance_control SET status = ? WHERE id_routine= ?");
	$stmt->bind_param("ss",$status2,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	// Buscar a última data de início da manutenção preventiva
	$queryPreventiva = "SELECT date_start FROM maintenance_preventive WHERE id_routine = ? ORDER BY date_start DESC LIMIT 1";
	if ($stmtPreventiva = $conn->prepare($queryPreventiva)) {
		$stmtPreventiva->bind_param("i", $codigoget); // Substitua "i" pelo tipo de dado correto de $codigoget
		
		if ($stmtPreventiva->execute()) {
			$stmtPreventiva->bind_result($programada);
			
			// Use uma condição if para verificar se há resultados
			if ($stmtPreventiva->fetch()) {
				// Faça algo com $programada, por exemplo, exibir ou processar
				//	echo "Última data de início da manutenção preventiva: " . $programada;
			} else {
				// Não foram encontrados registros
				//	echo "Nenhuma manutenção preventiva encontrada para o código $codigoget";
			}
		} else {
			// Tratar erro na execução da consulta
			//	echo "Erro na execução da consulta de manutenção preventiva: " . $stmtPreventiva->error;
		}
		
		$stmtPreventiva->close(); // Fechar o statement
	} else {
		// Tratar erro na preparação da consulta
		//	echo "Erro na preparação da consulta de manutenção preventiva: " . $conn->error;
	}
	
	// Buscar a periodicidade da rotina de manutenção
	$queryRotina = "SELECT periodicidade FROM maintenance_routine WHERE id = ?";
	if ($stmtRotina = $conn->prepare($queryRotina)) {
		$stmtRotina->bind_param("i", $codigoget); // Substitua "i" pelo tipo de dado correto de $codigoget
		
		if ($stmtRotina->execute()) {
			$stmtRotina->bind_result($periodicidade);
			
			// Use uma condição if para verificar se há resultados
			if ($stmtRotina->fetch()) {
				// Faça algo com $periodicidade, por exemplo, exibir ou processar
				//		echo "Periodicidade da rotina de manutenção: " . $periodicidade;
			} else {
				// Não foram encontrados registros
				//		echo "Nenhuma rotina de manutenção encontrada para o código $codigoget";
			}
		} else {
			// Tratar erro na execução da consulta
			//	echo "Erro na execução da consulta de rotina de manutenção: " . $stmtRotina->error;
		}
		
		$stmtRotina->close(); // Fechar o statement
	} else {
		// Tratar erro na preparação da consulta
		//	echo "Erro na preparação da consulta de rotina de manutenção: " . $conn->error;
	}
	
	
	
	
	
	
	
	$stmt = $conn->prepare("UPDATE maintenance_control SET data_start = ? WHERE id_routine= ?");
	$stmt->bind_param("ss",$programada,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	
	$query = "SELECT return_mp FROM tools";
	
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($return_mp);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	
	
	if($return_mp == 0){
		
		$query = "SELECT return_type FROM tools";
		
		
		if ($stmt = $conn->prepare($query)) {
			$stmt->execute();
			$stmt->bind_result($return_type);
			while ($stmt->fetch()) {
				//printf("%s, %s\n", $solicitante, $equipamento);
			}
		}
		
		
		$stmt = $conn->prepare("UPDATE maintenance_preventive SET return_mp = 0 WHERE id= ?");
		$stmt->bind_param("s",$id_mp_reurn);
		$execval = $stmt->execute();
		$stmt->close();
		
		if($return_type == 1){
			
			$query = "SELECT instituicao_area.return_user FROM instituicao_area LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id LEFT JOIN equipamento ON equipamento.id_instituicao_localizacao =  instituicao_localizacao.id LEFT JOIN  maintenance_routine ON maintenance_routine.id_equipamento = equipamento.id where maintenance_routine.id =  $routine";
			
			
			if ($stmt = $conn->prepare($query)) {
				$stmt->execute();
				$stmt->bind_result($return_user);
				while ($stmt->fetch()) {
					//printf("%s, %s\n", $solicitante, $equipamento);
				}
			}
			
			$stmt = $conn->prepare("UPDATE maintenance_preventive SET return_mp_id = ? WHERE id= ?");
			$stmt->bind_param("ss",$return_user,$id_mp_reurn);
			$execval = $stmt->execute();
			$stmt->close();
			
			
		}
		
		if($return_type == 2){
			
			$query = "SELECT return_user FROM maintenance_routine WHERE id = $id_mp_reurn";
			if ($stmt = $conn->prepare($query)) {
				$stmt->execute();
				$stmt->bind_result($return_user);
				while ($stmt->fetch()) {
					$periodicidade;
				}
			}
			
			$stmt = $conn->prepare("UPDATE maintenance_preventive SET return_mp_id = ? WHERE id= ?");
			$stmt->bind_param("ss",$return_user,$id_mp_reurn);
			$execval = $stmt->execute();
			$stmt->close();
			
			
		}
		
	}
	
	
	setcookie('anexo', null, -1);


$query = "SELECT cal FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($cal);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }
$data_start = $date_mp_start;
$val = $_GET["val"];
$id_manufacture = $_GET["id_manufacture"];
 
$id_equipamento = $_GET["equipamento_cal"];
$codigo = $_GET["codigo"];
$obs = $_GET["obs"];
$id_colaborador = $_GET["colaborador"];
$id_responsavel = $_GET["id_responsavel"];
$temp = $_GET["temp"];
$hum = $_GET["hum"];
$equipamento_tools = $_GET["equipamento_tools"];
$row = $_GET["row"];
$errop = "errop_";
$ac = "ac_";
$p = "p_";
$t = "t_";
$d = "d_";
$ve = "ve_";
$vl1 = "vl1_";
$vl2 = "vl2_";
$vl3 = "vl3_";
$vl_avg = "vl_avg_";
$erro = "erro_";
$dp = "dp_";
$ia = "ia_";
$ib = "ib_";
$ic = "ic_";
$k = "k_";
$ie = "ie_";
$erroac = "erroac_";
$status_ = "status_";
$ven= 0;
$status= 0;
$procedure_cal=0;
//if($obs==""){
 //   $obs="Este certificado &#233; v&#225;lido para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar.";
//}

$stmt = $conn->prepare("INSERT INTO calibration (id_equipamento, data_start, val, ven, status, id_manufacture, procedure_cal, id_colaborador, id_responsavel, temp, hum, obs,codigo) VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?,?,?,?)");
$stmt->bind_param("sssssssssssss",$id_equipamento, $data_start, $val, $ven, $status, $id_manufacture, $procedure_cal, $id_colaborador, $id_responsavel, $temp, $hum, $obs,$codigo);
$execval = $stmt->execute();
 $last_id = $conn->insert_id;
 $stmt->close();


 

 $stmt = $conn->prepare("UPDATE  equipamento SET data_calibration = ? WHERE id= ?");
 $stmt->bind_param("ss",$data_start,$id_equipamento);
 $execval = $stmt->execute();
 $stmt->close();

 $stmt = $conn->prepare("UPDATE  equipamento SET id_calibration = ? WHERE id= ?");
 $stmt->bind_param("ss",$last_id,$id_equipamento);
 $execval = $stmt->execute();
 $stmt->close();

    $data_calibration_end=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));

 $stmt = $conn->prepare("UPDATE  equipamento SET data_calibration_end = ? WHERE id= ?");
 $stmt->bind_param("ss",$data_calibration_end,$id_equipamento);
 $execval = $stmt->execute();
 $stmt->close();


$row = trim($row);
$line = $row+1;
 
for ($x = 0; $x <= $line; $x++) {
  
    $arrayp = $p.$x;
    $arrayp = $_GET["$arrayp"];
    $arrayt = $t.$x;
    $arrayt = $_GET["$arrayt"];
    $arrayd = $d.$x;
    $arrayd = $_GET["$arrayd"];
    $arrayve = $ve.$x;
    $arrayve = $_GET["$arrayve"];
    $arrayvl1 = $vl1.$x;
    $arrayvl1 = $_GET["$arrayvl1"];
    $arrayvl2 = $vl2.$x;
    $arrayvl2 = $_GET["$arrayvl2"];
    $arrayvl3 = $vl3.$x;
    $arrayvl3 = $_GET["$arrayvl3"];
    $arrayvl_avg = $vl_avg.$x;
    $arrayvl_avg = $_GET["$arrayvl_avg"];
    $arrayerro = $erro.$x;
    $arrayerro = $_GET["$arrayerro"];
    $arraydp = $dp.$x;
    $arraydp = $_GET["$arraydp"];
    $arrayia = $ia.$x;
    $arrayia = $_GET["$arrayia"];
    $arrayib = $ib.$x;
    $arrayib = $_GET["$arrayib"];
    $arrayic = $ic.$x;
    $arrayic = $_GET["$arrayic"];
    $arrayk = $k.$x;
    $arrayk = $_GET["$arrayk"];
    $arrayie = $ie.$x;
    $arrayie = $_GET["$arrayie"];
    $arrayerroac = $erroac.$x;
    $arrayerroac = $_GET["$arrayerroac"];
    $arraystatus = $status_.$x;
    $arraystatus = $_GET["$arraystatus"];
     
 if (!empty($arrayve) && is_numeric($arrayve)) {
    
    if($arraystatus == "AP"){
      $status = 0;
    }
    if($arraystatus == "RP"){
      $status = 1;
    }
    
    
    if($arraystatus == "RP"){
      $arraystatus = 1;
      $stmt = $conn->prepare("UPDATE calibration SET status = 1 WHERE id= ?");
      $stmt->bind_param("s",$last_id);
      $execval = $stmt->execute();
      $stmt->close();
      
    }   
    
    if($arrayd != ""){
      $stmt = $conn->prepare("INSERT INTO calibration_report (id_calibration, id_calibration_parameter_tools, id_calibration_parameter_tools_dados, id_calibration_parameter_dados_parameter, ve, vl_1, vl_2, vl_3, vl_avg, erro, dp, ie, ia, k, ib, ic, status, erro_ac) VALUES (?, ?,?, ?, ?,?, ?,?, ?, ?, ?, ?, ?, ?, ?,?,?,?)");
      $stmt->bind_param("ssssssssssssssssss",$last_id, $arrayt, $arrayd, $arrayp, $arrayve, $arrayvl1, $arrayvl2, $arrayvl3, $arrayvl_avg, $arrayerro, $arraydp, $arrayie,$arrayia,$arrayk,$arrayib,$arrayic,$status,$arrayerroac);
      $execval = $stmt->execute();
      $stmt->close();
      
    }
    
    
  }



}
	$stmt = $conn->prepare("INSERT INTO  regdate_mp_signature (id_mp, id_user, id_signature) VALUES (?, ?, ?)");
	$stmt->bind_param("sss",$id_mp_reurn,$usuario,$v4uuid_obs);
	$execval = $stmt->execute();
	$stmt->close();
	
if($cal == "0"){ 

echo "<script>document.location='../maintenance-preventive?sweet_salve=1'; window.open('../calibration-report-viewer-copy-print.php?laudo=$last_id', '_blank');</script>";
 
} else {
echo "<script>document.location='../maintenance-preventive?sweet_salve=1'</script>";
}


 
	
	
	
?>

