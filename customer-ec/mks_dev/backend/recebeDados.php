<?php
include("../database/database.php");
$codigoget = 3730;
  $tabela = json_decode($_POST['dados_tabela'], true);
  // faça algo com a $tabela

  $stmt = $conn->prepare("UPDATE maintenance_preventive SET digital= ? WHERE id= ?");
$stmt->bind_param("ss",$tabela,$codigoget);
$execval = $stmt->execute();
$stmt->close();

 //
/*
   $data terá o seguinte valor:
   array(
      array(
          "Codigo": 1
          , "Nome": "Carlos"
      )
      ... os demais nomes
   )

   É só percorrer o array com um laço e gravar no banco
*/
// Retorna a confirmação para o javascript
//echo json_encode(array('success' => true));

echo  "<script>alert('Atualizado!');location.href=\"../maintenance-preventive-shut-down?routine=$codigoget\";</script>";

?>
