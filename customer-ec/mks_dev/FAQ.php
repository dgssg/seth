<?php 
$codigoget = ($_GET["laudo"]);
//namespace Phppot;

use Phppot\Datasource;

class FAQ
{
    private $ds;

    function __construct()
    {
        require_once __DIR__ . 'database/DataSource.php';
        $this->ds = new DataSource();
    }

    /**
     * to get the interview questions
     *
     * @return array result record
     */
    function getFAQ()
    {
        $query = "SELECT * FROM calibration_report WHERE id_calibration = $codigoget ";
        $result = $this->ds->select($query);
        return $result;
    }

    /**
     * to edit redorcbased on the question_id
     *
     * @param string $columnName
     * @param string $columnValue
     * @param string $questionId
     */
    function editRecord($columnName, $columnValue, $questionId)
    {
        $query = "UPDATE calibration_report set " . $columnName . " = ? WHERE  id = ?";

        $paramType = 'si';
        $paramValue = array(
            $columnValue,
            $questionId
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }
}

