<?php
	include("../database/database.php");
	
	// Verifique se o ID e a rotina foram recebidos
	if (isset($_POST['id']) && isset($_POST['routine'])) {
		$id = $_POST['id'];
		$routine = $_POST['routine'];
		
		
		$stmt = $conn->prepare("DELETE FROM regdate_mp_registro_customer WHERE  id= ?");
		$stmt->bind_param("i",$id);
		$execval = $stmt->execute();
		$stmt->close();

		// Coloque aqui a lógica de exclusão no banco de dados
		// Substitua este exemplo com a lógica do seu aplicativo
		// Exemplo: $result = mysqli_query($conn, "DELETE FROM sua_tabela WHERE id = $id");
		
		// Se a exclusão for bem-sucedida, envie uma resposta JSON de sucesso
		// Se estiver usando MySQLi, você pode verificar $result
		$response = array('success' => true);
		echo json_encode($response);
	} else {
		// Se os parâmetros necessários não foram fornecidos, envie uma resposta JSON de erro
		$response = array('error' => 'Parâmetros inválidos');
		echo json_encode($response);
	}
?>

