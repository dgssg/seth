<?php

include("../database/database.php");

$codigoget= $_GET['routine'];
$time_ms= $_POST['time_ms'];
$periodicidade= $_POST['periodicidade'];
$habilitado= $_POST['habilitado'];
$categoria= $_POST['categoria'];
$colaborador= $_POST['colaborador'];
$fornecedor= $_POST['fornecedor'];
$procedimento_1= $_POST['procedimento_1'];
$procedimento_2= $_POST['procedimento_2'];
$pop= $_POST['pop'];

$stmt = $conn->prepare("UPDATE calibration_routine SET time_ms = ? WHERE id= ?");
$stmt->bind_param("ss",$time_ms,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET periodicidade = ? WHERE id= ?");
$stmt->bind_param("ss",$periodicidade,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET habilitado = ? WHERE id= ?");
$stmt->bind_param("ss",$habilitado,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET id_category = ? WHERE id= ?");
$stmt->bind_param("ss",$categoria,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET id_colaborador = ? WHERE id= ?");
$stmt->bind_param("ss",$colaborador,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET id_fornecedor = ? WHERE id= ?");
$stmt->bind_param("ss",$fornecedor,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET id_calibration_procedures_before = ? WHERE id= ?");
$stmt->bind_param("ss",$procedimento_1,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET id_calibration_procedures_after = ? WHERE id= ?");
$stmt->bind_param("ss",$colaborador,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_routine SET id_pop= ? WHERE id= ?");
$stmt->bind_param("ss",$pop,$codigoget);
$execval = $stmt->execute();
echo "<script>alert('Alteração Registrado!');document.location='../flow-calibration-routine-edit?routine=$codigoget'</script>";

header('Location: ../flow-calibration-routine-edit?routine='.$codigoget);

?>
