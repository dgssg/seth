<?php

include("../database/database.php");
$codigoget= $_GET['id'];

$iten = $_POST['iten'];
$un= $_POST['un'];
$qtd = $_POST['qtd'];
$vlr = $_POST['vlr_print'];
 
	
	// Agora, $vlr contém a string sem espaços no início e sem o símbolo "R$"

	
	// Agora, $vlr contém apenas o valor numérico

	$vlr = trim($vlr);
	
	// Remove o símbolo "R$"
	$vlr = str_replace('R$', '', $vlr);
	
	// Remove espaços não convencionais no início da string
	$vlr = preg_replace('/^\s+/u', '', $vlr);

 
//echo "New records created successfully";

	$stmt = $conn->prepare("UPDATE budget_itens SET iten= ? WHERE id= ?");
	$stmt->bind_param("ss",$iten,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE budget_itens SET un= ? WHERE id= ?");
	$stmt->bind_param("ss",$un,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE budget_itens SET qtd= ? WHERE id= ?");
	$stmt->bind_param("ss",$qtd,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE budget_itens SET vlr= ? WHERE id= ?");
	$stmt->bind_param("ss",$vlr,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
//header('Location: ../purchases-progress-edit?purchases='.$codigoget);
echo "<script>document.location='../budget-edit?sweet_salve=1&div_itens=1&id=$codigoget'</script>";

?>