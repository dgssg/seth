<style>
  /* Estilo para os botões em mosaico */
  .btn-group-mosaic {
    display: grid;
    grid-template-columns: repeat(4, auto);
    gap: 4px;
  }
  
  .btn-group-mosaic .btn {
    padding: 4px 6px;
    font-size: 12px;
    width: 100%;
    text-align: center;
  }
</style> 
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Equipamento</small></h3>
              </div>


            </div>

 <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                   <a class="btn btn-app"  href="register-equipament" style="border: 2px solid #ffcc00; padding: 5px;">
                    <i class="glyphicon glyphicon-list-alt"></i> Equipamentos
                  </a>
                    <a class="btn btn-app"  href="register-equipament-register">
                    <i class="glyphicon glyphicon-plus"></i> Cadastro
                  </a>

                  <a class="btn btn-app"  href="register-equipament-group">
                    <i class="fa fa-cog"></i> Grupo
                  </a>

                  <a class="btn btn-app"  href="register-equipament-family">
                    <i class="fa fa-cogs"></i> Familia
                  </a>
                  <a class="btn btn-app"  href="register-equipament-family-movement">
                    <i class="fa fa-eject"></i> Saida de Equipamento
                  </a>
                  <a class="btn btn-app"  href="register-equipament-family-movement-list">
                    <i class="fa fa-list-ol"></i> Historico de Saida de Equipamento
                  </a>
                  <a class="btn btn-app"  href="register-equipament-form">
                    <i class="fa fa-list-ol"></i> Formulario
                  </a>
                  <a class="btn btn-app"  href="register-equipament-location-register">
                    <i class="fa fa-cogs"></i> Localização
                  </a>
                 

                </div>
              </div>

              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form id="filter-form" class="container">
                      <div class="form-row">
                      <?php if($assistence == "0"){  ?>
                        <div class="col-md-3">
                          <label for="unidade">Empresa</label>
                          <input type="text" id="unidade" name="unidade" class="form-control">
                        </div>
                        <?php }else {   ?>
                        <div class="col-md-3">
                          <label for="unidade">Unidade</label>
                          <input type="text" id="unidade" name="unidade" class="form-control">
                        </div>
                        <?php }  ?>
                        <?php if($assistence == "0"){  ?>
                        <div class="col-md-3">
                          <label for="setor">Cliente</label>
                          <input type="text" id="setor" name="setor" class="form-control">
                        </div>
                        <?php }else {   ?>
                        <div class="col-md-3">
                          <label for="setor">Setor</label>
                          <input type="text" id="setor" name="setor" class="form-control">
                        </div>
                        <?php }  ?>
                        <?php if($assistence == "0"){  ?>
                        <div class="col-md-3">
                          <label for="area">Localização</label>
                          <input type="text" id="area" name="area" class="form-control">
                        </div>
                        <?php }else {   ?>
                        <div class="col-md-3">
                          <label for="area">Área</label>
                          <input type="text" id="area" name="area" class="form-control">
                        </div>
                        <?php }  ?>
                        <div class="col-md-3">
                          <label for="grupo">Grupo de Equipamento</label>
                          <input type="text" id="grupo" name="grupo" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3">
                          <label for="familia">Família de Equipamento</label>
                          <input type="text" id="familia" name="familia" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="fabricante">Fabricante</label>
                          <input type="text" id="fabricante" name="fabricante" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="modelo">Modelo</label>
                          <input type="text" id="modelo" name="modelo" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="codigo">Código</label>
                          <input type="text" id="codigo" name="codigo" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3">
                          <label for="patrimonio">Patrimônio</label>
                          <input type="text" id="patrimonio" name="patrimonio" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <label for="serie">Série</label>
                          <input type="text" id="serie" name="serie" class="form-control">
                        </div>
                      </div>
                        <div class="form-row">
                          <div class="col-md-2">
                          <label for="ativo">Ativo</label>
                          <select id="ativo" name="ativo" class="form-control">
                            <option value="">Selecione</option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <label for="baixa">Baixa</label>
                          <select id="baixa" name="baixa" class="form-control">
                            <option value="">Selecione</option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                          </select>
                        </div>
                     
                        <div class="col-md-2">
                          <label for="comodato">Comodato</label>
                          <select id="comodato" name="comodato" class="form-control">
                            <option value="">Selecione</option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                          </select>
                        </div>
                       
                     
                
                 
                      
                        <div class="col-md-3">
                          <button type="button" class="btn btn-primary mt-4" onclick="fetchData()">Filtrar</button>
                        </div>

                      </div>
                  
                    </form>
                    <script>
                      $(document).ready(function() {
                        $('#ativo, #baixa, #comodato').select2();
                      });
                      
                      function fetchData() {
                        // Função de filtro a ser implementada
                      }
                    </script>
                   
                  </div>
                </div>
              </div>
            </div>



	       <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta <small> Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
      

                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    
                    <div class="col-md-11 col-sm-11 ">
                      <div class="x_panel">
                        
                        <div class="x_content">
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="card-box table-responsive">
                                
                                
                                <table id="result-table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                  <thead>
                                    <tr>
<th><input type="checkbox" id="check_all"></th>           
                                      <th>Ação</th>
                                         <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                                        <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                                        <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  
                                      <th>Grupo</th>
                                      <th>Codigo</th>
                                      <th>Equipamento</th>
                                      <th>Modelo</th>
                                      <th>Fabricante</th>
                                      <th>Série</th>
                                      <th>Patrimônio</th>
                                      <th>Ativo</th>
                                      <th>Baixa</th>
                                      <th>Comodato</th>
                                      <th>Anvisa</th>
                                      <th>Nº</th>
                                                                           

                                    </tr>
                                  </thead>
                                  <tbody>
                                    <!-- Os resultados serão inseridos aqui -->
                                  </tbody>
                                </table>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  
                    

                    <script>
                      let badgeHTML = '';
                      let badgeHTMLBaixa = '';
                      let badgeHTMLcheckist= '';
                      let badgeHTMLtreinamento= '';
                      let badgeHTMLparecer_1= '';
                      let badgeHTMLparecer_2= '';
                      let badgeHTMLsaida_1= '';
                      let badgeHTMLsaida_2= '';
                      let badgeHTMLconformidade= '';

                      function fetchData() {
                        // Obtenha os valores dos filtros
                       
                        const unidade = document.getElementById('unidade').value;
                        const setor = document.getElementById('setor').value;
                        const area = document.getElementById('area').value;
                        const familia = document.getElementById('familia').value;
                        const fabricante = document.getElementById('fabricante').value;
                        const modelo = document.getElementById('modelo').value;
                        const serie = document.getElementById('serie').value;
                        const codigo = document.getElementById('codigo').value;
                        const patrimonio = document.getElementById('patrimonio').value;
                        const baixa = document.getElementById('baixa').value;
                        const grupo = document.getElementById('grupo').value;
                        const ativo = document.getElementById('ativo').value;
                        const comodato = document.getElementById('comodato').value;

                        // Monta a URL com parâmetros
                        let url = 'table/table-search-equipamento-filter.php?';
                        if (grupo) url += `grupo=${encodeURIComponent(grupo)}&`;  if (unidade) url += `unidade=${encodeURIComponent(unidade)}&`;
                        if (setor) url += `setor=${encodeURIComponent(setor)}&`;
                        if (area) url += `area=${encodeURIComponent(area)}&`;
                        if (familia) url += `familia=${encodeURIComponent(familia)}&`;  if (fabricante) url += `fabricante=${encodeURIComponent(fabricante)}&`;  if (modelo) url += `modelo=${encodeURIComponent(modelo)}&`;  if (serie) url += `serie=${encodeURIComponent(serie)}&`;  if (codigo) url += `codigo=${encodeURIComponent(codigo)}&`;  if (patrimonio) url += `patrimonio=${encodeURIComponent(patrimonio)}&`;       if (comodato) url += `comodato=${encodeURIComponent(comodato)}&`; if (baixa) url += `baixa=${encodeURIComponent(baixa)}&`;
                        if (ativo) url += `ativo=${ativo}`;
                        
                        // Faz a requisição
                        fetch(url)
                        .then(response => response.json())
                        .then(data => {
                          // Limpa a tabela de resultados
                          const tbody = document.getElementById('result-table').getElementsByTagName('tbody')[0];
                          tbody.innerHTML = '';
                          
                          // Insere as linhas na tabela
                          data.forEach(row => {
                            
                            const tr = document.createElement('tr');
                            tr.innerHTML = `

              <td>
  <input type="checkbox" class="checkbox" value="${row.id}">

  
</td>
<td><div class="btn-group-mosaic">

  <!-- Grupo 1: Funções 1 a 4 -->
  <div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 1
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="register-equipament-edit?equipamento=${row.id}" onclick="new PNotify({ title: 'Editar', text: 'Abrindo Edição', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-edit"></i> Editar
      </a>
      <a class="dropdown-item" href="register-equipament-tag?equipamento=${row.id}" target="_blank" onclick="new PNotify({ title: 'Etiqueta', text: 'Abrir Etiqueta', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-qrcode"></i> Etiqueta
      </a>
      <a class="dropdown-item" href="register-equipament-label?equipamento=${row.id}" target="_blank" onclick="new PNotify({ title: 'Etiqueta', text: 'Abrir Etiqueta', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-fax"></i> Etiqueta
      </a>
      <a class="dropdown-item" href="register-equipament-record?equipamento=${row.id}" target="_blank" onclick="new PNotify({ title: 'Ficha', text: 'Abrir Ficha', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file"></i> Ficha
      </a>
                        <a  class="dropdown-item"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-equipament-trash-backend?id=${row.id}';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
    </div>
  </div>

  <!-- Grupo 2: Funções 5 a 8 -->
  <div class="btn-group">
    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 2
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="register-equipament-prontuario?equipamento=${row.id}" target="_blank" onclick="new PNotify({ title: 'Prontuario', text: 'Abrir Prontuario', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-o"></i> Prontuário
      </a>
      <a class="dropdown-item" href="register-equipament-disable?equipamento=${row.id}" onclick="new PNotify({ title: 'Baixa', text: 'Baixa Equipamento', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-trash"></i> Baixa ${row.baixa ? '<span class="badge badge-success">OK</span>' : ''}
      </a>
      <a class="dropdown-item" href="dropzone/nf/${row.nf_dropzone}" target="_blank" onclick="new PNotify({ title: 'Prontuario', text: 'Abrir Prontuario', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-o"></i> Nota Fiscal ${row.nf_dropzone ? '<span class="badge badge-success">OK</span>' : ''}
      </a>    
      <a class="dropdown-item" href="register-equipament-check?equipamento=${row.id}" onclick="new PNotify({ title: 'Check List', text: 'Abrir Check List', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-text-o"></i> Check List ${row.checklist ? '<span class="badge badge-success">OK</span>' : ''}
      </a>
    </div>
  </div>

  <!-- Grupo 3: Funções 9 a 12 -->
  <div class="btn-group">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 3
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="register-equipament-training?equipamento=${row.id}" onclick="new PNotify({ title: 'Treinamento', text: 'Abrir Treinamento', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-text-o"></i> Treinamento ${row.treinamento ? '<span class="badge badge-success">OK</span>' : ''}
      </a>
      <a class="dropdown-item" href="register-equipament-report?equipamento=${row.id}" onclick="new PNotify({ title: 'Parecer', text: 'Abrir Parecer', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-text-o"></i> Parecer
      </a>
      <a class="dropdown-item" href="register-equipament-exit?equipamento=${row.id}" onclick="new PNotify({ title: 'Saida', text: 'Abrir Saida', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-text-o"></i> Saída
      </a>
      <a class="dropdown-item" href="register-equipament-compliance?equipamento=${row.id}" onclick="new PNotify({ title: 'Conformidade', text: 'Abrir Conformidade', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-file-text-o"></i> Conformidade
      </a>
    </div>
  </div>

  <!-- Grupo 4: Funções 13 a 16 -->
  <div class="btn-group">
    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Grupo 4
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="register-equipament-timeline?equipamento=${row.id}" onclick="new PNotify({ title: 'Timeline', text: 'Abrir Timeline', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-hourglass"></i> Timeline
      </a>
      <a class="dropdown-item" href="register-equipament-location?equipamento=${row.id}" onclick="new PNotify({ title: 'Localização', text: 'Abrir Localização', type: 'success', styling: 'bootstrap3' });">
        <i class="fa fa-cogs"></i> Localização
      </a>
      <a class="dropdown-item" href="https://consultas.anvisa.gov.br/#/genericos/q/?numeroRegistro=${row.anvisa}" target="_blank" onclick="new PNotify({ title: 'Anvisa', text: 'Abrir Anvisa', type: 'success', styling: 'bootstrap3' });">
        <i class="glyphicon glyphicon-exclamation-sign"></i> Anvisa
      </a>
      <a class="dropdown-item" onclick="open('report-equipament-family-anvisa?id=${row.anvisa}','report-equipament-family-anvisa?id=${row.anvisa}','status=no,Width=320,Height=285');">
        <i class="glyphicon glyphicon-info-sign"></i> Família Anvisa
      </a>
    </div>
  </div>

</div>
</td>

              <td>${row.instituicao}</td>
              <td>${row.area}</td>
              <td>${row.setor}</td>
            
              <td>${row.grupo}</td>
              <td>${row.codigo}</td>
              <td>${row.equipamento}</td>
              <td>${row.modelo}</td>
              <td>${row.fabricante}</td>
              <td>${row.serie}</td>
              <td>${row.patrimonio}</td>
              <td>${row.ativo ? 'Sim' : 'Não'}</td>
              <td>${row.baixa ? 'Sim' : 'Não'}</td>
              <td>${row.comodato ? 'Sim' : 'Não'}</td>
              <td>${row.anvisa}</td>
              <td>${row.id}</td>
             
 
            `;
                            tbody.appendChild(tr);
                          });
                        })
                        .catch(error => console.error('Erro ao buscar dados:', error));
                      }
                      function takeAction() {
                        var selectedRows = [];
                        $('.checkbox:checked').each(function() {
                          selectedRows.push($(this).val());
                        });
                        
                        // Verifica se pelo menos um item foi selecionado
                        if (selectedRows.length > 0) {
                          // Constrói o URL com os IDs dos itens selecionados
                          var url = 'register-equipament-tag-selected?equipamento=' + selectedRows.join(',');
                          // Abre o URL em uma nova aba
                          window.open(url, '_blank');
                        } else {
                          // Se nenhum item foi selecionado, mostra uma mensagem
                          alert('Por favor, selecione pelo menos um item.');
                        }
                      }
                      $('#check_all').click(function() {
                        $('input[type=checkbox]').prop('checked', this.checked); // Seleciona todas as checkboxes
                      });
                      $('#datatable').dataTable( {
                        "processing": true,
                        "stateSave": true,
                        responsive: true,
                        
                        
                        
                        "language": {
                          "loadingRecords": "Carregando dados...",
                          "processing": "Processando  dados...",
                          "infoEmpty": "Nenhum dado a mostrar",
                          "emptyTable": "Sem dados disponíveis na tabela",
                          "zeroRecords": "Não há registros a serem exibidos",
                          "search": "Filtrar registros:",
                          "info": "Mostrando página _PAGE_ de _PAGES_",
                          "infoFiltered": " - filtragem de _MAX_ registros",
                          "lengthMenu": "Mostrar _MENU_ registros",
                          
                          "paginate": {
                            "previous": "Página anterior",
                            "next": "Próxima página",
                            "last": "Última página",
                            "first": "Primeira página",
                            
                            
                            
                          }
                        }
                        
                        
                        
                      } );
                    </script>


                  </div>
                </div>
              </div>
	       </div>

	     
 

                </div>
              </div>


        <!-- /page content -->
      