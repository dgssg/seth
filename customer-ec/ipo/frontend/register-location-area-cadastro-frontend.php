	<?php
include("database/database.php");

?>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
  <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
  
	 <form  action="backend/register-location-area-cadastro-backend.php" method="post">

	 <div class="item form-group">
                         <?php if($assistence == "0"){  ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Empresa <span class="required"></span>
        </label>
        <?php }else {   ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
        </label>            
        <?php }  ?>	
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="instituicao" id="instituicao"  placeholder="Unidade">
										 <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	
										  	<?php



										   $sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$instituicao);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($instituicao);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
											</div>

								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#instituicao').select2();
                                      });
                                 </script>

		 

					<div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Centro de Custo<span
                          ></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control"  type="text" name="custo"  id="custo" ></div>
                    </div>
					<div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Nome<span
                          ></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control"  type="text" name="nome"  id="nome" ></div>
                    </div>
					<div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Codigo<span
                          ></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control"  type="text" name="codigo"  id="codigo" ></div>
                    </div>
                                     <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align">Cadastro de Area</label>
    <div class="col-md-6 col-sm-6">
        <div class="">
            <label>
            <input type="hidden" name="auto_1" value="1">
                <input name="auto_1_checkbox" type="checkbox" class="js-switch" id="auto_1" value="0"
                    <?php if($auto_1 == "0"){printf("checked"); }?> />
            </label>
        </div>
    </div>
</div>

<div class="field item form-group" id="nome_area_field" style="display:none;">
    <label class="col-form-label col-md-3 col-sm-3 label-align">Nome<span></span></label>
    <div class="col-md-6 col-sm-6">
        <input class="form-control" type="text" name="nome_area" id="nome_area">
    </div>
</div>

<div class="item form-group" id="auto_2_field" style="display:none;">
    <label class="col-form-label col-md-3 col-sm-3 label-align">Cadastro de Acessórios</label>
    <div class="col-md-6 col-sm-6">
        <div class="">
            <label>
            <input type="hidden" name="auto_2" value="1">
                <input name="auto_2_checkbox" type="checkbox" class="js-switch" id="auto_2" value="0"
                    <?php if($auto_2 == "0"){printf("checked"); }?> />
            </label>
        </div>
    </div>
</div>

<div class="item form-group" id="equipamento_field" style="display:none;">
    <label class="col-form-label col-md-3 col-sm-3 label-align">Equipamento<span></span></label>
     <div class="col-md-6 col-sm-6">
        <select multiple="multiple" type="text" class="form-control col-md-6 col-sm-6" name="equipamento[]" id="equipamento" >
          
            <option value="" >Selecione os equipamentos na lista</option>
            <?php
                $sql = "SELECT equipamento_familia.id, equipamento_familia.nome, equipamento_familia.fabricante, equipamento_familia.modelo
                        FROM equipamento_familia
                        INNER JOIN equipamento_grupo ON equipamento_grupo.id = equipamento_familia.id_equipamento_grupo
                        WHERE equipamento_grupo.accessories = 0 AND equipamento_familia.trash != 0";

                if ($stmt = $conn->prepare($sql)) {
                    $stmt->execute();
                    $stmt->bind_result($id, $nome, $fabricante, $modelo);
                    while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>"><?php printf($nome);?> - <?php printf($fabricante);?> - <?php printf($modelo);?></option>
                        <?php
                    }
                }
                $stmt->close();
            ?>
        </select>
        	 <script>
                                    $(document).ready(function() {
                                    $('#equipamento').select2();
                                      });
                                 </script>

       
    </div>
</div>

<script>
    $(document).ready(function() {
        // Controla a exibição do campo nome_area e do checkbox auto_2
        $('#auto_1').change(function() {
            if($(this).is(':checked')) {
                $('#nome_area_field').show();
                $('#auto_2_field').show();
            } else {
                $('#nome_area_field').hide();
                $('#auto_2_field').hide();
                $('#auto_2').prop('checked', false);  // Desmarcar o auto_2 quando o auto_1 for desmarcado
                $('#equipamento_field').hide();       // Esconder o campo de equipamentos também
            }
        });

        // Controla a exibição do campo equipamento
        $('#auto_2').change(function() {
            if($(this).is(':checked')) {
                $('#equipamento_field').show();
            } else {
                $('#equipamento_field').hide();
            }
        });

        // Inicializa o select2
       // $('#equipamento').select2();

        // Verifica o estado inicial dos checkboxes
        if($('#auto_1').is(':checked')) {
            $('#nome_area_field').show();
            $('#auto_2_field').show();
        }
        if($('#auto_2').is(':checked')) {
            $('#equipamento_field').show();
        }
    });
</script>

					<div class="ln_solid"></div>

											<button type="reset" class="btn btn-primary"onclick="new PNotify({
																title: 'Limpado',
																text: 'Todos os Campos Limpos',
																type: 'info',
																styling: 'bootstrap3'
														});" />Limpar</button>
											<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar"/>

												</form>
														<div class="ln_solid"></div>
