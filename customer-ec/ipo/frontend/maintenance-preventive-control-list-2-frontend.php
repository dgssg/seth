<?php
include("database/database.php");
$query = "SELECT * FROM maintenance_control_2 ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$rotina_id,$data_start, $data_after,$status,$procedure_mp,$reg_date,$upgrade);



  ?>


  <div class="col-md-11 col-sm-11 ">
    <div class="x_panel">

      <div class="x_content">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box table-responsive">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>

                    <th>Rotina</th>
                    <th>Programada</th>
                    <th>Proxima</th>
                    <th>Status</th>
                    <th>Procedimento</th>
                    <th>Atualização</th>
                    <th>Cadastro</th>


                    <th>Ação</th>

                  </tr>
                </thead>


                <tbody>
                  <?php   while ($stmt->fetch()) {   ?>
                    <tr>
                      <td ><?php printf($id); ?> </td>
                      <td ><?php printf($rotina_id); ?> </td>
                      <td ><?php printf($data_start); ?> </td>
                      <td ><?php printf($data_after); ?> </td>
                      <td><?php if($status==0){printf("Programada");}if($status==1){printf("Aberta");} ?></td>
                        <td><?php printf($procedure_mp); ?></td>
                      <td><?php printf($upgrade); ?></td>
                      <td><?php printf($reg_date); ?></td>


                      <td>

                        <a class="btn btn-app"  href="maintenance-preventive-viwer?routine=<?php printf($rotina_id); ?>" target="_blank"  onclick="new PNotify({
                          title: 'Visualizar',
                          text: 'Visualizar Rotina',
                          type: 'info',
                          styling: 'bootstrap3'
                        });">
                        <i class="fa fa-file-pdf-o"></i> Visualizar
                      </a>

                      <a class="btn btn-app"  href="maintenance-preventive-open-tag?id=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                        title: 'Visualizar',
                        text: 'Visualizar Etiqueta',
                        type: 'info',
                        styling: 'bootstrap3'
                      });">
                      <i class="fa fa-qrcode"></i> Etiqueta
                    </a>


                    <a class="btn btn-app"  href="maintenance-preventive-viwer-print?routine=<?php printf($rotina_id); ?>" target="_blank"  onclick="new PNotify({
                      title: 'Imprimir',
                      text: 'Imprimir Rotina',
                      type: 'info',
                      styling: 'bootstrap3'
                    });">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
                  <a class="btn btn-app"  href="backend/maintenance-routine-retweet-backend.php?routine=<?php printf($rotina); ?>" target="_blank"   onclick="new PNotify({
                    title: 'Manutenção Preventiva',
                    text: 'Manutenção Preventiva Retroativo',
                    type: 'warning',
                    styling: 'bootstrap3'
                  });">
                  <i class="fa fa-retweet"></i> MP Retroativo
                </a>
                <a class="btn btn-app"  href="backend/maintenance-routine-fast-forward-backend.php?routine=<?php printf($rotina); ?>" target="_blank"   onclick="new PNotify({
                  title: 'Manutenção Preventiva',
                  text: 'Manutenção Preventiva Antecipada',
                  type: 'warning',
                  styling: 'bootstrap3'
                });">
                <i class="fa fa-fast-forward"></i> MP Antecipada
              </a>
            </td>
          </tr>
        <?php   } }  ?>
      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>
</div>
