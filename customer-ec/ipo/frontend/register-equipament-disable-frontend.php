  <?php
$codigoget = ($_GET["equipamento"]);
    
    $sweet_salve = ($_GET["sweet_salve"]);
    $sweet_delet = ($_GET["sweet_delet"]);
    if ($sweet_delet == 1) {
      // Redirecionamento para a âncora "registro" usando JavaScript
      echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
      
      
    }
    if ($sweet_salve == 1) {
      // Redirecionamento para a âncora "registro" usando JavaScript
      echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
      
      
    }
// Create connection

include("database/database.php");
$query = "SELECT equipamento_familia.anvisa,equipamento.baixa,equipamento.ativo,equipamento.inventario,equipamento.comodato,equipamento.duplicidade,equipamento.preventive,equipamento.obsolescence,equipamento.rfid,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento.serie,equipamento.patrimonio,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao WHERE equipamento.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($anvisa,$baixa,$ativo,$inventario,$comodato,$duplicidade,$preventive,$obsolecencia,$rfid,$data_val,$nf,$data_fab,$data_instal,$serie,$patrimonio,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }
 

}
?>
<?php
    function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
        $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
        $con = 1;
        $var = '';
        while($con < $QuantidadeDeCaracteresDaChave ){
            $n = rand(0, 57); 
            if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
        }else{
            $var = $var.$n.$res[$n];
            $con++;
            }
        }
            $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
        }
        //chamando a função.
       // echo chaveAlfaNumerica(5);
     //   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
 * doing:
 *
 * $uniqid = $prefix . uniqid();
 * $uniqid = uniqid($prefix);
 
printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
 * required on some systems, like Cygwin. This makes uniqid()
 * produce a value like: 4b340550242239.64159797
 
printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
  public static function v3($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = md5($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 3
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function v4() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),

      // 16 bits for "time_mid"
      mt_rand(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  public static function v5($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = sha1($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 5
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function is_valid($uuid) {
    return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                      '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  }
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();
//echo $v4uuid; //chave da assinatura
?>

  <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script> 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Desabilitar <small>Equipamento</small></h3>
              </div>

           
            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Dados <small>Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($nome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Modelo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text"  value="<?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Fabricante <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($fabricante); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="anvisa">Anvisa <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($anvisa); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"readonly="readonly"  required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="patrimonio">Patromonio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="patrimonio" name="patrimonio" value="<?php printf($patrimonio); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="serie">N/S <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="serie" name="serie" value="<?php printf($serie); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_instal" name="data_instal" value="<?php printf($data_instal); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_fab">Fabricação  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_fab" name="data_fab" value="<?php printf($data_fab); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_val">Garantia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_val" name="data_val" value="<?php printf($data_val); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">Observação <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="obs" name="obs" value="<?php printf($obs); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                     


                  </div>
                </div>
              </div>
	       </div>
	      
	  <?php


// Create connection

include("database/database.php");
$query = "SELECT id,id_equipamento,date_disable,id_disable,des,file,upgrade,reg_date FROM equipament_disable WHERE id_equipamento like '$codigoget' order by id DESC limit 1 ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$id_equipamento,$data_disable,$id_disable,$des,$file,$upgrade,$reg_date);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }
 

}
?>       
	       
  <div class="x_panel">
                <div class="x_title">
                  <h2>Anexar</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <form  class="dropzone" action="backend/register-equipament-disable-upload-backend.php" method="post">
    </form >
                  
                </div>
              </div>	       
	       
            <div class="clearfix"></div>
  <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Anexo <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">
                      <div class="col-md-55">
                          <?php if($file ==! "" ){ ?>
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="dropzone/equipamento-baixa/<?php printf($file); ?>" alt="image" />
                            <div class="mask">
                              <p>Anexo</p>
                              <div class="tools tools-bottom">
                                <a href="dropzone/equipamento-baixa/<?php printf($file); ?>" download><i class="fa fa-download"></i></a>
                                
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>Anexo baixa</p>
                          </div>
                        </div>
                          <?php }; ?>
                      </div>
                     
                    
                   
                  

                   
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>



            <div class="x_panel">
                <div class="x_title">
                  <h2>Anexos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo -->
       <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_equipament_disable_dropzone WHERE id_equipamento like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_drop,$file,$titulo,$reg_date,$upgrade);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                          <th>Titulo</th>
                          <th>Registro</th>
                          <th>Atualização</th>
                         <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id_drop); ?></td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($upgrade); ?></td>
                         <td> <a class="btn btn-app"   href="dropzone/equipamento-baixa/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Anexo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Anexo
                  </a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/equipamento-dropzone-disable-2-trash.php?id=<?php printf($id_drop); ?>&equipamento=<?php printf($codigoget); ?> ';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir
                  </a>
                </td>

                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>




                </div>
              </div>

              <!-- Posicionamento -->

	          <!-- Registro -->
               <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>

                        <div class="modal-body">

                        <!-- Registro forms-->
                          <form action="backend/equipament-disable-2-dropzone-backend.php?id_equipamento=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>


                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="titulo" required='required'></div>
                    </div>




                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>

                        </div>
                 </div>
             </form>
              <form  class="dropzone" action="backend/equipament-disable-2-dropzone-upload-backend.php" method="post">
    </form >
     </div>
                    </div>
                  </div>
              <!-- Registro -->
     
         
               <div class="x_panel">
                <div class="x_title">
                  <h2>Cadastro da Baixa</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
  
	 <form  action="backend/register-equipament-disable-backend.php?id=<?php printf($codigoget); ?>" method="post">
                   <!-- Posicionamento -->
                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="date_disable">Data <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                <input type="date" id="date_disable" name="date_disable" value="<?php printf($data_disable); ?>" required="required" class="form-control ">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="date_disable">Motivo da Baixa <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="disable" id="disable"  placeholder="Categoria">
                      <option value=""> Selecione um motivo </option>

										  	<?php
										  	
										  
										  	
										   $sql = " SELECT id, nome FROM equipament_disable_status ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"<?php if($id==$id_disable){printf("selected");}?>><?php printf($nome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um motivo "></span>
										</div>	
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#disable').select2();
                                      });
                                 </script>
                                 
                                 	 <div class="item form-group">
                    

                       

                    <label for="des">Descrição da Baixa :</label>
                         <textarea id="des" class="form-control" name="des" data-parsley-trigger="keyup"
                           data-parsley-validation-threshold="10" placeholder="<?php   echo htmlspecialchars_decode($des); ?>"value="<?php   echo htmlspecialchars_decode($des); ?>"><?php   echo htmlspecialchars_decode($des); ?></textarea>

                   <!-- Posicionamento -->
                </div>

                <div class="modal-body">
    <strong>    <h4>Assinatura digital</h4></strong> 
    <strong>                  <p>Para Assinar digite o texto abaixo.</p></strong> 
    <strong>            <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?></strong> 
                          <p> <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> Assinatura<span > </span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="assinature" name="assinature"   class="form-control " class="docs-tooltip" data-toggle="tooltip" title=" ">
                          
                        </div>
                        <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "> </label>
                        <div class="col-md-9 col-sm-9 ">
                          <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "> </label>
                        <div class="col-md-9 col-sm-9 ">
                          <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >
                        </div>
                      </div>
                      </div></p>
                        </div>
                        <script>
    var chave = "<?php echo $chave; ?>"; // Obtém a chave do PHP e armazena na variável JavaScript
    var assinaturaInput = document.getElementById("assinature");

    assinaturaInput.addEventListener("blur", function () {
        var assinatura = this.value;

        if (assinatura !== chave) {
          Swal.fire('A assinatura está incorreta!');
            // Aqui você pode realizar a ação desejada caso a assinatura seja divergente, como exibir uma mensagem de erro ou realizar algum redirecionamento.
        } else {
        
            // Aqui você pode realizar a ação desejada caso a assinatura seja correta.
        }
    });
</script>
                	<button type="reset" class="btn btn-primary"onclick="new PNotify({
																title: 'Limpado',
																text: 'Todos os Campos Limpos',
																type: 'info',
																styling: 'bootstrap3'
														});" />Limpar</button>
											<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar"/>
													
												</form>
														<div class="ln_solid"></div>
              </div>
            </div>
              
              <!-- Posicionamento -->
	       
	       
    <div class="x_panel">
                <div class="x_title">
                  <h2>Assinatura</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                <!--arquivo -->
       <?php    
$query="SELECT regdate_disable_signature.id,regdate_disable_signature.id_equipamento,regdate_disable_signature.id_user,regdate_disable_signature.id_signature,regdate_disable_signature.reg_date,usuario.nome FROM regdate_disable_signature INNER JOIN usuario ON regdate_disable_signature.id_user = usuario.id   WHERE regdate_disable_signature.id_equipamento like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$id_os,$id_user,$id_signature,$reg_date,$id_user);
  
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th>Usuário</th>
                          <th>Assinatura</th>
                           <th>Registro</th>
                           <th>Ação</th>
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id_user); ?></td>
                          <td><?php printf($id_signature); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td>                                                                <a class="btn btn-app" onclick="
                            Swal.fire({
                              title: 'Tem certeza?',
                              text: 'Você não será capaz de reverter isso!',
                              icon: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: 'Sim, Deletar!'
                            }).then((result) => {
                              if (result.isConfirmed) {
                                Swal.fire(
                                  'Deletando!',
                                  'Seu arquivo será excluído.',
                                  'success'
                                ),
window.location = 'backend/signature-equipament-disable-trash-backend.php?id=<?php printf($id); ?>&page=<?php printf($codigoget); ?>';                              }
                            })
                          ">
                            <i class="fa fa-trash"></i> Excluir
                          </a>
                          </td>

                         
                          
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

                
              
                
                </div>
              </div>  
           
	         
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="register-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                   <a  class="btn btn-app" href="register-equipament-disable-viewer?id=<?php printf($codigoget); ?> "target="_blank" onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar Baixa',
                                type: 'info',
                                styling: 'bootstrap3'
                            });" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-equipament-disable-trash-backend?id=<?php printf($codigoget); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Reativar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-equipament-disable-trash-trash-backend?id=<?php printf($codigoget); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir Baixa</a>
               
              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->
                  
                </div>
              </div>
	       
	        
                  
                  
                </div>
              </div>
              
  