<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require '../../framework/PHPMailer/src/PHPMailer.php';
require '../../framework/PHPMailer/src/SMTP.php';
require '../../framework/PHPMailer/src/Exception.php';

// Instancie o objeto do PHPMailer
$mail = new PHPMailer(true);

try {
    // Configurações do servidor SMTP
    $mail->isSMTP();
    $mail->Host       = 'seth.mksistemasbiomedicos.com.br'; // Substitua pelo host do seu provedor
    $mail->SMTPAuth   = true;
    $mail->Username   = 'seth@seth.mksistemasbiomedicos.com.br'; // Substitua pelo seu endereço de e-mail
    $mail->Password   = 'mks@2024'; // Substitua pela sua senha
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Use 'tls' ou 'ssl' se necessário
    $mail->Port       = 587; // Porta do servidor SMTP

    // Configurações do e-mail
    $mail->setFrom('seth@seth.mksistemasbiomedicos.com.br', 'MKS');
    $mail->addAddress('douglasengenhariaclinica@gmail.com', 'Douglas Pereira Gomes'); // Substitua pelo endereço do destinatário
    $mail->Subject = 'Ordem de Serviço';
    $mail->Body    = 'Ordem de Serviço n 2024';

    // Enviar e-mail
    $mail->send();
    echo 'E-mail enviado com sucesso!';
} catch (Exception $e) {
    echo "Erro ao enviar o e-mail: {$mail->ErrorInfo}";
}
?>
