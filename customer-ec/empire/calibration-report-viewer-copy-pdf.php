<?php
  //============================================================+
  // File name   : example_001.php
  // Begin       : 2008-03-04
  // Last Update : 2013-05-14
  //
  // Description : Example 001 for TCPDF class
  //               Default Header and Footer
  //
  // Author: Nicola Asuni
  //
  // (c) Copyright:
  //               Nicola Asuni
  //               Tecnick.com LTD
  //               www.tecnick.com
  //               info@tecnick.com
  //============================================================+
  
  /**
  * Creates an example PDF TEST document using TCPDF
  * @package com.tecnick.tcpdf
  * @abstract TCPDF - Example: Default Header and Footer
  * @author Nicola Asuni
  * @since 2008-03-04
  */
  
  // Include the main TCPDF library (search for installation path).
  require_once('../../framework/TCPDF/tcpdf.php');
  
  // create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('MK Sistemas Biomedicos');
  $pdf->SetTitle('Sistema SETH');
  $pdf->SetSubject('Ordem de Serviço');
  $pdf->SetKeywords('Ordem de Serviço');
  
  // set default header data
  $pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH,'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
  $pdf->setFooterData(array(0,64,0), array(0,64,128));
  
  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  
  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  
  // set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  
  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  
  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
  }
  
  // ---------------------------------------------------------
  
  // set default font subsetting mode
  $pdf->setFontSubsetting(true);
  
  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage();
  
  // set text shadow effect
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
  
     $codigoget = ($_GET["laudo"]);

	include_once("database/database.php");
	date_default_timezone_set('America/Sao_Paulo');

  $transacoes = ''; // ou atribua um valor padrão
  $pop_codigo = '';
  $pop_titulo = '';




$print= date(DATE_RFC2822);

	$html = '<table  background-color: #fff; color: black; style="font-size:12px;width:100%;border-collapse:collapse;">';
//	$html .= '<thead>';
//  $html .= '<tr class="gridHeaderRelatorio">';
//	$html .= '<th colspan="9"><strong></strong></th>';
 // $html .= '</tr>';
 // $html .= '</thead>';


	$result_transacoes = "SELECT calibration_report.status,calibration_parameter_dados_parameter.nome,calibration_parameter_dados_parameter.unidade,calibration_report.ve, calibration_report.vl_1, calibration_report.vl_2, calibration_report.vl_3, calibration_report.vl_avg, calibration_report.erro, calibration_report.dp, calibration_report.ie FROM calibration_report INNER JOIN calibration_parameter_dados_parameter on calibration_parameter_dados_parameter.id = calibration_report.id_calibration_parameter_dados_parameter WHERE id_calibration like '$codigoget' AND calibration_report.ve IS NOT NULL ORDER BY calibration_parameter_dados_parameter.nome ASC";
	$resultado_trasacoes = mysqli_query($conn, $result_transacoes);
	while($row_transacoes = mysqli_fetch_assoc($resultado_trasacoes)){
		$html .= '<tr><th colspan="9"><small style="font-size: 12px;">';

		if($row_transacoes['nome'] !== $transacoes){
		$transacoes=$row_transacoes['nome'];

		$html .=$row_transacoes['nome'] .'-'.$row_transacoes['unidade'];

		$html .= '</small>';
    $html .='</th></tr>"';
    $html .= '<thead>';
    $html .= '<tr class="">';
    $html .= '<th bgcolor="#AAAAAA"><strong>Valor de Ensaio</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Valor Aferido-1</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Valor Aferido-2</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Valor Aferido-3</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Média do Valor Aferido</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Erro</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Desvio Padrão</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Incerteza Expandida</strong></th>';
    $html .= '<th bgcolor="#AAAAAA"><strong>Status</strong></th>';
    $html .= '</tr>';
    $html .= '</thead>';
		}
	$html .='</th><tbody><tr class="" >';




		$html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.$row_transacoes['ve'] . "</small></center></td>";
		$html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.$row_transacoes['vl_1'] . "</small></center></td>";
		$html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.$row_transacoes['vl_2'] . "</small></center></td>";
		$html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.$row_transacoes['vl_3'] . "</small></center></td>";
	  $html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.number_format($row_transacoes['vl_avg'], 2, '.', '')  . "</small></center></td>";
		$html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.number_format($row_transacoes['erro'], 2, '.', '')  . "</small></center></td>";
    $html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.number_format($row_transacoes['dp'], 2, '.', '')  . "</small></center></td>";
    $html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.number_format($row_transacoes['ie'], 2, '.', '')  . "</small></center></td>";
     	if($row_transacoes['status']=="0"){
     	    $status="Aprovado";
     	}
     		if($row_transacoes['status']=="1"){
     		       $status="Reprovado";
     	}

		$html .= '<td style="border: 1px solid black;"><center><small style="font-size: 10px;">'.$status." </small></center></td></tr>";

	}


	$rotina = 0;
$query = "SELECT calibration_parameter_manufacture.ibge,calibration_parameter_manufacture.cnpj,calibration_parameter_manufacture.adress,calibration_parameter_manufacture.bairro,calibration_parameter_manufacture.cidade,calibration_parameter_manufacture.estado,calibration_parameter_manufacture.cep,equipamento.patrimonio,equipamento.serie,calibration.temp,calibration.hum,calibration.obs,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade,calibration.procedure_cal,calibration_parameter_manufacture.nome,calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration INNER JOIN equipamento on equipamento.id = calibration.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN calibration_parameter_manufacture on calibration_parameter_manufacture.id =  calibration.id_manufacture INNER JOIN colaborador on colaborador.id = calibration.id_colaborador WHERE calibration.id like '$codigoget' ";



if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($manufacture_ibge,$manufacture_cnpj,$manufacture_adress,$manufacture_bairro,$manufacture_cidade,$manufacture_estado,$manufacture_cep,$patrimonio,$serie,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
$data_start=date("d/m/Y", strtotime($data_start));
  }

}

$query = "SELECT calibration.codigo,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade FROM calibration INNER JOIN colaborador on colaborador.id = calibration.id_responsavel WHERE calibration.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($codigo_externo,$primeironome_responsavel,$ultimonome_responsavel,$entidade_responsavel );
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  }

}

$query = "SELECT instituicao.cep,instituicao.endereco,instituicao.bairro,instituicao.cidade,instituicao.estado,instituicao.ibge,instituicao.instituicao,instituicao_area.nome,instituicao_localizacao.nome,instituicao.cnpj  FROM calibration INNER JOIN equipamento on equipamento.id = calibration.id_equipamento INNER JOIN instituicao_localizacao on instituicao_localizacao.id =  equipamento.id_instituicao_localizacao INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao on instituicao.id = instituicao_area.id_unidade WHERE calibration.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($cliente_cep,$cliente_adress,$cliente_bairro,$cliente_cidade,$cliente_estado,$cliente_ibge,$cliente_unidade,$cliente_area,$cliente_setor,$cliente_cnpj);
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  }

}


$query = "SELECT calibration_parameter_tools.id,calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo, calibration_parameter_tools.serie,calibration_parameter_tools.patrimonio,calibration_parameter_tools.calibration,calibration_parameter_tools.date_calibration,calibration_parameter_tools.date_validation FROM calibration_report INNER JOIN calibration_parameter_tools on calibration_parameter_tools.id = calibration_report.id_calibration_parameter_tools WHERE calibration_report.id_calibration like '$codigoget' GROUP by calibration_parameter_tools.modelo  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $fabricante, $modelo, $serie, $patrimonio,$calibration, $date_calibration, $date_validation);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}

$query = "SELECT calibration.val,calibration.id_responsavel,calibration.id_colaborador,calibration.codigo,calibration.id_equipamento,calibration.id_manufacture,equipamento_familia.id_equipamento_grupo,calibration.temp,calibration.hum,calibration.obs,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade,calibration.procedure_cal,calibration_parameter_manufacture.nome,calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration LEFT join equipamento on equipamento.id = calibration.id_equipamento LEFT join equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT join calibration_parameter_manufacture on calibration_parameter_manufacture.id =  calibration.id_manufacture LEFT join colaborador on colaborador.id = calibration.id_colaborador WHERE calibration.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($date_validation,$id_responsavel,$id_colaborador,$codigo_caliration,$id_equipamento,$id_manufacture,$id_equipamento_grupo,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}
  $stmt->close();


	$html2 = '<table border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:100%;border-collapse:collapse;">';
	$html2 .= '<thead>';
	$html2 .= '<tr class="gridHeaderRelatorio">';
	$html2 .= '<th>Nome</th>';
	$html2 .= '<th>Fabricante</th>';
	$html2 .= '<th>Modelo</th>';
	$html2 .= '<th>Serie</th>';
	$html2 .= '<th>Patrimonio</th>';
	$html2 .= '<th>Laudo</th>';
	$html2 .= '<th>Data Calibração</th>';
	$html2 .= '<th>Validade</th>';

	$html2 .= '</tr>';
	$html2 .= '</thead>';
	$html2 .= '<tbody>';
		$result_transacoes2 = "SELECT calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo, calibration_parameter_tools.serie,calibration_parameter_tools.patrimonio,calibration_parameter_tools.calibration,calibration_parameter_tools.date_calibration,calibration_parameter_tools.date_validation FROM calibration_report INNER JOIN calibration_parameter_tools on calibration_parameter_tools.id = calibration_report.id_calibration_parameter_tools WHERE calibration_report.id_calibration like '$codigoget' GROUP by calibration_parameter_tools.nome  ";
	$resultado_trasacoes2 = mysqli_query($conn, $result_transacoes2);
	while($row_transacoes2 = mysqli_fetch_assoc($resultado_trasacoes2)){
    $date_calibration=date("d/m/Y", strtotime($row_transacoes2['date_calibration']));
      $date_validation=date("d/m/Y", strtotime($row_transacoes2['date_validation']));
		$html2 .= '<tr><td><small>'.$row_transacoes2['nome'] . "</small></td>";
		$html2 .= '<td><small>'.$row_transacoes2['fabricante'] . "</small></td>";
		$html2 .= '<td><small>'.$row_transacoes2['modelo'] . "</small></td>";
		$html2 .= '<td><small>'.$row_transacoes2['serie'] . "</small></td>";
		$html2 .= '<td><small>'.$row_transacoes2['patrimonio'] . "</small></td>";
	    $html2 .= '<td><small>'.$row_transacoes2['calibration'] . "</small></td>";
		$html2 .= '<td><small>'.$date_calibration. "</small></td>";

		$html2 .= '<td><small>'.$date_validation. " </small></td></tr>";
	}

  $texto_quebrado = wordwrap($obs, 100, "<br>", true);

	$html2 .= '</tbody>';
	$html2 .= '</table>';

	$html .= '</tbody>';
	$html .= '</table>';
  // Em vez de usar mb_convert_encoding
  // $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
  
  // Use htmlspecialchars ou htmlentities
  //$html = htmlspecialchars($html, ENT_QUOTES, 'UTF-8');
  // ou
  // $html = htmlentities($html, ENT_QUOTES, 'UTF-8');

  $html = <<<EOD

  <div class="wraper">
    <!-- Início do Cabeçalho -->
   
  <div style="clear: both;"></div>
  <hr style="border-top: dashed 1px; width: 900px; color: gray; clear: both;">
  <!-- Fim do Cabeçalho -->
  <!-- Início do Título -->
   <br>
  <center>
  <h2 class="titulo-relatorio">CERTIFICADO DE CALIBRAÇÃO Nº $codigoget</h2>
  </center>
  <br>
  <!-- Fim do Título -->
    <main>
      <div class="container">
        <section class="content-block">

          <header>
            <h4 class="content-block-title">Dados do Cliente</h4>
          </header>
          <table class="table-documento">
            <tbody>
              <tr>
                <td >
                  Cliente:
                  <span class="uppercase weight-bold"> $cliente_unidade</span>
                </td>

                <td >
                  Area:
                  <span class="uppercase weight-bold"> $cliente_setor</span>
                </td>

                <td >
                  Setor:
                  <span class="uppercase weight-bold"> $cliente_area</span>
                </td>

                <td>
                  CEP:
                  <span class="uppercase weight-bold"> $cliente_cep </span>
                </td>

                <td >
                  Rua:
                  <span class="uppercase weight-bold"> $cliente_adress</span>
                </td>

                <td >
                  Bairro:
                  <span class="uppercase weight-bold"> $cliente_bairro</span>
                </td>
              </tr>

              <tr>
                <td>
                  Cidade:
                  <span class="uppercase weight-bold">$cliente_cidade</span>
                </td>

                <td >
                  Estado:
                  <span class="uppercase weight-bold"> $cliente_estado</span>
                </td>



                <td>
                  CNPJ:
                  <span class="uppercase weight-bold">$cliente_cnpj</span>
                </td>


              </tr>
            </tbody>
          </table>
        </section>
        <!--./content-block-->

        <section class="content-block">
          <header>
            <h4 class="content-block-title">DADOS DO EQUIPAMENTO</h4>
          </header>

           <table class="table-documento">
            <tbody>
              <tr>
                <td >
                  Equipamento:
                  <span class="uppercase weight-bold"> $nome</span>
                </td>

                <td >
                  Fabricante:
                  <span class="uppercase weight-bold"> $fabricante</span>
                </td>

                <td >
                  Modelo:
                  <span class="uppercase weight-bold"> $modelo</span>
                </td>

                <td>
                   Nº de Série::
                  <span class="uppercase weight-bold"> $serie </span>
                </td>

                <td >
                  Patrimonio:
                  <span class="uppercase weight-bold"> $patrimonio </span>
                </td>

                <td >
                  Codigo:
                  <span class="uppercase weight-bold"> $codigo </span>
                </td>
              </tr>






            </tbody>
          </table>
        </section>

        <section class="content-block">
          <header>
            <h4 class="content-block-title">calibração</h4>
          </header>
          <table class="table-documento lista-atividades">
            <thead>
              <tr>
                <th class="width-30-percent text-left">Data Calibração: $data_start</th>
                   <th class="width-10-percent text-left">Validade: $val</th>
                   <td colspan="3" class="atv-principal">* Codigo Externo $codigo_externo </td>
                   <td colspan="3" class="atv-principal">* Procedimento $pop_codigo $pop_titulo</td>
                   
                   

              </tr>
            </thead>
         
         

          </table>
          <table class="table-documento lista-atividades">
          <thead>
          <tr>
          <th class="width-100-percent text-left">
          Temperatura Obtida (ºC):
          <span class="uppercase weight-bold"> $temperatura</span>  Faixa aceitável (ºc): 0 a 40
      
          Umidade Relat. do Ar (%):
          <span class="uppercase weight-bold"> $umidade</span> Faixa aceitável (ºC): 20 a 80
          </th>

          </tr>
          </thead>
          </table>

        </section>


        <section class="content-block">
          <header>
            <h4 class="content-block-title">DADOS DOS EQUIPAMENTOS PADRÕES</h4>
          </header>
            $html2

        </section>

        <section class="content-block">
         <header>
           <h4 class="content-block-title">obervação</h4>
         </header>
         <table class="table-documento lista-atividades">
        
           <tbody>
             <tr class="width-100-percent">
               <td colspan="3" class="atv-principal">
1. Eventuais re-calibrações são de responsabilidade do USUÁRIO. <br>
2. Equipamentos rastreável pelo LOTE de fabricação<br>
3. Incerteza expandida de medição relatada é baseada em uma incerteza padronizada combinada multiplicada pelo fator de abrangência K=2,<br>  o qual corresponde a uma probabilidade de abrangência de aproximadamente 95%.
<pre>$texto_quebrado </pre>   
</td> 
</tr>
           </tbody>
         </table>
       </section>

       <section class="content-block">
        <header>
          <h4 class="content-block-title">Empresa</h4>
        </header>
           <table class="table-documento">
          <tbody>
            <tr>
              <td >
                Empresa:
                <span class="uppercase weight-bold"> $manufacture</span>
              </td>




            </tr>


            <tr>
              <td>
                CEP:
                <span class="uppercase weight-bold"> $manufacture_cep </span>
              </td>

              <td >
                Rua:
                <span class="uppercase weight-bold"> $manufacture_adress</span>
              </td>

              <td >
                Bairro:
                <span class="uppercase weight-bold"> $manufacture_bairro</span>
              </td>
            </tr>

            <tr>
              <td>
                Cidade:
                <span class="uppercase weight-bold">$manufacture_cidade</span>
              </td>

              <td >
                Estado:
                <span class="uppercase weight-bold"> $manufacture_estado</span>
              </td>


            </tr>

            <tr>
              <td>
                IBGE:
                <span class="uppercase weight-bold">$manufacture_ibge</span>
              </td>


            </tr>
          </tbody>
        </table>
      </section>
      

        <section class="content-block">

          <header>
            <br>
            <h4 class="content-block-title">ENSAIO</h4>
          </header>
          	 $html 
        </section>

        <br>
        <br>

         <section class="content-block">
          <header>
          <br>
            <h4 class="content-block-title">Assinatura</h4>
          </header>
          <br>
           <table style="width: 100%; margin-right: 25px" class="table-documento lista-atividades">
                    <tbody style="text-align: left">
                    <tr>
                      <td class="width-10-percent text-left">

                      </td>
                    </tr>
                    <tr>
                      <td class="width-10-percent text-left">

                      </td>
                    </tr>
                      <tr>


                       <td class="width-10-percent text-left"></br></br>_______________________________________________</td>

                        <td class="width-10-percent text-left"></br></br>______________________________________________</td>
                      </tr>
                      <tr>
                        <td>Executante: $primeironome $ultimonome CREA/CFT: $entidade</td>
                         <td>Aceite:</td>
                      </tr>
                        <tr>
                          <td class="width-10-percent text-left">

                          </td>
                        </tr>
                        <tr>
                          <td class="width-10-percent text-left">

                          </td>
                        </tr>
                        <tr>
                          <td class="width-10-percent text-left">

                          </td>
                        </tr>
                      <tr>


                        <td class="width-10-percent text-left"</br></br>______________________________________________</td>


                      </tr>
                      <tr>
                        <td>Responsável Técnico:  $primeironome_responsavel $ultimonome_responsavel CREA/CFT: $entidade_responsavel</td>

                      </tr>
                    </tbody>

                  </table>

        </section>
        <br>
        <section class="content-block">


                 <div class="center-side">

                   <div class="center-side-text">


                       <small>CERTIFICADO DE CALIBRAÇÃO Nº $codigoget</small>
                     <br>

                   </div>
                 </div>

       </section>

        <!--./content-block-->
      </div>
      <!--/.container-->
    </main>

    <footer class="footer">
    <div class="">
<center>
 Copyright ©2022 MK Sistemas Biomédicos- by MK Sistemas Biomédicos.
</center>

       </br>
     </div>
      <table>
        <tr>
          <td class="footer-info base-background">


          </td>

        </tr>

      </table>
    </footer>

  </div>




EOD;
  // Print text using writeHTMLCell()
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  
  // ---------------------------------------------------------
  
  // Close and output PDF document
  // This method has several options, check the source code documentation for more information.
  $pdf->Output('Rotina.pdf', 'I');