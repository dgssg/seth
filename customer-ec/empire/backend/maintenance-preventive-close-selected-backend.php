<?php

include("../database/database.php");
date_default_timezone_set('America/Sao_Paulo');
$codigoget = ($_GET["id"]);
$id_mp_reurn = $codigoget;
$fornecedor = $_POST['fornecedor'];
$date_mp_start = $_POST['date_start'];
$date_mp_end = $_POST['date_end'];
$time_mp = $_POST['time'];
$colaborador = $_POST['colaborador'];
$message_mp = $_POST['message_mp'];
$message_tc = $_POST['message_tc'];
$routine = $_POST['routine'];
$periodicidade = $_POST['periodicidade'];
$programada= $_POST['programada'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];
$table = $_POST['table'];

$anexo=$_COOKIE['anexo'];

$status="3";
$status2=0;
$mp_control=1;
  $id_array = explode(",", $codigoget);
  
  // Agora, você pode iterar sobre esse array usando um loop
  foreach($id_array as $id_explod) {
  
  $query = "SELECT maintenance_preventive.id_routine,maintenance_preventive.date_start, maintenance_routine.periodicidade FROM maintenance_preventive INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine WHERE maintenance_preventive.id like '$id_explod'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($routine,$programada,$periodicidade);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
}
}
  
 
  $stmt = $conn->prepare("UPDATE maintenance_preventive SET mp_control= ? WHERE id= ?");
  $stmt->bind_param("ss",$mp_control,$id_explod);
  $execval = $stmt->execute();
  $stmt->close();


$stmt = $conn->prepare("UPDATE maintenance_preventive SET digital= ? WHERE id= ?");
$stmt->bind_param("ss",$table,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_fornecedor= ? WHERE id= ?");
$stmt->bind_param("ss",$fornecedor,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_start= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_start,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_end,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET time_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$time_mp,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_status= ? WHERE id= ?");
$stmt->bind_param("ss",$status,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET file= ? WHERE id= ?");
$stmt->bind_param("ss",$anexo,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$colaborador,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$message_mp,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_tc= ? WHERE id= ?");
$stmt->bind_param("ss",$message_tc,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_end,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET temp= ? WHERE id= ?");
$stmt->bind_param("ss",$temp,$id_explod);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET hum= ? WHERE id= ?");
$stmt->bind_param("ss",$hum,$id_explod);
$execval = $stmt->execute();
$stmt->close();


if($periodicidade==365){

  $data_after=date('Y-m-d', strtotime($programada.' + 12 months'));

}

if($periodicidade==180){

  $data_after=date('Y-m-d', strtotime($programada.' + 6 months'));

}

if($periodicidade==30){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 months'));

}

if($periodicidade==1){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 days'));

}

if($periodicidade==5){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 weekday'));

  
}

if($periodicidade==7){

  $data_after=date('Y-m-d', strtotime($programada.' + 7 days'));


}

if($periodicidade==14){

  $data_after=date('Y-m-d', strtotime($programada.' + 14 days'));


}

if($periodicidade==21){

  $data_after=date('Y-m-d', strtotime($programada.' + 21 days'));


}

if($periodicidade==28){

  $data_after=date('Y-m-d', strtotime($programada.' + 28 days'));


}

if($periodicidade==60){

  $data_after=date('Y-m-d', strtotime($programada.' + 2 months'));


}

if($periodicidade==90){

  $data_after=date('Y-m-d', strtotime($programada.' + 3 months'));


}

if($periodicidade==120){

  $data_after=date('Y-m-d', strtotime($programada.' + 4 months'));


}

if($periodicidade==730){

  $data_after=date('Y-m-d', strtotime($programada.' + 24 months'));


}
  if($periodicidade==1095){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 36 months'));
    
    
  }
  if($periodicidade==1460){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 48 months'));
    
    
  }
  


  $stmt = $conn->prepare("UPDATE maintenance_control SET data_after = ? WHERE id_routine= ?");
  $stmt->bind_param("ss",$data_after,$routine);
  $execval = $stmt->execute();
  $stmt->close();

  $stmt = $conn->prepare("UPDATE maintenance_control SET status = ? WHERE id_routine= ?");
  $stmt->bind_param("ss",$status2,$routine);
  $execval = $stmt->execute();
  $stmt->close();


  $query = "SELECT date_start FROM maintenance_preventive WHERE id_routine = $routine ORDER BY date_start DESC LIMIT 1 ";
  if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
         $stmt->bind_result($programada);
   while ($stmt->fetch()) {
     $programada;
   }
  }

  $query = "SELECT periodicidade FROM maintenance_routine WHERE id = $routine";
  if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
         $stmt->bind_result($periodicidade);
   while ($stmt->fetch()) {
     $periodicidade;
   }
  }





  if($periodicidade==365){

    $data_after=date('Y-m-d', strtotime($programada.' + 12 months'));

  }

  if($periodicidade==180){

    $data_after=date('Y-m-d', strtotime($programada.' + 6 months'));

  }

  if($periodicidade==30){

    $data_after=date('Y-m-d', strtotime($programada.' + 1 months'));

  }

  if($periodicidade==1){

    $data_after=date('Y-m-d', strtotime($programada.' + 1 days'));

  }
  if($periodicidade==5){
    
    
    $data_after=date('Y-m-d', strtotime($programada.' + 1 weekday'));

  }

  if($periodicidade==7){

    $data_after=date('Y-m-d', strtotime($programada.' + 7 days'));


  }

  if($periodicidade==14){

    $data_after=date('Y-m-d', strtotime($programada.' + 14 days'));


  }

  if($periodicidade==21){

    $data_after=date('Y-m-d', strtotime($programada.' + 21 days'));


  }

  if($periodicidade==28){

    $data_after=date('Y-m-d', strtotime($programada.' + 28 days'));


  }

  if($periodicidade==60){

    $data_after=date('Y-m-d', strtotime($programada.' + 2 months'));


  }

  if($periodicidade==90){

    $data_after=date('Y-m-d', strtotime($programada.' + 3 months'));


  }

  if($periodicidade==120){

    $data_after=date('Y-m-d', strtotime($programada.' + 4 months'));


  }

  if($periodicidade==730){

    $data_after=date('Y-m-d', strtotime($programada.' + 24 months'));


  }
  if($periodicidade==1095){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 36 months'));
    
    
  }
  if($periodicidade==1460){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 48 months'));
    
    
  }

  
    $stmt = $conn->prepare("UPDATE maintenance_control SET data_after = ? WHERE id_routine= ?");
    $stmt->bind_param("ss",$data_after,$id_explod);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE maintenance_control SET status = ? WHERE id_routine= ?");
    $stmt->bind_param("ss",$status2,$id_explod);
    $execval = $stmt->execute();
    $stmt->close();

$id_explod = $routine;

$query = "SELECT date_start FROM maintenance_preventive WHERE id_routine = $id_explod ORDER BY date_start DESC LIMIT 1 ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($programada);
 while ($stmt->fetch()) {
   $programada;
 }
}

$query = "SELECT periodicidade FROM maintenance_routine WHERE id = $id_explod";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($periodicidade);
 while ($stmt->fetch()) {
   $periodicidade;
 }
}





if($periodicidade==365){

  $data_after=date('Y-m-d', strtotime($programada.' + 12 months'));

}

if($periodicidade==180){

  $data_after=date('Y-m-d', strtotime($programada.' + 6 months'));

}

if($periodicidade==30){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 months'));

}

if($periodicidade==1){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 days'));

}
if($periodicidade==5){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 weekday'));

}

if($periodicidade==7){

  $data_after=date('Y-m-d', strtotime($programada.' + 7 days'));


}

if($periodicidade==14){

  $data_after=date('Y-m-d', strtotime($programada.' + 14 days'));


}

if($periodicidade==21){

  $data_after=date('Y-m-d', strtotime($programada.' + 21 days'));


}

if($periodicidade==28){

  $data_after=date('Y-m-d', strtotime($programada.' + 28 days'));


}

if($periodicidade==60){

  $data_after=date('Y-m-d', strtotime($programada.' + 2 months'));


}

if($periodicidade==90){

  $data_after=date('Y-m-d', strtotime($programada.' + 3 months'));


}

if($periodicidade==120){

  $data_after=date('Y-m-d', strtotime($programada.' + 4 months'));


}

if($periodicidade==730){

  $data_after=date('Y-m-d', strtotime($programada.' + 24 months'));


}
  if($periodicidade==1095){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 36 months'));
    
    
  }
  if($periodicidade==1460){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 48 months'));
    
    
  }

  
  $stmt = $conn->prepare("UPDATE maintenance_control SET data_after = ? WHERE id_routine= ?");
  $stmt->bind_param("ss",$data_after,$id_explod);
  $execval = $stmt->execute();
  $stmt->close();

  $stmt = $conn->prepare("UPDATE maintenance_control SET status = ? WHERE id_routine= ?");
  $stmt->bind_param("ss",$status2,$id_explod);
  $execval = $stmt->execute();
  $stmt->close();

  $query = "SELECT date_start FROM maintenance_preventive WHERE id_routine = $id_explod ORDER BY date_start DESC LIMIT 1 ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($programada);
 while ($stmt->fetch()) {
   $programada;
 }
}

$query = "SELECT periodicidade FROM maintenance_routine WHERE id = $id_explod";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($periodicidade);
 while ($stmt->fetch()) {
   $periodicidade;
 }
}





  $stmt = $conn->prepare("UPDATE maintenance_control SET data_start = ? WHERE id_routine= ?");
  $stmt->bind_param("ss",$programada,$id_explod);
  $execval = $stmt->execute();
  $stmt->close();

  $query = "SELECT return_mp FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($return_mp);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  
  
  if($return_mp == 0){
    
    $query = "SELECT return_type FROM tools";
    
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($return_type);
      while ($stmt->fetch()) {
        //printf("%s, %s\n", $solicitante, $equipamento);
      }
    }
    
    
    $stmt = $conn->prepare("UPDATE maintenance_preventive SET return_mp = 0 WHERE id= ?");
    $stmt->bind_param("s",$id_mp_reurn);
    $execval = $stmt->execute();
    $stmt->close();
    
    if($return_type == 1){
      
      $query = "SELECT instituicao_area.return_user FROM instituicao_area LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id LEFT JOIN equipamento ON equipamento.id_instituicao_localizacao =  instituicao_localizacao.id LEFT JOIN  maintenance_routine ON maintenance_routine.id_equipamento = equipamento.id where maintenance_routine.id =  $routine";
      
      
      if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($return_user);
        while ($stmt->fetch()) {
          //printf("%s, %s\n", $solicitante, $equipamento);
        }
      }
      
      $stmt = $conn->prepare("UPDATE maintenance_preventive SET return_mp_id = ? WHERE id= ?");
      $stmt->bind_param("ss",$return_user,$id_mp_reurn);
      $execval = $stmt->execute();
      $stmt->close();
      
      
    }
      
    if($return_type == 2){
      
      $query = "SELECT return_user FROM maintenance_routine WHERE id = $id_mp_reurn";
      if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($return_user);
        while ($stmt->fetch()) {
          $periodicidade;
        }
      }
      
      $stmt = $conn->prepare("UPDATE maintenance_preventive SET return_mp_id = ? WHERE id= ?");
      $stmt->bind_param("ss",$return_user,$id_mp_reurn);
      $execval = $stmt->execute();
      $stmt->close();
      
      
    }
    
  }
  
}
  
setcookie('anexo', null, -1);
 
echo "<script>document.location='../maintenance-preventive?sweet_salve=1'</script>";

?>
