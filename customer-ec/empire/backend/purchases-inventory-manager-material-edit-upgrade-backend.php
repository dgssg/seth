<?php

include("../database/database.php");

$codigoget = $_POST['id'];
$purchases_species = $_POST['purchases_species'];
$purchases_class = $_POST['purchases_class'];
$purchases_subclass = $_POST['purchases_subclass'];
$cod = $_POST['cod'];
$nome = $_POST['nome'];
$unidade = $_POST['unidade'];
$qt_min = $_POST['qt_min'];
$qt_max = $_POST['qt_max'];
$qt_now = $_POST['qt_now'];
$vlr = $_POST['vlr'];
	 

$stmt = $conn->prepare("UPDATE purchases_material SET purchases_species = ? WHERE id= ?");
$stmt->bind_param("ss",$purchases_species,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET purchases_class = ? WHERE id= ?");
$stmt->bind_param("ss",$purchases_class,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE purchases_material SET purchases_subclass = ? WHERE id= ?");
$stmt->bind_param("ss",$purchases_subclass,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET cod = ? WHERE id= ?");
$stmt->bind_param("ss",$cod,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET unidade = ? WHERE id= ?");
$stmt->bind_param("ss",$unidade,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET qt_min = ? WHERE id= ?");
$stmt->bind_param("ss",$qt_min,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET qt_max = ? WHERE id= ?");
$stmt->bind_param("ss",$qt_max,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET qt_now = ? WHERE id= ?");
$stmt->bind_param("ss",$qt_now,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE purchases_material SET vlr = ? WHERE id= ?");
$stmt->bind_param("ss",$vlr,$codigoget);
$execval = $stmt->execute();





//echo "New records created successfully";



echo "<script>alert('Atualizado!');document.location='../purchases-inventory-manager'</script>";
?>
