<?php

include("../database/database.php");

$id_calibration= $_GET['laudo'];
$ve= $_POST['ve'];

$vl_1= $_POST['vl_1'];
$vl_2= $_POST['vl_2'];
$vl_3= $_POST['vl_3'];
$k= $_POST['k']; //A linha referente ao Grau de Liberdade Efetivo na coluna 0,025 da tabela T-Student, quando calculado.
$id_calibration_parameter_dados_parameter= $_POST['parametro'];
$id_calibration_parameter_tools_dados= $_POST['parameter_rg'];

$id_tools=trim($id_calibration_parameter_tools_dados);
$query="SELECT i_h,id,id_calibration_parameter_tools FROM calibration_parameter_tools_dados WHERE id like '$id_tools' ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($ih,$id_dados,$id_calibration_parameter_tools);


 while ($stmt->fetch()) {
     $ih;
     $id_calibration_parameter_tools=$id_calibration_parameter_tools;
 }
}

$ih;
$ibn=$ih;  //Esse cálculo só se aplica se a Incerteza for do tipo B, e o tipo for, (resolução ou outras), então é a estimativa convertida dividida pela distribuição

$vl_avg= ($vl_1 + $vl_2 + $vl_3)/3; //Valor medio

$erro=abs($ve-$vl_avg); //Erro

$dp=sqrt((pow(($vl_1-$vl_avg),2)+pow(($vl_2-$vl_avg),2)+pow(($vl_3-$vl_avg),2))/3); // Desvio Padrão

$ia=$dp/(sqrt(3)); // Incerteza do Tipo A / Repetitividade


$ib= $ibn; //É a raiz quadrada das somas das incertezas do tipo B ao quadrado.

$ic=sqrt((pow($ib,2)+pow($ia,2))); //É a raiz quadrada da soma da incerteza do tipo B ao quadrado mais a incerteza do tipo A ao quadrado.

$ie=$ic*$k;//É a Incerteza Combinada vezes o fator de Abrangência.

$stmt = $conn->prepare("INSERT INTO calibration_report( id_calibration, id_calibration_parameter_tools, id_calibration_parameter_tools_dados, id_calibration_parameter_dados_parameter, ve, vl_1, vl_2, vl_3, vl_avg, erro, dp, ie, ia, k, ib, ic) VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?)");
$stmt->bind_param("ssssssssssssssss",$id_calibration, $id_calibration_parameter_tools, $id_calibration_parameter_tools_dados, $id_calibration_parameter_dados_parameter, $ve, $vl_1, $vl_2, $vl_3, $vl_avg, $erro, $dp, $ie, $ia, $k, $ib, $ic);
$execval = $stmt->execute();
 $stmt->close();
//echo "New records created successfully";

header('Location: ../calibration-report-edit-copy?laudo='.$id_calibration);
?>
