<?php

include("../database/database.php");

$codigoget= $_POST['id'];
$primeironome= $_POST['primeironome'];
$ultimonome= $_POST['ultimonome'];
$email= $_POST['email'];
$telefone= $_POST['telefone'];
$cargo= $_POST['cargo'];
$matricula= $_POST['matricula'];
$nascimento= $_POST['nascimento'];
$entidade= $_POST['entidade'];
$admissao= $_POST['admissao'];
$carga= $_POST['carga'];
$start_exp= $_POST['start_exp'];
$end_exp= $_POST['end_exp'];

$cep= $_POST['cep'];
$rua= $_POST['rua'];
$bairro= $_POST['bairro'];
$cidade= $_POST['cidade'];
$estado= $_POST['estado'];
$ibge= $_POST['ibge'];

$terceirizado= $_POST['terceirizado'];
$ativo= $_POST['ativo'];

$id_unidade=$_POST['duallistbox_demo1'];
$id_categoria=$_POST['id_categoria'];
foreach($id_unidade as $id_unidade){
    $idunidade=$id_unidade.",".$idunidade;
}
 $id_unidade= $idunidade;
 foreach($id_categoria as $id_categoria){
    $idcategoria=$id_categoria.",".$idcategoria;
}
 $id_categoria= $idcategoria;


$stmt = $conn->prepare("UPDATE colaborador SET primeironome= ? WHERE id= ?");
$stmt->bind_param("ss",$primeironome,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET ultimonome= ? WHERE id= ?");
$stmt->bind_param("ss",$ultimonome,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET email= ? WHERE id= ?");
$stmt->bind_param("ss",$email,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET telefone= ? WHERE id= ?");
$stmt->bind_param("ss",$telefone,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET cargo= ? WHERE id= ?");
$stmt->bind_param("ss",$cargo,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET matricula= ? WHERE id= ?");
$stmt->bind_param("ss",$matricula,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET nascimento= ? WHERE id= ?");
$stmt->bind_param("ss",$nascimento,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET entidade= ? WHERE id= ?");
$stmt->bind_param("ss",$entidade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET admissao= ? WHERE id= ?");
$stmt->bind_param("ss",$admissao,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET carga= ? WHERE id= ?");
$stmt->bind_param("ss",$carga,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET start_exp= ? WHERE id= ?");
$stmt->bind_param("ss",$start_exp,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET end_exp= ? WHERE id= ?");
$stmt->bind_param("ss",$end_exp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE colaborador SET cep= ? WHERE id= ?");
$stmt->bind_param("ss",$cep,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET rua= ? WHERE id= ?");
$stmt->bind_param("ss",$rua,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET bairro= ? WHERE id= ?");
$stmt->bind_param("ss",$bairro,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET cidade= ? WHERE id= ?");
$stmt->bind_param("ss",$cidade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET estado= ? WHERE id= ?");
$stmt->bind_param("ss",$estado,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET ibge= ? WHERE id= ?");
$stmt->bind_param("ss",$ibge,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET terceirizado= ? WHERE id= ?");
$stmt->bind_param("ss",$terceirizado,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET ativo= ? WHERE id= ?");
$stmt->bind_param("ss",$ativo,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET id_unidade= ? WHERE id= ?");
$stmt->bind_param("ss",$id_unidade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE colaborador SET id_categoria= ? WHERE id= ?");
$stmt->bind_param("ss",$id_categoria,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../register-user');
?>