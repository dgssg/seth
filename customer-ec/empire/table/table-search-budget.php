<?php
include("../database/database.php");
 
$query = "SELECT purchases_status.status,budget.descr,budget.id, budget.id_os, budget.solicitacao, budget.data_purchases, budget.upgrade, customer.empresa,equipamento_grupo.nome,equipamento_familia.fabricante,equipamento_familia.modelo,equipamento.codigo,equipamento.serie,equipamento.patrimonio FROM budget LEFT join customer on customer.id = budget.id_fornecedor LEFT JOIN os ON os.id = budget.id_os LEFT JOIN equipamento ON equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  LEFT JOIN equipamento_grupo ON equipamento_grupo.id = equipamento_familia.id_equipamento_grupo LEFT JOIN purchases_status ON purchases_status.id = budget.id_status_purchases ORDER BY budget.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
