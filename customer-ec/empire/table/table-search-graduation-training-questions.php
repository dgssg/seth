<?php
include("../database/database.php");
$query = "SELECT training_questions.id, training_questions.question,training_questions.type,training_questions.active,documentation_graduation.titulo,documentation_graduation.ativo,category.nome,equipamento_familia.nome AS 'equipamento',equipamento_familia.fabricante,equipamento_familia.modelo,documentation_graduation.dropzone FROM training_questions LEFT JOIN  documentation_graduation ON training_questions.id_training = documentation_graduation.id LEFT JOIN category ON category.id = documentation_graduation.id_categoria LEFT JOIN equipamento_familia ON equipamento_familia.id = documentation_graduation.id_equipamento_familia order by training_questions.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
