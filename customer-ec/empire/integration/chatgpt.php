<?php
$api_key = "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";

$data = [
    "model" => "gpt-3.5-turbo",
    "messages" => [
        ["role" => "system", "content" => "Você é um assistente especializado em engenharia clínica."],
        ["role" => "user", "content" => "Gere um relatório sobre a importância da manutenção preventiva de equipamentos médicos."]
    ]
];


$ch = curl_init("https://api.openai.com/v1/chat/completions");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    "Authorization: Bearer $api_key",
    "Content-Type: application/json"
]);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

$response = curl_exec($ch);
curl_close($ch);

$result = json_decode($response, true);

if ($result === null) {
    echo "Erro ao decodificar a resposta da API.<br>";
    echo "Resposta da API: " . htmlspecialchars($response);
    exit;
}

if (!isset($result["choices"][0]["message"]["content"])) {
    echo "A API retornou um erro:<br>";
    echo "<pre>" . print_r($result, true) . "</pre>";
    exit;
}

echo "<h2>Relatório Gerado:</h2>";
echo "<p>" . $result["choices"][0]["message"]["content"] . "</p>";

?>
