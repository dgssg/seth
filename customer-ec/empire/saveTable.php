<?php

include("database/database.php");

// Verificar se os dados foram recebidos via POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Recuperar os dados enviados via POST
    $codigoget = $_POST['tableId'];
    $tableHTML = $_POST['tableHTML'];
    
    // Escapando as aspas antes de salvar no banco de dados
    $tableHTML = addslashes($tableHTML);
    //$codigoget = 4451;
    //$tableHTML = "teste";
    // Preparar e executar a consulta SQL para atualizar os dados no banco de dados
    $stmt = $conn->prepare("UPDATE maintenance_preventive SET digital = ? WHERE id = ?");
    $stmt->bind_param("ss", $tableHTML, $codigoget);
    $execval = $stmt->execute();

    // Verificar se a consulta foi executada com sucesso e fechar a declaração
    if ($execval) {
        echo "Dados atualizados com sucesso.";
    } else {
        echo "Erro ao atualizar os dados.";
    }
    $stmt->close();
} else {
    // Se não foi uma solicitação POST, retornar uma mensagem de erro
    echo "Apenas solicitações POST são permitidas.";
}
?>
