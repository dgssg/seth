<?php
$codigoget = ($_GET["equipamento"]);
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
// Create connection

include("database/database.php");

?>

<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
             <h3>  Manutenção <small>Preventiva</small></h3>
            </div>


          </div>

          <div class="clearfix"></div>





          <div class="clearfix"></div>




            <!-- Posicionamento -->



           <div class="x_panel">
              <div class="x_title">
                <h2>Menu</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="flow-calibration">
                  <i class="glyphicon glyphicon-open"></i> Aberta
                </a>

                <a class="btn btn-app"  href="flow-calibration-before">
                  <i class="glyphicon glyphicon-export"></i> Atrasada
                </a>

               <a class="btn btn-app"  href="flow-calibration-close">
                  <i class="glyphicon glyphicon-save"></i> Fechada
                </a>

                 <a class="btn btn-app"  href="flow-calibration-print">
                  <i class="glyphicon glyphicon-print"></i> Impressão
                </a>

                <a class="btn btn-app"  href="flow-calibration-print-register">
                  <i class="glyphicon glyphicon-folder-open"></i> Registro de Impressão
                </a>

                <a class="btn btn-app"  href="flow-calibration-control">
                  <i class="fa fa-check-square-o"></i> Controle
                </a>

              </div>
            </div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

            <a class="btn btn-app"  href="backend/flow-calibration-print-all-backend.php" target="_blank" onclick="new PNotify({
                              title: 'Imprimir',
                              text: 'Imprimir Rotina',
                              type: 'info',
                              styling: 'bootstrap3'
                          });">
                  <i class="fa fa-print"></i> Imprimir Todas
                </a>


              </div>
            </div>

             <div class="x_panel">
              <div class="x_title">
                <h2>Lista MP para Impressão</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

              <?php include 'frontend/flow-calibration-print-open-frontend.php';?>

              </div>
            </div>




              </div>
            </div>
