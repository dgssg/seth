<?php
include("database/database.php");
$codigoget = ($_GET["id"]);
$pagina = ($_GET["pagina"]);


$query = "SELECT id, nome, upgrade, reg_date,yr,id_class,id_categoria,id_mp_group,id_procedimento_1,id_procedimento_2,id_periodicidade,id_time,id_colaborador,id_pop,accessories FROM equipamento_grupo WHERE id = $codigoget";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id,$nome,$upgrade,$regdate,$yr,$id_class,$id_categoria,$id_mp_group,$id_procedimento_1,$id_procedimento_2,$id_periodicidade,$id_time,$id_colaborador,$id_pop,$accessories);
   while ($stmt->fetch()) {
   }
}


?>
  <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
  <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
  
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Editar <small>Grupo Equipamento</small></h3>
              </div>


            </div>

             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar<small>Grupo Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<form  action="backend/register-equipament-group-edit-backend.php?id=<?php printf($codigoget);?>" method="post">

<div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="nome" class="form-control" name="nome" type="text" value="<?php printf($nome); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estimativa de Vida</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="yr" class="form-control" name="yr" type="number" value="<?php printf($yr); ?>" >
                        </div>
                      </div>
  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="grupo">Classificação de Risco  
    </label>
    <div class="col-md-6 col-sm-6">
      
      <select type="text" class="form-control has-feedback-right"
        name="id_class" id="id_class"
       >
        <option value="">Selecione um Item</option>
        <?php
          $sql = "SELECT  id, nome FROM anvisa_class ";
          if ($stmt = $conn->prepare($sql)) {
            $stmt->execute();
            $stmt->bind_result($id,$equipamento_grupo);
            while ($stmt->fetch()) {
        ?>
        <option value="<?php printf($id);?>	"
          <?php if($id==$id_class){printf("selected");}?>>
          <?php printf($equipamento_grupo);?> </option>
        <?php
          // tira o resultado da busca da memória
          }
          }
          $stmt->close();
        ?>
      </select>
      <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"
        class="docs-tooltip" data-toggle="tooltip"
        title=""></span>
      
      
    </div>
    
    <script>
      $(document).ready(function() {
        $('#id_class').select2();
      });
    </script>
  </div>
  <!--
   <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria</label>*
	     <div class="col-md-4 col-sm-4 ">
										<select type="text" class="form-control has-feedback-right" name="id_categoria" id="id_categoria"  placeholder="Categoria" >
										<option value="" disabled selected>Selecione uma Categoria</option>
										  	<?php



										   $sql = " SELECT id, nome FROM category WHERE ativo like '0' and trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	" <?php if($id == $id_categoria){ printf("selected"); } ?> ><?php printf($nome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a categoria "></span>

													</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#id_categoria').select2();
                                      });
                                 </script>
                                 <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grupo de Rotina</label>
      <div class="col-md-6 col-sm-6 ">
        <select type="text" class="form-control has-feedback-right" name="id_mp_group" id="id_mp_group"  placeholder="Grupo de Rotina"  >
          <option value="">Selecione uma opção</option>
          <?php
            
            
            
            $sql = " SELECT id, nome FROM maintenance_group  ";
            
            
            if ($stmt = $conn->prepare($sql)) {
              $stmt->execute();
              $stmt->bind_result($id,$nome);
              while ($stmt->fetch()) {
          ?>
          <option value="<?php printf($id);?>	"<?php if( $id == $id_mp_group){ printf("selected");} ?>><?php printf($nome);?>	 </option>
          <?php
            // tira o resultado da busca da memória
            }
            
            }
            $stmt->close();
            
          ?>
        </select>
        <span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a opção "></span>
        
      </div>
      
      
      <script>
        $(document).ready(function() {
          $('#id_mp_group').select2();
        });
      </script>
    </div>
																 <div class="item form-group">
																	 <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
																							<div class="col-md-4 col-sm-4 ">

																		 </div>
															 </div>
 <small>Procedimento com Laudo dentro da Validade</small>
					<center>


																  <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Procedimento</label>
	     <div class="col-md-4 col-sm-4 ">
												<select type="text" class="form-control has-feedback-right" name="id_procedimento_1" id="id_procedimento_1"  placeholder="Procedimento" value="" >
								 <option value="">Selecione um procedimento</option>
									<?php





										 $sql = "SELECT  id, name, codigo FROM maintenance_procedures WHERE trash =1";


			if ($stmt = $conn->prepare($sql)) {
			$stmt->execute();
			$stmt->bind_result($id_procedimento,$procedimento,$codigo);
			while ($stmt->fetch()) {
			?>
			<option value="<?php printf($id_procedimento);?>	" <?php if($id_procedimento == $id_procedimento_1){ printf("selected"); } ?>><?php printf($codigo);?> - <?php printf($procedimento);?>	</option>
											<?php
										// tira o resultado da busca da memória
										}

																					}
										$stmt->close();

										?>
																			</select>
																		</div>
											</div>

											 <script>
																	$(document).ready(function() {
																	$('#id_procedimento_1').select2();
																		});
															 </script>
                   	</center>
                        <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade</label>
	     <div class="col-md-4 col-sm-4 ">
                          <select class="form-control has-feedback-right" tabindex="-1" name="id_periodicidade" id="id_periodicidade" ="" >
                            <option value="">Selecione uma periodicidade</option>
                            <option value="1"  <?php if("1" == $id_periodicidade){ printf("selected"); } ?>>Diaria [1 dia]</option>
							<option value="5" <?php if( "5" == $id_periodicidade){ printf("selected"); } ?>>Diaria [ 1 dia Uteis]</option>

                            <option value="7" <?php if("7" == $id_periodicidade){ printf("selected"); } ?>>Semanal [7 dias]</option>
                            <option value="14" <?php if("14" == $id_periodicidade){ printf("selected"); } ?>>Bisemanal [14 dias]</option>
                            <option value="21" <?php if("21" == $id_periodicidade){ printf("selected"); } ?>>Trisemanal [21 dias]</option>
                            <option value="28" <?php if("28" == $id_periodicidade){ printf("selected"); } ?>>Quadrisemanal [28 dias]</option>
                            <option value="30" <?php if("30" == $id_periodicidade){ printf("selected"); } ?>>Mensal [30 dias]</option>
                            <option value="60" <?php if("60" == $id_periodicidade){ printf("selected"); } ?>>Bimensal [60 dias]</option>
                            <option value="90" <?php if("90" == $id_periodicidade){ printf("selected"); } ?>>Trimestral [90 dias]</option>
                            <option value="120" <?php if("120" == $id_periodicidade){ printf("selected"); } ?>>Quadrimestral [120 dias]</option>
                            <option value="180" <?php if("180" == $id_periodicidade){ printf("selected"); } ?>>Semestral [180 dias]</option>
                            <option value="365" <?php if("365" == $id_periodicidade){ printf("selected"); } ?>>Anual [365 dias]</option>
                            <option value="730" <?php if("730" == $id_periodicidade){ printf("selected"); } ?>>Bianual [730 dias]</option>
                            <option value="1095"<?php if($id_periodicidade == 1095){ printf("selected");};?>>Trianual [1095 Dias]</option>
                            <option value="1460"<?php if($id_periodicidade == 1460){ printf("selected");};?>>Quadrianual [1460 Dias]</option>

                          </select>
                          	<span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Periodicidade "></span>

                        </div>
                      </div>


                        <script>
                                    $(document).ready(function() {
                                    $('#id_periodicidade').select2();
                                      });
                                 </script>
                                 




 



    <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempo </label>
	     <div class="col-md-4 col-sm-4 ">

                            <input  type="text" id="id_time" name="id_time"  class="form-control "   pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99"  placeholder="Tempo" value="<?php printf($id_time);?>" ="" >


                        </div>
                      </div>
												<div class="item form-group">
										 		 <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
										 								<div class="col-md-4 col-sm-4 ">

										 			 </div>
										  </div>
											  <small>Procedimento com Laudo fora da Validade</small>

<center>


											 <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Procedimento</label>
	     <div class="col-md-4 col-sm-4 ">
											 			<select type="text" class="form-control has-feedback-right" name="id_procedimento_2" id="id_procedimento_2"  placeholder="Procedimento" value="">
											 <option value="">Selecione um procedimento</option>
											 <?php





											 	 $sql = "SELECT  id, name, codigo FROM maintenance_procedures WHERE trash =1";


											 if ($stmt = $conn->prepare($sql)) {
											 $stmt->execute();
											 $stmt->bind_result($id_procedimento,$procedimento,$codigo);
											 while ($stmt->fetch()) {
											 ?>
											 <option value="<?php printf($id_procedimento);?>	"<?php if($id_procedimento == $id_procedimento_2){ printf("selected"); } ?>><?php printf($codigo);?> - <?php printf($procedimento);?>	</option>
											 		<?php
											 	// tira o resultado da busca da memória
											 	}

											 												}
											 	$stmt->close();

											 	?>
											 										</select>
											 		</div>
											 	</div>

											 		 <script>
											 								$(document).ready(function() {
											 								$('#id_procedimento_2').select2();
											 									});
											 						 </script>
                        </center>




                        







												<div class="item form-group">
										 		 <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
										 								<div class="col-md-4 col-sm-4 ">

										 			 </div>
										  </div>


                    <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Colaborador</label>
	     <div class="col-md-4 col-sm-4 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador" >
										  <option value="">Selecione um Colaborador</option>

										  	<?php



										   $sql = "SELECT  id, primeironome, ultimonome FROM colaborador  WHERE trash =1";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$primeironome,$ultimonome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	" <?php if($id == $id_colaborador){ printf("selected"); } ?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
	                                     	<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um colaborador "></span>

                                        </div>
                                      </div>

								 <script>
                                    $(document).ready(function() {
                                    $('#id_colaborador').select2();
                                      });
                                 </script>

                     -->              


															 <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">POP</label>
	     <div class="col-md-4 col-sm-4 ">
															 <select type="text" class="form-control has-feedback-right" name="id_pop" id="id_pop"  placeholder="pop" value="">
															 <option value="">Selecione um pop</option>
															 <?php





															 $sql = "SELECT  id, titulo  FROM documentation_pop ";


															 if ($stmt = $conn->prepare($sql)) {
															 $stmt->execute();
															 $stmt->bind_result($id,$titulo);
															 while ($stmt->fetch()) {
															 ?>
															 <option value="<?php printf($id);?>	" <?php if($id == $id_pop){ printf("selected");};?> <?php if($id == $id_pop){ printf("selected"); } ?>>  <?php printf($titulo);?>	</option>
															 <?php
															 // tira o resultado da busca da memória
															 }

															 						}
															 $stmt->close();

															 ?>
															 				</select>

															 </div>
															</div>

															 <script>
															 		$(document).ready(function() {
															 		$('#id_pop').select2();
															 			});
															  </script>
															       <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">Acessórios</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="accessories" type="checkbox" class="js-switch"
                                                  <?php if($accessories == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
  
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                       <input readonly="readonly" type="text"  class="form-control"  value="<?php printf($regdate); ?> ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                       <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                        </div>
                      </div>






			






										
											<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar Informações"/>

												</form>
														<div class="ln_solid"></div>



                  </div>
                </div>
              </div>
	       </div>

        





	  <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament-group?pagina=<?php printf($pagina); ?>">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  <a class="btn btn-app"  href="report-equipament-in-family?id=<?php printf($codigoget); ?>" target="_blank">
                    <i class="glyphicon glyphicon-file"></i> Familias
                  </a>


              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
