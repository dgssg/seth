
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Rotina</small></h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

         <a class="btn btn-app"  href="maintenance-routine">
            <i class="glyphicon glyphicon-list-alt"></i> Rotinas
          </a>

          <a class="btn btn-app"  href="maintenance-routine-register">
            <i class="glyphicon glyphicon-plus"></i> Cadastro
          </a>

          <a class="btn btn-app"  href="backend/register-equipament-family-pop.php">
                    <i class="glyphicon glyphicon-refresh"></i> Atualização POP
                  </a>

                  <a class="btn btn-app"  href="maintenance-routine-incomplete-viewer" target="_blank" >
                    <i class="fa fa-file-pdf-o"></i> Sem Procedimentos
                  </a>

                  <a class="btn btn-app"  href="maintenance-routine-time-viewer" target="_blank" >
                    <i class="fa fa-file-pdf-o"></i> Sem Periodicidade
                  </a>
                    
                    <a class="btn btn-app"  href="backend/maintenance-routine-checklist-all-backend.php">
                      <i class="glyphicon  glyphicon-list-alt"></i> MP Check List
                    </a>
<?php //include 'frontend/maintenance-routine-register-frontend.php';?>




                  </div>
                </div>
              </div>
	       </div>
         <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista<small>Rotina</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

<?php include 'frontend/maintenance-routine-table-frontend.php';?>




                  </div>
                </div>
              </div>
	       </div>
	       
	       
          

            
	        
                  
                  
                </div>
              </div>
              
            