<style>
* {
  box-sizing: border-box;
}

body {
  background-color: #f1f1f1;
}

#regForm {
/*  background-color: #ffffff;
  margin: 100px auto;
  font-family: Raleway;
  padding: 5px;
  width: 70%;
  min-width: 200px;*/
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: ariel;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #04AA6D;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
  
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #04AA6D;
}
</style>



<?php

$query = "SELECT cal_val,cal_manufacture,cal_responsabile FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($cal_val,$cal_manufacture,$cal_responsabile);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }


?>
<form id="regForm" action="backend/action_page.php">

  <!-- One "tab" for each step in the form: -->
 <div class="tab">Cabeçalho:
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Realizado</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="data_start"  type="date" name="data_start" required>
    </div>
  </div>
  <script>
 
document.getElementById('regForm').addEventListener('submit', function(event) {
    var dataStart = document.getElementById('data_start').value;
    if (!dataStart) {
        alert('Por favor, preencha o campo de data.');
        event.preventDefault(); // Impede o envio do formulário
    }
});
 
</script>

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Validade (Meses)</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="date_validation"  type="number" name="val" value="<?php printf($cal_val); ?>" required>
    </div>
  </div>



  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
    <div class="col-md-6 col-sm-6 ">
      <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  >
        <option value="">Selecione a Empresa</option>
        <?php



        $sql = "SELECT  id, nome FROM calibration_parameter_manufacture ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$nome);
          while ($stmt->fetch()) {
            ?>
<option value="<?php printf($id);?>" <?php if($id == $cal_manufacture){ printf("selected"); }; ?> > <?php printf($nome);?>	</option>
            <?php
            // tira o resultado da busca da memória
          }

        }
        $stmt->close();

        ?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_manufacture').select2();
  });
  </script>

  <div class="form-group row">
    <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Empresa <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="unidade" id="unidade"   placeholder="Unidade">
  <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
        <?php



        $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#unidade').select2();
  });
  </script>



  

  <div class="form-group row">
      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Cliente <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
        <option value="">Selecione uma Opção</option>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#setor').select2();
  });
  </script>

<div class="form-group row">
      <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Localização <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Area <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
        <option value="">Selecione uma Opção</option>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#area').select2();
  });
  </script>



  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Equipamento</label>
    <div class="col-md-6 col-sm-6 ">
      <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento" required="required">
        <option value="">Selecione o equipamento</option>
        <?php



        $query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao where equipamento.ativo = 0 and equipamento.baixa is null and equipamento.trash != 0 or equipamento.ativo = 0 and equipamento.baixa = 1 and equipamento.trash != 0";

        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
        if ($stmt = $conn->prepare($query)) {
          $stmt->execute();
          $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($codigo);?>  - <?php printf($nome);?> - <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
            <?php
            // tira o resultado da busca da memória
          }

        }
        $stmt->close();

        ?>
      </select>
      <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#equipamento').select2();
  });
  </script>


  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo Externo</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="codigo"  type="text" name="codigo"  >
    </div>
  </div>






  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observa&ccedil;&atilde;o de calibra&ccedil;&atilde;o</label>
    <div class="col-md-4 col-sm-4 ">


      <textarea  rows="5" cols="5" id="obs"  class="form-control" name="obs" data-parsley-trigger="keyup"
      data-parsley-validation-threshold="2" >Este certificado &#233; valido para o equipamento ensaiado, n&#227;o sendo extensivo para quaisquer lotes, mesmo similar."  </textarea>

    </div>
  </div>


  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Colaborador <span class="required" >*</span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador" value="<?php printf($id_tecnico); ?>">
        <option value="	">Selecione o Executante</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>


  <script>
  $(document).ready(function() {
    $('#id_colaborador').select2();
  });
  </script>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Responsável ">Responsável Tecnico <span class="required" >*</span>
    </label>
    <div class="col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="id_responsavel" id="id_responsavel"  placeholder="Responsavel" value="<?php printf($id_tecnico); ?>">
        <option value="	">Selecione o Responsável</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"<?php if($id == $cal_responsabile){ printf("selected"); }; ?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_responsavel').select2();
  });
  </script>

  <div class="ln_solid"></div>

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Temperatura</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="date_validation"  type="text" name="temp" value="<?php printf($temp); ?>" required>
    </div>
  </div>

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Umidade</label>
    <div class="col-md-4 col-sm-4 ">
      <input id="date_validation"  type="text" name="hum" value="<?php printf($hum); ?>" required>
    </div>
  </div>

 <!-- <div class="item form-group ">

    <label  >Temperatura  </label>

    <input name="temp" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#26B99A" data-skin="tron" data-thickness=".2"  data-step=".1">
    <!-- <input id="carga" class="form-control" type="number" name="carga" " > -->


   <!-- <label >Umidade  </label>

    <input name="hum" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#34495E" data-skin="tron" data-thickness=".2"  data-step=".1">
    <!-- <input id="carga" class="form-control" type="number" name="carga"  " > -->
<!--
  </div>  -->
 
  

  </div>
   <div class="tab">Registro de Ensaio:
  
   <!--<button id="compose" class="btn btn-sm btn-success btn-block" type="button">Equipamento</button> -->
   
  <div id="calibration"> </div>


  </div> 
 
  <div style="overflow:auto;">
    <div style="float:right;">
    <button type="reset" onclick="clean()" class="btn btn-primary"onclick="new PNotify({
    title: 'Limpado',
    text: 'Todos os Campos Limpos',
    type: 'info',
    styling: 'bootstrap3'
  });" />Limpar</button>
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Voltar</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)" disabled>Avançar</button>
        <script>
        // Função para verificar se a data está preenchida e habilitar/desabilitar o botão
        function checkDate() {
            var dataStart = document.getElementById('data_start').value;
            var nextBtn = document.getElementById('nextBtn');
            if (dataStart) {
                nextBtn.disabled = false; // Habilita o botão
            } else {
                nextBtn.disabled = true; // Desabilita o botão
            }
        }

        // Adiciona um ouvinte de evento ao campo de data
        document.getElementById('data_start').addEventListener('input', checkDate);

        // Inicializa a verificação no carregamento da página
        window.onload = checkDate;

        // Exemplo de função nextPrev (pode ser personalizada de acordo com suas necessidades)
        function nextPrev(n) {
            // Código para avançar para a próxima etapa do formulário
          //  alert('Avançando para a próxima etapa!');
        }
    </script>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
     <span class="step"></span>
    
  </div>
</form>

 <!-- compose -->
    <div class="compose col-md-6  ">
      <div class="compose-header">
       Atualziar Equipamento
        <button type="button" class="close compose-close">
          <span>×</span>
        </button>
      </div>

      <div class="compose-body">
        <div id="alerts"></div>

  <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Parâmetro de Ensaio  <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <select type="text" class="form-control has-feedback-right" name="parametro" id="parametro"  placeholder="parametro">
                    <?php



                    $sql = "SELECT  calibration_parameter_dados_parameter.id, calibration_parameter_dados_parameter.nome, calibration_parameter_dados_parameter.unidade,calibration_parameter_dados.id FROM calibration_parameter_dados_parameter INNER JOIN calibration_parameter_dados on calibration_parameter_dados.id = calibration_parameter_dados_parameter.id_calibration_parameter_dados WHERE calibration_parameter_dados.id_equipamento_grupo = '$id_equipamento_grupo'";


                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id,$nome,$unidade,$id_equipamento_grupo_row);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>	"><?php printf($nome);?>-<?php printf($unidade);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
                  </select>
                  <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>


                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#parametro');
              });
              </script>

       
<div class="ln_solid"></div>

<div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Equipamento ou Analisador</label>
    <div class="col-md-6 col-sm-6 ">
    <select type="text" class="form-control " name="tools_rg" id="tools_rg"  placeholder="Equipamento e Analisador">

      <option value=""> Selecione um Equipamento ou Analisador</option>
      <?php 


     $result_cat_post  = "SELECT  id, nome,fabricante,modelo FROM calibration_parameter_tools WHERE id_equipamento_grupo  like '%$equipamento_familia' and trash = 1";

      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
        echo '<option> </option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].' '.$row_cat_post['fabricante'].' '.$row_cat_post['modelo'].'</option>';
      }
?> 

    </select>
 

  </div>
</div>

<script>
$(document).ready(function() {
  $('#tools_rg');
});
</script>

<div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Parâmetro</label>
    <div class="col-md-6 col-sm-6 ">
    <select type="text" class="form-control " name="parameter_rg" id="parameter_rg"  placeholder="Area">
      <option value="">Selecione um Parâmetro</option>
    </select>
  


  </div>
</div>

<script>
$(document).ready(function() {
  $('#parameter_rg');
});
</script>


       
      </div>

      <div class="compose-footer">
        <button id="send" class="btn btn-sm btn-success" type="button">Send</button>
      </div>
    </div>
    <!-- /compose -->

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Salvar";
  } else {
    document.getElementById("nextBtn").innerHTML = "Avançar";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  //  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    //  if (y[i].value == "") {
      // add an "invalid" class to the field:
     //   y[i].className += " invalid";
      // and set the current valid status to false
     //   valid = false;
      //  }
    // }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>
<!--
<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
 compose -->
<script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>
    								<script>
function clean() {
  $('#unidade').select2().load('reset_unidade.php');
  $('#area').select2().load('reset_area.php');
  $('#setor').select2().load('reset_setor.php');
  $('#equipamento').select2().load('reset_equipamento.php');


}

</script>


 <script type="text/javascript">
$(document).ready(function(){
  $('#tools_rg').change(function(){
    $('#parameter_rg').load('sub_categorias_post_tools_rg.php?tools_rg='+$('#tools_rg').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#equipamento').change(function(){
    $('#parametro').load('sub_categorias_post_equipament_parameter.php?equipamento='+$('#equipamento').val());

  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#equipamento').change(function(){
    $('#tools_rg').load('sub_categorias_post_equipament_tools_rg.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#equipamento').change(function(){
    $('#calibration').load('sub_categorias_post_equipament_calibration.php?equipamento='+$('#equipamento').val());
  });
});
</script>
      <script type="text/javascript">
$(document).ready(function(){
  $('#tools_rg').change(function(){
    $('#parameter_rg').load('sub_categorias_post_tools_rg.php?tools_rg='+$('#tools_rg').val());
  });
});
</script>


<script type="text/javascript">
       var tabela = document.getElementById('tabela').val();
       var conta = 0;
       var p = 0;
     //  var conta = document.getElementById('row').val();
      
       // conta = parseFloat(conta, 10);
       
       function novaLinha(code){
      //  tabelarow = document.getElementById(name);
       var tabelarow = "tabela_produto_"+code;
       var erro = "ac_"+code;
       var valueerro = parseFloat(document.getElementById(erro).value, 10);

     //  p = document.getElementById(code); 
     //  var conta = 0;

  // var date_start = document.getElementById("programada_data").value;
//  var  date_start = +$('#programada_data').val(programada_data);
 // $('#date_startsecond').val(date_start);
       conta = document.getElementById("row").value; 
      
        
        


        conta++;
        

          var tabela = document.getElementById(tabelarow); // Substitua 'sua-tabela' pelo ID da sua tabela
          var row = tabela.insertRow(-1); // Insere uma nova linha no final da tabela
          
          var partes = [
            "<input type='hidden' class='form-control' readonly value='" + conta + "'>",
            "<input type='hidden' class='form-control' readonly value='" + code + "' id='p_" + conta + "' name='p_" + conta + "'>",
            "<input type='hidden' class='form-control' readonly id='t_" + conta + "' name='t_" + conta + "' onfocus='t(" + code + "," + conta + ")'>",
            "<input type='hidden' class='form-control' readonly id='d_" + conta + "' name='d_" + conta + "' onfocus='d(" + code + "," + conta + ")'>",
            "<input type='number' class='form-control' id='ve_" + conta + "' name='ve_" + conta + "'>",
            "<input type='number' class='form-control' id='vl1_" + conta + "' name='vl1_" + conta + "' onfocus='average(" + conta + ")'>",
            "<input type='number' class='form-control' id='vl2_" + conta + "' name='vl2_" + conta + "' onfocus='average(" + conta + ")'>",
            "<input type='number' class='form-control' id='vl3_" + conta + "' name='vl3_" + conta + "' onfocus='average(" + conta + ")'>",
            "<input type='text' class='form-control' readonly id='vl_avg_" + conta + "' name='vl_avg_" + conta + "'>",
            "<input type='text' class='form-control' readonly id='erro_" + conta + "' name='erro_" + conta + "' onfocus='erro(" + conta + ")'>",
            "<input type='text' class='form-control' readonly id='dp_" + conta + "' name='dp_" + conta + "' onfocus='dp(" + conta + ")'>",
            "<input type='text' class='form-control' readonly id='ia_" + conta + "' name='ia_" + conta + "' onfocus='ia(" + conta + ")'>",
            "<input type='text' class='form-control' readonly id='ib_" + conta + "' name='ib_" + conta + "' onfocus='ib(" + code + "," + conta + ")'>",
            "<input type='text' class='form-control' readonly id='ic_" + conta + "' name='ic_" + conta + "' onfocus='ic(" + conta + ")'>",
            "<input type='hidden' class='form-control' value='2' id='k_" + conta + "' name='k_" + conta + "'>",
            "<input type='text' class='form-control' readonly id='ie_" + conta + "' name='ie_" + conta + "' onfocus='ie(" + conta + ")'>",
            "<input type='hidden' class='form-control' readonly id='erroac_" + conta + "' name='erroac_" + conta + "' value='" + valueerro + "'>",
            "<input type='text' class='form-control' readonly id='status_" + conta + "' name='status_" + conta + "' onfocus='status(" + code + "," + conta + ")'>"
          ];
        

          for (var i = 0; i < partes.length; i++) {
            var cell = row.insertCell(i);
            cell.innerHTML = partes[i];
          }
          //row.id = "tabela_linha_" + conta;  // Adiciona o ID à nova linha
          
          $('#row').val(conta);
        }
       function removeLinha(id){

           teste = document.getElementById(id);
           teste.parentNode.parentNode.removeChild(teste.parentNode);
}

function average(id){

  var v1 = "vl1_"+id;
  var v2 = "vl2_"+id;
  var v3 = "vl3_"+id;
  var avg = "vl_avg_"+id;
  var precavg = 0;

    var value1 = parseFloat(document.getElementById(v1).value, 10);
    var value2 = parseFloat(document.getElementById(v2).value, 10);
    var value3 = parseFloat(document.getElementById(v3).value, 10);
    precavg = value1 + value2 + value3;
    precavg = precavg/3;
    document.getElementById(avg).value = precavg;
}

function erro(id){

var ve = "ve_"+id;
var avg = "vl_avg_"+id;
var erro = "erro_"+id;   
var precavgpr = 0;

  var value_ve = parseFloat(document.getElementById(ve).value, 10);
  var value_avg = parseFloat(document.getElementById(avg).value, 10);

  precavgpr = value_ve - value_avg;
  precavgpr = Math.abs(precavgpr);
  document.getElementById(erro).value = precavgpr;
}

function dp(id){

var v1 = "vl1_"+id
var v2 = "vl2_"+id;
var v3 = "vl3_"+id;
var avg = "vl_avg_"+id;
var dp= "dp_"+id;
var pre1 = 0;
var pre2 = 0;
var pre3 = 0;
var pre = 0;
var result = 0;


  var value1 = parseFloat(document.getElementById(v1).value, 10);
  var value2 = parseFloat(document.getElementById(v2).value, 10);
  var value3 = parseFloat(document.getElementById(v3).value, 10);
  var value_avg = parseFloat(document.getElementById(avg).value, 10);

  pre1 = value1 - value_avg;
  pre1 = Math.pow(pre1,2);

  pre2 = value2 - value_avg;
  pre2 = Math.pow(pre2,2);
  
  pre3 = value3 - value_avg;
  pre3 = Math.pow(pre3,2);

  pre = pre1 + pre2 + pre3 ;

  pre = pre/3;

  result = Math.sqrt(pre);
  
  document.getElementById(dp).value = result;
}

function ia(id){
  var dp= "dp_"+id;
  var ia= "ia_"+id;
  var result = 0;
  var pre = 0;
  

  var value1 = parseFloat(document.getElementById(dp).value, 10);
  result = Math.sqrt(3);
  pre = value1/result;
  document.getElementById(ia).value = pre;

}

function ib(id,row){
  var ih= "ih_"+id;
  var ib= "ib_"+row;
     
  //passar dois parametros linha e id_calibration

  var value1 = parseFloat(document.getElementById(ih).value, 10);
 
  document.getElementById(ib).value = value1;

}

function ic(id){
  var ia= "ia_"+id;
  var ib= "ib_"+id;
  var ic= "ic_"+id;
  var pre1 = 0;
  var pre2 = 0;  
  var result = 0;
 

  var value1 = parseFloat(document.getElementById(ia).value, 10);
  var value2 = parseFloat(document.getElementById(ib).value, 10);



  pre1 = Math.pow(value1,2);
  pre2 = Math.pow(value2,2);

 
  result = Math.sqrt(Math.pow(value1, 2) + Math.pow(value2, 2));

  document.getElementById(ic).value = result;

}
function ie(id){
  var ie= "ie_"+id;
  var ic= "ic_"+id;
  var ik= "k_"+id;
   
  var result = 0;
 

  var value1 = parseFloat(document.getElementById(ic).value, 10);
  var value2 = parseFloat(document.getElementById(ik).value, 10);

  
  result = value1 * value2;

  document.getElementById(ie).value = result;

}
function ie(id){
  var ie = "ie_"+id;
  var ic = "ic_"+id;
  var ik = "k_"+id;
   
  var result = 0;
 

  var value1 = parseFloat(document.getElementById(ic).value, 10);
  var value2 = parseFloat(document.getElementById(ik).value, 10);

  
  result = value1 * value2;

  document.getElementById(ie).value = result;

}

function t(id,row){
  var tools = "tools_rg_"+id;
  var t = "t_"+row;
  var value = parseFloat(document.getElementById(tools).value, 10);
   document.getElementById(t).value = value;


}
function d(id,row){
  var dados = "parameter_rg_"+id;
  var d = "d_"+row;
  var value = parseFloat(document.getElementById(dados).value, 10);
   document.getElementById(d).value = value;


}
function kp(id){
  var p = "p_";
  var kp = "kp_"+id;
  var k = "k_";
  var number =0;
  var value1 =0;
  var value2 =0;

  conta = document.getElementById("row").value; 
  value1 = document.getElementById(kp).value; 

  for (let i = 1; i <= conta; i++) {
    number = p+i;
number = document.getElementById(number).value; 
if(id == number){
  value2 = k+i;
  document.getElementById(value2).value = value1;
} 
  }

}

function calcular(id){
  var p = "p_";
  var number =0;
  conta = document.getElementById("row").value; 
  for (let i = 1; i <= conta; i++) {
number = p+i;
number = document.getElementById(number).value; 
if(id == number){

  var tools = "tools_rg_"+id;
  var t = "t_"+i;
  var value = parseFloat(document.getElementById(tools).value, 10);
   document.getElementById(t).value = value;
   var dados = "parameter_rg_"+id;
  var d = "d_"+i;
  var value = parseFloat(document.getElementById(dados).value, 10);
   document.getElementById(d).value = value;

  
  var v1 = "vl1_"+i;
  var v2 = "vl2_"+i;
  var v3 = "vl3_"+i;
  var avg = "vl_avg_"+i;
  var precavg = 0;

    var value1 = parseFloat(document.getElementById(v1).value, 10);
    var value2 = parseFloat(document.getElementById(v2).value, 10);
    var value3 = parseFloat(document.getElementById(v3).value, 10);
    precavg = value1 + value2 + value3;
    precavg = precavg/3;
    document.getElementById(avg).value = precavg.toFixed(2);;

var ve = "ve_"+i;
var avg = "vl_avg_"+i;
var erro = "erro_"+i;   
var precavgpr = 0;

  var value_ve = parseFloat(document.getElementById(ve).value, 10);
  var value_avg = parseFloat(document.getElementById(avg).value, 10);

  precavgpr = value_ve - value_avg;
  precavgpr = Math.abs(precavgpr);
  document.getElementById(erro).value = precavgpr.toFixed(2);;

var v1 = "vl1_"+i
var v2 = "vl2_"+i;
var v3 = "vl3_"+i;
var avg = "vl_avg_"+i;
var dp= "dp_"+i;
var pre1 = 0;
var pre2 = 0;
var pre3 = 0;
var pre = 0;
var result = 0;


  var value1 = parseFloat(document.getElementById(v1).value, 10);
  var value2 = parseFloat(document.getElementById(v2).value, 10);
  var value3 = parseFloat(document.getElementById(v3).value, 10);
  var value_avg = parseFloat(document.getElementById(avg).value, 10);

  pre1 = value1 - value_avg;
  pre1 = Math.pow(pre1,2);

  pre2 = value2 - value_avg;
  pre2 = Math.pow(pre2,2);
  
  pre3 = value3 - value_avg;
  pre3 = Math.pow(pre3,2);

  pre = pre1 + pre2 + pre3 ;

  pre = pre/3;

  result = Math.sqrt(pre);
  
  document.getElementById(dp).value = result.toFixed(2);;

  var dp= "dp_"+i;
  var ia= "ia_"+i;
  var result = 0;
  var pre = 0;
  

  var value1 = parseFloat(document.getElementById(dp).value, 10);
  result = Math.sqrt(3);
  pre = value1/result;
  document.getElementById(ia).value = pre.toFixed(2);;

  var ih= "ih_"+id;
  var ib= "ib_"+i;
     
  //passar dois parametros linha e id_calibration

  var value1 = parseFloat(document.getElementById(ih).value, 10);
 
  document.getElementById(ib).value = value1.toFixed(2);;

  var ia= "ia_"+i;
  var ib= "ib_"+i;
  var ic= "ic_"+i;
  var pre1 = 0;
  var pre2 = 0;  
  var result = 0;
 

  var value1 = parseFloat(document.getElementById(ia).value, 10);
  var value2 = parseFloat(document.getElementById(ib).value, 10);

  pre1 = Math.pow(value1,2);
  pre2 = Math.pow(value2,2);

 
  result = Math.sqrt(Math.pow(value1, 2) + Math.pow(value2, 2));


  document.getElementById(ic).value = result.toFixed(2);;

  var ie= "ie_"+i;
  var ic= "ic_"+i;
  var ik= "k_"+i;
   
  var result = 0;
 

  var value1 = parseFloat(document.getElementById(ic).value, 10);
  var value2 = parseFloat(document.getElementById(ik).value, 10);

  
  result = value1 * value2;

  document.getElementById(ie).value = result.toFixed(2);;
  var ie = "ie_"+i;
  var avg = "vl_avg_"+i;
  var erro = "errop_"+id;
  var ac = "ac_"+id;
  var status = "status_"+i;
  var ve = "ve_"+i;
  
  var ap = 0;
  var ap1 = 0;
   var ap2 = 0;
  var status_1 = "AP";
  var status_2 = "RP";
  var status_3 = "ER";

  //passar dois parametros linha e id_calibration

  var value1 = parseFloat(document.getElementById(ie).value, 10);
  var value2 = parseFloat(document.getElementById(avg).value, 10);
  var value3 = parseFloat(document.getElementById(erro).value, 10);
  var value4 = parseFloat(document.getElementById(ac).value, 10);
  var value5 = parseFloat(document.getElementById(ve).value, 10);
    var erro2 = "erro_"+i;   
    var value6 = parseFloat(document.getElementById(erro2).value, 10);



  ap = value1 + value2;
  ap2 = value1 + value6;
    ap3 = (value4/100) * value5;
  
if (value3 < 1){

  ap1 =  (ap - value5);
  ap1 = Math.abs(ap1);

 

  if (ap2 < value4 ){

    document.getElementById(status).value = status_1;
    document.getElementById(status).style.color = 'green';
  } else {

    document.getElementById(status).value = status_2;
    document.getElementById(status).style.color = 'red';

  }


}
 if (value3 > 0){


 

   // ap1 = 100-ap1;

 
  //  ap1 = Math.abs(ap1);
  

  if (ap2 < ap3   ){

document.getElementById(status).value = status_1;
document.getElementById(status).style.color = 'green';
} else {

document.getElementById(status).value = status_2;
document.getElementById(status).style.color = 'red';


}


//document.getElementById(status).value = value3;


}


if(value3 != 0 && value3 != 1 ){
  document.getElementById(status).value = status_3;
  document.getElementById(status).style.color = 'blue';

}
  


}


 }   

}

     </script>
     <script>
      const input = document.querySelector('input[name="temp"]');

input.addEventListener('change', function() {
  const value = this.value.replace(',', '.');
  this.value = value;
});
const input = document.querySelector('input[name="hum"]');

input.addEventListener('change', function() {
  const value = this.value.replace(',', '.');
  this.value = value;
});

      </script>
     	<script>
function clean() {
  $('#unidade').select2().load('reset_unidade.php');
  $('#area').select2().load('reset_area.php');
  $('#setor').select2().load('reset_setor.php');
  $('#equipamento').select2().load('reset_equipamento.php');


}

</script>

