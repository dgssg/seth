  <?php
$codigoget = ($_GET["os"]);
date_default_timezone_set('America/Sao_Paulo');
$ano=date("Y");
// Create connection
include("database/database.php");// remover ../

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Acompanhamento de Manutenção Preventiva</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>




             <!-- page content -->








              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro <small>do Indicador</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="backend/report-indicator-kpi-26-backend.php" method="post"  target="_blank">

 <div class="ln_solid"></div>

   <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="yr" id="yr"  placeholder="equipamento">
										  <option value="">Selecione o ano</option>
										  	<?php



										 $query = "SELECT id, yr FROM kpi_year";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $yr);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($yr);?>	"<?php if($yr == $ano){ printf("selected"); } ?>><?php printf($yr);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Ano "></span>

										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#yr').select2();
                                      });
                                 </script>

                

 <div class="item form-group">
                         <?php if($assistence == "0"){  ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Empresa <span class="required"></span>
        </label>
        <?php }else {   ?>
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
        </label>            
        <?php }  ?>	
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="instituicao" id="instituicao"  placeholder="Unidade" required>
										 <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	
										  	<?php



										   $sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$instituicao);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($instituicao);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
											</div>

								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#instituicao').select2();
                                      });
                                 </script>

                                 <div class="form-group row">
           <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Cliente <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
           <div class="input-group col-md-6 col-sm-6">
  <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
        <?php if($assistence == "0"){  ?>
              <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>	  
                      </select>
  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


  </div>
      </div>

<script>
                   $(document).ready(function() {
                   $('#area').select2();
                     });
                </script>

                <div class="form-group row">
          <?php if($assistence == "0"){  ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Localização <span
                          class="required">*</span>
                        </label>
                        <?php }else {   ?>
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Area <span
                          class="required">*</span>
                        </label>        
                        <?php }  ?>	
           <div class="input-group col-md-6 col-sm-6">
  <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
      <option value="">Selecione uma Opção</option>
                      </select>
  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


  </div>
      </div>

<script>
                   $(document).ready(function() {
                   $('#setor').select2();
                     });
                </script>

                     

                     <br>


                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatório</button>
                         </center>
                        </div>
                      </div>




                  </div>
                </div>
              </div>
            </div>






             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="report-indicator">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




                </div>
              </div>







        </div>
            </div>
        <!-- /page content -->
    <script type="text/javascript">
    $(document).ready(function(){
        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });
    </script>
     <script type="text/javascript">
    $(document).ready(function(){
        $('#area').change(function(){
            $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
        });
      $('#family').load('family.php?'.val());
    });
    </script>
                       
