<?php
$codigoget = ($_GET["id"]);

include("database/database.php"); // Remover ../

$query = "SELECT maintenance_preventive.id_mp,maintenance_routine.id_maintenance_procedures_before,maintenance_routine.signature_digital, maintenance_routine.digital, maintenance_routine.id_category,maintenance_preventive.id_mp,maintenance_routine.id_fornecedor,maintenance_preventive.date_end,maintenance_preventive.file,maintenance_preventive.time_mp,maintenance_preventive.obs_tc,maintenance_preventive.obs_mp,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id like ?";

if ($stmt = $conn->prepare($query)) {
    $stmt->bind_param("s", $codigoget); // Bind do parâmetro para evitar SQL injection
    $stmt->execute();
    $stmt->bind_result($id_tecnico, $id_maintenance_procedures_before, $signature_digital, $digital, $id_category, $id_tecnico, $id_fornecedor, $date_end, $id_anexo, $time_mp, $obs_tc, $obs_mp, $programada, $ms_upgrade, $status, $date_end_ms, $date_start_ms, $time_ms, $rotina_id, $rotina, $data_start, $periodicidade, $reg_date, $upgrade, $codigo, $nome, $modelo);

    if ($stmt->fetch()) {
        // Prepare os dados para retornar
        $response_data = array(
            'date_start_ms' => $date_start_ms,
            'date_end_ms' => $date_end_ms,
            'time_mp' => $time_ms,
            'id_tecnico' => $id_tecnico
        );

        // Envie os dados de volta como resposta em formato JSON
        header('Content-Type: application/json');
        echo json_encode($response_data);
    } else {
        // Se a consulta não retornar resultados
        http_response_code(404); // Código 404 para indicar "não encontrado"
        echo json_encode(array('error' => 'Registro não encontrado.'));
    }
    
    $stmt->close();
} else {
    // Se houver um erro na preparação da consulta
    http_response_code(500); // Código 500 para indicar "erro interno do servidor"
    echo json_encode(array('error' => 'Erro na preparação da consulta.'));
}

$conn->close();
?>
