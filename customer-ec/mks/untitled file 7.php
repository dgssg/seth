<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Equipamento - Filtro e Resultados</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f4f4f4;
        }
    </style>
</head>
<body>

    <h2>Filtro de Equipamentos</h2>

    <!-- Formulário de Filtros -->
    <form id="filter-form">
        <label for="equipamento_id">ID do Equipamento:</label>
        <input type="number" id="equipamento_id" name="equipamento_id" min="1">
        
        <label for="grupo">Grupo:</label>
        <input type="text" id="grupo" name="grupo">
        
        <label for="ativo">Ativo:</label>
        <select id="ativo" name="ativo">
            <option value="">Selecione</option>
            <option value="1">Sim</option>
            <option value="0">Não</option>
        </select>
        
        <button type="button" onclick="fetchData()">Filtrar</button>
    </form>

    <!-- Tabela de Resultados -->
    <table id="result-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Patrimônio</th>
                <th>Grupo</th>
                <th>Equipamento</th>
                <th>Modelo</th>
                <th>Fabricante</th>
                <th>Série</th>
                <th>Ativo</th>
            </tr>
        </thead>
        <tbody>
            <!-- Os resultados serão inseridos aqui -->
        </tbody>
    </table>

    <script>
        function fetchData() {
            // Obtenha os valores dos filtros
            const equipamentoId = document.getElementById('equipamento_id').value;
            const grupo = document.getElementById('grupo').value;
            const ativo = document.getElementById('ativo').value;

            // Monta a URL com parâmetros
            let url = 'table/table-search-equipamento-filter.php?';
            if (equipamentoId) url += `equipamento_id=${equipamentoId}&`;
            if (grupo) url += `grupo=${encodeURIComponent(grupo)}&`;
            if (ativo) url += `ativo=${ativo}`;

            // Faz a requisição
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    // Limpa a tabela de resultados
                    const tbody = document.getElementById('result-table').getElementsByTagName('tbody')[0];
                    tbody.innerHTML = '';

                    // Insere as linhas na tabela
                    data.forEach(row => {
                        const tr = document.createElement('tr');
                        tr.innerHTML = `
                            <td>${row.id}</td>
                            <td>${row.patrimonio}</td>
                            <td>${row.grupo}</td>
                            <td>${row.equipamento}</td>
                            <td>${row.modelo}</td>
                            <td>${row.fabricante}</td>
                            <td>${row.serie}</td>
                            <td>${row.ativo ? 'Sim' : 'Não'}</td>
                        `;
                        tbody.appendChild(tr);
                    });
                })
                .catch(error => console.error('Erro ao buscar dados:', error));
        }
    </script>

</body>
</html>
