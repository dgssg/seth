<?php
include("database/database.php");
    $codigoget = ($_GET["id"]);
try {
    // Query para obter perguntas e respostas
    $query = "
        SELECT q.id AS question_id, q.question, q.type, a.id AS answer_id, a.answer, a.is_correct
        FROM training_questions q
        LEFT JOIN training_answers a ON q.id = a.id_question
        WHERE q.active = 1 and q.id_training = $codigoget
        ORDER BY q.id, a.id
    ";

    // Executa a consulta
    $resultados = $conn->query($query);

    // Verifica erros na execução da consulta
    if (!$resultados) {
        throw new Exception("Erro na consulta: " . $conn->error);
    }

    // Processa os resultados
    $questions = [];
    while ($row = $resultados->fetch_assoc()) {
        $questionId = $row['question_id'];

        // Adiciona a pergunta apenas uma vez
        if (!isset($questions[$questionId])) {
            $questions[$questionId] = [
                'question_id' => $questionId,
                'question' => $row['question'],
                'type' => $row['type'],
                'answers' => []
            ];
        }

        // Adiciona as alternativas, se existirem
        if (!is_null($row['answer_id'])) {
            $questions[$questionId]['answers'][] = [
                'answer_id' => $row['answer_id'],
                'answer' => $row['answer'],
                'is_correct' => $row['is_correct']
            ];
        }
    }

    // Retorna os dados como JSON
    echo json_encode(array_values($questions), JSON_PRETTY_PRINT);

} catch (Exception $e) {
    // Retorna erro
    echo json_encode(["error" => $e->getMessage()]);
}
?>
