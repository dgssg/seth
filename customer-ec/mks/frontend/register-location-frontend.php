 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Localização</small></h3>
              </div>

           
            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="unidade-tab" data-toggle="tab" href="#unidade" role="tab" aria-controls="unidade" aria-selected="true">Unidade</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="area-tab" data-toggle="tab" href="#area" role="tab" aria-controls="area" aria-selected="false">Area</a>
                      </li>
                       <li class="nav-item">
                        <a class="nav-link" id="setor-tab" data-toggle="tab" href="#setor" role="tab" aria-controls="setor" aria-selected="false">Setor</a>
                      </li>
                     
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="unidade" role="tabpanel" aria-labelledby="unidade-tab">
                          
                      <?php include 'frontend/register-location-unidade-frontend.php';?>
                      
                   
                      
                      </div>
                      <div class="tab-pane fade" id="area" role="tabpanel" aria-labelledby="area-tab">
                          
                       <?php include 'frontend/register-location-area-frontend.php';?>
                     
                     
                     
                      
                       
                      </div>
                       <div class="tab-pane fade" id="setor" role="tabpanel" aria-labelledby="setor-tab">
                          
                       <?php include 'frontend/register-location-setor-frontend.php';?>
                    
                              
                     
                      </div>
                    
                    </div>



                  </div>
                </div>
              </div>
	       </div>
	       
	        
                  
                  
                </div>
              </div>
              
            
        <!-- /page content -->