	<?php
include("../database/database.php");

?>
  <link rel="stylesheet" href="https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.css">
    <style>
        .main-container {
            width: 795px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro <small>Descritivo</small></h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cadastro<small>Descritivo</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<form  action="../backend/descritivo-add-backend.php" method="post">
	     
	     	
                          
                            <div class="col-md-12 col-sm-12">
      	<div class="col-md-12 col-sm-12 form-group has-feedback" >
										<select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="Grupo">
										  	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo  WHERE trash = 1 ";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
                                            }
											$stmt->close();
											?>
	                                     	</select>  
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
										 </div>	
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>
								
							
                                 
			<div class="col-md-12 col-sm-12 form-group has-feedback">
			<input type="text" class="form-control has-feedback-left" name="titulo" id="titulo"  placeholder="Titulo">
					<span class="fa fa-bookmark form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Titulo "></span>
					</div>
					
					
					
				
  <textarea name="editor" id="editor"  placeholder="" value="" rows="100" cols="800"><?php echo htmlspecialchars_decode($descritivo);?>
    
    
    
    
  </textarea>
  <script type="importmap">
    {
      "imports": {
        "ckeditor5": "https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.js",
        "ckeditor5/": "https://cdn.ckeditor.com/ckeditor5/42.0.0/"
      }
    }
  </script>
  <script type="module">
    import {
      ClassicEditor,
      Essentials,
      Paragraph,
      Bold,
      Italic,
      Font,
      Table,
      TableToolbar,
      PasteFromOffice,
      Image,
      ImageToolbar,
      ImageCaption,
      ImageStyle,
      ImageResize,
      ImageUpload,
      ImageInsert,
      List,
      Alignment,
      Heading,
      FontSize,
      WordCount
      
      
    } from 'ckeditor5';
    
    ClassicEditor
    .create(document.querySelector('#editor'), {
      plugins: [
        Essentials,
        Paragraph,
        Bold,
        Italic,
        Font,
        Table,
        TableToolbar,
        PasteFromOffice,
        Image,
        ImageToolbar,
        ImageCaption,
        ImageStyle,
        ImageResize,
        ImageUpload,
        ImageInsert,
        List,
        Alignment,
        Heading,
        FontSize,
        WordCount
        
      ],
      toolbar: [
        'undo', 'redo', '|',
        'heading', '|',
        'bold', 'italic', '|',
        'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', '|',
        'alignment', '|',
        'numberedList', 'bulletedList', '|',
        'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
        'imageUpload', 'imageInsert', '|', // Adicionando o botão ImageInsert na toolbar
        'outdent', 'indent'
      ],
      fontSize: {
        options: [
          'tiny',
          'small',
          'default',
          'big',
          'huge',
          '10',
          '12',
          '14',
          '16',
          '18',
          '20',
          '22',
          '24',
          '26',
          '28',
          '36',
          '48',
          '72'
        ],
        
      }, 
      image: {
        toolbar: [
          'imageTextAlternative', '|',
          'imageStyle:full', 'imageStyle:side', '|',
          'imageResize'
        ]
      },
      table: {
        contentToolbar: [
          'tableColumn', 'tableRow', 'mergeTableCells'
        ]
      }
    })
    .then(editor => {
      window.editor = editor;
    })
    .catch(error => {
      console.error(error);
    });
  </script>
  <!-- A friendly reminder to run on a server, remove this during the integration. -->
  <script>
    window.onload = function() {
      if (window.location.protocol === "file:") {
        alert("This sample requires an HTTP server. Please serve this file with a web server.");
      }
    };
  </script>
  
  <script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //	CKEDITOR.replace( 'editor1' );
  </script>
  
                      	
				
						
									
										
											<button type="reset" class="btn btn-primary"onclick="new PNotify({
																title: 'Limpado',
																text: 'Todos os Campos Limpos',
																type: 'info',
																styling: 'bootstrap3'
														});" />Limpar</button>
											<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" />
													
												</form>
														<div class="ln_solid"></div>



                  </div>
                </div>
              </div>
	       </div>
	  <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="dashboard">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
             
               
              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->
                  
                </div>
              </div>
	       
	        
                  
                  
                </div>
              </div>
              
  