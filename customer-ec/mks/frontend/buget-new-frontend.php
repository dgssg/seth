
<style>
  * {
    box-sizing: border-box;
  }
  
  body {
    background-color: #f1f1f1;
  }
  
  #regForm {
    /*  background-color: #ffffff;
    margin: 100px auto;
    font-family: Raleway;
    padding: 5px;
    width: 70%;
    min-width: 200px;*/
  }
  
  h1 {
    text-align: center;  
  }
  
  input {
    padding: 10px;
    width: 100%;
    font-size: 17px;
    font-family: ariel;
    border: 1px solid #aaaaaa;
  }
  
  /* Mark input boxes that gets an error on validation: */
  input.invalid {
    background-color: #ffdddd;
  }
  
  /* Hide all steps by default: */
  .tab {
    display: none;
  }
  
  button {
    background-color: #04AA6D;
    color: #ffffff;
    border: none;
    padding: 10px 20px;
    font-size: 17px;
    font-family: Raleway;
    cursor: pointer;
  }
  
  button:hover {
    opacity: 0.8;
  }
  
  #prevBtn {
    background-color: #bbbbbb;
  }
  
  /* Make circles that indicate the steps of the form: */
  .step {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none;  
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
    
  }
  
  .step.active {
    opacity: 1;
  }
  
  /* Mark the steps that are finished and valid: */
  .step.finish {
    background-color: #04AA6D;
  }
</style>

 <!-- page content -->
           <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Abertura de <small>Orçamento</small> </h3>
              </div>

           
            </div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Menu</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                
                <style>
                  .btn.btn-app {
                    border: 2px solid transparent; /* Define a borda como transparente por padrão */
                    padding: 5px;
                    position: relative; /* Necessário para posicionar o pseudo-elemento */
                  }
                  
                  .btn.btn-app.active {
                    border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
                  }
                  
                  .btn.btn-app.active::after {
                    content: "";
                    position: absolute;
                    left: 0;
                    bottom: 0; /* Posição da linha no final do elemento */
                    width: 100%;
                    height: 3px; /* Espessura da linha */
                    background-color: #ffcc00; /* Cor da linha */
                  }
                  
                </style>
                <?php
                  $current_page = basename($_SERVER['REQUEST_URI'], ".php");
                ?>
                
                <!-- Menu -->
                
                
                <a class="btn btn-app <?php echo $current_page == 'budget' ? 'active' : ''; ?>" href="budget">
                  <i class="glyphicon glyphicon-list-alt"></i> Consulta
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'budget-new' ? 'active' : ''; ?>" href="budget-new">
                  
                  <i class="glyphicon glyphicon-plus"></i> Registro
                </a>
                
                
                
              </div>
            </div>
            
            
            <div class="clearfix"></div>
             
              
            <form id="regForm" action="backend/buget-wizard-backend.php">
              <input id="row"  type="hidden" name="row" value="1">

              <!-- One "tab" for each step in the form: -->
              <div class="tab">Cabeçalho:
              
                  <div class="item form-group">
             <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ordem de Serviço</label>
                        <div class="col-md-4 col-sm-4 ">
										<select type="text" class="form-control has-feedback-left" name="id_os" id="id_os"  >
										     <option value="">Selecione uma Ordem de Serviço</option>
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  os.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM os INNER JOIN equipamento ON equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE os.id_status like '2' ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><b>  O.S:</b> <?php printf($id);?>	 <b> Equipamento: </b> <?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($modelo);?></option>
										  	<?php
											// tira o resultado da busca da mem贸ria
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-cog form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Ordem de serviço "></span>
										
										
								      	</div>	
								      </div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#id_os').select2();
                                      });
                                 </script>
                                 
                                 
                                 
        
         
	   
               

    


                      <label for="solicitacao">Orçamento  :</label>
                          <textarea id="solicitacao" class="form-control" name="solicitacao" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($solicitacao); ?>" value="<?php printf($solicitacao); ?>"><?php printf($solicitacao); ?></textarea>

                            <label for="justification">Observação :</label>
                          <textarea id="justification" class="form-control" name="justification" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($justification); ?>" value="<?php printf($justification); ?>"><?php printf($justification); ?></textarea>

                            <label for="technique">Observação Tecnica  :</label>
                          <textarea id="technique" class="form-control" name="technique" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($technique); ?>" value="<?php printf($technique); ?>"><?php printf($technique); ?></textarea>

                            <label for="des">Descrição :</label>
                          <textarea id="des" class="form-control" name="des" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($des); ?>" value="<?php printf($des); ?>"><?php printf($des); ?></textarea>

                            <div class="ln_solid"></div>

                           
                      
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Cliente <span></span>
                        </label>
                        
                        <div class="col-md-4 col-sm-4 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_fornecedor" id="id_fornecedor" >
										     <option value="">Selecione uma Opção</option>
										   
										  
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  id, empresa FROM customer  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$empresa);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"<?php if($id == $id_fornecedor){printf("selected");}?>	><?php printf($empresa);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                         
								 <script>
                                    $(document).ready(function() {
                                    $('#id_fornecedor').select2();
                                      });
                                 </script>
                                      
                                 
<div class="item form-group">
             <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Equipamento</label>
                        <div class="col-md-4 col-sm-4 ">
                         	<select multiple="multiple"  type="text" class="form-control has-feedback-right" name="id_equipamento[]" id="id_equipamento" readonly>
										   <option value="">Selecione um Equipamento</option>
										   
										  
										  
	                                     	</select>  
                        </div>
                      </div>
                      
                         
								 <script>
                                    $(document).ready(function() {
                                    $('#id_equipamento').select2();
                                      });
                                 </script>

 
 
        <script type="text/javascript">
    $(document).ready(function(){
      $('#id_os').change(function(){
        $('#vf_1').load('id_sub_categorias_post_os_equipamento.php?id_os='+$('#id_os').val());
        $('#id_equipamento').select2().load('id_sub_categorias_post_os_equipamento.php?id_os='+$('#id_os').val());
      });
    });
    $(document).ready(function(){
      $('#id_os').change(function(){
        $('#instituicao').select2().load('sub_categorias_post_os_instituicao.php?id_os='+$('#id_os').val());
      });
    });
    $(document).ready(function(){
      $('#id_os').change(function(){
        $('#area').select2().load('sub_categorias_post_os_area.php?id_os='+$('#id_os').val());
      });
    });
    $(document).ready(function(){
      $('#id_os').change(function(){
        $('#setor').select2().load('sub_categorias_post_os_setor.php?id_os='+$('#id_os').val());
      });
    });
    </script>
      <div class="ln_solid"></div>
                    
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_a">Data de Envio para o Cliente <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_a" name="data_a" value="<?php printf($data_a); ?>"   class="form-control ">
                        </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_b">Data da Aprovação do Cliente<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_b" name="data_b" value="<?php printf($data_b); ?>"   class="form-control ">
                        </div>
                      </div>
                      
              
                             
                <div class="ln_solid"></div>
                
             
                
                           
              </div>
              <div class="tab">Itens da Solicitação:
                
                <!--<button id="compose" class="btn btn-sm btn-success btn-block" type="button">Equipamento</button> -->
                <div class="row">
                  <div class="col-md-12 col-sm-12  ">
                    <div class="x_panel">
                      <div class="x_title">
                     
                        <ul class="nav navbar-right panel_toolbox">
                     Valor Total (R$):   <input  class="form-control " type="text" id="result_global" name="result_global" readonly> 

                          <li><a class="badge bg-green pull-right" value="Linha" onClick="javascript:novaLinha()"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Nova Linha</strong> </i></a>
                            <a class="badge bg-green pull-right"  onClick="javascript:calcular('')"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Calcular</strong> </i></a>
                                                      
                            
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Settings 1</a>
                                <a class="dropdown-item" href="#">Settings 2</a>
                              </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                            </ul>
                            <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                             
                              
                              
                              <table id="tabela_itens" >
                                <th class="table-header" >Item</th>
                                <th class="table-header">Unid</th>
                                <th class="table-header">Qtd</th>
                                <th class="table-header">Vlr. Unit.</th>
                                <th class="table-header">Valor</th>
                                 <th class="table-header">Equipamento</th>
                                  <th class="table-header">Ações</th>

                                
                                <tr id="row_1">
                                                             
          <th tabindex="1" > <input  class="form-control " type="text" id="va_1" name="va_1" > </th>
          <th tabindex="2" >  <input  class="form-control " type="text" id="vb_1" name="vb_1" > </th>
          <th tabindex="3" > <input  class="form-control " type="number" id="vc_1" name="vc_1"> </th>
          <th tabindex="4" > <input  class="form-control " type="number" id="vd_1" name="vd_1"> </th>
          <th tabindex="5" > <input  class="form-control " type="number" id="ve_1" name="ve_1"> </th>
          <th tabindex="6" ><select  type="text" class="form-control" name="vf_1" id="vf_1" > <option value="">Selecione um Equipamento</option></select> </th>      
          <th tabindex="7" ><a class="btn btn-danger" onClick="javascript:ocultarLinha('row_1')">Excluir</a></th>
          <th tabindex="8" ><input type="hidden" id="status_1" name="status_1" value="1"></th>
          <th tabindex="9" ><input type="hidden" id="line_1" name="line_1" value="1"></th>
                        	
                                </tr>
                                
                              
                                
                              </table>
                               <input type="hidden" id="row" value="1">
                              
                            </div>
                            </div>
                            </div>
                            </div>
                               
                
              </div> 
              
              <div style="overflow:auto;">
                <div style="float:right;">
                  <button type="reset" onclick="clean()" class="btn btn-primary"onclick="new PNotify({
                    title: 'Limpado',
                    text: 'Todos os Campos Limpos',
                    type: 'info',
                    styling: 'bootstrap3'
                  });" />Limpar</button>
                  <button type="button" id="prevBtn" onclick="nextPrev(-1)">Voltar</button>
                  <button type="button" id="nextBtn" onclick="nextPrev(1)">Avançar</button>
                </div>
              </div>
              <!-- Circles which indicates the steps of the form: -->
              <div style="text-align:center;margin-top:40px;">
                <span class="step"></span>
                <span class="step"></span>
                
              </div>
            </form>
            
                        <script>
              var currentTab = 0; // Current tab is set to be the first tab (0)
              showTab(currentTab); // Display the current tab
              
              function showTab(n) {
                // This function will display the specified tab of the form...
                var x = document.getElementsByClassName("tab");
                x[n].style.display = "block";
                //... and fix the Previous/Next buttons:
                if (n == 0) {
                  document.getElementById("prevBtn").style.display = "none";
                } else {
                  document.getElementById("prevBtn").style.display = "inline";
                }
                if (n == (x.length - 1)) {
                  document.getElementById("nextBtn").innerHTML = "Salvar";
                } else {
                  document.getElementById("nextBtn").innerHTML = "Avançar";
                }
                //... and run a function that will display the correct step indicator:
                fixStepIndicator(n)
              }
              
              function nextPrev(n) {
                // This function will figure out which tab to display
                var x = document.getElementsByClassName("tab");
                // Exit the function if any field in the current tab is invalid:
                if (n == 1 && !validateForm()) return false;
                // Hide the current tab:
                x[currentTab].style.display = "none";
                // Increase or decrease the current tab by 1:
                currentTab = currentTab + n;
                // if you have reached the end of the form...
                if (currentTab >= x.length) {
                  // ... the form gets submitted:
                  document.getElementById("regForm").submit();
                  return false;
                }
                // Otherwise, display the correct tab:
                showTab(currentTab);
              }
              
              function validateForm() {
                // This function deals with validation of the form fields
                var x, y, i, valid = true;
                x = document.getElementsByClassName("tab");
                y = x[currentTab].getElementsByTagName("input");
                // A loop that checks every input field in the current tab:
                //  for (i = 0; i < y.length; i++) {
                // If a field is empty...
                //  if (y[i].value == "") {
                // add an "invalid" class to the field:
                //   y[i].className += " invalid";
                // and set the current valid status to false
                //   valid = false;
                //  }
                // }
                // If the valid status is true, mark the step as finished and valid:
                if (valid) {
                  document.getElementsByClassName("step")[currentTab].className += " finish";
                }
                return valid; // return the valid status
              }
              
              function fixStepIndicator(n) {
                // This function removes the "active" class of all steps...
                var i, x = document.getElementsByClassName("step");
                for (i = 0; i < x.length; i++) {
                  x[i].className = x[i].className.replace(" active", "");
                }
                //... and adds the "active" class on the current step:
                x[n].className += " active";
              }
           

            </script>
   
            
       
           
             
           
           
            
            
          
            <script>
              const input = document.querySelector('input[name="temp"]');
              
              input.addEventListener('change', function() {
                const value = this.value.replace(',', '.');
                this.value = value;
              });
              const input = document.querySelector('input[name="hum"]');
              
              input.addEventListener('change', function() {
                const value = this.value.replace(',', '.');
                this.value = value;
              });
              
            </script>
           
              
              
              
          </div>
          
          
        </div>

<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>
<script type="text/javascript">
  var tabela = document.getElementById('tabela').val();

  var p = 0;

  
function novaLinha() {
    var tabela = document.getElementById('tabela_itens');
    var conta = parseInt(document.getElementById("row").value);
    conta++;

    var row = tabela.insertRow(-1);
    row.id = 'row_' + conta;

    var partes = [
        "<input type='text' class='form-control' id='va_" + conta + "' name='va_" + conta + "' >",
        "<input type='text' class='form-control' id='vb_" + conta + "' name='vb_" + conta + "'>",
        "<input type='number' class='form-control' id='vc_" + conta + "' name='vc_" + conta + "'>",
        "<input type='number' class='form-control' id='vd_" + conta + "' name='vd_" + conta + "'>",
        "<input type='number' class='form-control' id='ve_" + conta + "' name='ve_" + conta + "'>",
        "<select class='form-control' id='vf_" + conta + "' name='vf_" + conta + "'></select>",
        "<a class='btn btn-danger' onclick=\"javascript:ocultarLinha('row_" + conta + "')\">Excluir</a>",
        "<input type='hidden' id='status_" + conta + "' name='status_" + conta + "' value='1'>",
        "<input type='hidden' id='line_" + conta + "' name='line_" + conta + "' value='" + conta + "'>"
    ];

    for (var i = 0; i < partes.length; i++) {
        var cell = row.insertCell(i);
        cell.innerHTML = partes[i];
    }

    var select1 = document.getElementById('vf_1');
    var select2 = document.getElementById('vf_' + conta);

    for (var i = 0; i < select1.options.length; i++) {
        var opt = document.createElement('option');
        opt.value = select1.options[i].value;
        opt.text = select1.options[i].text;
        select2.appendChild(opt);
    }

    document.getElementById("row").value = conta;
}



    function removeLinha(id) {
        var row = document.getElementById(id);
        row.parentNode.removeChild(row);

        // Update the row counter
        var conta = parseInt(document.getElementById("row").value);
        conta--;
        document.getElementById("row").value = conta;

        // Recalculate the total
        calcular();
    }
  
  
function ocultarLinha(id) {
    var row = document.getElementById(id);
    if (row) {
        row.style.display = 'none';

        var inputs = row.getElementsByTagName('input');
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].type !== 'hidden') {
                inputs[i].value = 0;
            }
        }

        var selects = row.getElementsByTagName('select');
        for (var i = 0; i < selects.length; i++) {
            selects[i].selectedIndex = 0;
        }

        var statusElement = document.getElementById('status_' + id.split('_')[1]);
        if (statusElement) {
            statusElement.value = '0';
        }

        calcular(); // Recalcular os valores após ocultar a linha
    } else {
        console.error('Linha com ID ' + id + ' não encontrada.');
    }
}

     
function calcular() {
    var conta2 = parseInt(document.getElementById("row").value);
    var result = "result_global";
    var result_global = 0;  // Inicialize como um número

    for (let i = 1; i <= conta2; i++) {
        var vc = "vc_" + i;
        var vd = "vd_" + i;
        var ve = "ve_" + i;

        var vc_number = parseFloat(document.getElementById(vc).value); 
        var vd_number = parseFloat(document.getElementById(vd).value); 
        var ve_number = vc_number * vd_number;

        document.getElementById(ve).value = ve_number;
        result_global += ve_number;  // Acumula a soma
    }

    document.getElementById(result).value = result_global;
}

    
     function transferirValor() {
    // Obtém os elementos select
    var select1 = document.getElementById('vf_1');
    var select2 = document.getElementById('select2');

    // Obtém o valor selecionado do primeiro select
    var valorSelecionado = select1.options[select1.selectedIndex].value;
    var textoSelecionado = select1.options[select1.selectedIndex].text;

    // Cria uma nova opção para o segundo select
    var novaOpcao = document.createElement('option');
    novaOpcao.value = valorSelecionado;
    novaOpcao.text = textoSelecionado;

    // Adiciona a nova opção ao segundo select
    select2.appendChild(novaOpcao);
}
  
</script>