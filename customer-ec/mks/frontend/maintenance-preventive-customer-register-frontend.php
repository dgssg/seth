
<style>
  .btn.btn-app {
    border: 2px solid transparent; /* Define a borda como transparente por padrão */
    padding: 5px;
    position: relative; /* Necessário para posicionar o pseudo-elemento */
  }
  
  .btn.btn-app.active {
    border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
  }
  
  .btn.btn-app.active::after {
    content: "";
    position: absolute;
    left: 0;
    bottom: 0; /* Posição da linha no final do elemento */
    width: 100%;
    height: 3px; /* Espessura da linha */
    background-color: #ffcc00; /* Cor da linha */
  }
  
</style>
<?php
  $current_page = basename($_SERVER['REQUEST_URI'], ".php");
  $query = "SELECT cal_val,cal_manufacture,cal_responsabile FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($cal_val,$cal_manufacture,$cal_responsabile);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }
?>

 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  <small> Manutenção Preventiva </small> Cliente</h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                  
                    <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-customer-register' ? 'active' : ''; ?>" href="maintenance-preventive-customer-register">
                      <i class="glyphicon glyphicon-plus"></i> Cadastro
                    </a>   
                    <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-customer-open' ? 'active' : ''; ?>" href="maintenance-preventive-customer-open">
                      <i class=" glyphicon glyphicon-open"></i> Aberto
                    </a> 
                    <a class="btn btn-app <?php echo $current_page == 'maintenance-preventive-customer-close' ? 'active' : ''; ?>" href="maintenance-preventive-customer-close">
                      <i class="glyphicon glyphicon-save"></i> Fechada
                    </a> 
                    


                  </div>
                </div>
              </div>
	       </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Registro  <small> Manutenção Preventiva </small> Cliente</h2>                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    
                    <form  action="backend/maintenance-preventive-customer-backend.php" method="post">
                      
                      
                    
                       <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
    <div class="col-md-6 col-sm-6 ">
      <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  >
        <option value="	">Selecione a Empresa</option>
        <?php



        $sql = "SELECT  id, nome FROM calibration_parameter_manufacture ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$nome);
          while ($stmt->fetch()) {
            ?>
<option value="<?php printf($id);?>" <?php if($id == $cal_manufacture){ printf("selected"); }; ?> > <?php printf($nome);?>	</option>
            <?php
            // tira o resultado da busca da memória
          }

        }
        $stmt->close();

        ?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_manufacture').select2();
  });
  </script>
                      
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cliente</label>*
                        <div class="col-md-4 col-sm-4 ">
                          <select type="text" class="form-control has-feedback-right" name="id_customer" id="id_customer"  required>
                            <option  disabled selected>Selecione uma cliente</option>
                            <?php
                              
                              
                              
                              $sql = " SELECT id, empresa FROM customer";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" ><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                         
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#id_customer').select2();
                        });
                      </script>
                       
                                       
                        
                        
                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Procedimento</label>
                          <div class="col-md-4 col-sm-4 ">
                            <select type="text" class="form-control has-feedback-right" name="id_procedimento" id="id_procedimento"     required>
                              <option  disabled selected>Selecione um procedimento</option>
                              <?php
                                
                                
                                
                                
                                
                                $sql = "SELECT  id, name, codigo FROM maintenance_procedures WHERE trash =1";
                                
                                
                                if ($stmt = $conn->prepare($sql)) {
                                  $stmt->execute();
                                  $stmt->bind_result($id_procedimento,$procedimento,$codigo);
                                  while ($stmt->fetch()) {
                              ?>
                              <option value="<?php printf($id_procedimento);?>" <?php if($id_procedimento == $procedimento_1){ printf("selected"); } ?>><?php printf($codigo);?> - <?php printf($procedimento);?>	</option>
                              <?php
                                // tira o resultado da busca da memória
                                }
                                
                                }
                                $stmt->close();
                                
                              ?>
                            </select>
                          </div>
                        </div>
                        
                        <script>
                          $(document).ready(function() {
                            $('#id_procedimento').select2();
                          });
                        </script>
                  
                     
                  
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">POP</label>
                        <div class="col-md-4 col-sm-4 ">
                          <select type="text" class="form-control has-feedback-right" name="id_pop" id="id_pop"   >
                            <option >Selecione um pop</option>
                            <?php
                              
                              
                              
                              
                              
                              $sql = "SELECT  id, titulo  FROM documentation_pop ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$titulo);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>"  >  <?php printf($titulo);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#id_pop').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="tecnico">Equipamento <span
                          class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
    <input type="text" id="equipamento" name="equipamento" class="form-control ">
                        </div>
                      </div>
                      
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="tecnico">Serie <span
                          class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="serie" name="serie" class="form-control ">
                        </div>
                      </div>
                      
                      
                      <div class="ln_solid"></div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data</label>
                        <div class="col-md-4 col-sm-4 ">
                          <input class="form-control" class='date' type="date" name="data" id="data"  required="required" required></div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Validade </label>
                        <div class="col-md-4 col-sm-4 ">
                          <input class="form-control" class='date' type="date" name="validade" id="validade" required="required" required></div>
                      </div>
                      <!-- /form datepicker -->
                      
                      
                   
                      <input type="submit" class="btn btn-primary" name="submit1" onclick="new PNotify({
                        title: 'Registrado',
                        text: 'Informações registrada!',
                        type: 'success',
                        styling: 'bootstrap3'
                      });" value="Salvar" />
                                         
                    </form>
                                     <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>





                  </div>
                </div>
              </div>
	       </div>
	       
	       
          

            
	        
                  
                  
                </div>
              </div>
              
  