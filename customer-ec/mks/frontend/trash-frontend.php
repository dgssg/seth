


        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Lixeira</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lixeira <small>Check</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                <div class="col-xs-3">
                      <!-- required for floating -->
                      <!-- Nav tabs -->
                      <div class="nav nav-tabs justify-content-end bar_tabs" id="v-pills-tab" role="tablist" >
                        <a class="nav-link active" id="v-pills-unidade-tab" data-toggle="pill" href="#v-pills-unidade" role="tab" aria-controls="v-pills-unidade" aria-selected="true">    <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	     </a>
                        <a class="nav-link" id="v-pills-setor-tab" data-toggle="pill" href="#v-pills-setor" role="tab" aria-controls="v-pills-setor" aria-selected="false">    <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                            <?php }else {   ?>
                          <th>Setor</th>
                            <?php }  ?>	     </a>
                        <a class="nav-link" id="v-pills-area-tab" data-toggle="pill" href="#v-pills-area" role="tab" aria-controls="v-pills-area" aria-selected="false">    <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                            <?php }else {   ?>
                          <th>Area</th>
                            <?php }  ?>	     </a>
                        <a class="nav-link" id="v-pills-grupo-tab" data-toggle="pill" href="#v-pills-grupo" role="tab" aria-controls="v-pills-grupo" aria-selected="false">Grupo de Equipamento</a>
                        <a class="nav-link" id="v-pills-familia-tab" data-toggle="pill" href="#v-pills-familia" role="tab" aria-controls="v-pills-familia" aria-selected="false">Familia de Equipamento</a>

                        <a class="nav-link" id="v-pills-equipamento-tab" data-toggle="pill" href="#v-pills-equipamento" role="tab" aria-controls="v-pills-equipamento" aria-selected="false">Equipamento</a>
                         <a class="nav-link" id="v-pills-fornecedor-tab" data-toggle="pill" href="#v-pills-fornecedor" role="tab" aria-controls="v-pills-fornecedor" aria-selected="false">Fornecedor</a>
                        <a class="nav-link" id="v-pills-colaborador-tab" data-toggle="pill" href="#v-pills-colaborador" role="tab" aria-controls="v-pills-colaborador" aria-selected="false">Colaborador</a>

                        <a class="nav-link" id="v-pills-procedimento-tab" data-toggle="pill" href="#v-pills-procedimento" role="tab" aria-controls="v-pills-procedimento" aria-selected="false">Procedimento</a>
                         <a class="nav-link" id="v-pills-categoria-tab" data-toggle="pill" href="#v-pills-categoria" role="tab" aria-controls="v-pills-categoria" aria-selected="false">Categoria</a>
                         <a class="nav-link" id="v-pills-compras-tab" data-toggle="pill" href="#v-pills-compras" role="tab" aria-controls="v-pills-compras" aria-selected="false">Compras</a>
                          <a class="nav-link" id="v-pills-usuario-tab" data-toggle="pill" href="#v-pills-usuario" role="tab" aria-controls="v-pills-usuario" aria-selected="false">Usuário</a>
                          <a class="nav-link" id="v-pills-laudo-tab" data-toggle="pill" href="#v-pills-laudo" role="tab" aria-controls="v-pills-laudo" aria-selected="false">Laudo</a>
                          <a class="nav-link" id="v-pills-rotina-tab" data-toggle="pill" href="#v-pills-rotina" role="tab" aria-controls="v-pills-rotina" aria-selected="false">Rotina</a>
                          <a class="nav-link" id="v-pills-tools-tab" data-toggle="pill" href="#v-pills-tools" role="tab" aria-controls="v-pills-tools" aria-selected="false">Analisador e  Simulador</a>

                      <!--    <a class="nav-link" id="v-pills-rotina-tab" data-toggle="pill" href="#v-pills-rotina" role="tab" aria-controls="v-pills-rotina" aria-selected="false">Rotina</a> -->
                      </div>

                    </div>

                    <div class="col-xs-9">
                      <!-- Tab panes -->

                      <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-unidade" role="tabpanel" aria-labelledby="v-pills-unidade-tab">  <!-- UNIDADE -->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_2" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>

                      <?php
include("database/database.php");
$query = "SELECT id, instituicao, codigo, endereco, site, upgrade, reg_date FROM instituicao WHERE trash = 0";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $instituicao,$codigo,$endereco,$site,$update,$reg_date);


?>

<div >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_2" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Codigo</th>
                            <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                          <th>Endereço</th>
                          <th>Site</th>
                          <th>Update</th>
                          <th>Cadastro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                           <td><?php printf($codigo); ?> </td>
                          <td><?php printf($instituicao); ?> </td>
                          <td><?php printf($endereco); ?></td>
                            <td><?php printf($site); ?></td>
                              <td><?php printf($update); ?></td>
                          <td><?php printf($reg_date); ?></td>

                          <td>


               <a class="btn btn-app"  href="api-recycle-unidade-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-unidade-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

              <!-- UNIDADE -->

                        </div>

                        <div class="tab-pane fade" id="v-pills-setor" role="tabpanel" aria-labelledby="v-pills-setor-tab">
               <!-- SETOR -->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_3" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>

                 <?php

$query = "SELECT instituicao_area.id,instituicao_area.id_unidade,instituicao_area.custo,instituicao_area.nome,instituicao_area.upgrade,instituicao_area.reg_date, instituicao.id, instituicao.instituicao FROM instituicao_area INNER JOIN instituicao ON instituicao_area.id_unidade = instituicao.id WHERE instituicao_area.trash = 0";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $id_unidade,$custo,$nome,$upgrade,$reg_date,$id2,$instituicao);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div  >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_3" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                            <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                          <th>Centro de Custo</th>
                          <th>Nome</th>
                          <th>Update</th>
                          <th>Cadastro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($instituicao); ?> </td>
                          <td><?php printf($custo); ?></td>
                          <td><?php printf($nome); ?></td>
                          <td><?php printf($upgrade); ?></td>
                          <td><?php printf($reg_date); ?></td>

                          <td>


              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                      <a class="btn btn-app"  href="api-recycle-setor-backend?id=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-setor-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
                 <!-- SETOR -->
                        </div>

                        <div class="tab-pane fade" id="v-pills-area" role="tabpanel" aria-labelledby="v-pills-area-tab">
                         <!-- AREA -->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_4" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>

                        <?php

$query = "SELECT instituicao_localizacao.id, instituicao.instituicao,instituicao_area.custo, instituicao_area.nome,instituicao_localizacao.codigo,instituicao_localizacao.andar,instituicao_localizacao.nome,instituicao_localizacao.observacao,instituicao_localizacao.upgrade,instituicao_localizacao.reg_date FROM instituicao_localizacao INNER JOIN instituicao ON instituicao.ID =   instituicao_localizacao.id_unidade INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area WHERE instituicao_localizacao.trash = 0 ORDER BY instituicao_localizacao.id";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $instituicao,$custo,$nome_area,$codigo_localizacao,$andar,$nome_localizacao,$observacao,$upgrade,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div  >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_4" class="table table-striped table-bordered dt-responsive nowrap">                      <thead>
                        <tr>
                          <th>#</th>
                            <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	
                            <?php if($assistence == "0"){  ?>
                          <th>Cliente</th>
                          <?php }else {   ?>
                          <th>Setor</th>
                          <?php }  ?>	
                            <?php if($assistence == "0"){  ?>
                          <th>Localização</th>
                          <?php }else {   ?>
                          <th>Area</th>
                          <?php }  ?>	  

                          <th>Update</th>
                          <th>Cadastro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($instituicao); ?></td>
                            <td><?php printf($nome_area); ?></td>
                            <td><?php printf($nome_localizacao); ?> </td>

                              <td><?php printf($upgrade); ?></td>
                          <td><?php printf($reg_date); ?></td>

                          <td>



              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                   <a class="btn btn-app"  href="api-recycle-area-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-area-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
                         <!-- AREA -->
                        </div>
                        <div class="tab-pane fade" id="v-pills-grupo" role="tabpanel" aria-labelledby="v-pills-grupo-tab"> <!-- Grupo de Equipamento-->
                          <div class="x_content">
                            <div class="clearfix"></div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Filtro</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a class="dropdown-item" href="#">Settings 1</a>
                                          </li>
                                          <li><a class="dropdown-item" href="#">Settings 2</a>
                                          </li>
                                        </ul>
                                      </li>
                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <div id="userstable_filter_5" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                    
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>

                    <br />


                    <?php

$query = "SELECT id, nome, upgrade, reg_date FROM equipamento_grupo WHERE trash = 0 ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id,$nome,$upgrade,$regdate);


?>






                      <div class="row">
                          <div >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_5" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Atualização</th>
                          <th>Cadastro</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($nome); ?> </td>
                          <td><?php printf($upgrade); ?> </td>
                          <td><?php printf($regdate); ?> </td>


                          <td>
             <a class="btn btn-app"  href="api-recycle-grupo-equipamento-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-grupo-equipamento-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
















                  </div>

                         <!-- Grupo de Equipamento-->
                        </div>
                         <div class="tab-pane fade" id="v-pills-familia" role="tabpanel" aria-labelledby="v-pills-familia-tab"><!--Familia de Equipamento-->
                            <div class="clearfix"></div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Filtro</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a class="dropdown-item" href="#">Settings 1</a>
                                          </li>
                                          <li><a class="dropdown-item" href="#">Settings 2</a>
                                          </li>
                                        </ul>
                                      </li>
                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <div id="userstable_filter_6" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                    
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="row">
              <div >
                <div class="x_panel">

                  <div class="x_content">
                    <br />


                      <?php

$query = "SELECT equipamento_grupo.nome, equipamento_familia.id, equipamento_familia.id_equipamento_grupo, equipamento_familia.nome, equipamento_familia.fabricante, equipamento_familia.modelo, equipamento_familia.anvisa, equipamento_familia.img, equipamento_familia.upgrade, equipamento_familia.reg_date FROM equipamento_familia INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo WHERE equipamento_familia.trash = 0 ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($equipamento_grupo,$id, $id_equipamento_grupo, $nome, $fabricante, $modelo, $anvisa, $img, $upgrade, $reg_date);


?>






                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_6" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Grupo</th>
                          <th>Fabricante</th>
                          <th>Modelo</th>
                          <th>Anvisa</th>
                          <th>Atualização</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($equipamento_grupo); ?> </td>
                          <td><?php printf($fabricante); ?> </td>
                         <td><?php printf($modelo); ?> </td>
                          <td><?php printf($anvisa); ?> </td>
                          <td><?php printf($upgrade); ?> </td>



                          <td>

                <a class="btn btn-app"  href="api-recycle-familia-equipamento-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-familia-equipamento-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>





                  </div>
                </div>
              </div>
            </div>
                         <!--Familia de Equipamento-->
                         </div>
                         <div class="tab-pane fade" id="v-pills-equipamento" role="tabpanel" aria-labelledby="v-pills-equipamento-tab"> <!--Equipamento-->
                            <div class="clearfix"></div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Filtro</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a class="dropdown-item" href="#">Settings 1</a>
                                          </li>
                                          <li><a class="dropdown-item" href="#">Settings 2</a>
                                          </li>
                                        </ul>
                                      </li>
                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <div id="userstable_filter_7" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                    
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>

                         <?php

$query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao WHERE equipamento.trash = 0";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

<div >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_7" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Equipamento</th>
                          <th>Codigo</th>
                          <th>Localização</th>
                          <th>Ação</th>


                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($nome); ?> <?php printf($modelo); ?> <?php printf($fabricante); ?></td>
                           <td><?php printf($codigo); ?> </td>
                          <td><?php printf($localizacao); ?> </td>


                          <td>
                 <a class="btn btn-app"  href="api-recycle-equipamento-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-equipamento-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                       </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


                         <!--Equipamento--></div>
                        <div class="tab-pane fade" id="v-pills-fornecedor" role="tabpanel" aria-labelledby="v-pills-fornecedor-tab"><!--Fornecedor-->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-9 col-sm-9 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_8" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>

                        <?php



$query = "SELECT id, empresa, cnpj, seguimento, adress, city, state, cep, contato, email, telefone_1, telefone_2, observacao,  reg_date, upgrade FROM fornecedor WHERE trash = 0";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $empresa, $cnpj, $seguimento, $adress, $city, $state, $cep, $contato, $email, $telefone_1, $telefone_2, $observacao,$reg_date,$update);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

                          <div  >
                            <div class="x_panel">
                              
                              <div class="x_content">
                                <div class="row">
                                  <div  >
                                    <div class="card-box table-responsive">
                              
                              <table id="datatable_8" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Empresa</th>
                          <th>CNPJ</th>
                          <th>Seguimento</th>
                          <th>Endereço</th>
                          <th>Cidade</th>
                          <th>Estado</th>

                         


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($empresa); ?> </td>
                          <td><?php printf($cnpj); ?></td>
                          <td><?php printf($seguimento); ?></td>
                          <td><?php printf($adress); ?></td>
                          <td><?php printf($city); ?></td>
                          <td><?php printf($state); ?></td>

                         



                          <td>
                  <a class="btn btn-app"  href="api-recycle-fornecedor-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-fornecedor-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


                        <!--Fornecedor-->
                        </div>
                        <div class="tab-pane fade" id="v-pills-colaborador" role="tabpanel" aria-labelledby="v-pills-colaborador-tab"><!--Colaborador-->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_9" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>

                       <?php

$query = "SELECT id, primeironome, ultimonome,email,telefone,cargo,matricula,nascimento,entidade,reg_date FROM colaborador WHERE trash = 0 ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $primeironome,$ultimonome,$email,$telefone,$cargo,$matricula,$nascimento,$entidade,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

                          <div  >
                            <div class="x_panel">
                              
                              <div class="x_content">
                                <div class="row">
                                  <div  >
                                    <div class="card-box table-responsive">

                              
                              <table id="datatable_9" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Colaborador</th>
                          <th>Email</th>
                          <th>Telefone</th>
                          <th>Cargo</th>
                          <th>Matricula</th>
                          <th>Nascimento</th>
                           

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($primeironome); ?> <?php printf($ultimonome); ?></td>
                          <td><?php printf($email); ?></td>
                            <td><?php printf($telefone); ?></td>
                              <td><?php printf($cargo); ?></td>
                               <td><?php printf($matricula); ?></td>
                                <td><?php printf($nascimento); ?></td>
                             

                          <td>
                  <a class="btn btn-app"  href="api-recycle-colaborador-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-colaborador-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                 

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


                        <!--Colaborador-->
                        </div>
                        <div class="tab-pane fade" id="v-pills-procedimento" role="tabpanel" aria-labelledby="v-pills-procedimento-tab"><!--Procedimento-->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_10" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>

                        <?php

$query = "SELECT maintenance_procedures.id, maintenance_procedures.name,category.nome,maintenance_procedures.codigo,maintenance_procedures.reg_date,maintenance_procedures.upgrade FROM maintenance_procedures INNER JOIN category ON category.id = maintenance_procedures.id_category WHERE maintenance_procedures.trash = 0  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $name, $categoria, $codigo, $reg_date, $upgrade);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

                          <div  >
                            <div class="x_panel">
                              
                              <div class="x_content">
                                <div class="row">
                                  <div  >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_10" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Categoria</th>


                          <th>Codigo</th>
                          <th>Atualização</th>
                           


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($name); ?> </td>
                          <td><?php printf($categoria); ?></td>


                             <td><?php printf($codigo); ?></td>
                                <td><?php printf($upgrade); ?></td>

                         

                          <td>
                 <a class="btn btn-app"  href="api-recycle-procedimento-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-procedimento-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


                        <!--Procedimento-->
                        </div>
                        <div class="tab-pane fade" id="v-pills-categoria" role="tabpanel" aria-labelledby="v-pills-categoria-tab"><!--Categoria-->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_11" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>

                                         <?php

$query = "SELECT id,nome,upgrade,reg_date FROM category WHERE trash = 0  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $name, $upgrade,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

<div  >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_11" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>

                          <th>Atualização</th>
                          <th>Cadastro</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($name); ?> </td>

                                <td><?php printf($upgrade); ?></td>

                          <td><?php printf($reg_date); ?></td>

                          <td>
                 <a class="btn btn-app"  href="api-recycle-categoria-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-categoria-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

                        <!--Categoria-->
                        </div>
                        <div class="tab-pane fade" id="v-pills-laudo" role="tabpanel" aria-labelledby="v-pills-laudo-tab"><!--Laudo-->
                          <div class="clearfix"></div>
                          <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Filtro</h2>
                                  <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item" href="#">Settings 1</a>
                                        </li>
                                        <li><a class="dropdown-item" href="#">Settings 2</a>
                                        </li>
                                      </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div id="userstable_filter_12" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                  
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>


                        <?php

$query = "SELECT calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration INNER JOIN equipamento on equipamento.id = calibration.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia WHERE calibration.trash =0";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div  >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_12" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Laudo</th>
                          <th>Equipamento</th>
                          <th>Realização</th>
                          <th>Validade</th>
                          <th>vencido</th>
                           <th>Status</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($codigo); ?> <?php printf($nome); ?> <?php printf($fabricante); ?> <?php printf($modelo); ?></td>
                          <td><?php printf($data_start); ?></td>
                            <td><?php printf($val); ?> Meses</td>
                            <td><?php if($ven ==0) {printf("Não");} if($ven ==1) {printf("Sim");}  ?></td>
                            <td><?php if($status ==0) {printf("Aprovado");} if($status ==1) {printf("Reprovado");}  ?> </td>



                          <td>
                    <a class="btn btn-app"  href="api-recycle-laudo-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-laudo-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


                        <!--Laudo-->
                        </div>
                           <div class="tab-pane fade" id="v-pills-compras" role="tabpanel" aria-labelledby="v-pills-compras-tab"><!--Compras-->

                              <div class="clearfix"></div>
                              <div class="row">
                                <div class="col-md-12 col-sm-12 ">
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <h2>Filtro</h2>
                                      <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                          <ul class="dropdown-menu" role="menu">
                                            <li><a class="dropdown-item" href="#">Settings 1</a>
                                            </li>
                                            <li><a class="dropdown-item" href="#">Settings 2</a>
                                            </li>
                                          </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                      </ul>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                      <div id="userstable_filter_13" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                      
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>

                           <?php

$query = "SELECT purchases.id, purchases.id_os, purchases.solicitacao, purchases.data_purchases, purchases.upgrade, fornecedor.empresa FROM purchases INNER JOIN fornecedor on fornecedor.id = purchases.id_fornecedor WHERE purchases.id_status_purchases like '2' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $os,$solicitacao,$data,$upgrade,$fornecedor);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div >
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_13" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Solicitação</th>
                          <th>O.S</th>
                          <th>Centro de Custo</th>
                          <th>Data</th>
                          <th>Atualização</th>
                          <th>Fornecedor</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($os); ?> </td>
                          <td><?php printf($solicitacao); ?></td>
                            <td><?php printf($data); ?></td>
                            <td><?php printf($upgrade); ?> </td>
                          <td><?php printf($fornecedor); ?></td>


                          <td>
                    <a class="btn btn-app"  href="api-recycle-compras-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-compras-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>




                           </div>



                             <div class="tab-pane fade" id="v-pills-usuario" role="tabpanel" aria-labelledby="v-pills-usuario-tab"><!--Usuário-->
                                <div class="clearfix"></div>
                                <div class="row">
                                  <div class="col-md-12 col-sm-12 ">
                                    <div class="x_panel">
                                      <div class="x_title">
                                        <h2>Filtro</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                          </li>
                                          <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                              <li><a class="dropdown-item" href="#">Settings 1</a>
                                              </li>
                                              <li><a class="dropdown-item" href="#">Settings 2</a>
                                              </li>
                                            </ul>
                                          </li>
                                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                                          </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="x_content">
                                        <div id="userstable_filter_14" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                                        
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="clearfix"></div>

                             <?php

$query = "SELECT id,nome,sobrenome,matricula,email,telefone,reg_date FROM usuario   WHERE trash = 0";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome,$sobrenome,$matricula,$email,$telefone,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>
                                <div  >
                                  <div class="x_panel">
                                    
                                    <div class="x_content">
                                      <div class="row">
                                        <div  >
 
                            <div class="card-box table-responsive">
                              
                              <table id="datatable_14" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Usuário</th>
                          <th>Matricula</th>
                          <th>Email</th>

                          <th>Telefone</th>

                          <th>Registro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($nome); ?> <?php printf($sobrenome); ?> </td>
                          <td><?php printf($matricula); ?></td>
                            <td><?php printf($email); ?></td>
                              <td><?php printf($telefone); ?></td>

                          <td><?php printf($reg_date); ?></td>

                          <td>
                 <a class="btn btn-app"  href="api-recycle-usuario-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-usuario-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>






                      </div>
                    </div>



                    <div class="clearfix"></div>

   <div class="tab-pane fade" id="v-pills-rotina" role="tabpanel" aria-labelledby="v-pills-rotina-tab"><!--Rotina-->
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Filtro</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div id="userstable_filter_15" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
              
              
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      
                             <?php

$query = "SELECT equipamento_familia.fabricante,equipamento.serie,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE maintenance_routine.trash =0" ;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($fabricante,$serie,$instituicao,$area,$setor,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);



?>

      <div  >
        <div class="x_panel">
          
          <div class="x_content">
            <div class="row">
              <div  >
                <div class="card-box table-responsive">
                              
                              <table id="datatable_15" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                        <th>Rotina</th>
                          <th>Data Inicio</th>
                          <th>Periodicidade</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                         



                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                        <td><?php printf($rotina); ?> </td>
                          <td><?php printf($data_start); ?> </td>
                          <td><?php if($periodicidade==5){printf("Diaria [1 Dias Uteis ]");}if($periodicidade==365){printf("Anual [365 Dias]");}if($periodicidade==180){printf("Semestral [180 Dias]");} if($periodicidade==30){printf("Mensal [30 Dias]");}if($periodicidade==1){printf("Diaria [1 Dias]");} if($periodicidade==7){printf("Semanal [7 Dias]");}if($periodicidade==14){printf("Bisemanal [14 Dias]");}if($periodicidade==21){printf("Trisemanal [21 Dias]");} if($periodicidade==28){printf("Quadrisemanal [28 Dias]");} if($periodicidade==60){printf("Bimestral [60 Dias]");}if($periodicidade==90){printf("Trimestral [90 Dias]");}if($periodicidade==120){printf("Quadrimestral [120 Dias]");}if($periodicidade==730){printf("Bianual [730 Dias]");} 
                            if($periodicidade==1460){printf("Quadrianual [1460 Dias]");}if($periodicidade==1095){printf("Trianual [1095 Dias]");} ?></td>
                          <td><?php printf($nome); ?> </td>
                          <td> <?php printf($modelo); ?></td>
                          <td> <?php printf($fabricante); ?></td>
                           <td><?php printf($codigo); ?> </td>
                           
 
                          <td>
                 <a class="btn btn-app"  href="api-recycle-rotina-backend?id=<?php printf($rotina); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-rotina-backend.php?id=<?php printf($rotina); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>






                      </div>
                    </div>


                    <div class="clearfix"></div>

   <div class="tab-pane fade" id="v-pills-tools" role="tabpanel" aria-labelledby="v-pills-rotina-tab"><!--Rotina-->
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Filtro</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div id="userstable_filter_1" style="column-count:6; -moz-column-count:6; -webkit-column-count:6; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
              
              
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
                             <?php

date_default_timezone_set('America/Sao_Paulo');
$ano=date("Y");
$today=date("Y-m-d");
$mes=date("m");
$query = "SELECT   calibration_parameter_tools_laudos.file,calibration_parameter_tools.id, calibration_parameter_tools.nome, calibration_parameter_tools.fabricante, calibration_parameter_tools.modelo, calibration_parameter_tools.serie, calibration_parameter_tools.patrimonio,calibration_parameter_tools.calibration,calibration_parameter_tools.date_calibration, calibration_parameter_tools.date_validation FROM calibration_parameter_tools LEFT JOIN calibration_parameter_tools_laudos ON calibration_parameter_tools_laudos.id_calibration_parameter_tools = calibration_parameter_tools.id and calibration_parameter_tools_laudos.date_validation =  calibration_parameter_tools.date_validation WHERE calibration_parameter_tools.trash = 0 GROUP BY calibration_parameter_tools.id ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($file,$id, $nome, $fabricante, $modelo, $serie, $patrimonio,$calibration, $date_calibration, $date_validation);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  } calibration_parameter_tools_laudos.file,



?>

      <div  >
        <div class="x_panel">
          
          <div class="x_content">
            <div class="row">
              <div  >
                <div class="card-box table-responsive">
                              
                              <table id="datatable_1" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                        <th>#</th>
                     
                          <th>Nome</th>
                          <th>Fabricante</th>
                          <th>Modelo</th>
                          <th>Nº Serie</th>
                         
                          <th>Nº Certificado</th>
                          <th>Calibração</th>
                           
                         

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                        <td><?php printf($id); ?> </td>
                       
                          <td><?php printf($nome); ?> </td>
                          <td><?php printf($fabricante); ?> </td>
                          <td><?php printf($modelo); ?> </td>
                          <td><?php printf($serie); ?> </td>
                         
                          <td><?php printf($calibration); ?> </td>
                           <td><?php printf($date_calibration); ?> </td>
                           
                           
                          <td>
                 <a class="btn btn-app"  href="api-recycle-calibration-parameter-equipament-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-calibration-parameter-equipament-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>






                      </div>
                    </div>
                    <!-- end -->
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_1').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5,6])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_1 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_1');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_1'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_1').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_1')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_2').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_2 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_2');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_2'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_2').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_2')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_3').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_3 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_3');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_3'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_3').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_3')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_4').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_4 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_4');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_4'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_4').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_4')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_5').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_5 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_5');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_5'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_5').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_5')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_6').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_6 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_6');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_6'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_6').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_6')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_7').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_7 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_7');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_7'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_7').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_7')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_8').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_8 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_8');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_8'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_8').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_8')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_9').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_9 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_9');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_9'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_9').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_9')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_10').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_10 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_10');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_10'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_10').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_10')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_11').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_11 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_11');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_11'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_11').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_11')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_12').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_12 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_12');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_12'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_12').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_12')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_13').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_13 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_13');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_13'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_13').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_13')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_14').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_14 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_14');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_14'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_14').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_14')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable_15').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([1,2,3,4,5])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable_15 th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter_15');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter_15'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable_15').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter_15')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>

