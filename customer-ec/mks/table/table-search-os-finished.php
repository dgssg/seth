<?php
include("../database/database.php");
$query = "SELECT regdate_os_dropzone.file, custom_service.nome AS 'custom_service',category.nome AS 'category',equipamento_familia.fabricante,os_sla.sla_verde,os_sla.sla_amarelo,os.sla_term,os_grupo_sla.grupo,os_posicionamento.cor,os_prioridade.prioridade,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome AS 'usuario',usuario.sobrenome AS 'sobrenome',instituicao_localizacao.nome AS 'area', instituicao_area.nome AS 'setor', os_status.status AS 'os_status', os_posicionamento.status AS 'os_posicionamento',os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome AS 'equipamento', equipamento_familia.modelo,os.assessment FROM os LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN os_prioridade on os_prioridade.id = os.id_prioridade LEFT JOIN os_sla on os_sla.id = os.id_sla LEFT JOIN os_grupo_sla on os_grupo_sla.id = os_sla.os_grupo_sla LEFT JOIN custom_service on custom_service.id = os.id_servico LEFT JOIN category on category.id = os.id_category LEFT JOIN regdate_os_dropzone ON regdate_os_dropzone.id_os = os.id WHERE os.id_status like '6' GROUP BY os.id order by os.id DESC ";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
