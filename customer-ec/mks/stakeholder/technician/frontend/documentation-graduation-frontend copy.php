<?php
    include("../../database/database.php");
    
    session_start();
    $setor = $_SESSION['setor'];
    
    $query = "SELECT documentation_graduation.id, documentation_graduation.dropzone, documentation_graduation.titulo, documentation_graduation.ativo, documentation_graduation.upgrade, documentation_graduation.reg_date, category.nome, equipamento_familia.nome AS 'equipamento', equipamento_familia.fabricante, equipamento_familia.modelo FROM documentation_graduation LEFT JOIN category ON category.id = documentation_graduation.id_categoria LEFT JOIN equipamento_familia ON equipamento_familia.id = documentation_graduation.id_equipamento_familia WHERE documentation_graduation.ativo = 0";
    $videosByCategory = array();
    
    if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($id, $dropzone, $titulo, $ativo, $upgrade, $reg_date, $category, $equipamento, $fabricante, $modelo);
        
        while ($stmt->fetch()) {
            $videosByCategory[$category][$equipamento][] = $dropzone;
        }
        $stmt->close();
    }
?>
<input  class="form-control" type="hidden"  id="dropzone" name="dropzone"  value="<?php printf($dropzone); ?>" >
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Educação Continuada</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                    <div class="input-group">
                        <span class="input-group-btn">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #video-container {
                width: 640px; /* Largura do vídeo em destaque */
                float: left;
            }
            
            #playlist-container {
                width: 300px; /* Largura da lista de reprodução */
                float: right;
            }
            
            #playlist {
                list-style-type: none;
                padding: 0;
            }
            
            #playlist li {
                margin-bottom: 10px;
            }
            
            #ytplayer {
                width: 100%;
                height: 360px;
            }
        </style>
        <div id="video-container">
            <div id="ytplayer"></div>
        </div>
        
        <div id="playlist-container">
            <ul id="playlist">
                <?php
                    foreach ($videosByCategory as $category => $equipamentos) {
                        foreach ($equipamentos as $equipamento => $videos) {
                            echo "<li>";
                            echo "<h4>$category - $equipamento</h4>";
                            echo "<ul>";
                            foreach ($videos as $video) {
                                echo "<li><a href='javascript:void(0);' onclick='loadVideo(\"$video\");'>Click Aqui| $video</a></li>";
                            }
                            echo "</ul>";
                            echo "</li>";
                        }
                    }
                ?>
            </ul>
        </div>
        
        <script>
            var player;
            function loadVideo(videoId) {
                player.loadVideoById(videoId);
            }
        </script>
    </div>
</div>

<script src="https://www.youtube.com/iframe_api"></script>

<script>
    var video = document.getElementById('dropzone').value;

    function onYouTubeIframeAPIReady() {
        player = new YT.Player('ytplayer', {
            height: '360',
            width: '640',
            videoId: video // Você pode definir um vídeo padrão aqui se desejar
        });
    }
    
    // Start the YouTube API
    if (typeof YT !== 'undefined') {
        onYouTubeIframeAPIReady();
    }
</script>
