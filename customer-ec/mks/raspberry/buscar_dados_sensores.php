<?php
	include("database/database.php");	
	
	date_default_timezone_set('America/Sao_Paulo');
	$year=date("Y");
	$today=date("Y-m-d");
	$mes=date("m");
	
	 	$status = "1,2,3"; // Status 1, 2 e 3
	
	// Inicializa os arrays para cada categoria
	$abertos = array_fill(0, 12, 0);
	$fechados = array_fill(0, 12, 0);
	$lacrados = array_fill(0, 12, 0);
	
	$stmt = $conn->prepare("
	SELECT
		MONTH(reg_date) AS mes,
		COUNT(id) AS total,
		id_alarm_status
	FROM
		reg_date_alarm
	WHERE
		id_alarm_status IN (1,2,3)
		AND YEAR(reg_date) = 2023
	GROUP BY
		MONTH(reg_date),
		id_alarm_status
	ORDER BY
		MONTH(reg_date),
		id_alarm_status
");
	
 
	$stmt->execute();
	$stmt->bind_result($mes, $total, $id_alarm_status);
	
	while ($stmt->fetch()) {
		// Atualiza os arrays de acordo com o status
		// Após recuperar os valores do banco de dados
	
		
		
		
		
		switch ($id_alarm_status) {
			case 1:
				$abertos[$mes - 1] = $total;
				 
				break;
			case 2:
				$fechados[$mes - 1] = $total;
			 
				break;
			case 3:
				$lacrados[$mes - 1] = $total;
				 

				break;
		}
	}
	
	$stmt->close();
	
	// Constrói o objeto data
	$data = [
		'labels' => ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		'abertos' => $abertos,
		'fechados' => $fechados,
		'lacrados' => $lacrados
	];
	
	// Converte para JSON (se necessário)
	$data_json = json_encode($data);
	
	echo "<script>";
	echo "var dataFromPHP = $data_json;";
	echo "</script>";

?>
<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row" style="display: inline-block;">
    <div class="tile_count">

			 <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-check red"></i>
        Bem vindo ao
        <strong class="black">
          AMON
        
        </strong>,
     Abertura e Monitoramento de Ocorrências Notificadas<a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas Biomédicos</a>.
      </div>
      <?php
        $stmt = $conn->prepare(" SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor ORDER BY notif.data DESC LIMIT 1");
        $stmt->execute();
        $stmt->bind_result($title, $notif_msg, $data);
        while ($stmt->fetch()) {};
      ?>
      <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-bell"></i>
        Última
        <strong class="black">Notificação</strong>
        <strong class="black"><?php echo $title; ?></strong> : <small><?php echo $notif_msg; ?> : <?php echo $data; ?></small>
      </div>

			
		</div>
		
		<div class="clearfix"></div>

		<div class="animated flipInY col-lg-2 col-md-2 col-sm-2  ">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-unlock"></i>
				</div>
				<div class="count"><?php
					$stmt = $conn->prepare(" SELECT COUNT(id) FROM car where id_status != 3 and trash = 1 and ativo = 0 ");
					$stmt->execute();
					$stmt->bind_result($result);
					while ( $stmt->fetch()) {
						printf($result);
					}
				?></div>
				<h3>Aberto</h3>
				<p>Carrinho de Emergência Aberto</p>
			</div>
		</div>
		<div class="animated flipInY col-lg-2 col-md-2 col-sm-2  ">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-lock"></i>
				</div>
				<div class="count"><?php
					$stmt = $conn->prepare(" SELECT COUNT(id) FROM car where id_status = 3 and trash = 1 and ativo = 0 ");
					$stmt->execute();
					$stmt->bind_result($result);
					while ( $stmt->fetch()) {
						printf($result);
					}
				?> </div>
				<h3>Fechado</h3>
				<p>Carrinho de Emergência Fechado</p>

			</div>
		</div>
		<div class="animated flipInY col-lg-2 col-md-2 col-sm-2  ">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-tags"></i>
				</div>
				<div class="count"><?php
					$stmt = $conn->prepare(" SELECT COUNT(id) FROM car where trash = 1 and ativo = 0 ");
					$stmt->execute();
					$stmt->bind_result($result);
					while ( $stmt->fetch()) {
						printf($result);
					}
				?></div>
				<h3>Total</h3>
				<p>Total Carrinho de Emergência</p>

			</div>
		</div>
		<div class="animated flipInY col-lg-2 col-md-2 col-sm-2  ">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-toggle-on"></i>
				</div>

				<div class="count"><?php
					$stmt = $conn->prepare("SELECT count(id) FROM device
						WHERE ativo = 0 and  (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(status_time)) < 60; ");
					$stmt->execute();
					$stmt->bind_result($result);
					while ($stmt->fetch()) {
						printf($result);
					}
				?></div>
				<h3>Online</h3>
				<p>Total Dispositivo Online</p>
				
			</div>
		</div>
		<div class="animated flipInY col-lg-2 col-md-2 col-sm-2  ">

			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-toggle-off"></i>
				</div>
				<div class="count"><?php
					$stmt = $conn->prepare("SELECT count(id) FROM device
						WHERE ativo = 0 and (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(status_time)) > 60;");
					$stmt->execute();
					$stmt->bind_result($result);
					while ($stmt->fetch()) {
						printf($result);
					}
				?></div>
				<h3>Offline</h3>
				<p>Total Dispositivo Offline</p>

			</div>
		</div>
		
		
		<div class="clearfix"></div>
		
<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					Eventos
					<small>Registro de Eventos</small>
				</h2>
			
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				 						<!-- Adicione aqui os links para bibliotecas como Chart.js, etc. -->
					
				
						<div class="container">
 							<div class="chart-container">
								<canvas id="myChart"></canvas>
							</div>
						</div>
						
					<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dataFromPHP.labels,
            datasets: [
                {
                    label: 'Abertos',
                    data: dataFromPHP.abertos,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1,
                    fill: true // Preenchimento do gráfico
                },
                {
                    label: 'Fechados',
                    data: dataFromPHP.fechados,
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    fill: true // Preenchimento do gráfico
                },
                {
                    label: 'Lacrados',
                    data: dataFromPHP.lacrados,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1,
                    fill: true // Preenchimento do gráfico
                }
            ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        color: 'rgba(255, 255, 255, 0.7)' // Cor dos números do eixo y
                    },
                    grid: {
                        color: 'rgba(255, 255, 255, 0.3)' // Cor da grade do eixo y
                    }
                },
                x: {
                    ticks: {
                        color: 'rgba(255, 255, 255, 0.7)' // Cor dos números do eixo x
                    },
                    grid: {
                        color: 'rgba(255, 255, 255, 0.3)' // Cor da grade do eixo x
                    }
                }
            },
            plugins: {
                legend: {
                    labels: {
                        color: 'rgba(255, 255, 255, 0.7)' // Cor das labels da legenda
                    }
                }
            }
        }
    });
</script>

				
			
			</div>
		</div>
	</div>
</div>
		
		
<div class="row">
	<div class="col-md-4">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					Alarme 
					<small>Aberto</small>
				</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							<i class="fa fa-wrench"></i>
						</a>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Settings 1</a>
							<a class="dropdown-item" href="#">Settings 2</a>
						</div>
					</li>
					<li>
						<a class="close-link">
							<i class="fa fa-close"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<?php
					$stmt = $conn->prepare(" SELECT reg_date_alarm.macadress,day(reg_date_alarm.reg_date) as 'day',month(reg_date_alarm.reg_date) as 'mes',alarm_status.alarm,car.nome,car.codigo,unidade.nome as 'unidade',unidade_setor.nome as 'setor',unidade_area.nome as 'area' FROM reg_date_alarm LEFT JOIN alarm_status ON alarm_status.id = reg_date_alarm.id_alarm_status LEFT JOIN device ON device.macadress = reg_date_alarm.macadress LEFT JOIN car ON car.id = device.id_car LEFT JOIN unidade_area ON unidade_area.id = car.id_unidade_area LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade where reg_date_alarm.id_alarm_status = 1 ORDER by reg_date_alarm.id DESC limit 5");
					$stmt->execute();
					$stmt->bind_result($mac,$day,$mes,$alarm,$nome,$codigo,$unidade,$setor,$area);
					while ( $stmt->fetch()) {
						
				?> 
				<article class="media event">
					<a class="pull-left date">
						<p class="month"><?php printf($mes); ?></p>
						<p class="day"><?php printf($day); ?></p>
					</a>
					<div class="media-body">
						<a class="title" href="#"><?php printf($mac); ?> - <?php printf($nome); ?> - <?php printf($codigo); ?></a>
						<p><?php printf($unidade); ?> - <?php printf($setor); ?> - <?php printf($area); ?></p>
					</div>
				</article>
			
			 <?php  } ?>
			 
			 
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					Alarme
					<small>Fechado</small>
				</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							<i class="fa fa-wrench"></i>
						</a>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Settings 1</a>
							<a class="dropdown-item" href="#">Settings 2</a>
						</div>
					</li>
					<li>
						<a class="close-link">
							<i class="fa fa-close"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<?php
					$stmt = $conn->prepare(" SELECT reg_date_alarm.macadress,day(reg_date_alarm.reg_date) as 'day',month(reg_date_alarm.reg_date) as 'mes',alarm_status.alarm,car.nome,car.codigo,unidade.nome as 'unidade',unidade_setor.nome as 'setor',unidade_area.nome as 'area' FROM reg_date_alarm LEFT JOIN alarm_status ON alarm_status.id = reg_date_alarm.id_alarm_status LEFT JOIN device ON device.macadress = reg_date_alarm.macadress LEFT JOIN car ON car.id = device.id_car LEFT JOIN unidade_area ON unidade_area.id = car.id_unidade_area LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade where reg_date_alarm.id_alarm_status = 2 ORDER by reg_date_alarm.id DESC limit 5");
					$stmt->execute();
					$stmt->bind_result($mac,$day,$mes,$alarm,$nome,$codigo,$unidade,$setor,$area);
					while ( $stmt->fetch()) {
						
				?> 
				<article class="media event">
					<a class="pull-left date">
						<p class="month"><?php printf($mes); ?></p>
						<p class="day"><?php printf($day); ?></p>
					</a>
					<div class="media-body">
						<a class="title" href="#"><?php printf($mac); ?> - <?php printf($nome); ?> - <?php printf($codigo); ?></a>
						<p><?php printf($unidade); ?> - <?php printf($setor); ?> - <?php printf($area); ?></p>
					</div>
				</article>
				
				<?php  } ?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					Dispositivos
					<small>OFFLine</small>
				</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							<i class="fa fa-wrench"></i>
						</a>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Settings 1</a>
							<a class="dropdown-item" href="#">Settings 2</a>
						</div>
					</li>
					<li>
						<a class="close-link">
							<i class="fa fa-close"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<?php
					$stmt = $conn->prepare(" SELECT reg_date_alarm.macadress,day(device.status_time) as 'day',month(device.status_time) as 'mes',alarm_status.alarm,car.nome,car.codigo,unidade.nome as 'unidade',unidade_setor.nome as 'setor',unidade_area.nome as 'area' FROM reg_date_alarm LEFT JOIN alarm_status ON alarm_status.id = reg_date_alarm.id_alarm_status LEFT JOIN device ON device.macadress = reg_date_alarm.macadress LEFT JOIN car ON car.id = device.id_car LEFT JOIN unidade_area ON unidade_area.id = car.id_unidade_area LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade WHERE device.ativo = 0 and (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(status_time)) > 60 group by device.id");
					$stmt->execute();
					$stmt->bind_result($mac,$day,$mes,$alarm,$nome,$codigo,$unidade,$setor,$area);
					while ( $stmt->fetch()) {
						
				?> 
				<article class="media event">
					<a class="pull-left date">
						<p class="month"><?php printf($mes); ?></p>
						<p class="day"><?php printf($day); ?></p>
					</a>
					<div class="media-body">
						<a class="title" href="#"><?php printf($mac); ?> - <?php printf($nome); ?> - <?php printf($codigo); ?></a>
						<p><?php printf($unidade); ?> - <?php printf($setor); ?> - <?php printf($area); ?></p>
					</div>
				</article>
				
				<?php  } ?>
			</div>
		</div>
	</div>
</div>


	</div>
</div>

<?php
  date_default_timezone_set('America/Sao_Paulo');
  $today = date("Y-m-d");
  $read_open = 1;
  
  $query = "SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1";
  
  $resultados = $conn->query($query);
  $notifications = array();
  
  while ($row = mysqli_fetch_assoc($resultados)) {
    $notifications[] = $row;
  }
  
?>

<script>
   if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
        // Após o registro do Service Worker, mostrar notificações
        <?php foreach ($notifications as $notification): ?>
        var title = "<?php echo $notification['nome']; ?>";
        var options = {
          body: "<?php echo $notification['dados']; ?>"
          // Adicione mais opções de notificação conforme necessário
        };
        showNotification(title, options);
        <?php endforeach; ?>
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  function showNotification(title, options) {
    if (Notification.permission === "granted") {
      var notification = new Notification(title, options);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification(title, options);
        }
      });
    }
  }
</script>
 
<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>