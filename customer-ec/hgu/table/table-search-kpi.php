<?php
include("../database/database.php");
$query = "SELECT kpi.id,kpi.name, kpi.typology, kpi.meta,kpi.value  FROM kpi";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
