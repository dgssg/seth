<?php
include("../database/database.php");
$query = "SELECT maintenance_procedures.id, maintenance_procedures.name,category.nome,maintenance_procedures.codigo,maintenance_procedures.reg_date,maintenance_procedures.upgrade FROM maintenance_procedures INNER JOIN category ON category.id = maintenance_procedures.id_category WHERE maintenance_procedures.trash = 1 order by maintenance_procedures.id DESC ";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
