<?php
// Configurações do banco de dados
$host = 'localhost';
$user = 'cvheal47_root';
$password = 'cvheal47_root';
$database = 'cvheal47_seth_hgu';

// Diretório para salvar o backup
$backupDir = '../backup/';
    // Obtém o nome do dia da semana (em inglês)
    $dayOfWeek = date('l'); // Outputs: Monday, Tuesday, etc.
    
    // Nome do arquivo de backup baseado no dia da semana
    $backupFile = $backupDir . $database . '_' . $dayOfWeek . '.sql';
    
    // Comando mysqldump para backup completo
    $command = "mysqldump --host=$host --user=$user --password=$password --databases $database --routines --events --triggers > $backupFile";
    
    // Executa o comando
    exec($command, $output, $result);
    
    // Verifica se o backup foi bem-sucedido
    if ($result === 0) {
        echo "Backup de $dayOfWeek realizado com sucesso: $backupFile";
    } else {
        echo "Erro ao realizar o backup.";
    }
?>