<?php
include("database/database.php");
$query = "SELECT purchases.id, purchases.id_os, purchases.solicitacao, purchases.data_purchases, purchases.upgrade, fornecedor.empresa FROM purchases INNER JOIN fornecedor on fornecedor.id = purchases.id_fornecedor WHERE purchases.id_status_purchases like '4' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $os,$solicitacao,$data,$upgrade,$fornecedor);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
 


?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Laudo</th>
                          <th>Equipamento</th>
                          <th>Realização</th>
                          <th>Validade</th>
                           <th>Status</th>
                          
                         
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($os); ?> </td>
                          <td><?php printf($solicitacao); ?></td>
                            <td><?php printf($data); ?></td>
                            <td><?php printf($upgrade); ?> </td>
                          <td><?php printf($fornecedor); ?></td>
                          
                      
                          <td>  
                   <a class="btn btn-app"  href="purchases-progress-edit?purchases=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                  
           <a  class="btn btn-app" href="purchases-viewer?purchases=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>