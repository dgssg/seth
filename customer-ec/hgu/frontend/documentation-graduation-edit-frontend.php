<?php
include("database/database.php");
$codigoget = ($_GET["id"]);
$query = "SELECT documentation_graduation.assessment,documentation_graduation.id,documentation_graduation.titulo,documentation_graduation.ativo,documentation_graduation.upgrade,documentation_graduation.reg_date,category.id,equipamento_familia.id,documentation_graduation.dropzone  FROM documentation_graduation  LEFT JOIN category ON category.id = documentation_graduation.id_categoria LEFT JOIN equipamento_familia ON equipamento_familia.id = documentation_graduation.id_equipamento_familia WHERE documentation_graduation.id = $codigoget";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($assessment,$id,$titulo,$ativo,$upgrade,$reg_date,$category,$equipamento_familia,$dropzone);


   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
  
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }

  
function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
  $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
  $con = 1;
  $var = '';
  while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
  }else{
      $var = $var.$n.$res[$n];
      $con++;
      }
  }
      $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
 // echo chaveAlfaNumerica(5);
//   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = md5($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 3
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function v4() {
return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

// 32 bits for "time_low"
mt_rand(0, 0xffff), mt_rand(0, 0xffff),

// 16 bits for "time_mid"
mt_rand(0, 0xffff),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand(0, 0x0fff) | 0x4000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand(0, 0x3fff) | 0x8000,

// 48 bits for "node"
mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
);
}

public static function v5($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = sha1($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function is_valid($uuid) {
return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();

//echo $v4uuid; //chave da assinatura
?>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>Educação Continuada</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Educação <small>Continuada</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                 <form action="backend/documentation-graduation-edit-backend.php?id=<?php printf($codigoget);?>" method="post">

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($codigoget); ?> " >
                        </div>
                      </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input  class="form-control" type="text"  id="titulo" name="titulo"  value="<?php printf($titulo); ?>" >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Link</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input  class="form-control" type="text"  id="dropzone" name="dropzone"  value="<?php printf($dropzone); ?>" >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Visualização</label>
                      <div class="col-md-6 col-sm-6 ">
                        <div class="">
                          <label>
                            <input name="ativo"type="checkbox" class="js-switch" <?php if($ativo == "0"){printf("checked"); }?> />
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align">Avaliação</label>
                      <div class="col-md-6 col-sm-6 ">
                        <div class="">
                          <label>
                            <input name="assessment"type="checkbox" class="js-switch" <?php if($assessment == "0"){printf("checked"); }?> />
                          </label>
                        </div>
                      </div>
                    </div>

                      <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Categoria <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="id_categoria" id="id_categoria"   >
        <option value="">Selecione uma Categoria</option>
        <?php



$sql = "SELECT  id, nome FROM category WHERE trash = 1";


if ($stmt = $conn->prepare($sql)) {
$stmt->execute();
$stmt->bind_result($id,$nome);
while ($stmt->fetch()) {
?>

<option value="<?php printf($id);?>	" <?php if($id == $category){printf("selected");};?>><?php printf($nome);?> 	</option>

 <?php
// tira o resultado da busca da memória
}

                     }
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Categoria "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_categoria').select2();
  });
  </script>
                  
                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Familia Equipamento <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-right" name="id_equipamento_familia" id="id_equipamento_familia"   >
                          <option value="">Selecione uma Familia</option>
                          <?php
                            
                            
                            
                            $sql = "SELECT  id, nome,fabricante,modelo FROM equipamento_familia WHERE trash = 1";
                            
                            
                            if ($stmt = $conn->prepare($sql)) {
                              $stmt->execute();
                              $stmt->bind_result($id,$nome,$fabricante,$modelo);
                              while ($stmt->fetch()) {
                          ?>
                          
                          <option value="<?php printf($id);?>	" <?php if($id == $equipamento_familia){printf("selected");};?>><?php printf($nome);?>  - <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
                          
                          <?php
                            // tira o resultado da busca da memória
                            }
                            
                            }
                            $stmt->close();
                            
                          ?>
                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Categoria "></span>
                        
                        
                      </div>
                    </div>
                    
                    <script>
                      $(document).ready(function() {
                        $('#id_equipamento_familia').select2();
                      });
                    </script>    
                    
                  
                   
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
                                     title: 'Registrado',
                                     text: 'Informações registrada!',
                                     type: 'success',
                                     styling: 'bootstrap3'
                                 });">Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                                  </form>



                  </div>
                </div>
              </div>
	       </div>


    
            <div class="clearfix"></div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Video</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div id="ytplayer"></div>
              
              <script>
                var video = document.getElementById('dropzone').value;
                
                // Load the IFrame Player API code asynchronously.
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/player_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                
                // Replace the 'ytplayer' element with an <iframe> and
                // YouTube player after the API code downloads.
                var player;
                function onYouTubePlayerAPIReady() {
                  player = new YT.Player('ytplayer', {
                    height: '360',
                    width: '640',
                    videoId: video
                  });
                }
              </script>
         
              

                              
              </div>
            </div>
    
                     

<div class="clearfix"></div>
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="documentation-graduation">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>

           