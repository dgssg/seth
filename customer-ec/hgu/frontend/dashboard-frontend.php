
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row" style="display: inline-block;" >
    <div class="tile_count">

  
      <div class="alert alert-block alert-success">
        
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>

        <i class="ace-icon fa fa-check red"></i>

        Bem vindo ao
        <strong class="black">
          SETH
          <small>Engenharia Clínica</small>
        </strong>,
        Sistema de Engenharia Técnica Hospitalar <a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas Biomédicos</a> .
      </div>
      
      <div class="alert alert-block alert-danger">
        
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        
        <i class="ace-icon fa fa-bell"></i>
        
        Ultima 
        <strong class="black">
          Notificação
           
        </strong> |
        <?php
          $stmt = $conn->prepare(" SELECT origem, ref,data FROM notif ORDER BY data DESC LIMIT 1");
          $stmt->execute();
          $stmt->bind_result($title,$notif_msg,$data);
          while ( $stmt->fetch()) {
           
          };

        ?>
        <strong class="black">  <?php printf($title); ?>  </strong> : <small><?php printf($notif_msg); ?> : <?php printf($data); ?></small>

      </div>



<script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-core.min.js"></script>
<script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-radar.min.js"></script>

<?php
date_default_timezone_set('America/Sao_Paulo');
$ano=date("Y");
$today=date("Y-m-d");
$mes=date("m");
$yr = $ano;
$month = $mes;

$stmt = $conn->prepare("SELECT COUNT(os.id)
FROM os 
INNER JOIN os_sla ON os_sla.id = os.id_sla
WHERE os.id_status = 2
  AND DATE_ADD(os.sla_term, INTERVAL os_sla.sla_primeiro MINUTE) < os.sla_register;
");
$stmt->execute();
$stmt->bind_result($sla_vermelho);
while ( $stmt->fetch()) {
  $sla_vermelho=$sla_vermelho;
};
$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 2 AND sla_term > sla_register ");
$stmt->execute();
$stmt->bind_result($sla_verde);
while ( $stmt->fetch()) {
  $sla_verde=$sla_verde;
};
$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 2 AND sla_term < '$today' AND  sla_term > '$today' ");
$stmt->execute();
$stmt->bind_result($sla_amarelo);
while ( $stmt->fetch()) {
  $sla_amarelo=$sla_amarelo;
};
$stmt = $conn->prepare(" SELECT COUNT(id) FROM os  ");
$stmt->execute();
$stmt->bind_result($os_total);
while ( $stmt->fetch()) {
  $os_total=$os_total;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 1 ");
$stmt->execute();
$stmt->bind_result($os_aberto);
while ( $stmt->fetch()) {
  $os_aberto=$os_aberto;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 2 ");
$stmt->execute();
$stmt->bind_result($os_ANDamento);
while ( $stmt->fetch()) {
  $os_ANDamento=$os_ANDamento;
};
$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 3 ");
$stmt->execute();
$stmt->bind_result($os_fechado);
while ( $stmt->fetch()) {
  $os_fechada=$os_fechada;
};
$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 4 ");
$stmt->execute();
$stmt->bind_result($os_cancelado);
while ( $stmt->fetch()) {
  $os_cancelado=$os_cancelado;
};
$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 5 ");
$stmt->execute();
$stmt->bind_result($os_assinatura);
while ( $stmt->fetch()) {
  $os_assinatura=$os_assinatura;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 6 ");
$stmt->execute();
$stmt->bind_result($os_concluido);
while ( $stmt->fetch()) {
  $os_concluido=$os_concluido;
}

$stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive ");
$stmt->execute();
$stmt->bind_result($mp_total);
while ( $stmt->fetch()) {
  $mp_total=$mp_total;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 1 ");
$stmt->execute();
$stmt->bind_result($mp_aberto);
while ( $stmt->fetch()) {
  $mp_aberto=$mp_aberto;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 2 ");
$stmt->execute();
$stmt->bind_result($mp_atrasada);
while ( $stmt->fetch()) {
  $mp_atrasada=$mp_atrasada;
};
$stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 3 ");
$stmt->execute();
$stmt->bind_result($mp_fechada);
while ( $stmt->fetch()) {
  $mp_fechada=$mp_fechada;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM calibration_preventive ");
$stmt->execute();
$stmt->bind_result($cal_total);
while ( $stmt->fetch()) {
  $cal_total=$cal_total;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM calibration_preventive WHERE id_status = 1 ");
$stmt->execute();
$stmt->bind_result($cal_aberto);
while ( $stmt->fetch()) {
  $cal_aberto=$cal_aberto;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM calibration_preventive WHERE id_status = 2 ");
$stmt->execute();
$stmt->bind_result($cal_atrasada);
while ( $stmt->fetch()) {
  $cal_atrasada=$cal_atrasada;
};

$stmt = $conn->prepare(" SELECT COUNT(id) FROM calibration_preventive WHERE id_status = 3 ");
$stmt->execute();
$stmt->bind_result($cal_fechada);
while ( $stmt->fetch()) {
  $cal_fechada=$cal_fechada;
};


$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-01%'");
$stmt->execute();
$stmt->bind_result($mp_jan);
while ( $stmt->fetch()) {
  $mp_jan=$mp_jan;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-01%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_jan);
while ( $stmt->fetch()) {
  $mp_aberto_jan=$mp_aberto_jan;
};
if ($mp_aberto_jan == 0) {
$mp_aberto_jan = 0.01;
}
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-01%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_jan);
while ( $stmt->fetch()) {
  $mp_atrasada_jan=$mp_atrasada_jan;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-01%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_jan);
while ( $stmt->fetch()) {
  $mp_fechada_jan=$mp_fechada_jan;
};

$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-02%'");
$stmt->execute();
$stmt->bind_result($mp_fev);
while ( $stmt->fetch()) {
  $mp_fev=$mp_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-02%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_fev);
while ( $stmt->fetch()) {
  $mp_aberto_fev=$mp_aberto_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-02%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_fev);
while ( $stmt->fetch()) {
  $mp_atrasada_fev=$mp_atrasada_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-02%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_fev);
while ( $stmt->fetch()) {
  $mp_fechada_fev=$mp_fechada_fev;
};

$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-03%'");
$stmt->execute();
$stmt->bind_result($mp_mar);
while ( $stmt->fetch()) {
  $mp_mar=$mp_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-03%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_mar);
while ( $stmt->fetch()) {
  $mp_aberto_mar=$mp_aberto_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-03%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_mar);
while ( $stmt->fetch()) {
  $mp_atrasada_mar=$mp_atrasada_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-03%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_mar);
while ( $stmt->fetch()) {
  $mp_fechada_mar=$mp_fechada_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-04%'");
$stmt->execute();
$stmt->bind_result($mp_abr);
while ( $stmt->fetch()) {
  $mp_abr=$mp_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-04%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_abr);
while ( $stmt->fetch()) {
  $mp_aberto_abr=$mp_aberto_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-04%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_abr);
while ( $stmt->fetch()) {
  $mp_atrasada_abr=$mp_atrasada_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-04%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_abr);
while ( $stmt->fetch()) {
  $mp_fechada_abr=$mp_fechada_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-05%'");
$stmt->execute();
$stmt->bind_result($mp_mai);
while ( $stmt->fetch()) {
  $mp_mai=$mp_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-05%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_mai);
while ( $stmt->fetch()) {
  $mp_aberto_mai=$mp_aberto_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-05%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_mai);
while ( $stmt->fetch()) {
  $mp_atrasada_mai=$mp_atrasada_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-05%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_mai);
while ( $stmt->fetch()) {
  $mp_fechada_mai=$mp_fechada_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-06%'");
$stmt->execute();
$stmt->bind_result($mp_jun);
while ( $stmt->fetch()) {
  $mp_jun=$mp_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-06%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_jun);
while ( $stmt->fetch()) {
  $mp_aberto_jun=$mp_aberto_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-06%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_jun);
while ( $stmt->fetch()) {
  $mp_atrasada_jun=$mp_atrasada_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-06%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_jun);
while ( $stmt->fetch()) {
  $mp_fechada_jun=$mp_fechada_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-07%'");
$stmt->execute();
$stmt->bind_result($mp_jul);
while ( $stmt->fetch()) {
  $mp_jul=$mp_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-07%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_jul);
while ( $stmt->fetch()) {
  $mp_aberto_jul=$mp_aberto_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-07%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_jul);
while ( $stmt->fetch()) {
  $mp_atrasada_jul=$mp_atrasada_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-07%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_jul);
while ( $stmt->fetch()) {
  $mp_fechada_jul=$mp_fechada_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-08%'");
$stmt->execute();
$stmt->bind_result($mp_ago);
while ( $stmt->fetch()) {
  $mp_ago=$mp_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-08%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_ago);
while ( $stmt->fetch()) {
  $mp_aberto_ago=$mp_aberto_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-08%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_ago);
while ( $stmt->fetch()) {
  $mp_atrasada_ago=$mp_atrasada_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-08%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_ago);
while ( $stmt->fetch()) {
  $mp_fechada_ago=$mp_fechada_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-09%'");
$stmt->execute();
$stmt->bind_result($mp_set);
while ( $stmt->fetch()) {
  $mp_set=$mp_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-09%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_set);
while ( $stmt->fetch()) {
  $mp_aberto_set=$mp_aberto_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-09%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_set);
while ( $stmt->fetch()) {
  $mp_atrasada_set=$mp_atrasada_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-09%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_set);
while ( $stmt->fetch()) {
  $mp_fechada_set=$mp_fechada_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-10%'");
$stmt->execute();
$stmt->bind_result($mp_out);
while ( $stmt->fetch()) {
  $mp_out=$mp_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-10%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_out);
while ( $stmt->fetch()) {
  $mp_aberto_out=$mp_aberto_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-10%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_out);
while ( $stmt->fetch()) {
  $mp_atrasada_out=$mp_atrasada_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-10%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_out);
while ( $stmt->fetch()) {
  $mp_fechada_out=$mp_fechada_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-11%'");
$stmt->execute();
$stmt->bind_result($mp_nov);
while ( $stmt->fetch()) {
  $mp_nov=$mp_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-11%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_nov);
while ( $stmt->fetch()) {
  $mp_aberto_nov=$mp_aberto_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-11%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_nov);
while ( $stmt->fetch()) {
  $mp_atrasada_nov=$mp_atrasada_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-11%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_nov);
while ( $stmt->fetch()) {
  $mp_fechada_nov=$mp_fechada_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-12%'");
$stmt->execute();
$stmt->bind_result($mp_dez);
while ( $stmt->fetch()) {
  $mp_dez=$mp_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-12%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($mp_aberto_dez);
while ( $stmt->fetch()) {
  $mp_aberto_dez=$mp_aberto_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_start like '%$ano-12%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($mp_atrasada_dez);
while ( $stmt->fetch()) {
  $mp_atrasada_dez=$mp_atrasada_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM maintenance_preventive WHERE date_end like '%$ano-12%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($mp_fechada_dez);
while ( $stmt->fetch()) {
  $mp_fechada_dez=$mp_fechada_dez;
};

//Ordem de serviço


$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-01%'");
$stmt->execute();
$stmt->bind_result($os_jan);
while ( $stmt->fetch()) {
  $os_jan=$os_jan;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-01%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_jan);
while ( $stmt->fetch()) {
  $os_aberto_jan=$os_aberto_jan;
};
if ($os_aberto_jan == 0) {
$os_aberto_jan = 0.01;
}
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-01%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_jan);
while ( $stmt->fetch()) {
  $os_atrasada_jan=$os_atrasada_jan;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-01%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_jan);
while ( $stmt->fetch()) {
  $os_fechada_jan=$os_fechada_jan;
};

$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-02%'");
$stmt->execute();
$stmt->bind_result($os_fev);
while ( $stmt->fetch()) {
  $os_fev=$os_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-02%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_fev);
while ( $stmt->fetch()) {
  $os_aberto_fev=$os_aberto_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-02%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_fev);
while ( $stmt->fetch()) {
  $os_atrasada_fev=$os_atrasada_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-02%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_fev);
while ( $stmt->fetch()) {
  $os_fechada_fev=$os_fechada_fev;
};

$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-03%'");
$stmt->execute();
$stmt->bind_result($os_mar);
while ( $stmt->fetch()) {
  $os_mar=$os_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-03%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_mar);
while ( $stmt->fetch()) {
  $os_aberto_mar=$os_aberto_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-03%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_mar);
while ( $stmt->fetch()) {
  $os_atrasada_mar=$os_atrasada_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-03%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_mar);
while ( $stmt->fetch()) {
  $os_fechada_mar=$os_fechada_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-04%'");
$stmt->execute();
$stmt->bind_result($os_abr);
while ( $stmt->fetch()) {
  $os_abr=$os_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-04%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_abr);
while ( $stmt->fetch()) {
  $os_aberto_abr=$os_aberto_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-04%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_abr);
while ( $stmt->fetch()) {
  $os_atrasada_abr=$os_atrasada_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-04%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_abr);
while ( $stmt->fetch()) {
  $os_fechada_abr=$os_fechada_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-05%'");
$stmt->execute();
$stmt->bind_result($os_mai);
while ( $stmt->fetch()) {
  $os_mai=$os_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-05%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_mai);
while ( $stmt->fetch()) {
  $os_aberto_mai=$os_aberto_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-05%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_mai);
while ( $stmt->fetch()) {
  $os_atrasada_mai=$os_atrasada_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-05%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_mai);
while ( $stmt->fetch()) {
  $os_fechada_mai=$os_fechada_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-06%'");
$stmt->execute();
$stmt->bind_result($os_jun);
while ( $stmt->fetch()) {
  $os_jun=$os_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-06%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_jun);
while ( $stmt->fetch()) {
  $os_aberto_jun=$os_aberto_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-06%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_jun);
while ( $stmt->fetch()) {
  $os_atrasada_jun=$os_atrasada_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-06%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_jun);
while ( $stmt->fetch()) {
  $os_fechada_jun=$os_fechada_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-07%'");
$stmt->execute();
$stmt->bind_result($os_jul);
while ( $stmt->fetch()) {
  $os_jul=$os_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-07%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_jul);
while ( $stmt->fetch()) {
  $os_aberto_jul=$os_aberto_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-07%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_jul);
while ( $stmt->fetch()) {
  $os_atrasada_jul=$os_atrasada_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-07%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_jul);
while ( $stmt->fetch()) {
  $os_fechada_jul=$os_fechada_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-08%'");
$stmt->execute();
$stmt->bind_result($os_ago);
while ( $stmt->fetch()) {
  $os_ago=$os_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-08%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_ago);
while ( $stmt->fetch()) {
  $os_aberto_ago=$os_aberto_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-08%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_ago);
while ( $stmt->fetch()) {
  $os_atrasada_ago=$os_atrasada_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-08%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_ago);
while ( $stmt->fetch()) {
  $os_fechada_ago=$os_fechada_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-09%'");
$stmt->execute();
$stmt->bind_result($os_set);
while ( $stmt->fetch()) {
  $os_set=$os_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE date_start like '%$ano-09%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_set);
while ( $stmt->fetch()) {
  $os_aberto_set=$os_aberto_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-09%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_set);
while ( $stmt->fetch()) {
  $os_atrasada_set=$os_atrasada_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-09%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_set);
while ( $stmt->fetch()) {
  $os_fechada_set=$os_fechada_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-10%'");
$stmt->execute();
$stmt->bind_result($os_out);
while ( $stmt->fetch()) {
  $os_out=$os_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-10%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_out);
while ( $stmt->fetch()) {
  $os_aberto_out=$os_aberto_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-10%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_out);
while ( $stmt->fetch()) {
  $os_atrasada_out=$os_atrasada_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-10%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_out);
while ( $stmt->fetch()) {
  $os_fechada_out=$os_fechada_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-11%'");
$stmt->execute();
$stmt->bind_result($os_nov);
while ( $stmt->fetch()) {
  $os_nov=$os_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-11%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_nov);
while ( $stmt->fetch()) {
  $os_aberto_nov=$os_aberto_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-11%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_nov);
while ( $stmt->fetch()) {
  $os_atrasada_nov=$os_atrasada_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-11%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_nov);
while ( $stmt->fetch()) {
  $os_fechada_nov=$os_fechada_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-12%'");
$stmt->execute();
$stmt->bind_result($os_dez);
while ( $stmt->fetch()) {
  $os_dez=$os_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-12%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_dez);
while ( $stmt->fetch()) {
  $os_aberto_dez=$os_aberto_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-12%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_dez);
while ( $stmt->fetch()) {
  $os_atrasada_dez=$os_atrasada_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$ano-12%' AND id_status = 6");
$stmt->execute();
$stmt->bind_result($os_fechada_dez);
while ( $stmt->fetch()) {
  $os_fechada_dez=$os_fechada_dez;
};


$stmt = $conn->prepare("SELECT COUNT(os_defeito.id),os_defeito.defeito FROM os LEFT JOIN os_defeito ON os.id_defeito = os_defeito.id WHERE os.id_status = 3 OR os.id_status = 6 GROUP BY os_defeito.id");
$stmt->execute();
$stmt->bind_result($os_defeito,$defeito);
?>

<!--

inicio Calculo  regressão linear:
-->
<?php
for ($i=0; $i <= $mes ; $i++) {
  $v_mes=$v_mes+$i;
  if ($i==1) {
    $v_mp_fechada=($mp_fechada_jan*$mp_fechada_jan)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_jan*$mp_aberto_jan)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_jan+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_jan+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_jan*1)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_jan*1)+$v_mp_aberto_prod;
  }
  if ($i==2) {
    $v_mp_fechada=($mp_fechada_fev*$mp_fechada_fev)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_fev*$mp_aberto_fev)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_fev+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_fev+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_fev*2)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_fev*2)+$v_mp_aberto_prod;
  }
  if ($i==3) {
    $v_mp_fechada=($mp_fechada_mar*$mp_fechada_mar)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_mar*$mp_aberto_mar)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_mar+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_mar+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_mar*3)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_mar*3)+$v_mp_aberto_prod;
  }
  if ($i==4) {
    $v_mp_fechada=($mp_fechada_abr*$mp_fechada_abr)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_abr*$mp_aberto_abr)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_abr+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_abr+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_abr*4)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_abr*4)+$v_mp_aberto_prod;
  }
  if ($i==5) {
    $v_mp_fechada=($mp_fechada_mai*$mp_fechada_mai)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_mai*$mp_aberto_mai)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_mai+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_mai+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_mai*5)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_mai*5)+$v_mp_aberto_prod;
  }
  if ($i==6) {
    $v_mp_fechada=($mp_fechada_jun*$mp_fechada_jun)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_jun*$mp_aberto_jun)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_jun+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_jun+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_jun*6)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_jun*6)+$v_mp_aberto_prod;
  }
  if ($i==7) {
    $v_mp_fechada=($mp_fechada_jul*$mp_fechada_jul)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_jul*$mp_aberto_jul)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_jul+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_jul+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_jul*7)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_jul*7)+$v_mp_aberto_prod;
  }
  if ($i==8) {
    $v_mp_fechada=($mp_fechada_ago*$mp_fechada_ago)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_ago*$mp_aberto_ago)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_ago+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_ago+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_ago*8)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_ago*8)+$v_mp_aberto_prod;
  }
  if ($i==9) {
    $v_mp_fechada=($mp_fechada_set*$mp_fechada_set)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_set*$mp_aberto_set)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_set+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_set+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_set*9)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_set*9)+$v_mp_aberto_prod;
  }
  if ($i==10) {
    $v_mp_fechada=($mp_fechada_out*$mp_fechada_out)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_out*$mp_aberto_out)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_out+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_out+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_out*10)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_out*10)+$v_mp_aberto_prod;
  }
  if ($i==11) {
    $v_mp_fechada=($mp_fechada_nov*$mp_fechada_nov)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_nov*$mp_aberto_nov)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_nov+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_nov+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_nov*11)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_nov*11)+$v_mp_aberto_prod;
  }
  if ($i==12) {
    $v_mp_fechada=($mp_fechada_dez*$mp_fechada_dez)+$v_mp_fechada;
    $v_mp_aberto=($mp_aberto_dez*$mp_aberto_dez)+$v_mp_aberto;
    $v_mp_fechada_sum=$mp_fechada_dez+$v_mp_fechada_sum;
    $v_mp_aberto_sum=$mp_aberto_dez+$v_mp_aberto_sum;
    $v_mp_fechada_prod=($mp_fechada_dez*12)+$v_mp_fechada_prod;
    $v_mp_aberto_prod=($mp_aberto_dez*12)+$v_mp_aberto_prod;
  }

}

$a1_mp_fechada = $v_mp_fechada*$v_mes;
$a1_mp_aberto = $v_mp_aberto*$v_mes;
$a2_mp_fechada = $v_mp_fechada_sum*$v_mp_fechada_prod;
$a2_mp_aberto = $v_mp_aberto_sum*$v_mp_aberto_prod;
$a3_mp_fechada = $mes*$v_mp_fechada;
$a3_mp_aberto = $mes*$v_mp_aberto;
$a4_mp_fechada =  $v_mp_fechada_sum*$v_mp_fechada_sum;
$a4_mp_aberto =  $v_mp_aberto_sum*$v_mp_aberto_sum;

  if ($a3_mp_fechada-$a4_mp_fechada == 0) {
    $a_mp_fechado = 0;
  }
  else{
  $a_mp_fechado=(($a1_mp_fechada-$a2_mp_fechada)/($a3_mp_fechada-$a4_mp_fechada));    
  }



  if ($a3_mp_aberto-$a4_mp_aberto == 0) {
    $a_mp_aberto = 0;
  }
  else{
  $a_mp_aberto=(($a1_mp_aberto-$a2_mp_aberto)/($a3_mp_aberto-$a4_mp_aberto));
    
  }



$b1_mp_fechada= $mes*$v_mp_fechada_sum;
$b1_mp_aberto= $mes*$v_mp_aberto_sum;
$b2_mp_fechada=$v_mp_fechada_sum*$v_mes;
$b2_mp_aberto=$v_mp_aberto_sum*$v_mes;
$b3_mp_fechada=$mes*$v_mp_fechada_prod;
$b3_mp_aberto=$mes*$v_mp_aberto_prod;
$b4_mp_fechada =  $v_mp_fechada_sum*$v_mp_fechada_sum;
$b4_mp_aberto =  $v_mp_aberto_sum*$v_mp_aberto_sum;


  if ($b3_mp_fechada-$b4_mp_fechada == 0) {
    $b_mp_fechado = 0;
  }
  else{
  $b_mp_fechado=(($b1_mp_fechada-$b2_mp_fechada)/($b3_mp_fechada-$b4_mp_fechada));
    
  }




  if ($b3_mp_aberto-$b4_mp_aberto == 0) {
    $b_mp_aberto = 0;
  }
  else{
  $b_mp_aberto=(($b1_mp_aberto-$b2_mp_aberto)/($b3_mp_aberto-$b4_mp_aberto));
    
  }




  if ($b_mp_fechado == 0) {
    $c_fechada = 0;
  }
  else{
  $c_fechada=(($mes+1)-$a_mp_fechado)/$b_mp_fechado;
    
  }



  if ($b_mp_aberto== 0) {
    $c_aberto = 0;
  }
  else{
  $c_aberto=(($mes+1)-$a_mp_aberto)/$b_mp_aberto;
    
  }


$c_fechada=abs($c_fechada);
$c_aberto=abs($c_aberto);
?>
<?php
for ($i=0; $i <= $mes ; $i++) {
  $v_mes=$v_mes+$i;
  if ($i==1) {
    $v_os_fechada=($os_fechada_jan*$os_fechada_jan)+$v_os_fechada;
    $v_os_aberto=($os_aberto_jan*$os_aberto_jan)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_jan+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_jan+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_jan*1)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_jan*1)+$v_os_aberto_prod;
  }
  if ($i==2) {
    $v_os_fechada=($os_fechada_fev*$os_fechada_fev)+$v_os_fechada;
    $v_os_aberto=($os_aberto_fev*$os_aberto_fev)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_fev+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_fev+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_fev*2)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_fev*2)+$v_os_aberto_prod;
  }
  if ($i==3) {
    $v_os_fechada=($os_fechada_mar*$os_fechada_mar)+$v_os_fechada;
    $v_os_aberto=($os_aberto_mar*$os_aberto_mar)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_mar+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_mar+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_mar*3)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_mar*3)+$v_os_aberto_prod;
  }
  if ($i==4) {
    $v_os_fechada=($os_fechada_abr*$os_fechada_abr)+$v_os_fechada;
    $v_os_aberto=($os_aberto_abr*$os_aberto_abr)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_abr+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_abr+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_abr*4)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_abr*4)+$v_os_aberto_prod;
  }
  if ($i==5) {
    $v_os_fechada=($os_fechada_mai*$os_fechada_mai)+$v_os_fechada;
    $v_os_aberto=($os_aberto_mai*$os_aberto_mai)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_mai+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_mai+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_mai*5)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_mai*5)+$v_os_aberto_prod;
  }
  if ($i==6) {
    $v_os_fechada=($os_fechada_jun*$os_fechada_jun)+$v_os_fechada;
    $v_os_aberto=($os_aberto_jun*$os_aberto_jun)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_jun+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_jun+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_jun*6)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_jun*6)+$v_os_aberto_prod;
  }
  if ($i==7) {
    $v_os_fechada=($os_fechada_jul*$os_fechada_jul)+$v_os_fechada;
    $v_os_aberto=($os_aberto_jul*$os_aberto_jul)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_jul+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_jul+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_jul*7)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_jul*7)+$v_os_aberto_prod;
  }
  if ($i==8) {
    $v_os_fechada=($os_fechada_ago*$os_fechada_ago)+$v_os_fechada;
    $v_os_aberto=($os_aberto_ago*$os_aberto_ago)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_ago+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_ago+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_ago*8)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_ago*8)+$v_os_aberto_prod;
  }
  if ($i==9) {
    $v_os_fechada=($os_fechada_set*$os_fechada_set)+$v_os_fechada;
    $v_os_aberto=($os_aberto_set*$os_aberto_set)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_set+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_set+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_set*9)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_set*9)+$v_os_aberto_prod;
  }
  if ($i==10) {
    $v_os_fechada=($os_fechada_out*$os_fechada_out)+$v_os_fechada;
    $v_os_aberto=($os_aberto_out*$os_aberto_out)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_out+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_out+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_out*10)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_out*10)+$v_os_aberto_prod;
  }
  if ($i==11) {
    $v_os_fechada=($os_fechada_nov*$os_fechada_nov)+$v_os_fechada;
    $v_os_aberto=($os_aberto_nov*$os_aberto_nov)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_nov+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_nov+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_nov*11)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_nov*11)+$v_os_aberto_prod;
  }
  if ($i==12) {
    $v_os_fechada=($os_fechada_dez*$os_fechada_dez)+$v_os_fechada;
    $v_os_aberto=($os_aberto_dez*$os_aberto_dez)+$v_os_aberto;
    $v_os_fechada_sum=$os_fechada_dez+$v_os_fechada_sum;
    $v_os_aberto_sum=$os_aberto_dez+$v_os_aberto_sum;
    $v_os_fechada_prod=($os_fechada_dez*12)+$v_os_fechada_prod;
    $v_os_aberto_prod=($os_aberto_dez*12)+$v_os_aberto_prod;
  }

}

$a1_os_fechada = $v_os_fechada*$v_mes;
$a1_os_aberto = $v_os_aberto*$v_mes;
$a2_os_fechada = $v_os_fechada_sum*$v_os_fechada_prod;
$a2_os_aberto = $v_os_aberto_sum*$v_os_aberto_prod;
$a3_os_fechada = $mes*$v_os_fechada;
$a3_os_aberto = $mes*$v_os_aberto;
$a4_os_fechada =  $v_os_fechada_sum*$v_os_fechada_sum;
$a4_os_aberto =  $v_os_aberto_sum*$v_os_aberto_sum;

if ($a3_os_fechada - $a4_os_fechada == 0) {
  $a_os_fechado = 0;
}
  else {
  
    $a_os_fechado=(($a1_os_fechada-$a2_os_fechada)/($a3_os_fechada-$a4_os_fechada));
  }
  if ($a3_os_aberto - $a4_os_aberto == 0) {
    $a_os_aberto = 0;
  }
  else{
    $a_os_aberto=(($a1_os_aberto-$a2_os_aberto)/($a3_os_aberto-$a4_os_aberto));

  }

$b1_os_fechada= $mes*$v_os_fechada_sum;
$b1_os_aberto= $mes*$v_os_aberto_sum;
$b2_os_fechada=$v_os_fechada_sum*$v_mes;
$b2_os_aberto=$v_os_aberto_sum*$v_mes;
$b3_os_fechada=$mes*$v_os_fechada_prod;
$b3_os_aberto=$mes*$v_os_aberto_prod;
$b4_os_fechada =  $v_os_fechada_sum*$v_os_fechada_sum;
$b4_os_aberto =  $v_os_aberto_sum*$v_os_aberto_sum;

  if ($b3_os_fechada - $b4_os_fechada == 0) {
    $b_os_fechado = 0;
  }
  else{
  $b_os_fechado=(($b1_os_fechada-$b2_os_fechada)/($b3_os_fechada-$b4_os_fechada));
    
  }

  if ($b3_os_aberto-$b4_os_aberto == 0) {
    $b_os_aberto = 0;
  }
  else{
  $b_os_aberto=(($b1_os_aberto-$b2_os_aberto)/($b3_os_aberto-$b4_os_aberto));
    
  }

  if ($b_os_fechado == 0) {
    $osc_fechada = 0;
  }
  else{
  $osc_fechada=(($mes+1)-$a_os_fechado)/$b_os_fechado;
    
  }


  if ($b_os_aberto == 0) {
    $osc_aberto = 0;
  }
  else{
    $osc_aberto=(($mes+1)-$a_os_aberto)/$b_os_aberto;
    
  }
  

$osc_fechada=abs($c_fechada);
$osc_aberto=abs($c_aberto);
?>
<script>
anychart.onDocumentReady(function () {
  // our data FROM bulbapedia
  var data1 = [
    <?php
    while ( $stmt->fetch()) {
      echo '  {x: "'.$defeito.'", value: '.$os_defeito.'},';
    }
    ?>

  ];


  // create radar chart
  var chart = anychart.radar();

  // set chart yScale settings
  chart.yScale()
  .minimum(1)
  .maximum(65)
  .ticks({'interval':10});

  // color alternating cells
  // chart.yGrid().palette(["gray 0.1", "gray 0.2"]);

  // create first series
  // chart.line(data1)
  chart.area(data1).name('Ordem de serviço').markers(true).fill("#E55934", 0.3).stroke("#E55934")
  // create second series


  // set chart title
  chart.title("Causalidade da O.S")
  // set legend
  .legend(true);

  // set container id for the chart
  chart.container('container');
  // initiate chart drawing
  chart.draw();

});





</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {'packages':['table']});
google.charts.setOnLoadCallback(drawTable);

function drawTable() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Mês');
  data.addColumn('number', 'Programada');
  data.addColumn('number', 'Aberto');
  data.addColumn('number', 'Fechado');
  data.addColumn('number', 'Tendência ');
  data.addRows([
    ['Janeiro', <?php printf($mp_jan); ?>,    <?php printf(number_format($mp_aberto_jan, 1, '.', '')); ?> , <?php printf($mp_fechada_jan); ?>, <?php if ($mes == 1){ printf($c_aberto); }else {printf($mp_aberto_jan);} ?>],
    ['Fevereiro',<?php printf($mp_fev); ?> , <?php printf($mp_aberto_fev); ?> , <?php printf($mp_fechada_fev); ?>, <?php if ($mes == 2){ printf($c_aberto); }else {printf($mp_aberto_fev);} ?> ],
    ['Março',<?php printf($mp_mar); ?>,<?php printf($mp_aberto_mar); ?>,<?php printf($mp_fechada_mar); ?>, <?php if ($mes == 3){ printf($c_aberto); }else {printf($mp_aberto_mar);} ?> ],
    ['Abril',<?php printf($mp_abr); ?>,<?php printf($mp_aberto_abr); ?>, <?php printf($mp_fechada_abr); ?>, <?php if ($mes == 4){ printf($c_aberto); }else {printf($mp_aberto_abr);} ?>  ],
    ['Maio',<?php printf($mp_mai); ?>,<?php printf($mp_aberto_mai); ?>,<?php printf($mp_fechada_mai); ?>, <?php if ($mes == 5){ printf($c_aberto); }else {printf($mp_aberto_mai);} ?> ],
    ['Junho',<?php printf($mp_jun); ?>,<?php printf($mp_aberto_jun); ?>,<?php printf($mp_fechada_jun); ?>, <?php if ($mes == 6){ printf($c_aberto); }else {printf($mp_aberto_jun);} ?> ],
    ['Julho',<?php printf($mp_jul); ?>,<?php printf($mp_aberto_jul); ?>,<?php printf($mp_fechada_jul); ?>, <?php if ($mes == 7){ printf($c_aberto); }else {printf($mp_aberto_jul);} ?> ],
    ['Agosto',<?php printf($mp_ago); ?>,<?php printf($mp_aberto_ago); ?>,<?php printf($mp_fechada_ago); ?>, <?php if ($mes == 8){ printf($c_aberto); }else {printf($mp_aberto_ago);} ?> ],
    ['Setembro',<?php printf($mp_set); ?>,<?php printf($mp_aberto_set); ?>,<?php printf($mp_fechada_set); ?>, <?php if ($mes == 9){ printf($c_aberto); }else {printf($mp_aberto_set);} ?> ],
    ['Outubro',<?php printf($mp_out); ?>,<?php printf($mp_aberto_out); ?>, <?php printf($mp_fechada_out); ?>, <?php if ($mes == 10){ printf($c_aberto); }else {printf($mp_aberto_out);} ?>],
    ['Novembro',<?php printf($mp_nov); ?>,<?php printf($mp_aberto_nov); ?>,<?php printf($mp_fechada_nov); ?>, <?php if ($mes == 11){ printf($c_aberto); }else {printf($mp_aberto_nov);} ?> ],
    ['Dezembro',<?php printf($mp_dez); ?>,<?php printf($mp_aberto_dez); ?>,<?php printf($mp_fechada_dez); ?>, <?php if ($mes == 12){ printf($c_aberto); }else {printf($mp_aberto_dez);} ?> ]

  ]);
  var options = {
    title : 'Manutenção Preventiva'

  };
  var table = new google.visualization.Table(document.getElementById('table_div'));

  table.draw(data, {showRowNumber: true, width: '100%', height: '100%'},options);
}
</script>
<script type="text/javascript">
google.charts.load('current', {'packages':['table']});
google.charts.setOnLoadCallback(drawTable);

function drawTable() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Mês');
  data.addColumn('number', 'Solicitado');
  data.addColumn('number', 'Aberto');
  data.addColumn('number', 'Concluido');
  data.addColumn('number', 'Tendência ');
  data.addRows([
    ['Janeiro', <?php printf($os_jan); ?>,  <?php printf(number_format($os_aberto_jan, 1, '.', '')); ?>, <?php printf($os_fechada_jan); ?>, <?php if ($mes == 1){ printf($osc_aberto); }else {printf($os_aberto_jan);} ?>],
    ['Fevereiro',<?php printf($os_fev); ?> , <?php printf($os_aberto_fev); ?> , <?php printf($os_fechada_fev); ?>, <?php if ($mes == 2){ printf($osc_aberto); }else {printf($os_aberto_fev);} ?> ],
    ['Março',<?php printf($os_mar); ?>,<?php printf($os_aberto_mar); ?>,<?php printf($os_fechada_mar); ?>, <?php if ($mes == 3){ printf($osc_aberto); }else {printf($os_aberto_mar);} ?> ],
    ['Abril',<?php printf($os_abr); ?>,<?php printf($os_aberto_abr); ?>, <?php printf($os_fechada_abr); ?>, <?php if ($mes == 4){ printf($osc_aberto); }else {printf($os_aberto_abr);} ?>  ],
    ['Maio',<?php printf($os_mai); ?>,<?php printf($os_aberto_mai); ?>,<?php printf($os_fechada_mai); ?>, <?php if ($mes == 5){ printf($osc_aberto); }else {printf($os_aberto_mai);} ?> ],
    ['Junho',<?php printf($os_jun); ?>,<?php printf($os_aberto_jun); ?>,<?php printf($os_fechada_jun); ?>, <?php if ($mes == 6){ printf($osc_aberto); }else {printf($os_aberto_jun);} ?> ],
    ['Julho',<?php printf($os_jul); ?>,<?php printf($os_aberto_jul); ?>,<?php printf($os_fechada_jul); ?>, <?php if ($mes == 7){ printf($osc_aberto); }else {printf($os_aberto_jul);} ?> ],
    ['Agosto',<?php printf($os_ago); ?>,<?php printf($os_aberto_ago); ?>,<?php printf($os_fechada_ago); ?>, <?php if ($mes == 8){ printf($osc_aberto); }else {printf($os_aberto_ago);} ?> ],
    ['Setembro',<?php printf($os_set); ?>,<?php printf($os_aberto_set); ?>,<?php printf($os_fechada_set); ?>, <?php if ($mes == 9){ printf($osc_aberto); }else {printf($os_aberto_set);} ?> ],
    ['Outubro',<?php printf($os_out); ?>,<?php printf($os_aberto_out); ?>, <?php printf($os_fechada_out); ?>, <?php if ($mes == 10){ printf($osc_aberto); }else {printf($os_aberto_out);} ?>],
    ['Novembro',<?php printf($os_nov); ?>,<?php printf($os_aberto_nov); ?>,<?php printf($os_fechada_nov); ?>, <?php if ($mes == 11){ printf($osc_aberto); }else {printf($os_aberto_nov);} ?> ],
    ['Dezembro',<?php printf($os_dez); ?>,<?php printf($os_aberto_dez); ?>,<?php printf($os_fechada_dez); ?>, <?php if ($mes == 12){ printf($osc_aberto); }else {printf($os_aberto_dez);} ?> ]

  ]);
  var options = {
    title : 'Manutenção Preventiva'

  };
  var table = new google.visualization.Table(document.getElementById('table_div2'));

  table.draw(data, {showRowNumber: true, width: '100%', height: '100%'},options);
}
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Task', 'Hours per Day'],
    ['Aberto <?php printf($os_aberto); ?>',     <?php printf($os_aberto); ?>],
    ['ANDamento <?php printf($os_ANDamento); ?>',   <?php printf($os_ANDamento); ?>],
    ['Fechado <?php printf($os_fechado); ?>',   <?php printf($os_fechado); ?>],
    ['Cancelado <?php printf($os_cancelado); ?>',   <?php printf($os_cancelado); ?>],
    ['Assinatura <?php printf($os_assinatura); ?>',   <?php printf($os_assinatura); ?>],
    ['Concluido <?php printf($os_concluido); ?>',  <?php printf($os_concluido); ?>]

  ]);

  var options = {
    title: 'Ordem de serviço',
    pieHole: 0.4,
  };

  var chart = new google.visualization.PieChart(document.getElementById('mc'));
  chart.draw(data, options);
}
</script>
<script type="text/javascript">
google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Task', 'Hours per Day'],
    ['Aberta <?php printf($mp_aberto); ?>',     <?php printf($mp_aberto); ?>],
    ['Atrasada <?php printf($mp_atrasada); ?>',     <?php printf($mp_atrasada); ?>],
    ['Fechada <?php printf($mp_fechada); ?>',     <?php printf($mp_fechada); ?>]

  ]);

  var options = {
    title: 'Manutenção Preventiva',
    pieHole: 0.4,
  };

  var chart = new google.visualization.PieChart(document.getElementById('mp'));
  chart.draw(data, options);
}
</script>
<script type="text/javascript">
google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Task', 'Hours per Day'],
    ['Aberta <?php printf($cal_aberto); ?>',     <?php printf($cal_aberto); ?>],
    ['Atrasada <?php printf($cal_atrasada); ?>',     <?php printf($cal_atrasada); ?>],
    ['Fechada <?php printf($cal_fechada); ?>',     <?php printf($cal_fechada); ?>]
  ]);

  var options = {
    title: 'Calibração',
    pieHole: 0.4,
  };

  var chart = new google.visualization.PieChart(document.getElementById('cal'));
  chart.draw(data, options);
}
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 
 

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawVisualization);

function drawVisualization() {
  // Some raw data (not necessarily accurate)
  var data = google.visualization.arrayToDataTable([
    ['Month', 'Aberto', 'Atrasado', 'Fechado',  'Tendência'],
    ['2022/01',  165,      938,         522,                614.6],
    ['2022/02',  165,      938,         522,                614.6],
    ['2022/03',  165,      938,         522,                614.6],
    ['2022/04',  165,      938,         522,                614.6],
    ['2022/05',  165,      938,         522,                614.6],
    ['2022/06',  165,      938,         522,                614.6],
    ['2022/07',  165,      938,         522,                614.6],
    ['2022/08',  165,      938,         522,                614.6],
    ['2022/09',  165,      938,         522,                614.6],
    ['2022/10',  165,      938,         522,                614.6],
    ['2022/11',  165,      938,         522,                614.6],
    ['2008/12',  136,      691,         629,                  569.6]
  ]);

  var options = {
    title : 'Indicador de Calibração',
    vAxis: {title: 'Calibração'},
    hAxis: {title: 'Mês',
      slantedText: true, // Isso permite o texto inclinado no eixo
      slantedTextAngle: 45}, // Define o ângulo de inclinação},
    seriesType: 'bars',
    legend: { position: 'top', maxLines: 3 },
    series: {5: {type: 'line'}},
    colors: ['blue', 'red', 'orange', 'green'], // Defina as cores das barras aqui
    bar: {  groupWidth: '80%', // largura do grupo
      gapWidth: '25%'
    }, 
    bars: { 
      width: '100%', // largura das barras
      lineWidth: 1, 
      barSpacing: '25%', // espaço entre as barras
      groupSpacing: '300%' // espaço entre os grupos
    },
    annotations: {
      boxStyle: {
        // Color of the box outline.
        stroke: '#888',
        // Thickness of the box outline.
        strokeWidth: 1,
        // x-radius of the corner curvature.
        rx: 10,
        // y-radius of the corner curvature.
        ry: 10,
        // Attributes for linear gradient fill.
        gradient: {
          // Start color for gradient.
          color1: '#fbf6a7',
          // Finish color for gradient.
          color2: '#33b679',
          // WHERE on the boundary to start AND
          // end the color1/color2 gradient,
          // relative to the upper left corner
          // of the boundary.
          x1: '0%', y1: '0%',
          x2: '100%', y2: '100%',
          // If true, the boundary for x1,
          // y1, x2, AND y2 is the box. If
          // false, it's the entire chart.
          useObjectBoundingBoxUnits: true
        }
      }
    }
  };
  

  var chart = new google.visualization.ComboChart(document.getElementById('chart_div_2'));
  chart.draw(data, options);
}
</script>



<div class="col-md-2 col-sm-3  tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i>1 SLA Verde</span>
  <div class="count green"><?php printf($sla_verde); ?></div>

</div>
<div class="col-md-2 col-sm-3  tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i>1 SLA Amarelo</span>
  <div class="count yellow"><?php printf($sla_amarelo); ?></div>

</div>
<div class="col-md-2 col-sm-3  tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i>1 SLA Vermelho</span>
  <div class="count red"><?php printf($sla_vermelho); ?></div>

</div>
      <?php 
        $stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 1 "); 
        $stmt->execute();
        $stmt->bind_result($result);
        while ( $stmt->fetch()) { 
              }
      ?>
      <div class="col-md-2 col-sm-3  tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> O.S Solicitado</span>
        <div class="count"><?php printf($result); ?></div>
      </div>
      <?php 
        $stmt = $conn->prepare(" SELECT COUNT(id) FROM os WHERE id_status = 2  "); 
        $stmt->execute();
        $stmt->bind_result($result);
        while ( $stmt->fetch()) { 
              }
      ?>
      <div class="col-md-2 col-sm-3  tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>O.S ANDamento</span>
        <div class="count"><?php printf($result); ?></div>
      </div>
<div class="col-md-2 col-sm-3  tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i> Total O.S</span>
  <div class="count"><?php printf($os_total); ?></div>

</div>


      
                     
                          <?php 
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 1 "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                                                   }
                          ?>  

                         
                      <div class="col-md-2 col-sm-3  tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> M.P Aberto</span>
                        <div class="count"><?php printf($result); ?></div>
                      </div>
                        

                                            
                            <?php 
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 2  "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                                               }
                          ?>  

                 
                      <div class="col-md-2 col-sm-3  tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> M.P Atrasada</span>
                        <div class="count"><?php printf($result); ?></div>
                      </div>
                      <div class="col-md-2 col-sm-3  tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> Total M.P</span>
                        <div class="count"><?php printf($mp_total); ?></div>
                        
                      </div>
                      
                 
                          
                 
                       
                   
                    </div>
                           
                    </div>
                                            <!-- /top tiles -->
<?php


$filtro="";
$tnd="00:00:00";
$maintenance_preventive_count=0;
$date_start_cm_2="0000-00-01";
$status_1=0;
$status_2=0;
$status_3=0;
$status_4=0;
$status_5=0;
$status_6=0;
$jan_01=0;
$jan_02=0;
$jan_03=0;
$jan_04=0;
$jan_05=0;
$jan_06=0;


$years_query = [];
$months_query = [];

for ($i = 0; $i < 12; $i++) {
    $current_year = $yr;
    $current_month = $month - $i;

    if ($current_month <= 0) {
        $current_month += 12;
        $current_year -= 1;
    }
    
    $years_query[] = $current_year;
    $months_query[] = str_pad($current_month, 2, '0', STR_PAD_LEFT);
}



$query_jan_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) =  $months_query[11] AND  YEAR(maintenance_preventive.date_start) = $years_query[11] ";
$query_jan_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) =  $months_query[11] AND  YEAR(maintenance_preventive.date_start) = $years_query[11]";
$query_jan_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) =  $months_query[11] AND  YEAR(maintenance_preventive.date_start) = $years_query[11] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_jan_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) =  $months_query[11] AND  YEAR(maintenance_preventive.date_start) = $years_query[11]";
$query_jan_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) =  $months_query[11] AND  YEAR(maintenance_preventive.date_start) = $years_query[11]";
$query_jan_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) =  $months_query[11] AND  YEAR(maintenance_preventive.date_start) = $years_query[11]";

$query_fev_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) =  $months_query[10] AND  YEAR(maintenance_preventive.date_start) = $years_query[10] ";
$query_fev_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[10] AND  YEAR(maintenance_preventive.date_start) = $years_query[10]";
$query_fev_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[10] AND  YEAR(maintenance_preventive.date_start) = $years_query[10] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_fev_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[10] AND  YEAR(maintenance_preventive.date_start) = $years_query[10]";
$query_fev_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[10] AND  YEAR(maintenance_preventive.date_start) = $years_query[10]";
$query_fev_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[10] AND  YEAR(maintenance_preventive.date_start) = $years_query[10]";

$query_mar_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[9] AND  YEAR(maintenance_preventive.date_start) = $years_query[9] ";
$query_mar_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[9] AND  YEAR(maintenance_preventive.date_start) = $years_query[9]";
$query_mar_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[9] AND  YEAR(maintenance_preventive.date_start) = $years_query[9] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_mar_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[9] AND  YEAR(maintenance_preventive.date_start) = $years_query[9]";
$query_mar_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[9] AND  YEAR(maintenance_preventive.date_start) = $years_query[9]";
$query_mar_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[9] AND  YEAR(maintenance_preventive.date_start) = $years_query[9]";

$query_abr_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[8] AND  YEAR(maintenance_preventive.date_start) = $years_query[8] ";
$query_abr_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[8] AND  YEAR(maintenance_preventive.date_start) = $years_query[8]";
$query_abr_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[8] AND  YEAR(maintenance_preventive.date_start) = $years_query[8] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_abr_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[8] AND  YEAR(maintenance_preventive.date_start) = $years_query[8]";
$query_abr_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[8] AND  YEAR(maintenance_preventive.date_start) = $years_query[8]";
$query_abr_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[8] AND  YEAR(maintenance_preventive.date_start) = $years_query[8]";

$query_mai_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[7] AND  YEAR(maintenance_preventive.date_start) = $years_query[7] ";
$query_mai_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[7]  AND  YEAR(maintenance_preventive.date_start) = $years_query[7]";
$query_mai_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[7]  AND  YEAR(maintenance_preventive.date_start) = $years_query[7] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_mai_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[7]  AND  YEAR(maintenance_preventive.date_start) = $years_query[7]";
$query_mai_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[7]  AND  YEAR(maintenance_preventive.date_start) = $years_query[7]";
$query_mai_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[7]  AND  YEAR(maintenance_preventive.date_start) = $years_query[7]";

$query_jun_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[6]  AND  YEAR(maintenance_preventive.date_start) = $years_query[6] ";
$query_jun_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[6] AND  YEAR(maintenance_preventive.date_start) = $years_query[6]";
$query_jun_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[6] AND  YEAR(maintenance_preventive.date_start) = $years_query[6] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_jun_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[6] AND  YEAR(maintenance_preventive.date_start) = $years_query[6]";
$query_jun_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[6] AND  YEAR(maintenance_preventive.date_start) = $years_query[6]";
$query_jun_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[6] AND  YEAR(maintenance_preventive.date_start) = $years_query[6]";

$query_jul_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[5] AND  YEAR(maintenance_preventive.date_start) = $years_query[5] ";
$query_jul_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[5] AND  YEAR(maintenance_preventive.date_start) = $years_query[5]";
$query_jul_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[5] AND  YEAR(maintenance_preventive.date_start) = $years_query[5] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_jul_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[5] AND  YEAR(maintenance_preventive.date_start) = $years_query[5]";
$query_jul_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[5] AND  YEAR(maintenance_preventive.date_start) = $years_query[5]";
$query_jul_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[5] AND  YEAR(maintenance_preventive.date_start) = $years_query[5]";

$query_ago_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[4] AND  YEAR(maintenance_preventive.date_start) = $years_query[4] ";
$query_ago_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[4] AND  YEAR(maintenance_preventive.date_start) = $years_query[4]";
$query_ago_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[4] AND  YEAR(maintenance_preventive.date_start) = $years_query[4] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_ago_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[4] AND  YEAR(maintenance_preventive.date_start) = $years_query[4]";
$query_ago_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[4] AND  YEAR(maintenance_preventive.date_start) = $years_query[4]";
$query_ago_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[4] AND  YEAR(maintenance_preventive.date_start) = $years_query[4]";

$query_set_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[3] AND  YEAR(maintenance_preventive.date_start) = $years_query[3] ";
$query_set_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[3] AND  YEAR(maintenance_preventive.date_start) = $years_query[3]";
$query_set_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[3] AND  YEAR(maintenance_preventive.date_start) = $years_query[3] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_set_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[3] AND  YEAR(maintenance_preventive.date_start) = $years_query[3]";
$query_set_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[3] AND  YEAR(maintenance_preventive.date_start) = $years_query[3]";
$query_set_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[3] AND  YEAR(maintenance_preventive.date_start) = $years_query[3]";

$query_out_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[2] AND  YEAR(maintenance_preventive.date_start) = $years_query[2] ";
$query_out_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[2] AND  YEAR(maintenance_preventive.date_start) = $years_query[2]";
$query_out_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[2] AND  YEAR(maintenance_preventive.date_start) = $years_query[2] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_out_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[2] AND  YEAR(maintenance_preventive.date_start) = $years_query[2]";
$query_out_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[2] AND  YEAR(maintenance_preventive.date_start) = $years_query[2]";
$query_out_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[2] AND  YEAR(maintenance_preventive.date_start) = $years_query[2]";

$query_nov_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[1] AND  YEAR(maintenance_preventive.date_start) = $years_query[1] ";
$query_nov_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[1] AND  YEAR(maintenance_preventive.date_start) = $years_query[1]";
$query_nov_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[1] AND  YEAR(maintenance_preventive.date_start) = $years_query[1] AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_nov_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[1] AND  YEAR(maintenance_preventive.date_start) = $years_query[1]";
$query_nov_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[1] AND  YEAR(maintenance_preventive.date_start) = $years_query[1]";
$query_nov_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[1] AND  YEAR(maintenance_preventive.date_start) = $years_query[1]";

$query_dez_01="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[0] AND  YEAR(maintenance_preventive.date_start) = $years_query[0] ";
$query_dez_02="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(2) AND MONTH(maintenance_preventive.date_start) = $months_query[0] AND  YEAR(maintenance_preventive.date_start) = $years_query[0]";
$query_dez_03="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(3) AND MONTH(maintenance_preventive.date_start) = $months_query[0] AND  YEAR(maintenance_preventive.date_start) = $years_query[0]  AND  MONTH(maintenance_preventive.date_start) = MONTH(maintenance_preventive.date_mp_end) ";
$query_dez_04="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(4) AND MONTH(maintenance_preventive.date_start) = $months_query[0] AND  YEAR(maintenance_preventive.date_start) = $years_query[0]";
$query_dez_05="SELECT id FROM maintenance_preventive WHERE maintenance_preventive.id_status IN(5) AND MONTH(maintenance_preventive.date_start) = $months_query[0] AND  YEAR(maintenance_preventive.date_start) = $years_query[0]";
$query_dez_06="SELECT id FROM maintenance_preventive WHERE  MONTH(maintenance_preventive.date_start) = $months_query[0] AND  YEAR(maintenance_preventive.date_start) = $years_query[0]";




$query = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) ";

if($instituicao==!""){
    $query = " $query AND  instituicao.id = '$instituicao'";

}
if($setor==!""){
    $query = " $query AND  instituicao_area.id = '$setor'";

}
if($area==!""){
    $query = " $query AND  instituicao_localizacao.id = '$area'";

}
if($equipamento==!""){
    $query = " $query AND  equipamento.id = '$equipamento'";
}

if($equipamento_familia==!""){
    $query = " $query AND  equipamento_familia.id = '$equipamento_familia'";
}

if($yr==!""){
    $query = " $query AND  YEAR(maintenance_preventive.date_start) = '$yr'";

}

if($month==!""){
    $query = " $query AND  MONTH(maintenance_preventive.date_start) = '$month'";
}
if($os_status==!""){
    $query = " $query AND  maintenance_status.status = '$os_status'";
}

if ($stmt = $conn->prepare($query_jan_01)) {
    $stmt->execute();
  $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jan_01=$jan_01+1;

}
}
if ($stmt = $conn->prepare($query_jan_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jan_02=$jan_02+1;

}
}
if ($stmt = $conn->prepare($query_jan_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jan_03=$jan_03+1;

}
}
if ($stmt = $conn->prepare($query_jan_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jan_06=$jan_06+1;

}
}

//fev
if ($stmt = $conn->prepare($query_fev_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $fev_01=$fev_01+1;

}
}
if ($stmt = $conn->prepare($query_fev_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $fev_02=$fev_02+1;

}
}
if ($stmt = $conn->prepare($query_fev_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $fev_03=$fev_03+1;

}
}
if ($stmt = $conn->prepare($query_fev_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $fev_06=$fev_06+1;

}
}
//mar
if ($stmt = $conn->prepare($query_mar_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mar_01=$mar_01+1;

}
}
if ($stmt = $conn->prepare($query_mar_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mar_02=$mar_02+1;

}
}
if ($stmt = $conn->prepare($query_mar_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mar_03=$mar_03+1;

}
}
if ($stmt = $conn->prepare($query_mar_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mar_06=$mar_06+1;

}
}
//abr
if ($stmt = $conn->prepare($query_abr_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $abr_01=$abr_01+1;

}
}
if ($stmt = $conn->prepare($query_abr_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $abr_02=$abr_02+1;

}
}
if ($stmt = $conn->prepare($query_abr_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $abr_03=$abr_03+1;

}
}
if ($stmt = $conn->prepare($query_abr_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $abr_06=$abr_06+1;

}
}
//mai
if ($stmt = $conn->prepare($query_mai_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mai_01=$mai_01+1;

}
}
if ($stmt = $conn->prepare($query_mai_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mai_02=$mai_02+1;

}
}
if ($stmt = $conn->prepare($query_mai_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mai_03=$mai_03+1;

}
}
if ($stmt = $conn->prepare($query_mai_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $mai_06=$mai_06+1;

}
}
//jun
if ($stmt = $conn->prepare($query_jun_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jun_01=$jun_01+1;

}
}
if ($stmt = $conn->prepare($query_jun_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jun_02=$jun_02+1;

}
}
if ($stmt = $conn->prepare($query_jun_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jun_03=$jun_03+1;

}
}
if ($stmt = $conn->prepare($query_jun_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jun_06=$jun_06+1;

}
}
//jul
if ($stmt = $conn->prepare($query_jul_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jul_01=$jul_01+1;

}
}
if ($stmt = $conn->prepare($query_jul_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jul_02=$jul_02+1;

}
}
if ($stmt = $conn->prepare($query_jul_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jul_03=$jul_03+1;

}
}
if ($stmt = $conn->prepare($query_jul_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $jul_06=$jul_06+1;

}
}
//ago
if ($stmt = $conn->prepare($query_ago_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $ago_01=$ago_01+1;

}
}
if ($stmt = $conn->prepare($query_ago_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $ago_02=$ago_02+1;

}
}
if ($stmt = $conn->prepare($query_ago_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $ago_03=$ago_03+1;

}
}
if ($stmt = $conn->prepare($query_ago_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $ago_06=$ago_06+1;

}
}
//set
if ($stmt = $conn->prepare($query_set_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $set_01=$set_01+1;

}
}
if ($stmt = $conn->prepare($query_set_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $set_02=$set_02+1;

}
}
if ($stmt = $conn->prepare($query_set_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $set_03=$set_03+1;

}
}
if ($stmt = $conn->prepare($query_set_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $set_06=$set_06+1;

}
}
//out
if ($stmt = $conn->prepare($query_out_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $out_01=$out_01+1;

}
}
if ($stmt = $conn->prepare($query_out_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $out_02=$out_02+1;

}
}
if ($stmt = $conn->prepare($query_out_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $out_03=$out_03+1;

}
}
if ($stmt = $conn->prepare($query_out_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $out_06=$out_06+1;

}
}
//nov
if ($stmt = $conn->prepare($query_nov_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $nov_01=$nov_01+1;

}
}
if ($stmt = $conn->prepare($query_nov_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $nov_02=$nov_02+1;

}
}
if ($stmt = $conn->prepare($query_nov_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $nov_03=$nov_03+1;

}
}
if ($stmt = $conn->prepare($query_nov_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $nov_06=$nov_06+1;

}
}
//dez
if ($stmt = $conn->prepare($query_dez_01)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $dez_01=$dez_01+1;

}
}
if ($stmt = $conn->prepare($query_dez_02)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $dez_02=$dez_02+1;

}
}
if ($stmt = $conn->prepare($query_dez_03)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $dez_03=$dez_03+1;

}
}
if ($stmt = $conn->prepare($query_dez_06)) {
    $stmt->execute();
    $stmt->bind_result($id);
    while ($stmt->fetch()) {
        $dez_06=$dez_06+1;

}
}
$query_table = " $query AND  MONTH(maintenance_preventive.date_mp_end) != $month group by maintenance_preventive.id ";
$query = " $query group by maintenance_preventive.id ";

$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[11] AND  MONTH(maintenance_preventive.date_start) = $months_query[11] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_01=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[10] AND  MONTH(maintenance_preventive.date_start) = $months_query[10] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_02=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[9] AND  MONTH(maintenance_preventive.date_start) = $months_query[9] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_03=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[8] AND  MONTH(maintenance_preventive.date_start) = $months_query[8] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_04=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[7] AND  MONTH(maintenance_preventive.date_start) = $months_query[7] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_05=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[6] AND  MONTH(maintenance_preventive.date_start) = $months_query[6] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_06=($status/$mp_cout)*100;  
}
}

$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[5] AND  MONTH(maintenance_preventive.date_start) = $months_query[5] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_07=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[4] AND  MONTH(maintenance_preventive.date_start) = $months_query[4] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_08=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[3] AND  MONTH(maintenance_preventive.date_start) = $months_query[3] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_09=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[2] AND  MONTH(maintenance_preventive.date_start) = $months_query[2] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_10=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[1] AND  MONTH(maintenance_preventive.date_start) = $months_query[1] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_11=($status/$mp_cout)*100;  
}
}
$query_kpi = "SELECT maintenance_preventive.date_mp_end,maintenance_preventive.obs_ne,maintenance_preventive.id_status,equipamento.codigo,equipamento_familia.fabricante,maintenance_status.status,instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_preventive.date_start,maintenance_preventive.id,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_status ON maintenance_status.id = maintenance_preventive.id_status WHERE maintenance_preventive.id_status IN(1,2,3) AND  YEAR(maintenance_preventive.date_start) = $years_query[0] AND  MONTH(maintenance_preventive.date_start) = $months_query[0] group by maintenance_preventive.id";
$mp_cout = 0;
$status = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($date_mp_end,$obs_ne,$maintenance_status,$codigo,$fabricante,$status_mp,$intituicao,$setor,$area,$date_start_mp,$id,$rotina,$id_rotina,$date_start,$periodicidade,$regdate,$upgrade,$codigo,$familia,$modelo);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
    $mes_open = date("m", strtotime($date_start_mp));
    $mes_close =  date("m", strtotime($date_mp_end));
    if($maintenance_status==3 &&  $mes_open == $mes_close ){$status=$status+1; }
     $kpi_12=($status/$mp_cout)*100;  
}
}
?>
  <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
      
      ['Programado', 'Programado',{ role: 'annotation'} ,'Realizado', { role: 'annotation'} ,'Atrasado',  { role: 'annotation'} , 'Tendência'],
        ['<?php printf($years_query[11]."/".$months_query[11]); ?>',  <?php printf($jan_06); ?>,<?php printf($jan_06); ?>,      <?php printf($jan_03); ?>,<?php printf($jan_03); ?>,          <?php printf($jan_02); ?>,<?php printf($jan_02); ?>,                <?php $jan=($jan_01+$jan_02+$jan_03+$jan_06)/4; printf($jan); ?>],
        ['<?php printf($years_query[10]."/".$months_query[10]); ?> ',  <?php printf($fev_06); ?>,<?php printf($fev_06); ?>,      <?php printf($fev_03); ?>,<?php printf($fev_03); ?>,          <?php printf($fev_02); ?>,<?php printf($fev_02); ?>,                 <?php $fev=($fev_01+$fev_02+$fev_03+$fev_06)/4; printf($fev); ?>],
        ['<?php printf($years_query[9]."/".$months_query[9]); ?> ',  <?php printf($mar_06); ?>,<?php printf($mar_06); ?>,      <?php printf($mar_03); ?>,<?php printf($mar_03); ?>,          <?php printf($mar_02); ?>,<?php printf($mar_02); ?>,             <?php $mar=($mar_01+$mar_02+$mar_03+$mar_06)/4; printf($mar); ?>],
        ['<?php printf($years_query[8]."/".$months_query[8]); ?> ',  <?php printf($abr_06); ?>,<?php printf($abr_06); ?>,      <?php printf($abr_03); ?>,<?php printf($abr_03); ?>,          <?php printf($abr_02); ?>,<?php printf($abr_02); ?>,               <?php $abr=($abr_01+$jabr_02+$abr_03+$abr_06)/4; printf($abr); ?>],
        ['<?php printf($years_query[7]."/".$months_query[7]); ?> ',  <?php printf($mai_06); ?>,<?php printf($mai_06); ?>,      <?php printf($mai_03); ?>,<?php printf($mai_03); ?>,          <?php printf($mai_02); ?>,<?php printf($mai_02); ?>,                <?php $mai=($mai_01+$mai_02+$mai_03+$mai_06)/4; printf($mai); ?>],
        ['<?php printf($years_query[6]."/".$months_query[6]); ?> ',  <?php printf($jun_06); ?>,<?php printf($jun_06); ?>,      <?php printf($jun_03); ?>,<?php printf($jun_03); ?>,          <?php printf($jun_02); ?>,<?php printf($jun_02); ?>,                  <?php $jun=($jun_01+$jun_02+$jun_03+$jun_06)/4; printf($jun); ?>],
        ['<?php printf($years_query[5]."/".$months_query[5]); ?> ',  <?php printf($jul_06); ?>,<?php printf($jul_06); ?>,      <?php printf($jul_03); ?>,<?php printf($jul_03); ?>,          <?php printf($jul_02); ?>,<?php printf($jul_02); ?>,               <?php $jul=($jul_01+$jul_02+$jul_03+$jul_06)/4; printf($jul); ?>],
        ['<?php printf($years_query[4]."/".$months_query[4]); ?> ',  <?php printf($ago_06); ?>,<?php printf($ago_06); ?>,      <?php printf($ago_03); ?>,<?php printf($ago_03); ?>,          <?php printf($ago_02); ?>,<?php printf($ago_02); ?>,                <?php $ago=($ago_01+$ago_02+$ago_03+$ago_06)/4; printf($ago); ?>],
        ['<?php printf($years_query[3]."/".$months_query[3]); ?>',  <?php printf($set_06); ?>,<?php printf($set_06); ?>,      <?php printf($set_03); ?>,<?php printf($set_03); ?>,          <?php printf($set_02); ?>,<?php printf($set_02); ?>,                <?php $set=($set_01+$set_02+$set_03+$set_06)/4; printf($set); ?>],
        ['<?php printf($years_query[2]."/".$months_query[2]); ?>',  <?php printf($out_06); ?>,<?php printf($out_06); ?>,      <?php printf($out_03); ?>,<?php printf($out_03); ?>,          <?php printf($out_02); ?>,<?php printf($out_02); ?>,               <?php $out=($out_01+$out_02+$out_03+$jout_06)/4; printf($out); ?>],
        ['<?php printf($years_query[1]."/".$months_query[1]); ?> ',  <?php printf($nov_06); ?>,<?php printf($nov_06); ?>,      <?php printf($nov_03); ?>,<?php printf($nov_03); ?>,          <?php printf($nov_02); ?>,<?php printf($nov_02); ?>,                  <?php $nov=($nov_01+$nov02+$nov_03+$nov_06)/4; printf($nov); ?>],
        ['<?php printf($years_query[0]."/".$months_query[0]); ?> ',  <?php printf($dez_06); ?>,<?php printf($dez_06); ?>,      <?php printf($dez_03); ?>,<?php printf($dez_03); ?>,          <?php printf($dez_02); ?>,<?php printf($dez_02); ?>,                <?php $dez=($dez_01+$dez_02+$dez_03+$dez_06)/4; printf($dez); ?>]
      ]);

        var options = {
          title : 'Indicador de Manutenção Preventiva',
          vAxis: {title: 'M.P'},
          hAxis: {title: 'Mês',
            slantedText: true, // Isso permite o texto inclinado no eixo
            slantedTextAngle: 45}, // Define o ângulo de inclinação},
          seriesType: 'bars',
          legend: { position: 'top', maxLines: 3 },
          series: {3: {type: 'line'}},
          colors: ['blue', 'red', 'orange', 'green'], // Defina as cores das barras aqui
          bar: {  groupWidth: '80%', // largura do grupo
            gapWidth: '25%'
          }, 
          bars: { 
            width: '100%', // largura das barras
            lineWidth: 1, 
            barSpacing: '25%', // espaço entre as barras
            groupSpacing: '300%' // espaço entre os grupos
          },
          annotations: {
            boxStyle: {
              // Color of the box outline.
              stroke: '#888',
              // Thickness of the box outline.
              strokeWidth: 1,
              // x-radius of the corner curvature.
              rx: 10,
              // y-radius of the corner curvature.
              ry: 10,
              // Attributes for linear gradient fill.
              gradient: {
                // Start color for gradient.
                color1: '#fbf6a7',
                // Finish color for gradient.
                color2: '#33b679',
                // WHERE on the boundary to start AND
                // end the color1/color2 gradient,
                // relative to the upper left corner
                // of the boundary.
                x1: '0%', y1: '0%',
                x2: '100%', y2: '100%',
                // If true, the boundary for x1,
                // y1, x2, AND y2 is the box. If
                // false, it's the entire chart.
                useObjectBoundingBoxUnits: true
              }
            }
          }
        };
        

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
        google.visualization.events.addListener(chart, 'ready', function () {
                chart_div.innerHTML = '<img id="chart" src=' + chart.getImageURI() + '>';
               

            });
            chart.draw(data, options);
          }
    </script>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="dashboard_graph">

      <div class="row x_title">


      </div>

      <div class="col-md-12 col-sm-12 ">
             <div id="chart_div" style="width:1000px; height: 500px;">  </div>

      </div>
       

      <div class="clearfix"></div>
    </div>
  </div>

</div>
<br />
<?php

$filtro="";
$tnd="00:00:00";
$os_count=0;
$date_start_cm_2="0000-00-01";
$status_1=0;
$status_2=0;
$status_3=0;
$status_4=0;
$status_5=0;
$status_6=0;
$jan_01=0;
$jan_02=0;
$jan_03=0;
$jan_04=0;
$jan_05=0;
$jan_06=0;








$query_jan_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) =  $months_query[11] AND  YEAR(os.reg_date) =  $years_query[11] ";
$query_jan_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) =  $months_query[11] AND  YEAR(os.reg_date) =  $years_query[11]";
$query_jan_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) =  $months_query[11] AND  YEAR(os.reg_date) =  $years_query[11]";
$query_jan_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) =  $months_query[11] AND  YEAR(os.reg_date) =  $years_query[11]";
$query_jan_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) =  $months_query[11] AND  YEAR(os.reg_date) =  $years_query[11]";
$query_jan_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) =  $months_query[11] AND  YEAR(os.reg_date) =  $years_query[11] AND  MONTH(os.date_end) <= '$months_query[11]'";

$query_fev_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[10] AND  YEAR(os.reg_date) =  $years_query[10] ";
$query_fev_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[10] AND  YEAR(os.reg_date) =  $years_query[10]";
$query_fev_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[10] AND  YEAR(os.reg_date) =  $years_query[10]";
$query_fev_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[10] AND  YEAR(os.reg_date) =  $years_query[10]";
$query_fev_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[10] AND  YEAR(os.reg_date) =  $years_query[10]";
$query_fev_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[10] AND  YEAR(os.reg_date) =  $years_query[10] AND  MONTH(os.date_end) <= '$months_query[10]'";

$query_mar_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[9] AND  YEAR(os.reg_date) =  $years_query[9] ";
$query_mar_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[9] AND  YEAR(os.reg_date) =  $years_query[9]";
$query_mar_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[9] AND  YEAR(os.reg_date) =  $years_query[9]";
$query_mar_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[9] AND  YEAR(os.reg_date) =  $years_query[9]";
$query_mar_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[9] AND  YEAR(os.reg_date) =  $years_query[9]";
$query_mar_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[9] AND  YEAR(os.reg_date) =  $years_query[9] AND  MONTH(os.date_end) <= '$months_query[9]'";

$query_abr_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[8] AND  YEAR(os.reg_date) =  $years_query[8] ";
$query_abr_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[8] AND  YEAR(os.reg_date) =  $years_query[8]";
$query_abr_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[8] AND  YEAR(os.reg_date) =  $years_query[8]";
$query_abr_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[8] AND  YEAR(os.reg_date) =  $years_query[8]";
$query_abr_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[8] AND  YEAR(os.reg_date) =  $years_query[8]";
$query_abr_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[8] AND  YEAR(os.reg_date) =  $years_query[8] AND  MONTH(os.date_end) <= '$months_query[8]'";

$query_mai_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[7] AND  YEAR(os.reg_date) =  $years_query[7] ";
$query_mai_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[7] AND  YEAR(os.reg_date) =  $years_query[7]";
$query_mai_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[7] AND  YEAR(os.reg_date) =  $years_query[7]";
$query_mai_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[7] AND  YEAR(os.reg_date) =  $years_query[7]";
$query_mai_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[7] AND  YEAR(os.reg_date) =  $years_query[7]";
$query_mai_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[7] AND  YEAR(os.reg_date) =  $years_query[7] AND  MONTH(os.date_end) <= '$months_query[7]'";

$query_jun_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[6] AND  YEAR(os.reg_date) =  $years_query[6] ";
$query_jun_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[6] AND  YEAR(os.reg_date) =  $years_query[6]";
$query_jun_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[6] AND  YEAR(os.reg_date) =  $years_query[6]";
$query_jun_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[6] AND  YEAR(os.reg_date) =  $years_query[6]";
$query_jun_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[6] AND  YEAR(os.reg_date) =  $years_query[6]";
$query_jun_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[6] AND  YEAR(os.reg_date) =  $years_query[6] AND  MONTH(os.date_end) <= '$months_query[6]'";

$query_jul_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[5] AND  YEAR(os.reg_date) =  $years_query[5] ";
$query_jul_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[5] AND  YEAR(os.reg_date) =  $years_query[5]";
$query_jul_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[5] AND  YEAR(os.reg_date) =  $years_query[5]";
$query_jul_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[5] AND  YEAR(os.reg_date) =  $years_query[5]";
$query_jul_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[5] AND  YEAR(os.reg_date) =  $years_query[5]";
$query_jul_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[5] AND  YEAR(os.reg_date) =  $years_query[5] AND  MONTH(os.date_end) <= '$months_query[5]'";

$query_ago_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[4] AND  YEAR(os.reg_date) =  $years_query[4] ";
$query_ago_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[4] AND  YEAR(os.reg_date) =  $years_query[4]";
$query_ago_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[4] AND  YEAR(os.reg_date) =  $years_query[4]";
$query_ago_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[4] AND  YEAR(os.reg_date) =  $years_query[4]";
$query_ago_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[4] AND  YEAR(os.reg_date) =  $years_query[4]";
$query_ago_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[4] AND  YEAR(os.reg_date) =  $years_query[4] AND  MONTH(os.date_end) <= '$months_query[4]'";

$query_set_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[3] AND  YEAR(os.reg_date) =  $years_query[3] ";
$query_set_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[3] AND  YEAR(os.reg_date) =  $years_query[3]";
$query_set_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[3] AND  YEAR(os.reg_date) =  $years_query[3]";
$query_set_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[3] AND  YEAR(os.reg_date) =  $years_query[3]";
$query_set_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[3] AND  YEAR(os.reg_date) =  $years_query[3]";
$query_set_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[3] AND  YEAR(os.reg_date) =  $years_query[3] AND  MONTH(os.date_end) <= '$months_query[3]'";

$query_out_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[2] AND  YEAR(os.reg_date) =  $years_query[2] ";
$query_out_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[2] AND  YEAR(os.reg_date) =  $years_query[2]";
$query_out_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[2] AND  YEAR(os.reg_date) =  $years_query[2]";
$query_out_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[2] AND  YEAR(os.reg_date) =  $years_query[2]";
$query_out_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[2] AND  YEAR(os.reg_date) =  $years_query[2]";
$query_out_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[2] AND  YEAR(os.reg_date) =  $years_query[2] AND  MONTH(os.date_end) <= '$months_query[2]'";

$query_nov_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[1] AND  YEAR(os.reg_date) =  $years_query[1] ";
$query_nov_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[1] AND  YEAR(os.reg_date) =  $years_query[1]";
$query_nov_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[1] AND  YEAR(os.reg_date) =  $years_query[1]";
$query_nov_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[1] AND  YEAR(os.reg_date) =  $years_query[1]";
$query_nov_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[1] AND  YEAR(os.reg_date) =  $years_query[1]";
$query_nov_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[1] AND  YEAR(os.reg_date) =  $years_query[1] AND  MONTH(os.date_end) <= '$months_query[1]'";

$query_dez_01="SELECT id FROM os WHERE os.id_status IN(1) AND MONTH(os.reg_date) = $months_query[0] AND  YEAR(os.reg_date) =  $years_query[0] ";
$query_dez_02="SELECT id FROM os WHERE os.id_status IN(2) AND MONTH(os.reg_date) = $months_query[0] AND  YEAR(os.reg_date) =  $years_query[0]";
$query_dez_03="SELECT id FROM os WHERE os.id_status IN(3) AND MONTH(os.reg_date) = $months_query[0] AND  YEAR(os.reg_date) =  $years_query[0]";
$query_dez_04="SELECT id FROM os WHERE os.id_status IN(4) AND MONTH(os.reg_date) = $months_query[0] AND  YEAR(os.reg_date) =  $years_query[0]";
$query_dez_05="SELECT id FROM os WHERE os.id_status IN(5) AND MONTH(os.reg_date) = $months_query[0] AND  YEAR(os.reg_date) =  $years_query[0]";
$query_dez_06="SELECT id FROM os WHERE os.id_status IN(6) AND MONTH(os.reg_date) = $months_query[0] AND  YEAR(os.reg_date) =  $years_query[0] AND  MONTH(os.date_end) <= '$months_query[0]'";




$query = "SELECT regdate_os_posicionamento.detail,os_posicionamento.status,os.defeito,os.services,os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os LEFT JOIN os_posicionamento ON os_posicionamento.id = os.id_posicionamento INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id LEFT JOIN regdate_os_posicionamento on regdate_os_posicionamento.id_os = os.id WHERE os.id_status IN(1,2,3,4,5,6) ";

if($instituicao==!""){
  $query = " $query AND  instituicao.id = '$instituicao'";

}
if($setor==!""){
  $query = " $query AND  instituicao_area.id = '$setor'";

}
if($area==!""){
  $query = " $query AND  instituicao_localizacao.id = '$area'";

}

if($equipamento==!""){
  $query = " $query AND  equipamento.id = '$equipamento'";
}

if($equipamento_familia==!""){
  $query = " $query AND  equipamento_familia.id = '$equipamento_familia'";
}

if($yr==!""){
  $query = " $query AND  YEAR(os.reg_date) = '$yr'";

}

if($month==!""){
  $query = " $query AND  MONTH(os.reg_date) = '$month'";
}
  $jan_01 = 0;
if ($stmt = $conn->prepare($query_jan_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jan_01=$jan_01+1;

  }
}
  $jan_02 = 0;
if ($stmt = $conn->prepare($query_jan_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jan_02=$jan_02+1;

  }
}
  $jan_03=0;
if ($stmt = $conn->prepare($query_jan_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jan_03=$jan_03+1;

  }
}
if ($stmt = $conn->prepare($query_jan_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jan_06=$jan_06+1;

  }
}

//fev
  $fev_01=0;
if ($stmt = $conn->prepare($query_fev_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $fev_01=$fev_01+1;

  }
}
if ($stmt = $conn->prepare($query_fev_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $fev_02=$fev_02+1;

  }
}
  $fev_03=0;
if ($stmt = $conn->prepare($query_fev_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $fev_03=$fev_03+1;

  }
}
  $fev_06=0;
if ($stmt = $conn->prepare($query_fev_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $fev_06=$fev_06+1;

  }
}
//mar
  $mar_01=0;
if ($stmt = $conn->prepare($query_mar_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mar_01=$mar_01+1;

  }
}
  $mar_02=0;
if ($stmt = $conn->prepare($query_mar_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mar_02=$mar_02+1;

  }
}
  $mar_03=0;
if ($stmt = $conn->prepare($query_mar_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mar_03=$mar_03+1;

  }
}
  $mar_06=0;
if ($stmt = $conn->prepare($query_mar_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mar_06=$mar_06+1;

  }
}
//abr
  $abr_01=0;
if ($stmt = $conn->prepare($query_abr_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $abr_01=$abr_01+1;

  }
}
  $abr_02=0;
if ($stmt = $conn->prepare($query_abr_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $abr_02=$abr_02+1;

  }
}
  $abr_03=0;
if ($stmt = $conn->prepare($query_abr_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $abr_03=$abr_03+1;

  }
}
  $abr_06=0;
if ($stmt = $conn->prepare($query_abr_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $abr_06=$abr_06+1;

  }
}
//mai
  $mai_01=0;
if ($stmt = $conn->prepare($query_mai_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mai_01=$mai_01+1;

  }
}
  $mai_02=0;
if ($stmt = $conn->prepare($query_mai_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mai_02=$mai_02+1;

  }
}
  $mai_03=0;
if ($stmt = $conn->prepare($query_mai_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mai_03=$mai_03+1;

  }
}
  $mai_06=0;
if ($stmt = $conn->prepare($query_mai_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $mai_06=$mai_06+1;

  }
}
//jun
  $jun_01=0;
if ($stmt = $conn->prepare($query_jun_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jun_01=$jun_01+1;

  }
}
  $jun_02=0;
if ($stmt = $conn->prepare($query_jun_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jun_02=$jun_02+1;

  }
}
  $jun_03=0;
if ($stmt = $conn->prepare($query_jun_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jun_03=$jun_03+1;

  }
}
  $jun_06=0;
if ($stmt = $conn->prepare($query_jun_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jun_06=$jun_06+1;

  }
}
//jul
  $jul_01=0;
if ($stmt = $conn->prepare($query_jul_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jul_01=$jul_01+1;

  }
}
  $jul_02=0;
if ($stmt = $conn->prepare($query_jul_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jul_02=$jul_02+1;

  }
}
  $jul_03=0;
if ($stmt = $conn->prepare($query_jul_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jul_03=$jul_03+1;

  }
}
  $jul_06=0;
if ($stmt = $conn->prepare($query_jul_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $jul_06=$jul_06+1;

  }
}
//ago
  $ago_01=0;
if ($stmt = $conn->prepare($query_ago_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $ago_01=$ago_01+1;

  }
}
  $ago_02=0;
if ($stmt = $conn->prepare($query_ago_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $ago_02=$ago_02+1;

  }
}
  $ago_03=0;
if ($stmt = $conn->prepare($query_ago_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $ago_03=$ago_03+1;

  }
}
  $ago_06=0;
if ($stmt = $conn->prepare($query_ago_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $ago_06=$ago_06+1;

  }
}
//set
  $set_01=0;
if ($stmt = $conn->prepare($query_set_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $set_01=$set_01+1;

  }
}
  $set_02=0;
if ($stmt = $conn->prepare($query_set_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $set_02=$set_02+1;

  }
}
  $set_03=0;
if ($stmt = $conn->prepare($query_set_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $set_03=$set_03+1;

  }
}
  $set_06=0;
if ($stmt = $conn->prepare($query_set_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $set_06=$set_06+1;

  }
}
//out
  $out_01=0;
if ($stmt = $conn->prepare($query_out_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $out_01=$out_01+1;

  }
}
  $out_02=0;
if ($stmt = $conn->prepare($query_out_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $out_02=$out_02+1;

  }
}
  $out_03=0;
if ($stmt = $conn->prepare($query_out_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $out_03=$out_03+1;

  }
}
  $out_06=0;
if ($stmt = $conn->prepare($query_out_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $out_06=$out_06+1;

  }
}
//nov
  $nov_01=0;
if ($stmt = $conn->prepare($query_nov_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $nov_01=$nov_01+1;

  }
}
  $nov_02=0;
if ($stmt = $conn->prepare($query_nov_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $nov_02=$nov_02+1;

  }
}
  $nov_03=0;
if ($stmt = $conn->prepare($query_nov_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $nov_03=$nov_03+1;

  }
}
  $nov_06=0;
if ($stmt = $conn->prepare($query_nov_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $nov_06=$nov_06+1;

  }
}
//dez
  $dez_01=0;
if ($stmt = $conn->prepare($query_dez_01)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $dez_01=$dez_01+1;

  }
}
  $dez_02=0;
if ($stmt = $conn->prepare($query_dez_02)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $dez_02=$dez_02+1;

  }
}
  $dez_03=0;
if ($stmt = $conn->prepare($query_dez_03)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $dez_03=$dez_03+1;

  }
}
  $dez_06=0;
if ($stmt = $conn->prepare($query_dez_06)) {
  $stmt->execute();
  $stmt->bind_result($id);
  while ($stmt->fetch()) {
    $dez_06=$dez_06+1;

  }
}
//Ordem de serviço


$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[11]-$months_query[11]%'");
$stmt->execute();
$stmt->bind_result($os_jan);
while ( $stmt->fetch()) {
    $os_jan=$os_jan;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[11]-$months_query[11]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_jan);
while ( $stmt->fetch()) {
    $os_aberto_jan=$os_aberto_jan;
};
if ($os_aberto_jan == 0) {
    $os_aberto_jan = 0.01;
}
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[11]-$months_query[11]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_jan);
while ( $stmt->fetch()) {
    $os_atrasada_jan=$os_atrasada_jan;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[11]-$months_query[11]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_jan);
while ( $stmt->fetch()) {
    $os_fechada_jan=$os_fechada_jan;
};

$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[10]-$months_query[10]%'");
$stmt->execute();
$stmt->bind_result($os_fev);
while ( $stmt->fetch()) {
    $os_fev=$os_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[10]-$months_query[10]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_fev);
while ( $stmt->fetch()) {
    $os_aberto_fev=$os_aberto_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[10]-$months_query[10]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_fev);
while ( $stmt->fetch()) {
    $os_atrasada_fev=$os_atrasada_fev;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[10]-$months_query[10]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_fev);
while ( $stmt->fetch()) {
    $os_fechada_fev=$os_fechada_fev;
};

$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[9]-$months_query[9]%'");
$stmt->execute();
$stmt->bind_result($os_mar);
while ( $stmt->fetch()) {
    $os_mar=$os_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[9]-$months_query[9]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_mar);
while ( $stmt->fetch()) {
    $os_aberto_mar=$os_aberto_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[9]-$months_query[9]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_mar);
while ( $stmt->fetch()) {
    $os_atrasada_mar=$os_atrasada_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[9]-$months_query[9]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_mar);
while ( $stmt->fetch()) {
    $os_fechada_mar=$os_fechada_mar;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[8]-$months_query[8]%'");
$stmt->execute();
$stmt->bind_result($os_abr);
while ( $stmt->fetch()) {
    $os_abr=$os_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[8]-$months_query[8]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_abr);
while ( $stmt->fetch()) {
    $os_aberto_abr=$os_aberto_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[8]-$months_query[8]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_abr);
while ( $stmt->fetch()) {
    $os_atrasada_abr=$os_atrasada_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[8]-$months_query[8]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_abr);
while ( $stmt->fetch()) {
    $os_fechada_abr=$os_fechada_abr;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[7]-$months_query[7]%'");
$stmt->execute();
$stmt->bind_result($os_mai);
while ( $stmt->fetch()) {
    $os_mai=$os_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[7]-$months_query[7]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_mai);
while ( $stmt->fetch()) {
    $os_aberto_mai=$os_aberto_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[7]-$months_query[7]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_mai);
while ( $stmt->fetch()) {
    $os_atrasada_mai=$os_atrasada_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[7]-$months_query[7]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_mai);
while ( $stmt->fetch()) {
    $os_fechada_mai=$os_fechada_mai;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[6]-$months_query[6]%'");
$stmt->execute();
$stmt->bind_result($os_jun);
while ( $stmt->fetch()) {
    $os_jun=$os_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[6]-$months_query[6]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_jun);
while ( $stmt->fetch()) {
    $os_aberto_jun=$os_aberto_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[6]-$months_query[6]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_jun);
while ( $stmt->fetch()) {
    $os_atrasada_jun=$os_atrasada_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[6]-$months_query[6]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_jun);
while ( $stmt->fetch()) {
    $os_fechada_jun=$os_fechada_jun;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[5]-$months_query[5]%'");
$stmt->execute();
$stmt->bind_result($os_jul);
while ( $stmt->fetch()) {
    $os_jul=$os_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[5]-$months_query[5]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_jul);
while ( $stmt->fetch()) {
    $os_aberto_jul=$os_aberto_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[5]-$months_query[5]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_jul);
while ( $stmt->fetch()) {
    $os_atrasada_jul=$os_atrasada_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[5]-$months_query[5]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_jul);
while ( $stmt->fetch()) {
    $os_fechada_jul=$os_fechada_jul;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[4]-$months_query[4]%'");
$stmt->execute();
$stmt->bind_result($os_ago);
while ( $stmt->fetch()) {
    $os_ago=$os_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[4]-$months_query[4]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_ago);
while ( $stmt->fetch()) {
    $os_aberto_ago=$os_aberto_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[4]-$months_query[4]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_ago);
while ( $stmt->fetch()) {
    $os_atrasada_ago=$os_atrasada_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[4]-$months_query[4]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_ago);
while ( $stmt->fetch()) {
    $os_fechada_ago=$os_fechada_ago;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[3]-$months_query[3]%'");
$stmt->execute();
$stmt->bind_result($os_set);
while ( $stmt->fetch()) {
    $os_set=$os_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE date_start like '%$years_query[3]-$months_query[3]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_set);
while ( $stmt->fetch()) {
    $os_aberto_set=$os_aberto_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[3]-$months_query[3]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_set);
while ( $stmt->fetch()) {
    $os_atrasada_set=$os_atrasada_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[3]-$months_query[3]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_set);
while ( $stmt->fetch()) {
    $os_fechada_set=$os_fechada_set;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[2]-$months_query[2]%'");
$stmt->execute();
$stmt->bind_result($os_out);
while ( $stmt->fetch()) {
    $os_out=$os_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[2]-$months_query[2]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_out);
while ( $stmt->fetch()) {
    $os_aberto_out=$os_aberto_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[2]-$months_query[2]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_out);
while ( $stmt->fetch()) {
    $os_atrasada_out=$os_atrasada_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[2]-$months_query[2]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_out);
while ( $stmt->fetch()) {
    $os_fechada_out=$os_fechada_out;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[1]-$months_query[1]%'");
$stmt->execute();
$stmt->bind_result($os_nov);
while ( $stmt->fetch()) {
    $os_nov=$os_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[1]-$months_query[1]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_nov);
while ( $stmt->fetch()) {
    $os_aberto_nov=$os_aberto_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[1]-$months_query[1]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_nov);
while ( $stmt->fetch()) {
    $os_atrasada_nov=$os_atrasada_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[1]-$months_query[1]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_nov);
while ( $stmt->fetch()) {
    $os_fechada_nov=$os_fechada_nov;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[0]-$months_query[0]%'");
$stmt->execute();
$stmt->bind_result($os_dez);
while ( $stmt->fetch()) {
    $os_dez=$os_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[0]-$months_query[0]%' AND id_status = 1");
$stmt->execute();
$stmt->bind_result($os_aberto_dez);
while ( $stmt->fetch()) {
    $os_aberto_dez=$os_aberto_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[0]-$months_query[0]%' AND id_status = 2");
$stmt->execute();
$stmt->bind_result($os_atrasada_dez);
while ( $stmt->fetch()) {
    $os_atrasada_dez=$os_atrasada_dez;
};
$stmt = $conn->prepare("SELECT COUNT(id) FROM os WHERE reg_date like '%$years_query[0]-$months_query[0]%' AND id_status = 3");
$stmt->execute();
$stmt->bind_result($os_fechada_dez);
while ( $stmt->fetch()) {
    $os_fechada_dez=$os_fechada_dez;
};

for ($i=0; $i <= $mes ; $i++) {
    $v_mes=$v_mes+$i;
    if ($i==1) {
        $v_os_fechada=($os_fechada_jan*$os_fechada_jan)+$v_os_fechada;
        $v_os_aberto=($os_aberto_jan*$os_aberto_jan)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_jan+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_jan+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_jan*1)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_jan*1)+$v_os_aberto_prod;
    }
    if ($i==2) {
        $v_os_fechada=($os_fechada_fev*$os_fechada_fev)+$v_os_fechada;
        $v_os_aberto=($os_aberto_fev*$os_aberto_fev)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_fev+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_fev+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_fev*2)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_fev*2)+$v_os_aberto_prod;
    }
    if ($i==3) {
        $v_os_fechada=($os_fechada_mar*$os_fechada_mar)+$v_os_fechada;
        $v_os_aberto=($os_aberto_mar*$os_aberto_mar)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_mar+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_mar+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_mar*3)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_mar*3)+$v_os_aberto_prod;
    }
    if ($i==4) {
        $v_os_fechada=($os_fechada_abr*$os_fechada_abr)+$v_os_fechada;
        $v_os_aberto=($os_aberto_abr*$os_aberto_abr)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_abr+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_abr+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_abr*4)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_abr*4)+$v_os_aberto_prod;
    }
    if ($i==5) {
        $v_os_fechada=($os_fechada_mai*$os_fechada_mai)+$v_os_fechada;
        $v_os_aberto=($os_aberto_mai*$os_aberto_mai)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_mai+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_mai+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_mai*5)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_mai*5)+$v_os_aberto_prod;
    }
    if ($i==6) {
        $v_os_fechada=($os_fechada_jun*$os_fechada_jun)+$v_os_fechada;
        $v_os_aberto=($os_aberto_jun*$os_aberto_jun)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_jun+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_jun+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_jun*6)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_jun*6)+$v_os_aberto_prod;
    }
    if ($i==7) {
        $v_os_fechada=($os_fechada_jul*$os_fechada_jul)+$v_os_fechada;
        $v_os_aberto=($os_aberto_jul*$os_aberto_jul)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_jul+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_jul+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_jul*7)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_jul*7)+$v_os_aberto_prod;
    }
    if ($i==8) {
        $v_os_fechada=($os_fechada_ago*$os_fechada_ago)+$v_os_fechada;
        $v_os_aberto=($os_aberto_ago*$os_aberto_ago)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_ago+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_ago+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_ago*8)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_ago*8)+$v_os_aberto_prod;
    }
    if ($i==9) {
        $v_os_fechada=($os_fechada_set*$os_fechada_set)+$v_os_fechada;
        $v_os_aberto=($os_aberto_set*$os_aberto_set)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_set+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_set+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_set*9)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_set*9)+$v_os_aberto_prod;
    }
    if ($i==10) {
        $v_os_fechada=($os_fechada_out*$os_fechada_out)+$v_os_fechada;
        $v_os_aberto=($os_aberto_out*$os_aberto_out)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_out+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_out+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_out*10)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_out*10)+$v_os_aberto_prod;
    }
    if ($i==11) {
        $v_os_fechada=($os_fechada_nov*$os_fechada_nov)+$v_os_fechada;
        $v_os_aberto=($os_aberto_nov*$os_aberto_nov)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_nov+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_nov+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_nov*11)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_nov*11)+$v_os_aberto_prod;
    }
    if ($i==12) {
        $v_os_fechada=($os_fechada_dez*$os_fechada_dez)+$v_os_fechada;
        $v_os_aberto=($os_aberto_dez*$os_aberto_dez)+$v_os_aberto;
        $v_os_fechada_sum=$os_fechada_dez+$v_os_fechada_sum;
        $v_os_aberto_sum=$os_aberto_dez+$v_os_aberto_sum;
        $v_os_fechada_prod=($os_fechada_dez*12)+$v_os_fechada_prod;
        $v_os_aberto_prod=($os_aberto_dez*12)+$v_os_aberto_prod;
    }
    
}

$a1_os_fechada = $v_os_fechada*$v_mes;
$a1_os_aberto = $v_os_aberto*$v_mes;
$a2_os_fechada = $v_os_fechada_sum*$v_os_fechada_prod;
$a2_os_aberto = $v_os_aberto_sum*$v_os_aberto_prod;
$a3_os_fechada = $mes*$v_os_fechada;
$a3_os_aberto = $mes*$v_os_aberto;
$a4_os_fechada =  $v_os_fechada_sum*$v_os_fechada_sum;
$a4_os_aberto =  $v_os_aberto_sum*$v_os_aberto_sum;
  if ($a3_os_fechada-$a4_os_fechada == 0) {
    $a_os_fechado = 0;
  }
  else{
$a_os_fechado=(($a1_os_fechada-$a2_os_fechada)/($a3_os_fechada-$a4_os_fechada));
    
  }

  if ($a3_os_aberto-$a4_os_aberto == 0) {
    $a_os_aberto = 0;
  }
  else{
$a_os_aberto=(($a1_os_aberto-$a2_os_aberto)/($a3_os_aberto-$a4_os_aberto));
    
  }


$b1_os_fechada= $mes*$v_os_fechada_sum;
$b1_os_aberto= $mes*$v_os_aberto_sum;
$b2_os_fechada=$v_os_fechada_sum*$v_mes;
$b2_os_aberto=$v_os_aberto_sum*$v_mes;
$b3_os_fechada=$mes*$v_os_fechada_prod;
$b3_os_aberto=$mes*$v_os_aberto_prod;
$b4_os_fechada =  $v_os_fechada_sum*$v_os_fechada_sum;
$b4_os_aberto =  $v_os_aberto_sum*$v_os_aberto_sum;
  if ($b3_os_fechada-$b4_os_fechada == 0) {
    $b_os_fechado = 0;
  }
  else{
$b_os_fechado=(($b1_os_fechada-$b2_os_fechada)/($b3_os_fechada-$b4_os_fechada));
    
  }


  if ($b3_os_aberto-$b4_os_aberto == 0) {
    $b_os_aberto = 0;
  }
  else{
$b_os_aberto=(($b1_os_aberto-$b2_os_aberto)/($b3_os_aberto-$b4_os_aberto));
    
  }

  if ($b_os_fechado == 0) {
    $osc_fechada = 0;
  }
  else{
  $osc_fechada=(($mes+1)-$a_os_fechado)/$b_os_fechado;
    
  }

  if ($b_os_aberto == 0) {
    $osc_aberto = 0;
  }
  else{
  $osc_aberto=(($mes+1)-$a_os_aberto)/$b_os_aberto;
    
  }

$osc_fechada=abs($c_fechada);
$osc_aberto=abs($c_aberto);

$query_table = " $query AND  (MONTH(os.date_end) != $month OR os.date_end IS NULL) AND  group by os.id ";

$query = " $query group by os.id ";

 
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[11] AND  MONTH(os.reg_date) = $months_query[11] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_01=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[10] AND  MONTH(os.reg_date) = $months_query[10] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_02=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[9] AND  MONTH(os.reg_date) = $months_query[9] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_03=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[8] AND  MONTH(os.reg_date) = $months_query[8] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_04=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[7] AND  MONTH(os.reg_date) = $months_query[7] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_05=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[6] AND  MONTH(os.reg_date) = $months_query[6] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_06=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[5] AND  MONTH(os.reg_date) = $months_query[5] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_07=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[4] AND  MONTH(os.reg_date) = $months_query[4] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_08=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[3] AND  MONTH(os.reg_date) = $months_query[3] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_09=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[2] AND  MONTH(os.reg_date) = $months_query[2] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_10=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[1] AND  MONTH(os.reg_date) = $months_query[1] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_11=($status_kpi/$mp_cout)*100;  
}
}
$query_kpi = "SELECT os_status.status,equipamento.codigo,os.reg_date,os.id,os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome AS 'equipamento_grupo',os.id_unidade FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN os_status ON os_status.id = os.id_status  INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN instituicao_area ON instituicao.id = instituicao_area.id_unidade INNER JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id WHERE os.id_status IN(1,2,3,4,5,6) AND  YEAR(os.reg_date) = $years_query[0] AND  MONTH(os.reg_date) = $months_query[0] group by os.id  ";

$mp_cout = 0;
$status_kpi = 0;
if ($stmt = $conn->prepare($query_kpi)) {
    $stmt->execute();
    $stmt->bind_result($os_status,$codigo,$reg_date,$id,$status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm,$id_unidade);


while ($stmt->fetch()) {   $mp_cout=$mp_cout+1;
      $mes_open = date("m", strtotime($reg_date));
          $mes_close =  date("m", strtotime($date_end_cm));
    if($status == 6 &&  $mes_open == $mes_close ){$status_kpi=$status_kpi+1; }
     $kpi_12=($status_kpi/$mp_cout)*100;  
}
}


?>
 <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
      // Some raw data (not necessarily accurate)
      var data = google.visualization.arrayToDataTable([
        ['Aberto', 'Aberto',{ role: 'annotation'} ,'ANDamento', { role: 'annotation'} ,'Solicitado', { role: 'annotation'} ,'Concluido', { role: 'annotation'} , 'Tendência'],
        ['<?php printf($years_query[11]."/".$months_query[11]); ?> ',  <?php printf($jan_01); ?>,<?php printf($jan_01); ?>,      <?php printf($jan_02); ?>,<?php printf($jan_02); ?>,          <?php printf($os_jan); ?>,<?php printf($os_jan); ?>,                <?php printf($jan_06); ?>, <?php printf($jan_06); ?>,  <?php $jan=($jan_01+$jan_02+$jan_03+$jan_06)/4; printf($jan); ?>],
        ['<?php printf($years_query[10]."/".$months_query[10]); ?> ',  <?php printf($fev_01); ?>,<?php printf($fev_01); ?>,      <?php printf($fev_02); ?>,<?php printf($fev_02); ?>,          <?php printf($os_fev); ?>,<?php printf($os_fev); ?>,                <?php printf($fev_06); ?>, <?php printf($fev_06); ?>,  <?php $fev=($fev_01+$fev_02+$fev_03+$fev_06)/4; printf($fev); ?>],
        ['<?php printf($years_query[9]."/".$months_query[9]); ?> ',  <?php printf($mar_01); ?>,<?php printf($mar_01); ?>,      <?php printf($mar_02); ?>,<?php printf($mar_02); ?>,          <?php printf($os_mar); ?>,<?php printf($os_mar); ?>,                <?php printf($mar_06); ?>, <?php printf($mar_06); ?>,  <?php $mar=($mar_01+$mar_02+$mar_03+$mar_06)/4; printf($mar); ?>],
        ['<?php printf($years_query[8]."/".$months_query[8]); ?> ',  <?php printf($abr_01); ?>,<?php printf($abr_01); ?>,      <?php printf($abr_02); ?>,<?php printf($abr_02); ?>,          <?php printf($os_abr); ?>,<?php printf($os_abr); ?>,                <?php printf($abr_06); ?>, <?php printf($abr_06); ?>, <?php $abr=($abr_01+$jabr_02+$abr_03+$abr_06)/4; printf($abr); ?>],
        ['<?php printf($years_query[7]."/".$months_query[7]); ?> ',  <?php printf($mai_01); ?>,<?php printf($mai_01); ?>,      <?php printf($mai_02); ?>,<?php printf($mai_02); ?>,          <?php printf($os_mai); ?>,<?php printf($os_mai); ?>,                <?php printf($mai_06); ?>, <?php printf($mai_06); ?>,  <?php $mai=($mai_01+$mai_02+$mai_03+$mai_06)/4; printf($mai); ?>],
        ['<?php printf($years_query[6]."/".$months_query[6]); ?> ',  <?php printf($jun_01); ?>,<?php printf($jun_01); ?>,      <?php printf($jun_02); ?>,<?php printf($jun_02); ?>,          <?php printf($os_jun); ?>,<?php printf($os_jun); ?>,                <?php printf($jun_06); ?>, <?php printf($jun_06); ?>,  <?php $jun=($jun_01+$jun_02+$jun_03+$jun_06)/4; printf($jun); ?>],
        ['<?php printf($years_query[5]."/".$months_query[5]); ?> ',  <?php printf($jul_01); ?>,<?php printf($jul_01); ?>,      <?php printf($jul_02); ?>,<?php printf($jul_02); ?>,          <?php printf($os_jul); ?>,<?php printf($os_jul); ?>,                <?php printf($jul_06); ?>,  <?php printf($jul_06); ?>, <?php $jul=($jul_01+$jul_02+$jul_03+$jul_06)/4; printf($jul); ?>],
        ['<?php printf($years_query[4]."/".$months_query[4]); ?> ',  <?php printf($ago_01); ?>,<?php printf($ago_01); ?>,      <?php printf($ago_02); ?>,<?php printf($ago_02); ?>,          <?php printf($os_ago); ?>,<?php printf($os_ago); ?>,                <?php printf($ago_06); ?>, <?php printf($ago_06); ?>,  <?php $ago=($ago_01+$ago_02+$ago_03+$ago_06)/4; printf($ago); ?>],
        ['<?php printf($years_query[3]."/".$months_query[3]); ?>',  <?php printf($set_01); ?>,<?php printf($set_01); ?>,      <?php printf($set_02); ?>,<?php printf($set_02); ?>,          <?php printf($os_set); ?>,<?php printf($os_set); ?>,                <?php printf($set_06); ?>, <?php printf($set_06); ?>,  <?php $set=($set_01+$set_02+$set_03+$set_06)/4; printf($set); ?>],
        ['<?php printf($years_query[2]."/".$months_query[2]); ?> ',  <?php printf($out_01); ?>,<?php printf($out_01); ?>,      <?php printf($out_02); ?>,<?php printf($out_02); ?>,          <?php printf($os_out); ?>,<?php printf($os_out); ?>,                <?php printf($out_06); ?>, <?php printf($out_06); ?>,  <?php $out=($out_01+$out_02+$out_03+$jout_06)/4; printf($out); ?>],
        ['<?php printf($years_query[1]."/".$months_query[1]); ?>',  <?php printf($nov_01); ?>,<?php printf($nov_01); ?>,      <?php printf($nov_02); ?>,<?php printf($nov_02); ?>,          <?php printf($os_nov); ?>,<?php printf($os_nov); ?>,                <?php printf($nov_06); ?>, <?php printf($nov_06); ?>,  <?php $nov=($nov_01+$nov02+$nov_03+$nov_06)/4; printf($nov); ?>],
        ['<?php printf($years_query[0]."/".$months_query[0]); ?> ',  <?php printf($dez_01); ?>,<?php printf($dez_01); ?>,      <?php printf($dez_02); ?>,<?php printf($dez_02); ?>,          <?php printf($os_dez); ?>,<?php printf($os_dez); ?>,                <?php printf($dez_06); ?>, <?php printf($dez_06); ?>,  <?php $dez=($dez_01+$dez_02+$dez_03+$dez_06)/4; printf($dez); ?>]
      ]);

      var options = {
        title : 'Indicador de Manutenção Corretiva',
        vAxis: {title: 'O.S'},
        hAxis: {title: 'Mês',
          slantedText: true, // Isso permite o texto inclinado no eixo
          slantedTextAngle: 45}, // Define o ângulo de inclinação},
        seriesType: 'bars',
        legend: { position: 'top', maxLines: 3 },
        series: {4: {type: 'line'}},
        colors: ['blue', 'red', 'orange', 'green'], // Defina as cores das barras aqui
        bar: {  groupWidth: '80%', // largura do grupo
          gapWidth: '25%'
        }, 
        bars: { 
          width: '100%', // largura das barras
          lineWidth: 1, 
          barSpacing: '25%', // espaço entre as barras
          groupSpacing: '300%' // espaço entre os grupos
        },
        annotations: {
          boxStyle: {
            // Color of the box outline.
            stroke: '#888',
            // Thickness of the box outline.
            strokeWidth: 1,
            // x-radius of the corner curvature.
            rx: 10,
            // y-radius of the corner curvature.
            ry: 10,
            // Attributes for linear gradient fill.
            gradient: {
              // Start color for gradient.
              color1: '#fbf6a7',
              // Finish color for gradient.
              color2: '#33b679',
              // WHERE on the boundary to start AND
              // end the color1/color2 gradient,
              // relative to the upper left corner
              // of the boundary.
              x1: '0%', y1: '0%',
              x2: '100%', y2: '100%',
              // If true, the boundary for x1,
              // y1, x2, AND y2 is the box. If
              // false, it's the entire chart.
              useObjectBoundingBoxUnits: true
            }
          }
        }
      };
      

    
      var chart = new google.visualization.ComboChart(document.getElementById('chart_div2'));

    google.visualization.events.addListener(chart, 'ready', function () {
            chart_div2.innerHTML = '<img id="chart" src=' + chart.getImageURI() + '>';
         

        });
        chart.draw(data, options);
      }

    </script>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="dashboard_graph">

      <div class="row x_title">


      </div>

      <div class="col-md-12 col-sm-12 ">
        <div id="chart_div2" style="width:1000px; height: 500px;">  </div>

      </div>
     
      <div class="clearfix"></div>
    </div>
  </div>

</div>
<br />

<div class="row">


  <div class="col-md-4 col-sm-4 ">
    <div class="x_panel tile fixed_height_320">
      <div class="x_title">

        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="mc">


      </div>
    </div>
  </div>

  <div class="col-md-4 col-sm-4 ">
    <div class="x_panel tile fixed_height_320 overflow_hidden">
      <div class="x_title">

        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="mp">

      </div>
    </div>
  </div>


  <div class="col-md-4 col-sm-4 ">
    <div class="x_panel tile fixed_height_320">
      <div class="x_title">


        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="cal">


      </div>
    </div>

  </div>


  <div class="col-md-6 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <p>Manutenção Preventiva</p>

        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="table_div">


      </div>
    </div>

  </div>
  <div class="col-md-6 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <p>Manutenção Corretiva</p>

        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="table_div2">


      </div>
    </div>

  </div>
  <div class="col-md-4 col-sm-4 ">
    <div class="x_panel tile fixed_height_320">
      <div class="x_title">


        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="container">


      </div>
    </div>

  </div>


<div class="col-md-3  widget widget_tally_box">
                        <div class="x_panel ui-ribbon-container fixed_height_390">
                          <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                             Backlog
                            </div>
                          </div>
                          <div class="x_title">
                            <h2>M.P</h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            

                           
                            <p>Manutenção preventiva Aberta</p><?php 
                          $stmt = $conn->prepare(" SELECT time_format( SEC_TO_TIME( SUM( TIME_TO_SEC( time_ms ) ) ),'%H:%i:%s') AS 'total_horas' FROM maintenance_preventive LEFT JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine WHERE id_status = 1 "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?>

                            <div class="divider"></div>
                            <p>Manutenção preventiva Atrasada</p><?php 
                          $stmt = $conn->prepare(" SELECT time_format( SEC_TO_TIME( SUM( TIME_TO_SEC( time_ms ) ) ),'%H:%i:%s') AS 'total_horas' FROM maintenance_preventive LEFT JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine WHERE id_status = 2 "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?>

                            <div class="divider"></div>
                            

                       

                          </div>
                        </div>
                      </div>


<div class="col-md-3  widget widget_tally_box">
                        <div class="x_panel ui-ribbon-container fixed_height_390">
                          <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                             Backlog
                            </div>
                          </div>
                          <div class="x_title">
                            <h2>O.S</h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            

                           
                            <p>O.S Aberta</p><?php 
                          $stmt = $conn->prepare(" SELECT time_format( SEC_TO_TIME( SUM( TIME_TO_SEC( time_backlog ) ) ),'%H:%i:%s') AS 'total_horas' FROM os  WHERE id_status = 1  "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?>

                           <div class="divider"></div>
                            <p>O.S ANDamento</p><?php 
                          $stmt = $conn->prepare(" SELECT time_format( SEC_TO_TIME( SUM( TIME_TO_SEC( time_backlog ) ) ),'%H:%i:%s') AS 'total_horas' FROM os  WHERE id_status = 2  "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          } 
                          ?>

                            <div class="divider"></div>
                              <p>O.S Abertura</p><?php 
                          $stmt = $conn->prepare(" SELECT  time_format( SEC_TO_TIME( SUM( TIME_TO_SEC( os_backlog.time_backlog ) ) ),'%H:%i:%s') AS 'total_horas' FROM os  LEFT JOIN os_backlog ON os_backlog.id = 1 WHERE os.id_status = 1  "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?>

                           <div class="divider"></div>
                            <p>O.S Fechamento</p><?php 
                          $stmt = $conn->prepare(" SELECT  time_format( SEC_TO_TIME( SUM( TIME_TO_SEC( os_backlog.time_backlog ) ) ),'%H:%i:%s') AS 'total_horas' FROM os  LEFT JOIN os_backlog ON os_backlog.id = 2 WHERE os.id_status = 2  "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          } 
                          ?>

                            <div class="divider"></div>
                            
                            

                       

                          </div>
                        </div>
                      </div>






  <!-- /page content -->

</div>
</div>



<?php
  date_default_timezone_set('America/Sao_Paulo');
  $today = date("Y-m-d");
  $read_open = 1;
  
  $query = "SELECT id, origem, ref, data, read_open FROM notif WHERE read_open = 1";
  
  $resultados = $conn->query($query);
  $notifications = array();
  
  while ($row = mysqli_fetch_assoc($resultados)) {
    $notifications[] = $row;
  }
  
 ?>

<script>
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
        // Após o registro do Service Worker, mostrar notificações
        <?php foreach ($notifications as $notification): ?>
        var title = "<?php echo $notification['origem']; ?>";
        var options = {
          body: "<?php echo $notification['ref']; ?>"
          // Adicione mais opções de notificação conforme necessário
        };
        showNotification(title, options);
        <?php endforeach; ?>
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  function showNotification(title, options) {
    if (Notification.permission === "granted") {
      var notification = new Notification(title, options);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification(title, options);
        }
      });
    }
  }
</script>

<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>
<!-- /page content -->

<!-- /page content -->
