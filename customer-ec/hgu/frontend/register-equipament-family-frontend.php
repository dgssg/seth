<?php
include("database/database.php");

?>


<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>

<div class="right_col" role="main">

    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>Familia Equipamento</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                    <div class="input-group">

                        <span class="input-group-btn">

                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2>Menu</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <a class="btn btn-app" href="register-equipament-family">
                    <i class="glyphicon glyphicon-list-alt"></i> Familias
                </a>
                <a class="btn btn-app" href="register-equipament-family-register">
                    <i class="glyphicon glyphicon-plus"></i> Cadastro
                </a>
                <a class="btn btn-app" href="backend/register-equipament-family-pop.php">
                    <i class="glyphicon glyphicon-refresh"></i> Atualização POP
                </a>
                <a class="btn btn-app" href="report-equipament-family-out-equipamento" target="_blank">
                    <i class="glyphicon glyphicon-file"></i> Sem Equipamento
                </a>
             <a class="btn btn-app" href="backend/register-equipament-family-anvisa.php">
                    <i class="glyphicon glyphicon-refresh"></i> Atualização Anvisa
                </a>


            </div>
        </div>





        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Filtro</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Settings 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="userstable_filter"
                            style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;">
                        </div>


                    </div>
                </div>
            </div>
        </div>





        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista <small>de Familia de Equipamento</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Settings 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <div class="col-md-11 col-sm-11 ">
                            <div class="x_panel">

                                <div class="x_content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">

                                                <table id="datatable"
                                                    class="table table-striped table-bordered dt-responsive nowrap"
                                                    style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Grupo</th>
                                                            <th>Familia</th>
                                                            <th>Fabricante</th>
                                                            <th>Modelo</th>
                                                            <th>Anvisa</th>
                                                            <th>Imagem</th>
                                                            <th>Manual Usuário</th>
                                                            <th>Manual Tecnico</th>
                                                            <th>Tecnovigilância</th>
<th>Classificação de Risco</th>
<th>Fornecedor</th>
                                                            <th>Ação</th>

                                                        </tr>
                                                    </thead>


                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </div>
</div>
</div>







</div>
</div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script language="JavaScript">
$(document).ready(function() {
    $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );
    // Define a URL da API que retorna os dados da query em formato JSON
    const apiUrl = 'table/table-search-equipamento-family.php';

    // Usa a função fetch() para obter os dados da API em formato JSON
    fetch(apiUrl)
        .then(response => response.json())
  .then(data => {
    const novosDados = data.map(item => {
      let badgeHTMLmanual_use = '';
      let badgeHTMLmanual_tec= '';
      if (item.manual_use != null) {
        badgeHTMLmanual_use = "<span class='badge badge-success'>OK</span>";
      }
      if (item.manual_tec != null) {
        badgeHTMLmanual_tec = "<span class='badge badge-success'>OK</span>";
      }
     
      return [
        ``,
                item.grupo,
                item.familia,
                item.fabricante,
                item.modelo,
                item.anvisa,
                item.img == null ? "Não" : "Sim",
                item.manual_use == null ? "Não" : "Sim",
                item.manual_tec == null ? "Não" : "Sim",
                ``,
                 item.nome,
                  item.empresa,
                `<a class="btn btn-app" href="register-equipament-family-edit?id=${item.id}" onclick="new PNotify({
    title: 'Editar',
    text: 'Abrindo Edição',
    type: 'success',
    styling: 'bootstrap3'
  });">
    <i class="fa fa-edit"></i> Editar
  </a>
  <a class="btn btn-app" onclick="Swal.fire({
    title: 'Tem certeza?',
    text: 'Você não será capaz de reverter isso!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sim, Deletar!'
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire(
        'Deletando!',
        'Seu arquivo será excluído.',
        'success'
      ),
      window.location = 'backend/register-equipament-family-trash-backend?id=${item.id}';
    }
  })">
    <i class="fa fa-trash"></i> Excluir
  </a>

    <a class="btn btn-app" href="dropzone/manual/${item.manual_use}" target="_blank" onclick="new PNotify({
      title: 'Ficha',
      text: 'Abrir Ficha',
      type: 'success',
      styling: 'bootstrap3'
    });">
      <i class="fa fa-file"></i> Manual Usuário
${badgeHTMLmanual_use}
    </a>
  
    <a class="btn btn-app" href="dropzone/manual/${item.manual_tec}" target="_blank" onclick="new PNotify({
      title: 'Prontuario',
      text: 'Abrir Prontuario',
      type: 'success',
      styling: 'bootstrap3'
    });">
      <i class="fa fa-file-o"></i> Manual Tecnico
${badgeHTMLmanual_tec}
    </a>
    <a class="btn btn-app" href="report-equipament-family-in-equipamento?id=${item.id}"
                      target="_blank">
                      <i class="glyphicon glyphicon-file"></i> Equipamentos
                  </a>
    <a class="btn btn-app" href="register-equipament-family-assessment?id=${item.id} "
                      onclick="new PNotify({
																title: 'Novo',
																text: 'Novo Parecer!',
																type: 'info',
																styling: 'bootstrap3'
														});">
                      <i class="fa fa-file-pdf-o"></i> Novo Parecer
                  </a>
    
    `
      ];
      
    });

    
            // Adiciona as novas linhas ao DataTables e desenha a tabela
            $('#datatable').DataTable().rows.add(novosDados).draw();


            // Cria os filtros após a tabela ser populada
            $('#datatable').DataTable().columns([1, 2, 3, 4, 5, 6, 7, 8,9,10,11]).every(function(d) {
                var column = this;
              var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
        });
});


</script>