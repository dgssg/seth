<?php
// Defina a chave da API DeepSeek
$api_key = "<DeepSeek API Key>"; // Substitua pela sua chave de API DeepSeek

// Defina a URL da API
$url = "https://api.deepseek.com/chat/completions";

// Dados para enviar no corpo da requisição
$data = [
    "model" => "deepseek-chat", // Nome do modelo
    "messages" => [
        ["role" => "system", "content" => "You are a helpful assistant."],
        ["role" => "user", "content" => "Hello!"]
    ],
    "stream" => false
];

// Inicializa o cURL
$ch = curl_init();

// Configurações do cURL
curl_setopt($ch, CURLOPT_URL, $url); // URL da API
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Retorna a resposta como string
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json', // Cabeçalho Content-Type
    'Authorization: Bearer ' . $api_key // Cabeçalho de autenticação com chave da API
]);
curl_setopt($ch, CURLOPT_POST, true); // Método POST
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); // Corpo da requisição em JSON

// Executa a requisição e captura a resposta
$response = curl_exec($ch);

// Verifica se ocorreu erro na requisição
if ($response === false) {
    echo "Erro cURL: " . curl_error($ch);
} else {
    // Exibe a resposta da API
    echo "Resposta da API: " . $response;
}

// Fecha a sessão cURL
curl_close($ch);
?>
