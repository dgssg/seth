<?php
include_once("database/database.php");
$term = $_GET['term'];
$sql = "SELECT * FROM lighthouse WHERE ativo = 0 AND (key_1 LIKE ? OR key_2 LIKE ? OR key_3 LIKE ?)";
$stmt = $conn->prepare($sql);
$likeTerm = "%".$term."%";
$stmt->bind_param("sss", $likeTerm, $likeTerm, $likeTerm);
$stmt->execute();
$result = $stmt->get_result();

$suggestions = [];
while ($row = $result->fetch_assoc()) {
    $suggestions[] = [
        'title' => $row['title'],
        'file' => $row['file']
    ];
}

echo json_encode($suggestions);

$conn->close();
?>
