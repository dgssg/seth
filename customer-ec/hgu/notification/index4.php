<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Demo Push Notification System with PHP & MySQL</title>
    <script src="notification2.js"></script>
</head>
<body>
    <div class="container">      
        <h2>Example: Push Notification System with PHP & MySQL</h2>
        <p>Abra o console do navegador para ver a notificação sendo exibida.</p>
    </div>
    <div class="insert-post-ads1" style="margin-top:20px;">
        
    </div>
    <script>
        $(document).ready(function() {
            showNotification();
            setInterval(function(){ showNotification(); }, 20000);
        });
        function showNotification() {	
            if (!Notification) {
                $('body').append('<h4 style="color:red">*Browser does not support Web Notification</h4>');
                return;
            }
            if (Notification.permission !== "granted") {		
                Notification.requestPermission();
            } else {		
                $.ajax({
                    url : "notification.php",
                    type: "POST",
                    success: function(data, textStatus, jqXHR) {
                        var data = jQuery.parseJSON(data);
                        if(data.result == true) {
                            var data_notif = data.notif;
                            for (var i = data_notif.length - 1; i >= 0; i--) {
                                var theurl = data_notif[i]['url'];
                                var notifikasi = new Notification(data_notif[i]['title'], {
                                    icon: data_notif[i]['icon'],
                                    body: data_notif[i]['msg'],
                                });
                                notifikasi.onclick = function () {
                                    window.open(theurl); 
                                    notifikasi.close();     
                                };
                                setTimeout(function(){
                                    notifikasi.close();
                                }, 5000);
                            };
                        } else {
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)	{}
                }); 
            }
        };
    </script>
</body>
</html>
