<?php
	include("../database/database.php");
	$codigoget= $_GET['id'];
	// Calcula a pontuação
	$query = "SELECT 
	SUM(CASE WHEN ur.is_correct = 1 THEN 1 ELSE 0 END) AS correct_answers,
	COUNT(*) AS total_questions,
	IFNULL((SUM(CASE WHEN ur.is_correct = 1 THEN 1 ELSE 0 END) / NULLIF(COUNT(*), 0)) * 100, 0) AS score_percentage
	FROM user_training_responses ur
	WHERE ur.id_attempt = ?";
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->bind_param("s", $codigoget);
		$stmt->execute();
		$stmt->bind_result($correto, $total, $score);
		$stmt->fetch();
		$stmt->close();
	}
	
	// Atualiza o score e status
	$status = "completed";
	$stmt = $conn->prepare("UPDATE user_training_attempts SET score= ?, status= ? WHERE id= ?");
	$stmt->bind_param("sss", $score, $status, $codigoget);
	$stmt->execute();
	$stmt->close();
	
	echo "<script>alert('Atualizado!');document.location='../documentation-graduation-training-feedback'</script>";
?>