<?php

include("../database/database.php");

$codigoget= $_POST['id'];
$nome= $_POST['nome'];
$cnpj= $_POST['cnpj'];
$email= $_POST['email'];
$telefone= $_POST['telefone'];


$cep= $_POST['cep'];
$rua= $_POST['rua'];
$bairro= $_POST['bairro'];
$cidade= $_POST['cidade'];
$estado= $_POST['estado'];
$ibge= $_POST['ibge'];



$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET nome= ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET cnpj= ? WHERE id= ?");
$stmt->bind_param("ss",$cnpj,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET email= ? WHERE id= ?");
$stmt->bind_param("ss",$email,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET telefone= ? WHERE id= ?");
$stmt->bind_param("ss",$telefone,$codigoget);
$execval = $stmt->execute();
$stmt->close();
 

$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET cep= ? WHERE id= ?");
$stmt->bind_param("ss",$cep,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET adress= ? WHERE id= ?");
$stmt->bind_param("ss",$rua,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET bairro= ? WHERE id= ?");
$stmt->bind_param("ss",$bairro,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET cidade= ? WHERE id= ?");
$stmt->bind_param("ss",$cidade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET estado= ? WHERE id= ?");
$stmt->bind_param("ss",$estado,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_parameter_customer SET ibge= ? WHERE id= ?");
$stmt->bind_param("ss",$ibge,$codigoget);
$execval = $stmt->execute();
$stmt->close();
 

header('Location: ../calibration-parameter');
?>