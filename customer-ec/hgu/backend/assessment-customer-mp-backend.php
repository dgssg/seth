<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;
	
	require '../../../framework/PHPMailer/src/PHPMailer.php';
	require '../../../framework/PHPMailer/src/SMTP.php';
	require '../../../framework/PHPMailer/src/Exception.php';
	
include("../database/database.php");
	session_start();
	
	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$setor = $_SESSION['setor'];
	$usuario = $_SESSION['id_usuario'];
	
$assessment=$_POST['assessment'];
$number_assessment=$_POST['number_assessment'];
$data_assessment=$_POST['data_assessment'];
$id_customer=$_POST['id_customer'];
$id_service=$_POST['customer_service'];
$obs=$_POST['obs'];
$count=0;
$id_mod=1;

	$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_customer = ? WHERE id= ?");
	$stmt->bind_param("ss",$id_customer,$number_assessment);
	$execval = $stmt->execute();
	
$stmt = $conn->prepare("INSERT INTO customer_assessment (assessment, number_assessment, data_assessment, id_customer, id_service, obs) VALUES (?, ?,?, ?, ?, ?)");
$stmt->bind_param("ssssss",$assessment,$number_assessment,$data_assessment,$id_customer,$id_service,$obs);
$execval = $stmt->execute();
$last_id = $conn->insert_id;
$stmt->close();

$query = "SELECT  id FROM customer_question WHERE id_customer_service  = $id_service ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
	$stmt->execute();
	$stmt->bind_result($id);
	while ($stmt->fetch()) {
	  $codigos[]  = $id;
    $count=$count+1;

	}
}

//echo implode( ', ', $codigos );

for ($i=0; $i < $count ; $i++) {
 $id_question=$codigos[$i];
 $avaliable=$_POST[$codigos[$i]];


$stmt = $conn->prepare("INSERT INTO customer_assessment_question (id_assessment, id_question, avaliable) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$last_id,$id_question,$avaliable);
$execval = $stmt->execute();
$stmt->close();
}
	
	$query="SELECT assessment FROM customer WHERE id like '$id_customer'";
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($assessment);
	}
	$stmt->close();
	
if($assessment == "0"){
	
	$stmt = $conn->prepare("UPDATE maintenance_preventive SET assessment = 0 WHERE id= ?");
	$stmt->bind_param("s",$number_assessment);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("INSERT INTO assessment_send (id_mod, id_number, id_usuario) VALUES (?,?,?)");
	$stmt->bind_param("sss",$id_mod,$number_assessment,$usuario);
	$execval = $stmt->execute();
	$last_id = $conn->insert_id;
	$stmt->close();
	
	$query="SELECT email FROM usuario WHERE id like '$usuario'";
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($email_usuario);
	}
	$stmt->close();
	
	$query = " SELECT customer.id,customer_assessment.id,customer.empresa,customer_assessment.reg_date, customer_question.question, customer_assessment_question.id_question,customer_assessment_question.avaliable, customer_service.service, customer_assessment.id_service,customer_assessment.assessment,customer_assessment.number_assessment, customer_assessment.data_assessment, customer_assessment.obs FROM customer_assessment LEFT JOIN customer_service ON customer_service.id = customer_assessment.id_service LEFT JOIN customer_assessment_question ON customer_assessment_question.id_assessment = customer_assessment.id LEFT JOIN customer_question ON customer_question.id = customer_assessment_question.id_question LEFT JOIN customer ON customer.id = customer_assessment.id_customer WHERE customer_assessment.id = $insert_id ";
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($customer,$id,$empresa,$reg_date,$question,$id_question,$avaliable,$service,$id_service,$assessment,$number_assessment,$data_assessment,$obs);
		
		while ($stmt->fetch()) { 
			
			switch ($avaliable) {
				case '1':
					$avaliable="Bom";
					break;
				case '2':
					$avaliable="Regular";
					break;
				case '3':
					$avaliable="Ruim";
					break; }
			switch ($assessment) {
				case '1':
					$assessment="Ordem de Serviço";
					$quer_assment="1";
					break;
				case '2':
					$assessment="Manutenção Preventiva";
					$quer_assment="2";
					break;
				case '3':
					$assessment="Calibração";
					$quer_assment="3";
					break;
				
				
			}
			$descricao="$descricao \n customer: $empresa\nFornecimento: $service \n Data avaliação: $reg_date\n Data do Serviço avaliado:$data_assessment \n Tipo do serviço:$assessment\nNumero do serviço:$number_assessment\n Avaliação:$question\n Conceito:$avaliable\nObservação:$obs \n";
			
		}
	}
	
	switch ($quer_assment) {
		case '1':
			$query = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM os INNER JOIN equipamento ON equipamento.id = os.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE os.id like  '$number_assessment'";
			break;
		case '2':
			$query = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM maintenance_preventive INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE maintenance_preventive.id  like '$number_assessment'";
			break;
		case '3':
			$query = "SELECT instituicao.instituicao, instituicao.cnpj,instituicao.endereco,instituicao.cep,instituicao.bairro,instituicao.cidade  FROM calibration_preventive INNER JOIN calibration_routine ON calibration_routine.id = calibration_preventive.id_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade  WHERE calibration_preventive.id  like '$number_assessment'";
			break;
		
		
	}
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($unidade, $unidade_cnpj, $unidade_adress, $unidade_cep, $unidade_bairro, $unidade_city);
		
		
		
		while ($stmt->fetch()) {
			
		}
	}
	$query = "SELECT id, empresa, cnpj, seguimento, adress, city, state, cep, contato, email, telefone_1, telefone_2, observacao, bairro, ibge, reg_date, upgrade,trash,assessment FROM customer WHERE id like '$id_customer'";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($id_customer, $empresa, $cnpj, $seguimento, $adress, $city, $state, $cep, $contato, $email, $telefone_1, $telefone_2, $observacao, $bairro, $ibge, $reg_date, $upgrade,$trash,$assessment);
		
		
		while ($stmt->fetch()) {
			
		}
	}
	$descricao=" 
Unidade: $unidade\n
CNPJ: $unidade_cnpj\n
Endereço: $unidade_adress $unidade_cep $unidade_bairro $unidade_city\n

Nome: $usuariologado
Email: $email_usuario
-------------------------------------------------------------------
\n\n $descricao \n\n\n\n  

Enviado por: $usuariologado
Contato para retorno: $email_usuario
-------------------------------------------------------------------
Mensagem automática enviada via sistema SETH. Não responda. MK Sistemas Biomedicos";
	$titulo="Avaliação de customer $number_assessment";
	// send email
 

	
}
		$mail = new PHPMailer(true);
	
	try {
		// Configurações do servidor SMTP
		$mail->isSMTP();
		$mail->Host       = 'seth.mksistemasbiomedicos.com.br'; // Substitua pelo host do seu provedor
		$mail->SMTPAuth   = true;
		$mail->Username   = 'seth@seth.mksistemasbiomedicos.com.br'; // Substitua pelo seu endereço de e-mail
		$mail->Password   = 'mks@2024'; // Substitua pela sua senha
		$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Use 'tls' ou 'ssl' se necessário
		$mail->Port       = 587; // Porta do servidor SMTP
		
		// Configurações do e-mail
		$mail->setFrom('seth@seth.mksistemasbiomedicos.com.br', $usuariologado);
		$mail->addAddress($email, $empresa); // Substitua pelo endereço do destinatário
		$mail->Subject = $titulo;
		$mail->Body    = $descricao;
		
		// Enviar e-mail
		$mail->send();
		
		
	 		echo "<script>document.location='../maintenance-preventive-shut-down?sweet_salve=1&routine=$number_assessment'</script>";

		
	} catch (Exception $e) {
		
	 
		echo "<script>document.location='../maintenance-preventive-shut-down?sweet_delet=1&routine=$number_assessment'</script>";

	}
	
	
?>

 