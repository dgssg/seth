<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="content-language" content="pt-br" />
  <meta http-equiv="refresh" content="7200" />
  <meta name="viewport" content="width=device-width, initial-scale=1">




  <title>SETH</title>

  <link rel='stylesheet' type='text/css' href='../../framework/viewer/./css/style.css' />
  <link rel='stylesheet' type='text/css' href='../../framework/viewer/./css/jquery-ui-1.8.11.custom.css' />

  <link href="../../framework/viewer/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../framework/viewer/bootstrap/css/cus-icons.css" rel="stylesheet">

  <link href="../../framework/viewer/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
  <link href="../../framework/viewer/bootstrap/css/bootstrap_custom.css" rel="stylesheet">
  <link href="../../framework/viewer/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet">
  <link href="../../framework/viewer/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../../framework/viewer/css/jquery.countdown.css">
  <link rel="stylesheet" href="../../framework/viewer/bootstrap/css/animate.css">

  <script type="text/javascript">
  var CI_ROOT = '../../framework/viewer/index.php';
  </script>
  <script type='text/javascript' src='../../framework/viewer/./js/jquery-1.11.1.min.js'></script>

  <script type='text/javascript' src='../../framework/viewer/./js/jquery.blockUI.js'></script>
  <script type='text/javascript' src='../../framework/viewer/./js/about.js'></script>



  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="../../framework/viewer/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script>
window.onload = function () {
  JsBarcode(".barcode").init();
}
</script>

<body>

  <div>




    <link rel="stylesheet" href="../../framework/viewer/css/pdf.css">







    <div id="view_content">


      <div class="formulario">



        <div >


          <div style="padding-bottom: 10px;">
            <table width="100%" style="vertical-align: bottom;">
              <tr>
                <td align="left"><img src="dropzone/logo/<?php printf($logo)?>" style="width:25%"/></td>
                                                  <td>
                                                       <strong  align="center"> <center><h4>      Manutenção Preventiva </h4></center>  <strong><br> 
<strong style="font-size:12px;"align="center"> <center><?php printf($area); ?></center></strong>
                                                  </td>
                                          
                                                  <td align="right"><img src="logo/clientelogo.png" style="width:20%"/></td>
              </tr>
            </table>
          </div>
          <div class="conteudo" style="min-height:auto; font-size:8px;">

            <style>
            #table-round-corner {

              margin-top: 0px;
              width: 100%;
              flex-direction: row;
              justify-content: center;
              align-items: center;
              overflow:hidden;
              border-collapse:separate;
              border: solid #ccc 1px;
                 /*-webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px; */
            }



            </style>  
            <?php
              $query = "SELECT assistence FROM tools";
              if ($stmt = $conn->prepare($query)) {
                $stmt->execute();
                $stmt->bind_result($assistence);
                while ($stmt->fetch()) {
                  //printf("%s, %s\n", $solicitante, $equipamento);
                }
              }
            $query = "SELECT maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_before WHERE maintenance_routine.id = '$rotina'";
            if ($stmt = $conn->prepare($query)) {
              $stmt->execute();
              $stmt->bind_result($id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
              while ($stmt->fetch()) {
                //printf("%s, %s\n", $solicitante, $equipamento);
              }
            }


            ?>
            <div class="container">



             <div class="rotina" align="center" ><br /> <strong style="font-size:14px;" align="center"> <center>Rotina N&ordm; <?php printf($rotina)?></strong><br></center><strong style="font-size:12px;" align="center"> <center>   <img src="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=www.seth.mksistemasbiomedicos.com.br/customer-ec/<?php printf($key); ?>/maintenance-preventive-viwer?id=<?php printf($codigoget); ?>" alt="MK Sistemas Biomedicos" width="70" /></center></strong>  <strong style="font-size:14px;" align="center"> <center> <br> <?php printf($codigo)?></strong></center></div>
              <div class="equipamento"align="center">  <strong style="font-size:10px;"align="center"> <center> Equipamento:</strong><br> <small style="font-size:10px;"><?php printf($nome); ?></small></center></div>
              <div class="fabricante"align="center"><strong style="font-size:10px;"align="center"> <center>Fabricante:</strong><br> <?php printf($fabricante); ?></div>
                <div class="modelo"align="center"><strong style="font-size:10px;"align="center"> <center>Modelo:</strong> <br> <small style="font-size:10px;"></small><?php printf($modelo)?></small></center></div>
                <div class="ns"align="center"><strong style="font-size:10px;"align="center"> <center> N&ordm; Serie:</strong> <br><?php printf($serie); ?></center></div>
                <div class="patrimonio"align="center"> <strong style="font-size:10px;"align="center"> <center>Patrimonio:</strong><br> <?php printf($patrimonio); ?></center></div>
                <div class="periodicidade"align="center"><strong style="font-size:10px;"align="center"> <center>Periodicidade:</strong><br> <?php printf($periodicidade)?>  (dias)</center></div>
                <div class="start"align="center"> <strong style="font-size:10px;"align="center"> <center>Inicio:</strong> <br><?php printf($data_start); ?></center></div>
                <div class="procedimento"align="center"><strong style="font-size:10px;"align="center"> <center>Procedimento 1:</strong><br> <?php printf($procedimento_1); ?></center></div>
                <div class="cod"align="center"><strong style="font-size:10px;"align="center"> <center>Codigo Procedimento:</strong><br> <?php printf($codigo_p); ?></center></div>
                <div class="unidade"align="center"><strong style="font-size:10px;"align="center"> <center>Instituição:</strong> <br><?php printf($instituicao)?></center></div>
                <div class="setor"align="center"><strong style="font-size:10px;"align="center"> <center>Setor</strong><br> <?php printf($area); ?></center></div>
                <div class="area"align="center"><strong style="font-size:10px;"align="center"> <center>Area:</strong> <br><?php printf($setor); ?></center></div>
                <div class="cadastro"align="center"> <strong style="font-size:10px;"align="center"><center>Cadastro:</strong> <br><?php printf($reg_date); ?></center></div>
                <div class="time"align="center"> <strong style="font-size:10px;"align="center"><center>Tempo:</strong><br> <?php printf($time_mp); ?></center></div>
                <div class="programada"align="center"> <strong style="font-size:10px;"align="center"><center>programada:</strong><br> <?php printf($programada); ?></center></div>
                <div class="responsavel"align="center"> <strong style="font-size:10px;"align="center"><center>Responsavel:</strong><br> <?php printf($id_tecnico); ?></center></div>

              </div>
              <!--  <table cellspacing="0"  width="100%" id="table-round-corner">
              <tbody>
              <tr>
              <td align="center"> </td> <td align="center"> <strong style="font-size:12px;"><svg class="barcode"
              jsbarcode-format="auto"
              jsbarcode-value="<?php printf($codigoget)?>"
              jsbarcode-textmargin="0"
              jsbarcode-fontoptions="bold"
              jsbarcode-width="2"
              jsbarcode-height="20"
              jsbarcode-displayValue="false"
              >
            </svg></strong>
            <center>
            <strong style="font-size:12px;">Rotina N&ordm; <?php printf($codigoget)?></strong>	 </center>								    </td>
            <td style="font-size:8px;"> <strong style="font-size:8px;"align="center"> Equipamento:</strong> <small style="font-size:8px;"><?php printf($nome); ?></small></td>

            <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Modelo:</strong>  <small style="font-size:8px;"></small><?php printf($modelo)?></small></td>


            <td style="font-size:8px;"> <strong style="font-size:8px;"align="center"> N&ordm; Serie:</strong> <?php printf($serie); ?></td>
            <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Patrimonio:</strong> <?php printf($patrimonio); ?></td>

            <td style="font-size:8px;" bgcolor="#AAAAAA"> <strong style="font-size:8px;"align="center"> Codigo:<?php printf($codigo); ?></strong> </td>
          </tr>

        </tbody>
      </table>
      <table cellspacing="0"  width="100%" id="table-round-corner">
      <tbody>
      <tr>



      <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Periodicidade:</strong> <?php printf($periodicidade)?>  (dias)</td>


      <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">inicio:</strong> <?php printf($data_start); ?></td>


      <td style="font-size:8px;"> <strong style="font-size:8px;"align="center"></strong> </td>
    </tr>
    <tr>
    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center"></strong></td>



    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Instituição:</strong> <?php printf($instituicao)?></td>
    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Setor</strong> <?php printf($area); ?></td>
    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Area:</strong> <?php printf($setor); ?></td>

    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Area:</strong> <?php printf($setor); ?></td>
    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Cadastro:</strong> <?php printf($reg_date); ?></td>
    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Atualização:</strong> <?php printf($upgrade); ?></td>
    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Procedimento 1:</strong> <?php printf($procedimento_1); ?></td>
    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Codigo:</strong> <?php printf($codigo_p); ?></td>

    <td style="font-size:8px;"> <strong style="font-size:8px;"align="center"></strong> </td>



  </tr>

</tbody>
</table> -->



<div >


<?php
switch ($procedure_mp) {
  case '1':
    $query_procedure="SELECT *FROM maintenance_procedures_itens WHERE id_maintenance_procedures like '$id_maintenance_procedures_before' order by position ASC ";
    $row=1;
    if ($stmt_procedure = $conn_procedure->prepare($query_procedure)) {
      $stmt_procedure->execute();
      $stmt_procedure->bind_result($id,$id_maintenance_procedures,$class,$column,$procedures);

  ?>
  <table cellspacing="0"  style=" border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
    <thead>
      <tr>
        <th style="font-size:8px;">#</th>


        <th style="font-size:8px;">Procedimento</th>

      </tr>
    </thead>
    <ul class="columns" data-columns="2">
      <tbody>
      <?php $dados = 1; while ($stmt_procedure->fetch()) { ?>
                                          <?php if($dados == 0 && $class == 4 ){ 
                                               $base = 30;
                                               $espaco = ((($base - ( $num_caracteres + $num_digitos ) )));
                                               $espaco =  $espaco/2;                                   
                                               for ($a = 1; $a <= $espaco; $a++) {
                                                printf("&nbsp;");

                                             }
                                             
                                             printf("|");
                                            
                                             for ($a = 1; $a <= $espaco; $a++) {
                                              printf("&nbsp;");

                                           }

                                              echo htmlspecialchars_decode($procedures); 
                                 
$num_caracteres = 0;
                                 $num_digitos = 0;
for ($i = 0; $i < strlen($procedures); $i++) {
                                  $caractere = $procedures[$i];
                                  if (ctype_alpha($caractere)) {
                                    $num_caracteres++;
                                  } elseif (ctype_digit($caractere)) {
                                    $num_digitos++;
                                  }
                                }
                                 ?>
                                            <?php $cont = $cont +1 ; }elseif($dados == 0 && $class != 4 ) { ?>
                    
                              
                                              </div>


</td>

</small>
</tr>
<th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

                            <td style="font-size:8px;"  > <small style="font-size:8px;">
                            <div class="input-group input-group-sm  ">

                            
                                               Medida 1
                                               <?php 
                                               for ($i = 1; $i <= $cont; $i++) {
                                                printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                             }
                                             printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                               ?>
                            </div>

<span class="right">


<?php printf("│ │-OK-│ │-NOK-│ │-NA");?> 

</span align="right"> 
</div>


</td>

</small>
</tr>
<th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

<td style="font-size:8px;"  > <small style="font-size:8px;">
<div class="input-group input-group-sm  ">

                                                Medida 2 
                                                <?php 
                                               for ($i = 1; $i <= $cont; $i++) {
                                                printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                             }
                                             printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                               ?>
</div>

<span class="right">


<?php printf("│ │-OK-│ │-NOK-│ │-NA");?> 

</span align="right"> 
</div>


</td>

</small>
</tr>
<th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

<td style="font-size:8px;"  > <small style="font-size:8px;">
<div class="input-group input-group-sm  ">

                                               Medida 3 
                                               <?php 
                                               for ($i = 1; $i <= $cont; $i++) {
                                                printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                             }
                                             printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                               ?>
</div>
                                                
                                                
                                           
                                        </div>
                                        <span class="right">


<?php printf("│ │-OK-│ │-NOK-│ │-NA");?> 

</span align="right"></b>



</td>

</small>
</tr>

<?php $dados = 1; }?>

<?php if($dados == 1 ){ ?>

                        <tr>
                            <th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

                            <td style="font-size:8px;"  > <small style="font-size:8px;">
                                    <center> <b>
                                            <?php if($class==1){echo htmlspecialchars_decode($procedures); $dados = 1;}  ?>
                                        </b> </center> <b>
                                        <?php if($class==2){echo htmlspecialchars_decode($procedures);  ?>
                                    </b>

                                    <span class="right">
                                        <!--   <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> OK</small>

                                                    <input align="right" type="radio" class="flat" name="iCheck2" disabled="disabled" style="font-size:8px;">  <small style="font-size:8px;">NOK</small>

                                                    <input align="right" type="radio" class="flat" name="iCheck3" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> NA
                                                  </small> -->

                                        <b> <?php printf("│ │-OK-│ │-NOK-│ │-NA");?> </b>
                                    </span align="right">
                                    <?php $dados = 1;} if($class==5){?>
                                    <?php echo htmlspecialchars_decode($procedures);?>

                                    <?php $dados = 1;} if($class==3){?>

                                    <div class="input-group input-group-sm  ">
                                        <b> <?php echo htmlspecialchars_decode($procedures);?>
                                            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
                                            <?php echo htmlspecialchars_decode($procedures);?>
                                            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
                                            <?php echo htmlspecialchars_decode($procedures);?>
                                            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
                                        </b>
                                    </div>
                                    <span class="right">

                                        <b> <?php printf("│ │-OK-│ │-NOK-│ │-NA");?> </b>
                                    </span align="right">
                                    <?php $dados = 1; } if($class == 4  ){ $cont = 1; ?>

                                    <div>
                                    <div class="input-group input-group-sm  ">
                               
                                   
                                 <b> <?php 
                                 
                                 printf("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                 echo htmlspecialchars_decode($procedures);
                                 
$num_caracteres = 0;
                                 $num_digitos = 0;
for ($i = 0; $i < strlen($procedures); $i++) {
                                  $caractere = $procedures[$i];
                                  if (ctype_alpha($caractere)) {
                                    $num_caracteres++;
                                  } elseif (ctype_digit($caractere)) {
                                    $num_digitos++;
                                  }
                                }
                                 ?>
                                
       


                                        <?php $dados=0; } }$row=$row+1; 
                                      } }  ?>
                                        </tbody>
                                        </ul>

                                        </table>


<?php
    break;
  case '2':
    $query_procedure = "SELECT maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_after WHERE maintenance_routine.id = '$rotina'";
    if ($stmt_procedure = $conn_procedure->prepare($query_procedure)) {
      $stmt_procedure->execute();
      $stmt_procedure->bind_result($id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
      while ($stmt_procedure->fetch()) {
        //printf("%s, %s\n", $solicitante, $equipamento);
      }
    }
    $query_procedure="SELECT *FROM maintenance_procedures_itens WHERE id_maintenance_procedures like '$id_maintenance_procedures' order by position ASC ";
    $row=1;
    if ($stmt_procedure = $conn->prepare($query_procedure)) {
      $stmt_procedure->execute();
      $stmt_procedure->bind_result($id,$id_maintenance_procedures,$class,$column,$procedures);

      ?>
      <table cellspacing="0"  style=" border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
        <thead>
          <tr>
            <th style="font-size:8px;">#</th>


            <th style="font-size:8px;">Procedimento</th>

          </tr>
        </thead>
        <ul class="columns" data-columns="2">
          <tbody>
          <?php $dados = 1; while ($stmt_procedure->fetch()) { ?>
                                          <?php if($dados == 0 && $class == 4 ){ 
                                               $base = 30;
                                               $espaco = ((($base - ( $num_caracteres + $num_digitos ) )));
                                               $espaco =  $espaco/2;                                   
                                               for ($a = 1; $a <= $espaco; $a++) {
                                                printf("&nbsp;");

                                             }
                                             
                                             printf("|");
                                            
                                             for ($a = 1; $a <= $espaco; $a++) {
                                              printf("&nbsp;");

                                           }

                                              echo htmlspecialchars_decode($procedures); 
                                 
$num_caracteres = 0;
                                 $num_digitos = 0;
for ($i = 0; $i < strlen($procedures); $i++) {
                                  $caractere = $procedures[$i];
                                  if (ctype_alpha($caractere)) {
                                    $num_caracteres++;
                                  } elseif (ctype_digit($caractere)) {
                                    $num_digitos++;
                                  }
                                }
                                 ?>
                                            <?php $cont = $cont +1 ; }elseif($dados == 0 && $class != 4 ) { ?>
                    
                              
                                              </div>


</td>

</small>
</tr>
<th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

                            <td style="font-size:8px;"  > <small style="font-size:8px;">
                            <div class="input-group input-group-sm  ">

                            
                                               Medida 1
                                               <?php 
                                               for ($i = 1; $i <= $cont; $i++) {
                                                printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                             }
                                             printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                               ?>
                            </div>

<span class="right">


<?php printf("│ │-OK-│ │-NOK-│ │-NA");?> 

</span align="right"> 
</div>


</td>

</small>
</tr>
<th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

<td style="font-size:8px;"  > <small style="font-size:8px;">
<div class="input-group input-group-sm  ">

                                                Medida 2 
                                                <?php 
                                               for ($i = 1; $i <= $cont; $i++) {
                                                printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                             }
                                             printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                               ?>
</div>

<span class="right">


<?php printf("│ │-OK-│ │-NOK-│ │-NA");?> 

</span align="right"> 
</div>


</td>

</small>
</tr>
<th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

<td style="font-size:8px;"  > <small style="font-size:8px;">
<div class="input-group input-group-sm  ">

                                               Medida 3 
                                               <?php 
                                               for ($i = 1; $i <= $cont; $i++) {
                                                printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                             }
                                             printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

                                               ?>
</div>
                                                
                                                
                                           
                                        </div>
                                        <span class="right">


<?php printf("│ │-OK-│ │-NOK-│ │-NA");?> 

</span align="right"></b>



</td>

</small>
</tr>

<?php $dados = 1; }?>

<?php if($dados == 1 ){ ?>

                        <tr>
                            <th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

                            <td style="font-size:8px;"  > <small style="font-size:8px;">
                                    <center> <b>
                                            <?php if($class==1){echo htmlspecialchars_decode($procedures); $dados = 1;}  ?>
                                        </b> </center> <b>
                                        <?php if($class==2){echo htmlspecialchars_decode($procedures);  ?>
                                    </b>

                                    <span class="right">
                                        <!--   <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> OK</small>

                                                    <input align="right" type="radio" class="flat" name="iCheck2" disabled="disabled" style="font-size:8px;">  <small style="font-size:8px;">NOK</small>

                                                    <input align="right" type="radio" class="flat" name="iCheck3" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> NA
                                                  </small> -->

                                        <b> <?php printf("│ │-OK-│ │-NOK-│ │-NA");?> </b>
                                    </span align="right">
                                    <?php $dados = 1;} if($class==5){?>
                                    <?php echo htmlspecialchars_decode($procedures);?>

                                    <?php $dados = 1;} if($class==3){?>

                                    <div class="input-group input-group-sm  ">
                                        <b> <?php echo htmlspecialchars_decode($procedures);?>
                                            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
                                            <?php echo htmlspecialchars_decode($procedures);?>
                                            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
                                            <?php echo htmlspecialchars_decode($procedures);?>
                                            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
                                        </b>
                                    </div>
                                    <span class="right">

                                        <b> <?php printf("│ │-OK-│ │-NOK-│ │-NA");?> </b>
                                    </span align="right">
                                    <?php $dados = 1; } if($class == 4  ){ $cont = 1; ?>

                                    <div>
                                    <div class="input-group input-group-sm  ">
                               
                                   
                                 <b> <?php 
                                 
                                 printf("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                 echo htmlspecialchars_decode($procedures);
                                 
$num_caracteres = 0;
                                 $num_digitos = 0;
for ($i = 0; $i < strlen($procedures); $i++) {
                                  $caractere = $procedures[$i];
                                  if (ctype_alpha($caractere)) {
                                    $num_caracteres++;
                                  } elseif (ctype_digit($caractere)) {
                                    $num_digitos++;
                                  }
                                }
                                 ?>
                                
       


                                        <?php $dados=0; } }$row=$row+1; 
                                      } }  ?>
                                        </tbody>
                                        </ul>

                                        </table>

<?php        break;

case '3':
$query = "SELECT maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_before WHERE maintenance_routine.id = '$rotina'";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
while ($stmt->fetch()) {
  //printf("%s, %s\n", $solicitante, $equipamento);
}
}
$query="SELECT *FROM maintenance_procedures_itens WHERE id_maintenance_procedures like '$id_maintenance_procedures' order by position ASC ";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$id_maintenance_procedures,$class,$column,$procedures);

?>
<table cellspacing="0"  style=" border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
  <thead>
    <tr>
      <th style="font-size:8px;">#</th>


      <th style="font-size:8px;">Procedimento</th>

    </tr>
  </thead>
  <ul class="columns" data-columns="2">
    <tbody>
      <?php  while ($stmt->fetch()) { ?>

        <tr>
          <th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

          <td style="font-size:8px;"> <small style="font-size:8px;"><center > <b> <?php if($class==1){printf("$procedures");} ?>  </b> </center>   <?php if($class==2){printf("$procedures");  ?>

            <span class="right">
              <!--   <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> OK</small>

              <input align="right" type="radio" class="flat" name="iCheck2" disabled="disabled" style="font-size:8px;">  <small style="font-size:8px;">NOK</small>

              <input align="right" type="radio" class="flat" name="iCheck3" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> NA
            </small> -->

            <?php printf("│ │-OK-│ │-NOK-│ │-NA");?>
          </span align="right">

        <?php } if($class==3){?>

          <div  class="input-group input-group-sm  ">
            <?php printf("$procedures");?>
            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
            <?php printf("$procedures");?>
            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
            <?php printf("$procedures");?>
            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
          </div>
          <span class="right">

            <?php printf("│ │-OK-│ │-NOK-│ │-NA");?>
          </span align="right">
        </small>
      </td>

    </tr>
  <?php } $row=$row+1; }
}   ?>
</tbody>
</ul>

</table>

<?php
$query = "SELECT maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_after WHERE maintenance_routine.id = '$rotina'";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
}
}
$query="SELECT *FROM maintenance_procedures_itens WHERE id_maintenance_procedures like '$id_maintenance_procedures' order by position ASC ";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$id_maintenance_procedures,$class,$column,$procedures);

?>
<table cellspacing="0"  style=" border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<thead>
<tr>
  <th style="font-size:8px;">#</th>


  <th style="font-size:8px;">Procedimento</th>

</tr>
</thead>
<ul class="columns" data-columns="2">
<tbody>
  <?php  while ($stmt->fetch()) { ?>

    <tr>
      <th scope="row"><small style="font-size:8px;"> <?php printf($row); ?></small></th>

      <td style="font-size:8px;"> <small style="font-size:8px;"><center > <b> <?php if($class==1){printf("$procedures");} ?>  </b> </center>   <?php if($class==2){printf("$procedures");  ?>

        <span class="right">
          <!--   <input align="right" type="radio" class="flat" name="iCheck1" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> OK</small>

          <input align="right" type="radio" class="flat" name="iCheck2" disabled="disabled" style="font-size:8px;">  <small style="font-size:8px;">NOK</small>

          <input align="right" type="radio" class="flat" name="iCheck3" disabled="disabled" style="font-size:8px;"> <small style="font-size:8px;"> NA
        </small> -->

        <?php printf("│ │-OK-│ │-NOK-│ │-NA");?>
      </span align="right">

    <?php } if($class==3){?>

      <div  class="input-group input-group-sm  ">
        <?php printf("$procedures");?>
        <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
        <?php printf("$procedures");?>
        <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
        <?php printf("$procedures");?>
        <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
      </div>
      <span class="right">

        <?php printf("│ │-OK-│ │-NOK-│ │-NA");?>
      </span align="right">
    </small>
  </td>

</tr>
<?php } $row=$row+1; }
}   ?>
</tbody>
</ul>

</table>
<?php
break;

} ?>

</div>

<center> <small style="font-size:8px;">TEMPERATURA DO AR AMBIENTE (Temp °C), UMIDADE DO AR AMBIENTE (URa %):</small></center>
<table cellspacing="0"  style="margin-top: 5px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<thead>
<tr>
<th style="font-size:8px;">  Temp</th>
<th style="font-size:8px;">URa</th>



</tr>
</thead>
<tbody>
<tr>
<td style="font-size:8px;"><small>            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
</small></td>
<td style="font-size:8px;"><small>            <?php printf("│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│");?>
</small></td>

</tr>
</tbody>
</table>
<!--arquivo -->
<?php
$query="SELECT colaborador.ultimonome,regdate_mp_registro.id_mp, regdate_mp_registro.id_user,regdate_mp_registro.date_start,regdate_mp_registro.date_end,regdate_mp_registro.time,regdate_mp_registro.reg_date, regdate_mp_registro.upgrade, colaborador.id, colaborador.primeironome FROM regdate_mp_registro INNER JOIN colaborador on regdate_mp_registro.id_user = colaborador.id WHERE regdate_mp_registro.id_mp like '$id_maintenance_preventive'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($ultimonome,$id_mp,$id_user,$date_start,$date_end,$time,$reg_date,$upgrade,$id,$usuario);

?>
<center> <small style="font-size:8px;">Executante:</small></center>

<table cellspacing="0"  style="margin-top: 5px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<thead>
<tr>
  <th style="font-size:8px;">#</th>
  <th style="font-size:8px;">Executante</th>
  <th style="font-size:8px;">Data de inicio</th>
  <th style="font-size:8px;">Data de Termino</th>
  <th style="font-size:8px;">Tempo</th>


</tr>
</thead>
<tbody>
<?php  while ($stmt->fetch()) { ?>

  <tr>
    <th scope="row"><?php printf($row); ?></th>

    <td style="font-size:8px;"><small><?php printf($usuario); ?> <?php printf($ultimonome); ?></small></td>
    <td style="font-size:8px;"><small><?php printf($date_start); ?></small></td>
    <td style="font-size:8px;"><small><?php printf($date_end); ?></small></td>
    <td style="font-size:8px;"><small><?php printf($time); ?></small></td>

  </tr>


  <?php  $row=$row+1; }
}   ?>
</tbody>
</table>
<label for="message">Observação de MP :</label>
<textarea rows="1" cols="1" id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup"
data-parsley-validation-threshold="2" placeholder="<?php printf($obs_mp); ?>"readonly="readonly"></textarea>

<label for="message">Observação Técnica :</label>
<textarea  rows="1" cols="1" id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup"
data-parsley-validation-threshold="2" placeholder="<?php printf($obs_tc); ?>"readonly="readonly"></textarea>

<table cellspacing="0"  style="margin-top: 5px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
  <td > <strong style="font-size:8px;"align="center"> Data Inicio:</strong> <?php printf($date_start_ms); ?></td>
  <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Data Termino:</strong> <?php printf($date_end_ms)?></td>
  <td style="font-size:8px;"> <strong style="font-size:8px;"align="center">Tempo Execução:</strong> <?php printf($time_mp)?></td>
</tr>




</tbody>
</table>

<center>  <small style="font-size:8px;">Declaro que os serviços constantes no presente relatório técnico foram efetivamente
prestados, sendo aceito por mim nesta data. </small>  </center>


<table cellspacing="0"  width="100%" cellpadding="0" cellspacing="0">
<tr>

  <td valign="bottom" align="center">
    <span id="ctl00_ContentPlaceHolder1_repRelatorio_ctl00_lblSupervisorMatricula">________________________</span><br />

  </td>
  <td valign="bottom" align="center"><span id="ctl00_ContentPlaceHolder1_repRelatorio_ctl00_lblUsuárioFechamentoMatricula">________________________</span><br />
  </td>
  <td valign="bottom" align="center">
    <span id="ctl00_ContentPlaceHolder1_repRelatorio_ctl00_responsavelAceiteMatricula">________________________</span><br />

  </td>
</tr>
<tr>

  <td valign="top" align="center" style="font-size:8px;">Supervisor de Manutenção</td>
  <td valign="top" align="center" style="font-size:8px;">Executante</td>
  <td valign="top" align="center" style="font-size:8px;">Matrícula / Resp. pelo aceite</td>
</tr>
</table>





<div class="footer" align="center">

<div align="center" style="padding-top: 8px;"><strong>Copyright © 2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados.</strong>www.mksistemasbiomedicos.com.br

  MKS-03-ra-1.0

</div>
</div>
</div>
</div>
</div>
</div>

<!--  Fim do container-->


<!--  Rodape -->

<!--  Fim do Rodape  -->










<script src="../../framework/viewer/bootstrap/js/bootstrap.min.js"></script>
<script src="../../framework/viewer/bootstrap/js/datatables.bootstrap.js"></script>
<script src="../../framework/viewer/bootstrap/js/bootstrap-select.min.js"></script>

<script src="../../framework/viewer/js/countdown/jquery.countdown.js"></script>
<script src="../../framework/viewer/js/countdown/jquery.countdown-pt-BR.js"></script>


</body>
</html>
