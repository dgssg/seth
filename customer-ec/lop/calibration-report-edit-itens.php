  <?php 
  include("database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
	if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
} if( $_SESSION['mod'] != $mod ){
    header ("Location: ../index.php");
}
	require_once 'auth.php';
	require_once 'permissions.php';
	// Verificar se o usuário é admin
	checkPermission('1', $pdo);
	// Verificar se o usuário está autenticado
	if (!isset($_SESSION['session_token'])) {
		header('Location: ../../index.php');
		exit;
	}
	
	// Valida o token de sessão
	$stmt = $conn->prepare("SELECT * FROM usuario WHERE session_token = ? AND is_active = TRUE");
	$stmt->bind_param('s', $_SESSION['session_token']);
	$stmt->execute();
	$result = $stmt->get_result();
	$user = $result->fetch_assoc();

	if (!$user) {
		// Token inválido ou sessão expirada
		session_destroy();
		header('Location: ../../index.php');
		exit;
	}
	
	// Função para obter a saudação com base no horário do dia
	function saudacao() {
		$hora = date('H');
		if ($hora < 12) {
			return "Bom dia";
		} elseif ($hora < 18) {
			return "Boa tarde";
		} else {
			return "Boa noite";
		}
}	
	$query = "SELECT budget FROM tools";	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($budget_status);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$query = "SELECT assistence FROM tools";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($assistence);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$usuariologado=$_SESSION['usuario'];
	// Adiciona a Função display_campo($nome_campo, $tcruzvermelhacastro_campo)
	//require_once "personalizacao_display.php";
	//	if (!$PHPSESSID) {
	//	header ("Location: ../index.php");

	//}

?>
<?php  
include("database/database.php");
$codigoget= $_GET['id'];
$d_1= $_GET['d_1'];
$d_2= $_GET['d_2'];
$erro= $_GET['erro'];
$query="SELECT id, id_calibration, id_calibration_parameter_tools, id_calibration_parameter_tools_dados, id_calibration_parameter_dados_parameter, ve, vl_1, vl_2, vl_3,k FROM calibration_report WHERE id like '$codigoget' ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $id_calibration, $id_calibration_parameter_tools, $id_calibration_parameter_tools_dados, $id_calibration_parameter_dados_parameter, $ve, $vl_1, $vl_2, $vl_3,$k);
    while ($stmt->fetch()) {
}
}

  
?>

                    
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      
      <!-- User name -->
     

      <!-- START LOCK SCREEN ITEM -->
     
              <form action="backend/calibration-report-edit-itens-backend.php?id=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>
                            
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="ve">Valor de Ensaio <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="ve" name="ve" value="<?php printf($ve); ?> " required="required" class="form-control ">
                        </div>
                      </div> 
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_1">1º Ensaio <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="vl_1" name="vl_1" value="<?php printf($vl_1); ?> " required="required" class="form-control ">
                        </div>
                      </div> 
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_2">2º Ensaio <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="vl_2" name="vl_2" value="<?php printf($vl_2); ?> "  required="required" class="form-control ">
                        </div>
                      </div> 
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vl_3">3º Ensaio <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="vl_3" name="vl_3" value="<?php printf($vl_3); ?> "  required="required" class="form-control ">
                        </div>
                      </div> 
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="k"> FATOR DE ABRANGÊNCIA (K) <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="k" name="k" value="<?php printf($k); ?> "  required="required" class="form-control ">
                        </div>
                      </div> 
                      
            
                     
 
                     
					
                    	<div class="col-md-7 col-sm-7  form-group has-feedback">
										<select type="text" class="form-control has-feedback-right" name="tools" id="tools"  placeholder="Equipamento">
										    	<?php
										  	
										  
										  	
										   $sql = "SELECT calibration_parameter_tools_dados.id, calibration_parameter_tools_dados.nome,calibration_parameter_tools_dados.unidade,calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo FROM calibration_parameter_tools  INNER JOIN calibration_parameter_tools_dados on calibration_parameter_tools_dados.id_calibration_parameter_tools = calibration_parameter_tools.id WHERE calibration_parameter_tools_dados.id like '$id_calibration_parameter_tools_dados'";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$parametro, $unidade, $equipamento,$fabricante,$modelo);
     while ($stmt->fetch()) {
     ?>
										
										   <option value="<?php printf($id);?>	"> <?php printf($equipamento);?>	<?php printf($fabricante);?> <?php printf($modelo);?> <?php printf($parametro);?> <?php printf($unidade);?></option>
										  <?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT calibration_parameter_tools_dados.id, calibration_parameter_tools_dados.nome,calibration_parameter_tools_dados.unidade,calibration_parameter_tools.nome,calibration_parameter_tools.fabricante,calibration_parameter_tools.modelo FROM calibration_parameter_tools  INNER JOIN calibration_parameter_tools_dados on calibration_parameter_tools_dados.id_calibration_parameter_tools = calibration_parameter_tools.id";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$parametro, $unidade, $equipamento,$fabricante,$modelo);
     while ($stmt->fetch()) {
     ?>
    
<option value="<?php printf($id);?>	"> <?php printf($equipamento);?>	<?php printf($fabricante);?> <?php printf($modelo);?> <?php printf($parametro);?> <?php printf($unidade);?></option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
											
	                                     	</select>  
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
										
										
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#tools').select2();
                                      });
                                 </script>
                    
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="d_1"> Erro Aceitável: <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="d_1" name="d_1" value="<?php printf($d_1); ?> "  class="form-control " <?php if($erro=="1"){printf("readonly='readonly'"); }?>>
                        </div>
                      </div> 
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="d_2">Erro Aceitável (%): <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="text" id="d_2" name="d_2" value="<?php printf($d_2); ?> "   class="form-control "  <?php if($erro=="0"){printf("readonly='readonly'"); }?>>
                        </div>
                      </div> 
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="erro"> <span class="required"></span>
                        </label>
                        <div class="col-md-7 col-sm-6 ">
                          <input type="hidden" id="erro" name="erro" value="<?php printf($erro); ?> "   class="form-control "  >
                        </div>
                      </div> 
                    
                           
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                        </div>

                     
                   
                  
              </form>
     
     
    </div><!-- /.center -->

    <!-- jQuery 2.1.4 -->
    <script src="../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../framework/bootstrap/js/bootstrap.min.js"></script>
	</body>
 
 </html>

