<?php


include("../database/database.php");
session_start();
if(!isset($_SESSION['usuario'])){
  header ("Location: ../index.php");
}
  
  
  date_default_timezone_set('America/Sao_Paulo');
  $ano=date("Y");
  $today=date("Y-m-d");
  $mes=date("m");


$usuariologado=$_SESSION['usuario'];
$instituicaologado=$_SESSION['instituicao'];
$setorlogado=$_SESSION['setor'];
 
 
$tools = $_POST['tools'];
$tools = trim($tools);

$equipamento_grupo = $_POST['equipamento_grupo'];
$equipamento_grupo = trim($equipamento_grupo);

$baixa = $_POST['baixa'];
$baixa = trim($baixa);

$ativo = $_POST['ativo'];
$ativo = trim($ativo);


$comodato = $_POST['comodato'];
$comodato = trim($comodato);

$model = $_POST['model'];
$model = trim($model);

$manufacture = $_POST['manufacture'];
$manufacture = trim($manufacture);


$trash = $_POST['trash'];
$trash = trim($trash);





$filtro="";
$query = "SELECT   calibration_parameter_tools_laudos.file,calibration_parameter_tools.id, calibration_parameter_tools.nome, calibration_parameter_tools.fabricante, calibration_parameter_tools.modelo, calibration_parameter_tools.serie, calibration_parameter_tools.patrimonio,calibration_parameter_tools.calibration,calibration_parameter_tools.date_calibration, calibration_parameter_tools.date_validation FROM calibration_parameter_tools LEFT JOIN calibration_parameter_tools_laudos ON calibration_parameter_tools_laudos.id_calibration_parameter_tools = calibration_parameter_tools.id and calibration_parameter_tools_laudos.date_validation =  calibration_parameter_tools.date_validation WHERE  calibration_parameter_tools.id > 1  ";
 
 

if($trash == "0"){
}
if($trash == "1"){
 $query = " $query and  calibration_parameter_tools.trash = 0";
}
if($trash == "2"){
 $query = " $query and  (calibration_parameter_tools.trash = 1 OR calibration_parameter_tools.trash IS NULL)";
}

  
  if($ativo == "0"){
  }
  if($ativo == "1"){
    $query = " $query and  calibration_parameter_tools.ativo = 0";
  }
  if($ativo == "2"){
    $query = " $query and  (calibration_parameter_tools.ativo = 1 OR calibration_parameter_tools.ativo IS NULL)";
  }

if($tools==!""){
  $query = " $query and  calibration_parameter_tools.nome = '$tools'";
}

if($model==!""){
    $query = " $query and  calibration_parameter_tools.modelo = '$model'";
}

if($manufacture==!""){
  $query = " $query and  calibration_parameter_tools.fabricante = '$manufacture'";
}

 


  if($instituicao==!""){
    $sql = "SELECT  instituicao,logo,endereco,cep,bairro,cidade,estado,logo,cnpj FROM instituicao WHERE id = $instituicao ";
    
    
    if ($stmt = $conn->prepare($sql)) {
      $stmt->execute();
      $stmt->bind_result($unidade,$logo,$rua,$cep,$bairro,$cidade,$estado,$logo,$cnpj);
      while ($stmt->fetch()) {
      }
    }
  }
    
  
  
  if($instituicao_area==!""){
    
    $sql = "SELECT instituicao_area.nome FROM instituicao_area WHERE instituicao_area.id = $instituicao_area";
    if ($stmt = $conn->prepare($sql)) {
      $stmt->execute();
      $stmt->bind_result( $setor_titulo);
      
      while ($stmt->fetch()) {
      }
    }
    
  }
  
  if($id_instituicao_localizacao==!""){
    $sql = "SELECT instituicao_localizacao.nome FROM instituicao_localizacao WHERE instituicao_localizacao.id = $id_instituicao_localizacao";
    if ($stmt = $conn->prepare($sql)) {
      $stmt->execute();
      $stmt->bind_result( $area_titulo);
      
      while ($stmt->fetch()) {
      }
    }
    
  }
  $query = " $query   GROUP BY calibration_parameter_tools.id";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($file,$id, $nome, $fabricante, $modelo, $serie, $patrimonio,$calibration, $date_calibration, $date_validation);



//header('Location: ../report-report-mc');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    body {
    font-size: 12pt;
    font-family: Calibri;
    padding : 10px;
}
table {
    border: 1px solid black;
}
th {
    border: 1px solid black;
    padding: 5px;
    background-color:grey;
    color: white;
}
td {
    border: 1px solid black;
    padding: 5px;
}
input {
    font-size: 12pt;
    font-family: Calibri;
}
</style>
  </head>
  <body>
  
                  <a  class="btn btn-secondary btn-xs"  id="btnExport"> Excel
                    
                  </a>
                   
                  <script type="text/javascript" language="JavaScript">

function printPage() {

  if (window.print) {

    agree = confirm("Deseja imprimir essa pagina ?");

    if (agree) {
      window.print();

      if (_GET("pg") != null)
      location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
      else
      history.go(-1);


      window.close();
    }



  }
}

function noPrint() {
  try {

    if (_GET("pg") != null)
    location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
    else
    history.go(-1);

    window.close();

  }
  catch (e) {
    alert();
    document.write("Error Message: " + e.message);
  }
}

function _GET(name) {
  var url = window.location.search.replace("?", "");
  var itens = url.split("&");

  for (n in itens) {
    if (itens[n].match(name)) {
      return decodeURIComponent(itens[n].replace(name + "=", ""));
    }
  }
  return null;
}





function visualizarImpressao() {

  var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

  document.body.insertAdjacentHTML("beforeEnd", Navegador);

  Navegador1.ExecWB(7, 1);

  Navegador1.outerHTML = "";

}



(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
    (i[r].q = i[r].q || []).push(arguments)
  }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-11247563-6', 'auto');
ga('send', 'pageview');

// -->
</script>

<style type="text/css">
page {
  /* background: white;*/
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  text-align: left;
}

page[size="A4"] {
  width: 21cm;
  /*height: 29.7cm;                */
}

page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /* height: 21cm; */
}

@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
    page-break-after:always;
  }

  .noprint {
    display: none;
  }
}




body {
  font-size: 11px;
  font-family: sans-serif;
}



.pagina_retrato {
  width: 720px;
}

.pagina_paisagem {
  width: 1025px;
}

.relatorio_filtro {
  padding: 10px 0px;
  border-collapse: collapse;
}

.relatorio_filtro tr td {
  border: 1px solid #000;
}


.relatorio_titulo {
  font-family: Times New Roman;
  font-size: 15px;
  color: Black;
  font-weight: bold;
  text-transform: uppercase;
}

.borda_celula {
  border: 1px solid #000;
  background-color: #fff;
}

.noprint {
  padding-bottom: 10px;
}
.right{
  float:right;
}

.left{
  float:left;
}
</style>

<style type="text/css" media="print">
/*
@page {
margin: 0.8cm;
}
*/
.pagina_retrato, .pagina_paisagem {
width: 100%;
}
 
</style>

<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script>
window.onload = function () {
JsBarcode(".barcode").init();
}
</script>

   <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <style>
    /* Estilos para o cabeçalho */
    header {
      padding: 20px;
      background-color: #f5f5f5;
      border-bottom: 1px solid #ddd;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      padding: 5px;
      border: 1px solid #ddd;
    } 

    #logo {
      width: 100px;
    }

    h1 {
      margin: 0;
      font-size: 24px;
    }

    #info {
      font-size: 14px;
    }
  </style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />
<link href="../App_Themes/Tema1/StyleSheet.css" type="text/css" rel="stylesheet" />

<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEwODA5NzUyMjMPZBYCZg9kFgICAw9kFgICAQ9kFggCAw8PFgIeCEltYWdlVXJsBUd+L2ltYWdlcy9ob3NwaXRhaXMvaV9jcnV6dmVybWVsaGFicmFzaWxlaXJhLWZpbGlhbGRvZXN0YWRvZG9wYXJhbsOhLnBuZ2RkAgUPDxYCHgRUZXh0BQcwMS8yMDE5ZGQCCw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIeCkhlYWRlclRleHQFBzAxLzIwMTg8KwAFAQAWAh8EBQcwMi8yMDE4PCsABQEAFgIfBAUHMDMvMjAxODwrAAUBABYCHwQFBzA0LzIwMTg8KwAFAQAWAh8EBQcwNS8yMDE4PCsABQEAFgIfBAUHMDYvMjAxODwrAAUBABYCHwQFBzA3LzIwMTg8KwAFAQAWAh8EBQcwOC8yMDE4PCsABQEAFgIfBAUHMDkvMjAxODwrAAUBABYCHwQFBzEwLzIwMTg8KwAFAQAWAh8EBQcxMS8yMDE4PCsABQEAFgIfBAUHMTIvMjAxODwrAAUBABYCHwQFBzAxLzIwMTkWDQIGAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGDBQrAABkAg8PPCsAEQMADxYEHwJnHwNmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIfBAUHMDEvMjAxODwrAAUBABYCHwQFBzAyLzIwMTg8KwAFAQAWAh8EBQcwMy8yMDE4PCsABQEAFgIfBAUHMDQvMjAxODwrAAUBABYCHwQFBzA1LzIwMTg8KwAFAQAWAh8EBQcwNi8yMDE4PCsABQEAFgIfBAUHMDcvMjAxODwrAAUBABYCHwQFBzA4LzIwMTg8KwAFAQAWAh8EBQcwOS8yMDE4PCsABQEAFgIfBAUHMTAvMjAxODwrAAUBABYCHwQFBzExLzIwMTg8KwAFAQAWAh8EBQcxMi8yMDE4PCsABQEAFgIfBAUHMDEvMjAxORYNAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGAgYMFCsAAGQYAgUeY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSRncnYyDzwrAAwBCGZkBR5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJGdydjEPPCsADAEIZmS9tcluC/wCTfFh0PbdSxSO8nxQJa5LJqUDKFT3sJzurQ==" />
</div>

<div>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3164E9EE" />
</div>

<div class="noprint">
<center>
  <input type="button" value="Não Imprimir" onclick="noPrint();">
  <input type="button" value="Imprimir" onclick="printPage();" >
  <input type="button" onclick="visualizarImpressao();" value="Visualizar Impressão" >
</center>
</div>

  <header>
    <table>
      <tr>
      <td><img src="../dropzone/logo/<?php printf($logo)?>" alt="Logo da empresa" id="logo"></td>
        <td>
          <center>
          <h1>Relatório de Analisador e Simulador</h1>
        
          <p>Data da impressão: <?php printf($today); ?> <small>Sistema SETH</small></p>
          </center>
        </td>
        <td><img src="../logo/clientelogo.png" alt="Logo da empresa" id="logo"></td>
      </tr>
      <tr>
        <td colspan="3" id="info">
          <table>
            <tr>
              <td>Unidade:</td>
              <td><?php printf($unidade); ?></td>
              <td>Setor:</td>
              <td><?php printf($setor_titulo); ?></td>
              <td>Area:</td>
              <td><?php printf($area_titulo); ?></td>
            </tr>
            <tr>
              <td>CEP:</td>
              <td><?php printf($cep); ?></td>
              <td>Rua:</td>
              <td><?php printf($rua); ?></td>
              <td>Bairro:</td>
              <td><?php printf($bairro); ?></td>
            </tr>
            <tr>
              <td>Cidade:</td>
              <td><?php printf($cidade); ?></td>
              <td>Estado:</td>
              <td><?php printf($estado); ?></td>
              <td>CNPJ:</td>
              <td><?php printf($cnpj); ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </header>
                    <div id="dvData">
                    <table style="border-collapse: collapse; width: 100%;">
                      <thead>
                        <tr>
                         <th style="border: 1px solid black; padding: 5px;">#</th>
                          <th style="border: 1px solid black; padding: 5px;">Nome</th>
                           <th style="border: 1px solid black; padding: 5px;">Fabricante</th>
                          <th style="border: 1px solid black; padding: 5px;">Modelo</th>
                          <th style="border: 1px solid black; padding: 5px;">Nº Serie</th>
                          <th style="border: 1px solid black; padding: 5px;">Patrim&#244;nio</th>
                          <th style="border: 1px solid black; padding: 5px;">Nº Certificado</th>
                          <th style="border: 1px solid black; padding: 5px;">Calibração</th>
                           <th style="border: 1px solid black; padding: 5px;">Validação</th>
                           <th style="border: 1px solid black; padding: 5px;">Status</th>
 



                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td>  </td>
                          <td><?php echo htmlspecialchars_decode($nome); ?> </td>
                          <td><? echo htmlspecialchars_decode($fabricante);?> </td>
                          <td>  <? echo htmlspecialchars_decode($modelo); ?> </td>
                          <td>   <? echo htmlspecialchars_decode($serie); ?></td>
                          
                            <td><?php echo htmlspecialchars_decode($patrimonio); ?> </td>
                             <td><?php echo htmlspecialchars_decode($calibration); ?> </td>
                              <td><?php echo htmlspecialchars_decode($date_calibration); ?> </td>
                              <td><?php echo htmlspecialchars_decode($date_validation); ?> </td>
                          <td><?php if($today > $date_validation ){ printf("Fora da Validade");}  if($today < $date_validation ){ printf("Dentro da Validade");}?></td> 
                          
                           
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                    </div>
                     <!-- jQuery 2.1.4 -->
    <script src="../../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../../framework/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="jquery.btechco.excelexport.js"></script>
<script src="jquery.base64.js"></script>
<!--<script>
    $(document).ready(function () {
        $("#btnExport").click(function () {
            $("#tblExport").btechco_excelexport({
                containerid: "tblExport"
               , datatype: $datatype.Table
               , filename: 'sample'
            });
        });
    });
</script>
<script>
    $("#btnExport").click(function (e) {
        window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
        e.preventDefault();
    });
  </script> -->
  <script>
  $(document).ready(function () {
   $("#btnExport").click(function (e) {
        e.preventDefault();
        var table_div = document.getElementById('dvData');
        // esse "\ufeff" é importante para manter os acentos
        var blobData = new Blob(['\ufeff'+table_div.outerHTML], { type: 'application/vnd.ms-excel' });
        var url = window.URL.createObjectURL(blobData);
        var a = document.createElement('a');
        a.href = url;
        a.download = 'SETH_Inventario'
              a.click();
          });
      });
  </script>
	</body>

 </html>
