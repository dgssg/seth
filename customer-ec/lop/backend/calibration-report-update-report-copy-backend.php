<?php

include("../database/database.php");

$codigoget = ($_GET["laudo"]);

$codigo= $_POST['codigo'];
$id_equipamento= $_POST['equipamento'];
$data_start= $_POST['data_start'];
$val= $_POST['date_validation'];
$id_manufacture= $_POST['id_manufacture'];
$id_colaborador= $_POST['id_colaborador'];
$id_responsavel= $_POST['id_responsavel'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];
$obs= $_POST['obs'];

$stmt = $conn->prepare("UPDATE  equipamento SET data_calibration = ? WHERE id= ?");
$stmt->bind_param("ss",$data_start,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$data_calibration_end=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));

$stmt = $conn->prepare("UPDATE  equipamento SET data_calibration_end = ? WHERE id= ?");
$stmt->bind_param("ss",$data_calibration_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET id_equipamento = ? WHERE id= ?");
$stmt->bind_param("ss",$id_equipamento,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET data_start = ? WHERE id= ?");
$stmt->bind_param("ss",$data_start,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET val = ? WHERE id= ?");
$stmt->bind_param("ss",$val,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET id_manufacture = ? WHERE id= ?");
$stmt->bind_param("ss",$id_manufacture,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET id_colaborador = ? WHERE id= ?");
$stmt->bind_param("ss",$id_colaborador,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET id_responsavel = ? WHERE id= ?");
$stmt->bind_param("ss",$id_responsavel,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET temp = ? WHERE id= ?");
$stmt->bind_param("ss",$temp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET hum = ? WHERE id= ?");
$stmt->bind_param("ss",$hum,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET obs = ? WHERE id= ?");
$stmt->bind_param("ss",$obs,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration SET codigo = ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

echo "<script>alert('Atualizado!');document.location='../calibration-report-edit-copy?laudo=$codigoget'</script>";

?>
