<?php
header("Content-Type: application/json");
require_once "../../database/database.php"; // Arquivo de conexão com o banco de dados
    session_start();
    $setor = $_SESSION['setor'];
    $usuario = $_SESSION['id_usuario'];
    $id = $_GET['id'];
    
    date_default_timezone_set('America/Sao_Paulo');
    $attempt_date = date('Y-m-d H:i:s'); // Formato compatível com MySQL
    
    

    // Verifica se é uma requisição POST
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
        $stmt = $conn->prepare("INSERT INTO user_training_attempts (id_user, id_training, attempt_date) VALUES (?, ?, ?)");
        $stmt->bind_param("sss", $usuario, $id, $attempt_date);
        $execval = $stmt->execute();
        $stmt->close();

             
        $data = json_decode(file_get_contents("php://input"), true);
        
        if (!$data) {
            echo json_encode(["status" => "error", "message" => "Nenhum dado recebido"]);
            exit;
        }
        
        $feedback_id = $data['feedback_id']; // ID do feedback (se aplicável)
        $responses = $data['responses']; // Respostas do usuário
        
        // Obtém o último ID da tentativa de treinamento
        $last_id = $conn->insert_id;
        if (!$last_id) {
            echo json_encode(["status" => "error", "message" => "Erro ao obter ID da tentativa"]);
            exit;
        }
        
        $values = [];
        foreach ($responses as $question_id => $answer) {
            if (!is_numeric($question_id) || $question_id <= 0) {
                echo json_encode(["status" => "error", "message" => "ID da questão inválido: $question_id"]);
                exit;
            }
            if (is_array($answer)) {
                foreach ($answer as $ans) {
                    $values[] = "('$last_id', '$question_id', '$ans', NULL)";
                }
            } elseif (is_numeric($answer)) {
                $values[] = "('$last_id', '$question_id', '$answer', NULL)";
            } else {
                $values[] = "('$last_id', '$question_id', NULL, '$answer')";
            }
        }
        
        if (!empty($values)) {
            $insertQuery = "INSERT INTO user_training_responses (id_attempt, id_question, id_answer, user_response) VALUES " . implode(", ", $values);
            if (!mysqli_query($conn, $insertQuery)) {
                echo json_encode(["status" => "error", "message" => "Erro ao salvar respostas: " . mysqli_error($conn)]);
                exit;
            }
        }
        
        // Calcula a pontuação
        $query = "SELECT 
        SUM(CASE WHEN ur.is_correct = 1 THEN 1 ELSE 0 END) AS correct_answers,
        COUNT(*) AS total_questions,
        IFNULL((SUM(CASE WHEN ur.is_correct = 1 THEN 1 ELSE 0 END) / NULLIF(COUNT(*), 0)) * 100, 0) AS score_percentage
        FROM user_training_responses ur
        WHERE ur.id_attempt = ?";
        
        if ($stmt = $conn->prepare($query)) {
            $stmt->bind_param("s", $last_id);
            $stmt->execute();
            $stmt->bind_result($correto, $total, $score);
            $stmt->fetch();
            $stmt->close();
        }
        
        // Atualiza o score e status
        $status = "completed";
        $stmt = $conn->prepare("UPDATE user_training_attempts SET score= ?, status= ? WHERE id= ?");
        $stmt->bind_param("sss", $score, $status, $last_id);
        $stmt->execute();
        $stmt->close();
        
        mysqli_close($conn);
        echo json_encode(["status" => "success", "message" => "Respostas salvas com sucesso!"]);
    } else {
        echo json_encode(["status" => "error", "message" => "Método inválido"]);
    }
