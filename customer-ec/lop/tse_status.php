<?php
// seu_script_de_salvamento.php
include("database/database.php");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Obter os parâmetros da solicitação POST
    $fieldName = $_POST['fieldName'];
    $value = $_POST['value'];
    $id = $_POST['id'];

    // Adicione aqui a lógica para salvar os dados no banco de dados
    // Substitua o código abaixo com a lógica real de atualização no banco de dados
    // Certifique-se de usar medidas para evitar injeção de SQL (consulte Prepared Statements)
 

    $sql = "UPDATE tse_report SET $fieldName = ? WHERE id = ?";
    
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param('si', $value, $id);
        
        if ($stmt->execute()) {
            // A atualização foi bem-sucedida
            echo json_encode(['success' => true]);
        } else {
            // Erro na execução da atualização
            echo json_encode(['error' => 'Erro na execução da atualização']);
        }

        $stmt->close();
    } else {
        // Erro na preparação da consulta
        echo json_encode(['error' => 'Erro na preparação da consulta']);
    }

    $conn->close();
} else {
    // Responda apenas a solicitações POST
    http_response_code(405); // Método Não Permitido
    echo json_encode(['error' => 'Método Não Permitido']);
}
?>
