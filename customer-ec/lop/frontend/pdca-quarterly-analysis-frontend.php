<?php
include("database/database.php");
$row=1;
$query = "SELECT  GROUP_CONCAT(regdate_quartely_dropzone.file SEPARATOR ';') AS 'id_anexo' ,quarterly_analysis.id,instituicao.instituicao,colaborador.primeironome,colaborador.ultimonome, kpi_year.yr,quarterly.nome,quarterly_analysis.analysis,quarterly_analysis.action,quarterly_analysis.data_after,quarterly_analysis.data_execution,quarterly_analysis.upgrade,quarterly_analysis.reg_date FROM quarterly_analysis INNER JOIN quarterly ON quarterly.id = quarterly_analysis.id_quarterly INNER JOIN kpi_year ON kpi_year.id = quarterly_analysis.id_kpi_year INNER JOIN colaborador ON colaborador.id = quarterly_analysis.id_colaborador INNER JOIN instituicao ON instituicao.id = quarterly_analysis.id_instituicao LEFT JOIN regdate_quartely_dropzone ON regdate_quartely_dropzone.id_quartely = quarterly_analysis.id  GROUP BY quarterly_analysis.id ORDER BY  quarterly_analysis.id DESC  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($anexo,$id,$instituicao,$primeironome,$ultimonome,$kpi_year,$quarterly,$analysis,$action,$data_after,$date_execution,$upgrade,$reg_date);


  ?>

  <!-- 1 -->
  <link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
  <div class="right_col" role="main">

    <div class="">

      <div class="page-title">
        <div class="title_left">
          <h3>Analise</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5  form-group pull-right top_search">
            <div class="input-group">

              <span class="input-group-btn">

              </span>
            </div>
          </div>
        </div>
      </div>


      <div class="x_panel">
        <div class="x_title">
          <h2>Menu</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app" href="pdca-quarterly-analysis" onclick="new PNotify({
              title: 'Analises',
              text: 'Analises',
              type: 'success',
              styling: 'bootstrap3'
            });">
            <i class="fa fa-cube"></i> Analises
          </a>

          <a class="btn btn-app" href="pdca-quarterly-report" onclick="new PNotify({
              title: 'Relatório',
              text: 'Relatório',
              type: 'success',
              styling: 'bootstrap3'
            });">
            <i class="fa fa-bar-chart"></i> Relatório
          </a>





        </div>
      </div>



      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>




      <div class="x_panel">
        <div class="x_title">
          <h2>Lista de Analises</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                  class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Anexo</th>
                    <th>Ano</th>
                    <th>Trimestre</th>
                      <?php if($assistence == "0"){  ?>
                          <th>Empresa</th>
                            <?php }else {   ?>
                          <th>Unidade</th>
                            <?php }  ?>	                  <!--  <th>Análise</th>
                    <th>Ação</th> -->
                    <th>Responsável</th>
                    <th>Data Prevista</th>
                    <th>Data de Execução</th>
                    <th>Atualização</th>
                    <th>Cadastro</th>
                    <th>Ação</th>


                  </tr>
                </thead>
                <tbody>
                  <?php  while ($stmt->fetch()) { ?>

                    <tr>
                      <th scope="row"><?php printf($row); ?></th>
                        <td>
                            <?php 
                                if ($anexo != "") {
                                    $files = explode(';', $anexo); // separa os arquivos pela vírgula
                                    foreach ($files as $file) {
                                        $file = trim($file); // remove espaços em branco extras
                                        if ($file != "") {
                                            echo '<a href="dropzone/quarterly/' . htmlspecialchars($file) . '" download><i class="fa fa-paperclip"></i> ' . htmlspecialchars($file) . '</a><br>';
                                        }
                                    }
                                }
                            ?>
                        </td>

                      <td><?php printf($kpi_year); ?></td>
                      <td><?php printf($quarterly); ?></td>
                      <td><?php printf($instituicao); ?></td>

                 <!--     <td><?php //echo htmlspecialchars_decode($analysis); ?></td>
                      <td><?php //echo htmlspecialchars_decode($action); ?></td> -->
                      <td><?php printf($primeironome); ?> <?php printf($ultimonome); ?></td>
                      <td><?php printf($data_after); ?></td>
                        <td><?php printf($date_execution); ?></td>
                      <td><?php printf($upgrade); ?></td>
                      <td><?php printf($reg_date); ?></td>


                      <td>

                        <a class="btn btn-app"  href="pdca-quarterly-analysis-edit?id=<?php printf($id); ?>"onclick="new PNotify({
                          title: 'Atualização',
                          text: 'Atualização!',
                          type: 'info',
                          styling: 'bootstrap3'
                        });">
                        <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                      </a>
                      <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/pdca-quarterly-analysis-trash.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                    <a  class="btn btn-app" href="pdca-quarterly-analysis-viewer?id=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Analise!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  </td>
                </tr>
                <?php  $row=$row+1; }
              }   ?>
            </tbody>
          </table>







          <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">

                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">PDCA</h4>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">

	<div class="clearfix"></div>
                <form  class="dropzone" action="backend/pdca-quarterly-analysis-upload-backend.php" method="post">
                    </form >
<div class="ln_solid"></div>
                  <form action="backend/pdca-quarterly-analysis-backend.php" method="post">
                    <div class="ln_solid"></div>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_quarterly">Trimestre <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_quarterly" id="id_quarterly"  placeholder="Trimestre">
                          <option value="">Selecione um Trimestre</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM quarterly";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Trimestre "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_kpi_year">Ano <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_kpi_year" id="id_kpi_year"  placeholder="Ano">
                          <option value="">Selecione um Ano</option>
                          <?php



                          $result_cat_post  = "SELECT  id, yr FROM kpi_year ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['yr'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Ano "></span>

                      </div>
                    </div>
                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>
                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_colaborador">colaborador <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador">
                          <option value="">Selecione um Colaborador</option>
                          <?php



                          $result_cat_post  = "SELECT  id, primeironome, ultimonome FROM colaborador  WHERE trash = 1 ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['primeironome'].' '.$row_cat_post['ultimonome'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Colaborador "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_instituicao">Unidade <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_instituicao" id="id_instituicao"  placeholder="Unidade">
                        <?php if($assistence == "0"){  ?>
            <option value="">Selecione uma Opção</option>
            <?php }else {   ?>
            <option value="">Selecione uma Opção</option>            
            <?php }  ?>
                          <?php



                          $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Unidade "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>

                         <label for="analysis">Análise :</label>
                          <textarea id="analysis" class="form-control" name="analysis" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="20"></textarea>
                          
                            <label for="action">Ação:</label>
                          <textarea id="action" class="form-control" name="action" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="20" ></textarea>

                     
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Prevista</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="data_after" class="form-control" type="date" name="data_after"   >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Execução</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="data_execution" class="form-control" type="date" name="data_execution"   >
                      </div>
                    </div>






                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary" onclick="new PNotify({
                      title: 'Registrado',
                      text: 'Informações registrada!',
                      type: 'success',
                      styling: 'bootstrap3'
                    });" >Salvar Informações</button>
                  </div>

                </div>
              </div>
            </div>
          </form>


        </div>
      </div>




    </div>
  </div>
  <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    },
        initComplete: function () {
            this.api()
                .columns([1,2,3,4,5,6])
                .every(function (d) {
                    var column = this;
                  var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      }); 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                
                
        },
    });
});


</script>



















</div>
</div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
