<?php
$codigoget = ($_GET["routine"]);


// Create connection

include("database/database.php");
$query = "SELECT calibration_routine.periodicidade_after,calibration_routine.data_after,calibration_routine.time_after,calibration_routine.id_pop,calibration_routine.id_calibration_procedures_after,calibration_routine.id_calibration_procedures_before,calibration_routine.id_fornecedor,calibration_routine.id_colaborador,calibration_routine.id_category,calibration_routine.habilitado,calibration_routine.time_ms,calibration_routine.id,calibration_routine.data_start, calibration_routine.periodicidade,calibration_routine.reg_date,calibration_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM calibration_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id WHERE calibration_routine.id like'$codigoget'";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($periodicidade_after,$data_after,$time_after,$id_pop,$id_calibration_procedures_after,$id_calibration_procedures_before,$id_fornecedor,$id_colaborador,$id_category,$habilitado,$time_ms,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
 while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
//  }
}
}

?>

<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>  Alteração <small>Procedimento</small></h3>
            </div>


          </div>

          <div class="clearfix"></div>

          <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Cabeçalho <small>Procedimento</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form action="backend/calibration-routine-reschedule-backend.php?routine=<?php printf($codigoget);?>" method="post">

                  <div class="ln_solid"></div>
                                 <div class="item form-group">
                                         <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                                         </label>
                                         <div class="col-md-6 col-sm-6 ">
                                           <input type="text" id="nome" name="nome" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                                         </div>
                                       </div>

                                       <div class="item form-group">
                                         <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                                         </label>
                                         <div class="col-md-6 col-sm-6 ">
                                           <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
                                         </div>
                                       </div>





                  <div class="ln_solid"></div>
                                                  <small>Procedimento com Laudo dentro da Validade</small>

                                                   <div class="item form-group">
                                         <label class="col-form-label col-md-3 col-sm-3 label-align">Procedimento 1º</label>
                                         <div class="col-md-6 col-sm-6 ">

                                             <label>
                                                   <select type="text" class="form-control has-feedback-right" name="procedimento_1" id="procedimento_1"  placeholder="Procedimento" readonly="readonly" value="">
                                    <option value="">Selecione um procedimento</option>
                                     <?php





                                        $sql = "SELECT  id, name, codigo FROM maintenance_procedures WHERE trash =1 ";


                  if ($stmt = $conn->prepare($sql)) {
                     $stmt->execute();
                      $stmt->bind_result($id_procedimento,$procedimento,$codigo);
                      while ($stmt->fetch()) {
                      ?>
                  <option value="<?php printf($id_procedimento);?>	" <?php if($id_procedimento == $id_maintenance_procedures_before){ printf("selected");};?>><?php printf($codigo);?> - <?php printf($procedimento);?>	</option>
                                         <?php
                                       // tira o resultado da busca da memória
                                       }

                                                             }
                                       $stmt->close();

                                       ?>
                                                         </select>
                                         </div>
                                         </div>

                                          <script>
                                                     $(document).ready(function() {
                                                     $('#procedimento_1').select2();
                                                       });
                                                  </script>

                                                  <div class="item form-group">
                                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio<span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 ">
                                                      <input type="date" id="date_start" name="date_start" required="required" class="form-control"  value="<?php printf($data_start); ?>">
                                                    </div>
                                                  </div>

                                                    <div class="item form-group">
                                                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="backlog" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Tempo <span></span>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 ">
                                                      <input type="text" id="timepicker1" name="time_ms"  class="form-control " value="<?php printf($time_ms); ?>"  pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99">
                                                    </div>
                                                  </div>

                                                 <div class="item form-group">
                                                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade</label>
                                                    <div class="col-md-6 col-sm-6 ">
                                                      <select class="form-control has-feedback-right" tabindex="-1" name="periodicidade" id="periodicidade">
                                                        <option value="">Selecione uma periodicidade</option>
                                                        <option value="1"<?php if($periodicidade == 1){ printf("selected");};?>>Diaria [1 Dias]</option>
                                                        <option value="5"<?php if($periodicidade == 5){ printf("selected");};?>>Diaria [1 Dias Dias]</option>
                                                        <option value="7"<?php if($periodicidade == 7){ printf("selected");};?>>Semanal [7 Dias]</option>
                                                        <option value="14"<?php if($periodicidade == 14){ printf("selected");};?>>Bisemanal [14 Dias]</option>
                                                        <option value="21"<?php if($periodicidade == 21){ printf("selected");};?>>Trisemanal [21 Dias]</option>
                                                        <option value="28"<?php if($periodicidade == 28){ printf("selected");};?>>Quadrisemanal [28 Dias]</option>
                                                        <option value="30"<?php if($periodicidade == 30){ printf("selected");};?>>Mensal [30 Dias]</option>
                                                        <option value="60"<?php if($periodicidade == 60){ printf("selected");};?>>Bimensal [60 Dias]</option>
                                                        <option value="90"<?php if($periodicidade == 90){ printf("selected");};?>>Trimestral [90 Dias]</option>
                                                        <option value="120"<?php if($periodicidade == 120){ printf("selected");};?>>Quadrimestral [120 Dias]</option>
                                                        <option value="180"<?php if($periodicidade == 180){ printf("selected");};?>>Semestral [180 Dias]</option>
                                                        <option value="365"<?php if($periodicidade == 365){ printf("selected");};?>>Anual [365 Dias]</option>
                                                        <option value="730"<?php if($periodicidade == 730){ printf("selected");};?>>Bianual [730 Dias]</option>
                                                        <option value="1095"<?php if($periodicidade == 1095){ printf("selected");};?>>Trianual [1095 Dias]</option>
                                                        <option value="1460"<?php if($periodicidade == 1460){ printf("selected");};?>>Quadrianual [1460 Dias]</option>
                                                        


                                                      </select>
                                                       <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Periodicidade "></span>

                                                    </div>
                                                 </div>
                                                  <script>
                                                                $(document).ready(function() {
                                                                $('#periodicidade').select2();
                                                                  });
                                                             </script>
                                                             <div class="ln_solid"></div>
                                                                                            <small>Procedimento com Laudo fora da Validade</small>

                                     <div class="item form-group">
                                         <label class="col-form-label col-md-3 col-sm-3 label-align">Procedimento 2º</label>
                                         <div class="col-md-6 col-sm-6 ">

                                             <label>
                                           <select type="text" class="form-control has-feedback-right" name="procedimento_2" id="procedimento_2" readonly="readonly"  placeholder="Procedimento" value="">
                                    <option value="">Selecione um procedimento</option>
                                     <?php





                                        $sql = "SELECT  id, name, codigo FROM maintenance_procedures WHERE trash =1";


                  if ($stmt = $conn->prepare($sql)) {
                     $stmt->execute();
                      $stmt->bind_result($id_procedimento,$procedimento,$codigo);
                      while ($stmt->fetch()) {
                      ?>
                  <option value="<?php printf($id_procedimento);?>	" <?php if($id_procedimento == $id_maintenance_procedures_after){ printf("selected");};?>><?php printf($codigo);?> - <?php printf($procedimento);?>	</option>
                                         <?php
                                       // tira o resultado da busca da memória
                                       }

                                                             }
                                       $stmt->close();

                                       ?>
                                                         </select>
                                                         </div>
                                         </div>

                                          <script>
                                                     $(document).ready(function() {
                                                     $('#procedimento_2').select2();
                                                       });
                                                  </script>



                                                     <div class="ln_solid"></div>


                                                   <div class="item form-group">
                                         <label class="col-form-label col-md-3 col-sm-3 label-align">POP </label>
                                         <div class="col-md-6 col-sm-6 ">

                                             <label>
                                           <select type="text" class="form-control has-feedback-right" name="pop" id="pop" readonly="readonly"  placeholder="pop" value="">
                                    <option value="">Selecione um pop</option>
                                     <?php





                                        $sql = "SELECT  id, titulo  FROM documentation_pop ";


                  if ($stmt = $conn->prepare($sql)) {
                     $stmt->execute();
                      $stmt->bind_result($id,$titulo);
                      while ($stmt->fetch()) {
                      ?>
                  <option value="<?php printf($id);?>	" <?php if($id == $id_pop){ printf("selected");};?>>  <?php printf($titulo);?>	</option>
                                         <?php
                                       // tira o resultado da busca da memória
                                       }

                                                             }
                                       $stmt->close();

                                       ?>
                                                         </select>
                                                         </div>
                                         </div>

                                          <script>
                                                     $(document).ready(function() {
                                                     $('#pop').select2();
                                                       });
                                                  </script>

                                        <div class="item form-group">
                                         <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                                         <div class="col-md-6 col-sm-6 ">
                                           <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                                         </div>
                                       </div>
                                        <div class="item form-group">
                                         <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                                         <div class="col-md-6 col-sm-6 ">
                                           <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                                         </div>
                                       </div>
                  <div class="ln_solid"></div>
                              <div class="form-group row">
                                        <label class="col-form-label col-md-3 col-sm-3 "></label>
                                         <div class="col-md-3 col-sm-3 ">
                                             <center>
                                            <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                                          </center>
                                         </div>
                                       </div>
                  </form>
                                   </div>
                                 </div>
                               </div>
                          </div>





            <!-- Posicionamento -->



           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="flow-calibration-routine">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>



            <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
                              title: 'Cancelamento',
                              text: 'Cancelamento de Abertura de O.S!',
                              type: 'error',
                              styling: 'bootstrap3'
                          });">
                  <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                </a>
                  <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
                              title: 'Visualizar',
                              text: 'Visualizar O.S!',
                              type: 'info',
                              styling: 'bootstrap3'
                          });" >
                  <i class="fa fa-file-pdf-o"></i> Visualizar
                </a> -->

              </div>
            </div>




              </div>
            </div>
                                                