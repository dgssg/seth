
<?php

include("database/database.php");

$codigoget= $_GET['id'];
$assessment= $_GET['assessment'];
//$id_fornecedor= $_GET['id_fornecedor'];
$id_fornecedor=$_COOKIE['assessment'];



?>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />

  <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>


  <!-- Bootstrap -->
  <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- bootstrap-daterangepicker -->
  <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- iCheck -->
  <link href="../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

  <!-- bootstrap-progressbar -->
  <link href="../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
  <!-- JQVMap -->
  <link href="../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
  <!-- bootstrap-daterangepicker -->
  <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="../../framework/build/css/custom.min.css" rel="stylesheet">

  <!-- PNotify -->
  <link href="../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <!-- <script src="jquery.min.js" ></script> -->
  <!-- Datatables -->

  <link href="../../framework/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <link href="../../framework/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
  <link href="../../framework/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
  <link href="../../framework/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
  <link href="../../framework/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  <!-- Switchery -->
  <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
</head>
<body class="nav-md">


  <script type="text/javascript" src="jquery-1.6.4.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#tools').change(function(){
      $('#parameter').load('sub_categorias_post_tools.php?tools='+$('#tools').val());
    });
  });
  </script>
  <form action="backend/assessment_manufacture-backend.php" method="post">
    <div class="ln_solid"></div>
    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Avaliação</label>
      <div class="col-md-4 col-sm-4 ">
        <select type="text" class="form-control has-feedback-left" name="assessment" id="assessment" readonly="readonly"  >
          <option value="	">Selecione a Avaliação</option>
          <option value="1"<?php if ($assessment==1) { printf("selected");} ?>>Ordem de Serviço</option>
          <option value="2"<?php if ($assessment==2) { printf("selected");} ?>>Manutenção Preventiva</option>
          <option value="3"<?php if ($assessment==3) { printf("selected");} ?>>Calibração</option>

        </select>
        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Avaliação "></span>


      </div>
    </div>

    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Numero</label>
      <div class="col-md-4 col-sm-4 ">
        <input id="number_assessment" class="form-control" type="text" name="number_assessment" value="<?php printf($codigoget); ?>" readonly="readonly" >
      </div>
    </div>
    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data</label>
      <div class="col-md-4 col-sm-4 ">
        <input id="data_assessment" class="form-control" type="date" name="data_assessment"  >
      </div>
    </div>
    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
      <div class="col-md-4 col-sm-4 ">
        <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  readonly="readonly" >

          <?php



          $sql = "SELECT  id, empresa FROM fornecedor WHERE id = $id_fornecedor ";


          if ($stmt = $conn->prepare($sql)) {
            $stmt->execute();
            $stmt->bind_result($id,$nome);
            while ($stmt->fetch()) {
              ?>
              <option value="<?php printf($id);?>	" selected ><?php printf($nome);?>	</option>
              <?php
              // tira o resultado da busca da mem贸ria
            }

          }
          $stmt->close();

          ?>
        </select>
        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>


      </div>
    </div>

    <script>
    $(document).ready(function() {
      $('#id_manufacture').select2();
    });
    </script>
    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de Fornecimento</label>
      <div class="col-md-4 col-sm-4 ">
        <select type="text" class="form-control has-feedback-left" name="fornecedor_service" id="fornecedor_service"  >
          <option value="	">Selecione o tipo</option>
          <?php



          $sql = "SELECT  id, service FROM fornecedor_service ";


          if ($stmt = $conn->prepare($sql)) {
            $stmt->execute();
            $stmt->bind_result($id,$nome);
            while ($stmt->fetch()) {
              ?>
              <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
              <?php
              // tira o resultado da busca da mem贸ria
            }

          }
          $stmt->close();

          ?>
        </select>
        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o tipo "></span>


      </div>
    </div>

    <script>
    $(document).ready(function() {
      $('#fornecedor_service').select2();
    });
    </script>

    <!--  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
    <div class="x_title">
    <h2>Avaliação <small>Fornecedor</small></h2>
    <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
  </li>
  <li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  <a class="dropdown-item" href="#">Settings 1</a>
  <a class="dropdown-item" href="#">Settings 2</a>
</div>
</li>
<li><a class="close-link"><i class="fa fa-close"></i></a>
</li>
</ul>
<div class="clearfix"></div>
</div>

<div class="x_content">

<p>Questionário <code>Avaliação</code> do fornecedor</p>

<div class="table-responsive">
<table class="table table-striped jambo_table bulk_action">
<thead>
<tr class="headings">
<th>
<input type="checkbox" id="check-all" class="flat">
</th>
<th class="column-title">Pergunta </th>
<th class="column-title">Avaliação</th>

<th class="column-title no-link last"><span class="nobr">Action</span>
</th>
<th class="bulk-actions" colspan="7">
<a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
</th>
</tr>
</thead>

<tbody id="fornecedor_service_question">
<tr class="even pointer">  <td class="a-center ">
<input type="checkbox" class="flat" name="table_records">
</td>
<td class=" "></td>
<td class=" "></td>
<td class=" last"><a href="#"></a>
</td> </tr>



</tbody>
</table>
</div>
</div>
</div>
</div> -->

<div class="item form-group" >
 
  <div class="col-md-12 col-sm-12 " >
    <span id="fornecedor_service_question"></span>
  </div>
</div>
<div class="ln_solid"></div>
<div class="item form-group" >
<label for="message_mp">Observação:</label>
            <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
            data-parsley-validation-threshold="10" placeholder=""></textarea>
            <br>

            </div>

<div class="modal-footer">

  <button type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informações registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" >Salvar Informações</button>
</div>

</form>

<script type="text/javascript">
$(document).ready(function(){
  $('#fornecedor_service').change(function(){
    $('#fornecedor_service_question').load('sub_categorias_post_fornecedor_service.php?id='+$('#fornecedor_service').val());
  });
});
</script>

<!-- jQuery
<script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../../framework/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- jQuery Sparklines -->
<script src="../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- Flot -->
<script src="../../framework/vendors/Flot/jquery.flot.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../../framework/vendors/DateJS/build/date.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../framework/vendors/moment/min/moment.min.js"></script>
<script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../framework/build/js/custom.min.js"></script>




<!-- jQuery-->
<script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap-
<script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../../framework/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../../framework/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../../framework/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../../framework/vendors/Flot/jquery.flot.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../../framework/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../framework/vendors/moment/min/moment.min.js"></script>
<script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../framework/build/js/custom.min.js"></script>
<!-- PNotify -->
<script src="../../framework/vendors/pnotify/dist/pnotify.js"></script>
<script src="../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<script src="../../framework/assets/js/select2.min.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="../../framework/vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Switchery -->
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="../../framework/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="../../framework/vendors/autosize/dist/autosize.min.js"></script>
<!-- jQuery autocomplete -->
<script src="../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<!-- starrr -->
<script src="../../framework/vendors/starrr/dist/starrr.js"></script>
<!-- Contador de caracter -->
<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
<!-- Datatables -->
<script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
<script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- PNotify -->
<script src="../../framework/vendors/pnotify/dist/pnotify.js"></script>
<script src="../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>


</body>
</html>
