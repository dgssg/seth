<?php
// Informações do banco de dados
$servername = "localhost";
$username = "cvheal47_root";
$password = "cvheal47_root";
$port = 3306;
$socket = "";
    
// Dependendo do nível de permissão, encaminha para a pasta correspondente
if ($nivelPermissao == '1') {
    showAlert("Redirecionando para admin");
   header('location:customer-ec/'.$instituicao.'/dashboard');
    exit();
} elseif ($nivelPermissao == '2') {
    header('location:../customer-ec/'.$instituicao.'/stakeholder/technician/dashboard');
    exit();
} elseif ($nivelPermissao == '3') {
     header('location:../customer-ec/'.$instituicao.'/stakeholder/user/dashboard');
    exit();
} elseif ($nivelPermissao == '4') {
     header('location:../customer-ec/'.$instituicao.'/stakeholder/manager/dashboard');
    exit();
} elseif ($nivelPermissao == '5') {
     header('location:../customer-ec/'.$instituicao.'/stakeholder/supervision/dashboard');
    exit();
} else {
    echo "Nível de permissão inválido.";
    exit();
}
?>
