
<html>
<head>
  <title>SETH</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="mqttws31.js" type="text/javascript"></script>
  <script src="jquery.min.js" type="text/javascript"></script>
  <script src="config.js" type="text/javascript"></script>

  <script type="text/javascript">
  var mqtt;
  var reconnectTimeout = 2000;

  function MQTTconnect() {
    if (typeof path == "undefined") {
      path = '/mqtt';
    }
    mqtt = new Paho.MQTT.Client(
      host,
      port,
      path,
      "web_" + parseInt(Math.random() * 100, 10)
    );
    var options = {
      timeout: 3,
      useSSL: useTLS,
      cleanSession: cleansession,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(MQTTconnect, reconnectTimeout);
      }
    };

    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;


    if (username != null) {
      options.userName = username;
      options.password = password;
    }
    console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
    mqtt.connect(options);
  }

  function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic
    mqtt.subscribe(lwt_availability, {qos: 0});
    $('#lwt_availability').val(lwt_availability);
    mqtt.subscribe(r1, {qos: 0});
    $('#r1').val(r1);
    mqtt.subscribe(r2, {qos: 0});
    $('#r2').val(r2);
    mqtt.subscribe(r3, {qos: 0});
    $('#r3').val(r3);
    mqtt.subscribe(r4, {qos: 0});
    $('#r4').val(r4);
    mqtt.subscribe(relay1, {qos: 0});
    $('#relay1').val(relay1);
    mqtt.subscribe(relay2, {qos: 0});
    $('#relay2').val(relay2);
    mqtt.subscribe(relay3, {qos: 0});
    $('#relay3').val(relay3);
    mqtt.subscribe(relay4, {qos: 0});
    $('#relay4').val(relay4);
    mqtt.subscribe(i1, {qos: 0});
    $('#i1').val(i1);
    mqtt.subscribe(i2, {qos: 0});
    $('#i2').val(i2);
    mqtt.subscribe(i3, {qos: 0});
    $('#i3').val(i3);
    mqtt.subscribe(i4, {qos: 0});
    $('#i4').val(i4);
    mqtt.subscribe(input1, {qos: 0});
    $('#input1').val(input1);
    mqtt.subscribe(input2, {qos: 0});
    $('#input2').val(input2);
    mqtt.subscribe(input3, {qos: 0});
    $('#input3').val(input3);
    mqtt.subscribe(input4, {qos: 0});
    $('#input4').val(input4);
    mqtt.subscribe(ip, {qos: 0});
    $('#ip').val(ip);
    mqtt.subscribe(sn, {qos: 0});
    $('#sn').val(sn);
    mqtt.subscribe(mac, {qos: 0});
    $('#mac').val(mac);
    mqtt.subscribe(input_cnt, {qos: 0});
    $('#input_cnt').val(input_cnt);
    mqtt.subscribe(relay_cnt, {qos: 0});
    $('#relay_cnt').val(relay_cnt);


  }

  function onConnectionLost(response) {
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

  };

  function onMessageArrived(message) {

    var topic = message.destinationName;
    var payload = message.payloadString;
    if(topic==r1){
      var  r1_payload = message.payloadString;
    }
    if(topic==r2){
      var  r2_payload = message.payloadString;
    }
    if(topic==r3){
      var  r3_payload = message.payloadString;
    }
    if(topic==r4){
      var  r4_payload = message.payloadString;
    }
    if(topic==sn){
      var  sn_payload = message.payloadString;
    }
    if(topic==ip){
      var  ip_payload = message.payloadString;
    }
    if(topic==mac){
      var  mac_payload = message.payloadString;
    }


    $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');
    $('#sn_payload').val(sn_payload);
    $('#ip_payload').val(ip_payload);
    $('#r1_payload').val(r1_payload);
    $('#r2_payload').val(r2_payload);
    $('#r3_payload').val(r3_payload);
    $('#r4_payload').val(r4_payload);
    $('#mac_payload').val(mac_payload);


  };


  $(document).ready(function() {
    MQTTconnect();

  });

  </script>
</head>
<body>
  <h1>SETH CPC</h1>
  <div>
    <div>Subscribed to <input type='text' id='topic' disabled />
      Status: <input type='text' id='status' size="80" disabled /></div>
      <div>SN <input type='text' id='sn_payload' disabled />
        IP: <input type='text' id='ip_payload'  disabled />
        R1: <input type='text' id='r1_payload'  disabled />
        R2: <input type='text' id='r2_payload'  disabled />
        R3: <input type='text' id='r3_payload'  disabled />
        R4: <input type='text' id='r4_payload'  disabled /></div>


        <ul id='ws' style="font-family: 'Courier New', Courier, monospace;"></ul>
      </div>

    </body>
    </html>
