<?php
// Faça a conexão com o banco de dados aqui
include("../database/database.php");
 ?>
 <!-- page content -->
<style>
    body {
        background: url('background.png') no-repeat center center fixed;
        background-size: cover;
        color: #ffffff;
    }
    .column-title {
            color: #ffffff; /* Define a cor do texto para branco */
        }

    .alert {
        background-color: rgba(0, 0, 0, 0.5);
        margin-left: 40px;
    }

    .tile-stats {
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 10px;
        padding: 20px;
        text-align: center;
        margin-left: 40px;
    }

    .graph-container {
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 10px;
        padding: 20px;
        margin-left: 40px;
    }

    .graph-background {
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 10px;
        padding: 10px;
        margin-left: 40px;
    }

    .graph-container .google-visualization-chart-wrapper,
    .graph-container .google-visualization-table,
    .graph-background .google-visualization-chart-wrapper,
    .graph-background .google-visualization-table {
        border: none;
    }

    .graph-container .google-visualization-chart {
        background-color: transparent;
    }

    .graph-container .google-visualization-table {
        color: #ffffff;
        background-color: rgba(0, 0, 0, 0.5) !important;
    }

    .graph-background .google-visualization-chart {
        background-color: transparent;
    }

    .graph-background .google-visualization-table {
        color: #ffffff;
        background-color: rgba(0, 0, 0, 0.5) !important;
    }

    .table-header {
        background-color: #333;
        color: #fff;
    }
    
    .table-row {
        background-color: rgba(0, 0, 0, 0.5);
        color: #fff;
    }

    .odd-table-row {
        background-color: rgba(0, 0, 0, 0.4);
    }

    .selected-table-row {
        background-color: #555;
    }

    .hover-table-row:hover {
        background-color: #666;
    }

    .google-visualization-table {
        background-color: rgba(0, 0, 0, 0.5);
    }

    .google-visualization-table table {
        background-color: rgba(0, 0, 0, 0.5);
    }

    .google-visualization-table table td,
    .google-visualization-table table th {
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff;
    }

    .x_panel {
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 10px;
        padding: 15px;
    }

    .x_title {
        background-color: rgba(0, 0, 0, 0.5);
        padding: 10px;
        border-bottom: 1px solid rgba(255, 255, 255, 0.2);
    }

    .x_title h2 {
        color: #fff;
    }

    .x_content {
        background-color: rgba(0, 0, 0, 0.5);
        padding: 15px;
        border-radius: 10px;
    }

    .x_panel .nav > li > a {
        color: #fff;
    }

    .x_panel .dropdown-menu {
        background-color: rgba(0, 0, 0, 0.5);
    }

    .x_panel .dropdown-menu a {
        color: #fff;
    }

    .media.event .pull-left.date {
        background-color: rgba(0, 0, 0, 0.5);
        border-radius: 5px;
        padding: 5px;
        text-align: center;
        color: #fff;
    }

    .media.event .media-body .title {
        color: #fff;
    }

    .media.event .media-body p {
        color: #fff;
    }
</style>
<?php
include("database/database.php");

?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

      <script type="text/javascript">
      google.charts.load('current', {'packages':['treemap']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Location', 'Parent', 'Market trade volume (size)', 'Market increase/decrease (color)'],
          ['Global',    null,                 0,                               0],
          ['America',   'Global',             0,                               0],
          ['Europe',    'Global',             0,                               0],
          ['Asia',      'Global',             0,                               0],
          ['Australia', 'Global',             0,                               0],
          ['Africa',    'Global',             0,                               0],
          ['Brazil',    'America',            11,                              10],
          ['USA',       'America',            52,                              31],
          ['Mexico',    'America',            24,                              12],
          ['Canada',    'America',            16,                              -23],
          ['France',    'Europe',             42,                              -11],
          ['Germany',   'Europe',             31,                              -2],
          ['Sweden',    'Europe',             22,                              -13],
          ['Italy',     'Europe',             17,                              4],
          ['UK',        'Europe',             21,                              -5],
          ['China',     'Asia',               36,                              4],
          ['Japan',     'Asia',               20,                              -12],
          ['India',     'Asia',               40,                              63],
          ['Laos',      'Asia',               4,                               34],
          ['Mongolia',  'Asia',               1,                               -5],
          ['Israel',    'Asia',               12,                              24],
          ['Iran',      'Asia',               18,                              13],
          ['Pakistan',  'Asia',               11,                              -52],
          ['Egypt',     'Africa',             21,                              0],
          ['S. Africa', 'Africa',             30,                              43],
          ['Sudan',     'Africa',             12,                              2],
          ['Congo',     'Africa',             10,                              12],
          ['Zaire',     'Africa',             8,                               10]
        ]);

        tree = new google.visualization.TreeMap(document.getElementById('chart_div'));

        tree.draw(data, {
          minColor: '#f00',
          midColor: '#ddd',
          maxColor: '#0d0',
          headerHeight: 15,
          fontColor: 'black',
          showScale: true
        });

      }
      </script>
      <div class="row" style="display: inline-block;" >
      <div class="tile_count">
      <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>

        <i class="ace-icon fa fa-check red"></i>

        Bem vindo ao
        <strong class="black">
          APOLLO
          <small>(PCP)</small>
        </strong>,
        Análise e Planejamento Operacional para Lotação de Leitos e Ocupação <a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas Biomédicos</a> .
      </div>
        </div>
          </div>
          <?php
      $query = "SELECT nivel.id,nivel.color_pcp,nivel.nivel_pcp,device.id,  unidade.instituicao, setor.nome, area.nome, device.upgrade, device.reg_date FROM device INNER JOIN area ON device.id_area = area.id INNER JOIN setor ON setor.id = area.id_area INNER JOIN unidade ON unidade.id = setor.id_unidade INNER JOIN nivel on nivel.id = device.id_nivel where device.trash = 0 and device.status = 1 ORDER BY device.upgrade LIMIT 1 ";
          //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($nivelid,$color,$nivel,$id, $unidade,$setor,$area,$update,$reg_date);

            //printf("%s, %s\n", $solicitante, $equipamento);

            ?>
<div class="row" style="display: block;">
          <div class="col-md-5 col-sm-5  ">
                         <div class="x_panel">
                           <div class="x_title">
                             <h2>Ultima Atualização<small>de Mudança de Nivel</small></h2>
                             <ul class="nav navbar-right panel_toolbox">
                               <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                               </li>
                               <li class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                     <a class="dropdown-item" href="#">Settings 1</a>
                                     <a class="dropdown-item" href="#">Settings 2</a>
                                   </div>
                               </li>
                               <li><a class="close-link"><i class="fa fa-close"></i></a>
                               </li>
                             </ul>
                             <div class="clearfix"></div>
                           </div>
                           <div class="x_content">

                             <table class="table table-striped">
                               <thead>
                                 <tr>
                                   <th class="column-title">#</th>
                                   <th class="column-title">Atualização</th>

                                   <th class="column-title">Nivel Atual</th>
                                 </tr>
                               </thead>
                               <tbody>
                                 <?php   while ($stmt->fetch()) { ?>
                               <tr>
                                 <th class="column-title" scope="row"></th>
                                 <th class="column-title"> <?php printf($update); ?></th>
                                   <th class="column-title"><?php printf($nivel); ?> </th>


                                 </tr>
                               <?php
                            }}
                              ?>


                               </tbody>
                             </table>

                           </div>
                         </div>
                       </div>

                       <?php
                   $query = "SELECT nivel.id,nivel.color_pcp,nivel.nivel_pcp,device.id,  unidade.instituicao, setor.nome, area.nome, device.upgrade, device.reg_date FROM device INNER JOIN area ON device.id_area = area.id INNER JOIN setor ON setor.id = area.id_area INNER JOIN unidade ON unidade.id = setor.id_unidade INNER JOIN nivel on nivel.id = device.id_nivel where device.trash = 0 and device.status = 1 and device.id_nivel = 4 ";
                       //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
                       if ($stmt = $conn->prepare($query)) {
                         $stmt->execute();
                         $stmt->bind_result($nivelid,$color,$nivel,$id, $unidade,$setor,$area,$update,$reg_date);

                         //printf("%s, %s\n", $solicitante, $equipamento);

                         ?>
                  <!--     <div class="col-md-5 col-sm-5  ">
                                      <div class="x_panel">
                                        <div class="x_title">
                                          <h2>Area <small>Nivel 4</small></h2>
                                          <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                  <a class="dropdown-item" href="#">Settings 1</a>
                                                  <a class="dropdown-item" href="#">Settings 2</a>
                                                </div>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                          </ul>
                                          <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                          <table class="table table-striped">
                                            <thead>
                                              <tr>
                                                <th>#</th>
                                                <th>Localização</th>


                                              </tr>
                                            </thead>
                                            <tbody>
                                                <?php   while ($stmt->fetch()) { ?>
                                              <tr>
                                                <th scope="row"></th>
                                                <td><?php printf($unidade); ?> - <?php printf($setor); ?> - <?php printf($area); ?></td>


                                              </tr>
                                            <?php
                                         }}
                                           ?>

                                            </tbody>
                                          </table>

                                        </div>
                                      </div>
                                    </div> -->


                                    <div class="col-md-7 col-sm-7  ">
                                                   <div class="x_panel">
                                                     <div class="x_title">
                                                       <h2>Alerta Status<small> </small></h2>

                                                       <div class="clearfix"></div>
                                                     </div>
                                                     <div class="x_content">
                                                        	<iframe src="../alert/index.html"width="100%" ></iframe>

                                                     </div>
                                                   </div>
                                                 </div>
                 </div>
                                                 <div class="clearfix"></div>


      <?php
      $query = "SELECT device.sn,nivel.id,nivel.color_pcp,nivel.nivel_pcp,device.id,  unidade.instituicao, setor.nome, area.nome, device.upgrade, device.reg_date FROM device INNER JOIN area ON device.id_area = area.id INNER JOIN setor ON setor.id = area.id_area INNER JOIN unidade ON unidade.id = setor.id_unidade INNER JOIN nivel on nivel.id = device.id_nivel where device.trash = 0 and device.status = 1  ORDER BY device.id LIMIT 1";
      //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
      if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($sn,$nivelid,$color,$nivel,$id, $unidade,$setor,$area,$update,$reg_date);

        //printf("%s, %s\n", $solicitante, $equipamento);

        ?>
        <div class="col-md-12 col-sm-12  ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Controle PCP <small>Plano de Capacidade Pleno</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                  </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">



              <div class="table-responsive">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">

                    <!--  <th class="column-title">Unidade </th>
                      <th class="column-title">Setor </th>
                      <th class="column-title">Area </th> -->
                      <th class="column-title">Nivel Atual</th>
                      <th class="column-title">Atualização </th>

                      <th class="column-title no-link last"><span class="nobr">Mudança de Nivel </span>
                      </th>
                      <th class="bulk-actions" colspan="6">
                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php   while ($stmt->fetch()) { ?>
                      <tr class="even pointer">

                      <!--  <td class=" "><?php printf($unidade); ?></td>
                        <td class=" "><?php printf($setor); ?></td>
                        <td class=" "><?php printf($area); ?></td> -->
                          <td> <button  <?php if($nivelid == "1") {?>class="btn btn-xs btn-info"  <?php } if($nivelid == "2") { ?> class="btn btn-xs btn-success" <?php } if($nivelid == "3") { ?> class="btn btn-xs btn-warning" <?php }if($nivelid == "4") { ?> class="btn btn-xs btn-danger" <?php }?> ><i ><?php printf($nivel); ?></i ></button></td>
                          <th class="column-title"><?php printf($update); ?></th>
                        <td class=" last">

                          <div class="hidden-sm hidden-xs btn-group">
                          															<a class="btn btn-xs btn-info"  href="nivel/change/nivel-one.php"   onclick="new PNotify({
                                        																title: 'Nivel 1',
                                        																text: 'Nivel 1',
                                        																type: 'info',
                                        																styling: 'bootstrap3'
                                        														});">
                          																<i > Nivel 1</i>
                          															</a>

                          															<a class="btn btn-xs btn-success"href="nivel/change/nivel-two.php"  onclick="new PNotify({
                                        																title: 'Nivel 2',
                                        																text: 'Nivel 2',
                                        																type: 'success',
                                        																styling: 'bootstrap3'
                                        														});">
                          																<i >Nivel 2</i>
                          															</a>

                          															<a class="btn btn-xs btn-warning"href="nivel/change/nivel-three.php"  onclick="new PNotify({
                                        																title: 'Nivel 3',
                                        																text: 'Nivel 3',
                                        																type: 'warning',
                                        																styling: 'bootstrap3'
                                        														});">
                          																<i >Nivel 3</i>
                          															</a>

                          															<a class="btn btn-xs btn-danger"href="nivel/change/nivel-four.php"  onclick="new PNotify({
                                        																title: 'Nivel 4',
                                        																text: 'Nivel 4',
                                        																type: 'danger',
                                        																styling: 'bootstrap3'
                                        														});">
                          																<i >Nivel 4</i>
                          															</a>
                          														</div>


                        </td>
                      </tr>
                      <?php
                    }
                  }
                  ?>

                </tbody>
              </table>
            </div>
            <!--<div id="table_div"></div> -->
            <!--<div id="chart_div" style="width: 900px; height: 500px;"></div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
  date_default_timezone_set('America/Sao_Paulo');
  $today = date("Y-m-d");
  $read_open = 1;
  
  $query = "SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1";
  
  $resultados = $conn->query($query);
  $notifications = array();
  
  while ($row = mysqli_fetch_assoc($resultados)) {
    $notifications[] = $row;
  }
  
?>

<script>
   if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
        // Após o registro do Service Worker, mostrar notificações
        <?php foreach ($notifications as $notification): ?>
        var title = "<?php echo $notification['nome']; ?>";
        var options = {
          body: "<?php echo $notification['dados']; ?>"
          // Adicione mais opções de notificação conforme necessário
        };
        showNotification(title, options);
        <?php endforeach; ?>
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  function showNotification(title, options) {
    if (Notification.permission === "granted") {
      var notification = new Notification(title, options);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification(title, options);
        }
      });
    }
  }
</script>
 
<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>