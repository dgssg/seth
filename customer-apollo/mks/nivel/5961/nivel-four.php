<?php include("../../database/database.php");
$codigoget = (int)($_GET["sn"]);
 ?>
<html>
<head>
  <title>SETH</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="mqttws31.js" type="text/javascript"></script>
  <script src="jquery.min.js" type="text/javascript"></script>
  <script src="config.js" type="text/javascript"></script>

  <script type="text/javascript">
  var mqtt;
  var reconnectTimeout = 2000;

  function MQTTconnect() {
    if (typeof path == "undefined") {
      path = '/mqtt';
    }
    mqtt = new Paho.MQTT.Client(
      host,
      port,
      path,
      "web_" + parseInt(Math.random() * 100, 10)
    );



    var options = {
      timeout: 3,
      useSSL: useTLS,
      cleanSession: cleansession,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(MQTTconnect, reconnectTimeout);
      }
    };

    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;


    if (username != null) {
      options.userName = username;
      options.password = password;
    }
    console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
    mqtt.connect(options);
  }

  function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r1_in;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r4_in;
        mqtt.send(message);


  }

  function onConnectionLost(response) {
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

  };

  function onMessageArrived(message) {

    var topic = message.destinationName;
    var payload = message.payloadString;



    $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');


  };


  $(document).ready(function() {
    MQTTconnect();

  });

  </script>

</head>
<body>

  <div>

<ul id='ws' style="font-family: 'Courier New', Courier, monospace;"></ul>
    <?php
  //  sleep(5);
    $id_nivel_1=1;
    $id_nivel_2=2;
    $id_nivel_3=3;
    $id_nivel_4=4;
    $stmt = $conn->prepare("UPDATE device SET id_nivel = ? WHERE sn= ?");
    $stmt->bind_param("ss",  $id_nivel_4,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();

    $on="ON";
    $off="OFF";

    $stmt = $conn->prepare("UPDATE device SET r1 = ? WHERE sn= ?");
    $stmt->bind_param("ss",  $id_nivel_1,$off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r2 = ? WHERE sn= ?");
    $stmt->bind_param("ss",  $id_nivel_1,$off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r3 = ? WHERE sn= ?");
    $stmt->bind_param("ss",  $id_nivel_1,$off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r4 = ? WHERE sn= ?");
    $stmt->bind_param("ss",  $id_nivel_1,$on);
    $execval = $stmt->execute();
    $stmt->close();
//echo "<script>alert('Atualizado!');document.location='../../dashboard'</script>";

     ?>

     <a class="btn btn-app"  href="../../dashboard">
       <i class="glyphicon glyphicon-arrow-left"></i> Clica para voltar ou aguarde o redirecionamento automático!
       <p class="glyphicon glyphicon-time" id="countdown"></p>
     </a>

      </div>

    </body>
    </html>
    <script type="text/javascript">

    // Total seconds to wait
    var seconds = 10;

    function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
        // Chnage your redirection link here
        document.location="../../dashboard";
      } else {
        // Update remaining seconds
        document.getElementById("countdown").innerHTML = seconds;
        // Count down using javascript
        window.setTimeout("countdown()", 1000);
      }
    }

    // Run countdown function
    countdown();

    </script>
    <!-- Bootstrap -->
  	<link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  	<!-- Font Awesome -->
  	<link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  	<!-- NProgress -->
  	<link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
  	<!-- bootstrap-daterangepicker -->
  	<link href="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

  	<!-- Bootstrap -->
  	<link href="../../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  	<!-- Font Awesome -->
  	<link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  	<!-- NProgress -->
  	<link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
  	<!-- iCheck -->
  	<link href="../../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Bootstrap -->
    <script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../framework/vendors/nprogress/nprogress.js"></script>
