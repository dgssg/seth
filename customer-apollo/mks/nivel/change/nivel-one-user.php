<?php include("../../database/database.php");
$codigoget = 5961;
$id_device = 1;
session_start();
$id_usuario=$_SESSION['id_usuario'];
 ?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="../../../../framework/images/favicon.ico" type="image/ico" />

	<title>APOLLO| Análise e Planejamento Operacional para Lotação de Leitos e Ocupação</title>

	<!-- Bootstrap -->
	<link href="../../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- bootstrap-daterangepicker -->
	<link href="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">



	<!-- iCheck -->
	<link href="../../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

	<!-- bootstrap-progressbar -->
	<link href="../../../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<!-- JQVMap -->
	<link href="../../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>


	<!-- Custom Theme Style -->
	<link href="../../../../framework/build/css/custom.min.css" rel="stylesheet">

	<!-- PNotify -->
	<link href="../../../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	<link href="../../../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	<link href="../../../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	
	

  <script src="mqttws31.js" type="text/javascript"></script>
  <script src="jquery.min.js" type="text/javascript"></script>
  <script src="config.js" type="text/javascript"></script>

  <script type="text/javascript">
  var mqtt;
  var reconnectTimeout = 2000;

  function MQTTconnect() {
    if (typeof path == "undefined") {
      path = '/mqtt';
    }
    mqtt = new Paho.MQTT.Client(
      host,
      port,
      path,
      "web_" + parseInt(Math.random() * 100, 10)
    );



    var options = {
      timeout: 3,
      useSSL: useTLS,
      cleanSession: cleansession,
      onSuccess: onConnect,
      onFailure: function (message) {
        $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
        setTimeout(MQTTconnect, reconnectTimeout);
      }
    };

    mqtt.onConnectionLost = onConnectionLost;
    mqtt.onMessageArrived = onMessageArrived;


    if (username != null) {
      options.userName = username;
      options.password = password;
    }
    console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
    mqtt.connect(options);
  }

  function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic


        //use the below if you want to publish to a topic on connect
        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_5961;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_5961;
        mqtt.send(message);


        //Placa 8853
        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8853;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_8853;
        mqtt.send(message);

        //Placa 8975
        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8975;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_8975;
        mqtt.send(message);


        //Placa 8971
        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8971;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_8971;
        mqtt.send(message);


        //Placa 8859
        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8859;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_8859;
        mqtt.send(message);


        //Placa 8972
        message = new Paho.MQTT.Message(r_on);
        message.destinationName = r1_in_8972;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r2_in_8972;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r3_in_8972;
        mqtt.send(message);

        message = new Paho.MQTT.Message(r_off);
        message.destinationName = r4_in_8972;
        mqtt.send(message);




  }

  function onConnectionLost(response) {
    setTimeout(MQTTconnect, reconnectTimeout);
    $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

  };

  function onMessageArrived(message) {

    var topic = message.destinationName;
    var payload = message.payloadString;



    $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');


  };


  $(document).ready(function() {
    MQTTconnect();

  });

  </script>

</head>




<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="dashboard" class="site_title"><i class="fa fa-500px"></i> <span>APOLLO</span></a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<img src="../../logo/img.jpg" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Bem vindo,</span>
							<h2><?php printf($usuariologado) ?></h2>
						
							<span>
								<script> document.write(new Date().toLocaleDateString()); </script>
							</span>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>Geral</h3>
							<ul class="nav side-menu">
								<li><a><i class="fa fa-home"></i> Início <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="dashboard" title="Painel de Supervisão">Dashboard</a></li>

										<!--     <li><a href="index3.php">Dashboard3</a></li>  -->
									</ul>
								</li>
								<li><a><i class="fa fa-pencil"></i> Cadastro <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="account" title="Cadastro e gerenciamento de usuario">Usuario</a></li>
										<li><a href="unit" title="Cadastro e gerenciamento de unidade">Unidade</a></li>
										<li><a href="sector" title="Cadastro e gerencimaneo de setor">Setor</a></li>
										<li><a href="field" title="Cadastro e gerenciamento de area">Area</a></li>
										<li><a href="device" title="Cadastro e gerenciamento de dispositivo">Dispositivo</a></li>
										<!--     <li><a href="index3.php">Dashboard3</a></li>  -->
									</ul>
								</li>


							</ul>
						</div>



					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
							<script>
							var goFS = document.getElementById("goFS");
							goFS.addEventListener("click", function() {
								document.body.requestFullscreen();
							}, false);
							</script>

						</a>
						<a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="lockscreen">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>


					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>
			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<nav class="nav navbar-nav">
						<ul class=" navbar-right">
							<li class="nav-item dropdown open" style="padding-left: 15px;">
								<a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
									<img src="../../logo/img.jpg" alt=""><?php printf($usuariologado) ?>
								</a>
								<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
									<a class="dropdown-item"  href="profile">   <i class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></i> Perfil </a>

									<a class="dropdown-item"  href="login"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
								</div>
							</li>


						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
	
	 <div class="right_col" role="main">

        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Mudança de Nivel</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                <div class="input-group">

                  <span class="input-group-btn">

                  </span>
                </div>
              </div>
            </div>
          </div>

  <div>
	  
<!-- <ul id='ws' style="font-family: 'Courier New', Courier, monospace;"></ul> -->

    <?php
  //  sleep(5);
    $id_nivel_1=1;
    $id_nivel_2=2;
    $id_nivel_3=3;
    $id_nivel_4=4;
    $on="ON";
    $off="OFF";

    $stmt = $conn->prepare("UPDATE device SET id_nivel = ? ");
    $stmt->bind_param("s",  $id_nivel_1);
    $execval = $stmt->execute();
    $stmt->close();
    $stmt = $conn->prepare("INSERT INTO report (id_nivel,id_device,id_user ) VALUES (?,?,? )");
    $stmt->bind_param("sss",$id_nivel_1,$id_device,$id_usuario );
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r1 = ? ");
    $stmt->bind_param("s",  $on);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r2 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r3 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE device SET r4 = ? ");
    $stmt->bind_param("s",  $off);
    $execval = $stmt->execute();
    $stmt->close();


//echo "<script>alert('Atualizado!');document.location='../../dashboard'</script>";

     ?>
     <div class="x_panel">
              <div class="x_title">
                <h2>Alteração</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

           <div >
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                    <a class="btn btn-app"  href="../../stakeholder/dashboard">
       <i class="glyphicon glyphicon-arrow-left"></i> Clica para voltar ou aguarde o redirecionamento automático!
       <p class="glyphicon glyphicon-time" id="countdown"></p>
     </a>
                </div>



              </div>
            </div>

   

      </div>
      
      	<!-- footer content -->
      	 </div>
      	 	 </div>
      	 	 	
	<footer>
		<div class="pull-right">
			©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://mksistemasbiomedicos.com.br"></a>
		</div>
		<div class="clearfix"></div>
	</footer>
	<!-- /footer content -->

 </div>
      	 	 </div>
      	 	    </body>
      	 	    
      	 	      <!-- Bootstrap -->
  
<!-- jQuery
<script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../../../framework/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../../../../framework/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- jQuery Sparklines -->
<script src="../../../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- Flot -->
<script src="../../../../framework/vendors/Flot/jquery.flot.js"></script>
<script src="../../../../framework/vendors/Flot/jquery.flot.pie.js"></script>
<script src="../../../../framework/vendors/Flot/jquery.flot.time.js"></script>
<script src="../../../../framework/vendors/Flot/jquery.flot.stack.js"></script>
<script src="../../../../framework/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../../../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../../../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../../../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../../../../framework/vendors/DateJS/build/date.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../../../framework/vendors/moment/min/moment.min.js"></script>
<script src="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../../../framework/build/js/custom.min.js"></script>




<!-- jQuery-->
<script src="../../../../framework/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap
<script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

<!-- Chart.js -->
<script src="../../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../../../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../../../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../../../../framework/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../../../../framework/vendors/skycons/skycons.js"></script>

<!-- DateJS -->
<script src="../../../../framework/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../../../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../../../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../../../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../../../framework/vendors/moment/min/moment.min.js"></script>
<script src="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../../../framework/build/js/custom.min.js"></script>
<!-- PNotify -->
<script src="../../../../framework/vendors/pnotify/dist/pnotify.js"></script>
<script src="../../../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="../../../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>



    
    <script type="text/javascript">

    // Total seconds to wait
    var seconds = 10;

    function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
        // Chnage your redirection link here
        document.location="../../stakeholder/dashboard";
      } else {
        // Update remaining seconds
        document.getElementById("countdown").innerHTML = seconds;
        // Count down using javascript
        window.setTimeout("countdown()", 1000);
      }
    }

    // Run countdown function
    countdown();

    </script>
  
 
    </html>
