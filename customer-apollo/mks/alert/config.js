// host = '172.16.153.122';	// hostname or IP address
host = 'mqtt.eclipseprojects.io';	// hostname or IP address
// host = '172.16.153.110';	// hostname or IP address
port = 443;

lwt_availability_5961 = '/dingtian/relay5961/out/lwt_availability';
sn_5961 = '/dingtian/relay5961/out/sn';

lwt_availability_8853 = '/dingtian/relay8853/out/lwt_availability';
sn_8853 = '/dingtian/relay8853/out/sn';

lwt_availability_8975 = '/dingtian/relay8975/out/lwt_availability';
sn_8975 = '/dingtian/relay8975/out/sn';

lwt_availability_8971 = '/dingtian/relay8971/out/lwt_availability';
sn_8971 = '/dingtian/relay8971/out/sn';

lwt_availability_8859 = '/dingtian/relay8859/out/lwt_availability';
sn_8859 = '/dingtian/relay8859/out/sn';

lwt_availability_8972 = '/dingtian/relay8972/out/lwt_availability';
sn_8972 = '/dingtian/relay8972/out/sn';

r1 = '/dingtian/relay5961/out/r1';		// topic to subscribe to
r2 = '/dingtian/relay5961/out/r2';
r3 = '/dingtian/relay5961/out/r3';
r4 = '/dingtian/relay5961/out/r4';
relay1 = '/dingtian/relay5961/out/relay1';		// topic to subscribe to
relay2 = '/dingtian/relay5961/out/relay2';
relay3 = '/dingtian/relay5961/out/relay3';
relay4 = '/dingtian/relay5961/out/relay4';
i1 = '/dingtian/relay5961/out/i1';		// topic to subscribe to
i2 = '/dingtian/relay5961/out/i2';
i3 = '/dingtian/relay5961/out/i3';
i4 = '/dingtian/relay5961/out/i4';
input1 = '/dingtian/relay5961/out/input1';		// topic to subscribe to
input2 = '/dingtian/relay5961/out/input2';
input3 = '/dingtian/relay5961/out/input3';
input4 = '/dingtian/relay5961/out/input4';
ip = '/dingtian/relay5961/out/ip';

mac = '/dingtian/relay5961/out/mac';
input_cnt = '/dingtian/relay5961/out/input_cnt';
relay_cnt = '/dingtian/relay5961/out/relay_cnt';
r1_in = '/dingtian/relay5961/in/r1';		// topic to subscribe to
r2_in = '/dingtian/relay5961/in/r2';
r3_in = '/dingtian/relay5961/in/r3';
r4_in = '/dingtian/relay5961/in/r4';
pub_qos = '0';
r_on='ON';
r_off='OFF';


useTLS = true;
username = null;
password = null;
// username = "jjolie";
// password = "aa";

// path as in "scheme:[//[user:password@]host[:port]][/]path[?query][#fragment]"
//    defaults to "/mqtt"
//    may include query and fragment
//
 //path = "/mqtt";
// path = "/data/cloud?device=12345";

cleansession = true;
