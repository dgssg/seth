<?php
 
// Create connection
include("database/database.php");// remover ../

?>
 <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Relatorio</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                <div class="input-group">

                  <span class="input-group-btn">

                  </span>
                </div>
              </div>
            </div>
          </div>




           <!-- page content -->








            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Filtro <small>do Relatorio</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a class="dropdown-item" href="#">Settings 1</a>
                        </li>
                        <li><a class="dropdown-item" href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form action="backend/report-filter-backend.php" method="post"  target="_blank">

<div class="ln_solid"></div>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Date Inicial<span
                        class="required"></span></label>
                    <div class="col-md-6 col-sm-6">
                      <input class="form-control" class='date' type="date" name="date_start" ></div>
                  </div>

                  <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Date Final<span
                        class="required"></span></label>
                    <div class="col-md-6 col-sm-6">
                      <input class="form-control" class='date' type="date" name="date_end" ></div>
                  </div>
 

                 <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nivel" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o nivel ">Nivel <span class="required" ></span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <select type="text" class="form-control has-feedback-right" name="nivel" id="nivel"  placeholder="nivel">
                        <option value=""> Selecione um nivel	</option>

                      <?php



                    $query = "SELECT * FROM nivel ";


if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id, $status,$cor);
 while ($stmt->fetch()) {
   ?>
<option value="<?php printf($id);?>	"><?php printf($status);?>	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }

                                          }
                    $stmt->close();

                    ?>
                                      </select>
                      </div>
                    </div>

                 <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_device" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma plataforma ">Plataforma <span class="required" ></span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <select type="text" class="form-control has-feedback-right" name="id_device" id="id_device"  placeholder="id_device">
                        <option value=""> Selecione uma Plataforma	</option>

                      <?php



                    $query = "SELECT * FROM report_device ";


if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id, $status);
 while ($stmt->fetch()) {
   ?>
<option value="<?php printf($id);?>	"><?php printf($status);?>	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }

                                          }
                    $stmt->close();

                    ?>
                                      </select>
                      </div>
                    </div>
                   
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_user" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Usuario ">Usuario <span class="required" ></span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <select type="text" class="form-control has-feedback-right" name="id_user" id="id_user"  placeholder="id_user">
                        <option value=""> Selecione um Usuario	</option>

                      <?php



                    $query = "SELECT id,nome,sobrenome FROM usuario ";


if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id, $nome, $sobrenome);
 while ($stmt->fetch()) {
   ?>
<option value="<?php printf($id);?>	"><?php printf($nome);?> - <?php printf($sobrenome);?>	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }

                                          }
                    $stmt->close();

                    ?>
                                      </select>
                      </div>
                    </div>




 


                   <br>


                              <div class="form-group row">
                     <label class="col-form-label col-md-3 col-sm-3 "></label>
                      <div class="col-md-3 col-sm-3 ">
                          <center>
                         <button class="btn btn-sm btn-success" type="submit">Gerar Relatorio</button>
                       </center>
                      </div>
                    </div>




                </div>
              </div>
            </div>
          </div>






           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="report-report">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>




              </div>
            </div>







      </div>
          </div>
      
