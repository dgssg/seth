<?php
// Substitua 'YOUR_BOT_TOKEN' pelo token do seu bot
    $id_dispositivo = ($_GET["id_dispositivo"]);
    $id_parametro = ($_GET["id_parametro"]);
    $vlr = ($_GET["vlr"]);
    
   // $botToken = '6420010515:AAFDY47xiVz80wiAncCHbV7lchuVBS2_IdM';
$chatID = '-4057846376'; // O ID do chat para o qual você deseja enviar a mensagem
    $message = 'Boletins informativos e Recomendações: Sistema Telemetria Santa Casa Curitiba' . "\n";
    $message .= 'ID do Dispositivo: ' . $id_dispositivo . "\n";
    $message .= 'ID do Parâmetro: ' . $id_parametro . "\n";
    $message .= 'Valor: ' . $vlr;

$telegramApiUrl = "https://api.telegram.org/bot$botToken/sendMessage";

$data = [
    'chat_id' => $chatID,
    'text' => $message,
];

$options = [
    'http' => [
        'method' => 'POST',
        'header' => 'Content-Type: application/x-www-form-urlencoded',
        'content' => http_build_query($data),
    ],
];

$context = stream_context_create($options);
$response = file_get_contents($telegramApiUrl, false, $context);

if ($response === false) {
    // Erro ao enviar a mensagem
    echo 'Erro ao enviar a mensagem.';
} else {
    // Mensagem enviada com sucesso
    echo 'Mensagem enviada com sucesso.';
}
