  <?php
$codigoget = ($_GET["id"]);

// Create connection

include("database/database.php");
    $query = "SELECT sensor.telegram_chat_id,sensor.telegram_token,sensor.telegram,sensor.ativo,sensor.alarm,sensor.trash,sensor.gate_sensor,sensor.energy_sensor,sensor.id, sensor.macadress, sensor_type.nome as 'id_sensor_type', sensor.nome, sensor.id_area, sensor.id_status, sensor.up_time, sensor.file, sensor.reg_date, sensor.upgrade, sensor.bat, sensor.last_alarm, sensor.trash, sensor.id_alarm, sensor.imei, sensor.firmware, sensor.linha, sensor.iccid, sensor.id_location, sensor_usb.nome as 'usb', sensor_charger.nome as 'charger', sensor.ativo, sensor.dados, sensor.id_dados,area.nome as 'area',setor.nome as 'setor',unidade.unidade FROM sensor LEFT JOIN sensor_type ON sensor_type.id =  sensor.id_sensor_type LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN sensor_usb ON sensor_usb.id =  sensor.usb LEFT JOIN sensor_charger ON sensor_charger.id =  sensor.charger WHERE sensor.id = $codigoget";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($telegram_chat_id,$telegram_token,$telegram,$ativo,$alarm,$trash,$gate_sensor,$gate_energy,$id,$macadress,$id_sensor_type, $nome, $id_area, $id_status,$up_time, $file, $reg_date,$upgrade, $bat, $last_alarm, $trash, $id_alarm, $imei, $firmware, $linha, $iccid, $id_location, $usb,$charger, $ativo, $dados, $id_dados,$area,$setor,$unidade);
      while ($stmt->fetch()) {
        
      }
    }


?>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Edição <small>Sensor</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar <small>Sensor</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form
                      action="backend/device-iot-edit-backend.php"
                      method="post">

                    <input type="hidden" id="codigoget" name="codigoget" value="<?php printf($id); ?>" readonly="readonly" required="required" class="form-control ">
                <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Macadress <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="$macadress" name="$macadress" value="<?php printf($macadress); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Sensor <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="sensor" name="sensor" value="<?php printf($id_sensor_type); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Nome<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" required="required" class="form-control"  value="<?php printf($nome); ?>" >
                        </div>
                      </div>
                   
                 
                   

                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">UP Time</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($up_time); ?>" readonly="readonly">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Bateria</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                      </div>
                    </div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Ativo </label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input name="ativo" type="checkbox" class=""
                                <?php if($ativo == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Alarme </label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input name="alarm" type="checkbox" class=""
                                <?php if($alarm == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Telegram </label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input name="telegram" type="checkbox" class=""
                                <?php if($telegram == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>

                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Excluido </label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input disabled="disabled" name="trash" type="checkbox" class=""
                                <?php if($trash == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Sensor Magnetic</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input disabled="disabled" name="gate_sensor" type="checkbox" class=""
                                <?php if($gate_sensor == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Sensor Corrente</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                            <input disabled="disabled" name="energy_sensor" type="checkbox" class=""
                            <?php if($energy_sensor == "0"){printf("checked"); }?> />
                        </label>
                          </div>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Token Telegram<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="telegram_token" name="telegram_token" required="required" class="form-control"  value="<?php printf($telegram_token); ?>" >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Id Chat Telegram<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="telegram_chat_id" name="telegram_chat_id" required="required" class="form-control"  value="<?php printf($telegram_chat_id); ?>" >
                        </div>
                      </div>
                    
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="info" class="form-control" type="text" name="info"  value="<?php printf($unidade); ?>" readonly="readonly">
                        </div>
                      </div>
                        
                        
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($setor); ?>" readonly="readonly">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Area</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($area); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                          <center>
                            <button class="btn btn-sm btn-success" type="submit"
                              onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });">Salvar
                              Informações</button>
                          </center>
                        </div>
                      </div>
                      
                      
                    </form>


                  </div>
                </div>
              </div>
	       </div>
                      <div class="clearfix"></div>
                      
                      <div class="row">
                        <div class="col-md-12">
                          <div class="x_panel">
                            <div class="x_title">
                              <h2>Imagem <small> </small></h2>
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Settings 1</a>
                                    <a class="dropdown-item" href="#">Settings 2</a>
                                  </div>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                              </ul>
                              <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                              
                              <div class="row">
                                <div class="col-md-55">
                                  <?php if($file ==! ""){ ?>
                                  
                                  <div class="thumbnail">
                                    <div class="image view view-first">
                                      <img style="width: 100%; display: block;"
                                        src="dropzone/sensor/<?php printf($file); ?>" alt="image" />
                                      <div class="mask">
                                        <p>Imagem</p>
                                        <div class="tools tools-bottom">
                                          <a href="dropzone/sensor/<?php printf($file); ?>" download><i
                                            class="fa fa-download"></i></a>
                                          
                                        </div>
                                      </div>
                                    </div>
                                    <div class="caption">
                                      <p>Imagem</p>
                                    </div>
                                  </div>
                                  <?php } ?>
                                  
                                </div>
                                
                                
                                
                                
                                
                                
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Dados <small>Localização</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                          aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    
                    
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="localizacao">Localização
                        <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="localizacao" name="localizacao"
                          value="<?php printf($unidade); ?> - <?php printf($setor); ?> - <?php printf($area); ?>"
                          readonly="readonly" required="required" class="form-control ">
                      </div>
                    </div>
                    
                    
                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 "></label>
                      <div class="col-md-3 col-sm-3 ">
                        <center>
                          <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#myModal">Alterar</button>

                        </center>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- Fechamento -->
            <div class="modal fade bs-example-modal-lg5" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
              
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Alterar</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <h4>Alterar Localização</h4>
                    <!-- Registro forms-->
                    <form
                      action="backend/device-iot-location-backend.php?id=<?php printf($codigoget); ?>"
                      method="post">
                      <div class="ln_solid"></div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="localizacao">Localização
                          <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="localizacao" name="localizacao"
                            value="<?php printf($area); ?>" readonly="readonly" required="required"
                            class="form-control ">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                          class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-left" name="unidade"
                            id="unidade" style="width:350px;" placeholder="Unidade" required="required">
                            <option> Selecione uma unidade</option>
                            <?php
                              
                              
                              
                              $result_cat_post  = "SELECT  id, unidade FROM unidade WHERE trash = 1";
                              
                              $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['unidade'].'</option>';
                              }
                            ?>
                            
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                            class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#unidade').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="setor">Setor <span
                          class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="setor" id="setor"
                            style="width:350px;" placeholder="Area">
                            <option value="">Selecione um Setor</option>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                            class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>
                          
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#setor').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="area">Area <span
                          class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="area" id="area"
                            style="width:350px;" placeholder="Area" required>
                            <option value="">Selecione uma Area</option>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                            class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>
                          
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#area').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Data
                          Alteração <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_location" name="data_location"
                            class="form-control ">
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      
                      <label for="obs">Justificativa:</label>
                      <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                        data-parsley-validation-threshold="10"></textarea>
                      
                      <div class="ln_solid"></div>
                      
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar Informações</button>
                      </div>
                      
                      </div>
                      </div>
                      </div>
                    </form>
                  </div>
                  <!-- Fechamen    to -->
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Inserir/Substituir</h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                              </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          
                          <form class="dropzone"
                            action="backend/device-iot-change-dropzone-upload-backend.php?id=<?php printf($codigoget); ?>"
                            method="post">
                          </form>
                          
                          <form action="backend/device-iot-change-img-upload-backend.php?id=<?php printf($codigoget); ?>"
                            method="post">
                            <center>
                              <button class="btn btn-sm btn-success" type="submit">Atualizar Imagem</button>
                            </center>
                          </form>
                          
                        </div>
                      </div>

                      
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-device-iot">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>


                </div>
              </div>




                </div>
              </div>

      <script type="text/javascript">
        $(document).ready(function() {
          $('#unidade').change(function() {
            $('#setor').select2({
              dropdownParent: $("#myModal")
            }).load('sub_categorias_post_unidade.php?id=' + $('#unidade').val());
          });
        });
      </script>
      <script type="text/javascript">
        $(document).ready(function() {
          $('#setor').change(function() {
            $('#area').select2({
              dropdownParent: $("#myModal")
            }).load('sub_categorias_post_setor.php?id=' + $('#setor').val());
          });
        });
      </script>
      

     