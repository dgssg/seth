<?php
include("../database/database.php");
$query = "SELECT device.id, device.nome, device.codigo,device.serie,device.ativo, device.upgrade, device.reg_date,unidade_area.nome as 'area', unidade_setor.nome as 'setor', unidade.nome as 'unidade',  device.macadress,alarm_status.alarm FROM device LEFT JOIN unidade_area ON device.id_unidade_area = unidade_area.id LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade  LEFT JOIN alarm_status ON alarm_status.id = device.id_status where device.trash = 1 GROUP BY  device.id ORDER by device.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
