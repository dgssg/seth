<?php
include("../database/database.php");
$query = "SELECT unidade_setor.codigo,unidade_setor.id,unidade_setor.id_unidade,unidade_setor.custo,unidade_setor.nome,unidade_setor.upgrade,unidade_setor.reg_date, unidade.nome as 'unidade' FROM unidade_setor INNER JOIN unidade ON unidade_setor.id_unidade = unidade.id where unidade_setor.trash = 1 GROUP BY  unidade_setor.id ORDER by unidade_setor.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
