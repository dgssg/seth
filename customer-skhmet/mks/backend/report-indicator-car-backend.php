<?php




include("../database/database.php");
date_default_timezone_set('America/Sao_Paulo');

$query = "SELECT kpi.interpretation,kpi.meta,kpi.value  FROM kpi where kpi.id = 1";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result( $interpretation,$meta, $value);
    
    while ($stmt->fetch()) {
    }
}

$yr = $_POST['yr'];
$month= $_POST['month'];
$instituicao=$_POST['instituicao'];
$setor=$_POST['setor'];
$area=$_POST['area'];
$id_alarm=$_POST['id_alarm'];
$id_device=$_POST['id_device'];

$yr=trim($yr);
$month=trim($month);
$instituicao=trim($instituicao);
$setor=trim($setor);
$area=trim($area);


$sql = "SELECT  nome,logo,endereco,cep,bairro,cidade,estado,logo,cnpj FROM unidade where id = $instituicao ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
   $stmt->bind_result($unidade,$logo,$rua,$cep,$bairro,$cidade,$estado,$logo,$cnpj);
   while ($stmt->fetch()) {
   }
  }

  $sql = "SELECT  nome FROM unidade_setor where id = $setor ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
   $stmt->bind_result($setor);
   while ($stmt->fetch()) {
   }
  }

  $sql = "SELECT  nome FROM unidade_aea where id = $area ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
   $stmt->bind_result($area);
   while ($stmt->fetch()) {
   }
  }

if($yr == ""){
 $yr = date("Y");
}
if($month == ""){
  $month = date("m");
 }
if($instituicao==!""){
    $query = "SELECT nome.unidade FROM unidade where unidade.id = $instituicao";
    if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result( $instituicao_name);
        
        while ($stmt->fetch()) {
        }
    }
    
}

if($setor==!""){
    
    $query = "SELECT unidade_setor.nome FROM unidade_setor where unidade_setor.id = $setor";
    if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result( $setor_name);
        
        while ($stmt->fetch()) {
        }
    }
    
}

if($area==!""){
    $query = "SELECT unidade_area.nome FROM unidade_area where unidade_area.id = $setor";
    if ($stmt = $conn->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result( $area_name);
        
        while ($stmt->fetch()) {
        }
    }
    
}
 
  date_default_timezone_set('America/Sao_Paulo');
  $year=date("Y");
  $today=date("Y-m-d");
  $mes=date("m");
  

  if($id_alarm == 0){
    $id_alarm = "1,2,3"; // Status 1, 2 e 3    
  }
  // Inicializa os arrays para cada categoria
  $abertos = array_fill(0, 12, 0);
  $fechados = array_fill(0, 12, 0);
  $lacrados = array_fill(0, 12, 0);
  $stmt = "  SELECT
  MONTH(reg_date) AS mes,
  COUNT(id) AS total,
  id_alarm_status
  FROM
  reg_date_alarm
  WHERE
  id_alarm_status IN ($id_alarm)
  AND YEAR(reg_date) = $yr
  GROUP BY
  MONTH(reg_date),
  id_alarm_status
  ORDER BY
  MONTH(reg_date),
  id_alarm_status
";
 
  $stmt_query = $stmt;
  $stmt = $conn->prepare($stmt);
  
  
  $stmt->execute();
  $stmt->bind_result($mes, $total, $id_alarm_status);
  
  while ($stmt->fetch()) {
    // Atualiza os arrays de acordo com o status
    // Após recuperar os valores do banco de dados
    
    
    
    
    
    switch ($id_alarm_status) {
      case 1:
        $abertos[$mes - 1] = $total;
        
        break;
      case 2:
        $fechados[$mes - 1] = $total;
        
        break;
      case 3:
        $lacrados[$mes - 1] = $total;
        
        
        break;
    }
  }
  
  $stmt->close();
  
  // Constrói o objeto data
  $data = [
    'labels' => ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    'abertos' => $abertos,
    'fechados' => $fechados,
    'lacrados' => $lacrados
  ];
  
  // Converte para JSON (se necessário)
  $data_json = json_encode($data);
  
  echo "<script>";
  echo "var dataFromPHP = $data_json;";
  echo "</script>";
  

  //header('Location: ../report-report-mc');
  ?>

  <script type="text/javascript" language="JavaScript">
  
  function printPage() {

    if (window.print) {

      agree = confirm("Deseja imprimir essa pagina ?");

      if (agree) {
        window.print();

        if (_GET("pg") != null)
        location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
        else
        history.go(-1);


        window.close();
      }



    }
  }

  function noPrint() {
    try {

      if (_GET("pg") != null)
      location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
      else
      history.go(-1);

      window.close();

    }
    catch (e) {
      alert();
      document.write("Error Message: " + e.message);
    }
  }

  function _GET(name) {
    var url = window.location.search.replace("?", "");
    var itens = url.split("&");

    for (n in itens) {
      if (itens[n].match(name)) {
        return decodeURIComponent(itens[n].replace(name + "=", ""));
      }
    }
    return null;
  }





  function visualizarImpressao() {

    var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

    document.body.insertAdjacentHTML("beforeEnd", Navegador);

    Navegador1.ExecWB(7, 1);

    Navegador1.outerHTML = "";

  }



  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

  ga('create', 'UA-11247563-6', 'auto');
  ga('send', 'pageview');

  // -->
  </script>
   

  <!DOCTYPE html>
  <html>
  <head>
       </head>
  <body>
  <script type="text/javascript" language="JavaScript">

function printPage() {

  if (window.print) {

    agree = confirm("Deseja imprimir essa pagina ?");

    if (agree) {
      window.print();

      if (_GET("pg") != null)
      location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
      else
      history.go(-1);


      window.close();
    }



  }
}

function noPrint() {
  try {

    if (_GET("pg") != null)
    location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
    else
    history.go(-1);

    window.close();

  }
  catch (e) {
    alert();
    document.write("Error Message: " + e.message);
  }
}

function _GET(name) {
  var url = window.location.search.replace("?", "");
  var itens = url.split("&");

  for (n in itens) {
    if (itens[n].match(name)) {
      return decodeURIComponent(itens[n].replace(name + "=", ""));
    }
  }
  return null;
}





function visualizarImpressao() {

  var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

  document.body.insertAdjacentHTML("beforeEnd", Navegador);

  Navegador1.ExecWB(7, 1);

  Navegador1.outerHTML = "";

}



(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
    (i[r].q = i[r].q || []).push(arguments)
  }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-11247563-6', 'auto');
ga('send', 'pageview');

// -->
</script>

<style type="text/css">
page {
  /* background: white;*/
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  text-align: left;
}

page[size="A4"] {
  width: 21cm;
  /*height: 29.7cm;                */
}

page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /* height: 21cm; */
}

@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
    page-break-after:always;
  }

  .noprint {
    display: none;
  }
}




body {
  font-size: 11px;
  font-family: sans-serif;
}



.pagina_retrato {
  width: 720px;
}

.pagina_paisagem {
  width: 1025px;
}

.relatorio_filtro {
  padding: 10px 0px;
  border-collapse: collapse;
}

.relatorio_filtro tr td {
  border: 1px solid #000;
}


.relatorio_titulo {
  font-family: Times New Roman;
  font-size: 15px;
  color: Black;
  font-weight: bold;
  text-transform: uppercase;
}

.borda_celula {
  border: 1px solid #000;
  background-color: #fff;
}

.noprint {
  padding-bottom: 10px;
}
.right{
  float:right;
}

.left{
  float:left;
}
</style>

<style type="text/css" media="print">
/*
@page {
margin: 0.8cm;
}
*/
.pagina_retrato, .pagina_paisagem {
width: 100%;
}
 
</style>

<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script>
window.onload = function () {
JsBarcode(".barcode").init();
}
</script>

   <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <style>
    /* Estilos para o cabeçalho */
    header {
      padding: 20px;
      background-color: #f5f5f5;
      border-bottom: 1px solid #ddd;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      padding: 5px;
      border: 1px solid #ddd;
    } 

    #logo {
      width: 100px;
    }

    h1 {
      margin: 0;
      font-size: 24px;
    }

    #info {
      font-size: 14px;
    }
  </style>
    <style>
    body {
    font-size: 12pt;
    font-family: Calibri;
    padding : 10px;
}
table {
    border: 1px solid black;
}
th {
    border: 1px solid black;
    padding: 5px;
    background-color:grey;
    color: white;
}
td {
    border: 1px solid black;
    padding: 5px;
}
input {
    font-size: 12pt;
    font-family: Calibri;
}
</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />
<link href="../App_Themes/Tema1/StyleSheet.css" type="text/css" rel="stylesheet" />

<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEwODA5NzUyMjMPZBYCZg9kFgICAw9kFgICAQ9kFggCAw8PFgIeCEltYWdlVXJsBUd+L2ltYWdlcy9ob3NwaXRhaXMvaV9jcnV6dmVybWVsaGFicmFzaWxlaXJhLWZpbGlhbGRvZXN0YWRvZG9wYXJhbsOhLnBuZ2RkAgUPDxYCHgRUZXh0BQcwMS8yMDE5ZGQCCw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIeCkhlYWRlclRleHQFBzAxLzIwMTg8KwAFAQAWAh8EBQcwMi8yMDE4PCsABQEAFgIfBAUHMDMvMjAxODwrAAUBABYCHwQFBzA0LzIwMTg8KwAFAQAWAh8EBQcwNS8yMDE4PCsABQEAFgIfBAUHMDYvMjAxODwrAAUBABYCHwQFBzA3LzIwMTg8KwAFAQAWAh8EBQcwOC8yMDE4PCsABQEAFgIfBAUHMDkvMjAxODwrAAUBABYCHwQFBzEwLzIwMTg8KwAFAQAWAh8EBQcxMS8yMDE4PCsABQEAFgIfBAUHMTIvMjAxODwrAAUBABYCHwQFBzAxLzIwMTkWDQIGAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGDBQrAABkAg8PPCsAEQMADxYEHwJnHwNmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIfBAUHMDEvMjAxODwrAAUBABYCHwQFBzAyLzIwMTg8KwAFAQAWAh8EBQcwMy8yMDE4PCsABQEAFgIfBAUHMDQvMjAxODwrAAUBABYCHwQFBzA1LzIwMTg8KwAFAQAWAh8EBQcwNi8yMDE4PCsABQEAFgIfBAUHMDcvMjAxODwrAAUBABYCHwQFBzA4LzIwMTg8KwAFAQAWAh8EBQcwOS8yMDE4PCsABQEAFgIfBAUHMTAvMjAxODwrAAUBABYCHwQFBzExLzIwMTg8KwAFAQAWAh8EBQcxMi8yMDE4PCsABQEAFgIfBAUHMDEvMjAxORYNAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGAgYMFCsAAGQYAgUeY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSRncnYyDzwrAAwBCGZkBR5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJGdydjEPPCsADAEIZmS9tcluC/wCTfFh0PbdSxSO8nxQJa5LJqUDKFT3sJzurQ==" />
</div>

<div>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3164E9EE" />
</div>

<div class="noprint">
<center>
  <input type="button" value="Não Imprimir" onclick="noPrint();">
  <input type="button" value="Imprimir" onclick="printPage();" >
  <input type="button" onclick="visualizarImpressao();" value="Visualizar Impressão" >
</center>
</div>

  <header>
    <table>
      <tr>
      <td><img src="../dropzone/logo/<?php printf($logo)?>" alt="Logo da empresa" id="logo"></td>
        <td>
          <center>
          <h1>Indicador Alarme Carrinho de Emergencia </h1>

          <p>Data da impressão: <?php printf($today); ?> <small>Sistema SETH</small></p>
          </center>
        </td>
        <td><img src="../logo/clientelogo.png" alt="Logo da empresa" id="logo"></td>
      </tr>
      <tr>
        <td colspan="3" id="info">
          <table>
            <tr>
              <td>Unidade:</td>
              <td><?php printf($unidade); ?></td>
              <td>Área:</td>
              <td><?php printf($area); ?></td>
              <td>Setor:</td>
              <td><?php printf($setor); ?></td>
            </tr>
            <tr>
              <td>CEP:</td>
              <td><?php printf($cep); ?></td>
              <td>Rua:</td>
              <td><?php printf($rua); ?></td>
              <td>Bairro:</td>
              <td><?php printf($bairro); ?></td>
            </tr>
            <tr>
              <td>Cidade:</td>
              <td><?php printf($cidade); ?></td>
              <td>Estado:</td>
              <td><?php printf($estado); ?></td>
              <td>CNPJ:</td>
              <td><?php printf($cnpj); ?></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
      <td colspan="6" id="info">
      <strong>Interpretação:</strong> <?php echo htmlspecialchars_decode($interpretation); ?>.
  </td>
  </tr>
  <tr>
      <td  colspan="6" id="info">
      <strong>Ficha Técnica do Indicador:</strong> <?php echo htmlspecialchars_decode($meta); ?>.
  </td>
  </tr>
    </table>
  </header>
                      


<br>  
<div style="display: flex;">            
  <div class="container">
    <div class="chart-container">
      <canvas id="alarmChart" style="width: 1000px; height: 500px;"></canvas>

    </div>
  </div>
  <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <?php 
   
    $stmt = $conn->prepare($stmt_query);
    
    
    $stmt->execute();
    $stmt->bind_result($mes, $total, $id_alarm_status);
    
    while ($stmt->fetch()) {
      // Atualiza os arrays de acordo com o status
      // Após recuperar os valores do banco de dados
      if($mes == $month && $id_alarm_status == 1 ){
      $aberto = $total;
      $grf = $total + $grf;
      
      }
      if($mes == $month && $id_alarm_status == 2 ){
        $fechado = $total;
        $grf = $total + $grf;
      }
      if($mes == $month && $id_alarm_status == 3 ){
        $lacrado = $total;
        $grf = $total + $grf;
      }
             }
   
    $grf1=($aberto/$grf)*100;
    $grf2=($fechado/$grf)*100;
    $grf3=($lacrado/$grf)*100;
    $grf4=($grf/$grf)*100;
  ?>
 
  <center>
    
    <div >
      
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Nº</th>
            <th>(%)</th>
            
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Total</th>
            <td><?php  printf($grf); ?></td>
            <td><?php echo number_format($grf4, 2) . '%'; ?></td>            
          </tr>
        
          <tr>
            <th scope="row">Aberto</th>
            <td><?php  printf($aberto); ?></td>
            <td><?php echo number_format($grf1, 2) . '%'; ?></td>
            
          </tr>
          <tr>
            <th scope="row">Fechado</th>
            <td><?php  printf($fechado); ?></td>
            <td><?php echo number_format($grf2, 2) . '%'; ?></td>
            
          </tr>
          <tr>
            <th scope="row">Lacrado</th>
            <td><?php  printf($lacrado); ?></td>
            <td><?php echo number_format($grf3, 2) . '%'; ?></td>
            
          </tr>
          
       
          
        </tbody>
        
      </table>
    </div>
  </center>
</div>
    
    <br>
  
  
  <!-- Adicione os scripts JavaScript necessários, como Chart.js, aqui -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>
  <script>
    // Inclua o objeto data do PHP aqui ou em um arquivo JavaScript externo
    const data = dataFromPHP;
    // Configuração do gráfico de barras
    const ctx = document.getElementById('alarmChart').getContext('2d');
    const alarmChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: data.labels,
        datasets: [
          {
            label: 'Abertos',
            data: data.abertos,
            backgroundColor: 'rgba(75, 192, 192, 0.6)'
          },
          {
            label: 'Fechados',
            data: data.fechados,
            backgroundColor: 'rgba(255, 99, 132, 0.6)'
          },
          {
            label: 'Lacrados',
            data: data.lacrados,
            backgroundColor: 'rgba(155, 155, 152, 0.6)'
          }
        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  </script>

</div>
     
<!-- jQuery 2.1.4 -->
<script src="../../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="../../../framework/bootstrap/js/bootstrap.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="jquery.btechco.excelexport.js"></script>
<script src="jquery.base64.js"></script>
<script>
$(document).ready(function () {
  $("#btnExport").click(function (e) {
    e.preventDefault();
    var table_div = document.getElementById('dvData');
    // esse "\ufeff" é importante para manter os acentos
    var blobData = new Blob(['\ufeff'+table_div.outerHTML], { type: 'application/vnd.ms-excel' });
    var url = window.URL.createObjectURL(blobData);
    var a = document.createElement('a');
    a.href = url;
    a.download = 'SETH_MC'
    a.click();
  });
});

</script>

    