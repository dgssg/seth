//Programa: Chamada de enfermagem unidade beiraleito
//Autor: MKS
 
/* Headers */ 
#include <WiFi.h> /* Header para uso das funcionalidades de wi-fi do ESP32 */
#include <HttpClient.h>
#include <PubSubClient.h>  /*  Header para uso da biblioteca PubSubClient */
#include "EasyMFRC522.h"
#include <HttpClient.h>
 
/* Defines do MQTT */
/* IMPORTANTE: recomendamos fortemente alterar os nomes
               desses tópicos. Caso contrário, há grandes
               chances de você enviar e receber mensagens de um ESP32
               de outra pessoa.
*/

#define MAX_STRING_SIZE 5000  // size of the char array that will be written to the tag
#define BLOCK 1              // initial block, from where the data will be stored in the tag


EasyMFRC522 rfidReader(5, 22); //the Mifare sensor, with the SDA and RST pins given
                                //the default (factory) keys A and B are used (or used setKeys to change)
                                
// printf-style function for serial output
void printfSerial(const char *fmt, ...);

/* Tópico MQTT para recepção de informações do broker MQTT para ESP32 */
#define TOPICO_SUBSCRIBE "mks_rfid_send_iscmc_01"   
/* Tópico MQTT para envio de informações do ESP32 para broker MQTT */
#define TOPICO_PUBLISH   "mks_rfid_read_iscmc_01"  
#define TOPICO_PUBLISH_status   "mks_rfid_status_iscmc_01" 
#define TOPICO_PUBLISH_write   "mks_rfid_write_iscmc_01" 
/* id mqtt (para identificação de sessão) */
/* IMPORTANTE: este deve ser único no broker (ou seja, 
               se um client MQTT tentar entrar com o mesmo 
               id de outro já conectado ao broker, o broker 
               irá fechar a conexão de um deles).
*/

#define ID_MQTT  "mks_rfid_01"     
/*  Variáveis e constantes globais */
/* SSID / nome da rede WI-FI que deseja se conectar */
const char* SSID = "Worksat_Douglas_2.4G"; 
/*  Senha da rede WI-FI que deseja se conectar */
const char* PASSWORD = "32497133dgssgDG@"; 
const int buttonPin = 13;  //chamado
int buttonState = 0;    //status do chamado
const int ledPin =  12;      // the number of the LED pin 
/* URL do broker MQTT que deseja utilizar */
const char* BROKER_MQTT = "broker.hivemq.com"; 
/* Porta do Broker MQTT */
int BROKER_PORT = 1883;
 
String serverName = "http://www.central.mksistemasbiomedicos.com.br/customer/iscmc/called/call.php";
String serverattendance = "http://www.central.mksistemasbiomedicos.com.br/customer/iscmc/called/attendance.php";
String serverfinish = "http://www.central.mksistemasbiomedicos.com.br/customer/iscmc/called/finish.php";
String serverstatus= "http://www.central.mksistemasbiomedicos.com.br/customer/iscmc/called/status.php";
/* Variáveis e objetos globais */
WiFiClient espClient;
PubSubClient MQTT(espClient);
  
//Prototypes
void init_serial(void);
void init_wifi(void);
void init_mqtt(void);
void reconnect_wifi(void); 
void mqtt_callback(char* topic, byte* payload, unsigned int length);
void verifica_conexoes_wifi_mqtt(void);
 
/* 
 *  Implementações das funções
 */
void setup() 
{
    init_serial();
    init_wifi();
    init_mqtt();
    rfidReader.init(); 
    pinMode(buttonPin, INPUT);
    pinMode(ledPin, OUTPUT);
}
  
/* Função: inicializa comunicação serial com baudrate 115200 (para fins de monitorar no terminal serial 
*          o que está acontecendo.
* Parâmetros: nenhum
* Retorno: nenhum
*/
void init_serial() 
{
    Serial.begin(115200);
}
 
/* Função: inicializa e conecta-se na rede WI-FI desejada
 * Parâmetros: nenhum
 * Retorno: nenhum
 */
void init_wifi(void) 
{
    delay(10);
    Serial.println("------Conexao WI-FI------");
    Serial.print("Conectando-se na rede: ");
    Serial.println(SSID);
    Serial.println("Aguarde");
    reconnect_wifi();
}
  
/* Função: inicializa parâmetros de conexão MQTT(endereço do  
 *         broker, porta e seta função de callback)
 * Parâmetros: nenhum
 * Retorno: nenhum
 */
void init_mqtt(void) 
{
    /* informa a qual broker e porta deve ser conectado */
    MQTT.setServer(BROKER_MQTT, BROKER_PORT); 
    /* atribui função de callback (função chamada quando qualquer informação do 
    tópico subescrito chega) */
    MQTT.setCallback(mqtt_callback);            
}
  
/* Função: função de callback 
 *          esta função é chamada toda vez que uma informação de 
 *          um dos tópicos subescritos chega)
 * Parâmetros: nenhum
 * Retorno: nenhum
 * */
void mqtt_callback(char* topic, byte* payload, unsigned int length) 
{
    String msg;
   int result;
     char stringBuffer[MAX_STRING_SIZE];
      int stringSize;
    //obtem a string do payload recebido
    for(int i = 0; i < length; i++) 
    {
       char c = (char)payload[i];
       msg += c;
    }
    Serial.print("[MQTT] Mensagem recebida: ");
    Serial.println(msg);

    
 if (msg == "1") {
 strcpy(stringBuffer, "usuario01");  // you may try a different string here, with LESS than MAX_STRING_SIZE characters
   int stringSize = strlen(stringBuffer);
      MQTT.publish(TOPICO_PUBLISH_status, "write");
      Serial.println("APPROACH a Mifare tag. Waiting...");

  bool success;
  do {
    // returns true if a Mifare tag is detected
    success = rfidReader.detectTag();    
    delay(50); //0.05s
  } while (!success);

  Serial.println("--> PICC DETECTED!");
     
    // starting from tag's block #1, writes a data chunk labeled "mylabel", with its content given by stringBuffer, of stringSize+1 bytes (because of the trailing 0 in strings) 
    result = rfidReader.writeFile(BLOCK, "mylabel", (byte*)stringBuffer, stringSize+1);

    if (result >= 0) {
      printfSerial("--> Successfully written \"%s\" to the tag, ending in block %d\n", stringBuffer, result);
    } else {
      printfSerial("--> Error writing to the tag: %d\n", result);
    }
  }
  if (msg == "2") {
 strcpy(stringBuffer, "usuario02");  // you may try a different string here, with LESS than MAX_STRING_SIZE characters
   int stringSize = strlen(stringBuffer);
      MQTT.publish(TOPICO_PUBLISH_status, "write");
      Serial.println("APPROACH a Mifare tag. Waiting...");

  bool success;
  do {
    // returns true if a Mifare tag is detected
    success = rfidReader.detectTag();    
    delay(50); //0.05s
  } while (!success);

  Serial.println("--> PICC DETECTED!");
     
   
   
  
    
    // starting from tag's block #1, writes a data chunk labeled "mylabel", with its content given by stringBuffer, of stringSize+1 bytes (because of the trailing 0 in strings) 
    result = rfidReader.writeFile(BLOCK, "mylabel", (byte*)stringBuffer, stringSize+1);

    if (result >= 0) {
      printfSerial("--> Successfully written \"%s\" to the tag, ending in block %d\n", stringBuffer, result);
    } else {
      printfSerial("--> Error writing to the tag: %d\n", result);
    }
  }
   if (msg == "3") {
 strcpy(stringBuffer, "usuario03");  // you may try a different string here, with LESS than MAX_STRING_SIZE characters
   int stringSize = strlen(stringBuffer);
      MQTT.publish(TOPICO_PUBLISH_status, "write");
      Serial.println("APPROACH a Mifare tag. Waiting...");

  bool success;
  do {
    // returns true if a Mifare tag is detected
    success = rfidReader.detectTag();    
    delay(50); //0.05s
  } while (!success);

  Serial.println("--> PICC DETECTED!");
     
   
   
  
    
    // starting from tag's block #1, writes a data chunk labeled "mylabel", with its content given by stringBuffer, of stringSize+1 bytes (because of the trailing 0 in strings) 
    result = rfidReader.writeFile(BLOCK, "mylabel", (byte*)stringBuffer, stringSize+1);

    if (result >= 0) {
      printfSerial("--> Successfully written \"%s\" to the tag, ending in block %d\n", stringBuffer, result);
    } else {
      printfSerial("--> Error writing to the tag: %d\n", result);
    }
  }
  if (msg == "4") {
 strcpy(stringBuffer, "usuario04");  // you may try a different string here, with LESS than MAX_STRING_SIZE characters
   int stringSize = strlen(stringBuffer);
      MQTT.publish(TOPICO_PUBLISH_status, "write");
      Serial.println("APPROACH a Mifare tag. Waiting...");

  bool success;
  do {
    // returns true if a Mifare tag is detected
    success = rfidReader.detectTag();    
    delay(50); //0.05s
  } while (!success);

  Serial.println("--> PICC DETECTED!");
     
   
   
  
    
    // starting from tag's block #1, writes a data chunk labeled "mylabel", with its content given by stringBuffer, of stringSize+1 bytes (because of the trailing 0 in strings) 
    result = rfidReader.writeFile(BLOCK, "mylabel", (byte*)stringBuffer, stringSize+1);

    if (result >= 0) {
      printfSerial("--> Successfully written \"%s\" to the tag, ending in block %d\n", stringBuffer, result);
    } else {
      printfSerial("--> Error writing to the tag: %d\n", result);
    }
  }
    
    if (msg == "a") {
      MQTT.publish(TOPICO_PUBLISH_status, "read");
       Serial.println("APPROACH a Mifare tag. Waiting...");

  bool success;
  do {
    // returns true if a Mifare tag is detected
    success = rfidReader.detectTag();    
    delay(50); //0.05s
  } while (!success);

  Serial.println("--> PICC DETECTED!");
      
    // starting from block #1, reads the data chunk labeled "mylabel", filling in the given buffer with the data
    result = rfidReader.readFile(BLOCK, "mylabel", (byte*)stringBuffer, MAX_STRING_SIZE);

    stringBuffer[MAX_STRING_SIZE-1] = 0;   // for safety; in case the string was stored without a \0 in the end 
                                           // (would not happen in this example, but it is a good practice when reading strings) 
      MQTT.publish(TOPICO_PUBLISH, stringBuffer);
    if (result >= 0) { // non-negative values indicate success, while negative ones indicate error
      printfSerial("--> String data retrieved: \"%s\" (bytes: %d)\n", stringBuffer, result);
    } else { 
      printfSerial("--> Error reading the tag (%d)! Probably there is no data labeled \"mylabel\".\n", result);
    }

  }
 
  
  // call this after doing all desired operations in the tag
  rfidReader.unselectMifareTag();
  
  Serial.println();
  Serial.println("Finished operation!");

         
}
  
/* Função: reconecta-se ao broker MQTT (caso ainda não esteja conectado ou em caso de a conexão cair)
 *          em caso de sucesso na conexão ou reconexão, o subscribe dos tópicos é refeito.
 * Parâmetros: nenhum
 * Retorno: nenhum
 */
void reconnect_mqtt(void) 
{
    while (!MQTT.connected()) 
    {
        Serial.print("* Tentando se conectar ao Broker MQTT: ");
        Serial.println(BROKER_MQTT);
        if (MQTT.connect(ID_MQTT)) 
        {
            Serial.println("Conectado com sucesso ao broker MQTT!");
            MQTT.subscribe(TOPICO_SUBSCRIBE); 
            MQTT.subscribe(TOPICO_PUBLISH_write); 
            
        } 
        else
        {
            Serial.println("Falha ao reconectar no broker.");
            Serial.println("Havera nova tentatica de conexao em 2s");
            delay(2000);
        }
    }
}
  
/* Função: reconecta-se ao WiFi
 * Parâmetros: nenhum
 * Retorno: nenhum
*/
void reconnect_wifi() 
{
    /* se já está conectado a rede WI-FI, nada é feito. 
       Caso contrário, são efetuadas tentativas de conexão */
    if (WiFi.status() == WL_CONNECTED)
        return;
         
    WiFi.begin(SSID, PASSWORD);
     
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(100);
        Serial.print(".");
    }
   
    Serial.println();
    Serial.print("Conectado com sucesso na rede ");
    Serial.print(SSID);
    Serial.println("IP obtido: ");
    Serial.println(WiFi.localIP());
}
 
/* Função: verifica o estado das conexões WiFI e ao broker MQTT. 
 *         Em caso de desconexão (qualquer uma das duas), a conexão
 *         é refeita.
 * Parâmetros: nenhum
 * Retorno: nenhum
 */
void verifica_conexoes_wifi_mqtt(void)
{
    /* se não há conexão com o WiFI, a conexão é refeita */
    reconnect_wifi(); 
    /* se não há conexão com o Broker, a conexão é refeita */
    if (!MQTT.connected()) 
        reconnect_mqtt(); 
} 

void printfSerial(const char *fmt, ...) {
  char buf[128];
  va_list args;
  va_start(args, fmt);
  vsnprintf(buf, sizeof(buf), fmt, args);
  va_end(args);
  Serial.print(buf);
}
 
/* programa principal */
void loop() 
{   
  int result;
     char stringBuffer[MAX_STRING_SIZE];
      int stringSize;
    /* garante funcionamento das conexões WiFi e ao broker MQTT */
    verifica_conexoes_wifi_mqtt();
    /* Envia frase ao broker MQTT */
   // MQTT.publish(TOPICO_PUBLISH, "ESP32 se comunicando com MQTT");
 
    /* keep-alive da comunicação com broker MQTT */    
    MQTT.loop();
    /* Agurda 1 segundo para próximo envio */
    buttonState = digitalRead(buttonPin);
    if (buttonState == HIGH) {
    // turn LED on:
   Serial.println("Solicitação de Chamado: ");
   HTTPClient http;
   String serverPath = serverName + "?id=1";
   http.begin(serverPath.c_str());
   int httpResponseCode = http.GET();
   if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
        if(payload == "1"){
          
          digitalWrite(ledPin, HIGH);
       
          MQTT.publish(TOPICO_PUBLISH_status, "read");
          Serial.println("APPROACH a Mifare tag. Waiting...");

          bool success;
          do {
           // returns true if a Mifare tag is detected
          success = rfidReader.detectTag();    
          delay(50); //0.05s
          } while (!success);

          Serial.println("--> PICC DETECTED!");
      
          // starting from block #1, reads the data chunk labeled "mylabel", filling in the given buffer with the data
           result = rfidReader.readFile(BLOCK, "mylabel", (byte*)stringBuffer, MAX_STRING_SIZE);

           stringBuffer[MAX_STRING_SIZE-1] = 0;   // for safety; in case the string was stored without a \0 in the end 
                                           // (would not happen in this example, but it is a good practice when reading strings) 
           MQTT.publish(TOPICO_PUBLISH, stringBuffer);
           if (result >= 0) { // non-negative values indicate success, while negative ones indicate error
           printfSerial("--> String data retrieved: \"%s\" (bytes: %d)\n", stringBuffer, result);
           } else { 
          printfSerial("--> Error reading the tag (%d)! Probably there is no data labeled \"mylabel\".\n", result);
           }

  
 
  
           // call this after doing all desired operations in the tag
            rfidReader.unselectMifareTag();
  
            Serial.println();
            Serial.println("Finished operation!");

             
               String serverPath = serverattendance + "?id=1&user="+stringBuffer;
               http.begin(serverPath.c_str());
               int httpResponseCode = http.GET();
               if (httpResponseCode>0) {
              Serial.print("HTTP Response code: ");
              Serial.println(httpResponseCode);
              String payload = http.getString();
              Serial.println(payload);
                  }

                
        
      }
 if(payload == "2"){
         MQTT.publish(TOPICO_PUBLISH_status, "read");
          Serial.println("APPROACH a Mifare tag. Waiting...");

          bool success;
          do {
           // returns true if a Mifare tag is detected
          success = rfidReader.detectTag();    
          delay(50); //0.05s
          } while (!success);

          Serial.println("--> PICC DETECTED!");
      
          // starting from block #1, reads the data chunk labeled "mylabel", filling in the given buffer with the data
           result = rfidReader.readFile(BLOCK, "mylabel", (byte*)stringBuffer, MAX_STRING_SIZE);

           stringBuffer[MAX_STRING_SIZE-1] = 0;   // for safety; in case the string was stored without a \0 in the end 
                                           // (would not happen in this example, but it is a good practice when reading strings) 
           MQTT.publish(TOPICO_PUBLISH, stringBuffer);
           if (result >= 0) { // non-negative values indicate success, while negative ones indicate error
           printfSerial("--> String data retrieved: \"%s\" (bytes: %d)\n", stringBuffer, result);
           } else { 
          printfSerial("--> Error reading the tag (%d)! Probably there is no data labeled \"mylabel\".\n", result);
           }

  
 
  
           // call this after doing all desired operations in the tag
            rfidReader.unselectMifareTag();
  
            Serial.println();
            Serial.println("Finished operation!");

             
               String serverPath = serverfinish  + "?id=1&user="+stringBuffer;
               http.begin(serverPath.c_str());
               int httpResponseCode = http.GET();
               if (httpResponseCode>0) {
              Serial.print("HTTP Response code: ");
              Serial.println(httpResponseCode);
              String payload = http.getString();
              Serial.println(payload);
                  }
 }
   }
      
      // Free resources
      http.end();
  } else {
    // turn LED off:
  //  Serial.println("Aguardando Solicitação: ");
  
 HTTPClient http;
   String serverPath = serverstatus + "?id=1";
   http.begin(serverPath.c_str());
   int httpResponseCode = http.GET();
   if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);

  if(payload == "1"){
          
          digitalWrite(ledPin, HIGH);
       
          MQTT.publish(TOPICO_PUBLISH_status, "read");
          Serial.println("APPROACH a Mifare tag. Waiting...");

          bool success;
          do {
           // returns true if a Mifare tag is detected
          success = rfidReader.detectTag();    
          delay(50); //0.05s
          } while (!success);

          Serial.println("--> PICC DETECTED!");
      
          // starting from block #1, reads the data chunk labeled "mylabel", filling in the given buffer with the data
           result = rfidReader.readFile(BLOCK, "mylabel", (byte*)stringBuffer, MAX_STRING_SIZE);

           stringBuffer[MAX_STRING_SIZE-1] = 0;   // for safety; in case the string was stored without a \0 in the end 
                                           // (would not happen in this example, but it is a good practice when reading strings) 
           MQTT.publish(TOPICO_PUBLISH, stringBuffer);
           if (result >= 0) { // non-negative values indicate success, while negative ones indicate error
           printfSerial("--> String data retrieved: \"%s\" (bytes: %d)\n", stringBuffer, result);
           } else { 
          printfSerial("--> Error reading the tag (%d)! Probably there is no data labeled \"mylabel\".\n", result);
           }

  
 
  
           // call this after doing all desired operations in the tag
            rfidReader.unselectMifareTag();
  
            Serial.println();
            Serial.println("Finished operation!");

             
               String serverPath = serverattendance + "?id=1&user="+stringBuffer;
               http.begin(serverPath.c_str());
               int httpResponseCode = http.GET();
               if (httpResponseCode>0) {
              Serial.print("HTTP Response code: ");
              Serial.println(httpResponseCode);
              String payload = http.getString();
              Serial.println(payload);
                  }

                
        
      }
 if(payload == "2"){
         MQTT.publish(TOPICO_PUBLISH_status, "read");
          Serial.println("APPROACH a Mifare tag. Waiting...");

          bool success;
          do {
           // returns true if a Mifare tag is detected
          success = rfidReader.detectTag();    
          delay(50); //0.05s
          } while (!success);

          Serial.println("--> PICC DETECTED!");
      
          // starting from block #1, reads the data chunk labeled "mylabel", filling in the given buffer with the data
           result = rfidReader.readFile(BLOCK, "mylabel", (byte*)stringBuffer, MAX_STRING_SIZE);

           stringBuffer[MAX_STRING_SIZE-1] = 0;   // for safety; in case the string was stored without a \0 in the end 
                                           // (would not happen in this example, but it is a good practice when reading strings) 
           MQTT.publish(TOPICO_PUBLISH, stringBuffer);
           if (result >= 0) { // non-negative values indicate success, while negative ones indicate error
           printfSerial("--> String data retrieved: \"%s\" (bytes: %d)\n", stringBuffer, result);
           } else { 
          printfSerial("--> Error reading the tag (%d)! Probably there is no data labeled \"mylabel\".\n", result);
           }

  
 
  
           // call this after doing all desired operations in the tag
            rfidReader.unselectMifareTag();
  
            Serial.println();
            Serial.println("Finished operation!");

             
               String serverPath = serverfinish  + "?id=1&user="+stringBuffer;
               http.begin(serverPath.c_str());
               int httpResponseCode = http.GET();
               if (httpResponseCode>0) {
              Serial.print("HTTP Response code: ");
              Serial.println(httpResponseCode);
              String payload = http.getString();
              Serial.println(payload);
                  }
 }
      
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
      }
      // Free resources
      http.end();
  
  digitalWrite(ledPin, LOW);
  }
    delay(1000);   
}
}
