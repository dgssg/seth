// host = '172.16.153.122';	// hostname or IP address
host = 'broker.hivemq.com';	// hostname or IP address
// host = '172.16.153.110';	// hostname or IP address
port = 8000;

ip = 'mks_rfid_status_mks_01';
mks_rfid_read_mks_01 = 'mks_rfid_read_mks_01';
mac = 'mks_rfid_send_mks_01';
input_cnt = 'mks_rfid_write_mks_01';

pub_qos = '0';
r_on='ON';
r_off='OFF';


useTLS = true;
username = null;
password = null;
// username = "jjolie";
// password = "aa";

// path as in "scheme:[//[user:password@]host[:port]][/]path[?query][#fragment]"
//    defaults to "/mqtt"
//    may include query and fragment
//
//path = "/mqtt";
// path = "/data/cloud?device=12345";

cleansession = true;
