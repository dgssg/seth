
<?php 

$codigoget = $_GET['device'];
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="Try HiveMQ MQTT browser client – a websocket client that you can use to experiment with publishing and subscribing MQTT messages over port 8000">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/jquery.minicolors.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
</head>
<body class="notconnected">
<div id="header">

</div>
<div id="content" class="row">
<div id="connection" class="row large-12 columns">
<input id="usuario" type="hidden" value="<?php printf($codigoget);?>"/>
<input id="load_cardread" type="hidden" />
    <div class="large-8 columns conniTop">
        <h3>Conexão</h3>
    </div>

    <div class="large-1 columns conniStatus">
        <div id="connectionStatus"></div>
    </div>

    <div class="large-2 columns conniArrow">
        <a class="small bottom conniArrow" onclick="websocketclient.render.toggle('conni');">
            <div class="icon-arrow-chevron"></div>
        </a>
    </div>
    <div class="large-12 columns" id="conniMain">
        <div class="panel">
            <div class="row">
                <form class="custom">
                    <div class="large-5 columns">

                        <input id="urlInput" type="hidden" value="broker.mqttdashboard.com">
                    </div>

                    <div class="large-1 columns">

                        <input id="portInput" type="hidden" value="8000"/>
                    </div>

                    <div class="large-4 columns">

                        <input id="clientIdInput" type="hidden"/>
                    </div>

                    <div class="large-2 columns">
                        <a id="connectButton" class="small button" onclick="websocketclient.connect();">Conectar</a>
                    </div>

                    <div class="large-2 columns">
                        <a id="disconnectButton" class="small button"
                           onclick="websocketclient.disconnect();">Disconnect</a>
                    </div>

                    <div class="large-4 columns">

                        <input id="userInput" type="hidden"/>
                    </div>

                    <div class="large-3 columns">

                        <input id="pwInput" type="hidden"/>
                    </div>

                    <div class="large-2 columns">

                        <input id="keepAliveInput" type="hidden" value="60"/>
                    </div>

                    <div class="large-1 columns">

                        <input id="sslInput" type="hidden"/>
                    </div>

                    <div class="large-2 columns">

                        <input class="checky" id="cleanSessionInput" type="hidden" checked="checked" disabled="disabled"/>
                    </div>

                    <div class="large-8 columns">

                        <input id="lwTopicInput" type="hidden"/>
                    </div>



                    <div class="large-2 columns">

                        <input class="checky" id="LWRInput" type="hidden"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<div class="empty"></div>
<div id="publish-sub" class="row large-12 columns">
    <div class="columns large-8">
        <div class="large-9 columns publishTop">
            <h3>Ação</h3>
        </div>

        <div class="large-3 columns publishArrow">
            <a class="small bottom publishArrow" onclick="websocketclient.render.toggle('publish');">
                <div class="icon-arrow-chevron"></div>
            </a>
        </div>

        <div class="large-12 columns" id="publishMain">

            <!-- Grid Example -->
            <div class="row panel" id="publishPanel">
                <div class="large-12 columns">
                    <form class="custom">
                        <div class="row">
                            <div class="large-6 columns">

                                <input id="publishTopic" type="hidden" value="mks_rfid_send_mks_01">
                            </div>
                            <div class="large-2 columns">
                                <label for="publishQoSInput"></label>
                                <input id="publishQoSInput" type="hidden" value="0">

                            </div>
                            <div class="large-2 columns">

                                <input id="publishRetain" type="hidden">
                            </div>
                            <div class="large-2 columns">
                                <a class="small button" id="publishButton" onclick="websocketclient.publish($('#publishTopic').val(),$('#publishPayload').val(),parseInt($('#publishQoSInput').val(),10),$('#publishRetain').is(':checked'))">Leitura</a>
                            </div>
                            <div class="large-2 columns">

                              <button id="loading-btn" type="button" class="btn btn-success" data-loading-text="Loading..."onclick="load_card();">Gravar Cartão</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">

                                <input id="publishPayload" type="hidden" value="a">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="empty"></div>
        <div class="large-9 columns messagesTop">
            <h3>Mensagem</h3>
        </div>

        <div class="large-3 columns messagesArrow">
            <a class="small bottom messagesArrow" onclick="websocketclient.render.toggle('messages');">
                <div class="icon-arrow-chevron"></div>
            </a>
        </div>

        <div class="large-12 columns" id="messagesMain">

            <!-- Grid Example -->
            <div class="row panel">
                <div class="large-12 columns">
                    <form class="custom">
                        <!--<div class="row">-->
                        <!--<div class="large-10 columns">-->
                        <!--<label>Filter</label>-->
                        <!--<input id="filterString" type="text">-->
                        <!--</div>-->

                        <!--<div class="large-2 columns">-->
                        <!--<a class="small button" id="filterButton"-->
                        <!--onclick="websocketclient.filter($('#filterString').val())">Filter</a>-->
                        <!--</div>-->
                        <!--</div>-->

                    </form>
                    <div class="row">
                        <ul id="messEdit" class="disc">

                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>






    <div class="columns large-4">

        <div class="large-8 columns subTop">
            <h3>Visualizar</h3>
        </div>

        <div class="large-3 columns subArrow">
            <a class="small bottom subArrow" onclick="websocketclient.render.toggle('sub');">
                <div class="icon-arrow-chevron"></div>
            </a>
        </div>
        <div class="large-12 columns" id="subMain">
            <div class="row panel">
                <div class="large-12 columns">

                    <a id="addSubButton" href="#data" class="small button addSubButton">Status Dispositivo</a>
                    <a id="addSubButton" href="#data2" class="small button addSubButton">Leitura Dispositivo </a>

                    <div style="display:none">
                        <div id="data">
                            <form class="custom">
                                <div class="row large-12 columns">
                                    <div class="large-4 columns">
                                        <label>Color</label>
                                        <input class="color" id="colorChooser" type="hidden">
                                    </div>
                                    <div class="large-5 columns">
                                        <label for="QoSInput"></label>
                                        <input id="QoSInput" class="small" type="hidden" value="0" >

                                    </div>
                                    <div class="large-3 columns">
                                        <a class="small button" id="subscribeButton"
                                           onclick="if(websocketclient.subscribe($('#subscribeTopic').val(),parseInt($('#QoSInput').val()),$('#colorChooser').val().substring(1))){$.fancybox.close();}">Visualizar</a>
                                    </div>
                                </div>
                                <div class="row large-12 columns">
                                    <div class="large-12 columns">

                                        <input id="subscribeTopic" type="hidden" value="mks_rfid_status_mks_01">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div style="display:none">
                        <div id="data2">
                            <form class="custom">
                                <div class="row large-12 columns">
                                    <div class="large-4 columns">
                                        <label>Color</label>
                                        <input class="color" id="colorChooser2" type="hidden">
                                    </div>
                                    <div class="large-5 columns">
                                        <label for="QoSInput"></label>
                                        <input id="QoSInput" class="small" type="hidden" value="0" >

                                    </div>
                                    <div class="large-3 columns">
                                        <a class="small button" id="subscribeButton"
                                           onclick="if(websocketclient.subscribe($('#subscribeTopic').val(),parseInt($('#QoSInput').val()),$('#colorChooser2').val().substring(1))){$.fancybox.close();}">Visualizar</a>
                                    </div>
                                </div>
                                <div class="row large-12 columns">
                                    <div class="large-12 columns">

                                        <input id="subscribeTopic" type="hidden" value="mks_rfid_read_mks_01">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <ul id="innerEdit" class="disc">

                    </ul>
                </div>
            </div>
        </div>
    </div>


</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/1.3.1/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/4.2.3/js/foundation.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/4.2.3/js/foundation/foundation.forms.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.1.0/moment.min.js"></script>
<script src="js/jquery.minicolors.min.js"></script>
<script src="js/mqttws31.js"></script>
<script src="js/encoder.js"></script>
<script src="js/app.js"></script>

<script>

    function randomString(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    $(document).foundation();
    $(document).ready(function () {

        $('#clientIdInput').val('clientId-' + randomString(10));

        $('#colorChooser').minicolors();
          $('#colorChooser2').minicolors();

        $("#addSubButton").fancybox({
            'afterShow': function () {
                var rndColor = websocketclient.getRandomColor();
                $("#colorChooser").minicolors('value', rndColor);
            }
        });
        $("#addSubButton").fancybox({
            'afterShow': function () {
                var rndColor = websocketclient.getRandomColor();
                $("#colorChooser2").minicolors('value', rndColor);
            }
        });

        websocketclient.render.toggle('publish');
        websocketclient.render.toggle('messages');
        websocketclient.render.toggle('sub');
    });
</script>
<script type="text/javascript">
 
    function load_card(){
      var usuario = document.getElementById('usuario').value;
      var card = websocketclient.render.toggle('messages');
      
      $('#load_cardread').load('sub_categorias_post_usuario.php?usuario='+$('#usuario').val()'&card='+$('#card').val());

}



  
    </script>

</body>
</html>
