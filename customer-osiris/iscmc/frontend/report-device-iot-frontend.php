 
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Relatorio Dispositivos </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 
           
         
            
             <!-- page content -->
       
           

           
            
            
            
            
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro <small>do Relatorio</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="report/report-device-iot-backend.php" method="post"  target="_blank">
 
 
                      
                      
                      <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Date Inicial<span
                        ></span></label>
                        <div class="col-md-6 col-sm-6">
                          <input class="form-control" class='date' type="date" name="date_start" ></div>
                      </div>
                      
                      <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Date Final<span
                        ></span></label>
                        <div class="col-md-6 col-sm-6">
                          <input class="form-control" class='date' type="date" name="date_end" ></div>
                      </div>
                      


                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Instituição</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">
                            <?php
                              
                              
                              
                              $result_cat_post  = "SELECT  id, unidade FROM unidade where trash = 1";
                              
                              $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              echo ' <option value=""> Selecione um setor</option>
';
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                $selected = ($id_unidade == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['unidade'].'</option>';
                              }
                            ?>		
                          </select>  
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                          
                        </div>	
                      </div>	
                      
                      <script>
                        $(document).ready(function() {
                          $('#instituicao').select2();
                          
                        });
                        
                      </script>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-left" name="setor" id="setor"  placeholder="setor">
                            <?php
                              
                              
                              
                              $result_cat_post  = "SELECT  id, nome FROM setor where trash = 1";
                              
                              $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              echo ' <option value=""> Selecione um setor</option>
';
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                $selected = ($unidade_setor == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['nome'].'</option>';
                              }
                            ?>		
                          </select>  
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um setor "></span>
                          
                        </div>	
                      </div>	
                      
                      <script>
                        $(document).ready(function() {
                          $('#setor').select2();
                          
                          $('#instituicao').change(function() {
                            $('#setor').select2().load('search/search-select-unidade-setor-change.php?id=' + $('#instituicao').val());
                            
                          });
                        });
                        
                      </script>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Area</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-left" name="id_unidade_area" id="id_unidade_area"  placeholder="setor">
                            <?php
                              
                              
                              
                              $result_cat_post  = "SELECT  id, nome FROM area where trash = 1";
                              
                              $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              echo ' <option value=""> Selecione um setor</option>
';
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                $selected = ($id_unidade_area == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['nome'].'</option>';
                              }
                            ?>		
                          </select>  
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um setor "></span>
                          
                        </div>	
                      </div>	
                      
                      <script>
                        $(document).ready(function() {
                          $('#id_unidade_area').select2();
                          
                          $('#setor').change(function() {
                            $('#id_unidade_area').select2().load('search/search-select-unidade-area-change.php?id=' + $('#setor').val());
                            
                          });
                        });
                        
                      </script>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Sensor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-left" name="id_device" id="id_device"  placeholder="Unidade">
                            <?php
                              
                              
                              
                              $result_cat_post  = "SELECT  id, nome,macadress FROM sensor where trash = 1";
                              
                              $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              echo ' <option value=""> Selecione um Sensor</option>
';
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                $selected = ($id_device == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['nome'].' - '.$row_cat_post['macadress'].'</option>';
                              }
                            ?>		
                          </select>  
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                          
                        </div>	
                      </div>	
                      
                      <script>
                        $(document).ready(function() {
                          $('#id_device').select2();
                          
                        });
                        
                      </script>
                 
                                               
                     <br>
                     
                    
                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatorio</button>
                         </center>
                        </div>
                      </div>
     
     
                    
                   
                   </div>
                </div>
              </div>
            </div>
         
         
         
         
        
            
                      
              
              

            

            
        </div>
            </div>
        <!-- /page content -->
                     