<?php include("../database/database.php"); ?>
<style>
    body {
      background: url('../background.png') no-repeat center center fixed;
      background-size: cover;
      color: #ffffff;
    }

    .alert {
      background-color: rgba(0, 0, 0, 0.5);
      margin-left: 40px;
    }

    .tile-stats {
      background-color: rgba(0, 0, 0, 0.5);
      border-radius: 10px;
      padding: 20px;
      text-align: center;
      margin-left: 40px;
    }

    .graph-container {
      background-color: rgba(0, 0, 0, 0.5);
      border-radius: 10px;
      padding: 20px;
      margin-left: 40px;
    }

    .graph-background {
      background-color: rgba(0, 0, 0, 0.5);
      border-radius: 10px;
      padding: 10px;
      margin-left: 40px;
    }

    .graph-container .google-visualization-chart-wrapper,
    .graph-container .google-visualization-table,
    .graph-background .google-visualization-chart-wrapper,
    .graph-background .google-visualization-table {
      border: none;
    }

    .graph-container .google-visualization-chart {
      background-color: transparent;
    }

    .graph-container .google-visualization-table {
      color: #ffffff;
      background-color: rgba(0, 0, 0, 0.5) !important;
    }

    .graph-background .google-visualization-chart {
      background-color: transparent;
    }

    .graph-background .google-visualization-table {
      color: #ffffff;
      background-color: rgba(0, 0, 0, 0.5) !important;
    }

    .table-header {
      background-color: #333;
      color: #fff;
    }
    
    .table-row {
      background-color: rgba(0, 0, 0, 0.5);
      color: #fff;
    }

    .odd-table-row {
      background-color: rgba(0, 0, 0, 0.4);
    }

    .selected-table-row {
      background-color: #555;
    }

    .hover-table-row:hover {
      background-color: #666;
    }

    .google-visualization-table {
      background-color: rgba(0, 0, 0, 0.5);
    }

    .google-visualization-table table {
      background-color: rgba(0, 0, 0, 0.5);
    }

    .google-visualization-table table td,
    .google-visualization-table table th {
      background-color: rgba(0, 0, 0, 0.5) !important;
      color: #fff;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row" style="display: inline-block;">
    <div class="tile_count">

      <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-check red"></i>
        Bem vindo ao
        <strong class="black">
          OSIRIS
          <small>Telemetria IoT</small>
        </strong>,
       Observação e Supervisão de Inspeção, Rastreamento e Integração de Sensores<a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas Biomédicos</a>.
      </div>
      <?php
        $stmt = $conn->prepare(" SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor ORDER BY notif.data DESC LIMIT 1");
        $stmt->execute();
        $stmt->bind_result($title, $notif_msg, $data);
        while ($stmt->fetch()) {};
      ?>
      <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-bell"></i>
        Última
        <strong class="black">Notificação</strong>
        <strong class="black"><?php echo $title; ?></strong> : <small><?php echo $notif_msg; ?> : <?php echo $data; ?></small>
      </div>

      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <div class>
            <div class="x_content">
              <div class="row">
                <?php
                  // Consultar os últimos dados de cada dispositivo
                  $sql = "SELECT painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
                          FROM painel 
                          INNER JOIN parametro ON parametro.id = painel.id_parametro
                          WHERE painel.id IN (
                              SELECT MAX(id) 
                              FROM painel 
                              GROUP BY id_parametro
                          )";
                  
                  $result = mysqli_query($conn, $sql);
                  
                  if (mysqli_num_rows($result) > 0) {
                    // Exibir os dados em gráficos de medidores
                    $rows = array(); // Array para armazenar os dados para os gráficos
                    while ($row = mysqli_fetch_assoc($result)) {
                      $deviceId = strtolower(preg_replace('/[^a-zA-Z0-9_]/', '', $row['nome'] . $row['unidade']));
                      $rows[] = array('id' => $deviceId, 'value' => $row['vlr']); // Armazenar dados no array

                      echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
                              <div class='tile-stats'>
                                  <h5>" . $row['nome'] . "</h5>
                                  <p>" . $row['reg_date'] . "</p>
                                  <p>" . $row['unidade'] . "</p>
                                  <div id='{$deviceId}' style='width: 400px; height: 120px;'></div>
                              </div>
                            </div>";
                    }
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
      google.charts.setOnLoadCallback(drawCharts);

      function drawCharts() {
        <?php
          // Exibir os gráficos de medidores
          foreach ($rows as $row) {
            echo "drawChart('{$row['id']}', {$row['value']});";
          }
        ?>
      }

      function drawChart(device, value) {
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', value] // Remova o rótulo, deixando-o vazio
        ]);
        
        var options = {
          width: 400,
          height: 120,
          redFrom: 8,
          redTo: 10,
          yellowFrom: -10,
          yellowTo: 2,
          greenFrom: 2,
          greenTo: 8,
          minorTicks: 5,
          min: -10,
          max: 10
        };
        
        var chart = new google.visualization.Gauge(document.getElementById(device));
        chart.draw(data, options);
      }
    </script>
 
<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 1 AND reg_date >= '$date_limit') OR (id_parametro = 4 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 1) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 4) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data = json_encode($temp_data);
  $json_hum_data = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline2);
  
  function drawChartline2() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data; ?>;
    var humData = <?php echo $json_hum_data; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura');
    data.addColumn('number', 'Umidade');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura e Umidade Sala Tomografia',
      curveType: 'function',
 backgroundColor: 'transparent', // Fundo transparente

legend: { position: 'bottom', textStyle: { color: '#FFF' } },
      titleTextStyle: {
        color: '#FFF'
      },
      hAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      },
      vAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      }
          };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart2'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart2" ></div>

<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 2 AND reg_date >= '$date_limit') OR (id_parametro = 5 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 2) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 5) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data3 = json_encode($temp_data);
  $json_hum_data3 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline3);
  
  function drawChartline3() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data3; ?>;
    var humData = <?php echo $json_hum_data3; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura');
    data.addColumn('number', 'Umidade');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura e Umidade Sala Tecnica',
      curveType: 'function',
 backgroundColor: 'transparent', // Fundo transparente

legend: { position: 'bottom', textStyle: { color: '#FFF' } },
      titleTextStyle: {
        color: '#FFF'
      },
      hAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      },
      vAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      }
          };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart3'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart3" ></div>

<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 3 AND reg_date >= '$date_limit') OR (id_parametro = 6 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 3) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 6) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data4 = json_encode($temp_data);
  $json_hum_data4 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline4);
  
  function drawChartline4() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data4; ?>;
    var humData = <?php echo $json_hum_data4; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura');
    data.addColumn('number', 'Umidade');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura e Umidade Sala Ressonância Magnetica',
      curveType: 'function',
 backgroundColor: 'transparent', // Fundo transparente

legend: { position: 'bottom', textStyle: { color: '#FFF' } },
      titleTextStyle: {
        color: '#FFF'
      },
      hAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      },
      vAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      }
          };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart4'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart4" ></div>
    <?php
              // Defina a data de 48 horas atrás
              $date_limit = date('Y-m-d', strtotime('-48 hours'));
              
              // Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nas últimas 48 horas
              $sql = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 7 AND reg_date >= '$date_limit')
)";
              
              // Execute a consulta e obtenha os resultados
              $result4 = mysqli_query($conn, $sql);
              
             
              // Converta os resultados em um array associativo para JSON
              $rows4 = array();
              while ($row4 = mysqli_fetch_assoc($result4)) {
                $rows4[] = $row4;
              }
              $json_data5 = json_encode($rows4);
            ?>
            
            <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChartline5);
              
              function drawChartline5() {
                var jsonData4 = <?php echo $json_data5; ?>;
                
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Data');
                data.addColumn('number', 'Corrente');
                
                // Adiciona as linhas com base nos dados fornecidos
                for (var i = 0; i < jsonData4.length; i++) {
                  var rowData = [
                    jsonData4[i].reg_date, // Assumindo que up_time contém os valores para o eixo X (Data)
                    parseFloat(jsonData4[i].vlr) // Assumindo que dados contém os valores para o eixo Y (Sensor)
                  ];
                  data.addRow(rowData);
                }
                
                
                var options = {
                  title: 'Corrente Criogenia 48 (horas)',
                  curveType: 'function',
 backgroundColor: 'transparent', // Fundo transparente

legend: { position: 'bottom', textStyle: { color: '#FFF' } },
      titleTextStyle: {
        color: '#FFF'
      },
      hAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      },
      vAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      }
                      };
                
                var chart = new google.visualization.LineChart(document.getElementById('curve_chart5'));
                chart.draw(data, options);
              }
            </script>
            
            
            
            <div class="clearfix"></div>
            <br>
            
            <div id="curve_chart5"  style="width: 100%; height: 500px"></div>
            
            <div class="clearfix"></div>
            <br>
            
<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 8 AND reg_date >= '$date_limit') OR (id_parametro = 9 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 8) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 9) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data6 = json_encode($temp_data);
  $json_hum_data6 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline6);
  
  function drawChartline6() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data6; ?>;
    var humData = <?php echo $json_hum_data6; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Temperatura IN');
    data.addColumn('number', 'Temperatura OUT');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Nível de Temperatura Entrada e Saida Criogenia',
      curveType: 'function',
 backgroundColor: 'transparent', // Fundo transparente

legend: { position: 'bottom', textStyle: { color: '#FFF' } },
      titleTextStyle: {
        color: '#FFF'
      },
      hAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      },
      vAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      }
          };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart6'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart6" ></div>


<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
FROM painel 
INNER JOIN parametro ON parametro.id = painel.id_parametro
WHERE painel.id IN (
    SELECT id
    FROM painel 
    WHERE (id_parametro = 10 AND reg_date >= '$date_limit') OR (id_parametro = 11 AND reg_date >= '$date_limit')
)";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Array para armazenar os dados de temperatura e umidade
  $temp_data = array();
  $hum_data = array();
  
  // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 10) {
      // Temperatura
      $temp_data[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 11) {
      // Umidade
      $hum_data[] = floatval($row5['vlr']);
    }
  }
  
  // Converter os arrays em JSON
  $json_temp_data7 = json_encode($temp_data);
  $json_hum_data7 = json_encode($hum_data);
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline7);
  
  function drawChartline7() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_data7; ?>;
    var humData = <?php echo $json_hum_data7; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Fluxo IN');
    data.addColumn('number', 'Fluxo OUT');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Fluxo de agua de entrada e saida da criogenia',
      curveType: 'function',
 backgroundColor: 'transparent', // Fundo transparente

legend: { position: 'bottom', textStyle: { color: '#FFF' } },
      titleTextStyle: {
        color: '#FFF'
      },
      hAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      },
      vAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      }
          };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart7'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart7" ></div>
<?php
  // Defina a data de 48 horas atrás
  $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
  
  // Consulta SQL para selecionar os dados de temperatura e umidade nas últimas 48 horas
  $sql2 = "SELECT parametro.id AS param_id, painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
    FROM painel 
    INNER JOIN parametro ON parametro.id = painel.id_parametro
    WHERE painel.id IN (
      SELECT id
      FROM painel 
      WHERE (id_parametro = 10 AND reg_date >= '$date_limit') OR 
            (id_parametro = 11 AND reg_date >= '$date_limit') OR 
            (id_parametro = 8 AND reg_date >= '$date_limit') OR 
            (id_parametro = 9 AND reg_date >= '$date_limit') 
    )";
  
  // Execute a consulta e obtenha os resultados
  $result5 = mysqli_query($conn, $sql2);
  
  // Verifique se há erros na consulta
  if (!$result5) {
    die('Erro na consulta: ' . mysqli_error($conn));
  }
  
  // Arrays para armazenar as diferenças de temperatura e umidade
  $diff_temp_8_9 = array();
  $diff_hum_10_11 = array();
  
  // Extrair os dados e calcular as diferenças
  while ($row5 = mysqli_fetch_assoc($result5)) {
    if ($row5['param_id'] == 8) {
      // Temperatura
      $diff_temp_8_9[] = floatval($row5['vlr']);
    } elseif ($row5['param_id'] == 11) {
      // Umidade
      $diff_hum_10_11[] = floatval($row5['vlr']);
    }
  }
  
  // Calcular as diferenças entre os pares de valores
  $temp_diff = array();
  $hum_diff = array();
  for ($i = 0; $i < count($diff_temp_8_9); $i++) {
    $temp_diff[] = $diff_temp_8_9[$i] - $diff_temp_8_9[$i];
    $hum_diff[] = $diff_hum_10_11[$i] - $diff_hum_10_11[$i];
  }
  
  // Converter os arrays em JSON
  $json_temp_diff = json_encode($temp_diff);
  $json_hum_diff = json_encode($hum_diff);
?>



<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline8);
  
  function drawChartline8() {
    // Obter os dados de temperatura e umidade do PHP
    var tempData = <?php echo $json_temp_diff; ?>;
    var humData = <?php echo $json_hum_diff; ?>;
    
    // Criar os dados para o gráfico
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Tempo');
    data.addColumn('number', 'Delta Temperatura');
    data.addColumn('number', 'Delta Fluxo');
    
    // Adicionar os valores de temperatura e umidade aos dados do gráfico
    var maxLength = Math.max(tempData.length, humData.length);
    for (var i = 0; i < maxLength; i++) {
      var tempValue = tempData[i] !== undefined ? tempData[i] : null;
      var humValue = humData[i] !== undefined ? humData[i] : null;
      data.addRow([i, tempValue, humValue]);
    }
    
    // Configurações do gráfico
    var options = {
      title: 'Delta Criogenia',
      curveType: 'function',
 backgroundColor: 'transparent', // Fundo transparente

legend: { position: 'bottom', textStyle: { color: '#FFF' } },
      titleTextStyle: {
        color: '#FFF'
      },
      hAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      },
      vAxis: {
        textStyle: { color: '#FFF' },
        titleTextStyle: { color: '#FFF' }
      }
      
          };
    
    // Desenhar o gráfico
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart8'));
    chart.draw(data, options);
  }
</script>

<div id="curve_chart8" ></div>
 </div>
</div>
<?php
  date_default_timezone_set('America/Sao_Paulo');
  $today = date("Y-m-d");
  $read_open = 1;
  
  $query = "SELECT sensor.nome, notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1";
  
  $resultados = $conn->query($query);
  $notifications = array();
  
  while ($row = mysqli_fetch_assoc($resultados)) {
    $notifications[] = $row;
  }
?>

<script>
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
        // Após o registro do Service Worker, mostrar notificações
        <?php foreach ($notifications as $notification): ?>
        var title = "<?php echo $notification['nome']; ?>";
        var options = {
          body: "<?php echo $notification['dados']; ?>"
        };
        showNotification(title, options);
        <?php endforeach; ?>
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  function showNotification(title, options) {
    if (Notification.permission === "granted") {
      var notification = new Notification(title, options);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification(title, options);
        }
      });
    }
  }
</script>

<?php 
  $query = "SELECT menu FROM tools";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      // printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if ($menu == "0") { ?>
<script type="text/javascript">
  window.onload = function() {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php } ?>
