<?php

$conn = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_mks_iscmc");
$conn_control = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_mks_iscmc");
$key = "mks";
$mod = "osiris";
$chatID = "-4057846376";
$message = "6420010515:AAFDY47xiVz80wiAncCHbV7lchuVBS2_IdM";
$query = "SELECT notif.id_sensor,notif.read_open,painel.id,parametro.ativo,parametro.alarm_min,parametro.alarm_max,parametro.id,painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
          FROM painel 
          INNER JOIN parametro ON parametro.id = painel.id_parametro LEFT JOIN notif ON notif.id_sensor = parametro.id
          WHERE painel.id IN (
              SELECT MAX(painel.id) 
              FROM painel 
              GROUP BY painel.id_parametro)  GROUP BY painel.id_parametro";

if($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result(
    $id_sensor,
    $read_open,
        $id_dados, 
        $ativo,
        $alarm_min,
        $alarm_max,
        $id_parametro,
        $vlr,
        $nome,
        $unidade,
        $reg_date
    );

    while ($stmt->fetch()) {
        if($vlr < $alarm_min || $vlr > $alarm_max) {
            $alerta = ($vlr < $alarm_min) ? "Parametro Baixo" : "Parametro Alto";
            $stmt_insert = $conn_control->prepare("INSERT INTO notif (id_sensor, dados, data, id_dados) VALUES (?, ?, ?, ?)");
            $stmt_insert->bind_param("ssss", $id_parametro, $vlr, $reg_date, $id_dados);
            $stmt_insert->execute();
            $last_id = $conn_control->insert_id;
            $stmt_insert->close();

            if($ativo == 0) {
                $message = 'Sistema Telemetria Osiris' . "\n";
                $message .= 'Nome: ' . $nome . "\n";
                $message .= 'Unidade: ' . $unidade . "\n";
                $message .= 'Alarme: ' . $alerta . "\n";
                $message .= 'Valor: ' . $vlr;

                $telegramApiUrl = "https://api.telegram.org/bot6420010515:AAFDY47xiVz80wiAncCHbV7lchuVBS2_IdM/sendMessage";

                $data = [
                    'chat_id' => $chatID,
                    'text' => $message,
                ];

                $options = [
                    'http' => [
                        'method' => 'POST',
                        'header' => 'Content-Type: application/x-www-form-urlencoded',
                        'content' => http_build_query($data),
                    ],
                ];

                $context = stream_context_create($options);
                $response = file_get_contents($telegramApiUrl, false, $context);
                
	$recipients = ["5541998945903", "55419"]; // Adicione quantos números desejar
	
	
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://graph.facebook.com/v19.0/104486769084481/messages',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS =>'{
		"messaging_product": "whatsapp",
		"to": "5541998945903",
		"type": "template",
		"template": {
				"name": "iscmc_alert",
				"language": {
						"code": "pt_BR"
				},
					"components": [
					{
						"type": "body",
						"parameters": [
								{
											"type": "text",
						"text": "Sistema Telemetria Osiris"
					},
					{
						"type": "text",
						"text": "'.$nome.'-'.$unidade.'-'.$alerta.' "
					},
					{
						"type": "text",
						"text": "'.$vlr.'"
								}
						]
					}
				]
				
		}
}',
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'Authorization: EAAtHP9Q4nkEBOwnqXJt4J0U6RxPheteeVz8Lm0oodRyD0s6NqEANqaXXb7bMcVEKSKokIA2pmmV8bAOSow1Kwj90bs4dY9PbWBmLwYZCBmYw1Tz2rFOmku2eftxlz0SuVYGL4OyDUBcgML8NV7x9sMHyKOEfamkWo75iFU18zMaoEdysWMCBT4MSYLhZAao0qVbhJxk0gHVhE6UfCRrDzpqaCd7C39ZAvMZD'
		),
	));
	
	$response = curl_exec($curl);
	
	curl_close($curl);
	echo $response;
	
	
	
	
            }
        }
        
    }

    $stmt->close();
}

$conn->close();
$conn_control->close();


?>

