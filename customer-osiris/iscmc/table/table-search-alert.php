<?php
	include("../database/database.php");
	$id = ($_GET["id"]);
	$id = trim($id);
	$query = "SELECT notif.id,parametro.nome,parametro.unidade, notif.dados, notif.read_open, notif.data,area.nome as 'area',setor.nome as 'setor',unidade.unidade FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN parametro ON parametro.id = notif.id_sensor ORDER by notif.id DESC";
	
	// Execute a query e retorne os resultados como JSON
	$resultados = $conn->query($query);
	$rows = array();
	while($r = mysqli_fetch_assoc($resultados)) {
		$rows[] = $r;
	}
	print json_encode($rows);
?>
