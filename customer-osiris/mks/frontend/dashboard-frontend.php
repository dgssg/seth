<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row" style="display: inline-block;">
    <div class="tile_count">

      <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-check red"></i>
        Bem vindo ao
        <strong class="black">
          OSIRIS
          <small>Telemetria IoT</small>
        </strong>,
       Observação e Supervisão de Inspeção, Rastreamento e Integração de Sensores<a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas Biomédicos</a>.
      </div>
      <?php
        $stmt = $conn->prepare(" SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor ORDER BY notif.data DESC LIMIT 1");
        $stmt->execute();
        $stmt->bind_result($title, $notif_msg, $data);
        while ($stmt->fetch()) {};
      ?>
      <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-bell"></i>
        Última
        <strong class="black">Notificação</strong>
        <strong class="black"><?php echo $title; ?></strong> : <small><?php echo $notif_msg; ?> : <?php echo $data; ?></small>
      </div>

      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <div class>
            <div class="x_content">
              <div class="row">
                <?php
                  // Consultar os últimos dados de cada dispositivo
                  $sql = "SELECT painel.vlr, parametro.nome, parametro.unidade, painel.reg_date 
                          FROM painel 
                          INNER JOIN parametro ON parametro.id = painel.id_parametro
                          WHERE painel.id IN (
                              SELECT MAX(id) 
                              FROM painel 
                              GROUP BY id_parametro
                          )";
                  
                  $result = mysqli_query($conn, $sql);
                  
                  if (mysqli_num_rows($result) > 0) {
                    // Exibir os dados em gráficos de medidores
                    $rows = array(); // Array para armazenar os dados para os gráficos
                    while ($row = mysqli_fetch_assoc($result)) {
                      $deviceId = strtolower(preg_replace('/[^a-zA-Z0-9_]/', '', $row['nome'] . $row['unidade']));
                      $rows[] = array('id' => $deviceId, 'value' => $row['vlr']); // Armazenar dados no array

                      echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
                              <div class='tile-stats'>
                                  <h5>" . $row['nome'] . "</h5>
                                  <p>" . $row['reg_date'] . "</p>
                                  <p>" . $row['unidade'] . "</p>
                                  <div id='{$deviceId}' style='width: 400px; height: 120px;'></div>
                              </div>
                            </div>";
                    }
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
      google.charts.setOnLoadCallback(drawCharts);

      function drawCharts() {
        <?php
          // Exibir os gráficos de medidores
          foreach ($rows as $row) {
            echo "drawChart('{$row['id']}', {$row['value']});";
          }
        ?>
      }

      function drawChart(device, value) {
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', value] // Remova o rótulo, deixando-o vazio
        ]);
        
        var options = {
          width: 400,
          height: 120,
          redFrom: 8,
          redTo: 10,
          yellowFrom: -10,
          yellowTo: 2,
          greenFrom: 2,
          greenTo: 8,
          minorTicks: 5,
          min: -10,
          max: 10
        };
        
        var chart = new google.visualization.Gauge(document.getElementById(device));
        chart.draw(data, options);
      }
    </script>
  </div>
</div>

<?php
  date_default_timezone_set('America/Sao_Paulo');
  $today = date("Y-m-d");
  $read_open = 1;
  
  $query = "SELECT sensor.nome, notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1";
  
  $resultados = $conn->query($query);
  $notifications = array();
  
  while ($row = mysqli_fetch_assoc($resultados)) {
    $notifications[] = $row;
  }
?>

<script>
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
        // Após o registro do Service Worker, mostrar notificações
        <?php foreach ($notifications as $notification): ?>
        var title = "<?php echo $notification['nome']; ?>";
        var options = {
          body: "<?php echo $notification['dados']; ?>"
        };
        showNotification(title, options);
        <?php endforeach; ?>
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  function showNotification(title, options) {
    if (Notification.permission === "granted") {
      var notification = new Notification(title, options);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification(title, options);
        }
      });
    }
  }
</script>

<?php 
  $query = "SELECT menu FROM tools";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      // printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if ($menu == "0") { ?>
<script type="text/javascript">
  window.onload = function() {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php } ?>
