  <?php
$codigoget = ($_GET["id"]);

// Create connection

include("database/database.php");
    $query = "SELECT sensor.id, sensor.macadress, sensor_type.nome as 'id_sensor_type', sensor.nome, sensor.id_area, sensor.id_status, sensor.up_time, sensor.file, sensor.reg_date, sensor.upgrade, sensor.bat, sensor.last_alarm, sensor.trash, sensor.id_alarm, sensor.imei, sensor.firmware, sensor.linha, sensor.iccid, sensor.id_location, sensor_usb.nome as 'usb', sensor_charger.nome as 'charger', sensor.ativo, sensor.dados, sensor.id_dados,area.nome as 'area',setor.nome as 'setor',unidade.unidade FROM sensor LEFT JOIN sensor_type ON sensor_type.id =  sensor.id_sensor_type LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN sensor_usb ON sensor_usb.id =  sensor.usb LEFT JOIN sensor_charger ON sensor_charger.id =  sensor.charger WHERE sensor.id = $codigoget";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($id,$macadress,$id_sensor_type, $nome, $id_area, $id_status,$up_time, $file, $reg_date,$upgrade, $bat, $last_alarm, $trash, $id_alarm, $imei, $firmware, $linha, $iccid, $id_location, $usb,$charger, $ativo, $dados, $id_dados,$area,$setor,$unidade);
      while ($stmt->fetch()) {
        
      }
    }


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Dashboard <small>Telemetria</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Telemetria <small>Sensor</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <?php
                      // Defina seus dados, suponha que você tenha esses valores em um array associativo chamado $dados
                      $dados = array(
                        "Macadress" => "$macadress",
                        "Sensor" => "$id_sensor_type",
                        "Nome" => "$nome",
                        "Unidade" => "$unidade",
                        "Setor" => "$setor",
                        "Área" => "$area",
                        "Status" => "$id_status",
                        "Up Time" => "$up_time",
                        "File" => "$file",
                        "Registro" => "$reg_date",
                        "Atualização" => "$upgrade",
                        "Bateria" => "$bat",
                        "Ultimo Alarme" => "$last_alarm",
                        "Lixeira" => "$trash",
                        "Registro Alarme" => "$id_alarm",
                        "IMEI" => "$imei",
                        "Firmware" => "$firmware",
                        "Linha" => "$linha",
                        "ICCID" => "$iccid",
                        "Localização" => "$id_location",
                        "USB" => "$usb",
                        "Energia" => "$charger",
                        "Ativo" => "$ativo",
                        "Dados" => "$dados",
                        "Registro Dados" => "$id_dados"
                      );
                    ?>
                    
                    <?php foreach ($dados as $campo => $valor): ?>
                    <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="<?php echo strtolower(str_replace(' ', '_', $campo)); ?>"><?php echo $campo; ?> <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input type="text" id="<?php echo strtolower(str_replace(' ', '_', $campo)); ?>" name="<?php echo strtolower(str_replace(' ', '_', $campo)); ?>" value="<?php echo htmlspecialchars($valor); ?>" readonly="readonly" required="required" class="form-control">
                      </div>
                    </div>
                    <?php endforeach; ?>



                  </div>
                </div>
              </div>
	       </div>
 





 <!-- Posicionamento -->
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-device-iot">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>


                </div>
              </div>




                </div>
              </div>
             
               