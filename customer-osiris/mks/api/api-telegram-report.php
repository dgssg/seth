<?php
  //============================================================+
  // File name   : example_001.php
  // Begin       : 2008-03-04
  // Last Update : 2013-05-14
  //
  // Description : Example 001 for TCPDF class
  //               Default Header and Footer
  //
  // Author: Nicola Asuni
  //
  // (c) Copyright:
  //               Nicola Asuni
  //               Tecnick.com LTD
  //               www.tecnick.com
  //               info@tecnick.com
  //============================================================+
  
  /**
  * Creates an example PDF TEST document using TCPDF
  * @package com.tecnick.tcpdf
  * @abstract TCPDF - Example: Default Header and Footer
  * @author Nicola Asuni
  * @since 2008-03-04
  */
  
  // Include the main TCPDF library (search for installation path).
  require_once('../../../framework/TCPDF/tcpdf.php');
  
  // create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('MK Sistemas Biomedicos');
  $pdf->SetTitle('Sistema OSIRIS');
  $pdf->SetSubject('Telemetria');
  $pdf->SetKeywords('Telemetria');
  
  // set default header data
  $pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH,'MK Sistemas Biomedicos', 'Sistema OSIRIS', array(0,64,255), array(0,64,128));
  $pdf->setFooterData(array(0,64,0), array(0,64,128));
  
  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  
  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  
  // set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  
  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  
  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
  }
  
  // ---------------------------------------------------------
  
  // set default font subsetting mode
  $pdf->setFontSubsetting(true);
  
  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage();
  
  // set text shadow effect
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));



// Create connection
include("../database/database.php");// remover ../




  $query="SELECT painel.vlr, parametro.nome, parametro.unidade, painel.reg_date  FROM painel  INNER JOIN parametro ON parametro.id = painel.id_parametro WHERE painel.id IN ( SELECT MAX(id)   FROM painel  GROUP BY id_parametro )";
  $row=1;
  $tabela = '';
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($vlr, $nome, $unidade, $reg_date );
    
    while ($stmt->fetch()) {
      $tabela .= '<tr>';
      $tabela .= '<th scope="row">' . $row . '</th>';
      $tabela .= '<td>' . $nome . '</td>';
      $tabela .= '<td>' . $unidade . '</td>';
      $tabela .= '<td>' . $vlr . '</td>';
      $tabela .= '<td>' . $reg_date . '</td>';
      $tabela .= '</tr>';
      $row = $row + 1;
    }
  }
  
     
  $html = <<<EOD
 
<div class="conteudo" style="min-height:900px; font-size:17px;">
  <table style="border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
    <tbody>
      <tr>
        <td align="center"><strong>Telemetria Sistema OSIRIS</strong> </td>
        <td align="center">
          
        </td>
      </tr>
    </tbody>
  </table>
 
<p><b><center> <h2><small>Dados:</small></h2> </center></b></p>
 
                    <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Unidade</th>
                            <th>Valor</th>
                          <th>Data</th>
                        
                        </tr>
                      </thead>
                      <tbody>
              $tabela
                      </tbody>
                    </table>
 
  <!-- Restante do código continua aqui... -->

</div>

<!-- Rodapé -->
<table width="100%" style="vertical-align: top;font-family:'Times New Roman',Times,serif; font-size: 11px;">
  <tr>
    <td align="center" colspan="2">
      <div style="padding-top: 20px;"><strong>Copyright © 2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados.</strong><br>www.mksistemasbiomedicos.com.br</div>
    </td>
  </tr>
  <tr>
    <td style="font-size: 9px" align="left">MK-01-ra-1.0</td>
    <td align="right"><!-- pagina --></td>
  </tr>
</table>
EOD;
  // Print text using writeHTMLCell()
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  
  // ---------------------------------------------------------
  
  // Close and output PDF document
  // This method has several options, check the source code documentation for more information.
  $pdf->Output('Sistema OSIRIS.pdf', 'I');
  
  //============================================================+
  // END OF FILE
  //============================================================+