<?php
include("database/database.php");
session_start();
if(!isset($_SESSION['usuario']) ){
	header ("Location: ../index.php");
}
if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
}
if( $_SESSION['mod'] != $mod ){
    header ("Location: ../index.php");
}
$usuariologado=$_SESSION['usuario'];
$instituicaologado=$_SESSION['instituicao'];
$setorlogado=$_SESSION['setor'];
$email=	$_SESSION['email'] ;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />

    <title>SETH-| Sistema de Telemetria IoT</title>

    <!-- Bootstrap -->
    <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    
    <!-- Bootstrap -->
    <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    
    <!-- bootstrap-progressbar -->
    <link href="../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../../framework/build/css/custom.min.css" rel="stylesheet">
    
    <!-- PNotify -->
    <link href="../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- <script src="jquery.min.js" ></script> -->
    <!-- Datatables -->
    
    <link href="../../framework/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="dashboard" class="site_title"><i class="fa fa-500px"></i> <span>SETH IoT</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="logo/img.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Bem vindo,</span>
                            <h2><?php printf($usuariologado) ?></h2>
                            <p class="glyphicon glyphicon-time" id="countdown"></p>
                            <span>
                                <script>
                                document.write(new Date().toLocaleDateString());
                                </script>
                            </span>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>Geral</h3>
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-home"></i> Início <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="dashboard" title="Painel de supervisão">Dashboard</a></li>

                                        <!--     <li><a href="index3.php">Dashboard3</a></li>  -->
                                    </ul>
                                </li>

                                <li><a><i class="fa fa-desktop"></i> Cadastro &amp; Consulta <span
                                            class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <!--<li><a href="general_elements.php">Instituição</a></li>-->

                                        <li><a>Localização<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li class="sub_menu"><a href="register-unidade"
                                                        title="Cadastrar ou gerenciar Unidades">Unidade</a>
                                                </li>
                                                <li><a href="register-setor"
                                                        title="Cadastrar ou gerenciar Setores">Setor</a>
                                                </li>
                                                <li><a href="register-area" title="Cadastrar ou gerenciar Area">Area</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a>Dispositivos IoT<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li><a href="register-device-iot"
                                                        title="Cadastrar ou gerenciar Dispositivos IoT">Dispositivos
                                                        IoT</a>
                                                </li>
                                            </ul>
                                        </li>


                                        <li><a href="register-user" title="Cadastrar ou gerenciar Usuário">Usuário</a>
                                        </li>


                                    </ul>
                                </li>


                                <li><a><i class="fa fa-bar-chart-o"></i> Relatório <span
                                            class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li><a>Relatórios<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li class="sub_menu"><a href="report-device-iot"
                                                        title="Gerenciamento e visualizar Relatório Dispositivos IoT">Dispositivos
                                                        IoT</a></li>
                                                <li><a href="report-alarm"
                                                        title="Gerenciamento e visualizar Relatório Alarmes">Alarmes
                                                    </a> </li>
                                                <li><a href="report-dados"
                                                        title="Gerenciamento e visualizar Relatório Monitoramento">Monitoramento</a>
                                                </li>







                                            </ul>
                                        </li>



                                    </ul>
                                </li>

                            </ul>
                        </div>


                        <div class="menu_section">
                            <h3>Alarmes </h3>
                            <ul class="nav side-menu">
                                <li><a href="alert-alarms" title="Gerenciamento de Alertas e Alarmes"><i
                                            class="fa fa-exclamation"></i> Alarmes <span class=""></span></a> </li>
                            </ul>
                        </div>
                        <div class="menu_section">
                            <h3>Business Intelligence </h3>
                            <ul class="nav side-menu">
                                <li><a href="business-intelligence" title="Gerenciamento de Business Intelligence"><i
                                    class="fa fa-desktop"></i> Business Intelligence <span class=""></span></a> </li>
                            </ul>
                        </div>
                        <div class="menu_section">

                            <ul class="nav side-menu">



                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Maximizar" id="goFS">

                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            <script>
                            var goFS = document.getElementById("goFS");
                            goFS.addEventListener("click", function() {
                                document.body.requestFullscreen();
                            }, false);
                            </script>

                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Bloquear" href="lockscreen">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>


                        </a>

                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <nav class="nav navbar-nav">
                        <ul class=" navbar-right">
                            <li class="nav-item dropdown open" style="padding-left: 15px;">
                                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true"
                                    id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                    <img src="logo/img.jpg" alt=""><?php printf($usuariologado) ?>
                                </a>
                                <div class="dropdown-menu dropdown-usermenu pull-right"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="profile"> <i
                                            class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></i> Perfil
                                    </a>



                                    <a class="dropdown-item" href="login"><i class="fa fa-sign-out pull-right"></i> Log
                                        Out</a>
                                </div>
                            </li>

                            <li role="presentation" class="nav-item dropdown open">
                                <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1"
                                    data-toggle="dropdown" aria-expanded="false" title="Novas Notificações">
                                    <i class="fa fa-bell-o"></i>

                                    <span class="badge badge-dark"> <?php include 'alert/api-alert.php';?> </span>
                                </a>


                        </ul>
                        </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <!-- /top navigation -->
            <!-- page content -->

            <?php include 'api.php';?>
            <?php include 'frontend/register-device-iot-edit-frontend.php';?>


            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    ©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a
                        href="https://mksistemasbiomedicos.com.br"></a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>
                                
                                <!-- jQuery
                                <script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
                                <!-- Bootstrap -->
                                <script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
                                <!-- FastClick -->
                                <script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
                                <!-- NProgress -->
                                <script src="../../framework/vendors/nprogress/nprogress.js"></script>
                                <!-- Chart.js -->
                                <script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
                                <!-- jQuery Sparklines -->
                                <script src="../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
                                <!-- Flot -->
                                <script src="../../framework/vendors/Flot/jquery.flot.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
                                <!-- Flot plugins -->
                                <script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
                                <script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
                                <script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
                                <!-- DateJS -->
                                <script src="../../framework/vendors/DateJS/build/date.js"></script>
                                <!-- bootstrap-daterangepicker -->
                                <script src="../../framework/vendors/moment/min/moment.min.js"></script>
                                <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
                                
                                <!-- Custom Theme Scripts -->
                                <script src="../../framework/build/js/custom.min.js"></script>
                                
                                
                                
                                
                                <!-- jQuery-->
                                <script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
                                <!-- Bootstrap
                                <script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
                                <!-- FastClick -->
                                <script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
                                <!-- NProgress -->
                                <script src="../../framework/vendors/nprogress/nprogress.js"></script>
                                <!-- Chart.js -->
                                <script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
                                <!-- gauge.js -->
                                <script src="../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
                                <!-- bootstrap-progressbar -->
                                <script src="../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
                                <!-- iCheck -->
                                <script src="../../framework/vendors/iCheck/icheck.min.js"></script>
                                <!-- Skycons -->
                                <script src="../../framework/vendors/skycons/skycons.js"></script>
                                <!-- Flot -->
                                <script src="../../framework/vendors/Flot/jquery.flot.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
                                <script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
                                <!-- Flot plugins -->
                                <script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
                                <script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
                                <script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
                                <!-- DateJS -->
                                <script src="../../framework/vendors/DateJS/build/date.js"></script>
                                <!-- JQVMap -->
                                <script src="../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
                                <script src="../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
                                <script src="../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
                                <!-- bootstrap-daterangepicker -->
                                <script src="../../framework/vendors/moment/min/moment.min.js"></script>
                                <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
                                
                                <!-- Custom Theme Scripts -->
                                <script src="../../framework/build/js/custom.min.js"></script>
                                <!-- PNotify -->
                                <script src="../../framework/vendors/pnotify/dist/pnotify.js"></script>
                                <script src="../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
                                <script src="../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>
                                
                                <script src="../../framework/assets/js/select2.min.js"></script>
                                
                                <!-- bootstrap-wysiwyg -->
                                <script src="../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
                                <script src="../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
                                <script src="../../framework/vendors/google-code-prettify/src/prettify.js"></script>
                                <!-- jQuery Tags Input -->
                                <script src="../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
                                <!-- Switchery -->
                                <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
                                <!-- Select2 -->
                                <script src="../../framework/vendors/select2/dist/js/select2.full.min.js"></script>
                                <!-- Parsley -->
                                <script src="../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
                                <!-- Autosize -->
                                <script src="../../framework/vendors/autosize/dist/autosize.min.js"></script>
                                <!-- jQuery autocomplete -->
                                <script src="../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
                                <!-- starrr -->
                                <script src="../../framework/vendors/starrr/dist/starrr.js"></script>
                                <!-- Contador de caracter -->
                                <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>
                                
                                <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
                                <!-- Datatables -->
                                <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
                                <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
                                <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
                                <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
                                <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
                                <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
                                <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
                                <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
                                <script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
                                <script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>
                                
                                <script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
                                <script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
                                <script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
                                <script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
                                </body>
                                <script language="JavaScript">
                                    
                                    
                                    history.pushState(null, null, document.URL);
                                    window.addEventListener('popstate', function () {
                                        history.pushState(null, null, document.URL);
                                    });
                                </script>
                                <script type="text/javascript">
                                    
                                    // Total seconds to wait
                                    var seconds = 1080;
                                    var timer;
                                    var interval;
                                    
                                    function countdown() {
                                        
                                        seconds--;
                                        if (seconds < 0) {
                                            clearInterval(interval);
                                            // Change your redirection link here
                                            window.location = "https://seth.mksistemasbiomedicos.com.br";
                                        } else if (seconds === 60) {
                                            // Show warning 60 seconds before redirect
                                            var warning = document.createElement("div");
                                            warning.innerHTML = "O sistema será desconectado por motivos de segurança em 60 segundos. Clique aqui para continuar no sistema e reiniciar o tempo.";
                                            warning.style.backgroundColor = "yellow";
                                            warning.style.padding = "10px";
                                            warning.style.position = "fixed";
                                            warning.style.bottom = "0";
                                            warning.style.right = "0";
                                            warning.style.zIndex = "999";
                                            warning.addEventListener("click", function() {
                                                warning.style.display = "none";
                                                clearTimeout(seconds);
                                                seconds = 1080;
                                                countdown();
                                            });
                                            document.body.appendChild(warning);
                                            countdown();
                                        } else {
                                            // Update remaining seconds
                                            document.getElementById("countdown").innerHTML = seconds;
                                            // Count down using javascript
                                            
                                            timer = setTimeout(countdown, 1000);
                                        }
                                    }
                                    
                                    // Run countdown function
                                    countdown();
                                    
                                    
                                    function stopCountdown() {
                                        clearInterval(seconds);
                                    }
                                    
                                    // Reset countdown on user interaction
                                    document.addEventListener("click", function() {
                                        clearTimeout(seconds);
                                        seconds = 1080;
                                        //  countdown();
                                    });
                                    document.addEventListener("mousemove", function() {
                                        seconds--;
                                        //clearTimeout(seconds);
                                        seconds = 1080;
                                        //  countdown();
                                    });
                                    
                                </script>
                                <script  type="text/javascript">
                                    $(function () {
                                        $('#myDatepicker').datetimepicker();
                                    });
                                    
                                    $('#myDatepicker2').datetimepicker({
                                        format: 'DD.MM.YYYY'
                                    });
                                    
                                    $('#myDatepicker3').datetimepicker({
                                        format: 'hh:mm '
                                    });
                                    $('#myDatepicker8').datetimepicker({
                                        format: 'hh:mm '
                                    });
                                    
                                    $('#myDatepicker4').datetimepicker({
                                        ignoreReadonly: true,
                                        allowInputToggle: true
                                    });
                                    
                                    $('#datetimepicker6').datetimepicker();
                                    
                                    $('#datetimepicker7').datetimepicker({
                                        useCurrent: false
                                    });
                                    
                                    $("#datetimepicker6").on("dp.change", function(e) {
                                        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
                                    });
                                    
                                    $("#datetimepicker7").on("dp.change", function(e) {
                                        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
                                    });
                                    
                                </script>
                                <script>
                                    var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
                                    $("#demoform").submit(function() {
                                        alert($('[name="duallistbox_demo1[]"]').val());
                                        return false;
                                    });
                                </script>
                                <script>
                                    var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
                                    $("#demoform").submit(function() {
                                        alert($('[name="instituicao_id"]').val());
                                        return false;
                                    });
                                </script>
                                
                                </html>
