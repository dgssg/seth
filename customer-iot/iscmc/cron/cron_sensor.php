<?php
	
	$conn = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_iot_iscmc");
	$conn_control= mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_iot_iscmc");
	$key = "iscmc";
	$mod="iot";
	$alerta = "Sensor Offline";
	$dados = "Desligado";
	
	$query="SELECT 
	sensor.id,sensor.nome,sensor.macadress,sensor.telegram_chat_id,sensor.telegram,sensor.telegram_token 
	FROM 
	sensor 
	WHERE 
	sensor.ativo = 0
	AND TIMESTAMPDIFF(MINUTE, sensor.up_time, NOW()) > 30 and sensor.id_status = 1 ;
";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result(
			$id,
			$nome,
			$macadress,
			$chatID,
			$telegram,
			$botToken
			
		);
		while ($stmt->fetch()) {
			$stmt = $conn_control->prepare("UPDATE sensor SET id_status= 1 WHERE id= ?");
			$stmt->bind_param("s",$id);
			$execval = $stmt->execute();
			
			if($telegram == 0){
				
				
				$message = 'Sistema Telemetria' . "\n";
				
				$message .= 'Nome: ' . $nome . "\n";
				$message .= 'Macadress: ' . $macadress . "\n";
				$message .= 'Alarme: ' . $alerta . "\n";
				$message .= 'Valor: ' . $dados;
				
				$telegramApiUrl = "https://api.telegram.org/bot$botToken/sendMessage";
				
				$data = [
					'chat_id' => $chatID,
					'text' => $message,
				];
				
				$options = [
					'http' => [
						'method' => 'POST',
						'header' => 'Content-Type: application/x-www-form-urlencoded',
						'content' => http_build_query($data),
					],
				];
				
				$context = stream_context_create($options);
				$response = file_get_contents($telegramApiUrl, false, $context);
				
				
				
			}
			
		}
	}
	
?>