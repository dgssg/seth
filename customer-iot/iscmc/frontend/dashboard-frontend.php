<!-- page content -->

<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row" style="display: inline-block;">
    <div class="tile_count">

      <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-check red"></i>
        Bem vindo ao
        <strong class="black">
          SETH
          <small>Telemetria IoT</small>
        </strong>,
        Sistema de Telemetria IoT Hospitalar <a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas Biomédicos</a>.
      </div>
      <?php
        $stmt = $conn->prepare(" SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor ORDER BY notif.data DESC LIMIT 1");
        $stmt->execute();
        $stmt->bind_result($title,$notif_msg,$data);
        while ( $stmt->fetch()) {
          
        };
        
      ?>
      <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>
        <i class="ace-icon fa fa-bell"></i>
        Última
        <strong class="black">
          Notificação
        </strong>
        <strong class="black"><?php echo $title; ?></strong> : <small><?php echo $notif_msg; ?> : <?php echo $data; ?></small>
      </div>
  
      
    
     
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <div class>
            <div class="x_content">
              <div class="row">
     
          <?php
          // Consultar os últimos dados de cada dispositivo
            $sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time
            FROM sensor_dados sd
            INNER JOIN (
              SELECT sensor_dados.device, MAX(sensor_dados.id) AS ultimo_id
              FROM sensor_dados
              GROUP BY device
            ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
            INNER JOIN (
              SELECT nome AS nome, up_time, macadress
              FROM sensor
            ) dados_ids ON sd.device = dados_ids.macadress;
";
          $result = mysqli_query($conn, $sql);

          if (mysqli_num_rows($result) > 0) {
            // Exibir os dados em gráficos de medidores
            while ($row = mysqli_fetch_assoc($result)) {
              
              echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
    <div class='tile-stats'>
        <h5>" . $row['nome'] . "</h5>
        <p>" . $row['up_time'] . "</p>
        <p>" . $row['device'] . "</p>
     <div id='{$row['device']}' style='width: 400px; height: 120px;'>
        
      </div>
    </div>
  </div>";
            }
          }
          ?>
        </div>
      </div>
              </div>
            </div>
          </div>
  
    </div>
    <?php
      // Consultar os dados dos sensores
      $query = "SELECT sensor.up_time FROM sensor where ativo = 0";
      $resultados = $conn->query($query);
      $rows = array();
      while ($r = mysqli_fetch_assoc($resultados)) {
        $rows[] = $r;
      }
      $json_data = json_encode($rows);
    ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
      google.charts.setOnLoadCallback(drawCharts);

      function drawCharts() {
        <?php
        // Exibir os gráficos de medidores
        mysqli_data_seek($result, 0); // Voltar para o início do resultado
        while ($row = mysqli_fetch_assoc($result)) {
          echo "drawChart('{$row['device']}', {$row['dados']});";
        }
        ?>
        drawDonutChart(<?php echo $json_data; ?>);
      }

      function drawChart(device, value) {
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['', value] // Remova o rótulo, deixando-o vazio
        ]);
        
        var options = {
          width: 400,
          height: 120,
          redFrom: 8,
          redTo: 10,
          yellowFrom:-10,
          yellowTo: 2,
          greenFrom: 2,
          greenTo: 8,
          minorTicks: 5,
          min: -10,
          max: 10
        };
        
        var chart = new google.visualization.Gauge(document.getElementById(device));
        
        chart.draw(data, options);
      }

      function drawDonutChart(jsonData) {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Task');
        data.addColumn('number', 'Hours per Day');

        var now = new Date();
        var thirtyMinutesAgo = new Date(now.getTime() - 30 * 60000); // 30 minutos atrás

        var activeCount = 0;
        jsonData.forEach(function (row) {
          var upTime = new Date(row['up_time']);
          if (upTime > thirtyMinutesAgo) {
            activeCount++;
          }
        });

        data.addRows([
          ['Online', activeCount],
          ['Offline', jsonData.length - activeCount]
        ]);

        var options = {
          title: 'Sensores Ativos',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
    
    <div id="donutchart" style="width: 500px; height: 500px;"></div>
    <?php
      // Consulta SQL para selecionar sensores inativos com up_time superior a 30 minutos
      $query3 = "SELECT nome, macadress , up_time
      FROM sensor 
      WHERE ativo = 0 and id_status = 2";
      
      // Execute a consulta e obtenha os resultados
      $result3 = mysqli_query($conn, $query3);
      
      // Verifique se há erros na consulta
      if (!$result3) {
        die('Erro na consulta: ' . mysqli_error($conn));
      }
      
      // Converta os resultados em um array associativo para JSON
      $rows3 = array();
      while ($row3 = mysqli_fetch_assoc($result3)) {
        $rows3[] = $row3;
      }
      $json_data3 = json_encode($rows3);
    ?>
    
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);
      
      function drawTable() {
        console.log("Desenhando a tabela...");
        var jsonData3 = <?php echo $json_data3; ?>;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Nome');
        data.addColumn('string', 'Mac Address');
        
        // Adicione uma linha para cada sensor
        jsonData3.forEach(function(sensor) {
          var now = new Date();
          var thirtyMinutesAgo = new Date(now.getTime() - 30 * 60000); // 30 minutos atrás
          var upTime = new Date(sensor.up_time);
          if (!sensor.ativo ) {
            data.addRow([sensor.nome, sensor.macadress]);
          }
        });
        
        var table = new google.visualization.Table(document.getElementById('table_div'));
        
        table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }

    </script>
    <div id="table_div"></div>
    <?php
      // Defina a data de 48 horas atrás
      $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
      
      // Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nas últimas 48 horas
      $sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time
      FROM sensor_dados sd
      INNER JOIN (
        SELECT sensor_dados.device, (sensor_dados.id) AS ultimo_id
        FROM sensor_dados
        WHERE sensor_dados.reg_date >= '$date_limit'  -- Limite para as últimas 48 horas
       ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
      INNER JOIN (
        SELECT nome AS nome, up_time, macadress, id_sensor_type
        FROM sensor
      ) dados_ids ON sd.device = dados_ids.macadress
      WHERE dados_ids.id_sensor_type = 3";
      
      // Execute a consulta e obtenha os resultados
      $result4 = mysqli_query($conn, $sql);
      
      // Verifique se há erros na consulta
      if (!$result4) {
        die('Erro na consulta: ' . mysqli_error($conn));
      }
      
      // Converta os resultados em um array associativo para JSON
      $rows4 = array();
      while ($row4 = mysqli_fetch_assoc($result4)) {
        $rows4[] = $row4;
      }
      $json_data4 = json_encode($rows4);
    ?>

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartline);
      
      function drawChartline() {
        var jsonData4 = <?php echo $json_data4; ?>;
        
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Data');
        data.addColumn('number', 'Sensor');
        
        // Adiciona as linhas com base nos dados fornecidos
        for (var i = 0; i < jsonData4.length; i++) {
          var rowData = [
            jsonData4[i].reg_date, // Assumindo que up_time contém os valores para o eixo X (Data)
            parseFloat(jsonData4[i].dados) // Assumindo que dados contém os valores para o eixo Y (Sensor)
          ];
          data.addRow(rowData);
        }
        
              
        var options = {
          title: 'Nivel',
          curveType: 'function',
          legend: { position: 'bottom' }
        };
        
        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
        chart.draw(data, options);
      }
    </script>



    <div id="curve_chart" style="width: 900px; height: 500px"></div>
  </div>
</div>

<?php
  date_default_timezone_set('America/Sao_Paulo');
  $today = date("Y-m-d");
  $read_open = 1;
  
  $query = "SELECT sensor.nome,notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor WHERE read_open = 1";
  
  $resultados = $conn->query($query);
  $notifications = array();
  
  while ($row = mysqli_fetch_assoc($resultados)) {
    $notifications[] = $row;
  }
  
?>

<script>
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('service-worker.js').then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
      }, function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    });
  }
  
  function showNotification(title, options) {
    if (Notification.permission === "granted") {
      var notification = new Notification(title, options);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then(function(permission) {
        if (permission === "granted") {
          var notification = new Notification(title, options);
        }
      });
    }
  }
  
  window.onload = function() {
    <?php foreach ($notifications as $notification): ?>
    var title = "<?php echo $notification['nome']; ?>";
    var options = {
      body: "<?php echo $notification['dados']; ?>",
      // Adicione mais opções de notificação conforme necessário
    };
    showNotification(title, options);
    <?php endforeach; ?>
  };
</script>

<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>