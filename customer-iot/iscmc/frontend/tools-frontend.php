<?php
	$sweet_salve = ($_GET["sweet_salve"]);
	$sweet_delet = ($_GET["sweet_delet"]);
	if ($sweet_delet == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
		Swal.fire({
				position: \'top-end\',
				icon: \'success\',
				title: \'Seu trabalho foi deletado!\',
				showConfirmButton: false,
				timer: 1500
		});
</script>';
		
		
	}
	if ($sweet_salve == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
				Swal.fire({
						position: \'top-end\',
						icon: \'success\',
						title: \'Seu trabalho foi salvo!\',
						showConfirmButton: false,
						timer: 1500
				});
		</script>';
		
		
	}
?>
<?php
	
	include("database/database.php");
	
	
	//$con->close();
	
	
	
?>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Parametrização <small>Tabelas do sistema</small></h3>
			</div>
			
			
		</div>
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Alerta</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<div class="alert alert-success alert-dismissible " role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<strong>Area de Edição!</strong> Após realizar uma ação utlize o botão [<i class="fa fa-refresh"></i> ] ao lado,
					para atualizar informações alteradas.
				</div>
				
				
				
			</div>
		</div>
		
		
		<div class="clearfix"></div>
	</div>
	
	<div class="row" style="display: block;">
		<div class="col-md-12 col-sm-12  ">
			<div class="x_panel">
				<div class="x_title">
					<h2>Automação <small>Configuração</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Settings 1</a>
								<a class="dropdown-item" href="#">Settings 2</a>
							</div>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<form action="backend/update-tools-backend.php" method="post">
						 
						
						
						
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
							<div class="col-md-6 col-sm-6 ">
								<input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
							</div>
						</div>
						
						<div class="form-group row">
							<label class="col-form-label col-md-3 col-sm-3 "></label>
							<div class="col-md-3 col-sm-3 ">
								<center>
									<button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
								</center>
							</div>
						</div>
						
						
					</form>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Logo Documentação<small> </small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Settings 1</a>
								<a class="dropdown-item" href="#">Settings 2</a>
							</div>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<div class="row">
						<div class="col-md-55">
							
							<div class="thumbnail">
								<div class="image view view-first">
									<img style="width: 100%; display: block;" src="logo/clientelogo.png" alt="image" />
									<div class="mask">
										<p>Logo Documentação</p>
										<div class="tools tools-bottom">
											<a href="logo/clientelogo.png" download><i class="fa fa-download"></i></a>
											
										</div>
									</div>
								</div>
								<div class="caption">
									<p>Logo</p>
								</div>
							</div>
							
						</div>
						
						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="x_panel">
		<div class="x_title">
			<h2>Trocar Logo Documentação</h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a>
						</li>
						<li><a href="#">Settings 2</a>
						</li>
					</ul>
				</li>
				<li><a class="close-link"><i class="fa fa-close"></i></a>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="alert alert-danger alert-dismissible " role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<strong>Atenção!</strong> Utilizar arquivos com a seguinte extensão ".png".
			</div>
			
			<form  class="dropzone" action="backend/tools-logo-upload-backend.php" method="post">
				</form >
				
				
				
				</div>
				</div>
				
				
				<div class="row">
					<div class="col-md-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Logo Sistema<small> </small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="#">Settings 1</a>
											<a class="dropdown-item" href="#">Settings 2</a>
										</div>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								
								<div class="row">
									<div class="col-md-55">
										
										<div class="thumbnail">
											<div class="image view view-first">
												<img style="width: 100%; display: block;" src="logo/clientelogo.png" alt="image" />
												<div class="mask">
													<p>Logo Sistema</p>
													<div class="tools tools-bottom">
														<a href="logo/img.jpg" download><i class="fa fa-download"></i></a>
														
													</div>
												</div>
											</div>
											<div class="caption">
												<p>Logo</p>
											</div>
										</div>
										
									</div>
									
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				
				
				
				
				
				
				
				<div class="x_panel">
					<div class="x_title">
						<h2>Trocar Logo Sistema</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
									class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a>
									</li>
									<li><a href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="alert alert-danger alert-dismissible " role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<strong>Atenção!</strong> Utilizar arquivos com a seguinte extensão ".img".
						</div>
						<form>
							
						</form>
						<form  class="dropzone" action="backend/tools-logo-sistema-upload-backend.php" method="post">
							</form >
							
							
							
							</div>
							</div>
							
							</div>
							</div>
							
							<!-- Modal adcionar itens de Configuração'-->
							
							<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										
										<div class="modal-header">
											<h4 class="modal-title" id="myModalLabel">Categoria</h4>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
											</button>
										</div>
										<div class="modal-body">
											<form action="backend/tools-add-category-backend.php" method="post">
												<div class="ln_solid"></div>
												
												
												
												
												<div class="item form-group">
													<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
													<div class="col-md-6 col-sm-6 ">
														<input id="nome" class="form-control" type="text" name="nome"  >
													</div>
												</div>
												
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
													<button type="submit" class="btn btn-primary" onclick="new PNotify({
														title: 'Registrado',
														text: 'Informações registrada!',
														type: 'success',
														styling: 'bootstrap3'
													});" >Salvar Informações</button>
												</div>
												
												</div>
												</div>
												</div>
											</form>
											
											
											<!-- -->
											
											<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
												<div class="modal-dialog modal-lg">
													<div class="modal-content">
														
														<div class="modal-header">
															<h4 class="modal-title" id="myModalLabel">Tecnovigilância</h4>
															<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
															</button>
														</div>
														<div class="modal-body">
															<form action="backend/tools-add-technological-backend.php" method="post">
																<div class="ln_solid"></div>
																
																
																<div class="item form-group">
																	<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
																	<div class="col-md-6 col-sm-6 ">
																		<input id="cod" class="form-control" type="text" name="cod"  >
																	</div>
																</div>
																
																<div class="item form-group">
																	<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
																	<div class="col-md-6 col-sm-6 ">
																		<input id="nome" class="form-control" type="text" name="nome"  >
																	</div>
																</div>
																
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																	<button type="submit" class="btn btn-primary" onclick="new PNotify({
																		title: 'Registrado',
																		text: 'Informações registrada!',
																		type: 'success',
																		styling: 'bootstrap3'
																	});" >Salvar Informações</button>
																</div>
																
																</div>
																</div>
																</div>
															</form>
															
															<!-- -->
															<div class="modal fade bs-example-modal-lg24" tabindex="-1" role="dialog" aria-hidden="true">
																<div class="modal-dialog modal-lg">
																	<div class="modal-content">
																		
																		<div class="modal-header">
																			<h4 class="modal-title" id="myModalLabel">Tipo</h4>
																			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																			</button>
																		</div>
																		<div class="modal-body">
																			<form action="backend/tools-add-equipamento-assessment-type-backend.php" method="post">
																				<div class="ln_solid"></div>
																				
																				
																				
																				
																				<div class="item form-group">
																					<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo</label>
																					<div class="col-md-6 col-sm-6 ">
																						<input id="nome" class="form-control" type="text" name="nome"  >
																					</div>
																				</div>
																				
																				</div>
																				<div class="modal-footer">
																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																					<button type="submit" class="btn btn-primary" onclick="new PNotify({
																						title: 'Registrado',
																						text: 'Informações registrada!',
																						type: 'success',
																						styling: 'bootstrap3'
																					});" >Salvar Informações</button>
																				</div>
																				
																				</div>
																				</div>
																				</div>
																			</form>
																			<div class="modal fade bs-example-modal-lg25" tabindex="-1" role="dialog" aria-hidden="true">
																				<div class="modal-dialog modal-lg">
																					<div class="modal-content">
																						
																						<div class="modal-header">
																							<h4 class="modal-title" id="myModalLabel">Avaliação</h4>
																							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																							</button>
																						</div>
																						<div class="modal-body">
																							<form action="backend/tools-add-equipamento-assessment-av-backend.php" method="post">
																								<div class="ln_solid"></div>
																								
																								
																								<div class="form-group row">
																									<label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Tipo <span class="required"></span>
																									</label>
																									<div class="input-group col-md-6 col-sm-6">
																										<select type="text" class="form-control has-feedback-left" name="id_equipamento_assessment_type" id="id_equipamento_assessment_type" required="required" >
																											<option value="">Selecione uma Serviço</option>
																											<?php
																												
																												
																												
																												$result_cat_post  = "SELECT  id, nome FROM equipamento_assessment_type";
																												
																												$resultado_cat_post = mysqli_query($conn, $result_cat_post);
																												while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
																													echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
																												}
																											?>
																											
																										</select>
																										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>
																										
																									</div>
																								</div>
																								
																								<div class="item form-group">
																									<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Avaliação</label>
																									<div class="col-md-6 col-sm-6 ">
																										<input id="nome" class="form-control" type="text" name="nome"  >
																									</div>
																								</div>
																								
																								</div>
																								<div class="modal-footer">
																									<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																									<button type="submit" class="btn btn-primary" onclick="new PNotify({
																										title: 'Registrado',
																										text: 'Informações registrada!',
																										type: 'success',
																										styling: 'bootstrap3'
																									});" >Salvar Informações</button>
																								</div>
																								
																								</div>
																								</div>
																								</div>
																							</form>
																							<div class="modal fade bs-example-modal-lg27" tabindex="-1" role="dialog" aria-hidden="true">
																								<div class="modal-dialog modal-lg">
																									<div class="modal-content">
																										
																										<div class="modal-header">
																											<h4 class="modal-title" id="myModalLabel">Fase Alerta Anvisa</h4>
																											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																											</button>
																										</div>
																										<div class="modal-body">
																											<form action="backend/tools-add-alert-phase-backend.php" method="post">
																												<div class="ln_solid"></div>
																												
																												
																												
																												
																												<div class="item form-group">
																													<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Fase</label>
																													<div class="col-md-6 col-sm-6 ">
																														<input id="nome" class="form-control" type="text" name="nome"  >
																													</div>
																												</div>
																												
																												</div>
																												<div class="modal-footer">
																													<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																													<button type="submit" class="btn btn-primary" onclick="new PNotify({
																														title: 'Registrado',
																														text: 'Informações registrada!',
																														type: 'success',
																														styling: 'bootstrap3'
																													});" >Salvar Informações</button>
																												</div>
																												
																												</div>
																												</div>
																												</div>
																											</form>
																											<div class="modal fade bs-example-modal-lg26" tabindex="-1" role="dialog" aria-hidden="true">
																												<div class="modal-dialog modal-lg">
																													<div class="modal-content">
																														
																														<div class="modal-header">
																															<h4 class="modal-title" id="myModalLabel">Motivo</h4>
																															<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																															</button>
																														</div>
																														<div class="modal-body">
																															<form action="backend/tools-add-assessment-backend.php" method="post">
																																<div class="ln_solid"></div>
																																
																																
																																
																																
																																<div class="item form-group">
																																	<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Motivo</label>
																																	<div class="col-md-6 col-sm-6 ">
																																		<input id="nome" class="form-control" type="text" name="nome"  >
																																	</div>
																																</div>
																																
																																</div>
																																<div class="modal-footer">
																																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																	<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																		title: 'Registrado',
																																		text: 'Informações registrada!',
																																		type: 'success',
																																		styling: 'bootstrap3'
																																	});" >Salvar Informações</button>
																																</div>
																																
																																</div>
																																</div>
																																</div>
																															</form>
																															
																															<div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
																																<div class="modal-dialog modal-lg">
																																	<div class="modal-content">
																																		
																																		<div class="modal-header">
																																			<h4 class="modal-title" id="myModalLabel">Serviço</h4>
																																			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																			</button>
																																		</div>
																																		<div class="modal-body">
																																			<form action="backend/tools-add-fornecedor-service-backend.php" method="post">
																																				<div class="ln_solid"></div>
																																				
																																				
																																				
																																				
																																				<div class="item form-group">
																																					<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Serviço</label>
																																					<div class="col-md-6 col-sm-6 ">
																																						<input id="nome" class="form-control" type="text" name="nome"  >
																																					</div>
																																				</div>
																																				
																																				</div>
																																				<div class="modal-footer">
																																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																					<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																						title: 'Registrado',
																																						text: 'Informações registrada!',
																																						type: 'success',
																																						styling: 'bootstrap3'
																																					});" >Salvar Informações</button>
																																				</div>
																																				
																																				</div>
																																				</div>
																																				</div>
																																			</form>
																																			
																																			<div class="modal fade bs-example-modal-lg20" tabindex="-1" role="dialog" aria-hidden="true">
																																				<div class="modal-dialog modal-lg">
																																					<div class="modal-content">
																																						
																																						<div class="modal-header">
																																							<h4 class="modal-title" id="myModalLabel">Obsolecência</h4>
																																							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																							</button>
																																						</div>
																																						<div class="modal-body">
																																							<form action="backend/tools-add-obsolescence-rooi-backend.php" method="post">
																																								<div class="ln_solid"></div>
																																								
																																								
																																								
																																								
																																								<div class="item form-group">
																																									<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">ROOI</label>
																																									<div class="col-md-6 col-sm-6 ">
																																										<input id="nome" class="form-control" type="text" name="nome"  >
																																									</div>
																																								</div>
																																								
																																								</div>
																																								<div class="modal-footer">
																																									<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																									<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																										title: 'Registrado',
																																										text: 'Informações registrada!',
																																										type: 'success',
																																										styling: 'bootstrap3'
																																									});" >Salvar Informações</button>
																																								</div>
																																								
																																								</div>
																																								</div>
																																								</div>
																																							</form>
																																							
																																							<div class="modal fade bs-example-modal-lg19" tabindex="-1" role="dialog" aria-hidden="true">
																																								<div class="modal-dialog modal-lg">
																																									<div class="modal-content">
																																										
																																										<div class="modal-header">
																																											<h4 class="modal-title" id="myModalLabel">Notificação</h4>
																																											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																											</button>
																																										</div>
																																										<div class="modal-body">
																																											<form action="backend/tools-add-os-compliance-backend.php" method="post">
																																												<div class="ln_solid"></div>
																																												
																																												
																																												
																																												
																																												<div class="item form-group">
																																													<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Notificação</label>
																																													<div class="col-md-6 col-sm-6 ">
																																														<input id="nome" class="form-control" type="text" name="nome"  >
																																													</div>
																																												</div>
																																												
																																												</div>
																																												<div class="modal-footer">
																																													<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																													<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																														title: 'Registrado',
																																														text: 'Informações registrada!',
																																														type: 'success',
																																														styling: 'bootstrap3'
																																													});" >Salvar Informações</button>
																																												</div>
																																												
																																												</div>
																																												</div>
																																												</div>
																																											</form>
																																											
																																											<!-- -->
																																											<div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-hidden="true">
																																												<div class="modal-dialog modal-lg">
																																													<div class="modal-content">
																																														
																																														<div class="modal-header">
																																															<h4 class="modal-title" id="myModalLabel">Questionário</h4>
																																															<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																															</button>
																																														</div>
																																														<div class="modal-body">
																																															<form action="backend/tools-add-fornecedor-question-backend.php" method="post">
																																																<div class="ln_solid"></div>
																																																
																																																<div class="form-group row">
																																																	<label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Serviço <span class="required"></span>
																																																	</label>
																																																	<div class="input-group col-md-6 col-sm-6">
																																																		<select type="text" class="form-control has-feedback-left" name="service" id="service" required="required" placeholder="Espécie">
																																																			<option value="">Selecione uma Serviço</option>
																																																			<?php
																																																				
																																																				
																																																				
																																																				$result_cat_post  = "SELECT  id, service FROM fornecedor_service";
																																																				
																																																				$resultado_cat_post = mysqli_query($conn, $result_cat_post);
																																																				while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
																																																					echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['service'].'</option>';
																																																				}
																																																			?>
																																																			
																																																		</select>
																																																		<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>
																																																		
																																																	</div>
																																																</div>
																																																
																																																
																																																
																																																
																																																
																																																<div class="item form-group">
																																																	<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Pergunta</label>
																																																	<div class="col-md-6 col-sm-6 ">
																																																		<input id="question" class="form-control" type="text" name="question"  >
																																																	</div>
																																																</div>
																																																
																																																
																																																
																																																
																																																
																																																
																																																
																																																
																																																</div>
																																																<div class="modal-footer">
																																																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																	<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																		title: 'Registrado',
																																																		text: 'Informações registrada!',
																																																		type: 'success',
																																																		styling: 'bootstrap3'
																																																	});" >Salvar Informações</button>
																																																</div>
																																																
																																																</div>
																																																</div>
																																																</div>
																																															</form>
																																															
																																															
																																															<!-- -->
																																															<!-- -->
																																															
																																															<div class="modal fade bs-example-modal-lg5" tabindex="-1" role="dialog" aria-hidden="true">
																																																<div class="modal-dialog modal-lg">
																																																	<div class="modal-content">
																																																		
																																																		<div class="modal-header">
																																																			<h4 class="modal-title" id="myModalLabel">Movimentação</h4>
																																																			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																			</button>
																																																		</div>
																																																		<div class="modal-body">
																																																			<form action="backend/tools-add-equipamento-mov-backend.php" method="post">
																																																				<div class="ln_solid"></div>
																																																				
																																																				
																																																				
																																																				
																																																				<div class="item form-group">
																																																					<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Movimentação</label>
																																																					<div class="col-md-6 col-sm-6 ">
																																																						<input id="nome" class="form-control" type="text" name="nome"  >
																																																					</div>
																																																				</div>
																																																				
																																																				</div>
																																																				<div class="modal-footer">
																																																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																					<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																						title: 'Registrado',
																																																						text: 'Informações registrada!',
																																																						type: 'success',
																																																						styling: 'bootstrap3'
																																																					});" >Salvar Informações</button>
																																																				</div>
																																																				
																																																				</div>
																																																				</div>
																																																				</div>
																																																			</form>
																																																			
																																																			<!-- -->
																																																			
																																																			<!-- -->
																																																			
																																																			<div class="modal fade bs-example-modal-lg6" tabindex="-1" role="dialog" aria-hidden="true">
																																																				<div class="modal-dialog modal-lg">
																																																					<div class="modal-content">
																																																						
																																																						<div class="modal-header">
																																																							<h4 class="modal-title" id="myModalLabel">Motivo</h4>
																																																							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																							</button>
																																																						</div>
																																																						<div class="modal-body">
																																																							<form action="backend/tools-add-equipamento-reason-backend.php" method="post">
																																																								<div class="ln_solid"></div>
																																																								
																																																								
																																																								
																																																								
																																																								<div class="item form-group">
																																																									<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Motivo</label>
																																																									<div class="col-md-6 col-sm-6 ">
																																																										<input id="nome" class="form-control" type="text" name="nome"  >
																																																									</div>
																																																								</div>
																																																								
																																																								</div>
																																																								<div class="modal-footer">
																																																									<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																									<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																										title: 'Registrado',
																																																										text: 'Informações registrada!',
																																																										type: 'success',
																																																										styling: 'bootstrap3'
																																																									});" >Salvar Informações</button>
																																																								</div>
																																																								
																																																								</div>
																																																								</div>
																																																								</div>
																																																							</form>
																																																							
																																																							<!-- -->
																																																							<!-- -->
																																																							
																																																							<div class="modal fade bs-example-modal-lg7" tabindex="-1" role="dialog" aria-hidden="true">
																																																								<div class="modal-dialog modal-lg">
																																																									<div class="modal-content">
																																																										
																																																										<div class="modal-header">
																																																											<h4 class="modal-title" id="myModalLabel">Status</h4>
																																																											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																											</button>
																																																										</div>
																																																										<div class="modal-body">
																																																											<form action="backend/tools-add-equipamento-status-backend.php" method="post">
																																																												<div class="ln_solid"></div>
																																																												
																																																												
																																																												
																																																												
																																																												<div class="item form-group">
																																																													<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
																																																													<div class="col-md-6 col-sm-6 ">
																																																														<input id="nome" class="form-control" type="text" name="nome"  >
																																																													</div>
																																																												</div>
																																																												
																																																												</div>
																																																												<div class="modal-footer">
																																																													<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																													<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																														title: 'Registrado',
																																																														text: 'Informações registrada!',
																																																														type: 'success',
																																																														styling: 'bootstrap3'
																																																													});" >Salvar Informações</button>
																																																												</div>
																																																												
																																																												</div>
																																																												</div>
																																																												</div>
																																																											</form>
																																																											
																																																											<!-- -->
																																																											<!-- -->
																																																											
																																																											<div class="modal fade bs-example-modal-lg8" tabindex="-1" role="dialog" aria-hidden="true">
																																																												<div class="modal-dialog modal-lg">
																																																													<div class="modal-content">
																																																														
																																																														<div class="modal-header">
																																																															<h4 class="modal-title" id="myModalLabel">Defeito</h4>
																																																															<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																															</button>
																																																														</div>
																																																														<div class="modal-body">
																																																															<form action="backend/tools-add-os-defeito-backend.php" method="post">
																																																																<div class="ln_solid"></div>
																																																																
																																																																
																																																																
																																																																
																																																																<div class="item form-group">
																																																																	<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Defeito</label>
																																																																	<div class="col-md-6 col-sm-6 ">
																																																																		<input id="nome" class="form-control" type="text" name="nome"  >
																																																																	</div>
																																																																</div>
																																																																
																																																																</div>
																																																																<div class="modal-footer">
																																																																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																	<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																		title: 'Registrado',
																																																																		text: 'Informações registrada!',
																																																																		type: 'success',
																																																																		styling: 'bootstrap3'
																																																																	});" >Salvar Informações</button>
																																																																</div>
																																																																
																																																																</div>
																																																																</div>
																																																																</div>
																																																															</form>
																																																															
																																																															<!-- -->
																																																															<!-- -->
																																																															
																																																															<div class="modal fade bs-example-modal-lg9" tabindex="-1" role="dialog" aria-hidden="true">
																																																																<div class="modal-dialog modal-lg">
																																																																	<div class="modal-content">
																																																																		
																																																																		<div class="modal-header">
																																																																			<h4 class="modal-title" id="myModalLabel">Posicionamento</h4>
																																																																			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																			</button>
																																																																		</div>
																																																																		<div class="modal-body">
																																																																			<form action="backend/tools-add-os-posicionamento-backend.php" method="post">
																																																																				<div class="ln_solid"></div>
																																																																				
																																																																				
																																																																				
																																																																				
																																																																				<div class="item form-group">
																																																																					<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Posicionamento</label>
																																																																					<div class="col-md-6 col-sm-6 ">
																																																																						<input id="nome" class="form-control" type="text" name="nome"  >
																																																																					</div>
																																																																				</div>
																																																																				
																																																																				</div>
																																																																				<div class="modal-footer">
																																																																					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																					<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																						title: 'Registrado',
																																																																						text: 'Informações registrada!',
																																																																						type: 'success',
																																																																						styling: 'bootstrap3'
																																																																					});" >Salvar Informações</button>
																																																																				</div>
																																																																				
																																																																				</div>
																																																																				</div>
																																																																				</div>
																																																																			</form>
																																																																			
																																																																			<!-- -->
																																																																			<!-- -->
																																																																			
																																																																			<div class="modal fade bs-example-modal-lg11" tabindex="-1" role="dialog" aria-hidden="true">
																																																																				<div class="modal-dialog modal-lg">
																																																																					<div class="modal-content">
																																																																						
																																																																						<div class="modal-header">
																																																																							<h4 class="modal-title" id="myModalLabel">Avaliação</h4>
																																																																							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																							</button>
																																																																						</div>
																																																																						<div class="modal-body">
																																																																							<form action="backend/tools-add-os-rating-backend.php" method="post">
																																																																								<div class="ln_solid"></div>
																																																																								
																																																																								
																																																																								
																																																																								
																																																																								<div class="item form-group">
																																																																									<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Avaliação</label>
																																																																									<div class="col-md-6 col-sm-6 ">
																																																																										<input id="nome" class="form-control" type="text" name="nome"  >
																																																																									</div>
																																																																								</div>
																																																																								
																																																																								</div>
																																																																								<div class="modal-footer">
																																																																									<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																									<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																										title: 'Registrado',
																																																																										text: 'Informações registrada!',
																																																																										type: 'success',
																																																																										styling: 'bootstrap3'
																																																																									});" >Salvar Informações</button>
																																																																								</div>
																																																																								
																																																																								</div>
																																																																								</div>
																																																																								</div>
																																																																							</form>
																																																																							
																																																																							<!-- -->
																																																																							
																																																																							<div class="modal fade bs-example-modal-lg12" tabindex="-1" role="dialog" aria-hidden="true">
																																																																								<div class="modal-dialog modal-lg">
																																																																									<div class="modal-content">
																																																																										
																																																																										<div class="modal-header">
																																																																											<h4 class="modal-title" id="myModalLabel">Carimbo </h4>
																																																																											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																											</button>
																																																																										</div>
																																																																										<div class="modal-body">
																																																																											
																																																																											<form  class="dropzone" action="backend/tools-carimbo-upload-backend.php" method="post">
																																																																												</form >
																																																																												<div class="ln_solid"></div>
																																																																												<form action="backend/tools-exchange-carimbo-backend.php" method="post">
																																																																													<div class="form-group row">
																																																																														<label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Carimbo <span class="required"></span>
																																																																														</label>
																																																																														<div class="input-group col-md-6 col-sm-6">
																																																																															<select type="text" class="form-control has-feedback-left" name="carimbo" id="carimbo" required="required" placeholder="Espécie">
																																																																																<option value="">Selecione uma Carimbo</option>
																																																																																<?php
																																																																																	
																																																																																	
																																																																																	
																																																																																	$result_cat_post  = "SELECT * FROM os_carimbo";
																																																																																	
																																																																																	$resultado_cat_post = mysqli_query($conn, $result_cat_post);
																																																																																	while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
																																																																																		echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['carimbo'].'</option>';
																																																																																	}
																																																																																?>
																																																																																
																																																																															</select>
																																																																															<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>
																																																																															
																																																																														</div>
																																																																													</div>
																																																																													
																																																																													<div class="ln_solid"></div>
																																																																													
																																																																													
																																																																													
																																																																													
																																																																													
																																																																													
																																																																													
																																																																													</div>
																																																																													<div class="modal-footer">
																																																																														<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																														<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																															title: 'Registrado',
																																																																															text: 'Informações registrada!',
																																																																															type: 'success',
																																																																															styling: 'bootstrap3'
																																																																														});" >Salvar Informações</button>
																																																																													</div>
																																																																													
																																																																													</div>
																																																																													</div>
																																																																													</div>
																																																																												</form>
																																																																												
																																																																												<div class="modal fade bs-example-modal-lg13" tabindex="-1" role="dialog" aria-hidden="true">
																																																																													<div class="modal-dialog modal-lg">
																																																																														<div class="modal-content">
																																																																															
																																																																															<div class="modal-header">
																																																																																<h4 class="modal-title" id="myModalLabel">Baixa </h4>
																																																																																<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																</button>
																																																																															</div>
																																																																															<div class="modal-body">
																																																																																
																																																																																
																																																																																<div class="ln_solid"></div>
																																																																																<form action="backend/equipamento-disable-register-backend.php" method="post">
																																																																																	
																																																																																	
																																																																																	<div class="item form-group">
																																																																																		<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Baixa</label>
																																																																																		<div class="col-md-6 col-sm-6 ">
																																																																																			<input id="nome" class="form-control" type="text" name="nome"  >
																																																																																		</div>
																																																																																	</div>
																																																																																	
																																																																																	
																																																																																	<div class="ln_solid"></div>
																																																																																	
																																																																																	
																																																																																	
																																																																																	
																																																																																	
																																																																																	
																																																																																	
																																																																																	</div>
																																																																																	<div class="modal-footer">
																																																																																		<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																		<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																			title: 'Registrado',
																																																																																			text: 'Informações registrada!',
																																																																																			type: 'success',
																																																																																			styling: 'bootstrap3'
																																																																																		});" >Salvar Informações</button>
																																																																																	</div>
																																																																																	
																																																																																	</div>
																																																																																	</div>
																																																																																	</div>
																																																																																</form>
																																																																																
																																																																																<div class="modal fade bs-example-modal-lg14" tabindex="-1" role="dialog" aria-hidden="true">
																																																																																	<div class="modal-dialog modal-lg">
																																																																																		<div class="modal-content">
																																																																																			
																																																																																			<div class="modal-header">
																																																																																				<h4 class="modal-title" id="myModalLabel">Grupo SLA </h4>
																																																																																				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																				</button>
																																																																																			</div>
																																																																																			<div class="modal-body">
																																																																																				
																																																																																				
																																																																																				<div class="ln_solid"></div>
																																																																																				<form action="backend/grupo-sla-register-backend.php" method="post">
																																																																																					
																																																																																					
																																																																																					<div class="item form-group">
																																																																																						<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grupo</label>
																																																																																						<div class="col-md-6 col-sm-6 ">
																																																																																							<input id="nome" class="form-control" type="text" name="nome"  >
																																																																																						</div>
																																																																																					</div>
																																																																																					
																																																																																					
																																																																																					<div class="ln_solid"></div>
																																																																																					
																																																																																					
																																																																																					
																																																																																					
																																																																																					
																																																																																					
																																																																																					
																																																																																					</div>
																																																																																					<div class="modal-footer">
																																																																																						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																						<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																							title: 'Registrado',
																																																																																							text: 'Informações registrada!',
																																																																																							type: 'success',
																																																																																							styling: 'bootstrap3'
																																																																																						});" >Salvar Informações</button>
																																																																																					</div>
																																																																																					
																																																																																					</div>
																																																																																					</div>
																																																																																					</div>
																																																																																				</form>
																																																																																				
																																																																																				<div class="modal fade bs-example-modal-lg15" tabindex="-1" role="dialog" aria-hidden="true">
																																																																																					<div class="modal-dialog modal-lg">
																																																																																						<div class="modal-content">
																																																																																							
																																																																																							<div class="modal-header">
																																																																																								<h4 class="modal-title" id="myModalLabel"> SLA </h4>
																																																																																								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																								</button>
																																																																																							</div>
																																																																																							<div class="modal-body">
																																																																																								
																																																																																								
																																																																																								<div class="ln_solid"></div>
																																																																																								<form action="backend/sla-register-backend.php" method="post">
																																																																																									
																																																																																									
																																																																																									<div class="item form-group">
																																																																																										<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">1º SLA</label>
																																																																																										<div class="col-md-6 col-sm-6 ">
																																																																																											<input id="sla_primeiro" class="form-control" type="number" name="sla_primeiro"  >
																																																																																										</div>
																																																																																									</div>
																																																																																									
																																																																																									<div class="item form-group">
																																																																																										<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">SLA Verde</label>
																																																																																										<div class="col-md-6 col-sm-6 ">
																																																																																											<input id="sla_verde" class="form-control" type="number" name="sla_verde"  >
																																																																																										</div>
																																																																																									</div>
																																																																																									
																																																																																									<div class="item form-group">
																																																																																										<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">SLA Amarelo</label>
																																																																																										<div class="col-md-6 col-sm-6 ">
																																																																																											<input id="sla_amarelo" class="form-control" type="number" name="sla_amarelo"  >
																																																																																										</div>
																																																																																									</div>
																																																																																									
																																																																																									
																																																																																									<div class="ln_solid"></div>
																																																																																									
																																																																																									
																																																																																									<div class="item form-group">
																																																																																										<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Prioridade</label>
																																																																																										<div class="col-md-6 col-sm-6 ">
																																																																																											<select class="form-control " name="os_prioridade" id="os_prioridade"  type="text" >
																																																																																												<?php $query="SELECT * FROM os_prioridade ";
																																																																																													$row=1;
																																																																																													if ($stmt = $conn->prepare($query)) {
																																																																																														$stmt->execute();
																																																																																														$stmt->bind_result($id, $prioridade,$emergencial);
																																																																																														while ($stmt->fetch()) {
																																																																																												?>
																																																																																												
																																																																																												<option value="<?php printf($id); ?>" <?php if($os_prioridade == $id){ printf("selected");}?>><?php printf($prioridade); ?></option>
																																																																																												<?php                     }
																																																																																													}
																																																																																												?>
																																																																																												
																																																																																											</select>
																																																																																										</div>
																																																																																									</div>
																																																																																									
																																																																																									<div class="item form-group">
																																																																																										<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grupo SLA</label>
																																																																																										<div class="col-md-6 col-sm-6 ">
																																																																																											<select class="form-control " name="os_grupo_sla" id="os_grupo_sla"  type="text" >
																																																																																												<?php $query="SELECT * FROM os_grupo_sla ";
																																																																																													$row=1;
																																																																																													if ($stmt = $conn->prepare($query)) {
																																																																																														$stmt->execute();
																																																																																														$stmt->bind_result($id, $grupo);
																																																																																														while ($stmt->fetch()) {
																																																																																												?>
																																																																																												
																																																																																												<option value="<?php printf($id); ?>" <?php if($os_grupo_sla == $id){ printf("selected");}?>><?php printf($grupo); ?></option>
																																																																																												<?php                     }
																																																																																													}
																																																																																												?>
																																																																																												
																																																																																											</select>            </div>
																																																																																									</div>
																																																																																									
																																																																																									
																																																																																									<div class="ln_solid"></div>
																																																																																									
																																																																																									
																																																																																									
																																																																																									
																																																																																									
																																																																																									
																																																																																									
																																																																																									</div>
																																																																																									<div class="modal-footer">
																																																																																										<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																										<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																											title: 'Registrado',
																																																																																											text: 'Informações registrada!',
																																																																																											type: 'success',
																																																																																											styling: 'bootstrap3'
																																																																																										});" >Salvar Informações</button>
																																																																																									</div>
																																																																																									
																																																																																									</div>
																																																																																									</div>
																																																																																									</div>
																																																																																								</form>
																																																																																								
																																																																																								<div class="modal fade bs-example-modal-lg16" tabindex="-1" role="dialog" aria-hidden="true">
																																																																																									<div class="modal-dialog modal-lg">
																																																																																										<div class="modal-content">
																																																																																											
																																																																																											<div class="modal-header">
																																																																																												<h4 class="modal-title" id="myModalLabel">Prioridade </h4>
																																																																																												<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																												</button>
																																																																																											</div>
																																																																																											<div class="modal-body">
																																																																																												
																																																																																												
																																																																																												<div class="ln_solid"></div>
																																																																																												<form action="backend/os-prioridade-register-backend.php" method="post">
																																																																																													
																																																																																													
																																																																																													<div class="item form-group">
																																																																																														<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Prioridade</label>
																																																																																														<div class="col-md-6 col-sm-6 ">
																																																																																															<input id="prioridade" class="form-control" type="text" name="prioridade"  >
																																																																																														</div>
																																																																																													</div>
																																																																																													
																																																																																													<div class="item form-group">
																																																																																														<label class="col-form-label col-md-3 col-sm-3 label-align">Emergencial</label>
																																																																																														<div class="col-md-6 col-sm-6 ">
																																																																																															<div class="">
																																																																																																<label>
																																																																																																	<input name="emergencial"type="checkbox" class="js-switch"  value="0"  />
																																																																																																</label>
																																																																																															</div>
																																																																																														</div>
																																																																																													</div>
																																																																																													<div class="ln_solid"></div>
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													
																																																																																													</div>
																																																																																													<div class="modal-footer">
																																																																																														<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																														<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																															title: 'Registrado',
																																																																																															text: 'Informações registrada!',
																																																																																															type: 'success',
																																																																																															styling: 'bootstrap3'
																																																																																														});" >Salvar Informações</button>
																																																																																													</div>
																																																																																													
																																																																																													</div>
																																																																																													</div>
																																																																																													</div>
																																																																																												</form>
																																																																																												
																																																																																												
																																																																																												<div class="modal fade bs-example-modal-lg17" tabindex="-1" role="dialog" aria-hidden="true">
																																																																																													<div class="modal-dialog modal-lg">
																																																																																														<div class="modal-content">
																																																																																															
																																																																																															<div class="modal-header">
																																																																																																<h4 class="modal-title" id="myModalLabel">Workflow </h4>
																																																																																																<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																																</button>
																																																																																															</div>
																																																																																															<div class="modal-body">
																																																																																																
																																																																																																
																																																																																																<div class="ln_solid"></div>
																																																																																																<form action="backend/os-workflow-register-backend.php" method="post">
																																																																																																	
																																																																																																	
																																																																																																	<div class="item form-group">
																																																																																																		<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Workflow</label>
																																																																																																		<div class="col-md-6 col-sm-6 ">
																																																																																																			<input id="workflow" class="form-control" type="text" name="workflow"  >
																																																																																																		</div>
																																																																																																	</div>
																																																																																																	
																																																																																																	<div class="item form-group">
																																																																																																		<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Prioridade</label>
																																																																																																		<div class="col-md-6 col-sm-6 ">
																																																																																																			<select class="form-control " name="os_prioridade" id="os_prioridade"  type="text" >
																																																																																																				<?php $query="SELECT * FROM os_prioridade ";
																																																																																																					$row=1;
																																																																																																					if ($stmt = $conn->prepare($query)) {
																																																																																																						$stmt->execute();
																																																																																																						$stmt->bind_result($id, $prioridade,$emergencial);
																																																																																																						while ($stmt->fetch()) {
																																																																																																				?>
																																																																																																				
																																																																																																				<option value="<?php printf($id); ?>" <?php if($id_os_prioridade == $id){ printf("selected");}?>><?php printf($prioridade); ?></option>
																																																																																																				<?php                     }
																																																																																																					}
																																																																																																				?>
																																																																																																				
																																																																																																			</select>            </div>
																																																																																																	</div>
																																																																																																	
																																																																																																	<div class="item form-group">
																																																																																																		<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
																																																																																																		<div class="col-md-6 col-sm-6 ">
																																																																																																			<select class="form-control " name="os_status" id="os_status"  type="text" >
																																																																																																				<?php $query="SELECT * FROM os_status ";
																																																																																																					$row=1;
																																																																																																					if ($stmt = $conn->prepare($query)) {
																																																																																																						$stmt->execute();
																																																																																																						$stmt->bind_result($id, $status,$cor);
																																																																																																						while ($stmt->fetch()) {
																																																																																																				?>
																																																																																																				
																																																																																																				<option value="<?php printf($id); ?>" <?php if($id_os_status == $id){ printf("selected");}?>><?php printf($status); ?></option>
																																																																																																				<?php                     }
																																																																																																					}
																																																																																																				?>
																																																																																																				
																																																																																																			</select>            </div>
																																																																																																	</div>
																																																																																																	
																																																																																																	<div class="item form-group">
																																																																																																		<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Poscionamento</label>
																																																																																																		<div class="col-md-6 col-sm-6 ">
																																																																																																			<select class="form-control " name="os_posicionamento" id="os_posicionamento"  type="text" >
																																																																																																				<?php $query="SELECT * FROM os_posicionamento ";
																																																																																																					$row=1;
																																																																																																					if ($stmt = $conn->prepare($query)) {
																																																																																																						$stmt->execute();
																																																																																																						$stmt->bind_result($id, $status,$cor);
																																																																																																						while ($stmt->fetch()) {
																																																																																																				?>
																																																																																																				
																																																																																																				<option value="<?php printf($id); ?>" <?php if($id_os_posicionamento == $id){ printf("selected");}?>><?php printf($status); ?></option>
																																																																																																				<?php                     }
																																																																																																					}
																																																																																																				?>
																																																																																																				
																																																																																																			</select>            </div>
																																																																																																	</div>
																																																																																																	
																																																																																																	
																																																																																																	
																																																																																																	
																																																																																																	<div class="ln_solid"></div>
																																																																																																	
																																																																																																	
																																																																																																	
																																																																																																	
																																																																																																	
																																																																																																	
																																																																																																	
																																																																																																	</div>
																																																																																																	<div class="modal-footer">
																																																																																																		<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																																		<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																																			title: 'Registrado',
																																																																																																			text: 'Informações registrada!',
																																																																																																			type: 'success',
																																																																																																			styling: 'bootstrap3'
																																																																																																		});" >Salvar Informações</button>
																																																																																																	</div>
																																																																																																	
																																																																																																	</div>
																																																																																																	</div>
																																																																																																	</div>
																																																																																																</form>
																																																																																																
																																																																																																<!-- -->
																																																																																																<div class="modal fade bs-example-modal-lg18" tabindex="-1" role="dialog" aria-hidden="true">
																																																																																																	<div class="modal-dialog modal-lg">
																																																																																																		<div class="modal-content">
																																																																																																			
																																																																																																			<div class="modal-header">
																																																																																																				<h4 class="modal-title" id="myModalLabel">Serviço </h4>
																																																																																																				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																																				</button>
																																																																																																			</div>
																																																																																																			<div class="modal-body">
																																																																																																				
																																																																																																				
																																																																																																				<div class="ln_solid"></div>
																																																																																																				<form action="backend/os-custom-service-register-backend.php" method="post">
																																																																																																					
																																																																																																					
																																																																																																					<div class="item form-group">
																																																																																																						<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
																																																																																																						<div class="col-md-6 col-sm-6 ">
																																																																																																							<input id="nome" class="form-control" type="text" name="nome"  >
																																																																																																						</div>
																																																																																																					</div>
																																																																																																					
																																																																																																					
																																																																																																					
																																																																																																					
																																																																																																					
																																																																																																					
																																																																																																					</div>
																																																																																																					<div class="modal-footer">
																																																																																																						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																																						<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																																							title: 'Registrado',
																																																																																																							text: 'Informações registrada!',
																																																																																																							type: 'success',
																																																																																																							styling: 'bootstrap3'
																																																																																																						});" >Salvar Informações</button>
																																																																																																					</div>
																																																																																																					
																																																																																																					</div>
																																																																																																					</div>
																																																																																																					</div>
																																																																																																				</form>
																																																																																																				<div class="modal fade bs-example-modal-lg22" tabindex="-1" role="dialog" aria-hidden="true">
																																																																																																					<div class="modal-dialog modal-lg">
																																																																																																						<div class="modal-content">
																																																																																																							
																																																																																																							<div class="modal-header">
																																																																																																								<h4 class="modal-title" id="myModalLabel">Modo Instalação</h4>
																																																																																																								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																																								</button>
																																																																																																							</div>
																																																																																																							<div class="modal-body">
																																																																																																								<form action="backend/tools-equipamento-check-mod-backend.php" method="post">
																																																																																																									<div class="ln_solid"></div>
																																																																																																									
																																																																																																									
																																																																																																									
																																																																																																									
																																																																																																									<div class="item form-group">
																																																																																																										<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
																																																																																																										<div class="col-md-6 col-sm-6 ">
																																																																																																											<input id="nome" class="form-control" type="text" name="nome"  >
																																																																																																										</div>
																																																																																																									</div>
																																																																																																									
																																																																																																									</div>
																																																																																																									<div class="modal-footer">
																																																																																																										<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																																										<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																																											title: 'Registrado',
																																																																																																											text: 'Informações registrada!',
																																																																																																											type: 'success',
																																																																																																											styling: 'bootstrap3'
																																																																																																										});" >Salvar Informações</button>
																																																																																																									</div>
																																																																																																									
																																																																																																									</div>
																																																																																																									</div>
																																																																																																									</div>
																																																																																																								</form>
																																																																																																								<div class="modal fade bs-example-modal-lg23" tabindex="-1" role="dialog" aria-hidden="true">
																																																																																																									<div class="modal-dialog modal-lg">
																																																																																																										<div class="modal-content">
																																																																																																											
																																																																																																											<div class="modal-header">
																																																																																																												<h4 class="modal-title" id="myModalLabel">Check List</h4>
																																																																																																												<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
																																																																																																												</button>
																																																																																																											</div>
																																																																																																											<div class="modal-body">
																																																																																																												<form action="backend/tools-equipamento-check-list-backend.php" method="post">
																																																																																																													<div class="ln_solid"></div>
																																																																																																													
																																																																																																													
																																																																																																													
																																																																																																													
																																																																																																													<div class="item form-group">
																																																																																																														<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
																																																																																																														<div class="col-md-6 col-sm-6 ">
																																																																																																															<input id="nome" class="form-control" type="text" name="nome"  >
																																																																																																														</div>
																																																																																																													</div>
																																																																																																													
																																																																																																													</div>
																																																																																																													<div class="modal-footer">
																																																																																																														<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
																																																																																																														<button type="submit" class="btn btn-primary" onclick="new PNotify({
																																																																																																															title: 'Registrado',
																																																																																																															text: 'Informações registrada!',
																																																																																																															type: 'success',
																																																																																																															styling: 'bootstrap3'
																																																																																																														});" >Salvar Informações</button>
																																																																																																													</div>
																																																																																																													
																																																																																																													</div>
																																																																																																													</div>
																																																																																																													</div>
																																																																																																												</form>
																																																																																																												<!-- -->
																																																																																																												
																																																																																																												
																																																																																																												
																																																																																																												
																																																																																																												<!-- -->
																																																																																																												
																																																																																																												
																																																																																																												
																																																																																																												
																																																																																																												<!-- /page content -->
