

<div class="right_col" role="main">
  
  <div class="">
    
    <div class="page-title">
      <div class="title_left">
        <h3>Sensor</h3>
      </div>
      
      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">
            
            <span class="input-group-btn">
              
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    
    
    
    
    <div class="clearfix"></div>     
    <div class="row">
      <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Filtro</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a class="dropdown-item" href="#">Settings 1</a>
                  </li>
                  <li><a class="dropdown-item" href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
            
            
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta<small>Unidade</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                                   
                
                    
                    <div class="col-md-11 col-sm-11 ">
                      <div class="x_panel">
                        
                        <div class="x_content">
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="card-box table-responsive">
                                
                                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Macadress</th>
                                      <th>Sensor</th>
                                      <th>Nome</th>
                                      <th>Unidade</th>
                                      <th>Setor</th>
                                      <th>Área</th>
                                      <th>Status</th>
                                      <th>Up Time</th>
                                      <th>File</th>
                                      <th>Registro</th>
                                      <th>Atualização</th>
                                      <th>Bateria</th>
                                      <th>Ultimo Alarme</th>
                                      <th>Lixeira</th>
                                      <th>Registro Alarme</th>
                                      <th>IMEI</th>
                                      <th>Firmware</th>
                                      <th>Linha</th>
                                      <th>ICCID</th>
                                      <th>Localização</th>
                                      <th>USB</th>
                                      <th>Energia</th>
                                      <th>Ativo</th>
                                      <th>Dados</th>
                                      <th>Registro Dados</th>

                                      <th>Ação</th>
                                      
                                    </tr>
                                  </thead>
                                  
                                  
                                  <tbody>
                                    
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
              
                    


                  </div>
                </div>
              </div>
	       </div>
	       
  </div>
</div>

                  
                  <script language="JavaScript">
                    $(document).ready(function() {
                      
                      $('#datatable').dataTable( {
                        "processing": true,
                        "stateSave": true,
                        "responsive": true,
                        
                        
                        
                        
                        
                        "language": {
                          "loadingRecords": "Carregando dados...",
                          "processing": "Processando  dados...",
                          "infoEmpty": "Nenhum dado a mostrar",
                          "emptyTable": "Sem dados disponíveis na tabela",
                          "zeroRecords": "Não há registros a serem exibidos",
                          "search": "Filtrar registros:",
                          "info": "Mostrando página _PAGE_ de _PAGES_",
                          "infoFiltered": " - filtragem de _MAX_ registros",
                          "lengthMenu": "Mostrar _MENU_ registros",
                          
                          "paginate": {
                            "previous": "Página anterior",
                            "next": "Próxima página",
                            "last": "Última página",
                            "first": "Primeira página",
                            
                            
                            
                          }
                        }
                        
                        
                        
                      } );
                      
                      
                      // Define a URL da API que retorna os dados da query em formato JSON
                      const apiUrl = 'table/table-search-device-iot.php';
                      
                      
                      
                      // Usa a função fetch() para obter os dados da API em formato JSON
                      fetch(apiUrl)
                      .then(response => response.json())
                      .then(data => {
                        // Mapeia os dados para o formato esperado pelo DataTables
                        const novosDados = data.map(item => [
                          ``,
                           item.macadress,
                            item.id_sensor_type,
                            item.nome,
                            item.unidade,
                            item.setor,
                            item.area,
                            item.id_status,
                            item.up_time,
                            item.file,
                            item.reg_date,
                            item.upgrade,
                            item.bat, 
                            item.last_alarm,
                            item.trash == "0" ? "Sim" : "Não",
                            item.id_alarm, 
                            item.imei, 
                            item.firmware,
                            item.linha, 
                            item.iccid,
                            item.id_location,
                            item.usb, 
                            item.charger,
                            item.ativo == "0" ? "Sim" : "Não",
                            item.dados, 
                            item.id_dados, 
                          
                          `<a class="btn btn-app" href="device-iot-edit?id=${item.id}" onclick=" new PNotify({
          title: 'Editar',
          text: 'Abrindo Edição',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-edit"></i> Editar
        </a>
<a class="btn btn-app" href="device-iot-telemetria?id=${item.id}" onclick=" new PNotify({
          title: 'Telemetria',
          text: 'Abrindo Telemetria',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-desktop"></i> Telemetria
        </a>
<a class="btn btn-app" href="device-iot-alarm?id=${item.id}" onclick=" new PNotify({
          title: 'Alarme',
          text: 'Abrindo Alarme',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-bell-o"></i> Parâmetro Alarme
        </a>
<a class="btn btn-app" href="device-iot-command?id=${item.id}" onclick=" new PNotify({
          title: 'Telecomando',
          text: 'Abrindo Telecomando',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-cogs"></i> Telecomando
        </a>
<a class="btn btn-app" href="device-iot-historic?id=${item.id}" onclick=" new PNotify({
          title: 'Historico',
          text: 'Abrindo Historico',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-database"></i> Historico Dados
        </a>
<a class="btn btn-app" href="device-iot-location?id=${item.id}" onclick=" new PNotify({
          title: 'Localização',
          text: 'Abrindo Localização',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-map-marker"></i> Historico Localização
        </a>
<a class="btn btn-app" href="device-iot-alert?id=${item.id}" onclick=" new PNotify({
          title: 'Historico Alertas',
          text: 'Abrindo Historico Alertas',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-exclamation"></i> Historico Alertas
        </a>
<a class="btn btn-app" href="device-iot-info?id=${item.id}" onclick=" new PNotify({
          title: 'Informações',
          text: 'Abrindo Informações',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-info"></i> Informações
        </a>

<a class="btn btn-app" href="device-iot-file?id=${item.id}" onclick=" new PNotify({
          title: 'PDF',
          text: 'Abrindo PDF',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-file-o"></i> PDF
        </a>
        <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Deletar!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Deletando!',
              'Seu arquivo será excluído.',
              'success'
            ),
            window.location = 'backend/device-iot-trash-backend?id=${item.id}';
          }
        });">
          <i class="fa fa-trash"></i> Excluir
        </a>`
                        ]);
                        
                        // Inicializa o DataTables com os novos dados
                        const table = $('#datatable').DataTable();
                        table.clear().rows.add(novosDados).draw();
                        
                        
                        // Cria os filtros após a tabela ser populada
                        $('#datatable').DataTable().columns([1, 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]).every(function(d) {
                          var column = this;
                          var theadname = $("#datatable th").eq([d]).text();
                          var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
                          var label = $('<label>' + theadname + ': </label>').appendTo(container);
                          var select = $('<select  class="form-control my-1"><option value="">' +
                            theadname + '</option></select>').appendTo('#userstable_filter').select2()
                          .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                          });
                          
                          column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                          });
                        });
                        
                        var oTable = $('#datatable').dataTable();
                        
                        // Avança para a próxima página da tabela
                        //oTable.fnPageChange('next');
                        
                        
                        
                      });
                      
                      
                      // Armazenar a posição atual ao sair da página
                      $(window).on('beforeunload', function() {
                        var table = $('#datatable').DataTable();
                        var pageInfo = table.page.info();
                        localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
                      });
                      
                      // Restaurar a posição ao retornar à página
                    
                      
                      
                      
                      
                    });
                    
                    $(document).ready(function() {
                      var storedPosition = localStorage.getItem('paginationPosition');
                      if (storedPosition) {
                        var pageInfo = JSON.parse(storedPosition);
                        var table = $('#datatable').DataTable();
                        table.page(pageInfo.page).draw('page');
                      }
                    });
                    
                  </script>
                  
                  
	        
                  
                  