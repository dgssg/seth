<?php
include("../database/database.php");
$query = "SELECT area.id, unidade.unidade,setor.custo, setor.nome AS 'setor',area.codigo,area.andar,area.nome,area.observacao,area.upgrade,area.reg_date FROM area INNER JOIN unidade ON unidade.id =   area.id_unidade INNER JOIN setor on setor.id = area.id_setor WHERE area.trash = 1 ORDER BY area.id  DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
