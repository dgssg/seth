<!-- page content -->

<?php
	$sweet_salve = ($_GET["sweet_salve"]);
	$sweet_delet = ($_GET["sweet_delet"]);
	if ($sweet_delet == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
		Swal.fire({
				position: \'top-end\',
				icon: \'success\',
				title: \'Seu trabalho foi deletado!\',
				showConfirmButton: false,
				timer: 1500
		});
</script>';
		
		
	}
	if ($sweet_salve == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
				Swal.fire({
						position: \'top-end\',
						icon: \'success\',
						title: \'Seu trabalho foi salvo!\',
						showConfirmButton: false,
						timer: 1500
				});
		</script>';
		
		
	}
?>
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<!-- Switchery -->
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<div class="right_col" role="main">
	
	<div class="">
		
		<div class="page-title">
			<div class="title_left">
				<h3>Registro</h3>
			</div>
			
			<div class="title_right">
				<div class="col-md-5 col-sm-5  form-group pull-right top_search">
					<div class="input-group">
						
						<span class="input-group-btn">
							
						</span>
					</div>
				</div>
			</div>
		</div>
		
		
		 		
		
		<!-- page content -->
		
		
		
		
		
		
		
		
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Cadastro <small>do Usuario</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a class="dropdown-item" href="#">Settings 1</a>
									</li>
									<li><a class="dropdown-item" href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br />
						<form action="backend/register-account-new-backend.php" method="post">
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
								<div class="col-md-6 col-sm-6 ">
									<input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($nome); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Sobrenome</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="sobrenome" class="form-control" type="text" name="sobrenome"  value="<?php printf($sobrenome); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Matricula</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="matricula" class="form-control" type="text" name="matricula"  value="<?php printf($matricula); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Email </label>
								<div class="col-md-6 col-sm-6 ">
									<input id="email" class="form-control" type="text" name="email"  value="<?php printf($email); ?> " >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Login </label>
								<div class="col-md-6 col-sm-6 ">
									<input id="login_acess" class="form-control" type="text" name="login_acess"  value="<?php printf($login_acess); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Telefone</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="telefone" class="form-control" type="text" name="telefone"  value="<?php printf($telefone); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cargo</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="cargo" class="form-control" type="text" name="cargo"  value="<?php printf($cargo); ?> " >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Perfil</label>
								<div class="col-md-6 col-sm-6 ">
									<select class="form-control has-feedback-left" name="id_nivel" value= " <?php printf($id_nivel); ?>" >
										<option>Selecione Perfil</option>
										<option value="1" <?php if($id_nivel == 1){ printf("selected");}?>  > Administrador</option>
										<option value="1" <?php if($id_nivel == 2){ printf("selected");}?>  > Usuario</option>
										
										
										
									</select>
								</div>
							</div>
							<script>
								$(document).ready(function() {
									$('#id_nivel').select2();
									
								});
								
							</script>

							
							<div class="item form-group">
								<label class="col-form-label col-md-3 col-sm-3 label-align">Aplicativo</label>
								<div class="col-md-6 col-sm-6 ">
									<div class="">
										<label>
											<input name="api_app"type="checkbox" class="js-switch" <?php if($api_app== "0"){printf("checked"); }?> />
										</label>
									</div>
								</div>
							</div>
						 
						 
						 
							
						 							
							<div class="form-group row">
								<label class="col-form-label col-md-3 col-sm-3 "></label>
								<div class="col-md-3 col-sm-3 ">
									<center>
										<button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
									</center>
								</div>
							</div>
							
							
						</form>
						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Ação</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<a class="btn btn-app"  href="register-account">
					<i class="glyphicon glyphicon-arrow-left"></i> Voltar
				</a>
				<a class="btn btn-app"  href="register-account-send?id=<?php printf($codigoget); ?>&nome=<?php printf($nome); ?>&sobrenome=<?php printf($sobrenome); ?>&email=<?php printf($email); ?>">
					<i class="fa fa-envelope-o"></i> Enviar Acesso
				</a>
				
				
				
			</div>
		</div>
		
		
		
		
	</div>
</div>
<!-- /page content -->

<!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
