<?php
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }
?>


        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Lixeira</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lixeira <small>Check</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                <div class="col-xs-3">
                      <!-- required for floating -->
                      <!-- Nav tabs -->
                      <div class="nav nav-tabs justify-content-end bar_tabs" id="v-pills-tab" role="tablist" >
                        <a class="nav-link active" id="v-pills-unidade-tab" data-toggle="pill" href="#v-pills-unidade" role="tab" aria-controls="v-pills-unidade" aria-selected="true">Unidade</a>
                        <a class="nav-link" id="v-pills-setor-tab" data-toggle="pill" href="#v-pills-setor" role="tab" aria-controls="v-pills-setor" aria-selected="false">Setor</a>
                        <a class="nav-link" id="v-pills-area-tab" data-toggle="pill" href="#v-pills-area" role="tab" aria-controls="v-pills-area" aria-selected="false">Area</a>
                                       
                      <!--    <a class="nav-link" id="v-pills-rotina-tab" data-toggle="pill" href="#v-pills-rotina" role="tab" aria-controls="v-pills-rotina" aria-selected="false">Rotina</a> -->
                      </div>

                    </div>

                    <div class="col-xs-9">
                      <!-- Tab panes -->

                      <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-unidade" role="tabpanel" aria-labelledby="v-pills-unidade-tab">  <!-- UNIDADE -->
                      <?php
$query = "SELECT id, unidade, codigo, endereco, site, upgrade, reg_date FROM unidade where trash = 0";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $instituicao,$codigo,$endereco,$site,$update,$reg_date);


?>

<div >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">

                    <table  class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Codigo</th>
                          <th>Unidade</th>
                          <th>Endereço</th>
                          <th>Site</th>
                          <th>Update</th>
                          <th>Cadastro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                           <td><?php printf($codigo); ?> </td>
                          <td><?php printf($instituicao); ?> </td>
                          <td><?php printf($endereco); ?></td>
                            <td><?php printf($site); ?></td>
                              <td><?php printf($update); ?></td>
                          <td><?php printf($reg_date); ?></td>

                          <td>


               <a class="btn btn-app"  href="api-recycle-unidade-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-unidade-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

              <!-- UNIDADE -->

                        </div>

                        <div class="tab-pane fade" id="v-pills-setor" role="tabpanel" aria-labelledby="v-pills-setor-tab">
               <!-- SETOR -->
                 <?php

$query = "SELECT setor.id,setor.id_unidade,setor.custo,setor.nome,setor.upgrade,setor.reg_date, unidade.id, unidade.unidade FROM setor INNER JOIN unidade ON setor.id_unidade = unidade.id where setor.trash = 0";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $id_unidade,$custo,$nome,$upgrade,$reg_date,$id2,$instituicao);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div  >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">

                    <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Unidade</th>
                          <th>Centro de Custo</th>
                          <th>Nome</th>
                          <th>Update</th>
                          <th>Cadastro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($instituicao); ?> </td>
                          <td><?php printf($custo); ?></td>
                          <td><?php printf($nome); ?></td>
                          <td><?php printf($upgrade); ?></td>
                          <td><?php printf($reg_date); ?></td>

                          <td>


              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                      <a class="btn btn-app"  href="api-recycle-setor-backend?id=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-setor-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
                 <!-- SETOR -->
                        </div>

                        <div class="tab-pane fade" id="v-pills-area" role="tabpanel" aria-labelledby="v-pills-area-tab">
                         <!-- AREA -->

                        <?php

$query = "SELECT area.id, unidade.unidade,setor.custo, setor.nome,area.codigo,area.andar,area.nome,area.observacao,area.upgrade,area.reg_date FROM area LEFT JOIN setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade where area.trash = 0 ORDER BY area.id";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $instituicao,$custo,$nome_area,$codigo_localizacao,$andar,$nome_localizacao,$observacao,$upgrade,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div  >
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div  >
                            <div class="card-box table-responsive">

                    <table   class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Unidade</th>

                          <th>Setor</th>
                          <th>Area</th>

                          <th>Update</th>
                          <th>Cadastro</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($instituicao); ?></td>
                            <td><?php printf($nome_area); ?></td>
                            <td><?php printf($nome_localizacao); ?> </td>

                              <td><?php printf($upgrade); ?></td>
                          <td><?php printf($reg_date); ?></td>

                          <td>



              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                   <a class="btn btn-app"  href="api-recycle-area-backend?id=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Restaurar',
																text: 'Restaurarando Informação',
																type: 'info',
																styling: 'bootstrap3'
														});">
                  <i class="fa fa-recycle"></i> Restaurar</a>
                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/api-recycle-area-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
                         <!-- AREA -->
                         

                         




                      </div>
                    </div>
                    <!-- end -->
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
 
