<?php
	$sweet_salve = ($_GET["sweet_salve"]);
	$sweet_delet = ($_GET["sweet_delet"]);
	if ($sweet_delet == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
		Swal.fire({
				position: \'top-end\',
				icon: \'success\',
				title: \'Seu trabalho foi deletado!\',
				showConfirmButton: false,
				timer: 1500
		});
</script>';
		
		
	}
	if ($sweet_salve == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
				Swal.fire({
						position: \'top-end\',
						icon: \'success\',
						title: \'Seu trabalho foi salvo!\',
						showConfirmButton: false,
						timer: 1500
				});
		</script>';
		
		
	}
?>
<?php
	
	include("database/database.php");
	
	
	//$con->close();
	
	
	
?>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Parametrização <small>Tabelas do sistema</small></h3>
			</div>
			
			
		</div>
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Alerta</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<div class="alert alert-success alert-dismissible " role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<strong>Area de Edição!</strong> Após realizar uma ação utlize o botão [<i class="fa fa-refresh"></i> ] ao lado,
					para atualizar informações alteradas.
				</div>
				
				
				
			</div>
		</div>
		
		
		<div class="clearfix"></div>
	</div>
	
	<div class="row" style="display: block;">
		<div class="col-md-12 col-sm-12  ">
			<div class="x_panel">
				<div class="x_title">
					<h2>Automação <small>Configuração</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Settings 1</a>
								<a class="dropdown-item" href="#">Settings 2</a>
							</div>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<form action="backend/update-tools-backend.php" method="post">
						 
						
						
						
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
							<div class="col-md-6 col-sm-6 ">
								<input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
							</div>
						</div>
						
						<div class="form-group row">
							<label class="col-form-label col-md-3 col-sm-3 "></label>
							<div class="col-md-3 col-sm-3 ">
								<center>
									<button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
								</center>
							</div>
						</div>
						
						
					</form>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Logo Documentação<small> </small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="#">Settings 1</a>
								<a class="dropdown-item" href="#">Settings 2</a>
							</div>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<div class="row">
						<div class="col-md-55">
							
							<div class="thumbnail">
								<div class="image view view-first">
									<img style="width: 100%; display: block;" src="logo/clientelogo.png" alt="image" />
									<div class="mask">
										<p>Logo Documentação</p>
										<div class="tools tools-bottom">
											<a href="logo/clientelogo.png" download><i class="fa fa-download"></i></a>
											
										</div>
									</div>
								</div>
								<div class="caption">
									<p>Logo</p>
								</div>
							</div>
							
						</div>
						
						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="x_panel">
		<div class="x_title">
			<h2>Trocar Logo Documentação</h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a>
						</li>
						<li><a href="#">Settings 2</a>
						</li>
					</ul>
				</li>
				<li><a class="close-link"><i class="fa fa-close"></i></a>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="alert alert-danger alert-dismissible " role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<strong>Atenção!</strong> Utilizar arquivos com a seguinte extensão ".png".
			</div>
			
			<form  class="dropzone" action="backend/tools-logo-upload-backend.php" method="post">
				</form >
				
				
				
				</div>
				</div>
				
				
				<div class="row">
					<div class="col-md-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Logo Sistema<small> </small></h2>
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="#">Settings 1</a>
											<a class="dropdown-item" href="#">Settings 2</a>
										</div>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								
								<div class="row">
									<div class="col-md-55">
										
										<div class="thumbnail">
											<div class="image view view-first">
												<img style="width: 100%; display: block;" src="logo/clientelogo.png" alt="image" />
												<div class="mask">
													<p>Logo Sistema</p>
													<div class="tools tools-bottom">
														<a href="logo/img.jpg" download><i class="fa fa-download"></i></a>
														
													</div>
												</div>
											</div>
											<div class="caption">
												<p>Logo</p>
											</div>
										</div>
										
									</div>
									
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				
				
				
				
				
				
				
				<div class="x_panel">
					<div class="x_title">
						<h2>Trocar Logo Sistema</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
									class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a>
									</li>
									<li><a href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="alert alert-danger alert-dismissible " role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<strong>Atenção!</strong> Utilizar arquivos com a seguinte extensão ".img".
						</div>
						<form>
							
						</form>
						<form  class="dropzone" action="backend/tools-logo-sistema-upload-backend.php" method="post">
							</form >
							
							
							
							</div>
							</div>
							
							</div>
							</div>
							
						