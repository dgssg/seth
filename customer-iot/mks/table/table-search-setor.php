<?php
include("../database/database.php");
$query = "SELECT setor.codigo,setor.id,setor.nome,setor.custo,setor.upgrade,setor.reg_date, unidade.unidade FROM setor INNER JOIN unidade ON setor.id_unidade = unidade.id WHERE setor.trash = 1 ORDER by setor.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
