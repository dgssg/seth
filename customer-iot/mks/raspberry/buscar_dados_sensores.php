<?php
// Faça a conexão com o banco de dados aqui
  include("../database/database.php");
  
// Consultar os dados dos sensores
$sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time, dados_ids.id_status,dados_ids.id_sensor_type
        FROM sensor_dados sd
        INNER JOIN (
          SELECT sensor_dados.device, MAX(sensor_dados.id) AS ultimo_id
          FROM sensor_dados
          GROUP BY device
        ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
        INNER JOIN (
          SELECT nome AS nome, up_time, macadress, id_status, id_sensor_type
          FROM sensor 
        ) dados_ids ON sd.device = dados_ids.macadress";

$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
  // Construir o HTML para exibir os dados dos sensores
  $html = '';
  while ($row = mysqli_fetch_assoc($result)) {
    if ($row['id_sensor_type'] == 1) {
      $html .= "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>";
      $html .= "<div class='tile-stats'>";
      $html .= "<h5>" . $row['nome'] . "</h5>";
      $html .= "<p>" . $row['up_time'] . "</p>";
      $html .= "<p>" . $row['device'] . "</p>";

      if ($row['id_status'] == 2) {
        $html .= "<span class='badge badge-danger'>Offline</span>";
      } else {
        $html .= "<span class='badge badge-success'>Online</span>";
      }

      $html .= "<div id='{$row['device']}' style='width: 400px; height: 120px;'></div>";
      $html .= "</div>";
      $html .= "</div>";
    }
  }
  echo $html;
} else {
  echo "Nenhum dado de sensor encontrado.";
}
?>
