function AboutDialog(information, infowidth, infoheight, backcolor)
{
  var self = this;
  this.dlg = app.CreateDialog("About:");
  this.lay = app.CreateLayout( "linear", "FillXY" );
  this.txt = app.CreateWebView(infowidth, infoheight);
  this.txt.SetBackColor(backcolor);
  this.txt.LoadHtml(information);
  this.txt.SetMargins(0,0.01,0,0);
  this.txt.parent = self;
  this.lay.AddChild(this.txt);
  this.btnlay = app.CreateLayout( "Linear", "Horizontal,FillXY" );
  this.dlgbtn = app.CreateButton("OK", 0.45,-1);
  this.dlgbtn.parent = self;
  this.dlgbtn.SetOnTouch(dlgbtn_OnTouch);
  this.btnlay.AddChild(this.dlgbtn);
  this.lay.AddChild(this.btnlay);
  this.dlg.AddLayout(this.lay);
  this.Show = function(){self.dlg.Show();}
  this.Hide = function(){self.dlg.Hide();}
}//function AboutDialog()
 
 
// Called if button is touched
function dlgbtn_OnTouch()
{
    var par = this.parent;
    par.Hide();
}//function dlgbtn_OnTouch