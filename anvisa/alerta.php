<?php

// Inclui a biblioteca simple_html_dom
//require 'simple_html_dom.php';
include("simple_html_dom.php");
// Carrega o Simple HTML DOM Parser
//require 'simple_html_dom.php';

// Define a URL a ser acessada
$url = 'http://antigo.anvisa.gov.br/alertas';

// Inicializa a sessão cURL
$ch = curl_init();

// Define as opções do cURL
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Faz a requisição e armazena o HTML retornado
$html = curl_exec($ch);

// Encerra a sessão cURL
curl_close($ch);

// Cria um objeto Simple HTML DOM a partir do HTML retornado
$dom = str_get_html($html);

// Encontra todos os elementos com a classe 'lista-noticias'
$alertas = $dom->find('.lista-noticias');

// Itera sobre os alertas encontrados
foreach ($alertas as $alerta) {
    // Extrai as informações relevantes do alerta
    $data = trim($alerta->find('.data', 0)->plaintext);
    $hora = trim($alerta->find('.hora', 0)->plaintext);
    $titulo = trim($alerta->find('.titulo', 0)->plaintext);
    $resumo = trim($alerta->find('.descricao-resumo', 0)->plaintext);

    // Imprime as informações do alerta
    echo "Data: $data<br>";
    echo "Hora: $hora<br>";
    echo "Título: $titulo<br>";
    echo "Resumo: $resumo<br><br>";
    $url = 'http://antigo.anvisa.gov.br/informacoes-tecnicas13/-/asset_publisher/R6VaZWsQDDzS/content/' . str_replace([' ', '(', ')', 'ê'], ['-', '', '', 'e'], $titulo);
   // echo "<li><a href=\"$url\" target=\"_blank\">$titulo</a></li>";
}
//http://antigo.anvisa.gov.br/informacoes-tecnicas13/-/asset_publisher/R6VaZWsQDDzS/content/

?>
