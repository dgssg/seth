<?php

date_default_timezone_set('America/Sao_Paulo');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../../../framework/images/favicon.ico" type="image/ico" />

<title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>

   <!-- Bootstrap -->
    <link href="../../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="../../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../../../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../../../framework/build/css/custom.min.css" rel="stylesheet">

      <!-- PNotify -->
  <link href="../../../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../../../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../../../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">



   <!-- <script src="jquery.min.js" ></script> -->

  </head>


  <body class="nav-md">







            <!-- menu profile quick info -->

            <!-- /menu profile quick info -->



  <!-- sidebar menu -->

            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Perfil" href="#">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                <script>
                var goFS = document.getElementById("goFS");
                goFS.addEventListener("click", function() {
                  document.body.requestFullscreen();
                }, true);
              </script>

              </a>
              <a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="#">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>


            </div>
            <!-- /menu footer buttons -->


 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i> </a>

                </div>
                <center>
                  Ultima Atualização: <?php echo date(DATE_RFC2822); ?>
                </center>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">







                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

        <!-- /top navigation -->
        <!-- page content -->


<?php include 'api.php';?>
<?php include 'frontend/dashboard-frontend.php';?>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           ©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://mksistemasbiomedicos.com.br"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->


    <!-- jQuery
    <script src="../../../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="../../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../../../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../../../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../../../framework/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../../../framework/build/js/custom.min.js"></script>




    <!-- jQuery-->
    <script src="../../../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap-->
    <script src="../../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../../../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../../../framework/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../../../../framework/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../../../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../../../framework/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../../../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../../../framework/build/js/custom.min.js"></script>
 <!-- PNotify -->
   <script src="../../../../framework/vendors/pnotify/dist/pnotify.js"></script>
   <script src="../../../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
   <script src="../../../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>


      <!-- bootstrap-wysiwyg -->
    <script src="../../../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../../../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../../../../framework/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../../../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../../../../framework/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->

    <!-- Parsley -->
    <script src="../../../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../../../../framework/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../../../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../../../../framework/vendors/starrr/dist/starrr.js"></script>
   <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>


	</body>


 </html>
