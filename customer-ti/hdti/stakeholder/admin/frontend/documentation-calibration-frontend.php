<?php
$codigoget = ($_GET["equipamento"]);

session_start();
$setor = $_SESSION['setor'] ;
// Create connection

include("../../database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Laudos de <small>Calibração</small></h3>
              </div>

           
            </div>
            
              
              
              
              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
              
                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Laudos </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                <?php

date_default_timezone_set('America/Sao_Paulo');
$data= date(DATE_RFC2822);
                  $carimbo_tempo = strtotime($data);
                  $data_now=date('Y-m-d',$carimbo_tempo);

$query = "SELECT instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration INNER JOIN equipamento on equipamento.id = calibration.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE calibration.trash =1 and equipamento.id_instituicao_localizacao in ($setor)";
$query = "SELECT calibration_preventive.file AS 'file_01',regdate_calibration_dropzone.file AS 'file_02',maintenance_preventive.file AS 'file_03',regdate_mp_dropzone.file AS 'file_04',instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration INNER JOIN equipamento on equipamento.id = calibration.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN maintenance_routine ON maintenance_routine.id_equipamento = equipamento.id LEFT JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id LEFT JOIN regdate_mp_dropzone ON regdate_mp_dropzone.id_mp = maintenance_preventive.id LEFT JOIN calibration_routine ON calibration_routine.id_equipamento = equipamento.id LEFT JOIN calibration_preventive ON  calibration_preventive.id_routine = calibration_routine.id LEFT JOIN regdate_calibration_dropzone ON regdate_calibration_dropzone.id_mp = calibration_preventive.id WHERE calibration.trash = 1 and equipamento.id_instituicao_localizacao in ($setor) GROUP BY equipamento.id
";
$query = "SELECT   calibration_preventive.file AS 'file_01',regdate_calibration_dropzone.file AS 'file_02',maintenance_preventive.file AS 'file_03',regdate_mp_dropzone.file AS 'file_04',instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration LEFT join equipamento on equipamento.id = calibration.id_equipamento LEFT join equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao LEFT JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN maintenance_routine ON maintenance_routine.id_equipamento = equipamento.id LEFT JOIN (
  SELECT  maintenance_preventive.file,maintenance_preventive.id_routine,maintenance_preventive.id, maintenance_preventive.date_end
  FROM    maintenance_preventive INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine
    WHERE maintenance_preventive.id_status = 3
  ORDER BY maintenance_preventive.id DESC ) maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id LEFT JOIN (
  SELECT  regdate_mp_dropzone.file,regdate_mp_dropzone.id_mp
  FROM    regdate_mp_dropzone INNER JOIN maintenance_preventive ON maintenance_preventive.id = regdate_mp_dropzone.id_mp INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine
WHERE maintenance_preventive.id_status = 3  ORDER BY regdate_mp_dropzone.id DESC ) regdate_mp_dropzone ON regdate_mp_dropzone.id_mp = maintenance_preventive.id LEFT JOIN calibration_routine ON calibration_routine.id_equipamento = equipamento.id LEFT JOIN calibration_preventive ON  calibration_preventive.id_routine = calibration_routine.id LEFT JOIN regdate_calibration_dropzone ON regdate_calibration_dropzone.id_mp = calibration_preventive.id WHERE calibration.trash = 1 and calibration.data_start = maintenance_preventive.date_end and equipamento.id_instituicao_localizacao in ($setor) GROUP by calibration.id_equipamento";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($file_01,$file_02,$file_03,$file_04,$instituicao,$area,$setor,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }



?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Laudo</th>
                          <th>Anexo</th>
                          <th>Equipamento</th>
                          <th>Realização</th>
                          <th>Validade</th>
                          <th>vencido</th>
                           <th>Status</th>
                           <th>Unidade</th>
                           <th>Setor</th>
                           <th>Area</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {
                               $data_now=date("Y-m-d");
                              $new_date=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));

                            if ( $new_date < $data_now ) {
                              $ven = 1;

                            }

                            if($ven == 0 && $status == 0 ){ 
                              ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td> <?php if($file_01==!""){?><a href="../../dropzone/calibration/<?php printf($file_01); ?> " download><i class="fa fa-paperclip"></i> </a>     <?php  }  ?>
                          <?php if($file_02==!""){?><a href="../../dropzone/calibration/<?php printf($file_02); ?> " download><i class="fa fa-paperclip"></i> </a>     <?php  }  ?>
                          <?php if($file_03==!""){?><a href="../../dropzone/mp/<?php printf($file_03); ?> " download><i class="fa fa-paperclip"></i> </a>     <?php  }  ?>
                          <?php if($file_04==!""){?><a href="../../dropzone/mp-dropzone/<?php printf($file_04); ?> " download><i class="fa fa-paperclip"></i> </a>     <?php  }  ?>

                        
                        
                        </td>
                          <td><?php printf($codigo); ?> <?php printf($nome); ?> <?php printf($fabricante); ?> <?php printf($modelo); ?></td>
                          <td><?php printf($data_start); ?></td>
                            <td><?php printf($val); ?> Meses</td>
                            <td><?php if($ven ==0) {printf("Não");} if($ven ==1) {printf("Sim");}  ?></td>
                            <td><?php if($status ==0) {printf("Aprovado");} if($status ==1) {printf("Reprovado");}  ?> </td>
                            <td><?php printf($instituicao); ?></td>
                               <td><?php printf($area); ?></td>
                               <td>  <?php printf($setor); ?></td>


                          <td>
              <!--    <a  class="btn btn-app" href="../../calibration-report-viewer-copy?laudo=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
                                       title: 'Visualizar',
                                       text: 'Visualizar Laudo!',
                                       type: 'info',
                                       styling: 'bootstrap3'
                                   });" >
                           <i class="fa fa-file-pdf-o"></i> PDF
                         </a> -->

                         <?php if ($file_01 != "") {

?>
<a class="btn btn-app"  href="../../dropzone/calibration/<?php printf($file_01); ?>" target="_blank" onclick="new PNotify({
  title: 'Anexo',
  text: 'Abrir Anexo',
  type: 'sucess',
  styling: 'bootstrap3'
});">
<i class="fa fa-file-text-o "></i> PDF
</a>
<?php } ?>

<?php if ($file_02 != "") {

?>
<a class="btn btn-app"  href="../../dropzone/calibration/<?php printf($file_02); ?>" target="_blank" onclick="new PNotify({
  title: 'Anexo',
  text: 'Abrir Anexo',
  type: 'sucess',
  styling: 'bootstrap3'
});">
<i class="fa fa-file-text-o "></i> PDF
</a>
<?php } ?>

<?php if ($file_03 != "") {

?>
<a class="btn btn-app"  href="../../dropzone/mp/<?php printf($file_03); ?>" target="_blank" onclick="new PNotify({
  title: 'Anexo',
  text: 'Abrir Anexo',
  type: 'sucess',
  styling: 'bootstrap3'
});">
<i class="fa fa-file-text-o "></i> PDF
</a>
<?php } ?>

<?php if ($file_04 != "") {

?>
<a class="btn btn-app"  href="../../dropzone/mp-dropzone/<?php printf($file_04); ?>" target="_blank" onclick="new PNotify({
  title: 'Anexo',
  text: 'Abrir Anexo',
  type: 'sucess',
  styling: 'bootstrap3'
});">
<i class="fa fa-file-text-o "></i> PDF
</a>
<?php } ?>

                  </td>
                        </tr>
                        <?php   } } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

                  
                </div>
              </div> 
	       
	        
                  
                  
                </div>
              </div>
              <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
        initComplete: function () {
            this.api()
                .columns([0,2,3,4,5,6,7,8,9])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
                    var container = $('<div class="filter-title"></div>')
                                    .appendTo('#userstable_filter');
                    var label = $('<label>'+theadname+': </label>')
                                .appendTo(container); 
                    var select = $('<select  class="form-control my-1"><option value="">' +
        theadname +'</option></select>')
                        .appendTo( '#userstable_filter'  ).select2()
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
 
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });

                
        },
    });
});
</script>
  