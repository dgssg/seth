<?php
include("database/database.php");
$query = "SELECT equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome, calibration_control.id,calibration_control.id_routine,calibration_control.data_start,calibration_control.data_after,calibration_control.status,calibration_control.procedure_mp,calibration_control.reg_date,calibration_control.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo  FROM calibration_control INNER JOIN calibration_routine ON calibration_routine.id = calibration_control.id_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id  INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($serie,$fabricante,$instituicao,$area,$setor,$id,$rotina_id,$data_start, $data_after,$status,$procedure_mp,$reg_date,$upgrade,$codigo,$nome,$modelo);



  ?>


  <div class="col-md-11 col-sm-11 ">
    <div class="x_panel">

      <div class="x_content">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box table-responsive">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                  <th>#</th>
                    <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>                   
                      <th>Unidade</th>
                      <th>Setor</th>
                      <th>Area</th> 

                    <th>Rotina</th>
                    <th>Programada</th>
                    <th>Proxima</th>
                    <th>Status</th>
                    <th>Procedimento</th>
                    <th>Atualização</th>
                    <th>Cadastro</th>

                    <th>Docuentação</th>
                    <th>Controle M.P</th>
                    <th>Cronograma M.P</th>
                    <th>Ação</th>

                  </tr>
                </thead>


                <tbody>
                  <?php   while ($stmt->fetch()) {   ?>
                    <tr>
                    <td ><?php printf($id); ?> </td>
                      <td><?php printf($nome); ?></td>
                              <td><?php printf($modelo); ?></td>
                            <td><?php printf($fabricante); ?></td>
                            <td><?php printf($codigo); ?></td>
                            <td><?php printf($serie); ?></td>
                              
                              <td><?php printf($instituicao); ?></td>
                                 <td><?php printf($area); ?></td>
                                 <td>  <?php printf($setor); ?></td>
                      <td ><?php printf($rotina_id); ?> </td>
                      <td ><?php printf($data_start); ?> </td>
                      <td ><?php printf($data_after); ?> </td>
                      <td><?php if($status==0){printf("Programada");}if($status==1){printf("Aberta");} ?></td>
                        <td><?php printf($procedure_mp); ?></td>
                      <td><?php printf($upgrade); ?></td>
                      <td><?php printf($reg_date); ?></td>


                      <td>

                        <a class="btn btn-app"  href="flow-calibration-viwer-control?routine=<?php printf($rotina_id); ?>" target="_blank"  onclick="new PNotify({
                          title: 'Visualizar',
                          text: 'Visualizar Rotina',
                          type: 'info',
                          styling: 'bootstrap3'
                        });">
                        <i class="fa fa-file-pdf-o"></i> Visualizar
                      </a>

                      <a class="btn btn-app"  href="flow-calibration-open-tag-control?routine=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                        title: 'Visualizar',
                        text: 'Visualizar Etiqueta',
                        type: 'info',
                        styling: 'bootstrap3'
                      });">
                      <i class="fa fa-qrcode"></i> Etiqueta
                    </a>


                    <a class="btn btn-app"  href="flow-calibration-viwer-print-control?routine=<?php printf($rotina_id); ?>" target="_blank"  onclick="new PNotify({
                      title: 'Imprimir',
                      text: 'Imprimir Rotina',
                      type: 'info',
                      styling: 'bootstrap3'
                    });">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
                  <a class="btn btn-app"  href="flow-calibration-preventive-label-control?routine=<?php printf($rotina_id); ?>" target="_blank" onclick="new PNotify({
                               title: 'Etiqueta',
                               text: 'Abrir Etiqueta',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-fax"></i> Rótulo
                 </a>
                </td>
                    <td>
                  <a class="btn btn-app"  href="backend/calibration-routine-retweet-backend.php?routine=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                    title: 'Calibração',
                    text: 'Calibração Retroativo',
                    type: 'warning',
                    styling: 'bootstrap3'
                  });">
                  <i class="fa fa-retweet"></i> Calibração Retroativo
                </a>
                <a class="btn btn-app"  href="backend/calibration-routine-fast-forward-backend.php?routine=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                  title: 'Calibração',
                  text: 'Calibração Antecipada',
                  type: 'warning',
                  styling: 'bootstrap3'
                });">
                <i class="fa fa-fast-forward"></i> Calibração Antecipada
              </a>

              <a class="btn btn-app"  href="calibration-routine-reschedule?routine=<?php printf($rotina); ?>"  onclick="new PNotify({
                           title: 'Calibração',
                           text: 'Calibração Reprogramada',
                           type: 'warning',
                           styling: 'bootstrap3'
                       });">
               <i class="fa fa-clock-o"></i> Calibração Reprogramada
             </a>
             <a class="btn btn-app"  href="backend/calibration-routine-update-date-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                          title: 'Atualização',
                          text: 'Atualização Preventiva Reprogramada',
                          type: 'warning',
                          styling: 'bootstrap3'
                      });">
              <i class="fa fa-calendar"></i> Atualização Data
            </a>
            <a class="btn btn-app"  href="flow-calibration-routine-new?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                         title: 'Nova',
                         text: 'Nova M.P',
                         type: 'sucess',
                         styling: 'bootstrap3'
                     });">
             <i class="fa fa-plus-square-o"></i> Nova Calibração
           </a>

           </td>

           <td>
             <a class="btn btn-app"  href="backend/calibration-routine-horaire-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                          title: 'Atualização',
                          text: 'Atualização Cronograma',
                          type: 'warning',
                          styling: 'bootstrap3'
                      });">
              <i class="fa fa-calendar-o"></i> Atualização Cronograma
            </a>

         </td>

               <td>
            
            <a class="btn btn-app"  href="backend/calibration-routine-status-open-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                        title: 'Aberto',
                        text: 'Status MP Aberto',
                        type: 'warning',
                        styling: 'bootstrap3'
                    });">
           <i class="fa fa-folder-open"></i> Aberto</a>
           <a class="btn btn-app"  href="backend/calibration-routine-status-reschedule-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                       title: 'Programada',
                       text: 'Status MP Programada',
                       type: 'warning',
                       styling: 'bootstrap3'
                   });">
          <i class="fa fa-folder"></i> Programado</a>
            </td>
          </tr>
        <?php   } }  ?>
      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>
</div>
