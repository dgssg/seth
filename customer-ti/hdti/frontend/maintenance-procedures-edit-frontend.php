<?php


namespace Phppot;

use Phppot\Model\FAQ;

include("database/database.php");
$codigoget = ($_GET["procedures"]);
$query = "SELECT maintenance_procedures.id,maintenance_procedures.habilitado,maintenance_procedures.info,maintenance_procedures.id, maintenance_procedures.name,category.nome,maintenance_procedures.codigo,maintenance_procedures.reg_date,maintenance_procedures.upgrade FROM maintenance_procedures INNER JOIN category ON category.id = maintenance_procedures.id_category WHERE maintenance_procedures.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $habilitado,$info,$id, $name, $categoria, $codigo, $reg_date, $upgrade);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}

?>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

<script src="./assets/js/inlineEdit_procedures.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>Procedimento</small></h3>
              </div>


            </div>



            <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="maintenance-procedures">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                   <a class="btn btn-app"  href="maintenance-procedures-viewer?procedures=<?php printf($codigoget ); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar procediemnto',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                 


              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Procedimento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                 <form action="backend/maintenance-procedures-edit-backend.php" method="post">

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($name); ?>"  required="required" class="form-control ">
                        </div>
                      </div>
                   <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"  value="<?php printf($categoria); ?>" readonly="readonly">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Info</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="info" class="form-control" type="text" name="info"  value="<?php printf($info); ?>" >
                        </div>
                      </div>

                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilitado</label>
                        <div class="col-md-6 col-sm-6 ">
                           <div class="">
                            <label>
                              <input name="habilitado"type="checkbox" class="js-switch" <?php if($habilitado == "0"){printf("checked"); }?> />
                            </label>
                          </div>

                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($codigo); ?>" >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                                  </form>



                  </div>
                </div>
              </div>
	       </div>

         
    <div class="x_panel">
                <div class="x_title">
                  <h2>Alerta</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

             <div class="alert alert-success alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Area de Edição!</strong> Para alterar qualquer item do procedimento basta realizar na respectiva linha que automaticamente será salvo pelo sistema.
                  </div>



                </div>
              </div>

            <div class="clearfix"></div>

          <div class="x_panel">
                <div class="x_title">
                  <h2>Itens</h2>
                  <ul class="nav navbar-right panel_toolbox">
                       <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo-->

                <table class="table table-striped jambo_table bulk_action">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Posição</th>
                          <th>Classe</th>
                          <th>Procedimento</th>
                           <th><center>A&ccedil;&atilde;o </center> </th>
                        </tr>
                      </thead>
                      <tbody>
       <?php

                                      require_once ("Model/FAQ_procedure.php");
                                      $faq = new FAQ();
                                      $faqResult = $faq->getFAQ();
            

foreach ($faqResult as $k => $v) {
  if(($faqResult[$k]["id_maintenance_procedures"] == $codigoget) ) {

 
?>                                        <tr class="table-row">

                        <td contenteditable="false"><?php echo $row ?></td>
                        <td bgcolor="" contenteditable="true"
                      onBlur="saveToDatabase(this,'position','<?php echo $faqResult[$k]["id"]; ?>')"
                      onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["position"]; ?></td>
                    <!--  <td bgcolor="" contenteditable="true"
                      onBlur="saveToDatabase(this,'classe','<?php echo $faqResult[$k]["id"]; ?>')"
                      onClick="showEdit(this);" bgcolor="" > <select type="text" >
                    <option value="1" <?php if($faqResult[$k]["classe"] == 1){printf("selected");}  ?> >1</option>Titulo
                    <option value="2"<?php if($faqResult[$k]["classe"] == 2){printf("selected");}  ?>>2</option>Procedimento
                    <option value="3"<?php if($faqResult[$k]["classe"] == 3){printf("selected");}  ?>>3</option>Coleta de Dados
                    </select>
                    </td> -->
                    <td bgcolor="" contenteditable="false">
  <select onChange="saveToDatabase(this,'classe','<?php echo $faqResult[$k]["id"]; ?>')">
    <option value="1" <?php if($faqResult[$k]["classe"] == 1){printf("selected");}  ?>>Titulo</option>
    <option value="2" <?php if($faqResult[$k]["classe"] == 2){printf("selected");}  ?>>Procedimento</option>
    <option value="3" <?php if($faqResult[$k]["classe"] == 3){printf("selected");}  ?>>Coleta de Dados Horizontal</option>
    <option value="4" <?php if($faqResult[$k]["classe"] == 4){printf("selected");}  ?>>Coleta de Dados Vertical</option>
    <option value="5" <?php if($faqResult[$k]["classe"] == 5){printf("selected");}  ?>>Sem formatação</option>

  </select>
</td>



                        <!--  <td><?php //if($class==1){printf("Titulo");} if($class==2){printf("Procedimento");} if($class==3){printf("Coleta de Dados");}?></td>
  --> 
                    <td bgcolor="" contenteditable="true"
                      onBlur="saveToDatabase(this,'procedures','<?php echo $faqResult[$k]["id"]; ?>')"
                      onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["procedures"]; ?></td>
                           <td>
                             
                           <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/maintenance-procedures-drop-itens.php?id=<?php echo $faqResult[$k]['id']; ?>&procedures=<?php printf($codigoget); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> </a>
                           </td>
                         </tr> 
                   <?php  $row=$row+1; } } ?>
                      </tbody>
                    </table>





                </div>
              </div>


               <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Itens</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                           <form action="backend/maintenance-procedures-edit-insert-itens-backend.php?id_procedures=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>

                   

          <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Procedimento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="procedures" name="procedures" required="required" class="form-control has-feedback-left">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Classe <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                        <select class="form-control has-feedback-left" name="classe" id="classe"  type="text" >
                            <option>Selecione Tipo</option>
                            <option value="1">Titulo</option>
                            <option value="2">Procedimento</option>
                            <option value="3">Coleta de Dados Horizontal</option>
                            <option value="4">Coleta de Dados Vertical</option>
                            <option value="5">Sem formatação</option>


                          </select>
 			                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Posição <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                        <input   class="form-control has-feedback-left" type="number" name="column" id="column" >
                        </div>
                      </div>

                   


                      <div class="ln_solid"></div>



                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>

              <!-- -->


              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="maintenance-procedures">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                   <a class="btn btn-app"  href="maintenance-procedures-viewer?procedures=<?php printf($codigoget ); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar procediemnto',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                   


              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
