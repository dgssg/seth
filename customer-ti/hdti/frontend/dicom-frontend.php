<?php
include("database/database.php");
$row=1;
$query = "SELECT dicom.reg_date,dicom.upgrade,dicom_type.nome,dicom_status.nome,dicom_mod.nome,dicom.id,dicom.number_dicom,dicom.object,dicom.manufacture FROM dicom INNER JOIN dicom_status ON dicom_status.id = dicom.id_dicom_status INNER JOIN dicom_mod ON dicom_mod.id = dicom.id_dicom_mod  INNER JOIN dicom_type ON dicom_type.id = dicom.id_dicom_type  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($reg_date,$upgrade,$dicom_type,$dicom_status,$dicom_mod,$id,$number_dicom,$object,$manufacture);


  ?>

  <div class="right_col" role="main">

    <div class="">

      <div class="page-title">
        <div class="title_left">
          <h3>DICOM</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5  form-group pull-right top_search">
            <div class="input-group">

              <span class="input-group-btn">

              </span>
            </div>
          </div>
        </div>
      </div>


      <div class="x_panel">
        <div class="x_title">
          <h2>Menu</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app" href="dicomdicom" onclick="new PNotify({
              title: 'DICOM',
              text: 'DICOM',
              type: 'success',
              styling: 'bootstrap3'
            });">
            <i class="fa fa-database"></i> DICOM
          </a>

                      <a class="btn btn-app" href="dicomdicom-configure" onclick="new PNotify({
                        title: 'Configurações DICOM',
                        text: 'Configurações DICOM',
                        type: 'success',
                        styling: 'bootstrap3'
                      });">
                      <i class="fa fa-cogs"></i> Configurações
                    </a>





        </div>
      </div>




      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>



      <div class="x_panel">
        <div class="x_title">
          <h2>Lista de DICOM</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                  class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Status</th>
                    <th>DICOM</th>
                    <th>Empresa</th>
                    <th>Objeto</th>
                    <th>Modalidade</th>
                    <th>Tipo</th>
                    
                    <th>Atualização</th>
                    <th>Cadastro</th>
                    <th>Ação</th>


                  </tr>
                </thead>
                <tbody>
                  <?php  while ($stmt->fetch()) { ?>

                    <tr>
                      <th scope="row"><?php printf($row); ?></th>
                      <td><?php printf($dicom_status); ?></td>
                      <td><?php printf($number_dicom); ?></td>
                      <td><?php printf($manufacture); ?></td>
                      <td><?php printf($object); ?></td>

                      <td><?php printf($dicom_mod); ?></td>
                      <td><?php printf($dicom_type); ?></td>
                      
                      <td><?php printf($upgrade); ?></td>
                      <td><?php printf($reg_date); ?></td>


                      <td>
                        <a class="btn btn-app"  href="dicomdicom-edit?id=<?php printf($id); ?>"onclick="new PNotify({
                          title: 'Atualização',
                          text: 'Atualização!',
                          type: 'danger',
                          styling: 'bootstrap3'
                        });">
                        <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                      </a>
                        <a class="btn btn-app"  href="dicomdicom-viwer?id=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
                          title: 'Visualizar',
                          text: 'Visualizar!',
                          type: 'success',
                          styling: 'bootstrap3'
                        });">
                          <i class="fa fa-file-pdf-o"></i> Visualizar
                        </a>
                      <a class="btn btn-app" onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/dicomdicom-trash.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                  </td>
                </tr>
                <?php  $row=$row+1; }
              }   ?>
            </tbody>
          </table>







          <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">

                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Itens</h4>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="backend/dicomdicom-backend.php" method="post">
                    <div class="ln_solid"></div>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_instituicao">unidade <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_instituicao" id="id_instituicao"  placeholder="Unidade">
                          <option value="">Selecione uma Unidade</option>
                          <?php



                          $result_cat_post  = "SELECT  id, codigo,instituicao FROM instituicao WHERE trash = 1";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['codigo'].' - '.$row_cat_post['instituicao'].' </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Unidade "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>


                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nº do Contrato</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="number_dicom" class="form-control" type="text" name="number_dicom"   >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_dicom_mod">Modalidade <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_dicom_mod" id="id_dicom_mod"  placeholder="Modalidade">
                          <option value="">Selecione uma Modalidade</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM dicom_mod ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Unidade "></span>

                      </div>
                    </div>
                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>
                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_dicom_type">Tipo <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_dicom_type" id="id_dicom_type"  placeholder="Tipo">
                          <option value="">Selecione um Tipo</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM dicom_type ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Tipo "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>

                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Objeto</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="object" class="form-control" type="text" name="object"   >
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_dicom_status">Status <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="id_dicom_status" id="id_dicom_status"  placeholder="Status">
                          <option value="">Selecione um Status</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM dicom_status ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Status "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>









                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary" onclick="new PNotify({
                      title: 'Registrado',
                      text: 'Informações registrada!',
                      type: 'success',
                      styling: 'bootstrap3'
                    });" >Salvar Informações</button>
                  </div>

                </div>
              </div>
            </div>
          </form>


        </div>
      </div>




    </div>
  </div>
  <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    },
        initComplete: function () {
            this.api()
                .columns([1,2,3,4,5,6])
                .every(function (d) {
                    var column = this;
                  var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      }); 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                
                
        },
    });
});


</script>



















</div>
</div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
