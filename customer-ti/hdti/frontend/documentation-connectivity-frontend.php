 <?php

include("database/database.php");


//$con->close();


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Documentos & POP <small></small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                                               
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                  <a class="btn btn-app <?php echo $current_page == 'documentation' ? 'active' : ''; ?>" href="documentation">
                   <i class="fa fa-code"></i> Sistema
                 </a>

              
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-pop' ? 'active' : ''; ?>" href="documentation-pop">

                   <i class="fa fa-book"></i> POP
                 </a>
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-update' ? 'active' : ''; ?>" href="documentation-update">

                   <i class="fa fa-history"></i> Atualização
                 </a>
                  <a class="btn btn-app <?php echo $current_page == 'documentation-hfmea' ? 'active' : ''; ?>" href="documentation-hfmea">
                    
                    <i class="fa fa-map"></i> HFMEA
                  </a>              
                                   
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-label' ? 'active' : ''; ?>" href="documentation-label">

                   <i class="fa fa-fax"></i> Rotuladora
                 </a>
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-bussines' ? 'active' : ''; ?>" href="documentation-bussines">

                    <i class="fa fa-folder-open"></i> Documentação
                  </a>
                
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-support' ? 'active' : ''; ?>" href="documentation-support">

                    <i class="fa fa-support"></i> Contingência
                  </a>

               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-bell' ? 'active' : ''; ?>" href="documentation-bell">

                    <i class="fa fa-bell"></i> Central de Notificações
                  </a>
                
                                    
            
                                  <a class="btn btn-app <?php echo $current_page == 'documentation-library' ? 'active' : ''; ?>" href="documentation-library">

                    <i class="fa fa-book"></i> Biblioteca
                  </a> 
               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-connectivity' ? 'active' : ''; ?>" href="documentation-connectivity">

                    <i class="fa fa-code-fork"></i> Conectividade &amp; Recursos
                  </a>
                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-event-task' ? 'active' : ''; ?>" href="documentation-event-task">

                    <i class="fa fa-tasks"></i> Eventos &amp; Taferas
                  </a>                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-widget' ? 'active' : ''; ?>" href="documentation-widget">

                    <i class="fa fa-desktop"></i> Widget
                  </a>

   <a class="btn btn-app <?php echo $current_page == 'documentation-xml' ? 'active' : ''; ?>" href="documentation-xml">

                    <i class="glyphicon glyphicon-upload"></i> XML
                  </a>



                </div>
              </div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="clearfix"></div>
                    <div class="col-md-12 col-sm-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>
                            <i class="fa fa-bars"></i>
                            Conectividade 
                            <small> &amp; Recursos</small>
                          </h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li>
                              <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                              </a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-wrench"></i>
                              </a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Settings 1</a>
                                <a class="dropdown-item" href="#">Settings 2</a>
                              </div>
                            </li>
                            <li>
                              <a class="close-link">
                                <i class="fa fa-close"></i>
                              </a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                     
                            <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal1">Baixar Aplicativo</button>
                            </div>
                            <div form-group id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal2">Acessar o Bot no Telegram</button>
                            </div>
                            <div class="form-group" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal3">Baixar Rotuladora</button>
                            </div>
                            <div class="form-group" id="tab4" role="tabpanel" aria-labelledby="tab4-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal4">Acessar ChatBoot</button>
                            </div>
                            <div class="form-group" id="tab5" role="tabpanel" aria-labelledby="tab5-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal5">Acessar Dialogflow</button>
                            </div>
                            <div class="form-group" id="tab6" role="tabpanel" aria-labelledby="tab6-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal6">Acessar LINE</button>
                            </div>
                            <div class="form-group" id="tab7" role="tabpanel" aria-labelledby="tab7-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal7">Acessar Slack</button>
                            </div>
                            <div class="form-group" id="tab8" role="tabpanel" aria-labelledby="tab8-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal8">Acessar Workplace</button>
                            </div>
                            <div class="form-group" id="tab9" role="tabpanel" aria-labelledby="tab9-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal9">Abrir no Messenger</button>
                            </div>
                            <div class="form-group" id="tab10" role="tabpanel" aria-labelledby="tab10-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal10">Acessar WhatsApp</button>
                            </div>
                            <div class="form-group" id="tab11" role="tabpanel" aria-labelledby="tab11-tab">
                              <button style="padding: 10px 15px; font-size: 16px;" data-toggle="modal" data-target="#modal11">Acessar Power B.I</button>
                            </div>
                         
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modais -->
            <!-- Modal 1 -->
            <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal1Label">Aplicativo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a href="https://seth.mksistemasbiomedicos.com.br/DroidScript/seth-ec/SETH%20Engenharia%20Clinica.apk" download>Link para baixar o aplicativo</a>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 2 -->
            <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal2Label">Telegram</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a href="https://t.me/mks_seth_bot" target="_blank">Link para acessar o bot no Telegram</a>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 3 -->
            <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="modal3Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal3Label">Rotuladora</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="panel">
                      <a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                        <h4 class="panel-title">Equipamento</h4>
                      </a>
                      <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <button class="btn btn-primary" onclick="downloadFile('label/seth_tag_brother.lbx')">Baixar Equipamento</button>
                        </div>
                      </div>
                    </div>
                    
                    <!-- Manutenção Preventiva -->
                    <div class="panel">
                      <a class="panel-heading" role="tab" id="headingOne2" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne">
                        <h4 class="panel-title">Manutenção Preventiva</h4>
                      </a>
                      <div id="collapseOne2" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <button class="btn btn-primary" onclick="downloadFile('label/seth_mp_brother.lbx')">Baixar Manutenção Preventiva</button>
                        </div>
                      </div>
                    </div>
                    
                    <!-- Calibração -->
                    <div class="panel">
                      <a class="panel-heading" role="tab" id="headingOne3" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne">
                        <h4 class="panel-title">Calibração</h4>
                      </a>
                      <div id="collapseOne3" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <button class="btn btn-primary" onclick="downloadFile('label/seth_cal_brother.lbx')">Baixar Calibração</button>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  
                  <script>
                    function downloadFile(url) {
                      // Simule o download do arquivo
                      window.location.href = url;
                    }
                  </script>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
                       
            <!-- Modal 4 -->
            <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="modal4Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal4Label">ChatBoot</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <iframe height="800" width="800" src="https://bot.dialogflow.com/411ac1cc-bc2e-4b98-83a8-3ac9a0ea40c7"></iframe>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 5 -->
            <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="modal5Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal5Label">Dialogflow</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <iframe height="800" width="800" src="api/api-datastudio.php"></iframe>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 6 -->
            <div class="modal fade" id="modal6" tabindex="-1" role="dialog" aria-labelledby="modal6Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal6Label">LINE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <?php
                      // Link para o QR Code
                      $link = "https://line.me/R/ti/p/%40624ibtlq";
                      
                      // Codifique o link para ser usado na URL da API do Google
                      $encodedLink = urlencode($link);
                      
                      // URL da API do Google para gerar o QR Code
                      $apiUrl = "https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl={$encodedLink}";
                      
                      // Exiba a imagem gerada
                      echo '<img src="' . $apiUrl . '" alt="QR Code">';
                    ?>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 7 -->
            <div class="modal fade" id="modal7" tabindex="-1" role="dialog" aria-labelledby="modal7Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal7Label">Slack</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <!-- Conteúdo do modal do Slack -->
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 8 -->
            <div class="modal fade" id="modal8" tabindex="-1" role="dialog" aria-labelledby="modal8Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal8Label">Workplace</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <!-- Conteúdo do modal do Workplace -->
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 9 -->
            <div class="modal fade" id="modal9" tabindex="-1" role="dialog" aria-labelledby="modal9Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal9Label">Facebook</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a href="https://www.facebook.com/profile.php?id=61555738596071" target="_blank">Link para abrir no Messenger</a>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 10 -->
            <div class="modal fade" id="modal10" tabindex="-1" role="dialog" aria-labelledby="modal10Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal10Label">WhatsApp</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <!-- Conteúdo do modal do WhatsApp -->
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>
            
            <!-- Modal 11 -->
            <div class="modal fade" id="modal11" tabindex="-1" role="dialog" aria-labelledby="modal11Label" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modal11Label">Power B.I</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <!-- Conteúdo do modal do Power B.I -->
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>




                </div>
              </div>


        <!-- /page content -->
        