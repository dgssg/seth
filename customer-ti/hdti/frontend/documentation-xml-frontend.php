<?php

include("database/database.php");


//$con->close();


?>

<!-- page content -->
       <div class="right_col" role="main">
         <div class="">
           <div class="page-title">
             <div class="title_left">
               <h3>Documentos & POP <small></small></h3>
             </div>


           </div>

           <div class="clearfix"></div>

            <div class="x_panel">
               <div class="x_title">
                 <h2>Ação</h2>
                 <ul class="nav navbar-right panel_toolbox">
                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                   </li>
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                         class="fa fa-wrench"></i></a>
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Settings 1</a>
                       </li>
                       <li><a href="#">Settings 2</a>
                       </li>
                     </ul>
                   </li>
                   <li><a class="close-link"><i class="fa fa-close"></i></a>
                   </li>
                 </ul>
                 <div class="clearfix"></div>
               </div>
               <div class="x_content">

                                              
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                  <a class="btn btn-app <?php echo $current_page == 'documentation' ? 'active' : ''; ?>" href="documentation">
                   <i class="fa fa-code"></i> Sistema
                 </a>

              
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-pop' ? 'active' : ''; ?>" href="documentation-pop">

                   <i class="fa fa-book"></i> POP
                 </a>
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-update' ? 'active' : ''; ?>" href="documentation-update">

                   <i class="fa fa-history"></i> Atualização
                 </a>
                  <a class="btn btn-app <?php echo $current_page == 'documentation-hfmea' ? 'active' : ''; ?>" href="documentation-hfmea">
                    
                    <i class="fa fa-map"></i> HFMEA
                  </a>              
                                   
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-label' ? 'active' : ''; ?>" href="documentation-label">

                   <i class="fa fa-fax"></i> Rotuladora
                 </a>
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-bussines' ? 'active' : ''; ?>" href="documentation-bussines">

                    <i class="fa fa-folder-open"></i> Documentação
                  </a>
                
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-support' ? 'active' : ''; ?>" href="documentation-support">

                    <i class="fa fa-support"></i> Contingência
                  </a>

               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-bell' ? 'active' : ''; ?>" href="documentation-bell">

                    <i class="fa fa-bell"></i> Central de Notificações
                  </a>
                
                                    
            
                                  <a class="btn btn-app <?php echo $current_page == 'documentation-library' ? 'active' : ''; ?>" href="documentation-library">

                    <i class="fa fa-book"></i> Biblioteca
                  </a> 
               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-connectivity' ? 'active' : ''; ?>" href="documentation-connectivity">

                    <i class="fa fa-code-fork"></i> Conectividade &amp; Recursos
                  </a>
                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-event-task' ? 'active' : ''; ?>" href="documentation-event-task">

                    <i class="fa fa-tasks"></i> Eventos &amp; Taferas
                  </a>                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-widget' ? 'active' : ''; ?>" href="documentation-widget">

                    <i class="fa fa-desktop"></i> Widget
                  </a>
   <a class="btn btn-app <?php echo $current_page == 'documentation-xml' ? 'active' : ''; ?>" href="documentation-xml">

                    <i class="glyphicon glyphicon-upload"></i> XML
                  </a>

               </div>
             </div>

           <div class="row" style="display: block;">
             <div class="col-md-12 col-sm-12  ">
               <div class="x_panel">
                 <div class="x_title">
                   <h2>Modelo <small>de Rotulos</small></h2>
                   <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="#">Settings 1</a>
                           <a class="dropdown-item" href="#">Settings 2</a>
                         </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                   </ul>
                   <div class="clearfix"></div>
                 </div>
                 <div class="x_content">


                   <!-- start accordion -->
                   <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                     <div class="panel">
                       <a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                         <h4 class="panel-title">Equipamento</h4>
                       </a>
                       <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                         <div class="panel-body">
                           <table class="table table-striped">
                             <thead>
                               <tr>
                                 <th>#</th>

                                 <th>Nome</th>

                                 <th>Versão</th>
                                 <th>Visualizar</th>



                               </tr>
                             </thead>
                             <tbody>

                               <tr>

                                 <th scope="row"> 1</th>
                                 <td>Grupo de Equipamento</td>
                                 <td>v1</td>
                                    <td><a  href="xml/grupo_equipamento.xml"   download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>



                               </tr>

                             </tbody>
                           </table>
                         </div>
                       </div>
                     </div>


                     















                   </div>
                   <!-- end of accordion -->


                 </div>
               </div>
             </div>
        </div>




               </div>
             </div>


       <!-- /page content -->
