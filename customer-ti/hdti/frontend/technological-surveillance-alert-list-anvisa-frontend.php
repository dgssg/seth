<?php  
 $sweet_salve = ($_GET["sweet_salve"]); 

if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi salvo!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';

        
    }
    ?>
    
   <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" style="width:100%">
                       <thead>
                                <tr>
                                    
                                    <th>#</th>
                                    <th>Resumo</th>
                                   
                                     
                                    <th>Data</th>
                                    <th>Anexos</th>
                                   <th>Leitura</th>
                                    
                                  
                              
                                    <th>Registro</th>
                                    <th>Título</th>
                                  
                                    <th>Link</th>
                                   
                                    <th>Ação</th>

                                </tr>
                            </thead>


                            <tbody>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
         
<script language="JavaScript">
 let paginaAtual = 0;

 $(document).ready(function() {
  $('#datatable').dataTable( {
    "processing": true,
        "stateSave": true,
        "responsive": true,
    scrollX: true,   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<        
    scrollY: 700,   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    scrollCollapse: true,
    responsive: true,
    colReorder: false,
    keys: true,
    select: true,
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );

  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-anvisa.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
  .then(data => {
    // Mapeia os dados para o formato esperado pelo DataTables
    const novosDados = data.map(item => [
      ``,
      item.resum,

 
  item.date_alert,
  `<a  href="${item.anexo}" target="_blank">Carta ao Cliente </a>`,
  item.read_open == "0" ? "Sim" : "Não",
  item.reg_date,
  item.titulo,
 
 


  ` 
     
                  <a class="btn btn-app"   href="${item.link_anvisa}" target="_blank" onclick="new PNotify({
																title: 'Acessar',
																text: 'Acessar',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-external-link"></i> Acessar
                  </a>
                
                `,
  ` 
        <a class="btn btn-app"  href="technological-surveillance-register?id=${item.id}" onclick="new PNotify({
																title: 'Adicionar',
																text: 'Adicionar Grupo',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-plus"></i> Adicionar 
                  </a>
      
                  <a class="btn btn-app"  href="backend/technological-surveillance-alert-anvisa-register-open?id=${item.id}" onclick="new PNotify({
																title: 'Abrindo',
																text: 'Abrindo',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-envelope"></i> Marcar como lido
                  </a>
                  <a class="btn btn-app"  type="button"  data-toggle="modal" data-target="#myModal${item.id}" onclick="new PNotify({
                                title: 'Dados Anvisa',
                                text: 'Dados Anvisa',
                                type: 'sucess',
                                styling: 'bootstrap3'
                            });">
                    <i class="glyphicon glyphicon-info-sign"></i> Dados Anvisa
                  </a>
                                                            
                `

]);

    // Adiciona as novas linhas ao DataTables e desenha a tabela
    $('#datatable').DataTable().rows.add(novosDados).draw();

 // Retorna à página atual após a exclusão

  $('#datatable').DataTable().page(paginaAtual).draw('page');
  

// Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([2,3,5,8 ]).every(function (d) {
        var column = this;
      var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
          
    // Adicione os modais
    data.forEach(item => {
      $('body').append(`
        <div class="modal fade" id="myModal${item.id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Alerta </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
  <center>${item.titulo}</center>
                <div class="modal-body" id="modalBody${item.id}">
                      <strong>Alerta:</strong> ${item.alert}<br>
     <strong>Resumo:</strong> ${item.resum}<br>
     <strong>Identificação do produto ou caso:</strong> ${item.identification}<br>
     <strong>Problema:</strong> ${item.problem}<br>
     <strong>Ação:</strong> ${item.action}<br>
     <strong>Histórico:</strong> ${item.history}<br>
     <strong>Recomendações:</strong> ${item.recomentation}<br>


                  <!-- Conteúdo dinâmico será carregado aqui -->
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  <!-- Adicione outros botões conforme necessário -->
                </div>
              </div>
            </div>
          </div>
    `);
      $(`#myModal${item.id}`).on('show.bs.modal', function () {
        // Carrega o conteúdo do link anvisa-viwer?id=item.id usando jQuery.ajax
        $.ajax({
          url: `anvisa-viwer.php?id=${item.id}`,
          type: 'GET',
          success: function(response) {
            console.log('Conteúdo retornado:', response); // Adicione esta linha para verificar o conteúdo retornado
            // Atualiza o conteúdo do modal com a resposta AJAX
            $(`#modalBody${item.id}`).html(response);
            
            // Abre o modal
            $(`#myModal${item.id}`).modal('show');
          },
          error: function(xhr, status, error) {
            console.error('Erro na requisição AJAX:', status, error);
          }
        });
      });
      document.addEventListener('DOMContentLoaded', function() {
        // Adiciona um ouvinte de evento de clique ao link da Anvisa
        document.getElementById('anvisaLink').addEventListener('click', function(event) {
          // Impede o comportamento padrão do link
          event.preventDefault();
          
          // Obtém o id da Anvisa do atributo de dados
          var anvisaId = this.getAttribute('data-anvisa-id');
          
          // Carrega o conteúdo da página usando AJAX
          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
              // Atualiza o conteúdo do modal com a resposta AJAX
              document.getElementById('ajaxContent').innerHTML = xhr.responseText;
              
              // Abre o modal
              var myModal = new bootstrap.Modal(document.getElementById('anvisaModal'));
              myModal.show();
            }
          };
          xhr.open('GET', 'report-equipament-family-anvisa?id=' + anvisaId, true);
          xhr.send();
        });
      });
    });
    });
    
    // Suponha que a variável 'table' seja a instância da DataTable
  

});


 
</script>
