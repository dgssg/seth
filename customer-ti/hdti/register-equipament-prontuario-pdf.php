<?php
  //============================================================+
  // File name   : example_001.php
  // Begin       : 2008-03-04
  // Last Update : 2013-05-14
  //
  // Description : Example 001 for TCPDF class
  //               Default Header and Footer
  //
  // Author: Nicola Asuni
  //
  // (c) Copyright:
  //               Nicola Asuni
  //               Tecnick.com LTD
  //               www.tecnick.com
  //               info@tecnick.com
  //============================================================+
  
  /**
  * Creates an example PDF TEST document using TCPDF
  * @package com.tecnick.tcpdf
  * @abstract TCPDF - Example: Default Header and Footer
  * @author Nicola Asuni
  * @since 2008-03-04
  */
  
  // Include the main TCPDF library (search for installation path).
  require_once('../../framework/TCPDF/tcpdf.php');
  
  // create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('MK Sistemas Biomedicos');
  $pdf->SetTitle('Sistema SETH');
  $pdf->SetSubject('Ordem de Serviço');
  $pdf->SetKeywords('Ordem de Serviço');
  
  // set default header data
  $pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH,'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
  $pdf->setFooterData(array(0,64,0), array(0,64,128));
  
  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  
  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  
  // set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  
  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  
  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
  }
  
  // ---------------------------------------------------------
  
  // set default font subsetting mode
  $pdf->setFontSubsetting(true);
  
  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage();
  
  // set text shadow effect
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
  
  include("database/database.php");// remover ../
  
$codigoget = ($_GET["equipamento"]);
$equipamento_telegram = ($_GET["equipamento_telegram"]);
if($codigoget == null){
  $codigoget = 0;
}
  if($equipamento_telegram == null){
    $equipamento_telegram = 0;
  }
date_default_timezone_set('America/Sao_Paulo');
$data= date(DATE_RFC2822);
  $carimbo_tempo = strtotime($data);
  $data_now=date('Y-m-d',$carimbo_tempo);

$query = "SELECT equipamento.id,equipamento_familia.id_equipamento_grupo,equipamento.baixa,instituicao.logo,instituicao_localizacao.nome,instituicao_area.nome,instituicao.instituicao,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento_familia.anvisa,equipamento_familia.fabricante,equipamento_familia.img,equipamento.obs,equipamento.data_instal,equipamento.data_fab,equipamento.patrimonio,equipamento.serie,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao ON instituicao.id = instituicao_localizacao.id_unidade INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area WHERE equipamento.id = $codigoget OR equipamento.codigo = '$equipamento_telegram'";


//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($codigoget,$id_equipamento_grupo,$baixa,$logo,$setor,$area,$instituicao,$data_val,$nf,$data_fab,$data_instal,$anvisa,$fab,$img,$obs,$data_fab,$data_fab,$patrimonio,$serie,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
  while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }
}

$query = "SELECT* FROM equipament_disable WHERE id_equipamento like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($equipament_disable,$id_equipamento,$data_disable,$id_disable,$des_disable,$file,$upgrade_disable,$reg_date_disable);
    while ($stmt->fetch()) {
        //printf("%s, %s\n", $solicitante, $equipamento);
    }
    
    
}
  if($id_disable !== null){
$sql = " SELECT id, nome FROM equipament_disable_status WHERE id = $id_disable ";


if ($stmt = $conn->prepare($sql)) {
    $stmt->execute();
    $stmt->bind_result($id_equipament_disable_status,$nome_equipament_disable_status);
    while ($stmt->fetch()) {
        
    }
}
  }

$sql = " SELECT codigo, titulo FROM documentation_pop WHERE id_equipamento_grupo = $id_equipamento_grupo ";


if ($stmt = $conn->prepare($sql)) {
    $stmt->execute();
    $stmt->bind_result($documentation_pop_codigo,$documentation_pop_titulo);
    while ($stmt->fetch()) {
        
    }
}

  $rotina_query=0;
  $tabela = '';
  $query = "SELECT maintenance_routine.periodicidade_after,maintenance_routine.data_after,maintenance_routine.time_after,maintenance_routine.id_pop,maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo,maintenance_procedures.name,maintenance_procedures.codigo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_before OR maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_after WHERE equipamento.id = '$codigoget'";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($periodicidade_after,$data_after,$time_after,$id_pop,$id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo,$procedimento_2,$codigo_p2);
   while ($stmt->fetch()) { 
 
 $tabela .= '<tr>'; 
 $tabela .= '<td width="50%"><strong> Rotina:</strong> ' . $rotina . '</td>'; 
 $tabela .= '<td><strong>Área:</strong> ' . $setor . '</td>'; 
 $tabela .= '</tr>'; 

 $tabela .= '<tr>'; 
 $tabela .= '<td><strong>Procedimento :</strong> ' . $procedimento_1 . '</td>'; 
 $tabela .= '<td><strong>Código:</strong> ' . $codigo_p . '</td>'; 
 $tabela .= '</tr>'; 

 $tabela .= '<tr>'; 
 $tabela .= '<td><strong>Início:</strong> ' . $data_start . '</td>'; 
 $tabela .= '<td><strong>Periodicidade:</strong> ' . (($rotina_query == $rotina) ? $periodicidade_after : $periodicidade) . ' (dias)</td>'; 
 $tabela .= '</tr>'; 

 $tabela .= '<tr>'; 
 $tabela .= '<td><strong>Cadastro:</strong> ' . $reg_date . '</td>'; 
 $tabela .= '<td><strong>Atualização:</strong> ' . $upgrade . '</td>'; 
 $tabela .= '</tr>'; 

 $row = $row + 1; 
 } 
  }
 

$tabela1 = '';
$query = "SELECT equipament_location.data_location,equipament_location.reg_date,equipament_location.obs,  instituicao_localizacao.nome,instituicao_area.nome FROM equipament_location  INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipament_location.id_localizacao INNER JOIN instituicao_area  ON instituicao_area.id = instituicao_localizacao.id_area WHERE equipament_location.id_equipamento like
'$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($data_location,$data,$obs,$localizacao,$area);
//  while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
// }
//}
  while ($stmt->fetch()) {
    $tabela1 .= '<tr>';
    $tabela1 .= '<th scope="row">' . $row . '</th>';
    $tabela1 .= '<td>' . $area . '</td>';
    $tabela1 .= '<td>' . $localizacao . '</td>';
    $tabela1 .= '<td>' . $obs . '</td>';
    $tabela1 .= '<td>' . $data_location . '</td>';
    $tabela1 .= '<td>' . $data . '</td>';
    $tabela1 .= '</tr>';
    $row = $row + 1;
  }
}

 $tabela2 = '';

$query = "SELECT os.defeito,os.date_end,os.date_start,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os INNER JOIN instituicao ON instituicao.id = os.id_unidade INNER JOIN usuario on usuario.id = os.id_usuario INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area INNER JOIN os_status on os_status.id = os.id_status INNER JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento INNER JOIN os_carimbo ON os_carimbo.id = os.id_carimbo INNER JOIN os_workflow on os_workflow.id = os.id_workflow INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia WHERE os.id_equipamento like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($defeito,$date_end,$date_start,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);


//}
  while ($stmt->fetch()) {
    $tabela2 .= '<tr>';
    $tabela2 .= '<th scope="row">' . $row . '</th>';
    $tabela2 .= '<td>' . $id_os . '</td>';
    $tabela2 .= '<td>' . $reg_date . '</td>';
    $tabela2 .= '<td>' . $id_solicitacao . '</td>';
    $tabela2 .= '<td>' . $date_start . '</td>';
    $tabela2 .= '<td>' . $date_end . '</td>';
    $tabela2 .= '<td>' . $id_status . '</td>';
    $tabela2 .= '<td>' . $defeito . '</td>';
    $tabela2 .= '</tr>';
    $row = $row + 1;
  }
}
 
  $tabela3 = '';
$query = "SELECT maintenance_preventive.file,maintenance_preventive.id,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id_routine like '$rotina'";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id_anexo,$id,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
  while ($stmt->fetch()) {
    if($status == 1){ $status = "Aberto" ;}
    if($status == 2){ $status = "Atrasado";}
    if($status == 3){ $status = "Fechado";}
    if($status == 4){ $status = "Cancelado";}
    $tabela3 .= '<tr>';
    $tabela3 .= '<th scope="row">' . $row . '</th>';
    $tabela3 .= '<td>' . $programada . '</td>';
    $tabela3 .= '<td>' . $date_start_ms . '</td>';
    $tabela3 .= '<td>' . $date_end_ms . '</td>';
    $tabela3 .= '<td>' . $status . '</td>';
    $tabela3 .= '<td>' . $ms_upgrade . '</td>';
 
    $tabela3 .= '</tr>';
    $row = $row + 1;
  }
}
 

  
  $tabela4 = '';
$query = "SELECT calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM calibration INNER JOIN equipamento on equipamento.id = calibration.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia WHERE equipamento.id like '$codigoget'  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
//while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
//  }

    $new_date=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));
    
    if ( $new_date < $data ) {
      $ven = 1;
      
    }
  while ($stmt->fetch()) {
    if($ven ==0) { $ven = "Não";} if($ven ==1) { $ven = "Sim";} 
    if($status ==0) { $status = "Aprovado";} if($status ==1) { $status = "Reprovado";} 
    $tabela4 .= '<tr>';
    $tabela4 .= '<th scope="row">' . $row . '</th>';
    $tabela4 .= '<td>' . $data_start . '</td>';
    $tabela4 .= '<td>' . $val . 'Meses</td>';
    $tabela4 .= '<td>' . $ven . '</td>';
    $tabela4 .= '<td>' . $status . '</td>';
 
    $tabela4 .= '</tr>';
    $row = $row + 1;
  }
}
   

  $tabela5 = '';
$query="SELECT equipamento.id,equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo,obsolescence_year.yr,obsolescence_rooi.rooi,obsolescence.id, obsolescence.id_equipamento, obsolescence.id_year, obsolescence.id_c1, obsolescence.id_c2, obsolescence.id_c3, obsolescence.id_c4, obsolescence.id_c5, obsolescence.id_c6, obsolescence.id_c7, obsolescence.id_rooi, obsolescence.score, obsolescence.obs, obsolescence.vlr_market, obsolescence.vlr_purchases, obsolescence.obs_purchases, obsolescence.upgrade, obsolescence.reg_date FROM obsolescence INNER JOIN obsolescence_rooi on obsolescence_rooi.id = obsolescence.id_rooi INNER JOIN obsolescence_year on obsolescence_year.id = obsolescence.id_year INNER JOIN equipamento on equipamento.id = obsolescence.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia WHERE equipamento.id = $codigoget ";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($equipamento,$nome,$fabricante,$modelo,$yr,$rooi,$id, $id_equipamento, $id_year, $id_c1, $id_c2, $id_c3, $id_c4, $id_c5, $id_c6, $id_c7, $id_rooi, $score, $obs, $vlr_market, $vlr_purchases, $obs_purchases, $upgrade, $reg_date);
  while ($stmt->fetch()) {
    $tabela5 .= '<tr>';
    $tabela5 .= '<th scope="row">' . $row . '</th>';
    $tabela5 .= '<td>' . $yr . '</td>';
    $tabela5 .= '<td>' . $score . '</td>';
    $tabela5 .= '<td>' . $rooi . '</td>';
    $tabela5 .= '<td>' . $reg_date . '</td>';
    $tabela5 .= '<td>' . $date_end . '</td>';
    $tabela5 .= '<td>' . $upgrade . '</td>';
 
    $tabela5 .= '</tr>';
    $row = $row + 1;
  }
}
  

  $tabela6 = '';
$query="SELECT equipament_exit.id,fornecedor.empresa,equipament_exit_mov.nome, equipament_exit_reason.nome,equipament_exit.date_send, equipament_exit.date_return, equipament_exit_status.nome FROM equipament_exit INNER JOIN fornecedor on fornecedor.id = equipament_exit.id_fornecedor INNER JOIN equipament_exit_mov on equipament_exit_mov.id= equipament_exit.id_equipament_exit_mov INNER JOIN equipament_exit_reason on equipament_exit_reason.id = equipament_exit.id_equipament_exit_reason  INNER JOIN equipament_exit_status on equipament_exit_status.id = equipament_exit.id_equipament_exit_status WHERE equipament_exit.id_equipamento like
'$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$empresa,$mov,$reason,$send,$return,$status);
  while ($stmt->fetch()) {
    $tabela6 .= '<tr>';
    $tabela6 .= '<th scope="row">' . $row . '</th>';
    $tabela6 .= '<td>' . $empresa . '</td>';
    $tabela6 .= '<td>' . $mov . '</td>';
    $tabela6 .= '<td>' . $reason . '</td>';
    $tabela6 .= '<td>' . $send . '</td>';
    $tabela6 .= '<td>' . $return . '</td>';
    $tabela6 .= '<td>' . $status . '</td>';
    
    $tabela6 .= '</tr>';
    $row = $row + 1;
  }
}
  $tabela7 = '';
$query="SELECT maintenance_routine.id_equipamento,os.id_equipamento, technological_surveillance.mp,technological_surveillance.id, technological_surveillance.id_os, technological_surveillance.descricao,technological_surveillance_list.nome, technological_surveillance.status_tec,technological_surveillance.acao_tec,technological_surveillance.data_inicio, technological_surveillance.data_fim FROM technological_surveillance LEFT join technological_surveillance_list on technological_surveillance_list.id = technological_surveillance.id_technological LEFT JOIN maintenance_preventive on maintenance_preventive.id = technological_surveillance.id_os AND  technological_surveillance.mp = 1 LEFT JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine LEFT JOIN os ON os.id = technological_surveillance.id_os and technological_surveillance.mp = 0 WHERE maintenance_routine.id_equipamento = $codigoget or os.id_equipamento = $codigoget  ";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($equipamento1,$equipamento2,$tipo,$id,$id_os, $descricao, $risco,$status,$acao,$data_inicio,$data_fim);
  while ($stmt->fetch()) {
    if($tipo==0){ $tipo= "MC"; }  if($tipo==1){ $tipo= "MP";}
    
    $tabela6 .= '<tr>';
    $tabela6 .= '<th scope="row">' . $row . '</th>';
    $tabela6 .= '<td>' . $tipo . '</td>';
    $tabela6 .= '<td>' . $id_os . '</td>';
    $tabela6 .= '<td>' . $descricao . '</td>';
    $tabela6 .= '<td>' . $risco . '</td>';
    $tabela6 .= '<td>' . $status . '</td>';
    $tabela6 .= '<td>' . $acao . '</td>';
    $tabela6 .= '<td>' . $data_inicio . '</td>';
    $tabela6 .= '<td>' . $data_fim . '</td>';
    
    $tabela6 .= '</tr>';
    $row = $row + 1;
  }
}
 
  $tabela8 = '';
  $query="SELECT equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo,equipamento_familia.fabricante,alert.resum,alert.identification,alert.problem,alert.action,alert.history,alert.recomentation,alert.anexo,equipamento_grupo.nome,equipamento_familia.modelo,equipamento_familia.fabricante,alert.id, equipamento_familia.nome,alert.alert,alert.date_alert,alert.obs,alert.upgrade,alert.reg_date,alert.titulo FROM equipamento inner JOIN alert on alert.equipamento_familia = equipamento.id_equipamento_familia LEFT JOIN equipamento_familia ON equipamento_familia.id = alert.equipamento_familia LEFT JOIN equipamento_grupo ON equipamento_grupo.id = alert.equipamento_grupo WHERE equipamento.id = $codigoget  ";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($codigo,$familia,$modelo,$fabricante,$resum,$identification,$problem,$action,$history,$recomentation,$anexo,$equipamento_grupo,$modelo,$fabricante,$id, $equipamento_familia,$alert,$date_alert,$obs,$upgrade,$reg_date,$yr);
    
    while ($stmt->fetch()) {
      $tabela8 .= '<tr>';
      $tabela8 .= '<th scope="row">' . $row . '</th>';
      $tabela8 .= '<td>' . $codigo . '</td>';
      $tabela8 .= '<td>' . $equipamento_grupo . '</td>';
      $tabela8 .= '<td>' . $equipamento_familia . '</td>';
      $tabela8 .= '<td>' . $fabricante . '</td>';
      $tabela8 .= '<td>' . $modelo . '</td>';
      $tabela8 .= '<td>' . $alert . '</td>';
      $tabela8 .= '<td>' . $yr . '</td>';
      $tabela8 .= '<td>' . $date_alert . '</td>';
      $tabela8 .= '<td>' . $resum . '</td>';
      
      $tabela8 .= '</tr>';
      $row = $row + 1;
    }
  }

  $html = <<<EOD
  
 <div class="conteudo" style="min-height:900px; font-size:17px;">
				<table style="border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td align="center"><strong>Prontuario: $codigo</strong> </td> <td align="center"><strong><svg class="barcode"
																			            jsbarcode-format="auto"
																			            jsbarcode-value="$codigo"
																			            jsbarcode-textmargin="0"
																			            jsbarcode-fontoptions="bold"
																			            jsbarcode-width="2"
																			            jsbarcode-height="40"
																			            jsbarcode-displayValue="false"
																			             >
																			    </svg></strong></td>
</tr>

</tbody>
</table>
<p><b><center> <h2><small>Dados do equipamento:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td width="50%"><strong> Nome:</strong> $nome</td>
<td><strong>Modelo:</strong> $modelo</td>
</tr>
<tr>
<td><strong>Fabricante</strong> $fab</td>
<td><strong>Anvisa:</strong> $anvisa</td>
</tr>
<tr>
<td><strong> N&ordm; Serie:</strong> $serie</td>
<td><strong>Patrimonio:</strong> $patrimonio</td>
</tr>
<tr>
<td><strong> Codigo:</strong> $codigo</td>
<td><strong>NF:</strong> $nf</td>
</tr>
<tr>
<td><strong>Fabricação:</strong> $data_fab</td>
<td><strong>Garantia:</strong> $data_val</td>
</tr>
<tr>
<td><strong>Baixa:</strong> $baixa</td>
<td><strong>Data:</strong> $data_disable</td>
</tr>
<tr>
<td><strong>Motivo:</strong> $nome_equipament_disable_status</td>
<td><strong>Descrição:</strong> $des_disable</td>
</tr>
<tr>
<td><strong>Codigo POP:</strong> $documentation_pop_codigo</td>
<td><strong>POP:</strong> $documentation_pop_titulo</td>
</tr>

</tbody>
</table>
<p><b><center> <h2><small>Localiza&ccedil;&atilde;o Atual:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td width="50%"><strong> Data instalação:</strong> $data_instal</td>
<td><strong>Instituição:</strong> $instituicao</td>
</tr>
<tr>
<td><strong>Setor</strong> $area</td>
<td><strong>Area:</strong> $setor</td>
</tr>


</tbody>
</table>
 
<p><b><center> <h2><small>Rotina do Equipamento:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
$table  
</tbody>
</table>

 

  <p><b><center> <h2><small>Historico de localiza&ccedil;&atilde;o:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                              <th>Area</th>
                          <th>Localiza&ccedil;&atilde;o</th>

                           <th>Observa&ccedil;&atilde;o</th>
                           <th>Data</th>
                          <th>Registro</th>

                        </tr>
                      </thead>
                      <tbody>
                    $table1
                      </tbody>
                    </table>



 

  <p><b><center> <h2><small>Ordem de Serviço:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                              <th>OS</th>
                           <th>Registro</th>
                          <th>Solicitação</th>
                           <th>Data inicio</th>
                            <th>Data Fim</th>
                            <th>Status</th>
                             <th>Reparo</th>
                        </tr>
                      </thead>
                      <tbody>
                      $table2
                      </tbody>
                    </table>


 

                      <p><b><center> <h2><small>Manutenção Preventiva:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                             <th>Data Programada</th>

                          <th>Data Abertura</th>
                          <th>Data Termino</th>

                          <th>Status</th>
                          <th>Atualização</th>
                        </tr>
                      </thead>
                      <tbody>
                      $table3

                      </tbody>
                    </table>

 
                      <p><b><center> <h2><small>Calibração:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                            <th>Laudo</th>
                          <th>Realização</th>
                          <th>Validade</th>
                          <th>vencido</th>
                           <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      $table4

                      </tbody>
                    </table>



                      <p><b><center> <h2><small>Avaliação:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                            <th>Ano</th>
                          <th>Pontuação</th>
                          <th>ROOI</th>
                          <th>Data</th>
                          <th>Atualização</th>

                        </tr>
                      </thead>
                      <tbody>
                      $table5
                      </tbody>
                    </table>
 



                      <p><b><center> <h2><small>Movimentação Equipamento:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                        <th>#</th>
                    <th>Empresa</th>
                    <th>Movimentação</th>
                    <th>Motivo</th>
                    <th>Data envio</th>
                    <th>Data reotrno</th>
                    <th>status</th>

                        </tr>
                      </thead>
                      <tbody>
                      $table6
                      </tbody>
                    </table>
 



                      <p><b><center> <h2><small>Evento Sentinela:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                        <th>Alerta</th>
                          <th>Tipo</th>
                          <th>Nº</th>
                          <th>Descrição</th>
                          <th>Risco</th>
                         
                          <th>Status</th>
                          <th>Ação</th>
                          <th>Data inicio</th>
                           <th>Data Fim</th>
                        </tr>
                      </thead>
                      <tbody>
                      $table7
                      </tbody>
                    </table>
                  


                      <p><b><center> <h2><small>Alerta Anvisa:</small></h2> </center></b></p>

                 <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                        <th>Codigo</th>
                        
                        <th>Grupo</th>
                        <th>Familia</th>
                        <th>Fabricante</th>
                        <th>Modelo</th>
                        <th>Alerta</th>
                        <th>Titulo</th>
                        <th>Data</th>
                        <th>Resumo</th>
                        </tr>
                      </thead>
                      <tbody>
                        $table8

                      </tbody>
                    </table>

</div>
      

	 

	</div>

	</div>
	<!-- fim da div formulario -->

	<div class="row" style="padding-top: 15px">



    </div>

</div>
<!-- fim da div  view_content -->



 

            </div>
            <!--  Fim do container-->



<!-- Rodapé -->
<table width="100%" style="vertical-align: top;font-family:'Times New Roman',Times,serif; font-size: 11px;">
  <tr>
    <td align="center" colspan="2">
      <div style="padding-top: 20px;"><strong>Copyright © 2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados.</strong><br>www.mksistemasbiomedicos.com.br</div>
    </td>
  </tr>
  <tr>
    <td style="font-size: 9px" align="left">MK-01-ra-1.0</td>
    <td align="right"><!-- pagina --></td>
  </tr>
</table>
EOD;
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Prontuario.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+