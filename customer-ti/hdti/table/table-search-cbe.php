<?php
include("../database/database.php");
$query = "SELECT cbe.id, cbe.name,category.nome,cbe.codigo,cbe.reg_date,cbe.upgrade FROM cbe INNER JOIN category ON category.id = cbe.id_category WHERE cbe.trash = 1 order by cbe.id DESC ";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
