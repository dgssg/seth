<?php
	$codigoget = ($_GET["id"]);
	include("database/database.php");
	$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_mp_dropzone WHERE id_mp like '$codigoget'";
	$row=1;
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);
?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>#</th>
			<th>Titulo</th>
			<th>Registro</th>
			<th>Atualização</th>
			<th>Ação</th>
			
		</tr>
	</thead>
	<tbody>
		<?php  while ($stmt->fetch()) { ?>
		
		<tr>
			<th scope="row"><?php printf($row); ?></th>
			<td><?php printf($id); ?></td>
			<td><?php printf($titulo); ?></td>
			<td><?php printf($reg_date); ?></td>
			<td><?php printf($upgrade); ?></td>
			<td> <a class="btn btn-app"   href="dropzone/mp-dropzone/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
				title: 'Visualizar',
				text: 'Visualizar Anexo',
				type: 'info',
				styling: 'bootstrap3'
			});">
				<i class="fa fa-file"></i> Anexo
			</a>
				<a class="btn btn-app" onclick="excluirAnexo('<?php echo $id; ?>', '<?php echo $codigoget; ?>')">
					<i class="fa fa-trash"></i> Excluir
				</a>
			</td>
			
		</tr>
		<?php  $row=$row+1; }
		}   ?>
	</tbody>
</table>

