<?php

include("../database/database.php");

$codigoget = $_POST['id'];
$nome = $_POST['nome'];
$sobrenome = $_POST['sobrenome'];
$matricula = $_POST['matricula'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$cargo = $_POST['cargo'];
$id_nivel = $_POST['id_nivel'];
$api_app = $_POST['api_app'];
$api_telegram = $_POST['api_telegram'];
$api_whats = $_POST['api_whats'];
$api_line = $_POST['api_line'];
$login_acess = $_POST['login_acess'];
$login_acess=trim($login_acess);

$id_setor=$_POST['duallistbox_demo1'];
foreach($id_setor as $id_setor){
    $id_setor=trim($id_setor);
    if($id_setor==!""){
    $idcategoria=$id_setor.",".$idcategoria;
    }
}
$id_setor=substr($idcategoria, 0, -1);
// $id_setor= $idcategoria;

$stmt = $conn->prepare("UPDATE usuario SET login_acess = ? WHERE id= ?");
$stmt->bind_param("ss",$login_acess,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET sobrenome = ? WHERE id= ?");
$stmt->bind_param("ss",$sobrenome,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET matricula = ? WHERE id= ?");
$stmt->bind_param("ss",$matricula,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET email = ? WHERE id= ?");
$stmt->bind_param("ss",$email,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET telefone = ? WHERE id= ?");
$stmt->bind_param("ss",$telefone,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET cargo = ? WHERE id= ?");
$stmt->bind_param("ss",$cargo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET id_nivel = ? WHERE id= ?");
$stmt->bind_param("ss",$id_nivel,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET api_app = ? WHERE id= ?");
$stmt->bind_param("ss",$api_app,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET api_telegram = ? WHERE id= ?");
$stmt->bind_param("ss",$api_telegram,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET api_whats = ? WHERE id= ?");
$stmt->bind_param("ss",$api_whats,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET api_line = ? WHERE id= ?");
$stmt->bind_param("ss",$api_line,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE usuario SET id_setor = ? WHERE id= ?");
$stmt->bind_param("ss",$id_setor,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../register-account');
?>