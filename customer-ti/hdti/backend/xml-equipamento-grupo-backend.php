<?php
// Configurações do banco de dados
$host = 'localhost';
$db = 'seu_banco';
$user = 'seu_usuario';
$pass = 'sua_senha';

try {
    // Conexão com o banco de dados usando PDO
    $pdo = new PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Carregar o arquivo XML
    $xml = simplexml_load_file('caminho/para/o/seu_arquivo.xml');

    // Percorrer cada equipamento no XML e inserir no banco de dados
    foreach ($xml->equipamento as $equip) {
        // Preparar o SQL de inserção
        $stmt = $pdo->prepare("INSERT INTO equipamento_grupo 
            (nome, yr, id_class, id_categoria, id_mp_group, id_procedimento_1, id_procedimento_2, id_periodicidade, id_time, id_colaborador, id_pop, accessories)
            VALUES (:nome, :yr, :id_class, :id_categoria, :id_mp_group, :id_procedimento_1, :id_procedimento_2, :id_periodicidade, :id_time, :id_colaborador, :id_pop, :accessories)");

        // Vincular os parâmetros usando os valores do XML
        $stmt->bindParam(':nome', $equip->nome);
        $stmt->bindParam(':yr', $equip->yr);
        $stmt->bindParam(':id_class', $equip->id_class);
        $stmt->bindParam(':id_categoria', $equip->id_categoria);
        $stmt->bindParam(':id_mp_group', $equip->id_mp_group);
        $stmt->bindParam(':id_procedimento_1', $equip->id_procedimento_1);
        $stmt->bindParam(':id_procedimento_2', $equip->id_procedimento_2);
        $stmt->bindParam(':id_periodicidade', $equip->id_periodicidade);
        $stmt->bindParam(':id_time', $equip->id_time);
        $stmt->bindParam(':id_colaborador', $equip->id_colaborador);
        $stmt->bindParam(':id_pop', $equip->id_pop);
        $stmt->bindValue(':accessories', (int)$equip->accessories); // Convertendo para inteiro para garantir compatibilidade

        // Executar a inserção
        $stmt->execute();
    }

    echo "Importação concluída com sucesso!";

} catch (PDOException $e) {
    echo 'Erro ao importar dados: ' . $e->getMessage();
}
?>
