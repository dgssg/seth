<?php


include("../database/database.php");


$id_fhir = $_GET['id_fhir'];

$date_start = $_POST['date_start'];
$index_number = $_POST['percentage'];
$period = $_POST['percentage_applied'];
$obs = $_POST['obs'];


$stmt = $conn->prepare("INSERT INTO fhir_readjustment (id_fhir,date_start,index_number,period,obs) VALUES (?, ?, ?,?, ?)");
$stmt->bind_param("sssss",$id_fhir,$date_start,$index_number,$period,$obs);
$execval = $stmt->execute();
$stmt->close();


echo "<script>alert('Adicionado!');document.location='../fhir-edit?id=$id_fhir'</script>";
?>
