<?php

include("../database/database.php");

$codigoget= $_POST['id'];
$id_instituicao= $_POST['id_instituicao'];
$number_fhir= $_POST['number_fhir'];
$id_fhir_mod= $_POST['id_fhir_mod'];
$id_fhir_type= $_POST['id_fhir_type'];
$object= $_POST['object'];
$id_fhir_status= $_POST['id_fhir_status'];
$counterparty_nome= $_POST['counterparty_nome'];
$counterparty_telephone= $_POST['counterparty_telephone'];
$counterparty_email= $_POST['counterparty_email'];
$manufacture= $_POST['manufacture'];
$cnpj= $_POST['cnpj'];
$cep= $_POST['cep'];
$adress= $_POST['adress'];
$city= $_POST['city'];
$state= $_POST['state'];
$ibge= $_POST['ibge'];
$district= $_POST['district'];
$nome= $_POST['nome'];
$telephone= $_POST['telephone'];
$email= $_POST['email'];





$stmt = $conn->prepare("UPDATE fhir SET id_instituicao = ? WHERE id= ?");
$stmt->bind_param("ss",$id_instituicao,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET number_fhir = ? WHERE id= ?");
$stmt->bind_param("ss",$number_fhir,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET id_fhir_mod = ? WHERE id= ?");
$stmt->bind_param("ss",$id_fhir_mod,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET id_fhir_type = ? WHERE id= ?");
$stmt->bind_param("ss",$id_fhir_type,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET object = ? WHERE id= ?");
$stmt->bind_param("ss",$object,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET id_fhir_status = ? WHERE id= ?");
$stmt->bind_param("ss",$id_fhir_status,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE fhir SET counterparty_nome = ? WHERE id= ?");
$stmt->bind_param("ss",$counterparty_nome,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET counterparty_telephone = ? WHERE id= ?");
$stmt->bind_param("ss",$counterparty_telephone,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET counterparty_email = ? WHERE id= ?");
$stmt->bind_param("ss",$counterparty_email,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET manufacture = ? WHERE id= ?");
$stmt->bind_param("ss",$manufacture,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET cnpj = ? WHERE id= ?");
$stmt->bind_param("ss",$cnpj,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET cep = ? WHERE id= ?");
$stmt->bind_param("ss",$cep,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET adress = ? WHERE id= ?");
$stmt->bind_param("ss",$adress,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE fhir SET city = ? WHERE id= ?");
$stmt->bind_param("ss",$city,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET state = ? WHERE id= ?");
$stmt->bind_param("ss",$state,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET ibge = ? WHERE id= ?");
$stmt->bind_param("ss",$ibge,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET district = ? WHERE id= ?");
$stmt->bind_param("ss",$district,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET telephone = ? WHERE id= ?");
$stmt->bind_param("ss",$telephone,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE fhir SET email = ? WHERE id= ?");
$stmt->bind_param("ss",$email,$codigoget);
$execval = $stmt->execute();





echo "<script>alert('Atualizado!');document.location='../fhir-edit?id=$codigoget'</script>";

?>
