<?php


include("../database/database.php");


$id_telemedicine = $_GET['id_telemedicine'];

$titulo = $_POST['titulo'];
$anexo=$_COOKIE['anexo'];

$stmt = $conn->prepare("INSERT INTO telemedicine_dropbox (id_telemedicine,title,file) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$id_telemedicine,$titulo,$anexo);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
echo "<script>alert('Adicionado!');document.location='../telemedicine-edit?id=$id_telemedicine'</script>";
?>
