 <?php

include("../database/database.php");

$codigoget= $_GET['id'];
$parameter= $_GET['parameter'];
$nome= $_POST['nome'];
$unidade= $_POST['unidade'];
$d_1= $_POST['d_1'];
$d_2= $_POST['d_2'];
$erro= $_POST['erro'];
$ve1= $_POST['ve1'];
$ve2= $_POST['ve2'];
$ve3= $_POST['ve3'];

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET ve1 = ? WHERE id= ?");
$stmt->bind_param("ss",$ve1,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET ve2 = ? WHERE id= ?");
$stmt->bind_param("ss",$ve2,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET ve3 = ? WHERE id= ?");
$stmt->bind_param("ss",$ve3,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET unidade = ? WHERE id= ?");
$stmt->bind_param("ss",$unidade,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET d_1 = ? WHERE id= ?");
$stmt->bind_param("ss",$d_1,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET d_2 = ? WHERE id= ?");
$stmt->bind_param("ss",$d_2,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_dados_parameter SET erro = ? WHERE id= ?");
$stmt->bind_param("ss",$erro,$codigoget);
$execval = $stmt->execute();

echo "<script>alert('Atualizado!');document.location='../calibration-parameter-dados-add?dados=$parameter'</script>";

?>
