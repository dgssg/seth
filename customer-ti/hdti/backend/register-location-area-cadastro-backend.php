<?php
include("../database/database.php");

$instituicao = $_POST['instituicao'];
$custo = $_POST['custo'];
$nome = $_POST['nome'];
$codigo = $_POST['codigo'];
// Ao receber o POST
$auto_1 = isset($_POST['auto_1_checkbox']) ? $_POST['auto_1_checkbox'] : '1'; // '0' se marcado, '1' se não marcado

// Agora você pode usar $auto_2 normalmente, sabendo que ele será '0' ou '1'
$nome_area = $_POST['nome_area'];
// Ao receber o POST
$auto_2 = isset($_POST['auto_2_checkbox']) ? $_POST['auto_2_checkbox'] : '1'; // '0' se marcado, '1' se não marcado

// Agora você pode usar $auto_2 normalmente, sabendo que ele será '0' ou '1'
$quarterly = $_POST['equipamento'];

// Primeira inserção
$stmt = $conn->prepare("INSERT INTO instituicao_area (id_unidade, custo, nome, codigo) VALUES (?, ?, ?, ?)");
$stmt->bind_param("ssss", $instituicao, $custo, $nome, $codigo);
$stmt->execute();
$last_id = $conn->insert_id;
$stmt->close();

// Segunda inserção, se auto_1 for 0
if($auto_1 == 0) {
    $stmt = $conn->prepare("INSERT INTO instituicao_localizacao (id_unidade, id_area, nome) VALUES (?, ?, ?)");
    $stmt->bind_param("sss", $instituicao, $last_id, $nome_area);
    $stmt->execute();
    $last_id_1 = $conn->insert_id;
    $stmt->close();
}

// Terceira inserção, se auto_2 for 0
if($auto_2 == 0) {
    date_default_timezone_set('America/Sao_Paulo');
    $date = date(DATE_RFC2822);
    $default = "Equipamento cadastrado no sistema em $date";
    $parametro = 0;

    // Certifique-se de que $quarterly é um array
    if (is_array($quarterly) && count($quarterly) > 0) {
        foreach ($quarterly as $idunidade) {
            // Inserir dados na tabela equipamento
               $sql = "SELECT equipamento_familia.id, equipamento_familia.nome, equipamento_familia.fabricante, equipamento_familia.modelo
                        FROM equipamento_familia
                        INNER JOIN equipamento_grupo ON equipamento_grupo.id = equipamento_familia.id_equipamento_grupo
                        WHERE equipamento_familia.id = $idunidade ";

                if ($stmt = $conn->prepare($sql)) {
                    $stmt->execute();
                    $stmt->bind_result($id, $familia, $fabricante, $modelo);
                    while ($stmt->fetch()) {
                    
                    }
                }
                $stmt->close();
            
            $codigo = "$familia - $nome";
            $stmt = $conn->prepare("INSERT INTO equipamento (id_equipamento_familia,codigo, id_instituicao_localizacao, ativo, duplicidade) VALUES (?,?, ?, ?, ?)");
            $stmt->bind_param("sssss", $idunidade,$codigo, $last_id_1, $parametro, $parametro);
            $stmt->execute();
            $last_id_2 = $conn->insert_id;
            $stmt->close();

            // Inserir dados na tabela equipament_location
            $stmt = $conn->prepare("INSERT INTO equipament_location (id_equipamento, id_localizacao, obs) VALUES (?, ?, ?)");
            $stmt->bind_param("sss", $last_id_2, $last_id_1, $default);
            $stmt->execute();
            $stmt->close();
        }
    } else {
        // Se não houver itens selecionados no select múltiplo
       // echo "Nenhum equipamento selecionado.";
    }
}

header('Location: ../register-location-area');
exit();
?>
