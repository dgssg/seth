<?php


include("../database/database.php");


$id_dicom = $_GET['id_dicom'];

$titulo = $_POST['titulo'];
$anexo=$_COOKIE['anexo'];

$stmt = $conn->prepare("INSERT INTO dicom_dropbox (id_dicom,title,file) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$id_dicom,$titulo,$anexo);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
echo "<script>alert('Adicionado!');document.location='../dicom-edit?id=$id_dicom'</script>";
?>
