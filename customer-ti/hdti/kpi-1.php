<?php

  date_default_timezone_set('America/Sao_Paulo');
 
include("database/database.php");
$tnd="00:00:00"; 
$os_count=0;
$date_start_cm_2="0000-00-01";
$familia="Aparelho de Anestesia";
$status_1=0;
$status_2=0;
$status_3=0;
$status_4=0;
$status_5=0;
$status_6=0;
$query = "SELECT os.id_status,os.date_start, os.date_end, os.time, equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome FROM  os INNER JOIN equipamento on equipamento.id = os.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($status,$date_start_cm,$date_end_cm,$time_cm,$familia_cm,$modelo_cm,$fabricante_cm,$grupo_cm);
    


//header('Location: ../report-report-mc');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
 
 <H1> Ordem de Serviço</H1>                 
                    <div id="dv">
<table id="tblExport" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>Status</th>
                         <th>Data Inicio</th>
                         <th>Data Final</th>
                          <th>Tempo</th>
                          <th>Familia</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Grupo</th>
                          
                         
                
                        </tr>
                      </thead>


                      <tbody>
                        <?php   while ($stmt->fetch()) {   $os_count=$os_count+1; ?>
                        
                        <tr>
                             <td ><?php 
                             if($status==1){$status_1=$status_1+1; }
                             if($status==2){$status_2=$status_2+1; }
                             if($status==3){$status_3=$status_3+1; }
                             if($status==4){$status_4=$status_4+1; }
                             if($status==5){$status_5=$status_5+1; }
                             if($status==6){$status_6=$status_6+1; }
                             printf($status); ?> </td>
                             <?php 
                         
                    $date_cm=date('d',strtotime($date_end_cm.'-'.$date_start_cm_2));
                          
                    $date_cm_count=$date_cm_count+$date_cm;
                             
                            ?>
                          <td ><?php printf($date_start_cm); ?>  -  <?php printf($date_cm); ?></td>
                          <td ><?php printf($date_end_cm); ?> </td>
                          <td ><?php 
                          $date_start_cm_2=$date_start_cm;
                            $v1_cm = explode(':', $tnd);
                            $v2_cm = explode(':', $time_cm);
                            
                            // Obtenha as partes de horas, minutos e segundos como números inteiros
                            $v_h_cm1 = (int)$v1_cm[0];
                            $v_h_cm2 = (int)$v2_cm[0];
                            
                            $v_m_cm1 = (int)$v1_cm[1];
                            $v_m_cm2 = (int)$v2_cm[1];
                            
                            $v_s_cm1 = (int)$v1_cm[2];
                            $v_s_cm2 = (int)$v2_cm[2];
                            
                            // Realize a adição considerando o excedente
                            $v_s_cm = ($v_s_cm1 + $v_s_cm2) % 60; // Segundos
                            $v_m_cm = $v_m_cm1 + $v_m_cm2 + floor(($v_s_cm1 + $v_s_cm2) / 60); // Minutos
                            $v_h_cm = $v_h_cm1 + $v_h_cm2 + floor(($v_m_cm1 + $v_m_cm2) / 60); // Horas
                            
                            // Formate o resultado
                            $tnd = sprintf('%02d:%02d:%02d', $v_h_cm, $v_m_cm, $v_s_cm);

                          
                           printf($time_cm);  printf(" - "); printf($v_h_cm); printf(":");  printf($v_m_cm);   printf(":");  printf($v_s_cm); ?> </td>
                          <td ><?php if($familia_cm == $familia){ 
                              $familia_count=$familia_count+1;
                          }
                          printf($familia_cm); ?> </td>
                          <td ><?php printf($modelo_cm); ?> </td>
                           <td ><?php printf($fabricante_cm); ?> </td>
                          <td ><?php printf($grupo_cm); ?> </td>
                          
                          
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                    </div>
                     <!-- jQuery 2.1.4 -->
    <script src="../../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../../framework/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="jquery.btechco.excelexport.js"></script>
<script src="jquery.base64.js"></script>
<script>
    $(document).ready(function () {
        $("#btnExport").click(function () {
            $("#tblExport").btechco_excelexport({
                containerid: "tblExport"
               , datatype: $datatype.Table
               , filename: 'sample'
            });
        });
    });
</script>
	</body>
 
 </html>
 
 <?php
$rt="00:00:00"; 
$query = "SELECT maintenance_preventive.id_routine, maintenance_preventive.date_mp_start, maintenance_preventive.date_mp_end, maintenance_preventive.time_mp, maintenance_routine.id_equipamento,equipamento_familia.nome, equipamento_familia.modelo, equipamento_familia.fabricante, equipamento_grupo.nome FROM maintenance_preventive INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine INNER JOIN equipamento on equipamento.id = maintenance_routine.id_equipamento INNER JOIN equipamento_familia ON  equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($rotina,$date_start_sm,$date_end_sm,$time_sm,$equipamento,$familia_sm,$modelo_sm,$fabricante_sm,$grupo_sm);
    


//header('Location: ../report-report-mc');
?>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="icon" href="../../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../../framework/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../framework/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
 
 <H1> Preventiva</H1>                 
                    <div id="dv">
<table id="tblExport" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                         <th>Data Inicio</th>
                         <th>Data Final</th>
                          <th>Tempo</th>
                          <th>Familia</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Grupo</th>
                          
                         
                
                        </tr>
                      </thead>


                      <tbody>
                        <?php   while ($stmt->fetch()) {  ?>
                        <tr>
                           
                          <td ><?php printf($date_start_sm); ?> </td>
                          <td ><?php printf($date_end_sm); ?> </td>
                          
                           <td ><?php 
                          
                          $v1_sm = explode(':', $rt);
                          $v2_sm = explode(':', $time_sm);
                          $v_h_sm=$v1_sm[0]+$v2_sm[0];
                          $v_m_sm=$v1_sm[1]+$v2_sm[1];
                          $v_s_sm=$v1_sm[2]+$v2_sm[2];
                         
                          $rt=$v_h_sm.":".$v_m_sm.":".$v_s_sm;
                          
                           printf($time_sm);  printf(" - "); printf($v_h_sm); printf(":");  printf($v_m_sm);   printf(":");  printf($v_s_sm); ?> </td>
                          <td ><?php printf($familia_sm); ?> </td>
                          <td ><?php printf($modelo_sm); ?> </td>
                           <td ><?php printf($fabricante_sm); ?> </td>
                          <td ><?php printf($grupo_sm); ?> </td>
                          
                          
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                    </div>
                     <!-- jQuery 2.1.4 -->
    <script src="../../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../../framework/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="jquery.btechco.excelexport.js"></script>
<script src="jquery.base64.js"></script>
<script>
    $(document).ready(function () {
        $("#btnExport").click(function () {
            $("#tblExport").btechco_excelexport({
                containerid: "tblExport"
               , datatype: $datatype.Table
               , filename: 'sample'
            });
        });
    });
</script>
	</body>
 
 </html>
 <?
  $vf = explode(':', $tnd);
   $v_h_f=$vf[0];
   $v_m_f=$vf[1];
   $v_s_f=$vf[2];
   if($v_s_f >= 60){
       $v1=$v_s_f%60;
       $v2=$v_s_f/60;
       $v_s_f=$v1;
       $v_m_f=$v_m_f+$v2;
   }
    if($v_m_f >= 60){
       $v3=$v_m_f%60;
       $v4=$v_m_f/60;
       $v_m_f=$v3;
       $v_h_f=$v_h_f+$v4;
   }
$tnd=$v_h_f.":".$v_m_f.":".$v_s_f;
$tnd=($v_h_f*60)+$v_m_f;

$vs = explode(':', $rt);
   $v_h_s=$vs[0];
   $v_m_s=$vs[1];
   $v_s_s=$vs[2];
   if($v_s_s >= 60){
       $v11=$v_s_s%60;
       $v22=$v_s_s/60;
       $v_s_s=$v11;
       $v_m_s=$v_m_s+$v22;
   }
    if($v_m_s >= 60){
       $v33=$v_m_s%60;
       $v44=$v_m_s/60;
       $v_m_s=$v33;
       $v_h_s=$v_h_s+$v44;
   }
$rt=$v_h_s.":".$v_m_s.":".$v_s_s;
 //$rt=($v_h_s*60)+$v_m_s;
  ?>
  </hr>
<h1>KPI-1</h1>  
 <br> 
<h1>Tnd = <?php printf($tnd); ?></h1>
<br>
<h1>Rt = <?php printf($rt); ?></h1>
<br>
<h1>TDown (%)= <?php $tdown=(($tnd/$rt)*100); printf($tdown); ?></h1>   
<br>
<br>
 </hr>
<h1>KPI-2</h1>
<br>
<h1>Td = <?php $td=$rt-$tnd; printf($td); ?></h1>
<br>
<h1>Rt = <?php printf($rt); ?></h1>
<br>
<h1>TUptime (%)= <?php $tuptime=(($td/$rt)*100); printf($tuptime); ?></h1>  
<br>
<br>
 </hr>
<h1>KPI-3</h1>
<br> 
<h1>Tf = <?php $tf=$tnd; printf($tf); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>MTTR (Minutos)= <?php $mttr=($tf/$tnc); printf($mttr); ?></h1>   
<br>
<br>
 </hr>
<h1>KPI-4</h1>
<br> 
<h1>Td = <?php $tf=$date_cm_count; printf($tf); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>MTBF (Dias)= <?php $mtbf=($tf/$tnc); printf($mtbf); ?></h1>   
<br>
<br>
</hr>
<h1>KPI-5</h1>
<br> 
<h1>Ncmi = <?php $ncmi=$familia_count; printf($ncmi); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Ratio (%)= <?php $ratio=($ncmi/$tnc)*100; printf($ratio); ?></h1>   
<br>
<br>
</hr> 
<h1>KPI-6</h1>
<br> 
<h1>Ncmi-1 = <?php  printf($status_1); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Grf-1 (%) = <?php $grf1=($status_1/$tnc)*100; printf($grf1); ?></h1>   
<br>
<br>
<h1>Ncmi-2 = <?php  printf($status_2); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Grf-2 (%) = <?php $grf2=($status_2/$tnc)*100; printf($grf2); ?></h1>   
<br>
<br>
<h1>Ncmi-3 = <?php  printf($status_3); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Grf-3 (%) = <?php $grf3=($status_3/$tnc)*100; printf($grf3); ?></h1>   
<br>
<br>
<h1>Ncmi-4 = <?php  printf($status_4); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Grf-4 (%)= <?php $grf4=($status_4/$tnc)*100; printf($grf4); ?></h1>   
<br>
<br>
<h1>Ncmi-5 = <?php  printf($status_5); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Grf-5 (%)= <?php $grf5=($status_5/$tnc)*100; printf($grf5); ?></h1>   
<br>
<br>
<h1>Ncmi-6 = <?php  printf($status_6); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Grf-6 (%)= <?php $grf6=($status_6/$tnc)*100; printf($grf6); ?></h1>   
<br>
<br>
<h1>Grf-T (%)= <?php $grft=($grf1+$grf2+$grf3+$grf4+$grf5+$grf6); printf($grft); ?></h1>   
</hr> 

<h1>KPI-7</h1>
<br> 
<h1>Ncmi-1 = <?php  printf($status_1); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Afr-1 (%) = <?php $Afr1=($status_1/$tnc)*100; printf($Afr1); ?></h1>   
<br>
<br>
<h1>Ncmi-2 = <?php  printf($status_2); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Afr-2 (%) = <?php $Afr2=($status_2/$tnc)*100; printf($Afr2); ?></h1>   
<br>
<br>
<h1>Ncmi-3 = <?php  printf($status_3); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Afr-3 (%) = <?php $Afr3=($status_3/$tnc)*100; printf($Afr3); ?></h1>   
<br>
<br>
<h1>Ncmi-4 = <?php  printf($status_4); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Afr-4 (%)= <?php $Afr4=($status_4/$tnc)*100; printf($Afr4); ?></h1>   
<br>
<br>
<h1>Ncmi-5 = <?php  printf($status_5); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Afr-5 (%)= <?php $Afr5=($status_5/$tnc)*100; printf($Afr5); ?></h1>   
<br>
<br>
<h1>Ncmi-6 = <?php  printf($status_6); ?></h1>
<br>
<h1>Ncm = <?php $tnc=$os_count; printf($tnc); ?></h1>
<br>
<h1>Afr-6 (%)= <?php $Afr6=($status_6/$tnc)*100; printf($grf6); ?></h1>   
<br>
<br>
<h1>Afr-T (%)= <?php $Afrt=($grf1+$grf2+$grf3+$grf4+$grf5+$grf6); printf($Afrt); ?></h1>   
<br> 
</hr> 

<h1>KPI-8</h1>