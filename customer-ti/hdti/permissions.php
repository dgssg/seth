<?php
require 'auth.php';

function checkPermission($requiredLevel) {
    $levels = ['1', '2', '3', '4','5','6'];
    if (array_search(USER_LEVEL, $levels) < array_search($requiredLevel, $levels)) {
        die("Acesso negado. Permissões insuficientes.");
    }
}
?>
