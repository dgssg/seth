<?php
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
$conn = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_ti_hdti");
$conn_control= mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_ti_hdti");

$key = "hdti";
$mod="ti";
$id_status=0;
$procedure=1;

$query = "SELECT equipamento.data_calibration_end,maintenance_control.procedure_mp,maintenance_control.data_after,maintenance_control.id_routine,maintenance_control.data_start ,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_control on maintenance_control.id_routine =  maintenance_routine.id WHERE maintenance_control.data_after <= '$today ' and maintenance_control.status = 0 and maintenance_routine.habilitado = 0 ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($data_calibration_end,$procedure,$data_after,$routine_control,$date_control,$instituicao,$area,$setor,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
    while ($stmt->fetch()) {
        $query_control = "SELECT maintenance_routine.id_maintenance_procedures_after, maintenance_control.data_after,maintenance_control.id_routine,maintenance_control.data_start ,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN maintenance_control on maintenance_control.id_routine =  maintenance_routine.id WHERE maintenance_control.id_routine = $routine_control ";
        if ($stmt_control = $conn_control->prepare($query_control)) {
          $stmt_control->execute();
          $stmt_control->bind_result($id_maintenance_procedures_after,$data_after,$routine_control,$date_control,$instituicao,$area,$setor,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
          while ($stmt_control->fetch()) {
          }
        }
        $date=$data_start;

if($data_calibration_end != ""){
  $data_calibration_end = date('Y-m', strtotime($data_calibration_end));
  $data_after_calibration = date('Y-m', strtotime($data_after));

  if($data_calibration_end <= $data_after_calibration){
    if ($id_maintenance_procedures_after != "") {
    $procedure=2;
    }

  }
}

$data_after_cron = $data_after;


if($periodicidade==365){

    $data_after=date('Y-m-d', strtotime($data_after.' + 12 months'));
  
  }
  
  if($periodicidade==180){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 6 months'));
  
  }
  
  if($periodicidade==30){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 1 months'));
  
  }
  
  if($periodicidade==1){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 1 days'));
  
  }
  
  if($periodicidade==5){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 1 weekday'));
  
    
  }
  
  if($periodicidade==7){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 7 days'));
  
  
  }
  
  if($periodicidade==14){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 14 days'));
  
  
  }
  
  if($periodicidade==21){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 21 days'));
  
  
  }
  
  if($periodicidade==28){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 28 days'));
  
  
  }
  
  if($periodicidade==60){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 2 months'));
  
  
  }
  
  if($periodicidade==90){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 3 months'));
  
  
  }
  
  if($periodicidade==120){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 4 months'));
  
  
  }
  
  if($periodicidade==730){
  
    $data_after=date('Y-m-d', strtotime($data_after.' + 24 months'));
  
  
  }
  
if(strtotime($data_after) <= strtotime($today) )  {

$stmt_control = $conn_control->prepare("INSERT INTO maintenance_control_2 ( id_routine, data_start,data_after,status,procedure_mp) VALUES (?,?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt_control->bind_param("sssss",$rotina,$data_after_cron,$data_after_cron,$id_status,$procedure);

$execval_control = $stmt_control->execute();
$stmt_control->close();

  

$stmt_control = $conn_control->prepare("UPDATE maintenance_control SET data_after = ? WHERE id_routine= ?");
$stmt_control->bind_param("ss",$data_after,$rotina);
$execval_control = $stmt_control->execute();
$stmt_control->close();
}

    }
}
?>