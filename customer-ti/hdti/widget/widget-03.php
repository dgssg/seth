<?php
// Inclua o arquivo de conexão com o banco de dados
// Consulta para recuperar o número de registros na tabela "os"
$sql = "SELECT count(id) AS result FROM os";
$result = $conn->query($sql);

// Exemplo de exibição de dados em um widget
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    $resultado = $row["result"];

    }
} 
    
    $sql = "SELECT count(id) AS result FROM os where id_status = 2";
    $result = $conn->query($sql);
    
    // Exemplo de exibição de dados em um widget
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $resultado_1 = $row["result"];
            
        }
    } 
// Fechar a conexão com o banco de dados

?>
<div class="row">
    <div class="col-md-3   widget widget_tally_box">
        <div class="x_panel">
            <div class="x_title">
                <h2>O.S</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li>
                        <a class="close-link">
                            <i class="fa fa-close"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                 
                <html>
                    <head>
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <script type="text/javascript">
                            google.charts.load('current', {'packages':['gauge']});
                            google.charts.setOnLoadCallback(drawChart);
                            
                            function drawChart() {
                                
                                var data = google.visualization.arrayToDataTable([
                                    ['Label', 'Value'],
                                    ['Andamento', <?php printf($resultado_1); ?>]
                                ]);
                                
                                var options = {
                                    width: 400, 
                                    height: 120 ,
                                    max: <?php printf($resultado); ?>,

                                };
                                
                                var chart = new google.visualization.Gauge(document.getElementById('chart_div_3'));
                                
                                chart.draw(data, options);
                                
                                                             
                            }
                        </script>
                    </head>
                    <body>
                        <div id="chart_div_3" style="width: 400px; height: 120px;"></div>
                    </body>
                </html>

                 
            </div>
        </div>
    </div>