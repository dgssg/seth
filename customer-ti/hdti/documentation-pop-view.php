<?php
// Include the main TCPDF library (search for installation path).
require_once('../../framework/TCPDF/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('MK Sistemas Biomedicos');
$pdf->SetTitle('Sistema SETH');
$pdf->SetSubject('PROCEDIMENTO OPERACIONAL PADRÃO');
$pdf->SetKeywords('PROCEDIMENTO OPERACIONAL PADRÃO');

// set default header data
$pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH, 'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$codigoget = $_GET["pop"];

// Create connection
include("database/database.php");

$query = "SELECT  documentation_pop.pop_ver,documentation_pop.id_signature, documentation_pop.id_resp, documentation_pop.id_setor, documentation_pop.id_unidade, documentation_pop.rev, documentation_pop.signature, documentation_pop.create, documentation_pop.group_pop, documentation_pop.titulo, documentation_pop.codigo, equipamento_grupo.nome, documentation_pop.id, documentation_pop.id_equipamento_grupo, documentation_pop.file, documentation_pop.pop, documentation_pop.data_now, documentation_pop.data_after, documentation_pop.upgrade, documentation_pop.reg_date 
          FROM documentation_pop 
          LEFT JOIN equipamento_grupo ON equipamento_grupo.id = documentation_pop.id_equipamento_grupo 
          WHERE documentation_pop.id = ?";
if ($stmt = $conn->prepare($query)) {
    $stmt->bind_param("i", $codigoget);
    $stmt->execute();
    $stmt->bind_result($pop_ver,$id_signature, $id_colaborador, $id_setor, $id_unidade, $rev, $signature, $create, $group_pop, $titulo, $codigo, $grupo, $id, $id_equipamento_grupo, $file, $pop, $data_now, $data_after, $upgrade, $reg_date);
    $stmt->fetch();
    $stmt->close();
}

if ($id_unidade) {
    $sql = "SELECT instituicao, logo, endereco, cep, bairro, cidade, estado, cnpj 
            FROM instituicao 
            WHERE id = ?";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("i", $id_unidade);
        $stmt->execute();
        $stmt->bind_result($unidade, $logo, $rua, $cep, $bairro, $cidade, $estado, $cnpj);
        $stmt->fetch();
        $stmt->close();
    }
}

if ($id_setor) {
    $sql = "SELECT nome 
            FROM instituicao_area 
            WHERE id = ?";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("i", $id_setor);
        $stmt->execute();
        $stmt->bind_result($setor_titulo);
        $stmt->fetch();
        $stmt->close();
    }
}
$today = date("Y-m-d"); // Formato de data: Ano-Mês-Dia

$tabela4 = '';
$query = "SELECT regdate_pop_signature.id_pop, regdate_pop_signature.id_user, regdate_pop_signature.id_signature, regdate_pop_signature.reg_date, usuario.nome, usuario.sobrenome ,usuario.signature 
          FROM regdate_pop_signature 
          INNER JOIN usuario ON regdate_pop_signature.id_user = usuario.id 
          WHERE regdate_pop_signature.id_pop = ? and regdate_pop_signature.id = ?";
if ($stmt = $conn->prepare($query)) {
    $stmt->bind_param("ii", $codigoget,$id_signature);
    $stmt->execute();
    $stmt->bind_result($id_os, $id_user, $id_signature, $reg_date, $usuario_nome, $usuario_sobrenome,$signature);
    $row = 1;
    while ($stmt->fetch()) {
        $tabela4 .= '<tr>';
        
        $tabela4 .= '<td><small>' . $usuario_nome . ' ' . $usuario_sobrenome . '</small></td>';
        $tabela4 .= '<td><small>' . $id_signature . '</small></td>';
        $tabela4 .= '<td><small>' . $reg_date . '</small></td>';
      $tabela4 .= '<td>';
      if ($signature_hand == 0) {
        $tabela4 .= '<img src="dropzone/user-signature/' . $signature . '" width="45px"/>';
      }
      $tabela4 .= '</td>';

        $tabela4 .= '</tr>';
        $row++;
    }
    $stmt->close();
}
  
  $query = "SELECT signature_hand FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($signature_hand);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }

$html = <<<EOD
<style>
    body {
    font-size: 12pt;
    font-family: Calibri;
    padding : 10px;
}
table {
    border: 1px solid black;
}
th {
    border: 1px solid black;
    padding: 5px;
    background-color:grey;
    color: white;
}
td {
    border: 1px solid black;
    padding: 5px;
}
input {
    font-size: 12pt;
    font-family: Calibri;
}
</style>
  

<style type="text/css">
page {
  /* background: white;*/
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  text-align: left;
}

page[size="A4"] {
  width: 21cm;
  /*height: 29.7cm;                */
}

page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /* height: 21cm; */
}

@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
    page-break-after:always;
  }

  .noprint {
    display: none;
  }
}




body {
  font-size: 11px;
  font-family: sans-serif;
}



.pagina_retrato {
  width: 720px;
}

.pagina_paisagem {
  width: 1025px;
}

.relatorio_filtro {
  padding: 10px 0px;
  border-collapse: collapse;
}

.relatorio_filtro tr td {
  border: 1px solid #000;
}


.relatorio_titulo {
  font-family: Times New Roman;
  font-size: 15px;
  color: Black;
  font-weight: bold;
  text-transform: uppercase;
}

.borda_celula {
  border: 1px solid #000;
  background-color: #fff;
}

.noprint {
  padding-bottom: 10px;
}
.right{
  float:right;
}

.left{
  float:left;
}
</style>

<style type="text/css" media="print">
/*
@page {
margin: 0.8cm;
}
*/
.pagina_retrato, .pagina_paisagem {
width: 100%;
}
 
</style>
 

   <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <style>
    /* Estilos para o cabeçalho */
    header {
      padding: 20px;
      background-color: #f5f5f5;
      border-bottom: 1px solid #ddd;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      padding: 5px;
      border: 1px solid #ddd;
    } 

    #logo {
      width: 100px;
    }

    h1 {
      margin: 0;
      font-size: 24px;
    }

    #info {
      font-size: 14px;
    }
  </style>
<div class="conteudo" style="min-height:900px; font-size:17px;">
<table style="border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
    <tr>
   
 <td style="width: 80%; text-align: center;">
                        
 <h7>PROCEDIMENTO OPERACIONAL PADRÃO</h7><br>
       
            <h9>$titulo - $codigo</h9>
  

            </td>
        
        <td style="width: 20%;">
       <div>
        <img src="logo/clientelogo.png" alt="Logo da empresa" id="logo" width="60">
  </div>
        </td>
    </tr>
    
</table>

<p> $pop_ver </p>
<br>
<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
    <thead>
        <tr>
            <th><small>Elaboracao</small></th>
            <th><small>Aprovacao</small></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><small>$create</small></td>
            <td><small>$signature</small></td>
        </tr>
    </tbody>
</table>
<br>
<p><b><center><h2><small>Dados de Assinatura Eletronica :</small></h2></center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
    <thead>
        <tr>
            
            <th><small>Usuário</small></th>
            <th><small>Assinatura</small></th>
            <th><small>Registro</small></th>
        </tr>
    </thead>
    <tbody>
        $tabela4
    </tbody>
</table>
 <br>
 <p><b><center><h2><small></small></h2></center></b></p>
            <table>
                <tr>
                    <td><small>Unidade: $unidade</small></td>
                 
                    <td><small>Setor: $setor_titulo</small></td>
                 
                   <td><small>Revisão: $rev</small></td>
                  
                </tr>
                <tr>
                    <td><small>POP: $titulo</small></td>
                  
                    <td><small>Codigo: $codigo</small></td>
                  
                    <td><small>Grupo: $grupo</small></td>
                  
                </tr>
                <tr>
                    <td><small>Date Revisão: $data_now</small></td>
                   
                    <td><small>Date Proxima revisão: $data_after</small></td>
                  
                    <td><small>Impressão: $today</small></td>
                     
                </tr>
            
             
            </table>
       
<!-- Rodapé -->
<table width="100%" style="vertical-align: top;font-family:'Times New Roman',Times,serif; font-size: 11px;">
    <tr>
        <td align="center" colspan="2">
            <div style="padding-top: 20px;"><strong>Copyright © 2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados.</strong><br>www.mksistemasbiomedicos.com.br</div>
        </td>
    </tr>
    <tr>
        <td style="font-size: 9px" align="left">MK-01-ra-1.0</td>
        <td align="right"><!-- pagina --></td>
    </tr>
</table>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// Close and output PDF document
$pdf->Output('PROCEDIMENTO_OPERACIONAL_PADRÃO.pdf', 'I');
?>
