<?php
// Configurações do banco de dados
include("database/database.php");
date_default_timezone_set('America/Sao_Paulo');
$arquivo= date(DATE_RFC2822);
$arquivo=$arquivo.".";
$ds          = DIRECTORY_SEPARATOR;  //1
 
$storeFolder = 'xml';   //2
 
if (!empty($_FILES)) {
     
    $tempFile = $_FILES['file']['tmp_name'];          //3             
      
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4
    $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
    $anexo=$arquivo.$extension;
    $targetFile =  $targetPath.$arquivo.$extension;  //5
     
   // $targetFile =  $targetPath. $_FILES['file']['name'];  //5
 
    move_uploaded_file($tempFile,$targetFile); //6
}
try {
    // Conexão com o banco de dados usando PDO

    // Carregar o arquivo XML


    $xml = simplexml_load_file('xml/'.$anexo);

    // Percorrer cada equipamento no XML e inserir no banco de dados
foreach ($xml->equipamento as $equip) {
    // Preparar o SQL de inserção
    $stmt = $conn->prepare("INSERT INTO equipamento_grupo (nome, yr) VALUES (?, ?)");

    // Executar a inserção usando os valores do XML diretamente
    $execval = $stmt->execute([$equip->nome, $equip->yr]);

    // Verificar se a execução foi bem-sucedida
    if ($execval) {
        echo "Equipamento inserido com sucesso: " . $equip->nome . "<br>";
    } else {
        echo "Erro ao inserir equipamento: " . $equip->nome . "<br>";
    }
}


    echo "Importação concluída com sucesso!";

} catch (PDOException $e) {
    echo 'Erro ao importar dados: ' . $e->getMessage();
}
?>