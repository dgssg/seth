<?php
  //============================================================+
  // File name   : example_001.php
  // Begin       : 2008-03-04
  // Last Update : 2013-05-14
  //
  // Description : Example 001 for TCPDF class
  //               Default Header and Footer
  //
  // Author: Nicola Asuni
  //
  // (c) Copyright:
  //               Nicola Asuni
  //               Tecnick.com LTD
  //               www.tecnick.com
  //               info@tecnick.com
  //============================================================+
  
  /**
  * Creates an example PDF TEST document using TCPDF
  * @package com.tecnick.tcpdf
  * @abstract TCPDF - Example: Default Header and Footer
  * @author Nicola Asuni
  * @since 2008-03-04
  */
  
  // Include the main TCPDF library (search for installation path).
  require_once('../../framework/TCPDF/tcpdf.php');
  
  // create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('MK Sistemas Biomedicos');
  $pdf->SetTitle('Sistema SETH');
  $pdf->SetSubject('Ordem de Serviço');
  $pdf->SetKeywords('Ordem de Serviço');
  
  // set default header data
  $pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH,'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
  $pdf->setFooterData(array(0,64,0), array(0,64,128));
  
  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  
  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  
  // set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  
  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  
  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
  }
  
  // ---------------------------------------------------------
  
  // set default font subsetting mode
  $pdf->setFontSubsetting(true);
  
  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage();
  
  // set text shadow effect
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
  
$codigoget = ($_GET["laudo"]);

include_once("database/database.php");
date_default_timezone_set('America/Sao_Paulo');





$print= date(DATE_RFC2822);


$query = "SELECT tse.val,tse.id_responsavel,tse.id_colaborador,tse.codigo,tse.id_equipamento,tse.id_manufacture,equipamento_familia.id_equipamento_grupo,tse.temp,tse.hum,tse.obs,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade,tse.procedure_cal,calibration_parameter_manufacture.nome,tse.id, tse.data_start, tse.val, tse.ven, tse.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM tse LEFT JOIN equipamento on equipamento.id = tse.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN calibration_parameter_manufacture on calibration_parameter_manufacture.id =  tse.id_manufacture LEFT JOIN colaborador on colaborador.id = tse.id_colaborador WHERE tse.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($date_validation,$id_responsavel,$id_colaborador,$codigo_caliration,$id_equipamento,$id_manufacture,$id_equipamento_grupo,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}
$query = "SELECT instituicao.cep,instituicao.endereco,instituicao.bairro,instituicao.cidade,instituicao.estado,instituicao.ibge,instituicao.instituicao,instituicao_area.nome,instituicao_localizacao.nome  FROM tse LEFT JOIN equipamento on equipamento.id = tse.id_equipamento LEFT JOIN instituicao_localizacao on instituicao_localizacao.id =  equipamento.id_instituicao_localizacao LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN instituicao on instituicao.id = instituicao_area.id_unidade WHERE tse.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($cliente_cep,$cliente_adress,$cliente_bairro,$cliente_cidade,$cliente_estado,$cliente_ibge,$cliente_unidade,$cliente_area,$cliente_setor );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}
$query = "SELECT calibration_parameter_manufacture.ibge,calibration_parameter_manufacture.cnpj,calibration_parameter_manufacture.adress,calibration_parameter_manufacture.bairro,calibration_parameter_manufacture.cidade,calibration_parameter_manufacture.estado,calibration_parameter_manufacture.cep FROM tse  LEFT JOIN calibration_parameter_manufacture on calibration_parameter_manufacture.id =  tse.id_manufacture  WHERE tse.id like '$codigoget' ";



if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($manufacture_ibge,$manufacture_cnpj,$manufacture_adress,$manufacture_bairro,$manufacture_cidade,$manufacture_estado,$manufacture_cep );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
    $data_start=date("d/m/Y", strtotime($data_start));
  }

}

$query = "SELECT colaborador.primeironome,colaborador.ultimonome,colaborador.entidade FROM tse LEFT JOIN colaborador on colaborador.id = tse.id_responsavel WHERE tse.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($primeironome_responsavel,$ultimonome_responsavel,$entidade_responsavel );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}

$rotina = 0;




$query = "SELECT   pop, titulo, codigo FROM documentation_pop WHERE id_equipamento_grupo like '$id_equipamento_grupo' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($pop, $titulo,$codigo);
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}

$query = "SELECT   tse_class.nome, tse_aplicada.nome FROM tse_parameter_dados LEFT JOIN tse_class ON tse_class.id = tse_parameter_dados.tse_class LEFT JOIN tse_aplicada ON tse_aplicada.id = tse_parameter_dados.tse_aplicada WHERE id_equipamento_grupo LIKE '%$id_equipamento_grupo%' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($tse_class, $tse_aplicada);
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}

$query = "SELECT   V1,V2,V3,V4,V5,V6,V7,V8,status,iCheck1, iCheck2, obs_iCheck1,obs_iCheck2 FROM tse_report WHERE id_calibration = $codigoget";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($V1,$V2,$V3,$V4,$V5,$V6,$V7,$V8,$status,$iCheck1, $iCheck2,$obs_iCheck1,$obs_iCheck2);
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}
switch ($status) {
  case '0':
  $status="NC — Condições normais";
    break;
    case '1':
    $status="SFC — Condições de falha simples";
      break;
  

}
switch ($iCheck1) {
  case '1':
  $iCheck1_1="checked";
  break;
  case '2':
  $iCheck1_2="checked";
  break;
  case '3':
  $iCheck1_3="checked";
  break;

}
switch ($iCheck2) {
  case '1':
  $iCheck2_1="checked";
  break;
  case '2':
  $iCheck2_2="checked";
  break;
  case '3':
  $iCheck2_3="checked";
  break;

}
$html3 = '<table border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:100%;border-collapse:collapse;">';

$html3 .= '<thead>';

$html3 .= '<tr class="gridHeaderRelatorio">';

$html3 .= '<th colspan="3">Inspeção Visual</th>';
$html3 .= '</tr>';
$html3 .= '</thead>';
$html3 .= '<tbody>';
$html3 .= '<tr><td><small> <input type="radio" '.$iCheck1_1.' /> OK</small></td>';
$html3 .= '<td><small>   <input type="radio"  '.$iCheck1_2.'  /> NOK</small></td>';
$html3 .= '<td><small>  <input type="radio" '.$iCheck1_3.'  />NA</small></td></tr>';
$html3 .= '</tbody>';
$html3 .= '</table>';

$html4 = '<table border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:100%;border-collapse:collapse;">';

$html4 .= '<thead>';

$html4 .= '<tr class="gridHeaderRelatorio">';

$html4 .= '<th colspan="3">Teste Funcional</th>';
$html4 .= '</tr>';
$html4 .= '</thead>';
$html4 .= '<tbody>';
$html4 .= '<tr><td><small> <input type="radio" '.$iCheck2_1.' /> OK</small></td>';
$html4 .= '<td><small>   <input type="radio"  '.$iCheck2_2.'  /> NOK</small></td>';
$html4 .= '<td><small>  <input type="radio" '.$iCheck2_3.'  />NA</small></td></tr>';
$html4 .= '</tbody>';
$html4 .= '</table>';



$html2 = '<table border=1  border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="10%" cellspacing="0" cellpadding="3" border="0" id="ctl00_ContentPlaceHolder1_rptParamentros_ctl00_grvParametros" style="font-size:9px;width:100%;border-collapse:collapse;">';
$html2 .= '<thead>';
$html2 .= '<tr class="gridHeaderRelatorio">';
$html2 .= '<th>Resistência ao aterramento de proteção</th>';
$html2 .= '<th>Resistência do Isolamento</th>';
$html2 .= '<th>Corrente de vazamento de aterramento</th>';
$html2 .= '<th>Corrente de toque</th>';
$html2 .= '<th>Corrente de vazamento do paciente DC</th>';
$html2 .= '<th>Corrente de vazamento do paciente AC</th>';
$html2 .= '<th>Corrente de vazamento do paciente Aplicada</th>';
$html2 .= '<th>Corrente auxiliar do paciente</th>';
$html2 .= '<th>Corrente auxiliar do paciente DC</th>';
$html2 .= '<th>Corrente auxiliar do paciente AC</th>';

$html2 .= '</tr>';
$html2 .= '</thead>';
$html2 .= '<tbody>';
  $result_transacoes2 = "SELECT r1, r2, v1, v2, v3, v4, v5, v6, v7, v8 FROM tse_report WHERE id_calibration = $codigoget";
  $resultado_transacoes2 = mysqli_query($conn, $result_transacoes2);

while($row_transacoes2 = mysqli_fetch_assoc($resultado_transacoes2)){

  $html2 .= '<tr><td><small>'.$row_transacoes2['r1'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['r2'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v1'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v2'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v3'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v4'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v5'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v6'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v7'] . "</small></td>";
  $html2 .= '<td><small>'.$row_transacoes2['v8'] . "</small></td></tr>";
}


$html2 .= '</tbody>';
$html2 .= '</table>';


  $html = <<<EOD

<div class="wraper">
<header class="header table-documento base-background">
 
</header>
<br>
<!-- Início do Cabeçalho -->
<div class="div-top">

<div class="center-side-text">
<small><h3>Sistema SETH</h4></small>

</div>
<div class="center-side">

<div class="center-side-text">


<small><span class="page-number"></span></small>
<div id="page-number"></div>
<br>

</div>
</div>
</div>
<div style="clear: both;"></div>
<hr style="border-top: dashed 1px; width: 900px; color: gray; clear: both;">
<!-- Fim do Cabeçalho -->
<!-- Início do Título -->
<br>
<center>
<h2 class="titulo-relatorio">TESTE DE SEGURANÇA ELÉTRICA Nº $codigoget</h2>
</center>
<br>
<!-- Fim do Título -->
<main>
<div class="container">
<section class="content-block">

<header>
<h4 class="content-block-title">Dados do Cliente</h4>
</header>
<table class="table-documento">
<tbody>
<tr>
<td >
Cliente:
<span class="uppercase weight-bold"> $cliente_unidade</span>
</td>

<td >
Area:
<span class="uppercase weight-bold"> $cliente_setor</span>
</td>

<td >
Setor:
<span class="uppercase weight-bold"> $cliente_area</span>
</td>

<td>
CEP:
<span class="uppercase weight-bold"> $cliente_cep </span>
</td>

<td >
Rua:
<span class="uppercase weight-bold"> $cliente_adress</span>
</td>

<td >
Bairro:
<span class="uppercase weight-bold"> $cliente_bairro</span>
</td>
</tr>

<tr>
<td>
Cidade:
<span class="uppercase weight-bold">$cliente_cidade</span>
</td>

<td >
Estado:
<span class="uppercase weight-bold"> $cliente_estado</span>
</td>



<td>
IBGE:
<span class="uppercase weight-bold">$cliente_ibge</span>
</td>


</tr>
</tbody>
</table>
</section>
<!--./content-block-->

<section class="content-block">
<header>
<h4 class="content-block-title">DADOS DO EQUIPAMENTO</h4>
</header>

<table class="table-documento">
<tbody>
<tr>
<td >
Equipamento:
<span class="uppercase weight-bold"> $nome</span>
</td>

<td >
Fabricante:
<span class="uppercase weight-bold"> $fabricante</span>
</td>

<td >
Modelo:
<span class="uppercase weight-bold"> $modelo</span>
</td>

<td>
Nº de Série::
<span class="uppercase weight-bold"> $serie </span>
</td>

<td >
Patrimonio:
<span class="uppercase weight-bold"> $patrimonio </span>
</td>

<td >
Codigo:
<span class="uppercase weight-bold"> $codigo </span>
</td>
</tr>






</tbody>
</table>
</section>

<section class="content-block">
<header>
<h4 class="content-block-title">parametrização</h4>
</header>
<table class="table-documento lista-atividades">
<thead>
<tr>
<th class="width-20-percent text-left">Data Calibração: $data_start</th>
<th class="width-10-percent text-left">Validade: $val</th>
<th class="width-10-percent text-left">Classe: $tse_class</th>
<th class="width-20-percent text-left">Parte Aplicada: $tse_aplicada</th>
</tr>
</thead>
<tbody>
<tr class="width-percent">
<td colspan="3" class="atv-principal">* Codigo Externo $codigo_externo </td>
<td colspan="3" class="atv-principal">* Procedimento $pop_codigo $pop_titulo</td>

</tr>
</tbody>


</table>

</section>


<section class="content-block">
<header>
<h4 class="content-block-title">resultados</h4>
</header>
<table class="table-documento lista-atividades">
<thead>
<tr>
<th class="width-50-percent text-left">$html3</th>
<th class="width-50-percent text-left"> $html4</th>

</tr>
</thead>



</table>

$html2
<table class="table-documento lista-atividades">
<thead>
<tr>
<th class="width-20-percent text-left">Status:$status</th>
<th class="width-20-percent text-left">Observação Inspeção Visual:$obs_iCheck1</th>
<th class="width-20-percent text-left">Observação Teste Funcional:$obs_iCheck2</th>
</tr>
</thead>


</table>
</section>



<section class="content-block">
<header>
<h4 class="content-block-title">Empresa</h4>
</header>
<table class="table-documento">
<tbody>
<tr>
<td >
Empresa:
<span class="uppercase weight-bold"> $manufacture</span>
</td>




</tr>


<tr>
<td>
CEP:
<span class="uppercase weight-bold"> $manufacture_cep </span>
</td>

<td >
Rua:
<span class="uppercase weight-bold"> $manufacture_adress</span>
</td>

<td >
Bairro:
<span class="uppercase weight-bold"> $manufacture_bairro</span>
</td>
</tr>

<tr>
<td>
Cidade:
<span class="uppercase weight-bold">$manufacture_cidade</span>
</td>

<td >
Estado:
<span class="uppercase weight-bold"> $manufacture_estado</span>
</td>


</tr>

<tr>
<td>
IBGE:
<span class="uppercase weight-bold">$manufacture_ibge</span>
</td>


</tr>
</tbody>
</table>
</section>







<section class="content-block">
<header>
<h4 class="content-block-title">Assinatura</h4>
</header>
<table style="width: 100%; margin-right: 25px" class="table-documento lista-atividades">
<tbody style="text-align: left">
<tr>
<td class="width-10-percent text-left">

</td>
</tr>
<tr>
<td class="width-10-percent text-left">

</td>
</tr>
<tr>


<td class="width-10-percent text-left"></br></br>_______________________________________________</td>

<td class="width-10-percent text-left"></br></br>______________________________________________</td>
</tr>
<tr>
<td>Executante: $primeironome $ultimonome CREA/CFT: $entidade</td>
<td>Aceite:</td>
</tr>
<tr>
<td class="width-10-percent text-left">

</td>
</tr>
<tr>
<td class="width-10-percent text-left">

</td>
</tr>
<tr>
<td class="width-10-percent text-left">

</td>
</tr>
<tr>


<td class="width-10-percent text-left"</br></br>______________________________________________</td>


</tr>
<tr>
<td>Responsável Técnico:  $primeironome_responsavel $ultimonome_responsavel CREA/CFT: $entidade_responsavel</td>

</tr>
</tbody>

</table>

</section>
<br>


<!--./content-block-->
</div>
<!--/.container-->
</main>

 
</div>



<!-- /.col -->
EOD;
  // Print text using writeHTMLCell()
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  
  // ---------------------------------------------------------
  
  // Close and output PDF document
  // This method has several options, check the source code documentation for more information.
  $pdf->Output('Ordem de Serviço.pdf', 'I');
  
  //============================================================+
  // END OF FILE
  //============================================================+
