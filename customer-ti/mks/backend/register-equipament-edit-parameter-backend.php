<?php

include("../database/database.php");

$codigoget = ($_GET["equipamento"]);

$ativo= $_POST['ativo'];
$inventario= $_POST['inventario'];
$comodato= $_POST['comodato'];
$duplicidade= $_POST['duplicidade'];
$preventive= $_POST['preventive'];
$obsolecencia= $_POST['obsolecencia'];
$rfid= $_POST['rfid'];
$contrato= $_POST['contrato'];
    $software=$_POST['software'];
    $pacs=$_POST['pacs'];
    $dicom=$_POST['dicom'];
    $ris=$_POST['ris'];
    $telemedicine=$_POST['telemedicine'];
    $fhir=$_POST['fhir'];

    $stmt = $conn->prepare("UPDATE  equipamento SET software = ? WHERE id= ?");
    $stmt->bind_param("ss",$software,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
    $stmt = $conn->prepare("UPDATE  equipamento SET pacs = ? WHERE id= ?");
    $stmt->bind_param("ss",$pacs,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
    $stmt = $conn->prepare("UPDATE  equipamento SET dicom = ? WHERE id= ?");
    $stmt->bind_param("ss",$dicom,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
    $stmt = $conn->prepare("UPDATE  equipamento SET ris = ? WHERE id= ?");
    $stmt->bind_param("ss",$ris,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
    $stmt = $conn->prepare("UPDATE  equipamento SET telemedicine = ? WHERE id= ?");
    $stmt->bind_param("ss",$telemedicine,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
    $stmt = $conn->prepare("UPDATE  equipamento SET fhir = ? WHERE id= ?");
    $stmt->bind_param("ss",$fhir,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();

    $stmt = $conn->prepare("UPDATE  equipamento SET contrato = ? WHERE id= ?");
    $stmt->bind_param("ss",$contrato,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();

$stmt = $conn->prepare("UPDATE  equipamento SET inventario = ? WHERE id= ?");
$stmt->bind_param("ss",$inventario,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE  equipamento SET ativo = ? WHERE id= ?");
$stmt->bind_param("ss",$ativo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE  equipamento SET comodato = ? WHERE id= ?");
$stmt->bind_param("ss",$comodato,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET duplicidade = ? WHERE id= ?");
$stmt->bind_param("ss",$duplicidade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET preventive = ? WHERE id= ?");
$stmt->bind_param("ss",$preventive,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET obsolescence = ? WHERE id= ?");
$stmt->bind_param("ss",$obsolecencia,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET rfid = ? WHERE id= ?");
$stmt->bind_param("ss",$rfid,$codigoget);
$execval = $stmt->execute();
$stmt->close();

if($ativo == ""){
    $stmt = $conn->prepare("UPDATE  maintenance_routine SET habilitado = null WHERE id_equipamento= ?");
$stmt->bind_param("s",$codigoget);
$execval = $stmt->execute();
$stmt->close();
}

header('Location: ../register-equipament-edit?equipamento='.$codigoget);
?>