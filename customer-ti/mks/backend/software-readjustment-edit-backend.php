<?php


include("../database/database.php");


$id_software = $_GET['id_software'];
$codigoget = $_GET['id'];

$date_start = $_POST['date_start'];
$index_number = $_POST['percentage'];
$period = $_POST['percentage_applied'];
$obs = $_POST['obs'];
 

	$stmt = $conn->prepare("UPDATE software_readjustment SET date_start = ? WHERE id= ?");
	$stmt->bind_param("ss",$date_start,$codigoget);
	$execval = $stmt->execute();

	$stmt = $conn->prepare("UPDATE software_readjustment SET index_number = ? WHERE id= ?");
	$stmt->bind_param("ss",$index_number,$codigoget);
	$execval = $stmt->execute();
	$stmt = $conn->prepare("UPDATE software_readjustment SET period = ? WHERE id= ?");
	$stmt->bind_param("ss",$period,$codigoget);
	$execval = $stmt->execute();

	$stmt = $conn->prepare("UPDATE software_readjustment SET obs = ? WHERE id= ?");
	$stmt->bind_param("ss",$obs,$codigoget);
	$execval = $stmt->execute();


echo "<script>alert('Atualizado!');document.location='../software-edit?id=$id_software'</script>";
?>
