<?php

include("../database/database.php");

$codigoget = ($_GET["equipamento"]);

$codigo= $_POST['codigo'];
$patrimonio= $_POST['patrimonio'];
$serie= $_POST['serie'];
$data_instal= $_POST['data_instal'];
$data_fab= $_POST['data_fab'];
$data_val= $_POST['data_val'];
$nf= $_POST['nf'];
$vlr= $_POST['vlr'];
$obs= $_POST['obs'];
$data_calibration= $_POST['data_calibration'];
$data_calibration_end= $_POST['data_calibration_end'];
$fornecedor= $_POST['fornecedor'];
$id_cbe=$_POST['id_cbe'];
$ip=$_POST['ip'];
$rede= $_POST['rede'];
$mac= $_POST['mac'];
	$stmt = $conn->prepare("UPDATE  equipamento SET mac = ? WHERE id= ?");
	$stmt->bind_param("ss",$mac,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();

	$stmt = $conn->prepare("UPDATE  equipamento SET id_cbe = ? WHERE id= ?");
	$stmt->bind_param("ss",$rede,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE  equipamento SET ip = ? WHERE id= ?");
	$stmt->bind_param("ss",$ip,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
$stmt = $conn->prepare("UPDATE  equipamento SET vlr = ? WHERE id= ?");
$stmt->bind_param("ss",$vlr,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE  equipamento SET data_calibration = ? WHERE id= ?");
$stmt->bind_param("ss",$data_calibration,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE  equipamento SET data_calibration_end = ? WHERE id= ?");
$stmt->bind_param("ss",$data_calibration_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE  equipamento SET codigo = ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET patrimonio = ? WHERE id= ?");
$stmt->bind_param("ss",$patrimonio,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET serie = ? WHERE id= ?");
$stmt->bind_param("ss",$serie,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET data_instal = ? WHERE id= ?");
$stmt->bind_param("ss",$data_instal,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET data_fab = ? WHERE id= ?");
$stmt->bind_param("ss",$data_fab,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET data_val = ? WHERE id= ?");
$stmt->bind_param("ss",$data_val,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET nf = ? WHERE id= ?");
$stmt->bind_param("ss",$nf,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET obs = ? WHERE id= ?");
$stmt->bind_param("ss",$obs,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE  equipamento SET id_fornecedor = ? WHERE id= ?");
$stmt->bind_param("ss",$fornecedor,$codigoget);
$execval = $stmt->execute();
$stmt->close();


header('Location: ../register-equipament-edit?equipamento='.$codigoget);
?>
