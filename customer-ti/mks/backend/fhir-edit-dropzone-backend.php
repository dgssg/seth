<?php


include("../database/database.php");


$id_fhir = $_GET['id_fhir'];

$titulo = $_POST['titulo'];
$anexo=$_COOKIE['anexo'];

$stmt = $conn->prepare("INSERT INTO fhir_dropbox (id_fhir,title,file) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$id_fhir,$titulo,$anexo);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
echo "<script>alert('Adicionado!');document.location='../fhir-edit?id=$id_fhir'</script>";
?>
