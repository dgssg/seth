<?php

include("../database/database.php");

$codigoget = ($_GET["out"]);

$responsavel= $_POST['responsavel'];
$telefone= $_POST['telefone'];
$date_send= $_POST['date_send'];
$date_return= $_POST['date_return'];
$id_equipament_exit_status= $_POST['id_equipament_exit_status'];
$obs= $_POST['obs'];


$stmt = $conn->prepare("UPDATE equipament_exit SET responsavel= ? WHERE id= ?");
$stmt->bind_param("ss",$responsavel,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE equipament_exit SET telefone= ? WHERE id= ?");
$stmt->bind_param("ss",$telefone,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE equipament_exit SET date_send= ? WHERE id= ?");
$stmt->bind_param("ss",$date_send,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE equipament_exit SET date_return= ? WHERE id= ?");
$stmt->bind_param("ss",$date_return,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE equipament_exit SET id_equipament_exit_status= ? WHERE id= ?");
$stmt->bind_param("ss",$id_equipament_exit_status,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE equipament_exit SET obs= ? WHERE id= ?");
$stmt->bind_param("ss",$obs,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../os-progress-upgrade-out-edit?out='.$codigoget);
?>