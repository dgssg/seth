<?php
include("os-upload-backend.php");
include("../database/database.php");

session_start();
if(!isset($_SESSION['usuario'])){
	header ("Location: ../index.php");
}
if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
}
 
	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];

$setor = $_SESSION['setor'];
$usuario = $_SESSION['id_usuario'];
$instituicao = $_POST['instituicao'];

$equipamento = $_POST['equipamento'];
$anexo=$_COOKIE['anexo'];

$solicitacao=$_POST['solicitacao'];

$restrictions=$_POST['restrictions'];

$status="1";
// tools
$query = "SELECT os_mp_1 FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($posicionamento);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }
//tools


$carimbo="1";
$workflow="1";
$os_open="1";
$id_origin="1";


$equipamento_query=trim($equipamento);
$username_exist=0;


if (isset($_POST['submit1'])) {

$query = "SELECT equipamento.id_instituicao_localizacao,equipamento.duplicidade,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.id like'$equipamento_query' ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
	$stmt->execute();
	$stmt->bind_result($id_instituicao_localizacao,$duplicidade,$id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
	while ($stmt->fetch()) {
		$unidade=$unidade;
	}
}

if($duplicidade == "0"){

	$result  = "SELECT * FROM os WHERE id_equipamento like '$equipamento_query' and id_status != 6 ";
	$resultado = mysqli_query($conn, $result);
	//contas os resultados
	$username_exist = mysqli_num_rows($resultado);

	if($username_exist) {
		$username_exist=1;
		?>

<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />

    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>

   <!-- Bootstrap -->
    <link href="../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- iCheck -->
    <link href="../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../../framework/build/css/custom.min.css" rel="stylesheet">
    
      <!-- PNotify -->
  <link href="../../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
  </head>

<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">Alerta duplicidade!</h4>
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" > <span class="required"></span>
			</label>
			<div class="col-md-6 col-sm-6 ">
				<form action="os-open-duplicidade-backend.php?equipamento=<?php printf($equipamento); ?>&solicitacao=<?php printf($solicitacao); ?>&restrictions=<?php printf($restrictions); ?>" method="post">

					<input type="submit" value="Alerta de duplicidade de O.S Click para continuar">
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</br>
</br>
<div class="item form-group">
	<label class="col-form-label col-md-3 col-sm-3 label-align" > <span class="required"></span>
	</label>
	<div class="col-md-6 col-sm-6 ">

		<a  class="btn btn-secondary btn-xs" href="../os-open.php" onclick="return confirm('Confirmar o retorno');">Alerta de duplicidade de O.S Click para Voltar</a>
	</div>
</div>
</div>
<div class="modal-footer">
</div>
</div>
</div>



		
 <!-- Bootstrap -->
 <script src="../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
   
<?php

}


}
if($username_exist==0) {
	$stmt = $conn->prepare("INSERT INTO os (id_unidade, id_usuario, id_equipamento, id_setor, id_solicitacao, id_anexo, id_status, id_posicionamento,id_carimbo,id_workflow,restrictions,os_open,id_origin) VALUES (?,?,?, ?,?, ?, ?, ?, ?, ?, ?, ?,?)");
	$stmt->bind_param("sssssssssssss",$unidade,$usuario,$equipamento,$id_instituicao_localizacao,$solicitacao,$anexo,$status,$posicionamento,$carimbo,$workflow,$restrictions,$os_open,$id_origin);
	$execval = $stmt->execute();
	$last_id = $conn->insert_id;
	$stmt->close();
	//echo "New records created successfully";


	$stmt = $conn->prepare("INSERT INTO regdate_os_carimbo (id_os, id_carimbo) VALUES (?, ?)");
	$stmt->bind_param("ss",$last_id,$carimbo);
	$execval = $stmt->execute();
	$stmt->close();

	$query = "SELECT workflow FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($workflow);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }
if($workflow == "0"){

	$query = "SELECT id,id_os_posicionamento FROM os_workflow WHERE id_os_status = 1";


	if ($stmt = $conn->prepare($query)) {
	  $stmt->execute();
	  $stmt->bind_result($workflow,$id_os_posicionamento);
	  while ($stmt->fetch()) {
		//printf("%s, %s\n", $solicitante, $equipamento);
	  }
	}

	$stmt = $conn->prepare("INSERT INTO regdate_os_posicionamento (id_os, id_posicionamento, id_status) VALUES (?, ?, ?)");
	$stmt->bind_param("sss",$last_id,$id_os_posicionamento,$status);
	$execval = $stmt->execute();
	$stmt->close();


	$stmt = $conn->prepare("INSERT INTO regdate_os_workflow (id_os, id_workflow) VALUES (?, ?)");
	$stmt->bind_param("ss",$last_id,$workflow);
	$execval = $stmt->execute();
	$stmt->close();

	setcookie('anexo', null, -1);
   
}
$query = "SELECT email FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($email);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }
		 if (empty($email)) {
			echo "<script>alert('Aberta Solicitação!');document.location='../os-open'</script>";
			
		}
	
	header('Location: ../workflow/os-open-workflow.php?os='.$last_id);
		  
 
}
exit();
} elseif (isset($_POST['submit2'])) {
    // processar informações do formulário e enviar para Destino 2

	$query = "SELECT equipamento.id_instituicao_localizacao,equipamento.duplicidade,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.id like'$equipamento_query' ";

	//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($id_instituicao_localizacao,$duplicidade,$id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
		while ($stmt->fetch()) {
			$unidade=$unidade;
		}
	}
	
	if($duplicidade == "0"){
	
		$result  = "SELECT * FROM os WHERE id_equipamento like '$equipamento_query' and id_status != 6 ";
		$resultado = mysqli_query($conn, $result);
		//contas os resultados
		$username_exist = mysqli_num_rows($resultado);
	
		if($username_exist) {
			$username_exist=1;
			?>
	
	<html lang="en">
	  <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />
	
		<title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
	
	   <!-- Bootstrap -->
		<link href="../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<!-- NProgress -->
		<link href="../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
		<!-- bootstrap-daterangepicker -->
		<link href="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
	
		<!-- iCheck -->
		<link href="../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
		<!-- bootstrap-progressbar -->
		<link href="../../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
		<!-- JQVMap -->
		<link href="../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
		<!-- bootstrap-daterangepicker -->
		<link href="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
	
		<!-- Custom Theme Style -->
		<link href="../../../framework/build/css/custom.min.css" rel="stylesheet">
		
		  <!-- PNotify -->
	  <link href="../../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	  <link href="../../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	  <link href="../../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	  </head>
	
	<div class="modal-dialog modal-lg">
	<div class="modal-content">
	<div class="modal-header">
	<h4 class="modal-title" id="myModalLabel">Alerta duplicidade!</h4>
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	</button>
	</div>
	<div class="modal-body">
	<div class="item form-group">
				<label class="col-form-label col-md-3 col-sm-3 label-align" > <span class="required"></span>
				</label>
				<div class="col-md-6 col-sm-6 ">
					<form action="os-open-duplicidade-backend.php?equipamento=<?php printf($equipamento); ?>&solicitacao=<?php printf($solicitacao); ?>&restrictions=<?php printf($restrictions); ?>" method="post">
	
						<input type="submit" value="Alerta de duplicidade de O.S Click para continuar">
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</br>
	</br>
	<div class="item form-group">
		<label class="col-form-label col-md-3 col-sm-3 label-align" > <span class="required"></span>
		</label>
		<div class="col-md-6 col-sm-6 ">
	
			<a  class="btn btn-secondary btn-xs" href="../os-open.php" onclick="return confirm('Confirmar o retorno');">Alerta de duplicidade de O.S Click para Voltar</a>
		</div>
	</div>
	</div>
	<div class="modal-footer">
	</div>
	</div>
	</div>
	
	
	
			
	 <!-- Bootstrap -->
	 <script src="../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
		<!-- FastClick -->
		<script src="../../../framework/vendors/fastclick/lib/fastclick.js"></script>
		<!-- NProgress -->
		<script src="../../../framework/vendors/nprogress/nprogress.js"></script>
		<!-- Chart.js -->
		<script src="../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
	   
	<?php
	
	}
	
	
	}
	if($username_exist==0) {
		$stmt = $conn->prepare("INSERT INTO os (id_unidade, id_usuario, id_equipamento, id_setor, id_solicitacao, id_anexo, id_status, id_posicionamento,id_carimbo,id_workflow,restrictions,os_open,id_origin) VALUES (?,?,?, ?,?, ?, ?, ?, ?, ?, ?, ?,?)");
		$stmt->bind_param("sssssssssssss",$unidade,$usuario,$equipamento,$id_instituicao_localizacao,$solicitacao,$anexo,$status,$posicionamento,$carimbo,$workflow,$restrictions,$os_open,$id_origin);
		$execval = $stmt->execute();
		$last_id = $conn->insert_id;
		$stmt->close();
		//echo "New records created successfully";
	
	
		$stmt = $conn->prepare("INSERT INTO regdate_os_carimbo (id_os, id_carimbo) VALUES (?, ?)");
		$stmt->bind_param("ss",$last_id,$carimbo);
		$execval = $stmt->execute();
		$stmt->close();
	
		$query = "SELECT workflow FROM tools";
	
	
			  if ($stmt = $conn->prepare($query)) {
				$stmt->execute();
				$stmt->bind_result($workflow);
				while ($stmt->fetch()) {
				  //printf("%s, %s\n", $solicitante, $equipamento);
				}
			  }
	if($workflow == "0"){
	
		$query = "SELECT id,id_os_posicionamento FROM os_workflow WHERE id_os_status = 1";
	
	
		if ($stmt = $conn->prepare($query)) {
		  $stmt->execute();
		  $stmt->bind_result($workflow,$id_os_posicionamento);
		  while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		  }
		}
	
		$stmt = $conn->prepare("INSERT INTO regdate_os_posicionamento (id_os, id_posicionamento, id_status) VALUES (?, ?, ?)");
		$stmt->bind_param("sss",$last_id,$id_os_posicionamento,$status);
		$execval = $stmt->execute();
		$stmt->close();
	
	
		$stmt = $conn->prepare("INSERT INTO regdate_os_workflow (id_os, id_workflow) VALUES (?, ?)");
		$stmt->bind_param("ss",$last_id,$workflow);
		$execval = $stmt->execute();
		$stmt->close();
	
		setcookie('anexo', null, -1);
	   
	}
	$query = "SELECT email FROM tools";
	
	
			  if ($stmt = $conn->prepare($query)) {
				$stmt->execute();
				$stmt->bind_result($email);
				while ($stmt->fetch()) {
				  //printf("%s, %s\n", $solicitante, $equipamento);
				}
			  }
		if (empty($email)) {
			echo "<script>alert('Aberta Solicitação!');document.location='../os-opened-open?os=$last_id'</script>";

		}
		header('Location: ../workflow/os-open-workflow.php?os='.$last_id);
			  
	
	}    exit();
} elseif (isset($_POST['submit3'])) {
    // processar informações do formulário e enviar para Destino 3

	$query = "SELECT equipamento.id_instituicao_localizacao,equipamento.duplicidade,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.id like'$equipamento_query' ";

	//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($id_instituicao_localizacao,$duplicidade,$id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
		while ($stmt->fetch()) {
			$unidade=$unidade;
		}
	}
	
	if($duplicidade == "0"){
	
		$result  = "SELECT * FROM os WHERE id_equipamento like '$equipamento_query' and id_status != 6 ";
		$resultado = mysqli_query($conn, $result);
		//contas os resultados
		$username_exist = mysqli_num_rows($resultado);
	
		if($username_exist) {
			$username_exist=1;
			?>
	
	<html lang="en">
	  <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />
	
		<title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
	
	   <!-- Bootstrap -->
		<link href="../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<!-- NProgress -->
		<link href="../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
		<!-- bootstrap-daterangepicker -->
		<link href="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
	
		<!-- iCheck -->
		<link href="../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
		<!-- bootstrap-progressbar -->
		<link href="../../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
		<!-- JQVMap -->
		<link href="../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
		<!-- bootstrap-daterangepicker -->
		<link href="../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
	
		<!-- Custom Theme Style -->
		<link href="../../../framework/build/css/custom.min.css" rel="stylesheet">
		
		  <!-- PNotify -->
	  <link href="../../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	  <link href="../../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	  <link href="../../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	  </head>
	
	<div class="modal-dialog modal-lg">
	<div class="modal-content">
	<div class="modal-header">
	<h4 class="modal-title" id="myModalLabel">Alerta duplicidade!</h4>
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	</button>
	</div>
	<div class="modal-body">
	<div class="item form-group">
				<label class="col-form-label col-md-3 col-sm-3 label-align" > <span class="required"></span>
				</label>
				<div class="col-md-6 col-sm-6 ">
					<form action="os-open-duplicidade-backend.php?equipamento=<?php printf($equipamento); ?>&solicitacao=<?php printf($solicitacao); ?>&restrictions=<?php printf($restrictions); ?>" method="post">
	
						<input type="submit" value="Alerta de duplicidade de O.S Click para continuar">
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</br>
	</br>
	<div class="item form-group">
		<label class="col-form-label col-md-3 col-sm-3 label-align" > <span class="required"></span>
		</label>
		<div class="col-md-6 col-sm-6 ">
	
			<a  class="btn btn-secondary btn-xs" href="../os-open.php" onclick="return confirm('Confirmar o retorno');">Alerta de duplicidade de O.S Click para Voltar</a>
		</div>
	</div>
	</div>
	<div class="modal-footer">
	</div>
	</div>
	</div>
	
	
	
			
	 <!-- Bootstrap -->
	 <script src="../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
		<!-- FastClick -->
		<script src="../../../framework/vendors/fastclick/lib/fastclick.js"></script>
		<!-- NProgress -->
		<script src="../../../framework/vendors/nprogress/nprogress.js"></script>
		<!-- Chart.js -->
		<script src="../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
	   
	<?php
	
	}
	
	
	}
	if($username_exist==0) {
		$stmt = $conn->prepare("INSERT INTO os (id_unidade, id_usuario, id_equipamento, id_setor, id_solicitacao, id_anexo, id_status, id_posicionamento,id_carimbo,id_workflow,restrictions,os_open,id_origin) VALUES (?,?,?, ?,?, ?, ?, ?, ?, ?, ?, ?,?)");
		$stmt->bind_param("sssssssssssss",$unidade,$usuario,$equipamento,$id_instituicao_localizacao,$solicitacao,$anexo,$status,$posicionamento,$carimbo,$workflow,$restrictions,$os_open,$id_origin);
		$execval = $stmt->execute();
		$last_id = $conn->insert_id;
		$stmt->close();
		//echo "New records created successfully";
	
	
		$stmt = $conn->prepare("INSERT INTO regdate_os_carimbo (id_os, id_carimbo) VALUES (?, ?)");
		$stmt->bind_param("ss",$last_id,$carimbo);
		$execval = $stmt->execute();
		$stmt->close();
	
		$query = "SELECT workflow FROM tools";
	
	
			  if ($stmt = $conn->prepare($query)) {
				$stmt->execute();
				$stmt->bind_result($workflow);
				while ($stmt->fetch()) {
				  //printf("%s, %s\n", $solicitante, $equipamento);
				}
			  }
	if($workflow == "0"){
	
		$query = "SELECT id,id_os_posicionamento FROM os_workflow WHERE id_os_status = 1";
	
	
		if ($stmt = $conn->prepare($query)) {
		  $stmt->execute();
		  $stmt->bind_result($workflow,$id_os_posicionamento);
		  while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		  }
		}
	
		$stmt = $conn->prepare("INSERT INTO regdate_os_posicionamento (id_os, id_posicionamento, id_status) VALUES (?, ?, ?)");
		$stmt->bind_param("sss",$last_id,$id_os_posicionamento,$status);
		$execval = $stmt->execute();
		$stmt->close();
	
	
		$stmt = $conn->prepare("INSERT INTO regdate_os_workflow (id_os, id_workflow) VALUES (?, ?)");
		$stmt->bind_param("ss",$last_id,$workflow);
		$execval = $stmt->execute();
		$stmt->close();
	
		setcookie('anexo', null, -1);
	   
	}
	$query = "SELECT email FROM tools";
	
	
			  if ($stmt = $conn->prepare($query)) {
				$stmt->execute();
				$stmt->bind_result($email);
				while ($stmt->fetch()) {
				  //printf("%s, %s\n", $solicitante, $equipamento);
				}
			  }
			  $today = date("Y-m-d H:i:s");  
			  $stmt = $conn->prepare("UPDATE os SET sla_register = ? WHERE id= ?");
	$stmt->bind_param("ss",$today,$last_id);
	$execval = $stmt->execute();
	$stmt->close();

	$stmt = $conn->prepare("UPDATE os SET pm = 0 WHERE id= ?");
	$stmt->bind_param("s",$last_id);
	$execval = $stmt->execute();
	$stmt->close();
			  
		if (empty($email)) {
			echo "<script>document.location='../os-close?sweet_salve=1&1&os=$last_id'</script>";
		}
		header('Location: ../workflow/os-open-workflow.php?os='.$last_id);
			  
			 
	// echo "<script>alert('Aberta Solicitação!');document.location='../os-progress-upgrade?os=$last_id'</script>";
    exit();
}
}
?>
		