<?php


include("../database/database.php");


$id_dicom = $_GET['id_dicom'];

$date_start = $_POST['date_start'];
$index_number = $_POST['percentage'];
$period = $_POST['percentage_applied'];
$obs = $_POST['obs'];


$stmt = $conn->prepare("INSERT INTO dicom_readjustment (id_dicom,date_start,index_number,period,obs) VALUES (?, ?, ?,?, ?)");
$stmt->bind_param("sssss",$id_dicom,$date_start,$index_number,$period,$obs);
$execval = $stmt->execute();
$stmt->close();


echo "<script>alert('Adicionado!');document.location='../dicom-edit?id=$id_dicom'</script>";
?>
