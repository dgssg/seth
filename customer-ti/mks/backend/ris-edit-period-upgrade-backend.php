<?php


include("../database/database.php");


$id_ris = $_GET['id_ris'];
$codigoget = $_GET['id'];
$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];
$percentage = $_POST['percentage'];
$percentage_applied = $_POST['percentage_applied'];
$vlr = $_POST['vlr'];
$obs = $_POST['obs'];

 
	$stmt = $conn->prepare("UPDATE ris_period SET date_start = ? WHERE id= ?");
	$stmt->bind_param("ss",$date_start,$codigoget);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("UPDATE ris_period SET date_end = ? WHERE id= ?");
	$stmt->bind_param("ss",$date_end,$codigoget);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("UPDATE ris_period SET percentage = ? WHERE id= ?");
	$stmt->bind_param("ss",$percentage,$codigoget);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("UPDATE ris_period SET percentage_applied = ? WHERE id= ?");
	$stmt->bind_param("ss",$percentage_applied,$codigoget);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("UPDATE ris_period SET vlr = ? WHERE id= ?");
	$stmt->bind_param("ss",$vlr,$codigoget);
	$execval = $stmt->execute();
	
	$stmt = $conn->prepare("UPDATE ris_period SET obs = ? WHERE id= ?");
	$stmt->bind_param("ss",$obs,$codigoget);
	$execval = $stmt->execute();
	
echo "<script>alert('Adicionado!');document.location='../ris-edit?id=$id_ris'</script>";
?>
