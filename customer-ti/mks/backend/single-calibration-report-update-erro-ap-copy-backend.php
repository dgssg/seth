<?php
$conn = null;
$conn_cal = null;
$stmt = null;
$stmt_cal = null;

include("../database/database.php");
include("../database/database_cal.php");

$get="0";
$loop="0";
$laudo= ($_GET['laudo']);
$class= ($_GET['class']);
$erro_vl= $_POST['erro_vl'];
$erro_ap= $_POST['erro_ap'];

//$query="SELECT calibration_parameter_tools_dados.i_h,calibration_report_single.id, calibration_report_single.id_calibration, calibration_report_single.id_calibration_parameter_tools, calibration_report_single.id_calibration_parameter_tools_dados, calibration_report_single.id_calibration_parameter_dados_parameter, calibration_report_single.ve, calibration_report_single.vl_1, calibration_report_single.vl_2, calibration_report_single.vl_3, calibration_report_single.vl_avg, calibration_report_single.erro, calibration_report_single.dp, calibration_report_single.ie, calibration_report_single.ia, calibration_report_single.k, calibration_report_single.ib, calibration_report_single.ic, calibration_report_single.status, calibration_report_single.upgrade, calibration_report_single.reg_date, calibration_report_single.trash FROM calibration_report INNER JOIN calibration_parameter_tools_dados ON calibration_parameter_tools_dados.id = calibration_report_single.id_calibration_parameter_tools_dados WHERE calibration_report_single.id_calibration = $laudo ";
//and calibration_report_single.ve != 0 and  calibration_report_single.vl_1 != 0 and  calibration_report_single.vl_2 != 0 and calibration_report_single.vl_3 != 0

$query="SELECT calibration_report_single.erro_ac,calibration_parameter_dados_parameter.d_1,calibration_parameter_dados_parameter.d_2,calibration_parameter_dados_parameter.erro,calibration_parameter_tools_dados.i_h,calibration_report_single.id, calibration_report_single.id_calibration, calibration_report_single.id_calibration_parameter_tools, calibration_report_single.id_calibration_parameter_tools_dados, calibration_report_single.id_calibration_parameter_dados_parameter, calibration_report_single.ve, calibration_report_single.vl_1, calibration_report_single.vl_2, calibration_report_single.vl_3, calibration_report_single.vl_avg, calibration_report_single.erro, calibration_report_single.dp, calibration_report_single.ie, calibration_report_single.ia, calibration_report_single.k, calibration_report_single.ib, calibration_report_single.ic, calibration_report_single.status, calibration_report_single.upgrade, calibration_report_single.reg_date, calibration_report_single.trash FROM calibration_report_single INNER JOIN calibration_parameter_tools_dados ON calibration_parameter_tools_dados.id = calibration_report_single.id_calibration_parameter_tools_dados INNER JOIN calibration_parameter_dados_parameter ON calibration_parameter_dados_parameter.id = calibration_report_single.id_calibration_parameter_dados_parameter WHERE calibration_report_single.id_calibration = $laudo and calibration_report_single.id_calibration_parameter_dados_parameter = $class";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($erro_ac,$d_1,$d_2,$d_erro,$i_h,$id,$id_calibration,$id_calibration_parameter_tools,$id_calibration_parameter_tools_dados,$id_calibration_parameter_dados_parameter,$ve,$vl_1,$vl_2,$vl_3,$vl_avg,$erro,$dp,$ie,$ia,$k,$ib,$ic,$status,$upgrade,$reg_date,$trash);
		 while ($stmt->fetch()) {

//printf("%s\n, %s\n,%s\n, %s\n,%s\n, %s\n", $i_h, $id,$ve,$vl_1,$vl_2,$vl_3);


$ibn=$i_h;  //Esse cálculo só se aplica se a Incerteza for do tipo B, e o tipo for, (resolução ou outras), então é a estimativa convertida dividida pela distribuição

$vl_avg= ($vl_1 + $vl_2 + $vl_3)/3; //Valor medio

$erro=abs($ve-$vl_avg); //Erro

$dp=sqrt((pow(($vl_1-$vl_avg),2)+pow(($vl_2-$vl_avg),2)+pow(($vl_3-$vl_avg),2))/3); // Desvio Padrão

$ia=$dp/(sqrt(3)); // Incerteza do Tipo A / Repetitividade


$ib= $ibn; //É a raiz quadrada das somas das incertezas do tipo B ao quadrado.

$ic=sqrt((pow($ib,2)+pow($ia,2))); //É a raiz quadrada da soma da incerteza do tipo B ao quadrado mais a incerteza do tipo A ao quadrado.

$ie=$ic*$k;//É a Incerteza Combinada vezes o fator de Abrangência.
$erro_ld=trim($erro_ld);
//if($erro_ld=="0"){
 $ap= (abs($ie) + abs($vl_avg));
 if($d_erro =="0"){
   if($vl_avg > $ve){
    $ac=(abs($ve) + abs($d_1));

     if($vl_avg < $ac){
      $status="0";
    }
     if($vl_avg > $ac){
        $status="1";
      }
  }
  if($vl_avg < $ve){
   $ac=(abs($ve) - abs($d_1));

    if($vl_avg > $ac){
     $status="0";
   }
    if($vl_avg < $ac){
       $status="1";
     }
 }

}

if($d_erro =="1"){
  if($vl_avg > $ve){
       $ac=(abs($ve))+(abs($ve) * abs($d_2));

    if($vl_avg < $ac){
     $status="0";
   }
    if($vl_avg > $ac){
       $status="1";
     }
 }
 if($vl_avg < $ve){
  $ac=(abs($ve))+(abs($ve) * abs($d_2));

   if($vl_avg > $ac){
    $status="0";
  }
   if($vl_avg < $ac){
      $status="1";
    }
}

}

  if($erro_ac != "0"){
    if($d_erro =="0"){
      if($vl_avg > $ve){
       $ac=(abs($ve) + abs($erro_ac));

        if($vl_avg < $ac){
         $status="0";
       }
        if($vl_avg > $ac){
           $status="1";
         }
     }
     if($vl_avg < $ve){
      $ac=(abs($ve) - abs($erro_ac));

       if($vl_avg > $ac){
        $status="0";
      }
       if($vl_avg < $ac){
          $status="1";
        }
    }

    }

    if($d_erro =="1"){
     if($vl_avg > $ve){
          $ac=(abs($ve))+(abs($ve) * abs($erro_ac));

       if($vl_avg < $ac){
        $status="0";
      }
       if($vl_avg > $ac){
          $status="1";
        }
    }
    if($vl_avg < $ve){
     $ac=(abs($ve))+(abs($ve) * abs($erro_ac));

      if($vl_avg > $ac){
       $status="0";
     }
      if($vl_avg < $ac){
         $status="1";
       }
    }

    }
  }

  if( $status == ""){
     $status = "0";
  }

$stmt_cal= $conn_cal->prepare("UPDATE calibration_report_single  SET vl_avg = ? WHERE id= ?");
//if(!$req){
  //    echo "Prepare failed: (". $conn_cal_cal->errno.") ".$conn_cal_cal->error."<br>";
   //}
$stmt_cal->bind_param("ss",$vl_avg,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET erro = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$erro,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET dp = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$dp,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET ia = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$ia,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET ie = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$ie,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET k = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$k,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET ib = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$ib,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET ic = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$ic,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();
//$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET id_calibration_parameter_tools = ? WHERE id= ?");
//$stmt_cal->bind_param("si",$id_calibration_parameter_tools,$id);
//$execval = $stmt_cal->execute();
//$stmt_cal->close();
//$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET id_calibration_parameter_tools_dados = ? WHERE id= ?");
//$stmt_cal->bind_param("si",$tools,$id);
//$execval = $stmt_cal->execute();
//$stmt_cal->close();

$stmt_cal = $conn_cal->prepare("UPDATE calibration_report_single  SET status = ? WHERE id= ?");
$stmt_cal->bind_param("ss",$status,$id);
$execval = $stmt_cal->execute();
$stmt_cal->close();

// printf("%s\n, %s\n", $ap, $ac);
//printf("%s\n, %s\n,%s\n, %s\n,%s\n, %s\n", $ap, $erro,$dp,$ia,$ie,$k);
}
}

echo "<script>alert('Calculado!');document.location='../calibration-report-edit-single-copy?laudo=$laudo'</script>";


//header("location:javascript:alert(\"Calculo Processado!\");location.href=\"../calibration-report-edit-copy?laudo='.$laudo\";")
header('Location: ../calibration-report-edit-single-copy?laudo='.$laudo);
?>
