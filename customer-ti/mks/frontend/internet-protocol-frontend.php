<?php
include("database/database.php");
 
  ?>

  <div class="right_col" role="main">

    <div class="">

      <div class="page-title">
        <div class="title_left">
          <h3>IP</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5  form-group pull-right top_search">
            <div class="input-group">

              <span class="input-group-btn">

              </span>
            </div>
          </div>
        </div>
      </div>


   


      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>



      <div class="x_panel">
        <div class="x_title">
          <h2>Lista de Equipamentos</h2>
          <ul class="nav navbar-right panel_toolbox">
            
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                  class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                      <th><input type="checkbox" id="check_all"></th>
                        <th>IP</th>
                          <th>Mac</th>
                       <th>Grupo</th>
                        <th>Codigo</th>
                        <th>Equipamento</th>
                        <th>Modelo</th>
                        <th>Fabricante</th>
                        <th>N/S</th>
                         <th>Patrimonio</th>
                        <th>Unidade</th>
                        
                        <th>Setor</th>
                        <th>Area</th>



                  </tr>
                </thead>
                <tbody>
                  
            </tbody>
          </table>




 


    </div>
  </div>
  <script language="JavaScript">
 let paginaAtual = 0;

 $(document).ready(function() {
    $('#check_all').click(function() {
      $('input[type=checkbox]').prop('checked', this.checked); // Seleciona todas as checkboxes
    });
  $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );

  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-equipamento.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
    .then(data => {
      const novosDados = data.map(item => {
      let badgeHTML = '';
      let badgeHTMLBaixa = '';
      let badgeHTMLcheckist= '';
      let badgeHTMLtreinamento= '';
      let badgeHTMLparecer_1= '';
      let badgeHTMLparecer_2= '';
      let badgeHTMLsaida_1= '';
      let badgeHTMLsaida_2= '';
      let badgeHTMLconformidade= '';
            
      if (item.nf_dropzone != null) {
        badgeHTML = "<span class='badge badge-success'>OK</span>";
      }
      if (item.checklist != null) {
          badgeHTMLcheckist = "<span class='badge badge-success'>OK</span>";
        }
         if (item.treinamento != null) {
          badgeHTMLtreinamento = "<span class='badge badge-success'>OK</span>";
        }
            if (item.parecer_1 != null) {
          badgeHTMLparecer_1 = "<span class='badge badge-success'>OK</span>";
        }
           if (item.parecer_2 != null) {
          badgeHTMLparecer_2 = "<span class='badge badge-success'>OK</span>";
        }
        if (item.saida_1 != null) {
          badgeHTMLsaida_1 = "<span class='badge badge-success'>OK</span>";
        }
         if (item.saida_2 != null) {
          badgeHTMLsaida_2 = "<span class='badge badge-success'>OK</span>";
        }
          if (item.conformidade != null) {
          badgeHTMLconformidade = "<span class='badge badge-success'>OK</span>";
        }
      if (item.baixa == 0) {
          badgeHTMLBaixa = "<span class='badge badge-success'>OK</span>";
        }
      return [
        ``,
        '<input type="checkbox" class="checkbox" value="' + item.id + '">', // Adiciona a checkbox em cada linha
 item.ip,
 item.mac,
  item.grupo,
  item.codigo,
  item.equipamento,
  item.modelo,
  item.fabricante,
  item.serie,
    item.patrimonio,
  item.instituicao,
  item.area,
 
  item.setor 
                 
                                    
               
      ];

      });

    // Adiciona as novas linhas ao DataTables e desenha a tabela
    $('#datatable').DataTable().rows.add(novosDados).draw();

 // Retorna à página atual após a exclusão

  $('#datatable').DataTable().page(paginaAtual).draw('page');

// Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([2,3,4,5,6,7,8,9,10,11,12,13,14,15]).every(function (d) {
        var column = this;
  var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
            
      
            
    });
});

                // Função para tomar a ação com as linhas selecionadas
                function takeAction() {
                  var selectedRows = [];
                  $('.checkbox:checked').each(function() {
                    selectedRows.push($(this).val());
                  });
                  
                  // Verifica se pelo menos um item foi selecionado
                  if (selectedRows.length > 0) {
                    // Constrói o URL com os IDs dos itens selecionados
                    var url = 'register-equipament-tag-selected?equipamento=' + selectedRows.join(',');
                    // Abre o URL em uma nova aba
                    window.open(url, '_blank');
                  } else {
                    // Se nenhum item foi selecionado, mostra uma mensagem
                    alert('Por favor, selecione pelo menos um item.');
                  }
                }


</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#btn-carregar').click(function() {
    $.ajax({
      url: 'loading/load_equipamento.php', // substitua pelo nome do seu arquivo PHP que busca os itens restantes da tabela
      type: 'POST', // ou 'GET', dependendo da sua implementação
      data: {
        offset: 10 // substitua pelo número de itens que você já carregou
      },
      success: function(data) {
        $('#datatable').append(data); // adicione os novos itens à tabela
      }
    });
  });
});

</script>

















</div>
</div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
