

<div class="right_col" role="main">

<div class="">

  <div class="page-title">
    <div class="title_left">
      <h3>Alerta Anvisa</h3>
    </div>

    <div class="title_right">
      <div class="col-md-5 col-sm-5  form-group pull-right top_search">
        <div class="input-group">

          <span class="input-group-btn">

          </span>
        </div>
      </div>
    </div>
  </div>


<div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

   <div class="alert alert-info alert-dismissible " role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <strong>Area de Alertas!</strong> Todo alteração é integrada.
        </div>



      </div>
    </div>



   <!-- page content -->


   <div class="clearfix"></div>
  <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

      <a class="btn btn-app" href="technological-surveillance">
                    <i class="glyphicon glyphicon-list-alt"></i> Consulta
                </a>
                <a class="btn btn-app" href="technological-surveillance-register">
                    <i class="glyphicon glyphicon-plus"></i> Cadastro
                </a>
                <a class="btn btn-app" href="technological-surveillance-list">
                    <i class="glyphicon glyphicon-th-list"></i> Meus Alertas
                </a>
               
       

      </div>
    </div>






  <div class="clearfix"></div>
  


  <div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Cadastro <small>de Alerta</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="dropdown-item" href="#">Settings 1</a>
              </li>
              <li><a class="dropdown-item" href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />


<?php include("frontend/"); ?>




      </div>
    </div>
  </div>
</div>




</div>
  </div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

