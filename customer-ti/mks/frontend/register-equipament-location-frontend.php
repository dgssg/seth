<?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");
$query = "SELECT equipamento_familia.anvisa,equipamento.baixa,equipamento.ativo,equipamento.inventario,equipamento.comodato,equipamento.duplicidade,equipamento.preventive,equipamento.obsolescence,equipamento.rfid,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento.serie,equipamento.patrimonio,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao WHERE equipamento.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($anvisa,$baixa,$ativo,$inventario,$comodato,$duplicidade,$preventive,$obsolecencia,$rfid,$data_val,$nf,$data_fab,$data_instal,$serie,$patrimonio,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }


}
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> Historico de localiza&ccedil;&atilde;o <small>Equipamento</small></h3>
            </div>


        </div>

        <div class="clearfix"></div>

        <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Dados <small>Equipamento</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Settings 1</a>
                                    <a class="dropdown-item" href="#">Settings 2</a>
                                </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">



                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" value="<?php printf($nome); ?>" readonly="readonly"
                                    required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Modelo <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" value="<?php printf($modelo); ?>" readonly="readonly"
                                    required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Fabricante <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" value="<?php printf($fabricante); ?>" readonly="readonly"
                                    required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="anvisa">Anvisa <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" value="<?php printf($anvisa); ?>" readonly="readonly"
                                    required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"
                                    readonly="readonly" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="patrimonio">Patromonio
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="patrimonio" name="patrimonio"
                                    value="<?php printf($patrimonio); ?>" readonly="readonly" required="required"
                                    class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="serie">N/S <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="serie" name="serie" value="<?php printf($serie); ?>"
                                    readonly="readonly" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Codigo <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="data_instal" name="data_instal"
                                    value="<?php printf($data_instal); ?>" readonly="readonly" required="required"
                                    class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_fab">Fabricação <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="data_fab" name="data_fab" value="<?php printf($data_fab); ?>"
                                    readonly="readonly" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_val">Garantia <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="data_val" name="data_val" value="<?php printf($data_val); ?>"
                                    readonly="readonly" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>" readonly="readonly"
                                    required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">Observação <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="obs" name="obs" value="<?php printf($obs); ?>"
                                    readonly="readonly" required="required" class="form-control ">
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>





        <div class="clearfix"></div>


        <!-- Posicionamento -->

        <div class="x_panel">
            <div class="x_title">
                <h2>Historico de localiza&ccedil;&atilde;o:</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2"><i
                                class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <!--arquivo -->
                <?php

$query = "SELECT equipament_location.data_location,equipament_location.id,equipament_location.reg_date,equipament_location.obs,  instituicao_localizacao.nome,instituicao_area.nome FROM equipament_location  INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipament_location.id_localizacao INNER JOIN instituicao_area  ON instituicao_area.id = instituicao_localizacao.id_area WHERE equipament_location.id_equipamento like
'$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($data_location,$id,$data,$obs,$area,$setor);
//  while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   // }
//}
$row=0;
?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Setor</th>
                            <th>Area</th>
                            <th>Justificativa</th>
                            <th>Data</th>
                            <th>Registro</th>
                            <th>Ação</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php  while ($stmt->fetch()) { ?>

                        <tr>
                            <th scope="row"><?php printf($row); ?></th>
                            <td><small><?php printf($setor); ?></small></td>
                            <td><small><?php printf($area); ?></small></td>
                            <td><small><?php printf($obs); ?></small></td>
                            <td><small><?php printf($data_location); ?></small></td>
                            <td><small><?php printf($data); ?></small></td>
                            <td>
                                <a class="btn btn-app" onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-equipamento-location-trash.php?id=<?php printf($id); ?>&equipamento=<?php printf($codigoget); ?> ';
  }
})
">
                                    <i class="fa fa-trash"></i> Excluir
                                </a>
                            </td>

                        </tr>
                        <?php  $row=$row+1; }
}   ?>
                    </tbody>
                </table>




            </div>
        </div>

        <!-- Posicionamento -->

        <!-- Registro -->
        <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true" id="myModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title" > localiza&ccedil;&atilde;o</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <!-- Registro forms-->
                        <form
                            action="backend/equipament-location-register-backend.php?equipamento=<?php printf($codigoget);?>"
                            method="post">
                            <div class="ln_solid"></div>


                            <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-left" name="instituicao"
                                  id="instituicao" style="width:350px;" placeholder="Unidade" required="required">
                                  <option> Selecione uma unidade</option>
                                  <?php



                                           $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
                              }
                         ?>

                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#instituicao').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>



                      <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="setor">Setor <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-right" name="area" id="area"
                                  style="width:350px;" placeholder="Area">
                                  <option value="">Selecione um Setor</option>
                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#area').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>

                      <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="area">Area <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-right" name="setor" id="setor"
                                  style="width:350px;" placeholder="Area" required>
                                  <option value="">Selecione uma Area</option>
                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#setor').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>

<div class="ln_solid"></div>
   <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Data
                                      Alteração <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="date" id="data_location" name="data_location"
                                          class="form-control " required>
                                  </div>
                              </div>

                      <div class="ln_solid"></div>

                            <label for="obs">Justificativa:</label>
                            <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                                data-parsley-validation-threshold="10"></textarea>

                            <div class="ln_solid"></div>




                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary">Salvar Informações</button>

                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Registro -->

        <div class="x_panel">
            <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <a class="btn btn-app" href="register-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>



                <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

            </div>
        </div>




    </div>
</div>
<!-- Fechamen    to -->

  <!-- Fechamen    to -->
  <script type="text/javascript">
$(document).ready(function() {
    $('#instituicao').change(function() {
        $('#area').select2({
            dropdownParent: $("#myModal")
        }).load('sub_categorias_post.php?instituicao=' + $('#instituicao').val());
    });
});
  </script>
  <script type="text/javascript">
$(document).ready(function() {
    $('#area').change(function() {
        $('#setor').select2({
            dropdownParent: $("#myModal")
        }).load('sub_categorias_post_setor.php?area=' + $('#area').val());
    });
});
  </script>
