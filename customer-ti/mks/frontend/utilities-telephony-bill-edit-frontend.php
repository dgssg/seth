<?php
setlocale(LC_ALL, 'pt_BR');

?>
<?php
$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT unidade_telephony_bill.id, unidade_telephony.unidade,month.month, year.yr,unidade_telephony_bill.upgrade,unidade_telephony_bill.reg_date FROM unidade_telephony_bill INNER JOIN unidade_telephony ON unidade_telephony.id = unidade_telephony_bill.id_unidade INNER JOIN month
ON month.id = unidade_telephony_bill.id_month INNER JOIN year ON year.id = unidade_telephony_bill.id_year where unidade_telephony_bill.id =  '$codigoget' ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$instituicao,$month,$yr,$upgrade,$reg_date);



  while ($stmt->fetch()) {

  }
}
?>

<div class="right_col" role="main">

  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>Edição</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <!-- page content -->









      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Informações <small>da Fatura de Telefonia</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                </div>
              </div>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                <div class="col-md-6 col-sm-6 ">
                  <input readonly="readonly" id="unidade" class="form-control" type="text" name="unidade"  value="<?php printf($instituicao); ?> " >
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                <div class="col-md-6 col-sm-6 ">
                  <input readonly="readonly" id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($yr); ?> " >
                </div>
              </div>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Mês</label>
                <div class="col-md-6 col-sm-6 ">
                  <input readonly="readonly" id="medidor" class="form-control" type="text" name="medidor"  value="<?php printf($month); ?> " >
                </div>
              </div>






              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                <div class="col-md-6 col-sm-6 ">
                  <input readonly="readonly" type="text"  class="form-control"  value="<?php printf($reg_date); ?> ">
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                <div class="col-md-6 col-sm-6 ">
                  <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                </div>
              </div>











            </div>
          </div>
        </div>
      </div>
 

     
     

        <div class="x_panel">
          <div class="x_title">
            <h2>Itens da Fatura</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                    class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <!--arquivo -->
                <?php
                $query="SELECT unidade_telephony_bill_itens.id,unidade_telephony_bill_itens.vlr,unidade_telephony_mod.name,unidade_telephony_operation.name,unidade_telephony_itens.iten FROM unidade_telephony_bill_itens INNER JOIN  unidade_telephony_mod ON unidade_telephony_mod.id = unidade_telephony_bill_itens.id_mod INNER JOIN  unidade_telephony_operation ON unidade_telephony_operation.id = unidade_telephony_bill_itens.id_operation INNER JOIN  unidade_telephony_itens ON unidade_telephony_itens.id = unidade_telephony_bill_itens.id_bill_itens where unidade_telephony_bill_itens.id_bill  = '$codigoget'";
                $row=1;
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id,$vlr,$mod,$operation,$iten);

                  ?>
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Modalidade</th>
                        <th>Operadora</th>
                        <th>Item</th>
                        <th>Valor</th>
                        <th>Ação</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($mod); ?></td>
                          <td><?php printf($operation); ?></td>
                          <td><?php printf($iten); ?></td>
                          <td><?php printf($vlr); ?></td>
                        
                          <td><p> <a   href="backend/unidade-energia-fatura-itens-trash-backend.php?bill=<?php printf($codigoget); ?>&iten=<?php printf($id); ?> " > <i class="fa fa-trash"></i> </a>
                            &emsp;
                            <a   href="utilities-energy-bill-edit-itens?id=<?php printf($codigoget); ?>&iten=<?php printf($id); ?> "> <i class="fa fa-edit"></i> </a> </p></td>
                          </tr>
                          <?php  $row=$row+1; }}   ?>
                        </tbody>
                      </table>




                    </div>
                  </div>

                  <!-- Posicionamento -->
                  <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Itens da Fatura</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form action="backend/utilities-telephony-bill-itens-backend.php?id=<?php printf($codigoget);?>" method="post">
                            <div class="ln_solid"></div>

                            <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Modalidade de Telefonia</label>
            <div class="col-md-6 col-sm-6 ">
            <select type="text" class="form-control has-feedback-right" name="id_mod" id="id_mod"   >
										  <option value="">Selecione o tipo de Modalidade</option>
										  	<?php
										  	
										  
										  	
										 $query = "SELECT id, cod, name from unidade_telephony_mod";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $cod,$name);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($cod);?> - <?php printf($name);?> </option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Operadora de Telefonia "></span>
										
										
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#id_mod').select2();
                                      });
                                 </script>
          </div>

                            <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Operadora de Telefonia</label>
            <div class="col-md-6 col-sm-6 ">
            <select type="text" class="form-control has-feedback-right" name="id_operation" id="id_operation"   >
										  <option value="">Selecione o tipo de Operadora</option>
										  	<?php
										  	
										  
										  	
										 $query = "SELECT id, cod, name from unidade_telephony_operation";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $cod,$name);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($cod);?> - <?php printf($name);?> </option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Operadora de Telefonia "></span>
										
										
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#id_operation').select2();
                                      });
                                 </script>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Iten de Telefonia</label>
            <div class="col-md-6 col-sm-6 ">
            <select type="text" class="form-control has-feedback-right" name="id_bill_itens" id="id_bill_itens"   >
										  <option value="">Selecione o tipo de Iten</option>
										  	<?php
										  	
										  
										  	
										 $query = "SELECT id, cod, iten from unidade_telephony_itens";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $cod,$name);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($cod);?> - <?php printf($name);?> </option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Operadora de Telefonia "></span>
										
										
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#id_bill_itens').select2();
                                      });
                                 </script>
          </div>
                     
                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor</label>
                          <div class="col-md-6 col-sm-6 ">
                          <input name="vlr" id="vlr" type="text"   class="form-control  " onKeyPress="return(moeda(this,'.',',',event))">
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <label for="message_mp">Observação:</label>
                          <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="" value=""></textarea>

                            <div class="ln_solid"></div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
                </form>
                <!-- Posicionamento -->
              </div>

             

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ação</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                      <a class="btn btn-app"  href="utilities-telephony-bill">
                        <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                      </a>



                    </div>
                  </div>




                </div>
              </div>
              <!-- /page content -->
              <!-- compose -->
              <!-- compose -->
        
              <!-- /compose -->
              <!-- /compose -->

              <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
              <script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.fromCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>  