<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Registro  &amp; Consulta <small>Cliente</small></h3>
      </div>
      
      
    </div>
    
    <div class="clearfix"></div>
    <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        
        <a class="btn btn-app"  href="register-customer">
          <i class="glyphicon glyphicon-list-alt"></i> Consulta
        </a>
        <a class="btn btn-app"  href="register-customer-register">
          <i class="glyphicon glyphicon-plus"></i> Cadastro
        </a>
        
        
        
      </div>
    </div>
    
    
    
    

    <div class="clearfix"></div>
    
    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cadastro<small>Cliente</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings 1</a>
                  <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            
            
            
            
            
            <!-- /page content -->
            <form  action="backend/register-customer-backend.php" method="post">
              
              
              
              
              
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="empresa" class="form-control" type="text" name="empresa"   >
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CNPJ</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="cnpj" class="form-control" type="text" name="cnpj"   >
                </div>
              </div>
              
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Contato</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="contato" class="form-control" type="text" name="contato"    >
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Email</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="email" class="form-control" type="text" name="email"    >
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Telefone 1</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="telefone_1" class="form-control" type="text" name="telefone_1"    >
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Telefone 2</label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="telefone_2" class="form-control" type="text" name="telefone_2"    >
                </div>
              </div>
              <script>
                
                function limpa_formulário_cep() {
                  //Limpa valores do formulário de cep.
                  document.getElementById('rua').value=("");
                  document.getElementById('bairro').value=("");
                  document.getElementById('cidade').value=("");
                  document.getElementById('uf').value=("");
                  document.getElementById('ibge').value=("");
                }
                
                function meu_callback(conteudo) {
                  if (!("erro" in conteudo)) {
                    //Atualiza os campos com os valores.
                    document.getElementById('rua').value=(conteudo.logradouro);
                    document.getElementById('bairro').value=(conteudo.bairro);
                    document.getElementById('cidade').value=(conteudo.localidade);
                    document.getElementById('uf').value=(conteudo.uf);
                    document.getElementById('ibge').value=(conteudo.ibge);
                  } //end if.
                  else {
                    //CEP não Encontrado.
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                  }
                }
                
                function pesquisacep(valor) {
                  
                  //Nova variável "cep" somente com dígitos.
                  var cep = valor.replace(/\D/g, '');
                  
                  //Verifica se campo cep possui valor informado.
                  if (cep != "") {
                    
                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;
                    
                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {
                      
                      //Preenche os campos com "..." enquanto consulta webservice.
                      document.getElementById('rua').value="...";
                      document.getElementById('bairro').value="...";
                      document.getElementById('cidade').value="...";
                      document.getElementById('uf').value="...";
                      document.getElementById('ibge').value="...";
                      
                      //Cria um elemento javascript.
                      var script = document.createElement('script');
                      
                      //Sincroniza com o callback.
                      script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
                      
                      //Insere script no documento e carrega o conteúdo.
                      document.body.appendChild(script);
                      
                    } //end if.
                    else {
                      //cep é inválido.
                      limpa_formulário_cep();
                      alert("Formato de CEP inválido.");
                    }
                  } //end if.
                  else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                  }
                };
                
              </script>
              <!-- Inicio do formulario -->
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CEP</label>
                <div class="col-md-6 col-sm-6 ">
                  <input name="cep" type="text" id="cep"   maxlength="9"
                    onblur="pesquisacep(this.value);"class="form-control"  value="<?php printf($cep); ?> ">
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Rua</label>
                <div class="col-md-6 col-sm-6 ">
                  <input  name="adress"  type="text" id="rua" class="form-control"  value="<?php printf($adress); ?> ">
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Bairro</label>
                <div class="col-md-6 col-sm-6 ">
                  <input  name="bairro" type="text" id="bairro"  class="form-control"  value="<?php printf($bairro); ?> ">
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cidade</label>
                <div class="col-md-6 col-sm-6 ">
                  <input  name="city" type="text" id="cidade"  class="form-control"  value="<?php printf($city); ?> ">
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estado</label>
                <div class="col-md-6 col-sm-6 ">
                  <input   name="state" type="text" id="uf"  class="form-control"  value="<?php printf($state); ?> ">
                </div>
              </div>
              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">IBGE</label>
                <div class="col-md-6 col-sm-6 ">
                  <input  name="ibge" type="text" id="ibge"  class="form-control"  value="<?php printf($ibge); ?> ">
                </div>
              </div>
              
              <div class="ln_solid"></div>
              
              <label for="middle-name">Observação</label>
              
              <textarea id="observacao" class="form-control" name="observacao" 
              > </textarea>
              
              
              
              <div class="ln_solid"></div>
              
              
              <div class="ln_solid"></div>				
              
              <button type="reset" class="btn btn-primary"onclick="new PNotify({
                title: 'Limpado',
                text: 'Todos os Campos Limpos',
                type: 'info',
                styling: 'bootstrap3'
              });" />Limpar</button>
              <input type="submit" class="btn btn-primary" onclick="new PNotify({
                title: 'Registrado',
                text: 'Informações registrada!',
                type: 'success',
                styling: 'bootstrap3'
              });" value="Salvar" />
              
            </form>
            <div class="ln_solid"></div>
            <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
            
            
          </div>
        </div>
      </div>
    </div>
    
    
  </div>
</div>


