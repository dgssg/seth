<?php
include("database/database.php");



  ?>

  <div class="right_col" role="main">

    <div class="">

      <div class="page-title">
        <div class="title_left">
          <h3>HL7/FHIR</h3>
        </div>

        <div class="title_right">
          <div class="col-md-5 col-sm-5  form-group pull-right top_search">
            <div class="input-group">

              <span class="input-group-btn">

              </span>
            </div>
          </div>
        </div>
      </div>


      <div class="x_panel">
        <div class="x_title">
          <h2>Menu</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app" href="fhir" onclick="new PNotify({
              title: 'HL7/FHIR',
              text: 'HL7/FHIR',
              type: 'success',
              styling: 'bootstrap3'
            });">
            <i class="fa fa-database"></i> HL7/FHIR
          </a>

                      <a class="btn btn-app" href="fhir-configure" onclick="new PNotify({
                        title: 'Configurações HL7/FHIR',
                        text: 'Configurações HL7/FHIR',
                        type: 'success',
                        styling: 'bootstrap3'
                      });">
                      <i class="fa fa-cogs"></i> Configurações
                    </a>





        </div>
      </div>











              <div class="row" style="display: block;">
                <div class="col-md-12 col-sm-12  ">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>HL7/FHIR <small>Configuração</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Modalidade</strong> </i></a>
            <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Status</strong> </i></a>
            <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg3"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Tipo</strong> </i></a>

            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="#">Settings 1</a>
                              <a class="dropdown-item" href="#">Settings 2</a>
                            </div>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                      <!-- start accordion -->
                      <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                          <a class="panel-heading" role="tab" id="headingOne12" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne12" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">Modalidade</h4>
                          </a>
                          <div id="collapseOne12" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                              <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Modalidade</th>
                                    <th>Ação</th>
                                     <th>Ação</th>

                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $query = "SELECT * FROM fhir_mod ";


             if ($stmt = $conn->prepare($query)) {
             $stmt->execute();
             $stmt->bind_result($id, $nome);
             while ($stmt->fetch()) {
             //printf("%s, %s\n", $solicitante, $equipamento);
             //  }

             ?>
                                  <tr>

                                    <th scope="row"> <?php   printf($id);   ?></th>
                                    <td><?php   printf($nome);   ?></td>
                                    <td>   <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    )
window.location = 'backend/fhir-configure-fhir-mod-trash.php?id=<?php printf($id); ?>';
  }
})
">
                    <i class="fa fa-trash"></i> Excluir
                  </a>
               </td>


                                  </tr>
                                  <?php       }
             }
             ?>

                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        </div>

                              <!-- start accordion -->
                      <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                          <a class="panel-heading" role="tab" id="headingOne13" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne13" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">Status</h4>
                          </a>
                          <div id="collapseOne13" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                              <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Status</th>
                                    <th>Ação</th>


                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $query = "SELECT * FROM fhir_status ";


             if ($stmt = $conn->prepare($query)) {
             $stmt->execute();
             $stmt->bind_result($id, $nome);
             while ($stmt->fetch()) {
             //printf("%s, %s\n", $solicitante, $equipamento);
             //  }

             ?>
                                  <tr>

                                    <th scope="row"> <?php   printf($id);   ?></th>
                                    <td><?php   printf($nome);   ?></td>
                                    <td>   <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    )
window.location = 'backend/fhir-configure-fhir-status-trash.php?id=<?php printf($id); ?>';
  }
})
">
                    <i class="fa fa-trash"></i> Excluir
                  </a>
               </td>


                                  </tr>
                                  <?php       }
             }
             ?>

                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        </div>

                                <!-- start accordion -->
                      <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                          <a class="panel-heading" role="tab" id="headingOne14" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne14" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">Tipo</h4>
                          </a>
                          <div id="collapseOne14" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                              <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Tipo</th>
                                     <th>Ação</th>


                                  </tr>
                                </thead>
                                <tbody>
                                    <?php $query = "SELECT * FROM fhir_type ";


             if ($stmt = $conn->prepare($query)) {
             $stmt->execute();
             $stmt->bind_result($id, $nome);
             while ($stmt->fetch()) {
             //printf("%s, %s\n", $solicitante, $equipamento);
             //  }

             ?>
                                  <tr>

                                    <th scope="row"> <?php   printf($id);   ?></th>
                                    <td><?php   printf($nome);   ?></td>

                                    <td>   <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/fhir-configure-fhir-type-trash.php?id=<?php printf($id); ?>';
  }
})
">
                    <i class="fa fa-trash"></i> Excluir
                  </a>
               </td>

                                  </tr>
                                  <?php       }
             }
             ?>

                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        </div>

                  </div>
                </div>
             </div>
             </div>








    </div>
  </div>




<!-- -->
<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modalidade </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/fhir-configure-mod-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

          




        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->

<!-- -->
<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Status </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/fhir-configure-status-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

          




        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<!-- -->
<div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Tipo </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/fhir-configure-type-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

          




        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->














</div>
</div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
