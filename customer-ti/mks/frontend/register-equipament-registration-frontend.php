<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<?php
include("database/database.php");
$new_manufacture=$_COOKIE['new_manufacture'];
$id_group = ($_GET["id_group"]);
$codigo = ($_GET["codigo"]);
$id_familia = ($_GET["id_familia"]);
$patrimonio = ($_GET["patrimonio"]);
$ns = ($_GET["ns"]);
$instal = ($_GET["instal"]);
$fab = ($_GET["fab"]);
$data_laudo = ($_GET["data_laudo"]);
$garantia = ($_GET["garantia"]);
$nf = ($_GET["nf"]);
$vlr = ($_GET["vlr"]);
$obs = ($_GET["obs"]);


?>
<?php
    function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
        $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
        $con = 1;
        $var = '';
        while($con < $QuantidadeDeCaracteresDaChave ){
            $n = rand(0, 57); 
            if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
        }else{
            $var = $var.$n.$res[$n];
            $con++;
            }
        }
            $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
        }
        //chamando a função.
       // echo chaveAlfaNumerica(5);
     //   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
 * doing:
 *
 * $uniqid = $prefix . uniqid();
 * $uniqid = uniqid($prefix);
 
printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
 * required on some systems, like Cygwin. This makes uniqid()
 * produce a value like: 4b340550242239.64159797
 
printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
  public static function v3($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = md5($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 3
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function v4() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),

      // 16 bits for "time_mid"
      mt_rand(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  public static function v5($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = sha1($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 5
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function is_valid($uuid) {
    return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                      '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  }
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();
//echo $v4uuid; //chave da assinatura
?>


<script type="text/javascript">
$(document).ready(function(){
  // Smart Wizard
  $('#wizard').smartWizard({contentURL:'services/service.php?action=1',transitionEffect:'slideleft',onFinish:onFinishCallback});

  function onFinishCallback(){
    alert('Finish Called');
  }
});
</script>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
<form action="backend/register-equipament-backend.php" method="post" class="form-horizontal form-label-left">
  <!-- Tabs -->
  <div id="wizard_verticle" class="form_wizard wizard_verticle">
    <ul class="list-unstyled wizard_steps">
      <li>
        <a href="#step-11">
          <span class="step_no">1</span>
        </a>
      </li>
      <li>
        <a href="#step-22">
          <span class="step_no">2</span>
        </a>
      </li>
      <li>
        <a href="#step-33">
          <span class="step_no">3</span>
        </a>
      </li>
      <li>
        <a href="#step-44">
          <span class="step_no">4</span>
        </a>
      </li>
      <li>
        <a href="#step-55">
          <span class="step_no">5</span>
        </a>
      </li>
    </ul>

    <div id="step-11">
      <h2 class="StepTitle">Passo 1</h2>
      <!--  <form action="backend/register-equipament-backend.php" method="post" class="form-horizontal form-label-left"> -->

      <span class="section"> Dados do Equipamento</span>



      <div class="ln_solid"></div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Equipamento <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <div class="col-md-12 col-sm-12 form-group has-feedback" >
            <select type="text" class="form-control has-feedback-right" name="equipamento_familia" id="equipamento_familia"  placeholder="Grupo">
              <option  >Selecione um Equipamento	</option>
              <?php
              $sql = "SELECT  id, nome,fabricante,modelo,img FROM equipamento_familia WHERE trash = 1";
              if ($stmt = $conn->prepare($sql)) {
                $stmt->execute();
                $stmt->bind_result($id,$nome_equipamento,$fabricante_equipamento,$modelo_equipamento,$img);
                while ($stmt->fetch()) {
                  ?>
                  <option value="<?php printf($id);?>	" <?php if($id == $id_familia){ printf("selected"); }; ?> ><?php printf($nome_equipamento);?> <?php printf($modelo_equipamento);?> <?php printf($fabricante_equipamento);?>	</option>
                  <?php
                  // tira o resultado da busca da memória
                }
              }
              $stmt->close();
              ?>
            </select>
            <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
          </div>
        </div>
      </div>
      <script>
      $(document).ready(function() {
        $('#equipamento_familia').select2();
      });
      </script>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Rede de Cabeamento Estruturado <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <div class="col-md-12 col-sm-12 form-group has-feedback" >
            <select type="text" class="form-control has-feedback-right" name="rede" id="rede"  placeholder="Grupo">
              <option  >Selecione uma rede	</option>
              <?php
                $sql = "SELECT  id, name FROM cbe WHERE trash = 1";
                if ($stmt = $conn->prepare($sql)) {
                  $stmt->execute();
                  $stmt->bind_result($id,$nome_equipamento);
                  while ($stmt->fetch()) {
              ?>
              <option value="<?php printf($id);?>	"><?php printf($nome_equipamento);?> </option>
              <?php
                // tira o resultado da busca da memória
                }
                }
                $stmt->close();
              ?>
            </select>
            <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma rede "></span>
          </div>
        </div>
      </div>
      <script>
        $(document).ready(function() {
          $('#rede').select2();
        });
      </script>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 "></label>
        <div class="col-md-3 col-sm-3 ">
          <center>
            <button id="compose" class="btn btn-sm btn-success btn-block" class="form-control "type="button">Adicionar Equipamento</button>
          </center>
        </div>
      
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="codigo" name="codigo" class="form-control" value="<?php printf($codigo);?>"> 
        </div>
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="patrimonio">Patrimonio <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="patrimonio" name="patrimonio" class="form-control">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="numeroserie">N/Sº <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="numeroserie" name="numeroserie" class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="ip">I.P <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="ip" name="ip" class="form-control" placeholder="Ex: 192.168.0.1">
        </div>
      </div>

      <script>
        $(document).ready(function(){
          $('#ip').mask('099.099.099.099', {placeholder: '0.0.0.0'});
        });
      </script>
      <div class="form-group row">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="mac">MAC Address <span class="required"></span>
  </label>
  <div class="col-md-6 col-sm-6">
    <input 
      type="text" 
      id="mac" 
      name="mac" 
      class="form-control" 
      placeholder="Ex: 00:1A:2B:3C:4D:5E" 
      pattern="^([0-9A-Fa-f]{2}:){5}[0-9A-Fa-f]{2}$" 
      title="Insira um MAC Address no formato 00:1A:2B:3C:4D:5E" 
      required>
  </div>
</div>


      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Instalação <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input id="birthday2" name="data_instal" class="date-picker form-control" type="date">
        </div>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Fabricação <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">

          <input type="text" id="timepicker1" name="data_fab"  class="form-control " value="<?php printf($time_backlog); ?>"  pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="9999"  placeholder="YYYY" >
        </div>

      </div>
     
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Termino da Garantia <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input id="birthday2" name="data_val"class="date-picker form-control" type="date">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span class="required"></span>
        </label>
        <div class="input-group col-md-6 col-sm-6">
          <input type="text" id="nf" name="nf" class="form-control">
          <span class="input-group-btn">
            <button data-toggle="modal" data-target=".bs-example-modal-lg2" type="button" class="btn btn-primary">Adicionar</button>
          </span>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vlr">Valor (R$) <span class="required"></span>
        </label>
        <div class="input-group col-md-6 col-sm-6">
        <input name="vlr" id="vlr" type="text" class="form-control money2" onKeyPress="return(moeda(this,'.',',',event))">
          
        </div>
      </div>



     <!-- <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">observação<span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="obs" name="obs" class="form-control">
        </div>
      </div> -->
      <div class="ln_solid"></div>

<label for="message">observação:</label>
<textarea id="obs" class="form-control" name="obs"></textarea>

<div class="ln_solid"></div>



      <!--  </form> -->
    </div>
    <div id="step-22">
      <h2 class="StepTitle">Passo 2 </h2>
      <span class="section"> Dados da Localização &amp; Fornecedor</span>
      <div class="ln_solid"></div>

      <div class="form-group ">

      </label>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
        </label>
        <div class="input-group col-md-6 col-sm-6">
          <select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">
          <option value="">Selecione uma Unidade</option>
            <?php



            $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
            while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
              echo '<option></option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
            }
            ?>

          </select>
          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

        </div>
      </div>

      <script>
      $(document).ready(function() {
        $('#instituicao').select2();
      });
      </script>



      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
        </label>
        <div class="input-group col-md-6 col-sm-6">
          <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
            <option value="">Selecione um Setor</option>
          </select>
          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


        </div>
      </div>

      <script>
      $(document).ready(function() {
        $('#area').select2();
      });
      </script>

      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required">*</span>
        </label>
        <div class="input-group col-md-6 col-sm-6">
          <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area" required >
            <option value="" value="" disabled selected>Selecione uma Area</option>
          </select>
                              <div class="invalid-feedback">
    Por favor, selecione uma periodicidade.
  </div>
          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


        </div>
      </div>

      <script>
      $(document).ready(function() {
        $('#setor').select2();
      });
      </script>

      <div class="ln_solid"></div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Fornecedor <span class="required"></span>
        </label>
        <div class="input-group col-md-6 col-sm-6">
          <select type="text" class="form-control has-feedback-right" name="id_fornecedor" id="fonrnecedor"  placeholder="Setor">
            <option value=""> Selecione um Fornecedor</option>
            <?php
            $query = "SELECT id, empresa FROM fornecedor WHERE trash = 1";

            if ($stmt = $conn->prepare($query)) {
              $stmt->execute();
              $stmt->bind_result($id, $empresa);

              while ($stmt->fetch()) {
                ?>
                <option value="<?php printf($id);?>	"><?php printf($empresa);?></option>
                <?php
                // tira o resultado da busca da memória
              }
            }
            $stmt->close();
            ?>
          </select>
          <span class="input-group-btn">
            <button data-toggle="modal" data-target=".bs-example-modal-lg3" type="button" class="btn btn-primary">Adicionar</button>

            <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">
            </span>
          </span>										 </div>
        </div>

        <script>
        $(document).ready(function() {
          $('#fonrnecedor').select2();
        });
        </script>
  <div class="ln_solid"></div>
  <span class="section"> Aviso ao adicionar fornecedor</span>
      <p><strong>Os dados do fornecedor adicionado nesse passo, será automaticamente inserido no cadastro do equipamento ao finalizar. </strong></p>
      
         
          
            <input type="hidden" name="new_manufacture" value="<?php printf($new_manufacture); ?>" readonly="readonly" class="form-control ">
         
       
      </div>

    </div>
    <div id="step-33">
      <h2 class="StepTitle">Passo 3 </h2>
      <span class="section"> Dados de Parametrização</span>

      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Baixa</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="baixa"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>

        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Ativo</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="ativo"type="checkbox" class="js-switch" checked value="0" />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Inventario</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="inventario"type="checkbox" class="js-switch" checked value="0" />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Comodato</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="comodato"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Duplicidade</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="duplicidade"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Preventiva</label>
        <div class="">
          <label>
            <input name="preventive"type="checkbox" class="js-switch" checked value="0" />
          </label>
        </div>
      </div>

     
       
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Contrato</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="contrato"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Sofwtare</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="software"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Pacs</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="contrato"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">DICOM</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="dicom"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">RIS</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="ris"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Telemedicina</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="telemedicina"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">HL7/FHIR</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="contrato"type="checkbox" class="js-switch"  value="0"  />
            </label>
          </div>
        </div>
      </div>

    </div>
    <div id="step-44">
      <h2 class="StepTitle">Passo 4 </h2>
    <span class="section">Check List de Instalação</span>

<div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align">Check List</label>
    <div class="col-md-6 col-sm-6">
        <div class="">
            <label>
                <input name="list" type="checkbox" class="js-switch" id="list"  value="0" />
            </label>
        </div>
    </div>
</div>

<!-- Os campos que serão escondidos inicialmente -->
<div id="extra_fields" style="display:none;">
    <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Assinatura Responsável</label>
        <div class="col-md-6 col-sm-6">
            <div class="">
                <label>
                    <input name="ass_adm" type="checkbox" class="js-switch" checked value="0" />
                </label>
            </div>
        </div>
    </div>

    <!-- Instalação Field -->
    <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Instalação <span class="required"></span></label>
        <div class="input-group col-md-6 col-sm-6">
            <select type="text" class="form-control" name="instal" id="instal" placeholder="Instalação">
                <option value="">Selecione uma Instalação</option>
                <?php
                    $result_cat_post  = "SELECT id, nome FROM equipament_check_mod ";
                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                    while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post)) {
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                    }
                ?>
            </select>
            <span class="fa fa-building form-control-feedback right" aria-hidden="true" class="docs-tooltip" data-toggle="tooltip" title="Selecione a Instalação "></span>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#instal');
        });
    </script>

    <!-- Check List Itens -->
    <?php 
    $sql = "SELECT id, nome FROM equipament_check_list";
    if ($stmt = $conn->prepare($sql)) {
        $stmt->execute();
        $stmt->bind_result($id, $nome);
        while ($stmt->fetch()) {
    ?>
        <div class="form-control">
            <strong><?php printf($nome); ?></strong> <strong>-</strong>
            <input type="radio" class="flat" name="<?php printf($id); ?>" value="1"> OK  
            <input type="radio" class="flat" name="<?php printf($id); ?>" value="2"> NOK  
            <input type="radio" class="flat" name="<?php printf($id); ?>" value="3"> NA | 
            <strong>Observação/Justificativa</strong>  
            <input type="text" class="flat" name="t_<?php printf($id); ?>"> 
        </div>
    <?php
        }
        $stmt->close();
    }
    ?>

    <!-- Assinatura Digital -->
    <div class="modal-body">
        <h4><strong>Assinatura digital</strong></h4>
        <p><strong>Para Assinar digite o texto abaixo.</strong></p>
        <strong><?php $chave=chaveAlfaNumerica(5); echo "$chave" ?></strong>
        <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature">Assinatura</label>
            <div class="col-md-6 col-sm-6">
                <input type="text" id="assinature" name="assinature" class="form-control" />
            </div>
        </div>
        <input type="hidden" class="form-control" value="<?php printf($v4uuid); ?>" id="v4uuid" name="v4uuid">
        <input type="hidden" class="form-control" value="<?php printf($chave); ?>" id="chave" name="chave">
    </div>
</div>

<!-- Script para validar a assinatura -->
<script>
    var chave = "<?php echo $chave; ?>"; // Obtém a chave do PHP e armazena na variável JavaScript
    var assinaturaInput = document.getElementById("assinature");

    assinaturaInput.addEventListener("blur", function () {
        var assinatura = this.value;
        if (assinatura !== chave) {
            Swal.fire('A assinatura está incorreta!');
        }
    });
</script>

<!-- Script para mostrar/esconder campos com base no checkbox -->
<script>
    $(document).ready(function() {
        // Verifica o estado inicial do checkbox e ajusta a visibilidade
        toggleExtraFields();

        // Adiciona o evento de mudança no checkbox
        $('#list').change(function() {
            toggleExtraFields();
        });

        // Função para mostrar/esconder os campos
        function toggleExtraFields() {
            if($('#list').is(':checked')) {
                $('#extra_fields').show();
            } else {
                $('#extra_fields').hide();
            }
        }
    });
</script>



    </div>
    <div id="step-55">
      <h2 class="StepTitle">Passo 5 </h2>
      <span class="section"> Confirmação de Cadastro</span>
      <p><strong>Click no botão Cadastrar para salvar as informações previamente informada. Você pode voltar aos passos anteriores caso precise alterar ou confirmar antes de salvar as informações no sistema. Para isso basta clicar no numero correspondente do passo desejado. </strong></p>
      <div class="col-md-55">
      <div class="item form-group">
        <label class="col-form-label col-md-3 col-sm-3 label-align">Abrir Rotina</label>
        <div class="col-md-6 col-sm-6 ">
          <div class="">
            <label>
              <input name="routine"type="checkbox" class="js-switch" checked value="0" />
            </label>
          </div>
        </div>
      </div>


      </div>


    </div>
    <!-- <button type="submit" class="btn btn-primary">Salvar Informações</button> -->
  </div>



</div>
<!-- End SmartWizard Content -->

<!-- Posicionamento -->
<form></form>
<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Grupo</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/register-equipament-group-backend.php" method="post">
          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required"></span>
            </label>
            <div class="col-md-6 col-sm-6">
              <input type="text" id="nome" name="nome" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>
<!-- Posicionamento -->

<!-- compose -->
<div>
</fom>
<div class="compose col-md-6  " >
  <div class="compose-header">
    Familia Equipamento
    <button type="button" class="close compose-close">
      <span>×</span>
    </button>
  </div>

  <div class="compose-body" id="myModal">
    <div id="alerts"></div>
    <form  action="backend/register-equipament-family-backend.php" method="post">
      <div class="ln_solid"></div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="grupo">Grupo <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <div class="col-md-12 col-sm-12 form-group has-feedback" >
            <select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="grupo"  placeholder="Grupo">
              <?php
              $sql = "SELECT  id, nome FROM equipamento_grupo  WHERE trash = 1 ";
              if ($stmt = $conn->prepare($sql)) {
                $stmt->execute();
                $stmt->bind_result($id,$equipamento_grupo);
                while ($stmt->fetch()) {
                  ?>
                  <option value="<?php printf($id);?>	" <?php if($id == $id_group){ printf("selected"); }; ?>><?php printf($equipamento_grupo);?>	</option>
                  <?php
                  // tira o resultado da busca da memória
                }
              }
              $stmt->close();
              ?>
            </select>
            <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
          </div>
        </div>
      </div>
      <script>
      $(document).ready(function() {
        $('#grupo').select({
                              dropdownParent: $("#myModal")
                          });
      });
      </script>
        <script>
      $(document).ready(function() {
        $('#equipamento_grupo').select({
                              dropdownParent: $("#myModal")
                          });
      });
      </script>

      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 "></label>
        <div class="col-md-3 col-sm-3 ">
          <center>
            <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg1"type="button">Adicionar</button>
          </center>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome_familia">Nome <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="nome_familia" name="nome_familia" class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="fabricante_familia">Fabricante <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="fabricante_familia" name="fabricante_familia" class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="modelo_familia">Modelo <span class="required"></span>
        </label>
        <div class="col-md-6 col-sm-6">
          <input type="text" id="modelo_familia" name="modelo_familia" class="form-control">
        </div>
      </div>
   



    </div>

    <div class="compose-footer">
      <input type="submit" class="btn btn-primary" onclick="new PNotify({
        title: 'Registrado',
        text: 'Informações registrada!',
        type: 'success',
        styling: 'bootstrap3'
      });" value="Salvar"/>
    </div>

  </form>
  <form  class="dropzone" action="backend/equipament-group-upload-backend.php?anexo=<?php printf($anexo);?>" method="post">    </form >
  </div>
  <!-- /compose -->

  <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Nota Fiscal</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
      </form>
      <form  class="dropzone" action="backend/equipament-group-upload-nf-backend.php?anexo=<?php printf($anexo);?>" method="post">    </form >

      </div>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Adicionar Fornecedor</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>

        <form  action="backend/register-equipament-manufacture-backend.php" target="_blank"  method="post">

          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="empresa" id="empresa"  placeholder="Empresa">
            <span class="fa fa-industry form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite nome da Empresa "></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="cnpj" id="cnpj"  placeholder="CNPJ">
            <span class="fa fa-building form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite nome da Empresa "></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="seguimento" id="seguimento"  placeholder="Seguimento">
            <span class="fa fa-bookmark form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Seguimento "></span>
          </div>
          <div class="ln_solid"></div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="adress" id="adress"  placeholder="Endereço">
            <span class="fa fa-map-marker  form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Email"></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="city" id="city"  placeholder="Cidade">
            <span class="fa fa-map   form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Email"></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="state" id="state"  placeholder="Estado">
            <span class="fa fa-map-o  form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Email"></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="cep" id="cep"  placeholder="Cep">
            <span class="fa fa-street-view   form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Email"></span>
          </div>
          <div class="ln_solid"> </div>

          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="contato" id="contato"  placeholder="Contato">
            <span class="fa fa-user form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Contato "></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="email" id="email"  placeholder="Email">
            <span class="fa fa-envelope-o  form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Email"></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="telefone_1" id="telefone_1"  placeholder="Telefone Principal">
            <span class="fa fa-phone form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Telefone "></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="telefone_2" id="telefone_2"  placeholder="Telefone Segundario">
            <span class="fa fa-phone form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Telefone "></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">
            <input type="text" class="form-control has-feedback-left" name="observacao" id="observacao"  placeholder="Observação">
            <span class="fa fa-book form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite um obervação "></span>
          </div>
          <div class="col-md-4 col-sm-4  form-group has-feedback">







            <button type="reset" class="btn btn-primary"onclick="new PNotify({
              title: 'Limpado',
              text: 'Todos os Campos Limpos',
              type: 'info',
              styling: 'bootstrap3'
            });" />Limpar</button>
            <input type="submit" class="btn btn-primary" onclick="new PNotify({
              title: 'Registrado',
              text: 'Informações registrada!',
              type: 'success',
              styling: 'bootstrap3'
            });" value="Salvar" />

          </form>
          <div class="ln_solid"></div>

        </form>


      </div>
    </div>
  </div>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#instituicao').change(function(){
      $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
    });
  });
  </script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#area').change(function(){
      $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
    });
  });
  </script>
   <script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.FROMCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>
 <script type="text/javascript">
$(document).ready(function(){
  $("#equipamento_familia").change(function(){
    $('#c5_result_8').load('c_equipamento.php?equipamento='+$('#equipamento_familia').val());
  });
});
</script>  
