<?php
$codigoget = ($_GET["equipamento"]);
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
// Create connection

include("database/database.php");

?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>  Manutenção <small>Preventiva</small></h3>
      </div>


    </div>

    <div class="clearfix"></div>





    <div class="clearfix"></div>




    <!-- Posicionamento -->



    <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <a class="btn btn-app"  href="maintenance-preventive">
            <i class="glyphicon glyphicon-open"></i> Aberta
              <span class="badge bg-green">  <h3> <?php
                $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 1 ");
                $stmt->execute();
                $stmt->bind_result($result);
                while ( $stmt->fetch()) {
                echo "$result" ;
              }?> </h3></span>

          </a>

          <a class="btn btn-app"  href="maintenance-preventive-before">
            <i class="glyphicon glyphicon-export"></i> Atrasada
            <span class="badge bg-red">  <h3> <?php
              $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status = 2 ");
              $stmt->execute();
              $stmt->bind_result($result);
              while ( $stmt->fetch()) {
              echo "$result" ;
            }?> </h3></span>
          </a>

          <a class="btn btn-app"  href="maintenance-preventive-close">
            <i class="glyphicon glyphicon-save"></i> Fechada
          </a>

          <a class="btn btn-app"  href="maintenance-preventive-print">
            <i class="glyphicon glyphicon-print"></i> Impressão
          </a>

          <a class="btn btn-app"  href="maintenance-preventive-print-register">
            <i class="glyphicon glyphicon-folder-open"></i> Registro de Impressão
          </a>

          <a class="btn btn-app"  href="maintenance-preventive-control">
            <i class="fa fa-check-square-o"></i> Controle
          </a>
          
          <a class="btn btn-app"  href="maintenance-preventive-control-return">
            <i class="fa fa-pencil-square-o"></i> Retorno
          </a>


        </div>
      </div>
      <div class="x_panel">
        <div class="x_title">
          <h2>Sub Menu</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <a class="btn btn-app"  href="maintenance-preventive-label-print" target="_blank" onclick="new PNotify({
                         title: 'Etiqueta',
                         text: 'Abrir Etiqueta',
                         type: 'sucess',
                         styling: 'bootstrap3'
                     });">
             <i class="fa fa-fax"></i> Rótulos
           </a>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Filtro</button>
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Filtro MP</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="maintenance-preventive-filter-backend.php" method="post">


                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao" required="required" placeholder="Unidade">
                      <option value="">Selecione uma Unidade</option>
                          <?php



                          $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
                          <option value="">Selecione um Setor</option>
                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>

                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
                          <option value="">Selecione uma Area</option>
                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Filtrar</button>
                  </div>
      </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="x_panel">
          <div class="x_title">
            <h2>Informação</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                  class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <div class="animated flipInY col-lg-2 col-md-1 col-sm-1  ">
                <div class="tile-stats">
                  <div class="icon">
                  </div>
                  <center>
                    <div class="count">
                      <?php
                      $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive WHERE id_status like '1' ");
                      $stmt->execute();
                      $stmt->bind_result($result);
                      while ( $stmt->fetch()) {
                        printf($result);
                      }
                      ?> </div>

                      <h2>Aberta</h2>

                    </div>
                  </center>
                </div>
                <div class="animated flipInY col-lg-2 col-md-1 col-sm-1  ">
                  <div class="tile-stats">
                    <div class="icon">
                    </div>
                    <center>
                      <div class="count">
                        <?php
                        $cont=0;
                        $stmt = $conn->prepare(" SELECT COUNT(id_routine) AS 'duplicidade',  id_routine FROM maintenance_preventive WHERE id_status like '1' AND id_routine = id_routine GROUP by id_routine");
                        $stmt->execute();
                        $stmt->bind_result($duplicidade,$rotina);
                        while ( $stmt->fetch()) {
                          if($duplicidade >1){
                            $cont=$cont+1;
                          }

                        }
                        printf($cont);
                        ?> </div>

                        <h2>Duplicidade</h2>

                      </div>
                    </center>
                  </div>
                  <div class="animated flipInY col-lg-2 col-md-1 col-sm-1  ">
                    <div class="tile-stats">
                      <div class="icon">
                      </div>
                      <center>
                        <div class="count">
                          <?php
                          $cont2=0;;
                          $stmt = $conn->prepare(" SELECT maintenance_preventive.id, maintenance_preventive.id_routine, maintenance_preventive.date_start, maintenance_routine.periodicidade FROM maintenance_preventive  INNER JOIN maintenance_routine on maintenance_routine.id = maintenance_preventive.id_routine WHERE  id_status like '1' ");
                          $stmt->execute();
                          $stmt->bind_result($id,$id_routine,$programada,$periodicidade);
                          while ( $stmt->fetch()) {

                            switch($periodicidade){
                              case 1:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }



                              break;
                              case 5:

             
                                $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                            
                                if($today > $new_date){
                            
                                  $cont2=$cont2+1;                                }
                            
                            
                            
                                    break;
                              
                              case 7:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }



                              break;
                              case 14:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }



                              break;
                              case 21:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }



                              break;
                              case 28:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }


                              break;
                              case 30:



                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }

                              break;
                              case 60:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }


                              break;
                              case 90:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }


                              break;
                              case 120:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }



                              break;
                              case 180:
                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }




                              break;
                              case 365:


                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }

                              break;
                              case 730:



                              $new_date=date('Y-m-d', strtotime($programada.' + 1 months'));
                              if($today > $new_date){

                                $cont2=$cont2+1;
                              }
                              break;
                            }

                          }

                          printf($cont2);
                          ?> </div>

                          <h2>Atrasada</h2>

                        </div>
                      </center>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-2 col-sm-2  ">
                      <div class="alert alert-success">

                        <li><i class="fa fa-clock-o" aria-hidden="true"> </i>       MP Atrasada.</li>
                        <li><i class="fa fa-files-o" aria-hidden="true"></i> MP Duplicada.</li>
                        <br>
                        <li> Legenda</li>
                      </div>
                    </div>

                  </div>
                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista MP Aberta</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                      <?php include 'frontend/maintenance-preventive-open-filter-frontend.php';?>

                    </div>
                  </div>




                </div>
              </div>
              <!-- /compose -->
              <script type="text/javascript">
              $(document).ready(function(){
                $('#instituicao').change(function(){
                  $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
                });
              });
              </script>
              <script type="text/javascript">
              $(document).ready(function(){
                $('#area').change(function(){
                  $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
                });
              });
              </script>
