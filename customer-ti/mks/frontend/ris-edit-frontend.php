<?php
$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT ris.counterparty_nome,ris.counterparty_telephone,ris.counterparty_email,ris.manufacture,ris.cnpj,ris.cep,ris.adress,ris.city,ris.state,ris.ibge,ris.district,ris.nome,ris.telephone,ris.email,ris.id_instituicao,ris.obs,ris.upgrade,ris.reg_date,ris.id_ris_status, ris.id_ris_type, ris.id_ris_mod, ris.id_instituicao,ris_type.nome,ris_status.nome,ris_mod.nome,ris.id,ris.number_ris,ris.object,ris.manufacture FROM ris INNER JOIN ris_status ON ris_status.id = ris.id_ris_status INNER JOIN ris_mod ON ris_mod.id = ris.id_ris_mod  INNER JOIN ris_type ON ris_type.id = ris.id_ris_type WHERE ris.id = $codigoget ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($counterparty_nome,$counterparty_telephone,$counterparty_email,$manufacture,$cnpj,$cep,$adress,$city,$state,$ibge,$district,$nome,$telephone,$email,$id_instituicao,$obs_ris,$upgrade,$reg_date,$id_ris_status,$id_ris_type,$id_ris_mod,$id_instituicao,$ris_type,$ris_status,$ris_mod,$id,$number_ris,$object,$manufacture);
  while ($stmt->fetch()) {
    //pintf("%s, %s\n", $solicitante, $equipamento);
  }
}
// Create connection
setlocale(LC_ALL, 'pt_BR');

?>

<div class="right_col" role="main">

  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>Edição</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <!-- page content -->









      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edição <small>de RIS</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form action="backend/ris-edit-backend.php" method="post">

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_instituicao">Unidade <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_instituicao" id="id_instituicao"  placeholder="Unidade">
                      <option value="">Selecione uma Unidade</option>
                      <?php



                      $result_cat_post  = "SELECT  id, codigo,instituicao FROM instituicao WHERE trash = 1";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_instituicao) {
                          echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['instituicao'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['codigo'].' - '.$row_cat_post['instituicao'].' </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Unidade "></span>

                  </div>
                </div>

                <script>
                $(document).ready(function() {
                  $('#id_instituicao').select2();
                });
                </script>


                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nº do Contrato</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="number_ris" class="form-control" type="text" name="number_ris" value="<?php printf($number_ris); ?>"  >
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_ris_mod">Modalidade <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_ris_mod" id="id_ris_mod"  placeholder="Modalidade">
                      <option value="">Selecione uma Modalidade</option>
                      <?php



                      $result_cat_post  = "SELECT  id, nome FROM ris_mod ";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_ris_mod) {
                          echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Unidade "></span>

                  </div>
                </div>
                <script>
                $(document).ready(function() {
                  $('#id_ris_mod').select2();
                });
                </script>
                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_ris_type">Tipo <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_ris_type" id="id_ris_type"  placeholder="Tipo">
                      <option value="">Selecione um Tipo</option>
                      <?php



                      $result_cat_post  = "SELECT  id, nome FROM ris_type ";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_ris_type) {
                          echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Tipo "></span>

                  </div>
                </div>

                <script>
                $(document).ready(function() {
                  $('#id_ris_type').select2();
                });
                </script>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Objeto</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="object" class="form-control" type="text" name="object" value="<?php printf($object); ?>"  >
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_ris_status">Status <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_ris_status" id="id_ris_status"  placeholder="Status">
                      <option value="">Selecione um Status</option>
                      <?php



                      $result_cat_post  = "SELECT  id, nome FROM ris_status ";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if ($row_cat_post['id'] == $id_ris_status) {
                          echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'  </option>';
                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'  </option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Status "></span>

                  </div>
                </div>

                <script>
                $(document).ready(function() {
                  $('#id_ris_status').select2();
                });
                </script>

                <div class="ln_solid"></div>

                <h2>Contraparte <small>de RIS</small></h2>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="counterparty_nome" class="form-control" type="text" name="counterparty_nome"  value="<?php printf($counterparty_nome); ?> " >
                  </div>
                </div>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Telefone</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="counterparty_telephone" class="form-control" type="text" name="counterparty_telephone"  value="<?php printf($counterparty_telephone); ?> " >
                  </div>
                </div>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Email </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="counterparty_email" class="form-control" type="text" name="counterparty_email"  value="<?php printf($counterparty_email); ?> " >
                  </div>
                </div>
                <div class="ln_solid"></div>
                <h2>Empresa <small>de RIS</small></h2>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="manufacture" class="form-control" type="text" name="manufacture"  value="<?php printf($manufacture); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CNPJ </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="cnpj" class="form-control" type="text" name="cnpj"  value="<?php printf($cnpj); ?> " >
                  </div>
                </div>
                <div class="ln_solid"></div>
                <h2>Endereço da Empresa <small>de RIS</small></h2>
                <script>

                function limpa_formulário_cep() {
                  //Limpa valores do formulário de cep.
                  document.getElementById('rua').value=("");
                  document.getElementById('bairro').value=("");
                  document.getElementById('cidade').value=("");
                  document.getElementById('uf').value=("");
                  document.getElementById('ibge').value=("");
                }

                function meu_callback(conteudo) {
                  if (!("erro" in conteudo)) {
                    //Atualiza os campos com os valores.
                    document.getElementById('rua').value=(conteudo.logradouro);
                    document.getElementById('bairro').value=(conteudo.bairro);
                    document.getElementById('cidade').value=(conteudo.localidade);
                    document.getElementById('uf').value=(conteudo.uf);
                    document.getElementById('ibge').value=(conteudo.ibge);
                  } //end if.
                  else {
                    //CEP não Encontrado.
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                  }
                }

                function pesquisacep(valor) {

                  //Nova variável "cep" somente com dígitos.
                  var cep = valor.replace(/\D/g, '');

                  //Verifica se campo cep possui valor informado.
                  if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                      //Preenche os campos com "..." enquanto consulta webservice.
                      document.getElementById('rua').value="...";
                      document.getElementById('bairro').value="...";
                      document.getElementById('cidade').value="...";
                      document.getElementById('uf').value="...";
                      document.getElementById('ibge').value="...";

                      //Cria um elemento javascript.
                      var script = document.createElement('script');

                      //Sincroniza com o callback.
                      script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                      //Insere script no documento e carrega o conteúdo.
                      document.body.appendChild(script);

                    } //end if.
                    else {
                      //cep é inválido.
                      limpa_formulário_cep();
                      alert("Formato de CEP inválido.");
                    }
                  } //end if.
                  else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                  }
                };

                </script>
                <!-- Inicio do formulario -->
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CEP</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input name="cep" type="text" id="cep"   maxlength="9"
                    onblur="pesquisacep(this.value);"class="form-control"  value="<?php printf($cep); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Rua</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  name="adress"  type="text" id="rua" class="form-control"  value="<?php printf($adress); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Bairro</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  name="district" type="text" id="bairro"  class="form-control"  value="<?php printf($district); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cidade</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  name="city" type="text" id="cidade"  class="form-control"  value="<?php printf($city); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estado</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input   name="state" type="text" id="uf"  class="form-control"  value="<?php printf($state); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">IBGE</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  name="ibge" type="text" id="ibge"  class="form-control"  value="<?php printf($ibge); ?> ">
                  </div>
                </div>
                <div class="ln_solid"></div>
                <h2>Contato <small>de RIS</small></h2>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Contato</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($nome); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Email</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="email" class="form-control" type="text" name="email"  value="<?php printf($email); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Telefone </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="telephone" class="form-control" type="text" name="telephone"  value="<?php printf($telephone); ?> " >
                  </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                  <div class="col-md-3 col-sm-3 ">
                    <center>
                      <button id="compose" class="btn btn-sm btn-success btn-block" class="form-control "type="button">Observação</button>
                    </center>
                  </div>
                </div>
 
                <div class="ln_solid"></div>

                <label for="message_mp">Observação :</label>
                          <textarea id="obs_ris" class="form-control" name="obs_ris" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($obs_ris); ?>" value="<?php printf($obs_ris); ?>" readonly="readonly"><?php printf($obs_ris); ?></textarea>

                            <div class="ln_solid"></div>






                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input readonly="readonly" type="text"  class="form-control"  value="<?php printf($reg_date); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                  </div>
                </div>



                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                  <div class="col-md-3 col-sm-3 ">
                    <center>
                      <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                    </center>
                  </div>
                </div>


              </form>






            </div>
          </div>
        </div>
      </div>

      <div class="x_panel">
        <div class="x_title">
          <h2>Equipamentos do RIS</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form action="backend/ris-equipament-backend.php?id_equipamento=<?php printf($codigoget); ?>" method="post">

              <select multiple="multiple" size="20"  name="duallistbox_demo1[]" title="duallistbox_demo1[]">

                <?php

                $stmt = $conn->prepare(" SELECT id_equipament FROM ris_equipament WHERE id_ris = $codigoget ");

                $stmt->execute();
                $stmt->bind_result($id_equipamento);
                while ( $stmt->fetch()) {

                }

                $stmt = $conn->prepare(" SELECT COUNT(id) FROM equipamento ");

                $stmt->execute();
                $stmt->bind_result($result);
                while ( $stmt->fetch()) {

                }

                $id_equipamento=explode(",",$id_equipamento);
                $query = "SELECT equipamento.id, equipamento.codigo,equipamento_familia.nome, equipamento_familia.fabricante,equipamento_familia.modelo FROM equipamento INNER JOIN equipamento_familia ON equipamento_familia.id =   equipamento.id_equipamento_familia WHERE equipamento.trash = 1 and equipamento.contrato = 0 ORDER BY equipamento.id";
                //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $codigo,$nome,$fabricante,$modelo);
                  while ($stmt->fetch()) {
                    ?>
                    <option value="<?php printf($id);?>	" <?php for($i = 0; $i <= $result; $i++){ if($id_equipamento[$i]==$id){printf("selected");}} ?> ><?php printf($codigo);?> <?php printf($nome);?> <?php printf($modelo);?>	<?php printf($fabricante);?></option>
                    <?php
                    // tira o resultado da busca da memória
                  }
                }
                $stmt->close();
                ?>
              </select>
              <br>



              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 "></label>
                <div class="col-md-3 col-sm-3 ">
                  <center>
                    <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                  </center>
                </div>
              </div>


            </form>
          </div>
        </div>

        <div class="clearfix"></div>


        <!-- 1 -->
        <link href="dropzone.css" type="text/css" rel="stylesheet" />

        <!-- 2 -->
        <script src="dropzone.min.js"></script>
        <!-- Posicionamento -->

        <div class="x_panel">
          <div class="x_title">
            <h2>Anexos</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2" ><i class="fa fa-plus"></i></a>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                    class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <!--arquivo -->
                <?php
                $query="SELECT id,  title, file, upgrade, reg_date FROM ris_dropbox WHERE id_ris like '$codigoget'";
                $row=1;
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id,  $titulo, $file, $upgrade, $reg_date);

                  ?>
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>#</th>
                        <th>Titulo</th>
                        <th>Registro</th>
                        <th>Atualização</th>
                        <th>Ação</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id); ?></td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($upgrade); ?></td>
                          <td> <a class="btn btn-app"   href="dropzone/ris/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
                            title: 'Visualizar',
                            text: 'Visualizar Anexo',
                            type: 'info',
                            styling: 'bootstrap3'
                          });">
                          <i class="fa fa-file"></i> Anexo
                        </a>
                        <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/ris-dropzone-trash.php?id=<?php printf($id); ?>&id_ris=<?php printf($codigoget); ?> ';
  }
})
">
                        <i class="fa fa-trash"></i> Excluir
                      </a>
                    </td>

                  </tr>
                  <?php  $row=$row+1; }
                }   ?>
              </tbody>
            </table>




          </div>
        </div>

        <!-- Posicionamento -->

        <!-- Registro -->
        <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>

              <div class="modal-body">

                <!-- Registro forms-->
                <form action="backend/ris-edit-dropzone-backend.php?id_ris=<?php printf($codigoget);?>" method="post">
                  <div class="ln_solid"></div>


                  <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                      class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="titulo" ></div>
                      </div>




                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar Informações</button>

                      </div>
                    </div>
                  </form>
                  <form  class="dropzone" action="backend/ris-edit-dropzone-upload-backend.php" method="post">
                  </form >
                </div>
              </div>
            </div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Período</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg3" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!--arquivo -->
                    <?php
                    $query="SELECT id, date_start, date_end,percentage,percentage_applied,vlr,obs, reg_date, upgrade FROM ris_period WHERE id_ris like '$codigoget'";
                    $row=1;
                    if ($stmt = $conn->prepare($query)) {
                      $stmt->execute();
                      $stmt->bind_result($id, $date_start, $date_end,$percentage,$percentage_applied,$vlr,$obs, $reg_date, $upgrade);

                      ?>
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Data de Inicio</th>
                            <th>Data de Fim</th>
                            <th>Porcentagem</th>
                            <th>Porcentagem aplicada</th>
                            <th>Valor</th>
                            <th>Observação</th>
                            <th>Ação</th>

                          </tr>
                        </thead>
                        <tbody>
                          <?php  while ($stmt->fetch()) { ?>

                            <tr>
                              <th scope="row"><?php printf($row); ?></th>
                              <td><?php printf($date_start); ?></td>
                              <td><?php printf($date_end); ?></td>
                              <td><?php printf($percentage); ?></td>
                              <td><?php printf($percentage_applied); ?></td>
                              <td><?php printf($vlr); ?></td>
                              <td><?php printf($obs); ?></td>
                              <td>
                                <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/ris-period-trash.php?id=<?php printf($id); ?>&id_ris=<?php printf($codigoget); ?> ';
  }
})
">
                                <i class="fa fa-trash"></i> Excluir
                              </a>
                                <a class="btn btn-app" onclick="
                                  Swal.fire({
                                    title: 'Editar Registro',
                                    html: `<form id='editForm'>
                                    <label for='date_start'>Data de Inicio:</label>
                                    <input type='date' class='swal2-input' id='date_start' name='date_start' value='<?php echo htmlspecialchars($date_start); ?>'>
                                    
                                    <label for='date_end'>Data de Fim:</label>
                                    <input type='date' class='swal2-input' id='date_end' name='date_end' value='<?php echo htmlspecialchars($date_end); ?>'>
                                    
                                    <label for='percentage'>Porcentagem:</label>
                                    <input type='number' class='swal2-input' id='percentage' name='percentage' value='<?php echo htmlspecialchars($percentage); ?>'>
                                    
                                    <label for='percentage_applied'>Porcentagem Aplicada:</label>
                                    <input type='number' class='swal2-input' id='percentage_applied' name='percentage_applied' value='<?php echo htmlspecialchars($percentage_applied); ?>'>
                                    
                                    <label for='vlr'>Porcentagem Valor:</label>
                                    <input type='text' class='swal2-input' id='vlr' name='vlr' value='<?php echo htmlspecialchars($vlr); ?>'>
                                    
                                    <label for='obs'>Porcentagem Observação:</label>
                                    <input type='text' class='swal2-input' id='obs' name='obs' value='<?php echo htmlspecialchars($obs); ?>'>
                                    </form>`,
                                    
                                    icon: 'info',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Salvar Alterações'
                                  }).then((result) => {
                                    if (result.isConfirmed) {
                                      // Obtenha os dados do formulário
                                      var formData = new FormData(document.getElementById('editForm'));
                                      
                                      var urlParams = '?id=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                      
                                      
                                      // Exemplo de código AJAX
                                      $.ajax({
                                        url: 'backend/ris-edit-period-upgrade-backend.php?id=<?php printf($id); ?>&id_ris=<?php printf($codigoget); ?>',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                        type: 'POST',
                                        data: formData,
                                        processData: false,
                                        contentType: false,
                                        success: function(response) {
                                          
                                          Swal.fire('Alterações Salvas!', '', 'success');
                                          // Adicione aqui qualquer ação adicional após o sucesso
                                          location.href = location.origin + location.pathname + urlParams;
                                          
                                        },
                                        error: function(error) {
                                          console.error('Erro ao salvar alterações:', error);
                                          Swal.fire('Erro ao salvar alterações!', '', 'error');
                                        }
                                      });
                                    }
                                  });
                                ">
                                  <i class="fa fa-edit"></i> Editar
                                </a>
                            </td>

                          </tr>
                          <?php  $row=$row+1; }
                        }   ?>
                      </tbody>
                    </table>




                  </div>
                </div>

                <!-- Posicionamento -->

                <!-- Registro -->
                <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                      <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel2">Periodo</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                      </div>

                      <div class="modal-body">

                        <!-- Registro forms-->
                        <form action="backend/ris-edit-period-backend.php?id_ris=<?php printf($codigoget);?>" method="post">
                          <div class="ln_solid"></div>


                          <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Data Inicio<span
                              class="required"></span></label>
                              <div class="col-md-6 col-sm-6">
                                <input class="form-control" class='date' type="date" name="date_start" id="date_start"></div>
                              </div>
                              <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Data Fim<span
                                  class="required"></span></label>
                                  <div class="col-md-6 col-sm-6">
                                    <input class="form-control" class='date' type="date" name="date_end" id="date_end"></div>
                                  </div>
                                  <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Porcentagem<span
                                      class="required"></span></label>
                                      <div class="col-md-6 col-sm-6">
                                        <input class="form-control" class='date' type="number" name="percentage" id="percentage"></div>
                                      </div>
                                      <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Porcentagem Aplicada<span
                                          class="required"></span></label>
                                          <div class="col-md-6 col-sm-6">
                                            <input class="form-control" class='date' type="number" name="percentage_applied" id="percentage_applied"></div>
                                          </div>
                                          <div class="field item form-group">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align">valor<span
                                              class="required"></span></label>
                                              <div class="col-md-6 col-sm-6">
                      <input name="vlr" id="vlr" type="text" class="form-control money2" onKeyPress="return(moeda(this,'.',',',event))">
                                              </div>
                                              </div>
                                              <div class="field item form-group">
                                                <label class="col-form-label col-md-3 col-sm-3  label-align">Observação<span
                                                  class="required"></span></label>
                                                  <div class="col-md-6 col-sm-6">
                                                    <input class="form-control" class='date' type="text" name="obs" id="obs"></div>
                                                  </div>




                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                <button type="submit" class="btn btn-primary">Salvar Informações</button>

                                              </div>
                                            </div>
                                          </form>

                                        </div>
                                      </div>
                                    </div>
                                    </div>
                                    <div class="x_panel">
                                      <div class="x_title">
                                        <h2>Reajuste</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg4" ><i class="fa fa-plus"></i></a>
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                                class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                  <li><a href="#">Settings 1</a>
                                                  </li>
                                                  <li><a href="#">Settings 2</a>
                                                  </li>
                                                </ul>
                                              </li>
                                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                                              </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                          </div>
                                          <div class="x_content">

                                            <!--arquivo -->
                                            <?php
                                            $query="SELECT id, date_start, index_number,period,obs, reg_date, upgrade FROM ris_readjustment WHERE id_ris like '$codigoget'";
                                            $row=1;
                                            if ($stmt = $conn->prepare($query)) {
                                              $stmt->execute();
                                              $stmt->bind_result($id, $date_start, $index_number,$period,$obs, $reg_date, $upgrade);

                                              ?>
                                              <table class="table table-striped" id="minhaTabela">
                                                <thead>
                                                  <tr>
                                                    <th>#</th>
                                                    <th>Data</th>
                                                    <th>Índice</th>
                                                    <th>Periodo</th>
                                                    <th>Observação</th>
                                                    <th>Ação</th>

                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <?php  while ($stmt->fetch()) { ?>

                                                    <tr>
                                                      <th scope="row"  ><?php printf($row); ?></th>
                                                      <td><?php printf($date_start); ?></td>
                                                      <td><?php printf($index_number); ?></td>
                                                      <td><?php printf($period); ?></td>

                                                      <td><?php printf($obs); ?></td>
                                                      <td>
                                                        <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/ris-readjustment-trash.php?id=<?php printf($id); ?>&id_ris=<?php printf($codigoget); ?>';
  }
})
">
                                                        <i class="fa fa-trash"></i> Excluir
                                                      </a>
                                                        <a class="btn btn-app" onclick="
                                                          Swal.fire({
                                                            title: 'Editar Registro',
                                                            html: `<form id='editForm'>
                                                            <label for='date_start'>Data:</label>
                                                            <input type='date' class='swal2-input' id='date_start' name='date_start' value='<?php echo htmlspecialchars($date_start); ?>'>
                                                            
                                                            <label for='percentage'>Índice:</label>
                                                            <input type='number' class='swal2-input' id='percentage' name='percentage' value='<?php echo htmlspecialchars($index_number); ?>'>
                                                            
                                                            <label for='percentage_applied'>Período:</label>
                                                            <input type='number' class='swal2-input' id='percentage_applied' name='percentage_applied' value='<?php echo htmlspecialchars($period); ?>'>
                                                            
                                                            <label for='obs'>Observação:</label>
                                                            <input type='text' class='swal2-input' id='obs' name='obs' value='<?php echo htmlspecialchars($obs); ?>'>
                                                            </form>`,

                                                            icon: 'info',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: 'Salvar Alterações'
                                                          }).then((result) => {
                                                            if (result.isConfirmed) {
                                                              // Obtenha os dados do formulário
                                                              var formData = new FormData(document.getElementById('editForm'));
                                                              
                                                              var urlParams = '?id=' + encodeURIComponent('<?php echo $codigoget; ?>');


                                                              // Exemplo de código AJAX
                                                              $.ajax({
                                                                url: 'backend/ris-readjustment-edit-backend.php?id=<?php printf($id); ?>&id_ris=<?php printf($codigoget); ?>',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                                                type: 'POST',
                                                                data: formData,
                                                                processData: false,
                                                                contentType: false,
                                                                success: function(response) {
                                                                  
                                                                  Swal.fire('Alterações Salvas!', '', 'success');
                                                                  // Adicione aqui qualquer ação adicional após o sucesso
                                                                  location.href = location.origin + location.pathname + urlParams;
  
                                                                },
                                                                error: function(error) {
                                                                  console.error('Erro ao salvar alterações:', error);
                                                                  Swal.fire('Erro ao salvar alterações!', '', 'error');
                                                                }
                                                              });
                                                            }
                                                          });
                                                        ">
                                                          <i class="fa fa-edit"></i> Editar
                                                        </a>
                                                      
                                                    </td>

                                                  </tr>
                                                  <?php  $row=$row+1; }
                                                }   ?>
                                              </tbody>
                                            </table>




                                          </div>
                                        </div>

                                        <!-- Posicionamento -->

                                        <!-- Registro -->
                                        <div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-hidden="true">
                                          <div class="modal-dialog modal-lg">
                                            <div class="modal-content">

                                              <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel2">Reajuste</h4>
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                </button>
                                              </div>

                                              <div class="modal-body">

                                                <!-- Registro forms-->
                                                <form action="backend/ris-edit-readjustment-backend.php?id_ris=<?php printf($codigoget);?>" method="post">
                                                  <div class="ln_solid"></div>


                                                  <div class="field item form-group">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Data Base<span
                                                      class="required"></span></label>
                                                      <div class="col-md-6 col-sm-6">
                                                        <input class="form-control" class='date' type="date" name="date_start" id="date_start"></div>
                                                      </div>

                                                          <div class="field item form-group">
                                                            <label class="col-form-label col-md-3 col-sm-3  label-align">Indice<span
                                                              class="required"></span></label>
                                                              <div class="col-md-6 col-sm-6">
                                                                <input class="form-control" class='date' type="number" name="percentage" id="percentage"></div>
                                                              </div>
                                                              <div class="field item form-group">
                                                                <label class="col-form-label col-md-3 col-sm-3  label-align">Periodo<span
                                                                  class="required"></span></label>
                                                                  <div class="col-md-6 col-sm-6">
                                                                    <input class="form-control" class='date' type="number" name="percentage_applied" id="percentage_applied"></div>
                                                                  </div>

                                                                      <div class="field item form-group">
                                                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Observação<span
                                                                          class="required"></span></label>
                                                                          <div class="col-md-6 col-sm-6">
                                                                            <input class="form-control" class='date' type="text" name="obs" id="obs"></div>
                                                                          </div>




                                                                      <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                                        <button type="submit" class="btn btn-primary">Salvar Informações</button>

                                                                      </div>
                                                                    </div>
                                                                  </form>

                                                                </div>
                                                              </div>
                                                            </div>

                                    <div class="x_panel">
                                      <div class="x_title">
                                        <h2>Ação</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                          </li>
                                          <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                              class="fa fa-wrench"></i></a>
                                              <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                              </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                          </ul>
                                          <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">

                                          <a class="btn btn-app"  href="ris">
                                            <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                                          </a>




                                        </div>
                                      </div>




                                    </div>
                                  </div>
                                  <!-- /page content -->

                                  <!-- /compose -->
                                  <!-- compose -->
                                  <div class="compose col-md-6  ">
                                    <div class="compose-header">
                                      Observação
                                      <button type="button" class="close compose-close">
                                        <span>×</span>
                                      </button>
                                    </div>
                                    <form  action="backend/ris-comment-backend.php?id=<?php printf($codigoget);?>" method="post">
                                      <div class="compose-body">
                                        <div id="alerts"></div>


                                        <input size="90" type="text" id="editor" name="editor" value="<?php printf($obs_ris);?>" class="editor-wrapper">


                                      </div>

                                      <div class="compose-footer">
                                        <button class="btn btn-sm btn-success" type="submit">Salvar</button>
                                      </div>
                                    </form>
                                  </div>
                                  <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
                                  <script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.FROMCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>  