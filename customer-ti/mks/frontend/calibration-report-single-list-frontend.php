<?php
include("database/database.php");
$query = "SELECT   calibration_single.id_equipamento_grupo,calibration_single.serie_number,calibration_single.documentation,calibration_single.customer,calibration_single.equipament,calibration_single.val,calibration_single.id_responsavel,calibration_single.id_colaborador,calibration_single.codigo,calibration_single.id_equipamento,calibration_single.id_manufacture,calibration_single.id_equipamento_grupo,calibration_single.temp,calibration_single.hum,calibration_single.obs,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade,calibration_single.procedure_cal,calibration_parameter_manufacture.nome,calibration_single.id, calibration_single.data_start, calibration_single.val, calibration_single.ven, calibration_single.status FROM calibration_single  LEFT JOIN calibration_parameter_manufacture on calibration_parameter_manufacture.id =  calibration_single.id_manufacture LEFT JOIN colaborador on colaborador.id = calibration_single.id_colaborador where calibration_single.trash != 0 order by calibration_single.id DESC";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id_equipamento_grupo,$serie_number,$documentation,$customer,$equipament,$date_validation,$id_responsavel,$id_colaborador,$codigo_caliration,$id_equipamento,$id_manufacture,$id_equipamento_grupo,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status);




?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>#</th>
                          <th>Laudo</th>

                          <th>Equipamento</th>
                          <th>Realização</th>
                          <th>Validade</th>
                          <th>vencido</th>
                           <th>Status</th>
                           <th>Cliente</th>
                           <th>CPF/CNPJ</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) { 
                             $row=$row+1;
                            ?>
                        <tr>
                        <td> <?php printf($row); ?> </td>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($equipament); ?> <?php printf($serie_number); ?></td>
                          <td><?php printf($date_validation); ?></td>
                            <td><?php printf($val); ?> Meses</td>
                            <td><?php if($ven ==0) {printf("Não");} if($ven ==1) {printf("Sim");}  ?></td>
                            <td><?php if($status ==0) {printf("Aprovado");} if($status ==1) {printf("Reprovado");}  ?> </td>
                            <td><?php printf($customer); ?></td>
                               <td><?php printf($documentation); ?></td>


                          <td>
              <!--     <a class="btn btn-app"  href="calibration-report-edit?laudo=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>-->
                <!--  <a class="btn btn-app"  href="calibration-report-edit-single-copy?laudo=<?php printf($id); ?>" onclick="new PNotify({
                               title: 'Editar',
                               text: 'Abrindo Edição',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-edit"></i> Editar
                 </a> -->

        <!--   <a  class="btn btn-app" href="calibration-report-viewer?laudo=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Laudo!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar v1
                  </a> -->
               <!--   <a  class="btn btn-app" href="single-calibration-report-viewer-copy-print?laudo=<?php printf($id); ?>"target="_blank" onclick="new PNotify({
                                       title: 'Visualizar',
                                       text: 'Visualizar Laudo!',
                                       type: 'info',
                                       styling: 'bootstrap3'
                                   });" >
                           <i class="fa fa-print"></i> Imprimir
                         </a>-->
                          <a  class="btn btn-app" href="calibration-report-viwer-copy-single-2?laudo=<?php printf($id); ?>"target="_blank" onclick="new PNotify({
                                       title: 'Visualizar',
                                       text: 'Visualizar Laudo!',
                                       type: 'info',
                                       styling: 'bootstrap3'
                                   });" >
                           <i class="fa fa-print"></i> Imprimir Digital
                         </a>
               <!--   <a  class="btn btn-app" href="single-calibration-report-viewer-copy?laudo=<?php printf($id); ?>"target="_blank" onclick="new PNotify({
                                       title: 'Visualizar',
                                       text: 'Visualizar Laudo!',
                                       type: 'info',
                                       styling: 'bootstrap3'
                                   });" >
                           <i class="fa fa-file-pdf-o"></i> Visualizar
                         </a> -->
                    <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/single-calibration-report-trash-backend?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
