  <?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Relatório de Registro Anvisa de Familia de Equipamentos</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>




             <!-- page content -->








              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro <small>do Relatório</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="backend/report-report-anvisa-family-backend.php" method="post"  target="_blank">

 <div class="ln_solid"></div>

  

               

 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span class="required" >*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="instituicao" id="instituicao"  placeholder="Unidade">
                    <option value=""> Selecione uma opção</option>
										  	<?php



										   $sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$instituicao);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($instituicao);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
											</div>

								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#instituicao').select2();
                                      });
                                 </script>

                                               	 <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
                            </label>
                            <div class="input-group col-md-6 col-sm-6">
										<select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
                    <option value=""> Selecione uma opção</option>
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>

                                 <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required"></span>
                            </label>
                            <div class="input-group col-md-6 col-sm-6">
										<select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
                    <option value=""> Selecione uma opção</option>
	                                     	</select>
										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>



<div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento_grupo" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Grupo Equipamento ">Grupo Equipamento 
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="Grupo de Equipamento">
                    <option value=""> Selecione uma opção</option>
										  	<?php



										 $query = "SELECT id,nome FROM equipamento_grupo  WHERE trash = 1 ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"> <?php printf($nome);?> 	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

										</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>

<div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento_familia" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma familia de Equipamento ">Familia de Equipamento 
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="equipamento_familia" id="equipamento_familia"  placeholder="Equipamento Familia">
                    <option value=""> Selecione uma opção</option>

                      <?php



$query = "SELECT equipamento_familia.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante  FROM equipamento_familia  ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id, $nome, $modelo, $fabricante);
while ($stmt->fetch()) {
?>
<option value="<?php printf($id);?>	"><?php printf($nome);?> - <?php printf($modelo);?> - <?php printf($fabricante);?>	</option>
   <?php
 // tira o resultado da busca da memória
 }

                       }
 $stmt->close();

 ?>
                    </select>
<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

</div>
   </div>

<script>
               $(document).ready(function() {
               $('#equipamento_familia').select2();
                 });
            </script>
                     

                     <div class="ln_solid"></div>


                     <br>

                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatório</button>
                         </center>
                        </div>
                      </div>




                  </div>
                </div>
              </div>
            </div>






             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="report-report">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




                </div>
              </div>







        </div>
            </div>
        <!-- /page content -->
     <script type="text/javascript">
    $(document).ready(function(){
        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });
    </script>
     <script type="text/javascript">
    $(document).ready(function(){
        $('#area').change(function(){
            $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
        });
    });
    </script>
