<?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../

?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Localização</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>




             <!-- page content -->








              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro <small>do Relatório</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="backend/report-report-equipament-location-backend.php" method="post"  target="_blank">

 <div class="ln_solid"></div>




 <div class="ln_solid"></div>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Unidade<span
                        ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="unidade" id="unidade"  placeholder="Unidade">
										  <option value="">Selecione uma Unidade</option>
                   <?php
                      

   $result_cat_post  = "SELECT instituicao.id, instituicao.instituicao FROM instituicao WHERE trash = 1 ";

   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
             while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
               echo '<option></option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
             }
                    ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma unidade "></span>

                  </div>

                  </div>

								 <script>
                                    $(document).ready(function() {
                                    $('#unidade').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Setor<span
                       ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="area" id="setor"  placeholder="Setor">
										<option value="">Selecione um Setor</option>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Setor "></span>

                  </div>
                  </div>
                            

								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Area<span
                       ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="setor" id="area"  placeholder="Area">
                    <option value="">Selecione uma Area</option>   
                  </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Area "></span>

                    </div>

                    </div>
                                
								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>
                                 

                                


<div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Equipamento Familia<span class="required" ></span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                  <select type="text" class="form-control has-feedback-right" name="equipamento_familia" id="equipamento_familia"  placeholder="Grupo">
                       <option value="">Selecione uma Familia</option>
                      <?php
                    $sql = "SELECT  id, nome,fabricante,modelo,img FROM equipamento_familia  WHERE trash = 1";
                                        if ($stmt = $conn->prepare($sql)) {
                                    $stmt->execute();
                                        $stmt->bind_result($id,$nome_equipamento,$fabricante_equipamento,$modelo_equipamento,$img);
                                        while ($stmt->fetch()) {
                                              ?>
                                          <option value="<?php printf($id);?>	"><?php printf($nome_equipamento);?> - <?php printf($modelo_equipamento);?> - <?php printf($fabricante_equipamento);?>	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }
                                          }
                    $stmt->close();
                    ?>
                                      </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
                   </div>
                      </div>

               <script>
                                  $(document).ready(function() {
                                  $('#equipamento_familia').select2();
                                    });
                               </script>

<div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Fabricante</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="manufacture" id="manufacture"  placeholder="Fabricante">
										  <option value="">Selecione um Fabricante</option>
                      <?php
                    $sql = "SELECT  id, nome,fabricante,modelo,img FROM equipamento_familia  WHERE trash = 1";
                                        if ($stmt = $conn->prepare($sql)) {
                                    $stmt->execute();
                                        $stmt->bind_result($id,$nome_equipamento,$fabricante_equipamento,$modelo_equipamento,$img);
                                        while ($stmt->fetch()) {
                                              ?>
                                          <option value="<?php printf($fabricante_equipamento);?>	">  <?php printf($fabricante_equipamento);?>	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }
                                          }
                    $stmt->close();
                    ?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Fabricante "></span>

                        </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#manufacture').select2();
                                      });
                                 </script>
                                  <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Modelo</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="model" id="model"  placeholder="Modelo">
										  <option value="">Selecione o Modelo</option>
                      <?php
                    $sql = "SELECT  id, nome,fabricante,modelo,img FROM equipamento_familia  WHERE trash = 1";
                                        if ($stmt = $conn->prepare($sql)) {
                                    $stmt->execute();
                                        $stmt->bind_result($id,$nome_equipamento,$fabricante_equipamento,$modelo_equipamento,$img);
                                        while ($stmt->fetch()) {
                                              ?>
                                          <option value="<?php printf($modelo_equipamento);?>	"> <?php printf($modelo_equipamento);?> 	</option>
                      <?php
                    // tira o resultado da busca da memória
                    }
                                          }
                    $stmt->close();
                    ?>
	                                     	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Modelo "></span>

                        </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#model').select2();
                                      });
                                 </script>


 
<div class="ln_solid"></div>
   
            



                     <br>


                                <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit">Gerar Relatório</button>
                         </center>
                        </div>
                      </div>




                  </div>
                </div>
              </div>
            </div>






             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




                </div>
              </div>




              <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>


        </div>
            </div>
<script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>

