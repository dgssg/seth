<?php
include("../../database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
	if($_SESSION['id_nivel'] != 2 ){
    header ("Location: ../index.php");
}
	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	$email=	$_SESSION['email'] ;

if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
}
  $query = "SELECT mod_01,mod_02 FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($mod_01,$mod_02);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../../../framework/images/favicon.ico" type="image/ico" />

<title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>

  
   <!-- Bootstrap -->
    <link href="../../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- iCheck -->
    <link href="../../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- JQVMap -->
    <link href="../../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    
    <!-- Custom Theme Style -->
    <link href="../../../../framework/build/css/custom.min.css" rel="stylesheet">

      <!-- PNotify -->
  <link href="../../../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../../../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../../../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
   <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


   <!-- <script src="jquery.min.js" ></script> -->

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard" class="site_title"><i class="fa fa-500px"></i> <span>SETH T.I</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../../logo/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?php printf($usuariologado) ?></h2>
                <p class="glyphicon glyphicon-time" id="countdown"></p>
               <span>
              <script> document.write(new Date().toLocaleDateString()); </script>
              </span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

  <!-- sidebar menu -->
           <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Geral</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Início <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                            <li><a href="dashboard" title="Painel de supervisão">Dashboard</a></li>

                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Ordem de Serviço <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="os-open" title="Abrir nova ordem de serviço">Abrir OS </a></li>
                      <li><a href="os-opened" title="Gerenciar ordem de serviço solicitadas">Aberto </a></li>
                <!--      <li><a href="form_validation.php">Buscar</a></li> -->
                     <li><a href="os-progress" title="Gerenciar ordem de serviço em andamento">Em processo</a></li>
                       <!--  <li><a href="os-concluded">Fechada</a></li> -->


                              <li><a href="os-finished" title="Gerenciar ordem de serviço concluida">Concluida</a></li>
                                 <li><a href="os-rating" title="Avaliação de ordem de serviço concluida">Avaliação</a></li>
 <li><a href="os-canceled" title="Ordem de serviço Cancelada">Cancelada</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-bar-chart-o"></i> Indicador &amp; Relatório <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a>Indicador<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-indicator-mc" title="Gerenciamento e visualizar Indicador Manutenção Corretiva">Manutenção Corretiva</a></li>
											<li><a href="report-indicator-mp" title="Gerenciamento e visualizar Indicador Manutenção Preventiva">Manutenção Preventiva </a>	</li>
											 </ul>
									</li>
                       <li><a>Relatórios<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-report-mc" title="Gerenciamento e visualizar Relatório Manutenção Corretiva">Manutenção Corretiva</a></li>
											<li><a href="report-report-mp" title="Gerenciamento e visualizar Relatório Manutenção Preventiva">Manutenção Preventiva </a>	</li>
											<li><a href="report-report-equipament" title="Gerenciamento e visualizar Relatório Inventario">Inventario</a>	</li>
<li><a href="report-report-obsolescence" title="Gerenciamento e visualizar Obsolecência">Obsolecência</a>	</li>
											 </ul>
									</li>

                    <!--  <li><a href="morisjs.php">Moris JS</a></li> -->
                     <!-- <li><a href="echarts.php">ECharts</a></li> -->
                     <!-- <li><a href="other_charts.php">Other Charts</a></li> -->
                    </ul>
                  </li>

               
                   <li><a><i class="fa fa-folder"></i> Documentação <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="documentation-bussines" title="Documentação Empresa ">Empresa</a></li>
        <li><a href="documentation-pop" title="Documentação POP">POP </a></li>
        <li><a href="documentation-equipament" title="Documentação Equipamentos">Prontuário </a></li>
        
        <li><a href="documentation-timeline" title="Documentação Cronograma">Cronograma </a></li>
        <li><a href="documentation-mc" title="Documentação Manutenção Corretiva">Manutenção Corretiva </a></li>
        <li><a href="documentation-mp" title="Documentação Manutenção Preventiva">Manutenção Preventiva </a></li>
        
  
                    
                   
                 
                           
                         
                 
                    </ul>
                  </li>
                
                
                  
                </ul>
              </div>
              
              
                

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                <script>
                var goFS = document.getElementById("goFS");
                goFS.addEventListener("click", function() {
                  document.body.requestFullscreen();
                }, false);
              </script>

              </a>
              <a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="lockscreen">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>


            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                        <img src="../../logo/img.jpg" alt=""><?php printf($usuariologado) ?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="profile">   <i class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></i> Perfil </a>
                        

                    <!--    <a class="dropdown-item"  href="javascript:;">
                          <span class="badge bg-red pull-right">100%</span>
                          <span>Settings</span>
                        </a>
              <!--      <a class="dropdown-item"  href="javascript:;">Help</a> -->
                      <a class="dropdown-item"  href="login"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>






                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

        <!-- /top navigation -->
        <!-- page content -->


<?php include 'api.php';?>
<?php include 'frontend/report-report-frontend.php';?>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           ©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://mksistemasbiomedicos.com.br"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery
    <script src="../../../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="../../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../../../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../../../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../../../framework/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../../../framework/build/js/custom.min.js"></script>


    <!-- gauge.js -->
    <script src="../../../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../../../framework/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../../../../framework/vendors/skycons/skycons.js"></script>

   <!-- JQVMap -->
    <script src="../../../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
  
 <!-- PNotify -->
   <script src="../../../../framework/vendors/pnotify/dist/pnotify.js"></script>
   <script src="../../../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
   <script src="../../../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>


      <!-- bootstrap-wysiwyg -->
    <script src="../../../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../../../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../../../../framework/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../../../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../../../../framework/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../../../../framework/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="../../../../framework/assets/js/select2.min.js"></script>
    <!-- Parsley -->
    <script src="../../../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../../../../framework/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../../../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../../../../framework/vendors/starrr/dist/starrr.js"></script>
   <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>




	</body>
  <script language="JavaScript">

 
 history.pushState(null, null, document.URL);
 window.addEventListener('popstate', function () {
     history.pushState(null, null, document.URL);
 });
 </script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;
  var timer;
  var interval;

  function countdown() {

      seconds--;
    if (seconds < 0) {
		clearInterval(interval);
      // Change your redirection link here
      window.location = "https://seth.mksistemasbiomedicos.com.br";
    } else if (seconds === 60) {
      // Show warning 60 seconds before redirect
      var warning = document.createElement("div");
      warning.innerHTML = "O sistema será desconectado por motivos de segurança em 60 segundos. Clique aqui para continuar no sistema e reiniciar o tempo.";
      warning.style.backgroundColor = "yellow";
      warning.style.padding = "10px";
      warning.style.position = "fixed";
      warning.style.bottom = "0";
      warning.style.right = "0";
      warning.style.zIndex = "999";
      warning.addEventListener("click", function() {
        warning.style.display = "none";
        clearTimeout(seconds);
		seconds = 1080;
        countdown();
      });
      document.body.appendChild(warning);
	  countdown();
    } else {
      // Update remaining seconds
      document.getElementById("countdown").innerHTML = seconds;
      // Count down using javascript
	
      timer = setTimeout(countdown, 1000);
    }
  }

  // Run countdown function
  countdown();
  
  
  function stopCountdown() {
    clearInterval(seconds);
  }
 
    // Reset countdown on user interaction
	document.addEventListener("click", function() {
    clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });
  document.addEventListener("mousemove", function() {
	seconds--;
    //clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });

</script>

 </html>
