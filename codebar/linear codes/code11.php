<div>
    <div style="padding:10px; font-size:15px; font-family:Arial, Helvetica; text-align:center; display:inline-block;">
        <a href="https://www.cognex.com" title="Cognex Corporation" style="display:block">
            <img src="https://www.cognex.com/gfx/site/pic-global-header-logo-cognex.png" alt="Barcode Software by Cognex Corporation"/>
        </a>
        <a href="https://www.cognex.com" title="Cognex Corporation">Cognex Corporation</a>
    </div>
    <div style="display:inline;">
        <img src="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=0123-4567&code=BCL_CODE11&width=300&imageType=JPG&foreColor=%23000000&backColor=%23FFFFFF&rotation=RotateNoneFlipNone" alt="MK Sistemas Biomedicos" width="300" />
    </div>
</div>
