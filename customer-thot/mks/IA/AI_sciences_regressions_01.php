<!DOCTYPE html>
<html>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<body>

<div id="myPlot" style="width:100%;max-width:700px"></div>

<script>
var xArray = [50,60,70,80,90,100,110,120,130,140,150];
var yArray = [7,8,8,9,9,9,10,11,14,14,15];

// Calculate Sums
var xSum=0, ySum=0, xxSum=0, xySum=0;
var count = xArray.length;
for (var i = 0, len = count; i < count; i++) {
  xSum += xArray[i];
  ySum += yArray[i];
  xxSum += xArray[i] * xArray[i];
  xySum += xArray[i] * yArray[i];
}

// Calculate slope and intercept
var slope = (count * xySum - xSum * ySum) / (count * xxSum - xSum * xSum);
var intercept = (ySum / count) - (slope * xSum) / count;

// Generate values
var xValues = [];
var yValues = [];
for (var x = 50; x <= 150; x += 1) {
  xValues.push(x);
  yValues.push(x * slope + intercept);
}

var data = [
  {x:xArray, y:yArray, mode:"markers"},
  {x:xValues, y:yValues, mode:"line"}
];

var layout = {
  xaxis: {range: [40, 160], title: "Square Meters"},
  yaxis: {range: [5, 16], title: "Price in Millions"},  
  title: "House Prices vs. Size"
};

Plotly.newPlot("myPlot", data, layout);
</script>

</body>
</html>
