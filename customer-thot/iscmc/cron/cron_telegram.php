<?php
	
	$conn = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_iot_mks");
	$conn_control= mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_iot_mks");
	$key = "mks";
	$mod="iot";
	
	$query="SELECT max(notif.read_open),max(notif.id),sensor.telegram_chat_id,sensor.telegram,sensor.telegram_token,sensor.id, sensor.macadress, sensor.id_sensor_type, sensor.nome, sensor.id_area, sensor.id_status, sensor.up_time,  sensor.bat, sensor.last_alarm, sensor.trash, sensor.id_alarm, sensor.imei, sensor.linha, sensor.iccid, sensor.id_location, sensor_usb.nome as 'usb', sensor_charger.nome as 'charger', sensor.ativo, sensor.dados, sensor.id_dados,area.nome as 'area',setor.nome as 'setor',unidade.unidade, sensor.alarm_min_temp_in, sensor.alarm_max_temp_in, sensor.alarm_min_temp_out, sensor.alarm_max_temp_out, sensor.alarm_min_hum,sensor.alarm_max_hum, sensor.alarm_min_nivel, sensor.alarm_max_nivel, sensor.alarm_min_amper, sensor.alarm_max_amper FROM sensor LEFT JOIN sensor_type ON sensor_type.id =  sensor.id_sensor_type LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN sensor_usb ON sensor_usb.id =  sensor.usb LEFT JOIN sensor_charger ON sensor_charger.id =  sensor.charger LEFT JOIN notif ON notif.id_sensor = sensor.id  WHERE sensor.trash != 0 and sensor.ativo = 0 and sensor.alarm = 0 GROUP BY sensor.id ";
	
	
 
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result(
			$max_read_open, 
			$max_notif_id, 
			$chatID, 
			$telegram, 
			$botToken, 
			$sensor_id, 
			$macadress, 
			$id_sensor_type, 
			$nome, 
			$id_area, 
			$id_status, 
			$up_time, 
			$bat, 
			$last_alarm, 
			$trash, 
			$id_alarm, 
			$imei, 
			$linha, 
			$iccid, 
			$id_location, 
			$usb, 
			$charger, 
			$ativo, 
			$dados, 
			$id_dados, 
			$area, 
			$setor, 
			$unidade, 
			$alarm_min_temp_in, 
			$alarm_max_temp_in, 
			$alarm_min_temp_out, 
			$alarm_max_temp_out, 
			$alarm_min_hum, 
			$alarm_max_hum, 
			$alarm_min_nivel, 
			$alarm_max_nivel, 
			$alarm_min_amper, 
			$alarm_max_amper
		);
		while ($stmt->fetch()) {
			
			switch($id_sensor_type){
				case 1:
					if($dados < $alarm_min_temp_in || $dados > $alarm_max_temp_in){
					       if ($max_read_open !== 1 || $max_read_open === null) {
							$stmt = $conn_control->prepare("INSERT INTO notif (id_sensor, dados,data,id_dados) VALUES (?, ?, ?, ?)");
							$stmt->bind_param("ssss",$sensor_id, $dados,$up_time,$id_dados);
							$execval = $stmt->execute();
							$last_id = $conn_control->insert_id;
						 
							$stmt = $conn_control->prepare("UPDATE sensor SET last_alarm= ? WHERE id= ?");
							$stmt->bind_param("ss",$last_id,$sensor_id);
							$execval = $stmt->execute();
						    if($dados < $alarm_min_temp_in){
								
							$stmt = $conn_control->prepare("UPDATE sensor SET id_alarm= 1 WHERE id= ?");
							$stmt->bind_param("s",$sensor_id);
							$execval = $stmt->execute();
						
							$alerta = "Temperatura Baixo";	
							
							}
							if($dados > $alarm_max_temp_in){
								
								$stmt = $conn_control->prepare("UPDATE sensor SET id_alarm= 2 WHERE id= ?");
								$stmt->bind_param("s",$sensor_id);
								$execval = $stmt->execute();
								
							$alerta = "Temperatura Alto";	
								
							}
							
							
							if($telegram == 0){
								
							 
								$message = 'Sistema Telemetria' . "\n";
								
								$message .= 'Nome: ' . $nome . "\n";
								$message .= 'Macadress: ' . $macadress . "\n";
								$message .= 'Alarme: ' . $alerta . "\n";
								$message .= 'Valor: ' . $dados;
								
								$telegramApiUrl = "https://api.telegram.org/bot$botToken/sendMessage";
								
								$data = [
									'chat_id' => $chatID,
									'text' => $message,
								];
								
								$options = [
									'http' => [
										'method' => 'POST',
										'header' => 'Content-Type: application/x-www-form-urlencoded',
										'content' => http_build_query($data),
									],
								];
								
								$context = stream_context_create($options);
								$response = file_get_contents($telegramApiUrl, false, $context);
								

								
							}
						}
						
					}
					break;
				case 2:
					
					break;
				case 3:
					
					break;
				
			}
			
		}
		
	}
			
	
	
?>