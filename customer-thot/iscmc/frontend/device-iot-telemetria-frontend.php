  <?php
$codigoget = ($_GET["id"]);

// Create connection

include("database/database.php");
    $query = "SELECT sensor.id_sensor_type,sensor.id, sensor.macadress, sensor_type.nome as 'id_sensor_type', sensor.nome, sensor.id_area, sensor.id_status, sensor.up_time, sensor.file, sensor.reg_date, sensor.upgrade, sensor.bat, sensor.last_alarm, sensor.trash, sensor.id_alarm, sensor.imei, sensor.firmware, sensor.linha, sensor.iccid, sensor.id_location, sensor_usb.nome as 'usb', sensor_charger.nome as 'charger', sensor.ativo, sensor.dados, sensor.id_dados,area.nome as 'area',setor.nome as 'setor',unidade.unidade FROM sensor LEFT JOIN sensor_type ON sensor_type.id =  sensor.id_sensor_type LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN sensor_usb ON sensor_usb.id =  sensor.usb LEFT JOIN sensor_charger ON sensor_charger.id =  sensor.charger WHERE sensor.id = $codigoget";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($sensor_id_sensor_type,$id,$macadress,$id_sensor_type, $nome, $id_area, $id_status,$up_time, $file, $reg_date,$upgrade, $bat, $last_alarm, $trash, $id_alarm, $imei, $firmware, $linha, $iccid, $id_location, $usb,$charger, $ativo, $dados, $id_dados,$area,$setor,$unidade);
      while ($stmt->fetch()) {
        
      }
    }


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Dashboard <small>Telemetria</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Sensor</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <input type="hidden" id="codigoget" name="codigoget" value="<?php printf($macadress); ?>" readonly="readonly" required="required" class="form-control ">
                <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Macadress <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="$macadress" name="$macadress" value="<?php printf($macadress); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Sensor <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($id_sensor_type); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Nome<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="categoria" name="categoria" required="required" class="form-control"  value="<?php printf($nome); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="info" class="form-control" type="text" name="info"  value="<?php printf($unidade); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($setor); ?>" readonly="readonly">
                        </div>
                      </div>
                 
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Area</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($area); ?>" readonly="readonly">
                    </div>
                  </div>



                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>



                  </div>
                </div>
              </div>
	       </div>
 <?php  if($sensor_id_sensor_type == 2){ ?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class>
      <div class="x_content">
        <div class="row">

          <?php
            // Consultar os últimos dados de cada dispositivo
            $sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time
            FROM sensor_dados sd
            INNER JOIN (
              SELECT sensor_dados.device, MAX(sensor_dados.id) AS ultimo_id
              FROM sensor_dados
              GROUP BY device
            ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
            INNER JOIN (
              SELECT nome AS nome, up_time, macadress
              FROM sensor
              WHERE id = $codigoget
            ) dados_ids ON sd.device = dados_ids.macadress;";
            $result = mysqli_query($conn, $sql);
            
            $temp_data = [];
            $hum_data = [];
            $devices = [];
            
            if (mysqli_num_rows($result) > 0) {
              while ($row = mysqli_fetch_assoc($result)) {
                // Dividir os dados em temperatura e umidade
                $dados = explode(" ", $row['dados']);
                $temp = null;
                $hum = null;
                
                foreach ($dados as $dado) {
                  if (strpos($dado, 'T:') === 0) {
                    $temp = floatval(substr($dado, 2));
                  } elseif (strpos($dado, 'H:') === 0) {
                    $hum = floatval(substr($dado, 2));
                  }
                }
                
                if ($temp !== null && $hum !== null) {
                  $temp_data[] = ["device" => $row['device'], "value" => $temp, "nome" => $row['nome'], "up_time" => $row['up_time']];
                  $hum_data[] = ["device" => $row['device'], "value" => $hum, "nome" => $row['nome'], "up_time" => $row['up_time']];
                }
                
                $devices[] = $row['device'];
              }
            }
            
            // Converter os arrays em JSON
            $json_temp_data = json_encode($temp_data);
            $json_hum_data = json_encode($hum_data);
          ?>

          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
          
          <script type="text/javascript">
            google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
            google.charts.setOnLoadCallback(drawCharts);
            
            function drawCharts() {
              var tempData = <?php echo $json_temp_data; ?>;
              var humData = <?php echo $json_hum_data; ?>;
              
              tempData.forEach(function(item) {
                drawChart(item.device + '_temp', item.value, 'Temperatura');
              });
              
              humData.forEach(function(item) {
                drawChart(item.device + '_hum', item.value, 'Umidade');
              });
            }
            
            function drawChart(elementId, value, label) {
              var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                [label, value]
              ]);
              
              var options = {
                width: 400,
                height: 120,
                redFrom: 4,
                redTo: 8,
                yellowFrom: 2,
                yellowTo: 4,
                greenFrom: -20,
                greenTo: 2,
                minorTicks: 5,
                min: -20,
                max: 8
              };
              
              var chart = new google.visualization.Gauge(document.getElementById(elementId));
              
              chart.draw(data, options);
            }
          </script>

          <?php
            foreach ($temp_data as $data) {
              echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
                <div class='tile-stats'>
                  <h5>" . $data['nome'] . " - Temperatura</h5>
                  <p>" . $data['up_time'] . "</p>
                  <p>" . $data['device'] . "</p>
                  <div id='" . $data['device'] . "_temp' style='width: 400px; height: 120px;'></div>
                </div>
              </div>";
            }

            foreach ($hum_data as $data) {
              echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
                <div class='tile-stats'>
                  <h5>" . $data['nome'] . " - Umidade</h5>
                  <p>" . $data['up_time'] . "</p>
                  <p>" . $data['device'] . "</p>
                  <div id='" . $data['device'] . "_hum' style='width: 400px; height: 120px;'></div>
                </div>
              </div>";
            }
          ?>

        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<?php
if ($sensor_id_sensor_type == 3) {
    $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));

    $sql = "SELECT dados_ids.nivel_max, dados_ids.nivel_min, sd.device, sd.dados, dados_ids.nome, dados_ids.up_time, dados_ids.vol
            FROM sensor_dados sd
            INNER JOIN (
              SELECT sensor_dados.device, MAX(sensor_dados.id) AS ultimo_id
              FROM sensor_dados
              WHERE sensor_dados.reg_date >= '$date_limit'
              GROUP BY sensor_dados.device
            ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
            INNER JOIN (
              SELECT nome AS nome, up_time, macadress, id_sensor_type, nivel_max, nivel_min, vol
              FROM sensor
            ) dados_ids ON sd.device = dados_ids.macadress
            WHERE dados_ids.id_sensor_type = 3";
    $result = mysqli_query($conn, $sql);

    $cap_data = [];
    $vol_data = [];

    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $capacidade = (($row['dados'] - $row['nivel_min']) / $row['nivel_max']) * 100;
            $volume =  (( ($capacidade/100)  * $row['vol']));

            $cap_data[] = [
                "device" => $row['device'],
                "value" => $capacidade,
                "nome" => $row['nome'],
                "up_time" => $row['up_time']
            ];
            $vol_data[] = [
                "device" => $row['device'],
                "value" => $volume,
                "nome" => $row['nome'],
                "up_time" => $row['up_time']
            ];
        }
    }

    $json_cap_data = json_encode($cap_data);
    $json_vol_data = json_encode($vol_data);
?>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
        <div class>
            <div class="x_content">
                <div class="row">
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <script type="text/javascript">
                        google.charts.load('current', { 'packages': ['gauge'] });
                        google.charts.setOnLoadCallback(drawCharts);

                        function drawCharts() {
                            var capData = <?php echo $json_cap_data; ?>;
                            var volData = <?php echo $json_vol_data; ?>;

                            capData.forEach(function(item) {
                                drawGaugeChart(item.device + '_cap', item.value, 'Capacidade (%)', 0, 100);
                            });

                            volData.forEach(function(item) {
                                drawGaugeChart(item.device + '_vol', item.value, 'Volume (L)', 0, item.value);
                            });
                        }

                        function drawGaugeChart(elementId, value, label, min, max) {
                            var data = google.visualization.arrayToDataTable([
                                ['Label', 'Value'],
                                [label, value]
                            ]);

                            var options = {
                                width: 400,
                                height: 120,
                                redFrom: 75,
                                redTo: 100,
                                yellowFrom: 50,
                                yellowTo: 75,
                                greenFrom: 0,
                                greenTo: 50,
                                minorTicks: 5,
                                min: min,
                                max: max
                            };

                            var chart = new google.visualization.Gauge(document.getElementById(elementId));
                            chart.draw(data, options);
                        }
                    </script>

                    <?php
                    foreach ($cap_data as $data) {
                        echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
                            <div class='tile-stats'>
                                <h5>" . $data['nome'] . " - Capacidade</h5>
                                <p>" . $data['up_time'] . "</p>
                                <p>" . $data['device'] . "</p>
                                <div id='" . $data['device'] . "_cap' style='width: 400px; height: 120px;'></div>
                            </div>
                        </div>";
                    }

                    foreach ($vol_data as $data) {
                        echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
                            <div class='tile-stats'>
                                <h5>" . $data['nome'] . " - Volume</h5>
                                <p>" . $data['up_time'] . "</p>
                                <p>" . $data['device'] . "</p>
                                <div id='" . $data['device'] . "_vol' style='width: 400px; height: 120px;'></div>
                            </div>
                        </div>";
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <div class>
                  <div class="x_content">
                    <div class="row">
            
                      <?php
                        // Consultar os últimos dados de cada dispositivo
                        $sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time
                        FROM sensor_dados sd
                        INNER JOIN (
                          SELECT sensor_dados.device, MAX(sensor_dados.id) AS ultimo_id
                          FROM sensor_dados
                          GROUP BY device
                        ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
                        INNER JOIN (
                          SELECT nome AS nome, up_time, macadress
                          FROM sensor
                          WHERE id =$codigoget
                        ) dados_ids ON sd.device = dados_ids.macadress;";
                        $result = mysqli_query($conn, $sql);
                        
                        if (mysqli_num_rows($result) > 0) {
                          // Exibir os dados em gráficos de medidores
                          while ($row = mysqli_fetch_assoc($result)) {
                            
                            echo "<div class='animated flipInY col-lg-3 col-md-3 col-sm-6'>
    <div class='tile-stats'>
        <h5>" . $row['nome'] . "</h5>
        <p>" . $row['up_time'] . "</p>
        <p>" . $row['device'] . "</p>
      <div id='{$row['device']}' style='width: 400px; height: 120px;'>
        
      </div>
    </div>
  </div>";
                          }
                        }
                      ?>
                  
         
         
          
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
          
          <script type="text/javascript">
            google.charts.load('current', { 'packages': ['gauge', 'corechart'] });
            google.charts.setOnLoadCallback(drawCharts);
            
            function drawCharts() {
              <?php
              // Exibir os gráficos de medidores
              mysqli_data_seek($result, 0); // Voltar para o início do resultado
              while ($row = mysqli_fetch_assoc($result)) {
                echo "drawChart('{$row['device']}', {$row['dados']});";
              }
              ?>
              drawDonutChart(<?php echo $json_data; ?>);
            }
            
            function drawChart(device, value) {
              var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['', value] // Remova o rótulo, deixando-o vazio
              ]);
              
              var options = {
                width: 400,
                height: 120,
                redFrom: 4,
                redTo: 8,
                yellowFrom: 2,
                yellowTo: 4,
                greenFrom: -20,
                greenTo: 2,
                minorTicks: 5,
                min: -20,
                max: 8
              };
              
              var chart = new google.visualization.Gauge(document.getElementById(device));
              
              chart.draw(data, options);
            }
            
           
          </script>
            
            <?php
              // Defina a data de 30 dias atrás
               
              // Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nos últimos 30 dias
              $sql = "SELECT sensor_dados.id, sensor_dados.device, sensor_dados.dados, sensor_gate.nome AS 'gate', sensor_dados.energy, sensor_dados.bat, sensor_usb.nome AS 'usb', sensor_charger.nome AS 'charger', sensor_dados.reg_date
              FROM sensor_dados
              LEFT JOIN sensor_usb ON sensor_usb.id = sensor_dados.usb
              LEFT JOIN sensor_charger ON sensor_charger.id = sensor_dados.charger
              LEFT JOIN sensor_gate ON sensor_gate.id = sensor_dados.gate
              WHERE sensor_dados.device LIKE '$macadress'
              ORDER BY sensor_dados.reg_date DESC
              LIMIT 10";
// Aqui você precisa substituir '$id' pelo valor adequado
              
              // Execute a consulta e obtenha os resultados
              $result = mysqli_query($conn, $sql);
              
             
              
              // Converta os resultados em um array associativo para JSON
              $rows = array();
              while ($row = mysqli_fetch_assoc($result)) {
                $rows[] = $row;
              }
              $json_data2 = json_encode($rows);
            ?>

                   <script type="text/javascript">
  google.charts.load('current', {'packages':['corechart', 'table']});
  google.charts.setOnLoadCallback(drawSort);

  function drawSort() {
    var jsonData2 = <?php echo $json_data2; ?>;

    // Define data table for the table visualization
    var tableData = new google.visualization.DataTable();
    tableData.addColumn('string', 'ID');
    tableData.addColumn('number', 'Dado');
    tableData.addColumn('number', 'Bateria');
    tableData.addColumn('number', 'Carregamento');

    // Define data table for the scatter chart visualization
    var chartData = new google.visualization.DataTable();
    chartData.addColumn('string', 'ID');
    chartData.addColumn('number', 'USB');
    chartData.addColumn('number', 'Bateria');
    chartData.addColumn('number', 'Carregamento');

    // Populate both data tables
    for (var i = 0; i < jsonData2.length; i++) {
      console.log(jsonData2[i]); // Logar dados para depuração

      // Add rows to the table data
      tableData.addRow([
        jsonData2[i].id, 
        parseFloat(jsonData2[i].dados), 
        parseFloat(jsonData2[i].bat), 
        parseFloat(jsonData2[i].charger)
      ]);

      // Add rows to the chart data
      chartData.addRow([
        jsonData2[i].id, 
        parseFloat(jsonData2[i].dados), 
        parseFloat(jsonData2[i].bat), 
        parseFloat(jsonData2[i].charger)
      ]);
    }

    // Draw the table visualization
    var table = new google.visualization.Table(document.getElementById('table_sort_div'));
    table.draw(tableData, {width: '100%', height: '100%'});

    // Define options for the scatter chart
    var options = {
      width: '100%',
      height: '100%',
      legend: { position: 'top' },
      colors: ['green', 'blue', 'red']
    };

    // Draw the scatter chart
    var chart = new google.visualization.ScatterChart(document.getElementById('chart_sort_div'));
    chart.draw(chartData, options);
  }
</script>


            
                         <div id="table_sort_div" style="width: 100%; height:100%;"></div>
         
           
              <div id="chart_sort_div" style="width: 100%; height: 100px;"></div>
             


                    </div>
                  </div>
                </div>
              </div>
            </div>
            

            <div class="clearfix"></div>
            <br>  
            
              
             <?php  if($sensor_id_sensor_type == 1){ ?>
                 
            <?php
              // Defina a data de 48 horas atrás
              $date_limit = date('Y-m-d', strtotime('-48 hours'));
              
              // Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nas últimas 48 horas
              $sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time
              FROM sensor_dados sd
              INNER JOIN (
                SELECT sensor_dados.device, (sensor_dados.id) AS ultimo_id
                FROM sensor_dados
                WHERE sensor_dados.reg_date >= '$date_limit'  
              ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
              INNER JOIN (
                SELECT id,nome AS nome, up_time, macadress, id_sensor_type
                FROM sensor
              ) dados_ids ON sd.device = dados_ids.macadress
              WHERE dados_ids.id =  $codigoget";
              
              // Execute a consulta e obtenha os resultados
              $result4 = mysqli_query($conn, $sql);
              
             
              // Converta os resultados em um array associativo para JSON
              $rows4 = array();
              while ($row4 = mysqli_fetch_assoc($result4)) {
                $rows4[] = $row4;
              }
              $json_data4 = json_encode($rows4);
            ?>
            
            <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChartline);
              
              function drawChartline() {
                var jsonData4 = <?php echo $json_data4; ?>;
                
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Data');
                data.addColumn('number', 'Temperatura');
                
                // Adiciona as linhas com base nos dados fornecidos
                for (var i = 0; i < jsonData4.length; i++) {
                  var rowData = [
                    jsonData4[i].reg_date, // Assumindo que up_time contém os valores para o eixo X (Data)
                    parseFloat(jsonData4[i].dados) // Assumindo que dados contém os valores para o eixo Y (Sensor)
                  ];
                  data.addRow(rowData);
                }
                
                
                var options = {
                  title: 'Temperatura 48 (horas)',
                  curveType: 'function',
                  legend: { position: 'bottom' }
                };
                
                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                chart.draw(data, options);
              }
            </script>
            
            
            
            <div class="clearfix"></div>
            <br>
            
            <div id="curve_chart"  style="width: 100%; height: 500px"></div>
            
            <div class="clearfix"></div>
            <br>
            
            <?php  } ?>
              <?php  if($sensor_id_sensor_type == 2){ ?>
                 
             <?php
      // Defina a data de 48 horas atrás
      $date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));
      
      // Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nas últimas 48 horas
      $sql2 = "SELECT dados_ids.nivel_max,dados_ids.nivel_min,sd.device, sd.dados, dados_ids.nome, dados_ids.up_time
      FROM sensor_dados sd
      INNER JOIN (
        SELECT sensor_dados.device, (sensor_dados.id) AS ultimo_id
        FROM sensor_dados
        WHERE sensor_dados.reg_date >= '$date_limit'  -- Limite para as últimas 48 horas
       ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
      INNER JOIN (
        SELECT nome AS nome, up_time, macadress, id_sensor_type,nivel_max,nivel_min
        FROM sensor
      ) dados_ids ON sd.device = dados_ids.macadress
      WHERE dados_ids.id_sensor_type = 2";
      
      // Execute a consulta e obtenha os resultados
      $result5 = mysqli_query($conn, $sql2);
      
      // Verifique se há erros na consulta
      if (!$result5) {
        die('Erro na consulta: ' . mysqli_error($conn));
      }
      
      // Array para armazenar os dados separados de temperatura e umidade
      $temp_data = array();
      $hum_data = array();
      
      // Extrair os dados de temperatura e umidade e armazená-los nos arrays correspondentes
      while ($row5 = mysqli_fetch_assoc($result5)) {
        // Dividir os dados em temperatura e umidade
        $dados = explode(" ", $row5['dados']);
        foreach ($dados as $dado) {
          if (strpos($dado, 'T:') === 0) {
            $temp_data[] = floatval(substr($dado, 2));
          } elseif (strpos($dado, 'H:') === 0) {
            $hum_data[] = floatval(substr($dado, 2));
          }
        }
      }
      
      // Converter os arrays em JSON
      $json_temp_data = json_encode($temp_data);
      $json_hum_data = json_encode($hum_data);
    ?>
    
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartline2);
      
      function drawChartline2() {
        // Obter os dados de temperatura e umidade do PHP
        var tempData = <?php echo $json_temp_data; ?>;
        var humData = <?php echo $json_hum_data; ?>;
        
        // Criar os dados para o gráfico
        var data = new google.visualization.DataTable();
        data.addColumn('number', 'Tempo');
        data.addColumn('number', 'Temperatura');
        data.addColumn('number', 'Umidade');
        
        // Adicionar os valores de temperatura e umidade aos dados do gráfico
        for (var i = 0; i < tempData.length; i++) {
          data.addRow([i, tempData[i], humData[i]]);
        }
        
        // Configurações do gráfico
        var options = {
          title: 'Nível de Temperatura e Umidade',
          curveType: 'function',
          legend: { position: 'bottom' }
        };
        
        // Desenhar o gráfico
        var chart = new google.visualization.LineChart(document.getElementById('curve_chart2'));
        chart.draw(data, options);
      }
    </script>
      <div class="clearfix"></div>
            <br>
    <div id="curve_chart2" style="width: 900px; height: 500px"></div>

      <div class="clearfix"></div>
            <br>
            
  
            
            <?php  } ?>
               <?php  if($sensor_id_sensor_type == 3){ ?>
                 
          <?php
// Defina a data de 48 horas atrás
$date_limit = date('Y-m-d H:i:s', strtotime('-48 hours'));

// Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nas últimas 48 horas
$sql = "SELECT dados_ids.nivel_max, dados_ids.nivel_min, sd.device, sd.dados, dados_ids.nome, dados_ids.up_time
        FROM sensor_dados sd
        INNER JOIN (
          SELECT sensor_dados.device, sensor_dados.id AS ultimo_id
          FROM sensor_dados
          WHERE sensor_dados.reg_date >= '$date_limit'  -- Limite para as últimas 48 horas
      
        ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
        INNER JOIN (
          SELECT nome AS nome, up_time, macadress, id_sensor_type, nivel_max, nivel_min
          FROM sensor
        ) dados_ids ON sd.device = dados_ids.macadress
        WHERE dados_ids.id_sensor_type = 3";

// Execute a consulta e obtenha os resultados
$result4 = mysqli_query($conn, $sql);

// Verifique se há erros na consulta
if (!$result4) {
  die('Erro na consulta: ' . mysqli_error($conn));
}

// Converta os resultados em um array associativo para JSON
$rows4 = array();
while ($row4 = mysqli_fetch_assoc($result4)) {

  // Adicione o resultado atualizado ao array
  $rows4[] = $row4;
}
$json_data4 = json_encode($rows4);
?>



   <script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChartline);
  
  function drawChartline() {
    var jsonData4 = <?php echo $json_data4; ?>;
    
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Data');
    data.addColumn('number', 'Sensor');
    
    // Adiciona as linhas com base nos dados fornecidos
    for (var i = 0; i < jsonData4.length; i++) {
      var novoDado = ((jsonData4[i].dados - jsonData4[i].nivel_min) / jsonData4[i].nivel_max) * 100;
      var rowData = [
        jsonData4[i].up_time, // Assumindo que up_time contém os valores para o eixo X (Data)
        parseFloat(novoDado) // Assumindo que dados contém os valores para o eixo Y (Sensor)
      ];
      data.addRow(rowData);
    }
    
          
    var options = {
      title: 'Nivel de Caixa de Agua',
      curveType: 'function',
      legend: { position: 'bottom' }
    };
    
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
    chart.draw(data, options);
  }
</script>

   <div class="clearfix"></div>
            <br>


    <div id="curve_chart" style="width: 900px; height: 500px"></div>
    
   
    

      <div class="clearfix"></div>
            <br>
            
  
            
            <?php  } ?>
            
            <?php
              // Calcular a data de 30 dias atrás
              $date_limit_30_days = date('Y-m-d', strtotime('-30 days'));
              
              // Consulta SQL para obter a média, o mínimo, o máximo e o desvio padrão dos valores diários nos últimos 30 dias
              $sql_stats = "SELECT  
              AVG(sensor_dados.dados) AS media, 
              MIN(sensor_dados.dados) AS minimo, 
              MAX(sensor_dados.dados) AS maximo,
              STDDEV(sensor_dados.dados) AS desvio_padrao
              FROM sensor_dados 
              INNER JOIN sensor  ON sensor_dados.device = sensor.macadress
              WHERE sensor.id = $codigoget";
              
              // Execute a consulta e obtenha os resultados
              $result_stats = mysqli_query($conn, $sql_stats);
              
              // Extrair os valores dos resultados
              if ($row = mysqli_fetch_assoc($result_stats)) {
                $minimo = $row['minimo'];
                $media = $row['media'];
                $maximo = $row['maximo'];
                $desvio = $row['desvio_padrao'];
              }
            ?>

            
         
            <script>
              google.charts.load('upcoming', {'packages': ['vegachart']}).then(drawChart5);
              
              function drawChart5() {
                const dataTable = new google.visualization.DataTable();
                dataTable.addColumn({type: 'string', 'id': 'category'});
                dataTable.addColumn({type: 'number', 'id': 'amount'});
                dataTable.addRows([
                  ['Minimo', <?php echo $minimo ?>],
                  ['Media', <?php echo $media ?>],
                  ['Maximo', <?php echo $maximo ?>],
                  ['Desvio Padrão', <?php echo $desvio ?>]
                ]);
                
                const options = {
                  "vega": {
                    "$schema": "https://vega.github.io/schema/vega/v4.json",
                    "width": 500,
                    "height": 200,
                    "padding": 5,
                    
                    'data': [{'name': 'table', 'source': 'datatable'}],
                    
                    "signals": [
                      {
                        "name": "tooltip",
                        "value": {},
                        "on": [
                          {"events": "rect:mouseover", "update": "datum"},
                          {"events": "rect:mouseout",  "update": "{}"}
                        ]
                      }
                    ],
                    
                    "scales": [
                      {
                        "name": "xscale",
                        "type": "band",
                        "domain": {"data": "table", "field": "category"},
                        "range": "width",
                        "padding": 0.05,
                        "round": true
                      },
                      {
                        "name": "yscale",
                        "domain": {"data": "table", "field": "amount"},
                        "nice": true,
                        "range": "height"
                      }
                    ],
                    
                    "axes": [
                      { "orient": "bottom", "scale": "xscale" },
                      { "orient": "left", "scale": "yscale" }
                    ],
                    
                    "marks": [
                      {
                        "type": "rect",
                        "from": {"data":"table"},
                        "encode": {
                          "enter": {
                            "x": {"scale": "xscale", "field": "category"},
                            "width": {"scale": "xscale", "band": 1},
                            "y": {"scale": "yscale", "field": "amount"},
                            "y2": {"scale": "yscale", "value": 0}
                          },
                          "update": {
                            "fill": {"value": "steelblue"}
                          },
                          "hover": {
                            "fill": {"value": "red"}
                          }
                        }
                      },
                      {
                        "type": "text",
                        "encode": {
                          "enter": {
                            "align": {"value": "center"},
                            "baseline": {"value": "bottom"},
                            "fill": {"value": "#333"}
                          },
                          "update": {
                            "x": {"scale": "xscale", "signal": "tooltip.category", "band": 0.5},
                            "y": {"scale": "yscale", "signal": "tooltip.amount", "offset": -2},
                            "text": {"signal": "tooltip.amount"},
                            "fillOpacity": [
                              {"test": "datum === tooltip", "value": 0},
                              {"value": 1}
                            ]
                          }
                        }
                      }
                    ]
                  }
                };
                
                const chart = new google.visualization.VegaChart(document.getElementById('chart-div'));
                chart.draw(dataTable, options);
              }
            </script>
           

           
            <div class="clearfix"></div>
            <br>
        <div id="chart-div" style="width: 700px; height: 250px;"></div>

            <div class="clearfix"></div>
            <br>
            
            
            <?php
              // Sua query SQL
              $query = "SELECT id, id_sensor, dados, data, read_open, id_dados
FROM notif
WHERE id_sensor = '$codigoget' and dados != 'Desligado'
ORDER BY data DESC
LIMIT 15;
";
              // Aqui você executa sua query e obtém os resultados, vamos supor que você já tenha essa parte do código
              
              // Vamos criar um array PHP para armazenar os resultados da consulta
                           
              $result = $conn->query($query);
              
              // Verificando se há resultados e preenchendo o array $dados
              $dados = array();
              if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                  // Preencha o array com os resultados da consulta
                  $dados[] = $row;
                }
              }
              
         
              
              // Convertendo os dados do PHP em um formato adequado para JavaScript
              $data_js = "var dados = [";
              
              foreach ($dados as $row) {
                // Construa as linhas de dados conforme necessário
                $data_js .= "[new Date('" . $row['data'] . "'), " . $row['dados'] . ", '" . $row['alerta'] . "', '" . $row['alerta'] . "', null, null, null],";
              }
              
              $data_js .= "];";
            ?>
            
            <script type='text/javascript'>
              google.charts.load('current', {'packages':['annotationchart']});
              google.charts.setOnLoadCallback(drawChart3);
              
              function drawChart3() { // Corrigindo o nome da função
                <?php echo $data_js; ?> // Inserindo os dados PHP convertidos para JavaScript
                
                var data = new google.visualization.DataTable();
                data.addColumn('date', 'Date');
                data.addColumn('number', 'Dados');
                data.addColumn('string', 'Alerta');
                data.addColumn('string', 'Alerta Text');
                data.addColumn('number', 'Gliese 163 mission');
                data.addColumn('string', 'Gliese title');
                data.addColumn('string', 'Gliese text');
                data.addRows(dados);
                
                var chart = new google.visualization.AnnotationChart(document.getElementById('chart_div3'));
                
                var options = {
                  displayAnnotations: true
                };
                
                chart.draw(data, options);
              }
            </script>
            <div class="clearfix"></div>
            <br>
            <div id='chart_div3' style='width: 900px; height: 600px;'></div>

            <div class="clearfix"></div>
            <br>

 <!-- Posicionamento -->
  <?php 
            // Defina a data de 30 dias atrás
            $date_limit = date('Y-m-d H:i:s', strtotime('-30 days'));
            
            // Consulta SQL para selecionar os dados dos sensores com sensor.id_sensor_type = 3 nos últimos 30 dias
    $sql = "SELECT sd.device, sd.dados, dados_ids.nome, dados_ids.up_time, sd.reg_date
    FROM sensor_dados sd
    INNER JOIN (
      SELECT  sensor_dados.reg_date,sensor_dados.device, (sensor_dados.id) AS ultimo_id
      FROM sensor_dados
      WHERE sensor_dados.reg_date >= '$date_limit'  -- Limite para as últimas 48 horas
    ) ultimos_ids ON sd.device = ultimos_ids.device AND sd.id = ultimos_ids.ultimo_id
    INNER JOIN (
      SELECT id,nome AS nome, up_time, macadress, id_sensor_type
      FROM sensor
    ) dados_ids ON sd.device = dados_ids.macadress
    WHERE dados_ids.id = $codigoget";
            
            // Execute a consulta e obtenha os resultados
            $result = mysqli_query($conn, $sql);
    
                     
            // Converta os resultados em um array associativo para JSON
            $rows = array();
            while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
            }
            $json_data5 = json_encode($rows);
?>
            <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChartstata);
              
              function drawChartstata() {
                var jsonData5 = <?php echo $json_data5; ?>; // Dados JSON da sua consulta PHP
                
                var data = new google.visualization.DataTable();
                data.addColumn('number', 'Dados');
                data.addColumn('number', 'Dados');
                
                // Adiciona os dados do JSON ao DataTable
                for (var i = 0; i < jsonData5.length; i++) {
                  data.addRow([parseFloat(jsonData5[i].dados), parseFloat(jsonData5[i].dados)]);
                }
                
                var options = {
                  hAxis: {title: 'Dados'},
                  vAxis: {title: 'Dados'},
                  chartArea: {width: '50%'},
                  trendlines: {
                    0: {
                      type: 'linear',
                      showR2: true,
                      visibleInLegend: true
                    }
                  }
                };
                
                var chartLinear = new google.visualization.ScatterChart(document.getElementById('chartLinear'));
                chartLinear.draw(data, options);
                
                options.trendlines[0].type = 'exponential';
                options.colors = ['#6F9654'];
                
                var chartExponential = new google.visualization.ScatterChart(document.getElementById('chartExponential'));
                chartExponential.draw(data, options);
              }
            </script>
            <div class="clearfix"></div>
            <br>
              
            <div id="chartLinear" style="height: 350px; width: 800px"></div>
          <div id="chartExponential" style="height: 350px; width: 800px"></div>
            <div class="clearfix"></div>
            <br>

             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-device-iot">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>


                </div>
              </div>




                </div>
              </div>
             
               