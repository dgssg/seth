  <?php
$codigoget = ($_GET["id"]);

// Create connection

include("database/database.php");
    $query = "SELECT sensor.id, sensor.macadress, sensor_type.nome as 'id_sensor_type', sensor.nome, sensor.id_area, sensor.id_status, sensor.up_time, sensor.file, sensor.reg_date, sensor.upgrade, sensor.bat, sensor.last_alarm, sensor.trash, sensor.id_alarm, sensor.imei, sensor.firmware, sensor.linha, sensor.iccid, sensor.id_location, sensor_usb.nome as 'usb', sensor_charger.nome as 'charger', sensor.ativo, sensor.dados, sensor.id_dados,area.nome as 'area',setor.nome as 'setor',unidade.unidade FROM sensor LEFT JOIN sensor_type ON sensor_type.id =  sensor.id_sensor_type LEFT JOIN area ON area.id = sensor.id_area LEFT join setor ON setor.id = area.id_setor LEFT JOIN unidade ON unidade.id = setor.id_unidade LEFT JOIN sensor_usb ON sensor_usb.id =  sensor.usb LEFT JOIN sensor_charger ON sensor_charger.id =  sensor.charger WHERE sensor.id = $codigoget";
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($id,$macadress,$id_sensor_type, $nome, $id_area, $id_status,$up_time, $file, $reg_date,$upgrade, $bat, $last_alarm, $trash, $id_alarm, $imei, $firmware, $linha, $iccid, $id_location, $usb,$charger, $ativo, $dados, $id_dados,$area,$setor,$unidade);
      while ($stmt->fetch()) {
        
      }
    }
    $query = "SELECT token_sensor FROM tools";
    
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($token_sensor);
      while ($stmt->fetch()) {
        //printf("%s, %s\n", $solicitante, $equipamento);
      }
    }

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Dashboard <small>Telemetria</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Telecomando <small>Sensor</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <input type="hidden" id="codigoget" name="codigoget" value="<?php printf($macadress); ?>" readonly="readonly" required="required" class="form-control ">
                <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Macadress <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="$macadress" name="$macadress" value="<?php printf($macadress); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Sensor <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($id_sensor_type); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Nome<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="categoria" name="categoria" required="required" class="form-control"  value="<?php printf($nome); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="info" class="form-control" type="text" name="info"  value="<?php printf($unidade); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($setor); ?>" readonly="readonly">
                        </div>
                      </div>
                

                 
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Area</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($area); ?>" readonly="readonly">
                    </div>
                  </div>

                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">ICIID</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="ICCID" class="form-control" type="text" name="ICCID"  value="<?php printf($iccid); ?>" readonly="readonly">
                      </div>
                    </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>



                  </div>
                </div>
              </div>
	       </div>
 
            <div class="clearfix"></div>
            
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Telecomando <small>Sensor</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php
                      
                       // URL da API
                      $url = 'https://veye.com.br/api/m/clienteAPI/autenticado/' . $iccid;
                      
                      // Cabeçalhos da solicitação
                      $headers = array(
                        'Authorization: Bearer ' . $token_sensor
                      );

                      
                      // Inicializa o cURL
                      $ch = curl_init();
                      
                      // Configura as opções do cURL
                      curl_setopt($ch, CURLOPT_URL, $url);
                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                      
                      // Executa a solicitação cURL e armazena a resposta
                      $response = curl_exec($ch);
                      
                      // Verifica se ocorreu algum erro durante a solicitação
                      if(curl_errno($ch)){
                        echo 'Erro ao executar a solicitação cURL: ' . curl_error($ch);
                      }
                      
                      // Fecha a sessão cURL
                      curl_close($ch);
                      
                      // Mostra a resposta da solicitação
                      $data = json_decode($response, true);
                      
                      // Verificando se a solicitação foi bem-sucedida
                      if ($data['status'] == 200) {
                        // Os dados estão disponíveis em $data['conteudo']
                        // Acessando o conteúdo dos dados
                        $conteudo = $data['conteudo'];
                        
                        // Iterando sobre as chaves e valores do array associativo
                        foreach ($conteudo as $chave => $valor) {
                          // Imprimindo a chave e o valor formatados
                          echo '<strong>' . $chave . ':</strong> ' . $valor . '<br>';
                        }

                                             } else {
                        echo 'Erro na solicitação: ' . $data['status'];
                      }
                    ?>

                   </div>
                </div>
              </div>
 <!-- Posicionamento -->
              <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
              <script type="text/javascript">
                google.charts.load('current', {
                  'packages': ['map'],
                  // Note: you will need to get a mapsApiKey for your project.
                  // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
                  'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
                });
                google.charts.setOnLoadCallback(fetchData);
                var codigoget = document.getElementById("ICCID").value;

                function fetchData() {
                  fetch('https://veye.com.br/api/m/clienteAPI/autenticado/geolocation/getBySimcardId/'+codigoget, {
                    headers: {
                      'Authorization: Bearer ' . $token_sensor
                    }
                  })
                  .then(response => response.json())
                  .then(data => {
                    drawMap(data.location);
                  })
                  .catch(error => {
                    console.error('Error fetching data:', error);
                  });
                }
                
                function drawMap(locationData) {
                  var data = new google.visualization.DataTable();
                  data.addColumn('string', 'Address');
                  data.addColumn('string', 'Location');
                  
                  // Adicionando a linha de dados mapeados ao DataTable
                  data.addRows([
                    [locationData.latitude + ', ' + locationData.longitude, 'Custom Location']
                  ]);
                  
                  var options = {
                    mapType: 'styledMap',
                    zoomLevel: 12,
                    showTooltip: true,
                    showInfoWindow: true,
                    useMapTypeControl: true,
                    maps: {
                      // Your custom mapTypeId holding custom map styles.
                      styledMap: {
                        name: 'Styled Map', // This name will be displayed in the map type control.
                        styles: [
                          {featureType: 'poi.attraction',
                            stylers: [{color: '#fce8b2'}]
                          },
                          {featureType: 'road.highway',
                            stylers: [{hue: '#0277bd'}, {saturation: -50}]
                          },
                          {featureType: 'road.highway',
                            elementType: 'labels.icon',
                            stylers: [{hue: '#000'}, {saturation: 100}, {lightness: 50}]
                          },
                          {featureType: 'landscape',
                            stylers: [{hue: '#259b24'}, {saturation: 10}, {lightness: -22}]
                          }
                        ]}}
                  };
                  
                  var map = new google.visualization.Map(document.getElementById('map_div'));
                  
                  map.draw(data, options);
                }
              </script>
           
                <div id="map_div" style="height: 500px; width: 900px"></div>
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-device-iot">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>


                </div>
              </div>




                </div>
              </div>
             
               