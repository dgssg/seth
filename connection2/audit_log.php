<?php

// Função para registrar ação de log
function logAction($userId, $message) {
    global $conn; // Mantendo o uso da variável global $conn

    // Verificar se o ID do usuário é nulo ou "Desconhecido" e ajustar
    if ($userId === null || $userId === 'Desconhecido') {
        $userId = null; // Definir como null
    }

    try {
        // Preparar a consulta SQL
        $stmt = $conn->prepare("INSERT INTO audit_log (user_id, action, created_at) VALUES (?, ?, NOW())");

        // Verificar se o ID do usuário é null e ajustar o bind adequadamente
        if ($userId === null) {
            $stmt->bind_param("ss", $userId, $message);
        } else {
            $stmt->bind_param("ss", $userId, $message);
        }

        // Executar a consulta
        $stmt->execute();

        // Fechar o statement
        $stmt->close();
    } catch (Exception $e) {
        // Tratar erro de log
        error_log("Erro ao registrar log: " . $e->getMessage());
    }
}
