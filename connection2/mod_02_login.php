<?php

$base = "cvheal47_building_";
$mod = "eb";
$database = "$base$instituicao";
$conn = mysqli_connect("localhost", "cvheal47_root", "cvheal47_root", $database);

if (!$conn) {
    // Opcional: Registrar um log para auditoria em caso de erro na conexão
    error_log('Erro ao conectar ao banco de dados: ' . mysqli_connect_error());

    // Redirecionar para a página de erro
    header('Location: index.php');
    exit;
}

require_once 'audit_log.php'; // Registro de auditoria

// Verificar o token CSRF primeiro
if (!isset($_POST['csrf_token']) || $_POST['csrf_token'] !== $_SESSION['csrf_token']) { 
    logAction(null, 'Erro de segurança: Token CSRF inválido');
    $_SESSION['error'] = 'Erro de segurança: Token CSRF inválido.';
    header('Location: index.php');
    exit;
}

// Preparar a consulta para buscar o usuário
$stmt = $conn->prepare('SELECT * FROM usuario WHERE username = ? AND is_active = 1');
if (!$stmt) {
    die('Erro na preparação da consulta: ' . $conn->error);
}

//$usuario = $_POST['username']; // Ajustar para o método correto de obtenção do username
$stmt->bind_param('s', $usuario);
$stmt->execute();
$result = $stmt->get_result();
$user = $result->fetch_assoc();
$stmt->close();

if ($user && password_verify($senha, $user['password_hash'])) {
    // Gerar token de sessão único
    $sessionToken = bin2hex(random_bytes(32));

    // Atualizar o último acesso do usuário
    $updateStmt = $conn->prepare('UPDATE usuario SET last_acess = NOW() WHERE id = ?');
    $updateStmt->bind_param('i', $user['id']);
    $updateStmt->execute();
    $updateStmt->close();

    // Atualizar o token de sessão, último login e IP
    $updateStmt = $conn->prepare('UPDATE usuario SET session_token = ?, last_login = NOW(), last_ip = ? WHERE id = ?');
    $updateStmt->bind_param('ssi', $sessionToken, $_SERVER['REMOTE_ADDR'], $user['id']);
    $updateStmt->execute();
    $updateStmt->close();

    // Registrar o token na sessão
    $_SESSION['session_token'] = $sessionToken;

    // Registrar auditoria de login bem-sucedido
    logAction($user['id'], 'Login bem-sucedido.');

    // Configurar variáveis de sessão
    $_SESSION['id_user'] = $user['id'];
    $_SESSION['usuario'] = $user['nome'];
    $_SESSION['id_usuario'] =  $user['id'];
    $_SESSION['instituicao'] = $instituicao;
    $_SESSION['setor'] = $user['id_setor'];
    $_SESSION['email'] = $user['email'];
    $_SESSION['id_nivel'] = $user['id_nivel'];
    $_SESSION['time'] = time() + (30);
    $_SESSION['mod'] = $mod;

    // Redirecionar com base no nível de acesso
    $nivel = $user['id_nivel'];
    switch ($nivel) {
        case 1:
            $redirect_url = "customer-eb/$instituicao/dashboard";
            break;
        case 2:
            $redirect_url = "customer-eb/$instituicao/stakeholder/technician/dashboard";
            break;
        case 3:
            $redirect_url = "customer-eb/$instituicao/stakeholder/user/dashboard";
            break;
        case 4:
            $redirect_url = "customer-eb/$instituicao/stakeholder/manager/dashboard";
            break;
        case 5:
            $redirect_url = "customer-eb/$instituicao/stakeholder/supervision/dashboard";
            break;
        case 6:
            $redirect_url = "customer-eb/$instituicao/stakeholder/logistic/dashboard";
            break;
        default:
            $redirect_url = 'index.php';
    }
} else {
    // Caso o login falhe
      logAction(null, 'Tentativa de login falhou para username: ' . $usuario);

            // Mensagem de erro
            $_SESSION['error'] = 'Credenciais inválidas.';
    $_SESSION['error'] = 'Usuário ou senha inválidos.';
    $redirect_url = 'index.php';
}

// Redirecionar
echo "<script>document.location='$redirect_url'</script>";
?>
