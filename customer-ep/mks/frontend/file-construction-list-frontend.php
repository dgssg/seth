<?php
include("database/database.php");
$query = "SELECT construction.id,construction.cod,construction.orc,construction.des,construction.city,construction.state,customer.customer FROM construction INNER JOIN customer ON customer.id = construction.id_customer WHERE construction.trash = 0 ";
if($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$cod,$orc,$des, $city, $state,$customer);


  ?>






  <div class="row">
    <div class="col-sm-12">
      <div class="card-box table-responsive">

        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Codigo</th>
              <th>Orçamento</th>
              <th>Descrição</th>
              <th>Estado</th>
              <th>Cidade</th>
              <th>Cliente</th>
              <th>Ação</th>

            </tr>
          </thead>


          <tbody>
            <?php   while ($stmt->fetch()) {   ?>
              <tr>
                <td><?php printf($id); ?> </td>

                <td><?php printf($cod); ?> </td>
                <td><?php printf($orc); ?> </td>
                <td><?php printf($des); ?> </td>
                <td><?php printf($city); ?> </td>
                <td><?php printf($state); ?></td>
                <td><?php printf($customer); ?></td>


                <td>


                  <a class="btn btn-app"  href="file-construction-edit?id=<?php printf($id); ?>"onclick="new PNotify({
                    title: 'Editar',
                    text: 'Edição de Alerta!',
                    type: 'info',
                    styling: 'bootstrap3'
                  });">
                  <i class="glyphicon glyphicon-edit"></i> Editar
                </a>
                <a  class="btn btn-app" href="backend/file-construction-trash-backend.php?id=<?php printf($id); ?> " onclick="new PNotify({
                  title: 'Excluir',
                  text: 'Exclusão Alerta!',
                  type: 'danger',
                  styling: 'bootstrap3'
                });" >
                <i class="fa fa-trash"></i> Excluir
              </a>



            </td>
          </tr>
        <?php   } }  ?>
      </tbody>
    </table>
  </div>
</div>
</div>
