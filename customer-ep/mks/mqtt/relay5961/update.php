<?php
include("../../database/database.php");

$codigoget = (int)($_GET["id"]);

$sn_payload = $_POST['sn_payload'];
$ip_payload = $_POST['ip_payload'];
$r1_payload = $_POST['r1_payload'];
$r2_payload = $_POST['r2_payload'];
$r3_payload = $_POST['r3_payload'];
$r4_payload = $_POST['r4_payload'];
$id_nivel_1=1;
$id_nivel_2=2;
$id_nivel_3=3;
$id_nivel_4=4;
$sn_payload_REQUEST = htmlspecialchars($_REQUEST['sn_payload']);
$ip_payload_REQUEST = htmlspecialchars($_REQUEST['ip_payload']);
$r1_payload_REQUEST = htmlspecialchars($_REQUEST['r1_payload']);
$r2_payload_REQUEST = htmlspecialchars($_REQUEST['r2_payload']);
$r3_payload_REQUEST = htmlspecialchars($_REQUEST['r3_payload']);
$r4_payload_REQUEST = htmlspecialchars($_REQUEST['r4_payload']);




$stmt = $conn->prepare("UPDATE device SET sn = ? WHERE id= ?");
$stmt->bind_param("ss",$sn_payload,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE device SET ip = ? WHERE id= ?");
$stmt->bind_param("ss",$ip_payload,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE device SET r1 = ? WHERE id= ?");
$stmt->bind_param("ss",  $r1_payload,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE device SET r2 = ? WHERE id= ?");
$stmt->bind_param("ss",  $r2_payload,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE device SET r3 = ? WHERE id= ?");
$stmt->bind_param("ss",  $r3_payload,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE device SET r4 = ? WHERE id= ?");
$stmt->bind_param("ss",  $r4_payload,$codigoget);
$execval = $stmt->execute();
$stmt->close();

if($r1_payload=="ON" and $r2_payload=="OFF" and $r3_payload=="OFF" and $r4_payload=="OFF" ){

  $stmt = $conn->prepare("UPDATE device SET id_nivel = ? WHERE id= ?");
  $stmt->bind_param("ss",  $id_nivel_1,$codigoget);
  $execval = $stmt->execute();
  $stmt->close();
}
if($r1_payload=="OFF" and $r2_payload=="ON" and $r3_payload=="OFF" and $r4_payload=="OFF" ){

  $stmt = $conn->prepare("UPDATE device SET id_nivel = ? WHERE id= ?");
  $stmt->bind_param("ss",  $id_nivel_2,$codigoget);
  $execval = $stmt->execute();
  $stmt->close();
}
if($r1_payload=="OFF" and $r2_payload=="OFF" and $r3_payload=="ON" and $r4_payload=="OFF" ){

  $stmt = $conn->prepare("UPDATE device SET id_nivel = ? WHERE id= ?");
  $stmt->bind_param("ss",  $id_nivel_3,$codigoget);
  $execval = $stmt->execute();
  $stmt->close();
}
if($r1_payload=="OFF" and $r2_payload=="OFF" and $r3_payload=="OFF" and $r4_payload=="ON" ){

  $stmt = $conn->prepare("UPDATE device SET id_nivel = ? WHERE id= ?");
  $stmt->bind_param("ss",  $id_nivel_4,$codigoget);
  $execval = $stmt->execute();
  $stmt->close();
}

echo "<script>window.close();</script>";
 ?>
