<!DOCTYPE html>
<html>
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"></script>
<body>

<h2>JavaScript</h2>
<p>Subtracting tensors with tensorflow.js</p>

<div id="demo"></div>

<script>
const tensorA = tf.tensor([[1, 2], [3, 4], [5, 6]]);
const tensorB = tf.tensor([[1,-1], [2,-2], [3,-3]]);

// Tensor Subtraction
const tensorNew = tensorA.sub(tensorB);

// Result: [ [0, 3], [1, 6], [2, 9] ]
document.getElementById("demo").innerHTML = tensorNew;
</script>

<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

</body>
<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

</html>
