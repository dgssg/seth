<!DOCTYPE html>
<html>
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"></script>
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-vis"></script><body>

<h2>TensorFlow JavaScript</h2>
<h3>Training data:</h3>
<div id="plot1"></div>
<div id="plot2"></div>
<script>

// Extract Correct Data 
function extractData(obj) {
  return {x:obj.Horsepower, y:obj.Miles_per_Gallon};
}
function removeErrors(obj) {
  return obj.x != null && obj.y != null;
}

// Plot Data
function tfPlot(values, surface) {
  tfvis.render.scatterplot(surface,
    {values:values, series:['Original','Predicted']},
    {xLabel:'Horsepower', yLabel:'MPG',});
}

// Main Function
async function runTF() {
const jsonData = await fetch("cardata.json");
let values = await jsonData.json();
values = values.map(extractData).filter(removeErrors);

// Plot the Data
const surface1 = document.getElementById("plot1");
const surface2 = document.getElementById("plot2");
tfPlot(values, surface1);

// Convert Input to Tensors
const inputs = values.map(obj => obj.x);
const labels = values.map(obj => obj.y);
const inputTensor = tf.tensor2d(inputs, [inputs.length, 1]);
const labelTensor = tf.tensor2d(labels, [labels.length, 1]);
const inputMin = inputTensor.min();  
const inputMax = inputTensor.max();
const labelMin = labelTensor.min();
const labelMax = labelTensor.max();
const nmInputs = inputTensor.sub(inputMin).div(inputMax.sub(inputMin));
const nmLabels = labelTensor.sub(labelMin).div(labelMax.sub(labelMin));

// Create a Tensorflow Model
const model = tf.sequential(); 
model.add(tf.layers.dense({inputShape: [1], units: 1, useBias: true}));
model.add(tf.layers.dense({units: 1, useBias: true}));
model.compile({loss:'meanSquaredError', optimizer:'sgd'});

// Start Training
await trainModel(model, nmInputs, nmLabels, surface2);

// Un-Normalize Data
let unX = tf.linspace(0, 1, 100);      
let unY = model.predict(unX.reshape([100, 1]));      
const unNormunX = unX
  .mul(inputMax.sub(inputMin))
  .add(inputMin);
const unNormunY = unY
  .mul(labelMax.sub(labelMin))
  .add(labelMin);
unX = unNormunX.dataSync();
unY = unNormunY.dataSync();

// Test the Model
const predicted = Array.from(unX).map((val, i) => {
  return {x: val, y: unY[i]}
});
tfPlot([values, predicted], surface1)

// End Main Function
}

// Asyncronous Function to Train the Model
async function trainModel(model, inputs, labels, surface) {
  const batchSize = 25;
  const epochs = 50;
  const callbacks = tfvis.show.fitCallbacks(surface, ['loss'], {callbacks:['onEpochEnd']})
  return await model.fit(inputs, labels,
    {batchSize, epochs, shuffle:true, callbacks:callbacks}
  );
}

runTF();

</script>
<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

</body>
<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

</html>
