<?php
include("database/database.php");
$query = "SELECT id,nome,upgrade,reg_date FROM bank ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$nome,$upgrade,$reg_date);


  ?>






  <div class="row">
    <div class="col-sm-12">
      <div class="card-box table-responsive">

        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Banco</th>
              <th>Cadastro</th>
              <th>Atualização</th>
              <th>Ação</th>

            </tr>
          </thead>


          <tbody>
            <?php   while ($stmt->fetch()) {   ?>
              <tr>
                <td><?php printf($id); ?> </td>

                <td><?php printf($nome); ?> </td>

                <td><?php printf($reg_date); ?></td>
                <td><?php printf($upgrade); ?></td>


                <td>


                  <a class="btn btn-app"  href="file-input-bank-edit?id=<?php printf($id); ?>"onclick="new PNotify({
                    title: 'Editar',
                    text: 'Edição de Alerta!',
                    type: 'info',
                    styling: 'bootstrap3'
                  });">
                  <i class="glyphicon glyphicon-edit"></i> Editar
                </a>
                <a  class="btn btn-app" href="backend/file-bank-trash-backend.php?id=<?php printf($id); ?> " onclick="new PNotify({
                  title: 'Excluir',
                  text: 'Exclusão Alerta!',
                  type: 'danger',
                  styling: 'bootstrap3'
                });" >
                <i class="fa fa-trash"></i> Excluir
              </a>



            </td>
          </tr>
        <?php   } }  ?>
      </tbody>
    </table>
  </div>
</div>
</div>
