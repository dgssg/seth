<!DOCTYPE html>
<html>
<script src="myperceptron.js"></script>
<script src="myplotlib.js"></script>
<body>
<canvas id="myCanvas" width="400px" height="400px" style="width:100%;max-width:400px;border:1px solid black"></canvas>
<div id="demo"></div>
<script>
// Initiate Values
const numPoints = 500;
const learningRate = 0.00001;

// Create a Plotter
const plotter = new XYPlotter("myCanvas");
plotter.transformXY();
const xMax = plotter.xMax;
const yMax = plotter.yMax;
const xMin = plotter.xMin;
const yMin = plotter.yMin;

// Create Random XY Points
const xPoints = [];
const yPoints = [];
for (let i = 0; i < numPoints; i++) {
  xPoints[i] = Math.random() * xMax;
  yPoints[i] = Math.random() * yMax;
}

// Line Function
function f(x) {
  return x * 1.2 + 50;
}

//Plot the Line
plotter.plotLine(xMin, f(xMin), xMax, f(xMax), "black");

// Compute Desired Answers
const desired = [];
for (let i = 0; i < numPoints; i++) {
  desired[i] = 0;
  if (yPoints[i] > f(xPoints[i])) {desired[i] = 1}
}

// Create a Perceptron
const ptron = new Perceptron(2, learningRate);

// Train the Perceptron
for (let j = 0; j <= 10000; j++) {
  for (let i = 0; i < numPoints; i++) {
    ptron.train([xPoints[i], yPoints[i]], desired[i]);
  }
}

// Test Against Unknown Data
const counter = 500;
let errors = 0;
for (let i = 0; i < counter; i++) {
  let x = Math.random() * xMax;
  let y = Math.random() * yMax;
  let guess = ptron.activate([x, y, ptron.bias]);
  let color = ((guess == 0) ? "blue" : "black");
  plotter.plotPoint(x, y, color);
  if (y > f(x) && guess == 0) {errors++}
}
document.getElementById("demo").innerHTML = errors + " Errors out of " + counter;
</script>
<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

</body>
<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

</html>
