<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../../index");
	}
	if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
	$usuariologado=$_SESSION['usuario'];

	// Adiciona a Função display_campo($nome_campo, $tipo_campo)
	//require_once "personalizacao_display";

?>
﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../../../framework/images/favicon.ico" type="image/ico" />

    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>

    <!-- Bootstrap -->
    <link href="../../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="../../../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../../../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../../../framework/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard" class="site_title"><i class="fa fa-500px"></i> <span>SETH Tecnico</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="../logo/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?php printf($usuariologado) ?></h2>
                <p class="glyphicon glyphicon-time" id="countdown"></p>
               <span>
              <script> document.write(new Date().toLocaleDateString()); </script>
              </span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Geral</h3>
                <ul class="nav side-menu">

									<li><a href="dashboard"><i class="fa fa-edit"></i> Manuais </span></a> </li>
									<li><a href="service"><i class="fa fa-wrench"></i> Serviço </span></a> </li>
									<li><a href="new-service"><i class="fa fa-plus"></i>Novo Serviço </span></a> </li>
                <!--   <li><a href="insumos">insumos </a></li>
                  <!--     <li><a href="form_upload">Form Upload</a></li> -->
                   <!--    <li><a href="form_buttons">Form Buttons</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Orçamento  <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <!--<li><a href="general_elements">Instituição</a></li>
                      <li><a href="sintetico">Sintetico</a></li>
                      <li><a href="analitico">Analitico</a></li>
                      <li><a href="abc">Curva ABC</a></li>
                      <li><a href="fornecedor">Fornecedor</a></li>
                      <li><a href="bdi">BDI</a></li>
                   <!--   <li><a href="widgets">Widgets</a></li> -->
                   <!--   <li><a href="invoice">Invoice</a></li> -->
                  <!--    <li><a href="inbox">Inbox</a></li>

                    </ul>
                  </li>
              <!--    <li><a><i class="fa fa-search-plus"></i> Consulta <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements_consulta">Instituição</a></li>
                      <li><a href="media_gallery_consulta">Localização</a></li>
                      <li><a href="typography_consulta">Equipamento</a></li>
                      <li><a href="icons_consulta">Colaborador</a></li>
                      <li><a href="glyphicons_Consulta">Fornecedor</a></li>
                   <!--   <li><a href="widgets">Widgets</a></li> -->
                   <!--   <li><a href="invoice">Invoice</a></li> -->
                  <!--    <li><a href="inbox">Inbox</a></li> -->

                <!--    </ul>
                </li> -->
               <!--   <li><a><i class="fa fa-table"></i> Cronograma <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="cronograma">Cadastro</a></li>
                      <!--<li><a href="fechamento">Fechamento</a></li>
			 <li><a href="imprimir">Imprimir</a></li>
                      <li><a href="curvas">Curva S</a></li>
			                   <li><a href="fisico">Fisico</a></li>
                      <!--<li><a href="historico">Historico</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Recursos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="seletividade">Seletividade</a></li>
                      <li><a href="adm">Administracao</a></li>
                      <li><a href="alvenaria">Alvenaria</a></li>
                      <li><a href="luminotecnico">Luminotecnico</a></li>
                      <li><a href="eletroduto">Eletroduto</a></li>
                    <!--  <li><a href="morisjs">Moris JS</a></li> -->
                     <!-- <li><a href="echarts">ECharts</a></li> -->
                     <!-- <li><a href="other_charts">Other Charts</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cart-arrow-down"></i> Banco <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="encargos">Encargos</a>
                        <li><a href="tcpo">TCPO</a>
                        <li><a href="stabille">STABILLE</a>
                        <li><a href="sicro2">SICRO2</a>
                        <li><a href="sicro2">SINAPI</a>
                        <li><a href="pini">PINI</a>
                        <li><a href="copel">COPEL</a>
                        <!--  <ul class="nav child_menu">
                            <li class="sub_menu"><a href="recebimento">Recebiemento</a>
                            </li>
                            <li><a href="saida">Saida</a>
                            </li>
                            <li><a href="levantamento">Levantamento</a>
                            </li>
                          </ul>

                    </ul>
                  </li>
                <!--  <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar">Fixed Sidebar</a></li>
                      <li><a href="fixed_footer">Fixed Footer</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                     <h3>Recursos</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Memorial <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="memorial">Documentos</a></li>
                    <li><a href="leed">LEED</a></li>
                    <li><a href="normativa">Normativa</a></li>
                    <li><a href="equivalencia">Equivalencia</a></li>
                    <li><a href="pee">PEE</a></li>
                    <li><a href="pscip">PSCIP</a></li>
                    <li><a href="spda">SPDA</a></li>
                    <li><a href="liewin">LIEWIN</a></li>
                    <li><a href="rbc">RBC</a></li>
                    <li><a href="hequip">H Equip</a></li>
                    <li><a href="hhora">H Hora</a></li>
                    <li><a href="cotacao">Cotação</a></li> -->


                  <!--    <li><a href="contacts">Contacts</a></li>
                      <li><a href="profile">Profile</a></li>
                    </ul>
                  </li>

            <!--      <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul> -->

                </div>
            <!--    <div class="menu_section">
                  <h3>Manuais &amp; ADMINISTRATIVO</h3>
                  <ul class="nav side-menu">
                    <li><a href="../contratosphp"target="_blank"><i class="fa fa-pencil-square-o"></i> Manuais Usuário </span></a>
                      <!--    <ul class="nav child_menu">
                        <li><a href="e_commerce">Novo</a></li>
                        <li><a href="projects">Contratos</a></li>
                        <li><a href="project_detail">Gestao</a></li>
                           <li><a href="contacts">Contacts</a></li>
                        <li><a href="profile">Profile</a></li>
                      </ul> -->
             <!--       </li>
                      <li><a  href="../sigcon"target="_blank"><i class="fa fa-check-square-o"></i> Manuais Tecnico </span></a>
                      <!--    <ul class="nav child_menu">
                          <li><a href="e_commerce">novo</a></li>
                          <li><a href="projects">Formularios</a></li>
                        <!--    <li><a href="contacts">Contacts</a></li>
                          <li><a href="profile">Profile</a></li>
                        </ul> -->
              <!--        </li>
                      <li><a href="../geradoc"target="_blank"><i class="fa fa-dropbox"></i> Dropzone </span></a>
                      <!--    <ul class="nav child_menu">
                          <li><a href="e_commerce">Arquivos</a></li>
                          <li><a href="projects">Documentos</a></li>
                          <li><a href="contacts">Contacts</a></li>
                          <li><a href="profile">Profile</a></li>
                        </ul>-->
                <!--      </li>
                    <li><a href="../rfid"target="_blank"><i class="fa fa-tags"></i> Rastreabilidade </span></a>
                  <!--    <ul class="nav child_menu">
                      <li><a href="e_commerce">Arquivos</a></li>
                      <li><a href="projects">Documentos</a></li>
                      <li><a href="contacts">Contacts</a></li>
                      <li><a href="profile">Profile</a></li>
                    </ul>
                  </li>

              </div> -->

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
          <!--  <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                <script>
                var goFS = document.getElementById("goFS");
                goFS.addEventListener("click", function() {
                  document.body.requestFullscreen();
                }, false);
              </script>

              </a>
              <a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="lockscreen">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Contrato" href="../contratosphp/pages/login"target="_blank">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Formulario" href="../sigcon"target="_blank">
                <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Arquivo" href="../geradoc"target="_blank">
                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Rastreabilidade" href="../rfid"target="_blank">
                <span class="glyphicon glyphicon-tags" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Predial" href="../predial"target="_blank">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
              </a>
                <a data-toggle="tooltip" data-placement="top" title="Manuais Tecnico" href="../tecnico"target="_blank">
                <span class="glyphicon glyphicon glyphicon-folder-close" aria-hidden="true"></span>
              </a>
                <a data-toggle="tooltip" data-placement="top" title="Manuais Usuário" href="../usuario"target="_blank">
                <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
              </a>
                <a data-toggle="tooltip" data-placement="top" title="Projetos" href="../projetos"target="_blank">
                <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
              </a>

            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">


                  </li>

              <!--    <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false" title="Novas Ordem de Serviços">
						<i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were WHERE...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were WHERE...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were WHERE...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were WHERE...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <div class="text-center">
                          <a class="dropdown-item">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li> -->
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

        <!-- /top navigation -->

        <!-- page content -->


        <div class="right_col" role="main">
          <div class="">
            <div class="row" style="display: inline-block;">
            <div class="top_tiles">

              <div class="alert alert-block alert-success">
                <button type="button" class="close" data-dismiss="alert">
                  <i class="ace-icon fa fa-times"></i>
                </button>

                <i class="ace-icon fa fa-check red"></i>

                Bem vindo ao
                <strong class="black">
                  SETH
                  <small>(v1.4)</small>
                </strong>,
sistema de Orçamento Tecnica Hospitalar <a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas</a> .
              </div>
            <!--  <object style="width:150%; height:600px" data="https://drive.google.com/drive/folders/184P049nufCsyqnLlPjVDI1zsc6mOp5e-?usp=sharing" type="application/pdf"  >
    <p>Seu navegador não tem um plugin pra PDF</p>
</object>    -->

<iframe src="https://drive.google.com/embeddedfolderview?id=184P049nufCsyqnLlPjVDI1zsc6mOp5e-#list" style="width:150%; height:500px; border:0;"></iframe>
           <!--   <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-wrench"></i></div>
                  <object  type="text/html" data="../php/dashboard/solicitado" > </object>
                  <h3>Solicitado</h3>
                  <p>Ordem de serviço solicitado.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-unlock"></i></div>
                      <object  type="text/html" data="../php/dashboard/aberto"> </object>
                  <h3>Aberto</h3>
                  <p>Ordem de serviço aberta.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-paper-plane"></i></div>
                    <object  type="text/html" data="../php/dashboard/andamento"> </object>
                  <h3>Andamento</h3>
                  <p>Ordem de serviço em processo.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-check-square-o"></i></div>
                    <object  type="text/html" data="../php/dashboard/concluido"> </object>
                  <h3>Concluido</h3>
                  <p>Ordem de serviço finalizadas</p>
                </div>
              </div>
            </div>
          </div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Indicador de Preventivas <small> Preventivas Executadas</small></h2>
                    <div class="filter">
                      <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-9 col-sm-12 ">

                <td>  <object style="width:130%; height:550px"   type="text/html" data="../php/dashboard/cohort"> </object> </td>

                      <div class="tiles">
                        <div class="col-md-4 tile">
                          <span>Preventivas Previstas</span>
                          <object  type="text/html" data="../php/dashboard/preventivaprevista"   > </object>
                          <span class="sparkline11 graph" style="height: 160px;">
                               <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                          </span>
                        </div>
                        <div class="col-md-4 tile">
                          <span>Preventivas Executadas</span>
                            <object  type="text/html" data="../php/dashboard/preventivaexecultada"> </object>
                          <span class="sparkline22 graph" style="height: 160px;">
                                <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                          </span>
                        </div>
                        <div class="col-md-4 tile">

                          <span class="sparkline11 graph" style="height: 160px;">
                                 <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                          </span>
                        </div>
                      </div>

                    </div>

              <!--      <div class="col-md-3 col-sm-12 ">
                      <div>
                        <div class="x_title">
                          <h2>Top Profiles</h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="#">Settings 1</a>
                                  <a class="dropdown-item" href="#">Settings 2</a>
                                </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <ul class="list-unstyled top_profiles scroll-view">
                          <li class="media event">
                            <a class="pull-left border-aero profile_thumb">
                              <i class="fa fa-user aero"></i>
                            </a>
                            <div class="media-body">
                              <a class="title" href="#">Ms. Mary Jane</a>
                              <p><strong>$2300. </strong> Agent Avarage Sales </p>
                              <p> <small>12 Sales Today</small>
                              </p>
                            </div>
                          </li>
                          <li class="media event">
                            <a class="pull-left border-green profile_thumb">
                              <i class="fa fa-user green"></i>
                            </a>
                            <div class="media-body">
                              <a class="title" href="#">Ms. Mary Jane</a>
                              <p><strong>$2300. </strong> Agent Avarage Sales </p>
                              <p> <small>12 Sales Today</small>
                              </p>
                            </div>
                          </li>
                          <li class="media event">
                            <a class="pull-left border-blue profile_thumb">
                              <i class="fa fa-user blue"></i>
                            </a>
                            <div class="media-body">
                              <a class="title" href="#">Ms. Mary Jane</a>
                              <p><strong>$2300. </strong> Agent Avarage Sales </p>
                              <p> <small>12 Sales Today</small>
                              </p>
                            </div>
                          </li>
                          <li class="media event">
                            <a class="pull-left border-aero profile_thumb">
                              <i class="fa fa-user aero"></i>
                            </a>
                            <div class="media-body">
                              <a class="title" href="#">Ms. Mary Jane</a>
                              <p><strong>$2300. </strong> Agent Avarage Sales </p>
                              <p> <small>12 Sales Today</small>
                              </p>
                            </div>
                          </li>
                          <li class="media event">
                            <a class="pull-left border-green profile_thumb">
                              <i class="fa fa-user green"></i>
                            </a>
                            <div class="media-body">
                              <a class="title" href="#">Ms. Mary Jane</a>
                              <p><strong>$2300. </strong> Agent Avarage Sales </p>
                              <p> <small>12 Sales Today</small>
                              </p>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div> -->

                  </div>
                </div>
              </div>
            </div>



          <!--  <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Indicador de Causas <small>Defeito</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Parametro 1</a>
                            <a class="dropdown-item" href="#">Parametro 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
                      <div class="col-md-7" style="overflow:hidden;">
                        <span class="sparkline_one" style="height: 160px; padding: 10px 25px;">
                                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                  </span>
                        <h4 style="margin:5px">Ativos de Ordem de Serviço</h4>
                      </div>

                      <div class="col-md-12">
                        <div class="row" style="text-align: center;">
                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/desgaste"> </object> </td>

                          </div>
                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/erro"> </object> </td>

                          </div>
                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/falha"> </object> </td>

                          </div>
                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/instalacao"> </object> </td>

                          </div>
                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/rotina"> </object> </td>

                          </div>
                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/calibracao"> </object> </td>

                          </div>
                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/acessorio"> </object> </td>

                          </div>

                          <div class="col-md-4">
                            <td>  <object style="width:100%; height:200px"   type="text/html" data="../php/dashboard/abuso"> </object> </td>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>




      <!--      <div class="row">
              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Top Profiles <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item One Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Three Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Top Profiles <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item One Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Three Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Top Profiles <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item One Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Two Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month">April</p>
                        <p class="day">23</p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#">Item Three Title</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      </div>
                    </article>
                  </div>
                </div> -->
               <!-- <div class="row">

                  <div class="col-md-4 col-sm-4 ">
                    <div class="x_panel tile fixed_height_320">
                      <div class="x_title">
                        <h2>Setores Preventivas</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Parametro 1</a>
                                <a class="dropdown-item" href="#">Parametro 2</a>
                              </div>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>

                    <!--  <div class="x_content">
                        <h4>Preventivas em Aberto</h4>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>UTI</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 66%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>123k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>CC</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>53k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>PA</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>23k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>PS</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>3k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>CME</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>1k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <object style="width:100%; height:500px"  type="text/html" data="../php/dashboard/previstas"   > </object>
                      </div>
                    </div>
                  </div> -->

               <!--   <div class="col-md-8 col-sm-12 ">
                    <div class="x_panel tile fixed_height_320 overflow_hidden">
                      <div class="x_title">
                        <h2>Setores OS</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Parametro 1</a>
                                <a class="dropdown-item" href="#">Parametro 2</a>
                              </div>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                      <!--  <table class="" style="width:100%">
                          <tr>
                            <th style="width:10%;">
                              <p>Indicador</p>
                            </th>
                            <th>
                              <div class="col-lg-5 col-md-2 col-sm-12 ">
                                <p class="">Setor</p>
                              </div>
                              <div class="col-lg-5 col-md-2 col-sm-12 ">
                                <p class="">      (%)</p>
                              </div>
                            </th>
                          </tr>
                          <tr>
                            <td> -->
                            <!--    <td>  <object style="width:100%; height:500px"   type="text/html" data="../php/dashboard/doughnutchart"> </object> </td>
                              <!--<canvas class="canvasDoughnut" height="150" width="140" style="margin: 5px 5px 10px 0"></canvas>
                            </td>
                      <!--      <td>
                              <table class="tile_info">
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square blue"></i>UTI </p>
                                  </td>
                                  <td>  <object height="38" width="38" type="text/html" data="../php/dashboard/uti"> </object> </td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square green"></i>CC </p>
                                  </td>
                                    <td>    <object height="38" width="38" type="text/html" data="../php/dashboard/cc"> </object> </td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square purple"></i>PA </p>
                                  </td>
                                  <td>     <object height="38" width="38" type="text/html" data="../php/dashboard/pa"> </object> </td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square aero"></i>PS </p>
                                  </td>
                                    <td>    <object height="38" width="38" type="text/html" data="../php/dashboard/ps"> </object> </td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square red"></i>CME</p>
                                  </td>
                                    <td>     <object height="38" width="38" type="text/html" data="../php/dashboard/cme"> </object> </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table> -->
                 <!--     </div>
                    </div>
                  </div>


              <!--    <div class="col-md-4 col-sm-4 ">
                    <div class="x_panel tile fixed_height_320">
                      <div class="x_title">
                        <h2>Quick Parametro</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Parametro 1</a>
                                <a class="dropdown-item" href="#">Parametro 2</a>
                              </div>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div class="dashboard-widget-content">
                          <ul class="quick-list">
                            <li><i class="fa fa-calendar-o"></i><a href="#">Parametro</a>
                            </li>
                            <li><i class="fa fa-bars"></i><a href="#">Subscription</a>
                            </li>
                            <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                            </li>
                            <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                            </li>
                            <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div> -->

              </div>
              <!-- end of weather widget -->
            </div>
          </div>
        </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           ©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://colorlib.com"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery
    <script src="../../../../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="../../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../../../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../../../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../../../framework/vendors/DateJS/../../../../framework/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../../../framework/build/js/custom.min.js"></script>




    <!-- jQuery-->
    <script src="../../../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap-->
    <script src="../../../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../../../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../../../framework/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../../../../framework/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../../../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../../../framework/vendors/DateJS/../../../../framework/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../../../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../../../framework/build/js/custom.min.js"></script>



  </body>
<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
    history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

    // Total seconds to wait
    var seconds = 1080;

    function countdown() {
        seconds = seconds - 1;
        if (seconds < 0) {
            // Chnage your redirection link here
            window.location = "https://seth.mksistemasbiomedicos.com.br";
        } else {
            // Update remaining seconds
            document.getElementById("countdown").innerHTML = seconds;
            // Count down using javascript
            window.setTimeout("countdown()", 1000);
        }
    }

    // Run countdown function
    countdown();

</script>
</html>
