<?php
include("database/database.php");
session_start();
if(!isset($_SESSION['usuario'])){
	header ("Location: ../index.php");
}
if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
}
$usuariologado=$_SESSION['usuario'];
$instituicaologado=$_SESSION['instituicao'];
$setorlogado=$_SESSION['setor'];

// Adiciona a Função display_campo($nome_campo, $tipo_campo)
//require_once "personalizacao_display.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />

	<title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>

	<!-- Bootstrap -->
	<link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- bootstrap-daterangepicker -->
	<link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

	<!-- Bootstrap -->
	<link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

	<!-- bootstrap-progressbar -->
	<link href="../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
	<!-- JQVMap -->
	<link href="../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
	<!-- bootstrap-daterangepicker -->
	<link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="../../framework/build/css/custom.min.css" rel="stylesheet">

	<!-- PNotify -->
	<link href="../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	<link href="../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	<link href="../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">

	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<!-- <script src="jquery.min.js" ></script> -->
	<!-- Datatables -->

	<link href="../../framework/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link href="../../framework/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
	<link href="../../framework/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
	<link href="../../framework/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
	<link href="../../framework/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="dashboard" class="site_title"><i class="fa fa-500px"></i> <span>SETH Patrimônio</span></a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<img src="logo/img.jpg" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Bem vindo,</span>
							<h2><?php printf($usuariologado) ?></h2>
							<p class="glyphicon glyphicon-time" id="countdown"></p>
							<span>
								<script> document.write(new Date().toLocaleDateString()); </script>
							</span>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br />

<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>Geral</h3>
							<ul class="nav side-menu">
								<li><a><i class="fa fa-home"></i> Início <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="dashboard" title="Painel de supervisão">Dashboard</a></li>
										<!--<li><a href="dashboard-rfid" title="Painel de supervisão RFID">Dashboard RFID</a></li> -->
										<!--     <li><a href="index3.php">Dashboard3</a></li>  -->
									</ul>
								</li>
								<li><a><i class="fa fa-edit"></i> Ordem de Serviço <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="os-open" title="Abrir nova ordem de serviço">Abrir OS </a></li>
										<li><a href="os-opened" title="Gerenciar ordem de serviço solicitadas">Aberto </a></li>
										<!--      <li><a href="form_validation.php">Buscar</a></li> -->
										<li><a href="os-progress" title="Gerenciar ordem de serviço em andamento">Em processo</a></li>
										<!--  <li><a href="os-concluded">Fechada</a></li> -->
										<li><a href="os-signature" title="Gerenciar ordem de serviço para assinatura eletrônica">Assinatura</a></li>
										<li><a href="os-signature-user" title="Gerenciar ordem de serviço para assinatura eletrônica do solicitante">Solicitante</a></li>
										<li><a href="os-finished" title="Gerenciar ordem de serviço concluida">Concluida</a></li>
										<li><a href="os-canceled" title="Gerenciar ordem de serviço cancelada">Canceleda</a></li>
										<li><a href="os-integration" title="Gerenciar ordem de Integração">Integração</a></li>
										<!--     <li><a href="form_upload.php">Form Upload</a></li> -->
										<!--    <li><a href="form_buttons.php">Form Buttons</a></li> -->
									</ul>
								</li>
								<li><a><i class="fa fa-desktop"></i> Cadastro &amp; Consulta <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<!--<li><a href="general_elements.php">Instituição</a></li>-->

										<li><a>Localização<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-location-unit" title="Cadastrar ou gerenciar Unidades">Unidade</a>
												</li>
												<li><a href="register-location-area" title="Cadastrar ou gerenciar Setores">Setor</a>
												</li>
												<li><a href="register-location-sector" title="Cadastrar ou gerenciar Area">Area</a>
												</li>
											</ul>
										</li>
										<li><a>Mobiliário<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-equipament-group" title="Cadastrar ou gerenciar Grupo de Mobiliário">Grupo de Mobiliário</a>
												</li>
												<li><a href="register-equipament-family" title="Cadastrar ou gerenciar Familia de Mobiliário">Familia de Mobiliário</a>
												</li>
												<li><a href="register-equipament" title="Cadastrar ou gerenciar Mobiliário">Mobiliário</a>
												</li>
												
											</ul>
										</li>

									<li><a>Engenharia Clinica<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-equipament-group-ec" title="Cadastrar ou gerenciar Grupo de Equipamento">Grupo de Equipamento</a>
												</li>
												<li><a href="register-equipament-family-ec" title="Cadastrar ou gerenciar Familia de Equipamento">Familia de Equipamento</a>
												</li>
												<li><a href="register-equipament-ec" title="Cadastrar ou gerenciar Equipamento">Equipamento</a>
												</li>
												
												
											</ul>
										</li>
<li><a>Engenharia Predial/Infraestrutura<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-equipament-group-ep" title="Cadastrar ou gerenciar Grupo de Equipamento">Grupo de Equipamento</a>
												</li>
												<li><a href="register-equipament-family-ep" title="Cadastrar ou gerenciar Familia de Equipamento">Familia de Equipamento</a>
												</li>
												<li><a href="register-equipament-ep" title="Cadastrar ou gerenciar Equipamento">Equipamento</a>
												</li>
												
												
											</ul>
										</li>
<li><a>Tecnologia da Informação<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-equipament-group-ti" title="Cadastrar ou gerenciar Grupo de Equipamento">Grupo de Equipamento</a>
												</li>
												<li><a href="register-equipament-family-ti" title="Cadastrar ou gerenciar Familia de Equipamento">Familia de Equipamento</a>
												</li>
												<li><a href="register-equipament-ti" title="Cadastrar ou gerenciar Equipamento">Equipamento</a>
												</li>
												
												
											</ul>
										</li>
<li><a>Hotelaria<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-equipament-group-ht" title="Cadastrar ou gerenciar Grupo de Equipamento">Grupo de Equipamento</a>
												</li>
												<li><a href="register-equipament-family-ht" title="Cadastrar ou gerenciar Familia de Equipamento">Familia de Equipamento</a>
												</li>
												<li><a href="register-equipament-ht" title="Cadastrar ou gerenciar Equipamento">Equipamento</a>
												</li>
												
												
											</ul>
										</li>

										<li><a href="register-user" title="Cadastrar ou gerenciar Colaborador">Colaborador</a></li>
										<li><a href="register-manufacture" title="Cadastrar ou gerenciar Fornecedor">Fornecedor</a></li>
										<li><a href="register-account" title="Cadastrar ou gerenciar Usuário">Usuário</a></li>
										<!--   <li><a href="widgets.php">Widgets</a></li> -->
										<!--   <li><a href="invoice.php">Invoice</a></li> -->
										<!--    <li><a href="inbox.php">Inbox</a></li> -->
										<li><a href="calendar" title="Cadastrar ou gerenciar Datas">Calendario</a></li>
									</ul>
								</li>
								<!--    <li><a><i class="fa fa-search-plus"></i> Consulta <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
								<li><a href="general_elements_consulta.php">Instituição</a></li>
								<li><a href="media_gallery_consulta.php">Localização</a></li>
								<li><a href="typography_consulta.php">Equipamento</a></li>
								<li><a href="icons_consulta.php">Colaborador</a></li>
								<li><a href="glyphicons_Consulta.php">Fornecedor</a></li>
								<!--   <li><a href="widgets.php">Widgets</a></li> -->
								<!--   <li><a href="invoice.php">Invoice</a></li> -->
								<!--    <li><a href="inbox.php">Inbox</a></li> -->

								<!--    </ul>
							</li> -->
							
							<li><a><i class="fa fa-bar-chart-o"></i> Indicador &amp; Relatório <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a href="report-indicator-metas" title="Gerenciamento e visualizar Metas Indicador ">Metas</a></li>
									<li><a>Indicador<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-indicator-mc" title="Gerenciamento e visualizar Indicador Manutenção Corretiva">Manutenção Corretiva</a></li>
											
											
												<li><a href="report-indicator-mc-sla" title="Gerenciamento e visualizar Indicador Prazo Atendimento de Manutenção Corretiva ">Prazo Atendimento de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-first-sla" title="Gerenciamento e visualizar Indicador Primeiro Atendimento de Manutenção Corretiva">Primeiro Atendimento de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-assessment" title="Gerenciamento e visualizar Indicador Pesquisa de Atendimento de Manutenção Corretiva">Pesquisa de Atendimento de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-prioridade" title="Gerenciamento e visualizar Indicador Prioridade de Manutenção Corretiva">Prioridade de Manutenção Corretiva </a>	</li>
											<li><a href="report-indicator-mc-service" title="Gerenciamento e visualizar Indicador Serviço de Manutenção Corretiva ">Serviço de Manutenção Corretiva  </a>	</li>
											<li><a href="report-indicator-mc-category" title="Gerenciamento e visualizar Indicador Categoria de Manutenção Corretiva ">Categoria de Manutenção Corretiva  </a>	</li>
											
											<li><a href="report-indicator" title="Gerenciamento e visualizar Indicador">Indicadores </a>	</li>
											 </ul>
									</li>
									<li><a>Relatórios<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-report-mc" title="Gerenciamento e visualizar Relatório Manutenção Corretiva">Manutenção Corretiva</a></li>
											
											
											<li><a href="report-report-equipament" title="Gerenciamento e visualizar Relatório Inventario">Inventario</a>	</li>
											
											<li><a href="report-report-hierarchy" title="Gerenciamento e visualizar Relatório Diagrama de Setor">Diagrama de Setor</a>	</li>
											<li><a href="report-report-manufacture" title="Gerenciamento e visualizar Relatório  Avaliação de Fornecedor">Avaliação de Fornecedor</a>	</li>
											
											<li><a href="report-report-purchases" title="Gerenciamento e visualizar Relatório Compras">Compras</a>	</li>
											
											
<li><a href="report-report-costs" title="Gerenciamento e visualizar Custos">Custos</a>	</li>
<li><a href="report-report-pdca-quarterly-analysis" title="Gerenciamento e visualizar Analise Trimestral Avaliação">Analise Trimestral Avaliação</a>	</li>
<li><a href="report-report-pdca-contracts" title="Gerenciamento e visualizar Contratos">Contratos</a>	</li>

<li><a href="report-report-pdca-analysis" title="Gerenciamento e visualizar Analise Trimestral O.S">Analise Trimestral O.S</a>	</li>

<li><a href="report-report-pdca-quarterly-analysis-purchases" title="Gerenciamento e visualizar Analise Trimestral Compras">Analise Trimestral Compras</a>	</li>
<li><a href="report-report-disable-equipament" title="Gerenciamento e visualizar Relatório Baixa Equipamento">Relatório Baixa Equipamento</a>	</li>
<li><a href="report-report-report-equipament" title="Gerenciamento e visualizar Relatório Parecer Tecnico de Equipamento">Relatório Parecer Tecnico de Equipamento</a>	</li>
<li><a href="report-report-assessment-os" title="Gerenciamento e visualizar Relatório de Avaliação de Ordem de Serviço">Relatório de Avaliação de Ordem de Serviço</a>	</li>
<li><a href="report-report-analysis-purchases" title="Gerenciamento e visualizar Analise Compras">Analise Compras</a>	</li>
<li><a href="report-report-analysis-os" title="Gerenciamento e visualizar Analise O.S">Analise O.S</a>	</li>
<li><a href="report-report" title="Gerenciamento e visualizar Relatório">Relatório</a>	</li>


										
										
										</ul>
									</li>
									<!--  <li><a href="morisjs.php">Moris JS</a></li> -->
									<!-- <li><a href="echarts.php">ECharts</a></li> -->
									<!-- <li><a href="other_charts.php">Other Charts</a></li> -->
								</ul>
							</li>
							<li><a><i class="fa fa-cart-arrow-down"></i> Compras <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a>Solicitação<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-acquisition" title="Abertura de Solititação com Ordem de Serviço">Solicitação com O.S</a>
											</li>
											<li><a href="purchases-acquisition-single" title="Abertura de Solititação Sem Ordem de Serviço">Solicitação Sem O.S</a>
											</li>

										</ul>
									</li>
									<li><a>Inventario<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-inventory" title="Gerenciamento e visualizar Entrada de Material">Entrada de Material</a></li>
											<li><a href="purchases-inventory-output" title="Gerenciamento e visualizar Saida de Material">Saida de Material</a></li>
											<li><a href="purchases-inventory-stock" title="Gerenciamento e visualizar Estoque de Material">Estoque de Material</a></li>
											<li><a href="purchases-inventory-manager" title="Gerenciamento e visualizar Parametrização">Gerenciar</a></li>
										</ul>
									</li>

									<li><a>Solicitação<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-progress" title="Gerenciamento de Solititação em Aberto">Solicitação Aberto</a></li>
											<li><a href="purchases-progress" title="Gerenciamento de Solititação em Aprovação">Solicitação Aprovação</a></li>
											<li><a href="purchases-progress-progress" title="Gerenciamento de Solititação em Andamento">Solicitação Andamento</a></li>
											<li><a href="purchases-progress-concluded" title="Gerenciamento de Solititação em Concluido">Solicitação Concluido</a></li>
										</ul>
									</li>

								</ul>
							</li>
							<!--  <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
							<li><a href="fixed_sidebar.php">Fixed Sidebar</a></li>
							<li><a href="fixed_footer.php">Fixed Footer</a></li>
						</ul>
					</li> -->
				</ul>
			</div>
			<div class="menu_section">
				<h3>Depreciação e Obsolescência</h3>
				<ul class="nav side-menu">
					
				<li><a><i class="fa fa-windows"></i> Depreciação <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li><a href="obsolescence-analysis" title="Elaboração de plano de Depreciação">Analise</a></li>
						<li><a href="obsolescence-report" title="Relatório de plano de Depreciação">Relatório</a></li>
					
						<!--      <li><a href="plain_page.php">Plain Page</a></li>
						<li><a href="login.php">Login Page</a></li>
						<li><a href="pricing_tables.php">Pricing Tables</a></li> -->
					</ul>
				</li>
					<li><a><i class="fa fa-windows"></i> Obsolescência <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="obsolescence-analysis-ob" title="Elaboração de plano de Obsolescência">Analise</a></li>
							<li><a href="obsolescence-report-ob" title="Relatório de plano de Obsolescência">Relatório</a></li>
							 
							<li><a href="obsolescence-prevision-provision" title="Relatório de Previsão e Provisão de Obsolescência">Previsão e Provisão </a></li>
							<!--      <li><a href="plain_page.php">Plain Page</a></li>
							<li><a href="login.php">Login Page</a></li>
							<li><a href="pricing_tables.php">Pricing Tables</a></li> -->
						</ul>
					</li>

				<!--      <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
				<li><a href="#level1_1">Level One</a>
				<li><a>Level One<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
				<li class="sub_menu"><a href="level2.php">Level Two</a>
			</li>
			<li><a href="#level2_1">Level Two</a>
		</li>
		<li><a href="#level2_2">Level Two</a>
	</li>
</ul>
</li>
<li><a href="#level1_2">Level One</a>
</li>
</ul>
</li>
<li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
</ul> -->

</div>
<div class="menu_section">
	<h3>Melhoria contínua  </h3>
	<ul class="nav side-menu">
		<li><a><i class="fa fa-cube"></i> Análise Trimestral <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="pdca-quarterly-analysis" title="Elaboração de Análise Trimestral">Análise</a></li>
				<li><a href="pdca-quarterly-report" title="Relatorio de Análise Trimestral">Relatorio</a></li>
				
				<!--     <li><a href="index3.php">Dashboard3</a></li>  -->
			</ul>
		</li>
		<li><a><i class="fa fa-database"></i> Contratos <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="pdca-contracts" title="Cadastro de Contratos">Contratos</a></li>
				<li><a href="pdca-contracts-configure" title="Gerenciamento de Contratos">Configuracoes</a></li>
				
				<!--     <li><a href="index3.php">Dashboard3</a></li>  -->
			</ul>
		</li>
	 
		
	</ul>
</div>

<div class="menu_section">
	<!-- <h3>Rastreabilidade & Lean </h3> -->

	<ul class="nav side-menu">
		<!--<li><a><i class="fa fa-tags"></i> RFID <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="rfid-tags" title="Cadastro e Gerenciamento de Tags">Tags </a></li>
				<li><a href="rfid-logs" title="Cadastro e Gerenciamento de Logs">Logs </a></li>
				<li><a href="rfid-point" title="Cadastro e Gerenciamento de Point">Point </a></li>

				<!--  <li><a href="insumos.php">Insumos </a></li>
				<li><a href="composicao.php">Composicao </a></li>
				<li><a href="fornecedor.php">Fornecedor </a></li>
				<!--   <li><a href="insumos.php">insumos </a></li>
				<!--     <li><a href="form_upload.php">Form Upload</a></li> -->
				<!--    <li><a href="form_buttons.php">Form Buttons</a></li>

			</ul>
		</li>-->

	

		<!-- <li><a><i class="fa fa-heart"></i> Lean Healthcare <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="lean-define" title="Cadastro e Gerenciamento de Lean Healthcare Definir">Definir</a></li>
				<li><a href="lean-measure" title="Cadastro e Gerenciamento de Lean Healthcare Medir">Medir</a></li>
				<li><a href="lean-analyse" title="Cadastro e Gerenciamento de Lean Healthcare Analisar">Analisar </a></li>
				<li><a href="lean-improve" title="Cadastro e Gerenciamento de Lean Healthcare Melhorar">Melhorar </a></li>
				<li><a href="lean-control" title="Cadastro e Gerenciamento de Lean Healthcare Controlar">Controlar </a></li>

				
			</ul>
		</li> -->
		<!-- <li><a  href="artificial-intelligence"title="Gerenciamento de Inteligência artificial"><i class="fa fa-android"></i> I.A <span class=""></span></a> </li> -->


	</div>
	<!--    <div class="menu_section">
	<h3>Manuais &amp; ADMINISTRATIVO</h3>
	<ul class="nav side-menu">
	<li><a href="../contratosphp"target="_blank"><i class="fa fa-pencil-square-o"></i> Manuais Usuário </span></a>
	<!--    <ul class="nav child_menu">
	<li><a href="e_commerce.php">Novo</a></li>
	<li><a href="projects.php">Contratos</a></li>
	<li><a href="project_detail.php">Gestao</a></li>
	<li><a href="contacts.php">Contacts</a></li>
	<li><a href="profile.php">Profile</a></li>
</ul> -->
<!--       </li>
<li><a  href="../sigcon"target="_blank"><i class="fa fa-check-square-o"></i> Manuais Tecnico </span></a>
<!--    <ul class="nav child_menu">
<li><a href="e_commerce.php">novo</a></li>
<li><a href="projects.php">Formularios</a></li>
<!--    <li><a href="contacts.php">Contacts</a></li>
<li><a href="profile.php">Profile</a></li>
</ul> -->
<!--        </li>
<li><a href="../geradoc"target="_blank"><i class="fa fa-dropbox"></i> Dropzone </span></a>
<!--    <ul class="nav child_menu">
<li><a href="e_commerce.php">Arquivos</a></li>
<li><a href="projects.php">Documentos</a></li>
<li><a href="contacts.php">Contacts</a></li>
<li><a href="profile.php">Profile</a></li>
</ul>-->
<!--      </li>
<li><a href="../rfid"target="_blank"><i class="fa fa-tags"></i> Rastreabilidade </span></a>
<!--    <ul class="nav child_menu">
<li><a href="e_commerce.php">Arquivos</a></li>
<li><a href="projects.php">Documentos</a></li>
<li><a href="contacts.php">Contacts</a></li>
<li><a href="profile.php">Profile</a></li>
</ul>
</li>

</div> -->

</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
	<a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
		<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

		<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
		<script>
		var goFS = document.getElementById("goFS");
		goFS.addEventListener("click", function() {
			document.body.requestFullscreen();
		}, false);
		</script>

	</a>
	<a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="lockscreen">
		<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
		<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
	</a>
	<!--     <a data-toggle="tooltip" data-placement="top" title="Contrato" href="http://contrato.mksistemasbiomedicos.com.br/"target="_blank">
	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
</a>
<a data-toggle="tooltip" data-placement="top" title="Formulario" href="http://licitacao.mksistemasbiomedicos.com.br/"target="_blank">
<span class="glyphicon glyphicon-check" aria-hidden="true"></span>
</a>
<a data-toggle="tooltip" data-placement="top" title="Arquivo" href="http://file.mksistemasbiomedicos.com.br/"target="_blank">
<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
</a> -->

<!--       <a data-toggle="tooltip" data-placement="top" title="Projetos" href="http://www.project.mksistemasbiomedicos.com.br/"target="_blank">
<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
</a> -->

</div>
<!-- /menu footer buttons -->
</div>
</div>
<!-- top navigation -->
<div class="top_nav">
	<div class="nav_menu">
		<div class="nav toggle">
			<a id="menu_toggle"><i class="fa fa-bars"></i></a>
		</div>
		<nav class="nav navbar-nav">
			<ul class=" navbar-right">
				<li class="nav-item dropdown open" style="padding-left: 15px;">
					<a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
						<img src="logo/img.jpg" alt=""><?php printf($usuariologado) ?>
					</a>
					<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
						<a class="dropdown-item"  href="profile">   <i class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></i> Perfil </a>
                        <a class="dropdown-item"  href="documentation-bussines">   <i class="glyphicon glyphicon-file pull-right" aria-hidden="true"></i> Documentos </a>
							   <a class="dropdown-item"  href="documentation-event-task">
						<span class="badge bg-red pull-right"><?php include 'api/api-event-task.php';?></span>
						<span>Eventos e Tarefas</span>
					</a>
				    <a class="dropdown-item"  href="documentation-widget"> <i class="fa fa-desktop pull-right" aria-hidden="true"></i>Widget</a>
					
						<a class="dropdown-item"  href="documentation">   <i class="fa fa-code pull-right" aria-hidden="true"></i> Sistema </a>
						<a class="dropdown-item"  href="documentation-pop">   <i class="fa fa-book pull-right" aria-hidden="true"></i> POP </a>
						<a class="dropdown-item"  href="documentation-update">   <i class="fa fa-history pull-right" aria-hidden="true"></i> Atualização </a>
						
						<a class="dropdown-item"  href="documentation-label">   <i class="fa fa-fax pull-right" aria-hidden="true"></i> Rotuladora </a>
						
						<a class="dropdown-item"  href="documentation-bell">   <i class="fa fa-bell pull-right" aria-hidden="true"></i> Central de Notificação </a>
						

						<!--    <a class="dropdown-item"  href="javascript:;">
						<span class="badge bg-red pull-right">100%</span>
						<span>Settings</span>
					</a>
					<!--      <a class="dropdown-item"  href="javascript:;">Help</a> -->
	 <a class="dropdown-item"  href="login"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>
				 


                <a href="documentation-bell"> 
				<li role="presentation" class="nav-item dropdown open">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="dropdown-toggle info-number"  data-toggle="dropdown" aria-expanded="false" title="Novas Notificações">
						<i class="fa fa-bell-o"></i>
						<span class="badge badge-dark"><?php include 'api/api-bell-notification.php';?></span>
					</a>
				</li>
			</a>
				 
				<a href="os-integration">
                 <li role="presentation" class="nav-item dropdown open">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a  class="dropdown-toggle info-number"  data-toggle="dropdown" aria-expanded="false" title="Novas Ordem de Serviço de Integração">
							<i class="fa fa-code-fork"></i>
							<span class="badge bg-blue"> <?php include 'api/api-os-integration.php';?> </span>
						</a>
				</li>
				</a> 				

				 
</a> 	
				<a href="os-opened">
				<li role="presentation" class="nav-item dropdown open">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a  class="dropdown-toggle info-number"  data-toggle="dropdown" aria-expanded="false" title="Novas Ordem de Serviços">
						<i class="fa fa-envelope-o"></i>
						<span class="badge bg-green"> <?php include 'workflow/envelope.php';?> </span>
					</a>

				</li>
			</a> 	 
				<a href="os-signature">
				<li role="presentation" class="nav-item dropdown open">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a  class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false" title="Novas Assinaturas de Ordem de Serviços">
						<i class="fa fa-envelope-o"></i>
					<span class="badge bg-red"> <?php include 'workflow/envelope-signature.php';?> </span>
					</a>


				</li>
				</a> 



				<!--
				<ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
				<li class="nav-item">
				<a class="dropdown-item">
				<span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
				<span>
				<span>John Smith</span>
				<span class="time">3 mins ago</span>
			</span>
			<span class="message">
			Film festivals used to be do-or-die moments for movie makers. They were WHERE...
		</span>
	</a>
</li>
<li class="nav-item">
<a class="dropdown-item">
<span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
<span>
<span>John Smith</span>
<span class="time">3 mins ago</span>
</span>
<span class="message">
Film festivals used to be do-or-die moments for movie makers. They were WHERE...
</span>
</a>
</li>
<li class="nav-item">
<a class="dropdown-item">
<span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
<span>
<span>John Smith</span>
<span class="time">3 mins ago</span>
</span>
<span class="message">
Film festivals used to be do-or-die moments for movie makers. They were WHERE...
</span>
</a>
</li>
<li class="nav-item">
<a class="dropdown-item">
<span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
<span>
<span>John Smith</span>
<span class="time">3 mins ago</span>
</span>
<span class="message">
Film festivals used to be do-or-die moments for movie makers. They were WHERE...
</span>
</a>
</li>
<li class="nav-item">
<div class="text-center">
<a class="dropdown-item">
<strong>See All Alerts</strong>
<i class="fa fa-angle-right"></i>
</a>
</div>
</li>
</ul>
</li> -->
</ul>
</li>
</ul>
</nav>
</div>
</div>

<!-- /top navigation -->
<!-- page content -->
<?php include 'api.php';?>
<?php include 'frontend/purchases-acquisition-wizard-single-frontend.php';?>

<!-- /page content -->

<!-- footer content -->
<footer>
	<div class="pull-right">
		©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://mksistemasbiomedicos.com.br"></a>
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery
<script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../../framework/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- jQuery Sparklines -->
<script src="../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- Flot -->
<script src="../../framework/vendors/Flot/jquery.flot.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../../framework/vendors/DateJS/build/date.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../framework/vendors/moment/min/moment.min.js"></script>
<script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../framework/build/js/custom.min.js"></script>




<!-- jQuery-->
<script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap
<script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../../framework/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../../framework/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../../framework/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="../../framework/vendors/Flot/jquery.flot.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
<script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="../../framework/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../framework/vendors/moment/min/moment.min.js"></script>
<script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="../../framework/build/js/custom.min.js"></script>
<!-- PNotify -->
<script src="../../framework/vendors/pnotify/dist/pnotify.js"></script>
<script src="../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<script src="../../framework/assets/js/select2.min.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="../../framework/vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Switchery -->
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="../../framework/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="../../framework/vendors/autosize/dist/autosize.min.js"></script>
<!-- jQuery autocomplete -->
<script src="../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<!-- starrr -->
<script src="../../framework/vendors/starrr/dist/starrr.js"></script>
<!-- Contador de caracter -->
<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
<!-- Datatables -->
<script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
<script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- jQuery Knob -->
<script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../framework/vendors/moment/min/moment.min.js"></script>
<script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>


</body>
<script language="JavaScript">
<!--
function verfonte()
{
	if (event.button==2)
	{
		window.alert('Este recurso foi desativado')
	}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
	history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

// Total seconds to wait
var seconds = 1080;

function countdown() {
	seconds = seconds - 1;
	if (seconds < 0) {
		// Chnage your redirection link here
		window.location = "https://seth.mksistemasbiomedicos.com.br";
	} else {
		// Update remaining seconds
		document.getElementById("countdown").innerHTML = seconds;
		// Count down using javascript
		window.setTimeout("countdown()", 1000);
	}
}

// Run countdown function
countdown();

</script>
</html>
