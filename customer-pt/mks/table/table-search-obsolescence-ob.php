<?php
include("../database/database.php");

$query = "
   SELECT pt.nome AS 'patrimonio',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN instituicao.instituicao
         WHEN obsolescence.id_pt = 2 THEN instituicao.instituicao
         WHEN obsolescence.id_pt = 3 THEN instituicao.instituicao
         WHEN obsolescence.id_pt = 4 THEN instituicao.instituicao
         WHEN obsolescence.id_pt = 5 THEN instituicao.instituicao
      END AS 'instituicao',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN instituicao_area.nome
         WHEN obsolescence.id_pt = 2 THEN instituicao_area.nome
         WHEN obsolescence.id_pt = 3 THEN instituicao_area.nome
         WHEN obsolescence.id_pt = 4 THEN instituicao_area.nome
         WHEN obsolescence.id_pt = 5 THEN instituicao_area.nome
      END AS 'setor',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN instituicao_localizacao.nome
         WHEN obsolescence.id_pt = 2 THEN instituicao_localizacao.nome
         WHEN obsolescence.id_pt = 3 THEN instituicao_localizacao.nome
         WHEN obsolescence.id_pt = 4 THEN instituicao_localizacao.nome
         WHEN obsolescence.id_pt = 5 THEN instituicao_localizacao.nome
      END AS 'area',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN equipamento.codigo
         WHEN obsolescence.id_pt = 2 THEN equipamento_ec.codigo
         WHEN obsolescence.id_pt = 3 THEN equipamento_eb.codigo
         WHEN obsolescence.id_pt = 4 THEN equipamento_ti.codigo
         WHEN obsolescence.id_pt = 5 THEN equipamento_ht.codigo
      END AS 'codigo',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN equipamento.serie
         WHEN obsolescence.id_pt = 2 THEN equipamento_ec.serie
         WHEN obsolescence.id_pt = 3 THEN equipamento_eb.serie
         WHEN obsolescence.id_pt = 4 THEN equipamento_ti.serie
         WHEN obsolescence.id_pt = 5 THEN equipamento_ht.serie
      END AS 'serie',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN equipamento.id
         WHEN obsolescence.id_pt = 2 THEN equipamento_ec.id
         WHEN obsolescence.id_pt = 3 THEN equipamento_eb.id
         WHEN obsolescence.id_pt = 4 THEN equipamento_ti.id
         WHEN obsolescence.id_pt = 5 THEN equipamento_ht.id
      END AS 'equipamento_id',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN equipamento_familia.nome
         WHEN obsolescence.id_pt = 2 THEN equipamento_familia_ec.nome
         WHEN obsolescence.id_pt = 3 THEN equipamento_familia_eb.nome
         WHEN obsolescence.id_pt = 4 THEN equipamento_familia_ti.nome
         WHEN obsolescence.id_pt = 5 THEN equipamento_familia_ht.nome
      END AS 'equipamento',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN equipamento_familia.fabricante
         WHEN obsolescence.id_pt = 2 THEN equipamento_familia_ec.fabricante
         WHEN obsolescence.id_pt = 3 THEN equipamento_familia_eb.fabricante
         WHEN obsolescence.id_pt = 4 THEN equipamento_familia_ti.fabricante
         WHEN obsolescence.id_pt = 5 THEN equipamento_familia_ht.fabricante
      END AS 'fabricante',
      CASE 
         WHEN obsolescence.id_pt = 1 THEN equipamento_familia.modelo
         WHEN obsolescence.id_pt = 2 THEN equipamento_familia_ec.modelo
         WHEN obsolescence.id_pt = 3 THEN equipamento_familia_eb.modelo
         WHEN obsolescence.id_pt = 4 THEN equipamento_familia_ti.modelo
         WHEN obsolescence.id_pt = 5 THEN equipamento_familia_ht.modelo
      END AS 'modelo',
      obsolescence_year.yr,
      obsolescence_rooi.rooi,
      obsolescence.id, 
      obsolescence.id_equipamento, 
      obsolescence.id_year, 
      obsolescence.id_c1, 
      obsolescence.id_c2, 
      obsolescence.id_c3, 
      obsolescence.id_c4, 
      obsolescence.id_c5, 
      obsolescence.id_c6, 
      obsolescence.id_c7, 
      obsolescence.id_rooi, 
      obsolescence.score, 
      obsolescence.obs, 
      obsolescence.vlr_market, 
      obsolescence.vlr_purchases, 
      obsolescence.obs_purchases, 
      obsolescence.upgrade, 
      obsolescence.reg_date
FROM obsolescence
LEFT JOIN obsolescence_rooi ON obsolescence_rooi.id = obsolescence.id_rooi
LEFT JOIN obsolescence_year ON obsolescence_year.id = obsolescence.id_year
LEFT JOIN pt ON pt.id = obsolescence.id_pt
LEFT JOIN equipamento ON obsolescence.id_pt = 1 AND equipamento.id = obsolescence.id_equipamento
LEFT JOIN equipamento_ec ON obsolescence.id_pt = 2 AND equipamento_ec.id = obsolescence.id_equipamento
LEFT JOIN equipamento_eb ON obsolescence.id_pt = 3 AND equipamento_eb.id = obsolescence.id_equipamento
LEFT JOIN equipamento_ti ON obsolescence.id_pt = 4 AND equipamento_ti.id = obsolescence.id_equipamento
LEFT JOIN equipamento_ht ON obsolescence.id_pt = 5 AND equipamento_ht.id = obsolescence.id_equipamento
LEFT JOIN equipamento_familia ON obsolescence.id_pt = 1 AND equipamento_familia.id = equipamento.id_equipamento_familia
LEFT JOIN equipamento_familia_ec ON obsolescence.id_pt = 2 AND equipamento_familia_ec.id = equipamento_ec.id_equipamento_familia
LEFT JOIN equipamento_familia_eb ON obsolescence.id_pt = 3 AND equipamento_familia_eb.id = equipamento_eb.id_equipamento_familia
LEFT JOIN equipamento_familia_ti ON obsolescence.id_pt = 4 AND equipamento_familia_ti.id = equipamento_ti.id_equipamento_familia
LEFT JOIN equipamento_familia_ht ON obsolescence.id_pt = 5 AND equipamento_familia_ht.id = equipamento_ht.id_equipamento_familia
LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = 
   CASE 
      WHEN obsolescence.id_pt = 1 THEN equipamento.id_instituicao_localizacao
      WHEN obsolescence.id_pt = 2 THEN equipamento_ec.id_instituicao_localizacao
      WHEN obsolescence.id_pt = 3 THEN equipamento_eb.id_instituicao_localizacao
      WHEN obsolescence.id_pt = 4 THEN equipamento_ti.id_instituicao_localizacao
      WHEN obsolescence.id_pt = 5 THEN equipamento_ht.id_instituicao_localizacao
   END
LEFT JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE obsolescence.id_type = 2
ORDER BY obsolescence.id, obsolescence_year.yr, equipamento_familia.nome DESC;

";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
