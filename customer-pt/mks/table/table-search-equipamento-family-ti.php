<?php
include("../database/database.php");

$query = "
SELECT 
    fornecedor.empresa,
    anvisa_class.nome,
    equipamento_familia_ti.img,
    equipamento_familia_ti.manual_use,
    equipamento_familia_ti.manual_tec,
    equipamento_grupo_ti.nome AS 'grupo', 
    equipamento_familia_ti.id, 
    equipamento_familia_ti.id_equipamento_grupo, 
    equipamento_familia_ti.nome AS 'familia', 
    equipamento_familia_ti.fabricante, 
    equipamento_familia_ti.modelo, 
    equipamento_familia_ti.anvisa, 
    equipamento_familia_ti.img, 
    equipamento_familia_ti.upgrade, 
    equipamento_familia_ti.reg_date 
FROM 
    equipamento_familia_ti 
INNER JOIN 
    equipamento_grupo_ti ON equipamento_grupo_ti.id = equipamento_familia_ti.id_equipamento_grupo 
LEFT JOIN 
    anvisa_class ON anvisa_class.id = equipamento_familia_ti.id_class 
LEFT JOIN 
    fornecedor ON fornecedor.id = equipamento_familia_ti.id_fornecedor 
WHERE 
    equipamento_familia_ti.trash = 1 
ORDER BY 
    equipamento_familia_ti.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

print json_encode($rows);
?>
