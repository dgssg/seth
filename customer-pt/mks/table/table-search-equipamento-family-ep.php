<?php
include("../database/database.php");

$query = "
SELECT 
    fornecedor.empresa,
    anvisa_class.nome,
    equipamento_familia_eb.img,
    equipamento_familia_eb.manual_use,
    equipamento_familia_eb.manual_tec,
    equipamento_grupo_eb.nome AS 'grupo', 
    equipamento_familia_eb.id, 
    equipamento_familia_eb.id_equipamento_grupo, 
    equipamento_familia_eb.nome AS 'familia', 
    equipamento_familia_eb.fabricante, 
    equipamento_familia_eb.modelo, 
    equipamento_familia_eb.anvisa, 
    equipamento_familia_eb.img, 
    equipamento_familia_eb.upgrade, 
    equipamento_familia_eb.reg_date 
FROM 
    equipamento_familia_eb 
INNER JOIN 
    equipamento_grupo_eb ON equipamento_grupo_eb.id = equipamento_familia_eb.id_equipamento_grupo 
LEFT JOIN 
    anvisa_class ON anvisa_class.id = equipamento_familia_eb.id_class 
LEFT JOIN 
    fornecedor ON fornecedor.id = equipamento_familia_eb.id_fornecedor 
WHERE 
    equipamento_familia_eb.trash = 1 
ORDER BY 
    equipamento_familia_eb.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

print json_encode($rows);
?>
