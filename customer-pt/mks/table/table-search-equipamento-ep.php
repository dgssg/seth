<?php
include("../database/database.php");

// Preparar a consulta
$query = "
SELECT 
    equipament_compliance.id AS 'conformidade', 
    os.id AS 'saida_1',
    equipament_exit.id AS 'saida_2',
    parecer_equipamento.id AS 'parecer_1',
    equipament_report.id AS 'parecer_2',
    equipament_training.id_equipamento AS 'treinamento', 
    equipamento_grupo_eb.nome AS 'grupo',
    equipamento_eb.patrimonio,
    equipamento_familia_eb.anvisa, 
    equipament_check.id AS 'checklist', 
    technological_surveillance.id AS 'technological',
    alert.id as 'alert', 
    equipamento_eb.nf_dropzone,
    equipamento_eb.serie,
    equipamento_eb.comodato,
    equipamento_eb.baixa,
    equipamento_eb.ativo,
    instituicao.instituicao, 
    instituicao_area.nome AS 'area',
    instituicao_localizacao.nome AS 'setor', 
    equipamento_eb.id, 
    equipamento_familia_eb.nome AS 'equipamento', 
    equipamento_familia_eb.modelo,
    equipamento_familia_eb.fabricante, 
    equipamento_eb.codigo 
FROM 
    equipamento_eb 
INNER JOIN 
    equipamento_familia_eb ON equipamento_familia_eb.id = equipamento_eb.id_equipamento_familia  
INNER JOIN 
    instituicao_localizacao on instituicao_localizacao.id = equipamento_eb.id_instituicao_localizacao 
INNER JOIN 
    instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  
INNER JOIN 
    instituicao ON instituicao.id = instituicao_area.id_unidade 
LEFT JOIN 
    alert on alert.equipamento_familia = equipamento_eb.id_equipamento_familia 
LEFT JOIN 
    os on os.id_equipamento = equipamento_eb.id 
LEFT JOIN 
    maintenance_routine on maintenance_routine.id_equipamento = equipamento_eb.id 
LEFT JOIN 
    technological_surveillance on os.id = technological_surveillance.id_os and technological_surveillance.mp = 0  
LEFT JOIN 
    equipament_check ON equipament_check.id_equipamento = equipamento_eb.id 
LEFT JOIN 
    equipamento_grupo_eb ON equipamento_grupo_eb.id = equipamento_familia_eb.id_equipamento_grupo 
LEFT JOIN 
    equipament_training ON equipament_training.id_equipamento =  equipamento_eb.id 
LEFT JOIN 
    equipament_report ON equipament_report.id_equipamento = equipamento_eb.id 
LEFT JOIN 
    parecer_equipamento ON parecer_equipamento.id_equipamento = equipamento_eb.id 
LEFT JOIN 
    equipament_exit ON equipament_exit.id_equipamento = equipamento_eb.id OR os.id =  equipament_exit.id_os 
LEFT JOIN  
    equipament_compliance ON equipament_compliance.id_os = os.id 
WHERE 
    equipamento_eb.trash = 1 
GROUP BY 
    equipamento_eb.id 
ORDER BY 
    equipamento_eb.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);

if (!$resultados) {
    die("Erro na consulta ao banco de dados: " . $conn->error);
}

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

// Configurar o cabeçalho para JSON
header('Content-Type: application/json');
echo json_encode($rows);

$conn->close();
?>
