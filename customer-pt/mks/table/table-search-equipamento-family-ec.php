<?php
include("../database/database.php");

$query = "
SELECT 
    fornecedor.empresa,
    anvisa_class.nome,
    equipamento_familia_ec.img,
    equipamento_familia_ec.manual_use,
    equipamento_familia_ec.manual_tec,
    equipamento_grupo_ec.nome AS 'grupo', 
    equipamento_familia_ec.id, 
    equipamento_familia_ec.id_equipamento_grupo, 
    equipamento_familia_ec.nome AS 'familia', 
    equipamento_familia_ec.fabricante, 
    equipamento_familia_ec.modelo, 
    equipamento_familia_ec.anvisa, 
    equipamento_familia_ec.img, 
    equipamento_familia_ec.upgrade, 
    equipamento_familia_ec.reg_date 
FROM 
    equipamento_familia_ec 
INNER JOIN 
    equipamento_grupo_ec ON equipamento_grupo_ec.id = equipamento_familia_ec.id_equipamento_grupo 
LEFT JOIN 
    anvisa_class ON anvisa_class.id = equipamento_familia_ec.id_class 
LEFT JOIN 
    fornecedor ON fornecedor.id = equipamento_familia_ec.id_fornecedor 
WHERE 
    equipamento_familia_ec.trash = 1 
ORDER BY 
    equipamento_familia_ec.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

print json_encode($rows);
?>
