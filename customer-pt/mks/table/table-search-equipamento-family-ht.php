<?php
include("../database/database.php");

$query = "
SELECT 
    fornecedor.empresa,
    anvisa_class.nome,
    equipamento_familia_ht.img,
    equipamento_familia_ht.manual_use,
    equipamento_familia_ht.manual_tec,
    equipamento_grupo_ht.nome AS 'grupo', 
    equipamento_familia_ht.id, 
    equipamento_familia_ht.id_equipamento_grupo, 
    equipamento_familia_ht.nome AS 'familia', 
    equipamento_familia_ht.fabricante, 
    equipamento_familia_ht.modelo, 
    equipamento_familia_ht.anvisa, 
    equipamento_familia_ht.img, 
    equipamento_familia_ht.upgrade, 
    equipamento_familia_ht.reg_date 
FROM 
    equipamento_familia_ht 
INNER JOIN 
    equipamento_grupo_ht ON equipamento_grupo_ht.id = equipamento_familia_ht.id_equipamento_grupo 
LEFT JOIN 
    anvisa_class ON anvisa_class.id = equipamento_familia_ht.id_class 
LEFT JOIN 
    fornecedor ON fornecedor.id = equipamento_familia_ht.id_fornecedor 
WHERE 
    equipamento_familia_ht.trash = 1 
ORDER BY 
    equipamento_familia_ht.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);

$rows = array();
while ($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}

print json_encode($rows);
?>
