<?php
class Push {
    private $host  = 'localhost';
    private $user  = 'cvheal47_root';
    private $password   = "cvheal47_root";
    private $database  = "cvheal47_pt_mks";
    private $notifTable = 'notif';
    private $userTable = 'notif_user';
    private $dbConnect = false;
    
    public function __construct(){
        if(!$this->dbConnect){ 
            $conn = new mysqli($this->host, $this->user, $this->password, $this->database);
            if($conn->connect_error){
                die("Error failed to connect to MySQL: " . $conn->connect_error);
            }else{
                $this->dbConnect = $conn;
            }
        }
    }
    
    private function getData($sqlQuery) {
        $result = mysqli_query($this->dbConnect, $sqlQuery);
        if(!$result){
            die('Error in query: '. mysqli_error());
        }
        $data= array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[]=$row;            
        }
        return $data;
    }
    
    public function listNotificationUser($user){
        $sqlQuery = "SELECT * FROM ".$this->notifTable." WHERE username='$user' AND notif_loop > 0 AND notif_time <= CURRENT_TIMESTAMP()";
        return  $this->getData($sqlQuery);
    }   
    
    public function updateNotification($id, $nextTime) {       
        $sqlUpdate = "UPDATE ".$this->notifTable." SET notif_time = '$nextTime', publish_date=CURRENT_TIMESTAMP(), notif_loop = notif_loop-1 WHERE id='$id')";
        mysqli_query($this->dbConnect, $sqlUpdate);
    }   
}
?>
