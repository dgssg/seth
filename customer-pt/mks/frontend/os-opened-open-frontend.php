<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT os.id_type,os.os_open,os.id_servico,os.id_category,os.restrictions,equipamento.data_val, os.standing,os.time_backlog,os.id_fornecedor,os.id_prioridade,os.id_observacao, os.id_chamado,os.id_tecnico,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia WHERE os.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id_type,$os_open,$id_service,$id_category,$restrictions,$data_val,$standing,$time_backlog,$id_fornecedor,$id_prioridade,$id_observacao,$id_chamado,$id_tecnico,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);



  while ($stmt->fetch()) {

  }
}

$query = "SELECT service_os,prioridade_os,category_os FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($service_os,$prioridade_os,$category_os);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }

date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
if($data_val >= $today){

  echo '<script>
    Swal.fire("Equipamento em Garantia!");
</script>';
}
?>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Ordem de Serviço</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Ação</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <a class="btn btn-app"  href="os-opened">
            <i class="glyphicon glyphicon-arrow-left"></i> Voltar
          </a>

          <a class="btn btn-app"  href="backend/os-opened-open-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
            title: 'Abertura',
            text: 'Ordem Recebida!',
            type: 'success',
            styling: 'bootstrap3'
          });">
          <i class="glyphicon glyphicon-floppy-saved"></i> Abrir
        </a>
        <a class="btn btn-app"  href="backend/os-opened-open-close-backend.php?os=<?php printf($id_os); ?>"onclick="new PNotify({
          title: 'Fechamento',
          text: 'Fechamento de O.S!',
          type: 'success',
          styling: 'bootstrap3'
        });">
        <i class="glyphicon glyphicon-floppy-remove"></i> Fechar
      </a>
      <!--   <a class="btn btn-app"  href=""onclick="new PNotify({
      title: 'Atualização',
      text: 'Atualização de Abertura de O.S!',
      type: 'danger',
      styling: 'bootstrap3'
    });">
    <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
  </a> -->
  <a class="btn btn-app"  href="os-opened-close?os=<?php printf($id_os); ?>" onclick="new PNotify({
    title: 'Cancelamento',
    text: 'Cancelamento de Abertura de O.S!',
    type: 'error',
    styling: 'bootstrap3'
  });">
  <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
</a>
<a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
  title: 'Visualizar',
  text: 'Visualizar O.S!',
  type: 'info',
  styling: 'bootstrap3'
});" >
<i class="fa fa-file-pdf-o"></i> Visualizar
</a>

</div>
</div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Descrição <small>da Solicitação</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a class="dropdown-item" href="#">Settings 1</a>
                  </li>
                  <li><a class="dropdown-item" href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />


            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="os">OS <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="os" name="os" value="<?php printf($id_os); ?>" readonly="readonly" required="required" class="form-control ">
              </div>
            </div>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="solicitante">Solicitante<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="solicitante" name="solicitante" required="required" class="form-control"  value="<?php printf($id_usuario_nome); ?> <?php printf($id_usuario_sobrenome); ?>" readonly="readonly">
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="setor" class="form-control" type="text" name="setor"  value="<?php printf($id_area); ?> <?php printf($id_localizacao); ?>" readonly="readonly">
              </div>
            </div>


            <div class="ln_solid"></div>

            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento">Equipamento <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="equipamento" name="equipamento" value="<?php printf($equipamento_nome); ?>" readonly="readonly" required="required" class="form-control ">
              </div>
            </div>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="modelo">Modelo<span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="modelo" name="modelo" required="required" class="form-control"  value="<?php printf($equipamento_modelo); ?> " readonly="readonly">
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($equipamento_codigo); ?> " readonly="readonly">
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Numero de Serie</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="numeroserie" class="form-control" type="text" name="numeroserie"  value="<?php printf($equipamento_numeroserie); ?> " readonly="readonly">
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Patrimonio</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="patromonio" class="form-control" type="text" name="patromonio"  value="<?php printf($equipamento_patrimonio); ?> " readonly="readonly">
              </div>
            </div>

            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
              <div class="col-md-6 col-sm-6 ">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Atualizar</button>
              </div>
            </div>
            <!-- configuração atualziar -->
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Atualização de Equipamento</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">

                    <!-- select equipamento -->
                    <form  action="backend/os-opened-open-upgrade-backend.php?os=<?php printf($codigoget);?>" method="post">

                      <div class="col-md-7 col-sm-7  form-group has-feedback">
                        <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
                          <?php



                          $sql = "SELECT  equipamento.id, equipamento_familia.nome,equipamento_familia.modelo,equipamento.codigo FROM equipamento LEFT join equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia  ";


                          if ($stmt = $conn->prepare($sql)) {
                            $stmt->execute();
                            $stmt->bind_result($id,$equipamento,$modelo,$codigo);
                            while ($stmt->fetch()) {
                              ?>
                              <option value="<?php printf($id);?>	"><?php printf($equipamento);?>-<?php printf($modelo);?>-<?php printf($codigo);?>	</option>
                              <?php
                              // tira o resultado da busca da memória
                            }

                          }
                          $stmt->close();

                          ?>
                        </select>
                        <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>


                      </div>

                      <script>
                      $(document).ready(function() {
                        $('#equipamento').select2();
                      });
                      </script>

                      <div class="col-md-7 col-sm-7  form-group has-feedback">
                        <textarea  type="text" class="form-control has-feedback-left" id="atualizacao" name="atualizacao" placeholder="atualizacao" data-ls-module="charCounter" maxlength="1000" data-parsley-maxlength="1000" data-parsley-trigger="keyup" data-parsley-maxlength="1000" data-parsley-minlength-message="Vamos! Você precisa inserir um comentário de pelo menos 20 caracteres."
                        data-parsley-validation-threshold="20"> </textarea>
                        <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" class="docs-tooltip" data-toggle="tooltip" title="Descrição da Atualizacao "></span>
                      </div>


                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <input type="submit" class="btn btn-primary" onclick="new PNotify({
                          title: 'Registrado',
                          text: 'Informações registrada!',
                          type: 'success',
                          styling: 'bootstrap3'
                        });" />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>


            <!-- Configuração atualizar -->
            <div class="ln_solid"></div>
             

            <label for="message">Solicitação:</label>
            <textarea id="message" required="required" class="form-control" name="id_solicitacao" data-parsley-trigger="keyup"
            data-parsley-validation-threshold="10" placeholder="<?php printf($id_solicitacao); ?>"readonly="readonly"></textarea>

            <div class="ln_solid"></div>
             
            <label for="message">Restrição:</label>
            <textarea id="message" required="required" class="form-control" name="restrictions" data-parsley-trigger="keyup"
            data-parsley-validation-threshold="10" placeholder="<?php printf($restrictions); ?>"readonly="readonly"></textarea>

            <div class="ln_solid"></div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da Solicitação</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="reg_date" class="form-control" type="text" name="reg_date"  value="<?php printf($reg_date); ?> " readonly="readonly">
              </div>
            </div>

            <?php				   $sql = "SELECT  id, nome FROM os_control WHERE id like '$os_open'  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$defeito_query);
     while ($stmt->fetch()) {
         $defeito_query=$defeito_query;
     }
}
     ?>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="tecnico">Ordem de Serviço Aberta via <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="tecnico" name="tecnico" value="<?php printf($defeito_query); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="tecnico">Tipo de Ordem de Serviço <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text"  value="<?php if($id_type == 1){ printf("Predial");} ?><?php if($id_type == 2){ printf("Infraestrutura");} ?>" readonly="readonly" required="required" class="form-control ">
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>


    <!-- page content -->



    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Anexo <small> </small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings 1</a>
                  <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">
              <div class="col-md-55">
                <?php if($id_anexo ==! "" ){ ?>
                  <div class="thumbnail">
                    <div class="image view view-first">
                      <img style="width: 100%; display: block;" src="dropzone/os/<?php printf($id_anexo); ?>" alt="image" />
                      <div class="mask">
                        <p>Anexo</p>
                        <div class="tools tools-bottom">
                          <a href="dropzone/os/<?php printf($id_anexo); ?>" download><i class="fa fa-download"></i></a>

                        </div>
                      </div>
                    </div>
                    <div class="caption">
                      <p>Anexo ordem de serviço</p>
                    </div>
                  </div>
                <?php }; ?>
              </div>







            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="row">
      <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Abertura <small>da Solicitação</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a class="dropdown-item" href="#">Settings 1</a>
                  </li>
                  <li><a class="dropdown-item" href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form action="backend/os-opened-open-info-backend.php?os=<?php printf($codigoget);?>" method="post">

              <div class="ln_solid"></div>

              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="equipamento" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Colaborador <span class="required" >*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <select type="text" class="form-control has-feedback-right" name="colaborador" id="colaborador"  placeholder="Colaborador" value="<?php printf($id_tecnico); ?>">
                    <option value="">Selecione um colaborador	</option>
                    <?php



                    $sql = "SELECT  id, primeironome,ultimonome FROM colaborador   where trash = 1";


                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id,$primeironome_query,$ultimonome_query);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>	" <?php if($id == $id_tecnico ) { printf("selected"); } ?>	><?php printf($primeironome_query);?>	    <?php printf($ultimonome_query);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
                     
                  </select>
                </div>
              </div>









              <script>
              $(document).ready(function() {
                $('#colaborador').select2();
              });
              </script>

              <br>

              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="backlog" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Backlog <span></span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="timepicker1" name="backlog"  class="form-control " value="<?php printf($time_backlog); ?>"  pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99">
                </div>
              </div>





              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="fornecedor" class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">Fornecedor <span aria-hidden="true"></span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <select type="text" class="form-control has-feedback-right" name="fornecedor" id="fornecedor"  placeholder="Fornecedor" value="<?php printf($id_fornecedor); ?>">
                    <option value="">Selecione um fornecedor	</option>
                    <?php



                    $sql = "SELECT  id, empresa FROM fornecedor  ";


                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id_empresa,$empresa_query);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id_empresa);?>	" <?php if($id_empresa == $id_fornecedor){ printf("selected"); }?> ><?php printf($empresa_query);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
   
                  </select>
                </div>
              </div>









              <script>
              $(document).ready(function() {
                $('#fornecedor').select2();
              });
              </script>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="chamado" class="docs-tooltip" data-toggle="tooltip" title="Digite o Nº Chamado ">Nº Chamado <span></span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="chamado" name="chamado"  class="form-control " value="<?php printf($id_chamado); ?>">
                </div>
              </div>

              <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="prioridade" class="docs-tooltip" data-toggle="tooltip" title="Selecione a Prioridade  ">Prioridade <span aria-hidden="true">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="text" class="form-control has-feedback-right" name="prioridade" id="prioridade"  placeholder="Prioridade" value="<?php printf($id_prioridade); ?>">
  

<?php



$sql = "SELECT  os_prioridade.id, os_prioridade.prioridade FROM os_prioridade 	LEFT JOIN os_sla ON os_sla.os_prioridade = os_prioridade.id ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
  $stmt->bind_result($id,$prioridade_smt);
  while ($stmt->fetch()) {
    if($id == $prioridade_os){
    ?>
    <option value="<?php printf($id);?>	" selected><?php printf($prioridade_smt);?>	</option>
    <?php	 } ?>
    <option value="<?php printf($id);?>	"><?php printf($prioridade_smt);?>	</option>
    <?php
    // tira o resultado da busca da memória
  }

}
$stmt->close();

?>
                      </select>

                    </div>
                  </div>
                  <script>
                  $(document).ready(function() {
                    $('#prioridade').select2();
                  });
                  </script>

<!-- new feature -->
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_category" class="docs-tooltip" data-toggle="tooltip" title="Selecione a Categoria  ">Categoria <span aria-hidden="true"></span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <select type="text" class="form-control has-feedback-right" name="id_category" id="id_category"  placeholder="Categoria" value="">
                  <option value="">Selecione uma Categoria	</option>
                    <?php				   $sql = "SELECT  id, nome FROM category 	 WHERE ativo = 0 and trash = 1 ";

if($id_category == ""){
  $id_category = $category_os;
}
                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id,$categoria_name);
                      while ($stmt->fetch()) {
                        if($id == $id_category){
                        ?> 
                        <option value="<?php printf($id);?>	"selected ><?php printf($categoria_name); ?>	</option>
                        <?php	 } ?>

                        <option value="<?php printf($id);?>	"><?php printf($categoria_name);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
 
                  </select>
                </div>
              </div>
              <script>
              $(document).ready(function() {
                $('#id_category').select2();
              });
              </script>

   <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_service" class="docs-tooltip" data-toggle="tooltip" title="Selecione um Tipo de serviço">Tipo de serviço <span aria-hidden="true"></span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <select type="text" class="form-control has-feedback-right" name="id_service" id="id_service"  placeholder="Serviço" value="">
                  <option value="">Selecione um Serviço	</option>
                <?php				   $sql = "SELECT  id, nome FROM custom_service ";

if($id_service == ""){
  $id_service = $service_os;
}
                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id,$custom_service);
                      while ($stmt->fetch()) {

                        if($id == $id_service ){
                        ?> 
                        <option value="<?php printf($id);?>	"selected ><?php printf($custom_service); ?>	</option>
                        <?php	 } ?>

                        <option value="<?php printf($id);?>	"><?php printf($custom_service);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
 
                  </select>
                </div>
              </div>
              <script>
              $(document).ready(function() {
                $('#id_service').select2();
              });
              </script>



              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align">Equipamento Parado</label>
                <div class="col-md-6 col-sm-6 ">
                  <div class="">
                    <label>
                      <input name="standing"type="checkbox" class="js-switch"  <?php if($standing=="0"){ printf("checked");}; ?>/>
                    </label>
                  </div>
                </div>
              </div>

              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="reg_date" class="docs-tooltip" data-toggle="tooltip" ><span></span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="reg_date" name="reg_date"  class="form-control " value="<?php printf($reg_date); ?>" hidden>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 "></label>
                <div class="col-md-3 col-sm-3 ">
                  <center>
                    <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações e Abrir</button>
                  </center>
                </div>
              </div>


            </form>
            <div class="ln_solid"></div>
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "></label>
              <div class="col-md-3 col-sm-3 ">
                <center>
                  <button id="compose" class="btn btn-sm btn-success btn-block" class="form-control "type="button">Observação</button>
                </center>
              </div>
            </div>

            <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 " class="docs-tooltip" data-toggle="tooltip" title="Observação "></label>
                  <div class="col-md-6 col-sm-6">
                  <textarea id="id_observacao" class="form-control" name="id_observacao" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($id_observacao); ?>" value="<?php printf($id_observacao); ?>" readonly="readonly"></textarea>

                  </div>
                </div>


          </div>
        </div>
      </div>
    </div>



    <div class="x_panel">
      <div class="x_title">
        <h2>Ação</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <a class="btn btn-app"  href="os-opened">
            <i class="glyphicon glyphicon-arrow-left"></i> Voltar
          </a>

          <a class="btn btn-app"  href="backend/os-opened-open-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
            title: 'Abertura',
            text: 'Ordem Recebida!',
            type: 'success',
            styling: 'bootstrap3'
          });">
          <i class="glyphicon glyphicon-floppy-saved"></i> Abrir
        </a>
        <a class="btn btn-app"  href="backend/os-opened-open-close-backend.php?os=<?php printf($id_os); ?>"onclick="new PNotify({
          title: 'Fechamento',
          text: 'Fechamento de O.S!',
          type: 'success',
          styling: 'bootstrap3'
        });">
        <i class="glyphicon glyphicon-floppy-remove"></i> Fechar
      </a>
      <!--   <a class="btn btn-app"  href=""onclick="new PNotify({
      title: 'Atualização',
      text: 'Atualização de Abertura de O.S!',
      type: 'danger',
      styling: 'bootstrap3'
    });">
    <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
  </a> -->
  <a class="btn btn-app"  href="os-opened-close?os=<?php printf($id_os); ?>" onclick="new PNotify({
    title: 'Cancelamento',
    text: 'Cancelamento de Abertura de O.S!',
    type: 'error',
    styling: 'bootstrap3'
  });">
  <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
</a>
<a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
  title: 'Visualizar',
  text: 'Visualizar O.S!',
  type: 'info',
  styling: 'bootstrap3'
});" >
<i class="fa fa-file-pdf-o"></i> Visualizar
</a>

</div>
</div>




</div>
</div>
<!-- /page content -->
<!-- compose -->
<div class="compose col-md-6  ">
  <div class="compose-header">
    Observação
    <button type="button" class="close compose-close">
      <span>×</span>
    </button>
  </div>
  <form  action="backend/os-opened-open-comment-backend.php?os=<?php printf($codigoget);?>" method="post">
    <div class="compose-body">
      <div id="alerts"></div>


      <input size="90" type="text" id="editor" name="editor" class="editor-wrapper">


    </div>

    <div class="compose-footer">
      <button class="btn btn-sm btn-success" type="submit">Salvar</button>
    </div>
  </form>
</div>
<!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
