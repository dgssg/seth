  <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
  <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
  <?php
$codigoget = ($_GET["id"]);

// Create connection

include("database/database.php");
$query = "SELECT  equipamento_familia_eb.id_fornecedor,equipamento_familia_eb.id_class,equipamento_familia_eb.preventive,equipamento_familia_eb.aquisicion,equipamento_familia_eb.manual_use,equipamento_familia_eb.manual_tec,equipamento_familia_eb.endoflife,equipamento_familia_eb.kpi,equipamento_grupo_eb.nome, equipamento_familia_eb.id, equipamento_familia_eb.id_equipamento_grupo, equipamento_familia_eb.nome, equipamento_familia_eb.fabricante, equipamento_familia_eb.modelo, equipamento_familia_eb.anvisa, equipamento_familia_eb.img, equipamento_familia_eb.upgrade, equipamento_familia_eb.reg_date FROM equipamento_familia_eb INNER JOIN equipamento_grupo_eb on equipamento_grupo_eb.id = equipamento_familia_eb.id_equipamento_grupo WHERE equipamento_familia_eb.id like '$codigoget' ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id_fornecedor,$id_class,$preventive,$aquisicion,$manual_use,$manual_tec,$endoflife,$kpi,$equipamento_grupo,$id, $id_equipamento_grupo, $nome, $fabricante, $modelo, $anvisa, $img, $upgrade, $reg_date);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
$img=trim($img);
    }


}
?>
  <!-- 1 -->
  <link href="dropzone.css" type="text/css" rel="stylesheet" />

  <!-- 2 -->
  <script src="dropzone.min.js"></script>
  <!-- page content -->
  <div class="right_col" role="main">
      <div class="">
          <div class="page-title">
              <div class="title_left">
                  <h3> Alteração <small> de Familia de Equipamento</small></h3>
              </div>


          </div>

          <div class="x_panel">
              <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                              aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                          </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">

                  <a class="btn btn-app" href="register-equipament-family-ep">
                      <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
              </div>
          </div>



          <div class="clearfix"></div>

          <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                  <div class="x_panel">
                      <div class="x_title">
                          <h2>Dados <small>da Familia</small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                      aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item" href="#">Settings 1</a>
                                      <a class="dropdown-item" href="#">Settings 2</a>
                                  </div>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">

                          <form
                              action="backend/register-equipament-edit-dados-update-backend-ep.php?id=<?php printf($codigoget); ?> "
                              method="post">

                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input name="nome" id="nome" type="text" value="<?php printf($nome); ?>"
                                          class="form-control ">
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="grupo">Grupo <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6">

                                      <select type="text" class="form-control has-feedback-right"
                                          name="equipamento_grupo" id="equipamento_grupo"
                                          placeholder="equipamento_grupo">
                                          <option value="	">Selecione o Grupo de Equipamento</option>
                                          <?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo_eb WHERE trash = 1";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                          <option value="<?php printf($id);?>	"
                                              <?php if($id==$id_equipamento_grupo){printf("selected");}?>>
                                              <?php printf($equipamento_grupo);?> </option>
                                          <?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
                                      </select>
                                      <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"
                                          class="docs-tooltip" data-toggle="tooltip"
                                          title="Selecione o Equipamento "></span>


                                  </div>

                                  <script>
                                  $(document).ready(function() {
                                      $('#equipamento_grupo').select2();
                                  });
                                  </script>
                              </div>
                          
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="modelo">Modelo <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input name="modelo" id="modelo" type="text" value="<?php printf($modelo); ?>"
                                          class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align"
                                      for="fabricante">Fabricante <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input name="fabricante" id="fabricante" type="text"
                                          value="<?php printf($fabricante); ?>" class="form-control ">
                                  </div>
                              </div>
                                                           <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">KPI</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="kpi" type="checkbox" class="js-switch"
                                                  <?php if($kpi == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="aquisicion">Valor
                                      Aquisição (R$) <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input name="aquisicion" id="aquisicion" type="text"
                                          value="<?php printf($aquisicion); ?>" class="form-control "
                                          onKeyPress="return(moeda(this,'.',',',event))">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="preventive">Custo M.P
                                      (R$) <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input name="preventive" id="preventive" type="text"
                                          value="<?php printf($preventive); ?>" class="form-control "
                                          onKeyPress="return(moeda(this,'.',',',event))">
                                  </div>
                              </div>

                            
                                <div class="form-group row">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="grupo">Fornecedor <span
                                          class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6">

                                      <select type="text" class="form-control has-feedback-right"
                                          name="id_fornecedor" id="id_fornecedor"
                                          placeholder="fornecedor">
                                          <option value="	">Selecione um Fornecedor</option>
                                          <?php
										  $sql = "SELECT  id, empresa FROM fornecedor WHERE trash = 1";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$empresa);
                                          while ($stmt->fetch()) {
                                                ?>
                                          <option value="<?php printf($id);?>	"
                                              <?php if($id==$id_fornecedor){printf("selected");}?>>
                                              <?php printf($empresa);?> </option>
                                          <?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
                                      </select>
                                     


                                  </div>

                                  <script>
                                  $(document).ready(function() {
                                      $('#id_fornecedor').select2();
                                  });
                                  </script>
                              </div>
                              <div class="item form-group">
                                  <label for="middle-name"
                                      class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input readonly="readonly" type="text" class="form-control"
                                          value="<?php printf($reg_date); ?> ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label for="middle-name"
                                      class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input readonly="readonly" type="text" class="form-control"
                                          value="<?php printf($upgrade); ?> ">
                                  </div>
                              </div>


                              <div class="form-group row">
                                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                                  <div class="col-md-3 col-sm-3 ">
                                      <center>
                                          <button class="btn btn-sm btn-success" type="submit"
                                              onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });">Salvar
                                              Informações</button>
                                      </center>
                                  </div>
                              </div>


                          </form>


                      </div>
                  </div>
              </div>




              <div class="clearfix"></div>

              <div class="row">
                  <div class="col-md-12">
                      <div class="x_panel">
                          <div class="x_title">
                              <h2>Imagem <small> </small></h2>
                              <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                  <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                          aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                          <a class="dropdown-item" href="#">Settings 1</a>
                                          <a class="dropdown-item" href="#">Settings 2</a>
                                      </div>
                                  </li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                                  </li>
                              </ul>
                              <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                              <div class="row">
                                  <div class="col-md-55">
                                      <?php if($img ==! ""){ ?>

                                      <div class="thumbnail">
                                          <div class="image view view-first">
                                              <img style="width: 100%; display: block;"
                                                  src="dropzone/equipamento/<?php printf($img); ?>" alt="image" />
                                              <div class="mask">
                                                  <p>Imagem</p>
                                                  <div class="tools tools-bottom">
                                                      <a href="dropzone/equipamento/<?php printf($img); ?>" download><i
                                                              class="fa fa-download"></i></a>

                                                  </div>
                                              </div>
                                          </div>
                                          <div class="caption">
                                              <p>Imagem</p>
                                          </div>
                                      </div>
                                      <?php } ?>

                                  </div>







                              </div>
                          </div>
                      </div>
                  </div>
              </div>



              <div class="x_panel">
                  <div class="x_title">
                      <h2>Inserir/Substituir</h2>
                      <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                  aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Settings 1</a>
                                  </li>
                                  <li><a href="#">Settings 2</a>
                                  </li>
                              </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                      </ul>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                      <form class="dropzone"
                          action="backend/regdate-equipament-familia-change-dropzone-upload-backend.php?id=<?php printf($id); ?>"
                          method="post">
                      </form>

                      <form action="backend/equipament-familia-img-upload-backend.php?id=<?php printf($codigoget); ?>"
                          method="post">
                          <center>
                              <button class="btn btn-sm btn-success" type="submit">Atualizar Imagem</button>
                          </center>
                      </form>

                  </div>
              </div>


              <!-- Posicionamento -->

               
              <!-- Registro -->
              <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                          <div class="modal-header">
                              <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                              </button>
                          </div>

                          <div class="modal-body">

                              <!-- Registro forms-->
                              <form
                                  action="backend/regdate-equipament-familia-dropzone-backend.php?id=<?php printf($codigoget);?>"
                                  method="post">
                                  <div class="ln_solid"></div>


                                  <div class="field item form-group">
                                      <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                                              class="required">*</span></label>
                                      <div class="col-md-6 col-sm-6">
                                          <input class="form-control" class='date' type="text" name="titulo"
                                              required='required'>
                                      </div>
                                  </div>




                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary"
                                          data-dismiss="modal">Fechar</button>
                                      <button type="submit" class="btn btn-primary">Salvar Informações</button>

                                  </div>
                          </div>
                          </form>
                          <form class="dropzone" action="backend/regdate-equipament-familia-dropzone-upload-backend.php"
                              method="post">
                          </form>

                      </div>
                  </div>
              </div>
              <!-- Registro -->

             
              <div class="x_panel">
                  <div class="x_title">
                      <h2>Ação</h2>
                      <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                  aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Settings 1</a>
                                  </li>
                                  <li><a href="#">Settings 2</a>
                                  </li>
                              </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                      </ul>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                      <a class="btn btn-app" href="register-equipament-family-ep">
                          <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                      </a>

                      

                  </div>
              </div>




          </div>
      </div>
      <!-- Fechamento -->
      <div class="modal fade bs-example-modal-lg5" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">

                  <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel4">Alterar</h4>
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <h4>Alterar Localização</h4>
                      <!-- Registro forms-->
                      <form action="" method="post">
                          <div class="ln_solid"></div>

                          <div class="item form-group">
                              <label class="col-form-label col-md-3 col-sm-3 label-align" for="localizacao">Localização
                                  <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 ">
                                  <input type="text" id="localizacao" name="localizacao"
                                      value="<?php printf($localizacao); ?>" class="form-control ">
                              </div>
                          </div>

                          <div class="item form-group">
                              <label class="col-form-label col-md-3 col-sm-3 label-align" for="">Localização <span
                                      class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 ">
                                  <select type="text" class="form-control" name="id_instituicao_localizacao" id="setor"
                                      placeholder="Setor">
                                      <?php
										$query = "SELECT instituicao_localizacao.id, instituicao.instituicao,instituicao_area.custo, instituicao_area.nome,instituicao_localizacao.codigo,instituicao_localizacao.andar,instituicao_localizacao.nome,instituicao_localizacao.observacao,instituicao_localizacao.upgrade,instituicao_localizacao.reg_date FROM instituicao_localizacao INNER JOIN instituicao ON instituicao.ID =   instituicao_localizacao.id_unidade INNER JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area ORDER BY instituicao_localizacao.id";
                                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                                        if ($stmt = $conn->prepare($query)) {
                                               $stmt->execute();
                                                  $stmt->bind_result($id, $area,$unidade,$custo,$codigo,$andar,$setor,$observacao,$upgrade,$reg_date);
                                          while ($stmt->fetch()) {
                                                ?>
                                      <option value="<?php printf($id);?>	"><?php printf($unidade);?>
                                          <?php printf($area);?>
                                          <?php printf($codigo);?><?php printf($andar);?><?php printf($setor);?>
                                      </option>
                                      <?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
                                  </select>
                              </div>
                          </div>










                          <script>
                          $(document).ready(function() {
                              $('#setor').select2();
                          });
                          </script>







                          <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                              <button type="submit" class="btn btn-primary">Salvar Informações</button>
                          </div>

                  </div>
              </div>
          </div>
          </form>
      </div>
      <!-- Fechamen    to -->

      <!-- compose -->
      <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript">
      </script>

      <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
      <script type="text/javascript">
      $("meuPrimeiroDropzone").dropzone({
          url: "upload.php"
      });
      Dropzone.options.meuPrimeiroDropzone = {
          paramName: "fileToUpload",
          dictDefaultMessage: "Arraste seus arquivos para cá!",
          maxFilesize: 300,
          accept: function(file, done) {
              if (file.name == "olamundo.png") {
                  done("Arquivo não aceito.");
              } else {
                  done();
              }
          }
      }
      </script>
      <script language="javascript">
      function moeda(a, e, r, t) {
          let n = "",
              h = j = 0,
              u = tamanho2 = 0,
              l = ajd2 = "",
              o = window.Event ? t.which : t.keyCode;
          if (13 == o || 8 == o)
              return !0;
          if (n = String.FROMCharCode(o),
              -1 == "0123456789".indexOf(n))
              return !1;
          for (u = a.value.length,
              h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
          ;
          for (l = ""; h < u; h++)
              -
              1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
          if (l += n,
              0 == (u = l.length) && (a.value = ""),
              1 == u && (a.value = "0" + r + "0" + l),
              2 == u && (a.value = "0" + r + l),
              u > 2) {
              for (ajd2 = "",
                  j = 0,
                  h = u - 3; h >= 0; h--)
                  3 == j && (ajd2 += e,
                      j = 0),
                  ajd2 += l.charAt(h),
                  j++;
              for (a.value = "",
                  tamanho2 = ajd2.length,
                  h = tamanho2 - 1; h >= 0; h--)
                  a.value += ajd2.charAt(h);
              a.value += r + l.substr(u - 2, u)
          }
          return !1
      }
      </script>
      <script language="JavaScript">
      $(document).ready(function() {

          $('#datatable').dataTable({
              "processing": true,
              "stateSave": true,
              responsive: true,



              "language": {
                  "loadingRecords": "Carregando dados...",
                  "processing": "Processando  dados...",
                  "infoEmpty": "Nenhum dado a mostrar",
                  "emptyTable": "Sem dados disponíveis na tabela",
                  "zeroRecords": "Não há registros a serem exibidos",
                  "search": "Filtrar registros:",
                  "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoFiltered": " - filtragem de _MAX_ registros",
                  "lengthMenu": "Mostrar _MENU_ registros",

                  "paginate": {
                      "previous": "Página anterior",
                      "next": "Próxima página",
                      "last": "Última página",
                      "first": "Primeira página",



                  }
              }




          });
      });
      


      </script>