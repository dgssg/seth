<?php
include("database/database.php");
$query = "SELECT   calibration_parameter_manufacture.email,calibration_parameter_manufacture.cnpj,calibration_parameter_manufacture.id,calibration_parameter_manufacture.nome,calibration_parameter_manufacture.cidade FROM calibration_parameter_manufacture  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($email,$cnpj,$id, $name, $cidade);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Empresa</th>
                          <th>Cidade</th>
                          <th>CNPJ</th>
                          <th>email</th>
                          <th>Ação</th>
                           </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($name); ?> </td>
                          <td><?php printf($cidade); ?></td>
                           <td><?php printf($cnpj); ?></td>
                            <td><?php printf($email); ?></td>
                             <td>
                  <a class="btn btn-app"  href="calibration-parameter-manufacture-edit?manufacture=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                    <a class="btn btn-app"  href="mailto:<?php printf($email); ?> ?subject=Solicitação&body=Prezado(a), <?php printf($contato); ?> "  onclick="new PNotify({
																title: 'Email',
																text: 'Email Fornecedor',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-envelope-o"></i> Email
                  </a>
                  <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/calibration-manufacture-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
