<?php
$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query="SELECT  purchases_input.id_fornecedor,purchases_input.id,purchases_material.nome,purchases_input.nf,purchases_input.data_nf,purchases_input.qtd,purchases_input.vlr,fornecedor.empresa,purchases_input.upgrade,purchases_input.reg_date FROM purchases_input INNER JOIN purchases_material ON purchases_material.id = purchases_input.id_purchases_material INNER JOIN fornecedor ON fornecedor.id = purchases_input.id_fornecedor WHERE purchases_input.id = $codigoget ";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result( $id_fornecedor,$id,$id_purchases_material,$nf,$data_nf,$qtd,$vlr,$fornecedor,$upgrade,$reg_date);

 while ($stmt->fetch()) {
//pintf("%s, %s\n", $solicitante, $equipamento);
  }
}
?>

 <div class="right_col" role="main">

        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Edição</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                <div class="input-group">

                  <span class="input-group-btn">

                  </span>
                </div>
              </div>
            </div>
          </div>


       <div class="x_panel">
              <div class="x_title">
                <h2>Alerta</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

           <div class="alert alert-success alert-dismissible " role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Area de Edição!</strong> Todo alteração não é irreversível.
                </div>



              </div>
            </div>



           <!-- page content -->









            <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edição <small>de Material</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a class="dropdown-item" href="#">Settings 1</a>
                        </li>
                        <li><a class="dropdown-item" href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />

                  <form action="backend/purchases-inventory-input-edit-backend.php?id=<?php printf($codigoget); ?>" method="post">
                    <div class="ln_solid"></div>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Materia <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="purchases_material" id="purchases_material"  placeholder="Material" readonly="readonly">
                      <option value="">Selecione um Material</option>
                          <?php



                          $result_cat_post  = "SELECT  id, cod,nome,unidade FROM purchases_material ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            if ($row_cat_post['id'] == $id) {
                              echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['cod'].' - '.$row_cat_post['nome'].' - '.$row_cat_post['unidade'].'</option>';

                            }

                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['cod'].' - '.$row_cat_post['nome'].' - '.$row_cat_post['unidade'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Material "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#').select2();
                    });
                    </script>


                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">NF</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="nf" class="form-control" type="text" name="nf" value="<?php printf($nf); ?>"  >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data NF</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="data_nf"  type="date" name="data_nf"   value="<?php printf($data_nf); ?>"  class="form-control">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="qtd" class="form-control" type="number" name="qtd"   value="<?php printf($qtd); ?>" >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="vlr" class="form-control" type="text" name="vlr"   value="<?php printf($vlr); ?>" >
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Fornecedor <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="fornecedor" id="fornecedor"  placeholder="Fornecedor">
                      <option value="">Selecione um Fornecedor</option>
                          <?php



                          $result_cat_post  = "SELECT  id, empresa FROM fornecedor  WHERE trash = 1 ";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            if ($row_cat_post['id'] == $id_fornecedor) {
                              echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['empresa'].'</option>';

                            }

                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['empresa'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Fornecedor "></span>

                      </div>
                    </div>

                    <script>
                    $(document).ready(function() {
                      $('#fornecedor').select2();
                    });
                    </script>



                    <div class="item form-group">
                           <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                           <div class="col-md-6 col-sm-6 ">
                          <input readonly="readonly" type="text"  class="form-control"  value="<?php printf($reg_date); ?> ">
                           </div>
                         </div>
                         <div class="item form-group">
                           <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                           <div class="col-md-6 col-sm-6 ">
                          <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                           </div>
                         </div>


                <div class="modal-footer">

                 <button type="submit" class="btn btn-primary" onclick="new PNotify({
                           title: 'Registrado',
                           text: 'Informações registrada!',
                           type: 'success',
                           styling: 'bootstrap3'
                       });" >Atualizar Informações</button>
                </div>


                </form>




                </div>
              </div>
            </div>
          </div>



           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="purchases-inventory">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>




              </div>
            </div>




      </div>
          </div>
      <!-- /page content -->

  <!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
