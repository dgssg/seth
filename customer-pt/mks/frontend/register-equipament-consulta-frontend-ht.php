<?php
include("database/database.php");

?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">


                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                   

                        <th>#</th>
                          <th><input type="checkbox" id="check_all"></th>
                            <th>Grupo</th>
                        <th>Codigo</th>
                        <th>Equipamento</th>
                        <th>Modelo</th>
                        <th>Fabricante</th>
                        <th>N/S</th>
                         <th>Patrimonio</th>
                        <th>Unidade</th>
                        
                        <th>Setor</th>
                        <th>Area</th>
                        <th>Baixa</th>
                        <th>Ativo</th>
                        <th>Comodato</th>
                                                  <th>Nº</th>

                        <th>Ação</th>
                          <th>Documentos</th>
                                               

                        </tr>
                      </thead>

                     
                      <tbody>
                      
                     
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
              <script language="JavaScript">
 let paginaAtual = 0;

 $(document).ready(function() {
    $('#check_all').click(function() {
      $('input[type=checkbox]').prop('checked', this.checked); // Seleciona todas as checkboxes
    });
  $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );

  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-equipamento-ht.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
    .then(data => {
      const novosDados = data.map(item => {
      let badgeHTML = '';
      let badgeHTMLBaixa = '';
      let badgeHTMLcheckist= '';
      let badgeHTMLtreinamento= '';
      let badgeHTMLparecer_1= '';
      let badgeHTMLparecer_2= '';
      let badgeHTMLsaida_1= '';
      let badgeHTMLsaida_2= '';
      let badgeHTMLconformidade= '';
            
      if (item.nf_dropzone != null) {
        badgeHTML = "<span class='badge badge-success'>OK</span>";
      }
      if (item.checklist != null) {
          badgeHTMLcheckist = "<span class='badge badge-success'>OK</span>";
        }
         if (item.treinamento != null) {
          badgeHTMLtreinamento = "<span class='badge badge-success'>OK</span>";
        }
            if (item.parecer_1 != null) {
          badgeHTMLparecer_1 = "<span class='badge badge-success'>OK</span>";
        }
           if (item.parecer_2 != null) {
          badgeHTMLparecer_2 = "<span class='badge badge-success'>OK</span>";
        }
        if (item.saida_1 != null) {
          badgeHTMLsaida_1 = "<span class='badge badge-success'>OK</span>";
        }
         if (item.saida_2 != null) {
          badgeHTMLsaida_2 = "<span class='badge badge-success'>OK</span>";
        }
          if (item.conformidade != null) {
          badgeHTMLconformidade = "<span class='badge badge-success'>OK</span>";
        }
      if (item.baixa == 0) {
          badgeHTMLBaixa = "<span class='badge badge-success'>OK</span>";
        }
      return [
        ``,
        '<input type="checkbox" class="checkbox" value="' + item.id + '">', // Adiciona a checkbox em cada linha
  item.grupo,
  item.codigo,
  item.equipamento,
  item.modelo,
  item.fabricante,
  item.serie,
    item.patrimonio,
  item.instituicao,
  item.area,
 
  item.setor,
  item.baixa == "0" ? "Sim" : "Não",
  item.ativo == "0" ? "Sim" : "Não",
  item.comodato == "0" ? "Sim" : "Não",
         item.id,

  `<a class="btn btn-app btn-editar" href="register-equipament-edit-ht?equipamento=${item.id}" onclick="new PNotify({
          title: 'Editar',
          text: 'Abrindo Edição',
          type: 'sucess',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-edit"></i> Editar
        </a>  <a class="btn btn-app"  href="register-equipament-tag-ht?equipamento=${item.id}" target="_blank" onclick="new PNotify({
																title: 'Etiqueta',
																text: 'Abrir Etiqueta',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-qrcode"></i> Etiqueta
                  </a><a class="btn btn-app"  href="register-equipament-label-ht?equipamento=${item.id}" target="_blank" onclick="new PNotify({
                               title: 'Etiqueta',
                               text: 'Abrir Etiqueta',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-fax"></i> Rótulo
                 </a>
                 <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-equipament-trash-backend-ht?id=${item.id}';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>`,
                   // botão de edição para cada linha
                   ` <a class="btn btn-app"  href="register-equipament-record-ht?equipamento=${item.id}" target="_blank" onclick="new PNotify({
																title: 'Ficha',
																text: 'Abrir Ficha',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Ficha
                  </a>
                    
                  <a class="btn btn-app" href="dropzone/nf/${item.nf_dropzone}" target="_blank" onclick="new PNotify({
      title: 'Prontuario',
      text: 'Abrir Prontuario',
      type: 'success',
      styling: 'bootstrap3'
    });">
      <i class="fa fa-file-o"></i> Nota Fiscal
  ${badgeHTML}
    </a>` 
                   
                 
      ];

      });

    // Adiciona as novas linhas ao DataTables e desenha a tabela
    $('#datatable').DataTable().rows.add(novosDados).draw();

 // Retorna à página atual após a exclusão

  $('#datatable').DataTable().page(paginaAtual).draw('page');

// Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([2,3,4,5,6,7,8,9,10,11,12,13,14,15]).every(function (d) {
        var column = this;
  var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
            
      
            
    });
});

                // Função para tomar a ação com as linhas selecionadas
                function takeAction() {
                  var selectedRows = [];
                  $('.checkbox:checked').each(function() {
                    selectedRows.push($(this).val());
                  });
                  
                  // Verifica se pelo menos um item foi selecionado
                  if (selectedRows.length > 0) {
                    // Constrói o URL com os IDs dos itens selecionados
                    var url = 'register-equipament-tag-selected-ht?equipamento=' + selectedRows.join(',');
                    // Abre o URL em uma nova aba
                    window.open(url, '_blank');
                  } else {
                    // Se nenhum item foi selecionado, mostra uma mensagem
                    alert('Por favor, selecione pelo menos um item.');
                  }
                }


</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#btn-carregar').click(function() {
    $.ajax({
      url: 'loading/load_equipamento.php', // substitua pelo nome do seu arquivo PHP que busca os itens restantes da tabela
      type: 'POST', // ou 'GET', dependendo da sua implementação
      data: {
        offset: 10 // substitua pelo número de itens que você já carregou
      },
      success: function(data) {
        $('#datatable').append(data); // adicione os novos itens à tabela
      }
    });
  });
});

</script>

