<?php
include("database/database.php");
$query="SELECT purchases_input.id,purchases_material.nome,purchases_input.nf,purchases_input.data_nf,purchases_input.qtd,purchases_input.vlr,fornecedor.empresa,purchases_input.upgrade,purchases_input.reg_date FROM purchases_input INNER JOIN purchases_material ON purchases_material.id = purchases_input.id_purchases_material INNER JOIN fornecedor ON fornecedor.id = purchases_input.id_fornecedor";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result( $id,$id_purchases_material,$nf,$data_nf,$qtd,$vlr,$id_fornecedor,$upgrade,$reg_date);

?>
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Material</th>
                  <th>NF</th>
                  <th>Data</th>
                  <th>Quantidade</th>
                  <th>Valor</th>
                  <th>Fornecedor</th>
                    <th>Registro</th>
                    <th>Atualização</th>
                  <th>Ação</th>


                 </tr>
               </thead>
               <tbody>
                      <?php  while ($stmt->fetch()) { ?>

                 <tr>
                   <th scope="row"><?php printf($row); ?></th>
                   <td><?php printf($id_purchases_material); ?></td>
                   <td><?php printf($nf); ?></td>
                   <td><?php printf($data_nf); ?></td>
                   <td><?php printf($qtd); ?></td>
                   <td><?php printf($vlr); ?></td>
                   <td><?php printf($id_fornecedor); ?></td>
                    <td><?php printf($reg_date); ?></td>
                    <td><?php printf($upgrade); ?></td>
                    
                     <td>
                       <a class="btn btn-app"  href="purchases-inventory-edit?id=<?php printf($id); ?>"onclick="new PNotify({
                                      title: 'Atualização',
                                      text: 'Atualização!',
                                      type: 'danger',
                                      styling: 'bootstrap3'
                                  });">
                          <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                        </a>
                        <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/purchases-inventory-hfmea-trash.php?id=<?php printf($id); ?>';
  }
})
">
                           <i class="glyphicon glyphicon-trash"></i> Excluir
                         </a>
                     </td>
                 </tr>
            <?php  $row=$row+1; }
}   ?>
               </tbody>
             </table>







        <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal">
             <div class="modal-dialog modal-lg">
               <div class="modal-content">

                 <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Itens</h4>
                   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                   </button>
                 </div>
                 <div class="modal-body">
                    <form action="backend/purchases-inventory-input-backend.php" method="post">
                      <div class="ln_solid"></div>



                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Materia <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-left" name="purchases_material" id="purchases_material"  placeholder="Material">
                        <option value="">Selecione um Material</option>
                            <?php



                            $result_cat_post  = "SELECT  id, cod,nome,unidade FROM purchases_material ";

                            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                            while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                              echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['cod'].' - '.$row_cat_post['nome'].' - '.$row_cat_post['unidade'].'</option>';
                            }
                            ?>

                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Material "></span>

                        </div>
                      </div>

                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>


                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">NF</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="nf" class="form-control" type="text" name="nf"   >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data NF</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_nf" class="form-control" type="date" name="data_nf"   >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="qtd" class="form-control" type="number" name="qtd"   >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="vlr" class="form-control" type="text" name="vlr"   >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Fornecedor <span class="required"></span>
                        </label>
                        <div class="input-group col-md-6 col-sm-6">
                          <select type="text" class="form-control has-feedback-left" name="fornecedor" id="fornecedor"  placeholder="Fornecedor">
                        <option value="">Selecione um Fornecedor</option>
                            <?php



                            $result_cat_post  = "SELECT  id, empresa FROM fornecedor  WHERE trash = 1 ";

                            $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                            while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                              echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['empresa'].'</option>';
                            }
                            ?>

                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Fornecedor "></span>

                        </div>
                      </div>

                      <script>
                        $(document).ready(function() {
                          $('#').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>


                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                   <button type="submit" class="btn btn-primary" onclick="new PNotify({
                             title: 'Registrado',
                             text: 'Informações registrada!',
                             type: 'success',
                             styling: 'bootstrap3'
                         });" >Salvar Informações</button>
                 </div>

               </div>
             </div>
           </div>
       </form>

       <!-- -->
                    