  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Saida de <small>material</small></h3>
              </div>


            </div>
       <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

<!-- Menu -->
<a class="btn btn-app <?php echo $current_page == 'purchases-inventory' ? 'active' : ''; ?>" href="purchases-inventory">
  <i class="fa fa-level-up"></i> Entrada
</a>

<a class="btn btn-app <?php echo $current_page == 'purchases-inventory-output' ? 'active' : ''; ?>" href="purchases-inventory-output">
  <i class="fa fa-level-down"></i> Saída
</a>

<a class="btn btn-app <?php echo $current_page == 'purchases-inventory-stock' ? 'active' : ''; ?>" href="purchases-inventory-stock">
  <i class="fa fa-exchange"></i> Estoque
</a>

<a class="btn btn-app <?php echo $current_page == 'purchases-inventory-manager' ? 'active' : ''; ?>" href="purchases-inventory-manager">
  <i class="fa fa-gears"></i> Gerenciar
</a>



                </div>
              </div>


            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                          aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="userstable_filter"
                      style="column-count:8; -moz-column-count:8; -webkit-column-count:8; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;">
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
                <div class="clearfix"></div>


               <div class="x_panel">
                <div class="x_title">
                  <h2>Saida de Material </h2>
                  <ul class="nav navbar-right panel_toolbox">
                        <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <?php include 'frontend/purchases-inventory-output-list-frontend.php';?>

                </div>
              </div>




                </div>
              </div>
              <script type="text/javascript">
              $(document).ready(function(){
                $('#instituicao').change(function(){
                  $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
                });
              });
              </script>
              <script type="text/javascript">
              $(document).ready(function(){
                $('#area').change(function(){
                  $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
                });
              });
              </script>
                          
                          <script type="text/javascript">
                            $(document).ready(function() {
                              $('#datatable').DataTable({
                                "processing": true,
                                "stateSave": true,
                                responsive: true,
                                
                                
                                
                                "language": {
                                  "loadingRecords": "Carregando dados...",
                                  "processing": "Processando  dados...",
                                  "infoEmpty": "Nenhum dado a mostrar",
                                  "emptyTable": "Sem dados disponíveis na tabela",
                                  "zeroRecords": "Não há registros a serem exibidos",
                                  "search": "Filtrar registros:",
                                  "info": "Mostrando página _PAGE_ de _PAGES_",
                                  "infoFiltered": " - filtragem de _MAX_ registros",
                                  "lengthMenu": "Mostrar _MENU_ registros",
                                  
                                  "paginate": {
                                    "previous": "Página anterior",
                                    "next": "Próxima página",
                                    "last": "Última página",
                                    "first": "Primeira página",
                                    
                                    
                                    
                                  }
                                },
                                initComplete: function() {
                                  this.api()
                                  .columns([1, 2,3,4,5,6])
                                  .every(function(d) {
                                    var column = this;
                                    var theadname = $("#datatable th").eq([d]).text();
                                    var container = $('<div class="filter-title"></div>')
                                    .appendTo('#userstable_filter');
                                    var label = $('<label>' + theadname + ': </label>')
                                    .appendTo(container);
                                    var select = $(
                                      '<select  class="form-control my-1 filter-title" ><option value="">' +
                                      theadname + '</option></select>')
                                    
                                    .appendTo(container).select2()
                                    .on('change', function() {
                                      var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                      
                                      column.search(val ? '^' + val + '$' : '', true, false).draw();
                                    });
                                    
                                    column
                                    .data()
                                    .unique()
                                    .sort()
                                    .each(function(d, j) {
                                      select.append('<option value="' + d + '">' + d + '</option>');
                                    });
                                  });
                                  
                                  
                                },
                              });
                            });
                            
                            
                          </script>