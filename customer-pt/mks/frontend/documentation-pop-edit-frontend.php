<?php
include("database/database.php");
$codigoget = ($_GET["pop"]);
$query = "SELECT documentation_pop.pop_ver,documentation_pop.id_signature,documentation_pop.id_resp,documentation_pop.id_setor,documentation_pop.id_unidade,documentation_pop.rev,documentation_pop.signature,documentation_pop.create,documentation_pop.group_pop,documentation_pop.titulo,documentation_pop.codigo,equipamento_grupo.nome, documentation_pop.id, documentation_pop.id_equipamento_grupo, documentation_pop.file, documentation_pop.pop, documentation_pop.data_now, documentation_pop.data_after, documentation_pop.upgrade, documentation_pop.reg_date FROM documentation_pop LEFT JOIN equipamento_grupo on equipamento_grupo.id = documentation_pop.id_equipamento_grupo WHERE documentation_pop.id = '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($pop_ver,$id_signature,$id_colaborador,$id_setor,$id_unidade,$rev,$signature,$create,$group_pop,$titulo,$codigo,$grupo,$id, $id_equipamento_grupo, $file, $pop, $data_now, $data_after, $upgrade, $reg_date);
}
 while ($stmt->fetch()) {
 }
$sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }

  
function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
  $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
  $con = 1;
  $var = '';
  while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
  }else{
      $var = $var.$n.$res[$n];
      $con++;
      }
  }
      $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
 // echo chaveAlfaNumerica(5);
//   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = md5($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 3
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function v4() {
return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

// 32 bits for "time_low"
mt_rand(0, 0xffff), mt_rand(0, 0xffff),

// 16 bits for "time_mid"
mt_rand(0, 0xffff),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand(0, 0x0fff) | 0x4000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand(0, 0x3fff) | 0x8000,

// 48 bits for "node"
mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
);
}

public static function v5($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = sha1($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function is_valid($uuid) {
return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();

//echo $v4uuid; //chave da assinatura
?>
 
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- <link rel="stylesheet" href="https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.css">
    <style>
        .main-container {
            width: 795px;
            margin-left: auto;
            margin-right: auto;
        }
    </style> -->
 <link rel="stylesheet" href="https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.css">
    <style>
        .main-container {
            width: 795px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Editar <small>POP</small></h3>
              </div>


            </div>

             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cadastro<small>POP</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<form  action="backend/documentation-pop-upgrade-backend.php?pop=<?php printf($codigoget);?>" method="post">





								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>

	 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Grupo Equipamento<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="Grupo Equipamento">
										    	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo WHERE id like '$id_equipamento_grupo'";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
										  	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo  WHERE trash = 1 ";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
	                                     	</select>
 <span class="input-group-btn">


										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">
										    </span>
										</span>										 </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>
                                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Grupo POP<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="group_pop" id="group_pop"  placeholder="Grupo POP">
										    	<?php
										  $sql = "SELECT  id, nome FROM documentation_pop_goup WHERE id like '$group_pop'";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
										  	<?php
										  $sql = "SELECT  id, nome FROM documentation_pop_goup ";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}
                                            }
											$stmt->close();
											?>
	                                     	</select>
 <span class="input-group-btn">


										<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">
										    </span>
										</span>										 </div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#group_pop').select2();
                                      });
                                 </script>
                                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="titulo"><span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="id" name="id" value="<?php printf($id); ?>"  required="required" class="form-control " hidden>
                        </div>
                      </div>


		 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="titulo">Titulo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="titulo" name="titulo" value="<?php printf($titulo); ?>"  required="required" class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"  required="required" class="form-control ">
                        </div>
                      </div>
                       <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Revisão<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" type="date" name="date_now"  value="<?php printf($data_now); ?>" class="form-control"></div>
                    </div>
                     <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Proxima revisão<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" type="date" name="date_after"   value="<?php printf($data_after); ?>" class="form-control"></div>
                    </div>

 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="create">Elaboração<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="create" name="create" value="<?php printf($create); ?>"  class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="signature">Aprovação<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="signature" name="signature" value="<?php printf($signature); ?>"   class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="rev">Revisão<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="rev" name="rev" value="<?php printf($rev); ?>"    class="form-control ">
                        </div>
                      </div>

<div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="id_unidade" id="id_unidade"  placeholder="Setor">
        <option value="">Selecione uma Unidade</option>
        <?php



$sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";


if ($stmt = $conn->prepare($sql)) {
$stmt->execute();
$stmt->bind_result($id,$instituicao);
while ($stmt->fetch()) {
?>

<option value="<?php printf($id);?>	" <?php if($id == $id_unidade){printf("selected");} ?> > <?php printf($instituicao);?>	</option>
 <?php
// tira o resultado da busca da memória
}

                     }
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Unidade "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_unidade').select2();
  });
  </script>
                      <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Setor">
        <option value="">Selecione um Setor</option>
        <?php



$sql = "SELECT  id, codigo,nome FROM instituicao_area WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
  $stmt->bind_result($id,$codigo,$nome);
  while ($stmt->fetch()) {
    ?>
    <option value="<?php printf($id);?>	" <?php if($id == $id_setor){printf("selected");};?>><?php printf($codigo);?> <?php printf($nome);?>	</option>
    <?php
    // tira o resultado da busca da mem��ria
  }

}
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#area').select2();
  });
  </script>
<div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Responsável 
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Responsável" >
        <option value="	">Selecione o Responsável</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	" <?php if($id == $id_colaborador){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_colaborador').select2();
  });
  </script>
		 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Cadastro <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($reg_date); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      	 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Atualização <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($upgrade); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                       	 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_signature">Assinatura Atual <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="id_signature" name="id_signature" value="<?php printf($id_signature); ?>" readonly="readonly" class="form-control ">
                        </div>
                      </div>




			<textarea name="editor" id="editor"  placeholder="" value="" rows="100" cols="800"><?php echo htmlspecialchars_decode($pop);?>




												</textarea>
												    <script type="importmap">
        {
            "imports": {
                "ckeditor5": "https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.js",
                "ckeditor5/": "https://cdn.ckeditor.com/ckeditor5/42.0.0/"
            }
        }
    </script>
   <script type="module">
        import {
            ClassicEditor,
            Essentials,
            Paragraph,
            Bold,
            Italic,
            Font,
            Table,
            TableToolbar,
            PasteFromOffice,
            Image,
            ImageToolbar,
            ImageCaption,
            ImageStyle,
            ImageResize,
            ImageUpload,
             ImageInsert,
            List,
            Alignment,
            Heading,
             FontSize,
             WordCount
            
            
        } from 'ckeditor5';

        ClassicEditor
            .create(document.querySelector('#editor'), {
                plugins: [
                    Essentials,
                    Paragraph,
                    Bold,
                    Italic,
                    Font,
                    Table,
                    TableToolbar,
                    PasteFromOffice,
                    Image,
                    ImageToolbar,
                    ImageCaption,
                    ImageStyle,
                    ImageResize,
                    ImageUpload,
                     ImageInsert,
                    List,
                    Alignment,
                    Heading,
                      FontSize,
                       WordCount
                   
                ],
                toolbar: [
                    'undo', 'redo', '|',
                    'heading', '|',
                    'bold', 'italic', '|',
                    'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', '|',
                    'alignment', '|',
                    'numberedList', 'bulletedList', '|',
                    'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
                      'imageUpload', 'imageInsert', '|', // Adicionando o botão ImageInsert na toolbar
                    'outdent', 'indent'
                ],
                   fontSize: {
                    options: [
                        'tiny',
                        'small',
                        'default',
                        'big',
                        'huge',
                        '10',
                        '12',
                        '14',
                        '16',
                        '18',
                        '20',
                        '22',
                        '24',
                        '26',
                        '28',
                        '36',
                        '48',
                        '72'
                    ],
                    
                }, 
                image: {
                    toolbar: [
                        'imageTextAlternative', '|',
                        'imageStyle:full', 'imageStyle:side', '|',
                        'imageResize'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn', 'tableRow', 'mergeTableCells'
                    ]
                }
            })
            .then(editor => {
                window.editor = editor;
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <!-- A friendly reminder to run on a server, remove this during the integration. -->
    <script>
        window.onload = function() {
            if (window.location.protocol === "file:") {
                alert("This sample requires an HTTP server. Please serve this file with a web server.");
            }
        };
    </script>

												<script>
														// Replace the <textarea id="editor1"> with a CKEditor
														// instance, using default configuration.
													//	CKEDITOR.replace( 'editor1' );
												</script>






											<button type="reset" class="btn btn-primary"onclick="new PNotify({
																title: 'Limpado',
																text: 'Todos os Campos Limpos',
																type: 'info',
																styling: 'bootstrap3'
														});" />Limpar</button>
											<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar" />

												</form>
														<div class="ln_solid"></div>



                  </div>
                </div>
              </div>
	       </div>

<div class="col-md-12 col-sm-12  ">
<div class="x_panel">
<div class="x_title">
<h2>POP <small>Atual</small></h2>
<ul class="nav navbar-right panel_toolbox">
<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
<a class="dropdown-item" href="#">Settings 1</a>
<a class="dropdown-item" href="#">Settings 2</a>
</div>
</li>
<li><a class="close-link"><i class="fa fa-close"></i></a>
</li>
</ul>
<div class="clearfix"></div>
</div>
<div class="x_content">
<div >
<div >
 
<p><?php echo htmlspecialchars_decode($pop_ver);?></p>
</div>
</div>
</div>
</div>
</div>
   <div class="clearfix"></div>
 

	        <!-- Posicionamento -->

	         <div class="x_panel">
                <div class="x_title">
                  <h2>Anexos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo -->
       <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_pop_dropzone WHERE id_pop like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                          <th>Titulo</th>
                          <th>Registro</th>
                          <th>Atualização</th>
                         <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id); ?></td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($upgrade); ?></td>
                         <td> <a class="btn btn-app"   href="dropzone/pop/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Anexo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Anexo
                  </a>
                     <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/documentation-pop-dropzone-trash.php?id=<?php printf($id); ?>&pop=<?php printf($codigoget); ?>';
  }
})
">
                    <i class="fa fa-trash"></i> Excluir
                  </a>
             
                  </td>

                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>




                </div>
              </div>

                <!-- Registro -->
               <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>

                        <div class="modal-body">

                        <!-- Registro forms-->
                          <form action="backend/documentation-pop-dropzone-backend.php?id_pop=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>


                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="titulo" required='required'></div>
                    </div>




                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>

                        </div>
                 </div>
             </form>
              <form  class="dropzone" action="backend/documentation-pop-dropzone-upload-backend.php" method="post">
    </form >
     </div>
                    </div>
                  </div>
              <!-- Registro -->
 


                     
                <div class="clearfix"></div>

 <!-- Posicionamento -->

	         <div class="x_panel">
                <div class="x_title">
                  <h2>Imagens</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg3" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo -->
       <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_pop_img_dropzone WHERE id_pop like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                          <th>Titulo</th>
                          <th>Registro</th>
                          <th>Atualização</th>
                         <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id); ?></td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($upgrade); ?></td>
                         <td> <a class="btn btn-app"   href="dropzone/files/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Anexo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Anexo
                  </a>
                   <button class="btn btn-app" onclick="copyUrlToClipboard_<?php printf($id); ?>('dropzone/files/<?php echo $file; ?>')">
    <i class="fa fa-clipboard"></i> Copiar URL
  </button>
  <script>
  function copyUrlToClipboard_<?php printf($id); ?>(url) {
    // Cria um elemento de texto temporário
    var tempInput = document.createElement("input");
    // Define o valor do elemento de texto como a URL
   // tempInput.value = window.location.origin + '/' + url;
    tempInput.value = url;
    // Adiciona o elemento de texto ao documento
    document.body.appendChild(tempInput);
    // Seleciona o conteúdo do elemento de texto
    tempInput.select();
    // Copia o conteúdo para a área de transferência
    document.execCommand("copy");
    // Remove o elemento de texto temporário
    document.body.removeChild(tempInput);
    // Notifica o usuário que a URL foi copiada
    new PNotify({
      title: 'URL Copiada',
      text: 'A URL foi copiada para a área de transferência.',
      type: 'success',
      styling: 'bootstrap3'
    });
  }
</script>
                     <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/documentation-pop-file-dropzone-trash.php?id=<?php printf($id); ?>&pop=<?php printf($codigoget); ?>';
  }
})
">
                    <i class="fa fa-trash"></i> Excluir
                  </a>
             
                  </td>

                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>




                </div>
              </div>

                <!-- Registro -->
               <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Imagem </h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>

                        <div class="modal-body">

                        <!-- Registro forms-->
                          <form action="backend/documentation-pop-file-dropzone-backend.php?id_pop=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>


                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="titulo" required='required'></div>
                    </div>




                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>

                        </div>
                 </div>
             </form>
              <form  class="dropzone" action="backend/documentation-pop-file-dropzone-upload-backend.php" method="post">
    </form >
     </div>
                    </div>
                  </div>
              <!-- Registro -->
 


                     
                <div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Assinatura <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="backend/documentation-pop-signature-backend.php?id=<?php printf($codigoget);?>" method="post">
          <div class="item form-group">
                        <label for="middle-name"
                            class="col-form-label col-md-3 col-sm-3 label-align">Chave da Assinatura</label>
                        <div class="col-md-6 col-sm-6 ">
                            <input class="form-control" type="text" value="<?php printf($signature_alert); ?> "
                                readonly="readonly">
                        </div>
                    </div>

<!-- Large modal --> <center>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Assinatura</button>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Assinatura</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <h4>Assinatura digital</h4>
                <p>Para Assinar digite o texto abaixo.</p>
               <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
                <p> <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> <span ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="assinature" name="assinature"   class="form-control " class="docs-tooltip" data-toggle="tooltip" title=" ">
                
              </div>
              <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >
              </div>
            </div>
            <script>
var chave = "<?php echo $chave; ?>"; // Obtém a chave do PHP e armazena na variável JavaScript
var assinaturaInput = document.getElementById("assinature");

assinaturaInput.addEventListener("blur", function () {
var assinatura = this.value;

if (assinatura !== chave) {
Swal.fire('A assinatura está incorreta!');
  // Aqui você pode realizar a ação desejada caso a assinatura seja divergente, como exibir uma mensagem de erro ou realizar algum redirecionamento.
} else {

  // Aqui você pode realizar a ação desejada caso a assinatura seja correta.
}
});
</script>
            </div></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Assinar</button>
              </div>

            </div>
          </div>
        </div>
       </center>
       
          </form>
        </div>
      </div>
    </div>
  </div>


  <div class="clearfix"></div>
  <div class="clearfix"></div>


<div class="x_panel">
    <div class="x_title">
      <h2>Assinatura</h2>
      <ul class="nav navbar-right panel_toolbox">
         
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      
    <!--arquivo -->
<?php    
$query="SELECT regdate_pop_signature.id, regdate_pop_signature.id_pop,regdate_pop_signature.id_user,regdate_pop_signature.id_signature,regdate_pop_signature.reg_date,usuario.nome FROM regdate_pop_signature INNER JOIN usuario ON regdate_pop_signature.id_user = usuario.id   WHERE regdate_pop_signature.id_pop like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$id_mp,$id_user,$id_signature,$reg_date,$id_user);

?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              
              <th>Usuário</th>
              <th>Assinatura</th>
               <th>Registro</th>
                <th>Ação</th>
            
            </tr>
          </thead>
          <tbody>
                 <?php  while ($stmt->fetch()) { ?>
                 
            <tr>
              <th scope="row"><?php printf($row); ?></th>
              <td><?php printf($id_user); ?></td>
              <td><?php printf($id_signature); ?></td>
              <td><?php printf($reg_date); ?></td>
              <td>                                                                <a class="btn btn-app" onclick="
                Swal.fire({
                  title: 'Tem certeza?',
                  text: 'Você não será capaz de reverter isso!',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Sim, Deletar!'
                }).then((result) => {
                  if (result.isConfirmed) {
                    Swal.fire(
                      'Deletando!',
                      'Seu arquivo será excluído.',
                      'success'
                    ),
                    window.location = 'backend/signature-pop-trash-backend.php?id=<?php printf($id); ?>&page=<?php printf($codigoget); ?>';
                  }
                })
              ">
                <i class="fa fa-trash"></i> Excluir
              </a>
</td>
             
              
            </tr>
       <?php  $row=$row+1; }
}   ?>
          </tbody>
        </table>

    
  
    
    </div>
  </div>  
    <!-- compose -->
        
 

	  <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="documentation-pop">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>


  

                </div>
              </div>
