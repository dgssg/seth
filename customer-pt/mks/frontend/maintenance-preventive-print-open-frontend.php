<?php
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
include("database/database.php");



$query = "SELECT equipamento.serie,equipamento_familia.fabricante,equipamento.data_calibration_end, maintenance_control.procedure_mp, maintenance_control.data_after, maintenance_control.id_routine, maintenance_control.data_start, instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_routine.id, maintenance_routine.data_start, maintenance_routine.periodicidade, maintenance_routine.reg_date, maintenance_routine.upgrade, equipamento.codigo, equipamento_familia.nome, equipamento_familia.modelo,  '1' as source,maintenance_control.id
FROM maintenance_routine
INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento
INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id
INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade
INNER JOIN maintenance_control ON maintenance_control.id_routine = maintenance_routine.id
WHERE maintenance_control.data_after <= '$today ' AND maintenance_control.status = 0 AND maintenance_routine.habilitado = 0

UNION ALL

SELECT equipamento.serie,equipamento_familia.fabricante,equipamento.data_calibration_end, maintenance_control_2.procedure_mp, maintenance_control_2.data_after, maintenance_control_2.id_routine, maintenance_control_2.data_start, instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_routine.id, maintenance_routine.data_start, maintenance_routine.periodicidade, maintenance_routine.reg_date, maintenance_routine.upgrade, equipamento.codigo, equipamento_familia.nome, equipamento_familia.modelo,  '2' as source,maintenance_control_2.id
FROM maintenance_routine
INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento
INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id
INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade
LEFT JOIN maintenance_control_2 ON maintenance_control_2.id_routine = maintenance_routine.id
WHERE maintenance_control_2.data_after <= '$today ' AND maintenance_control_2.status = 0 AND maintenance_routine.habilitado = 0;

";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($serie,$fabricante,$data_calibration_end,$procedure,$data_after,$routine_control,$date_control,$instituicao,$area,$setor,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo,$surce,$id_control);



?>


<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Rotina</th>
													<th>Laudo</th>
                          <th>Periodicidade</th>
                          
                          <th>Programada</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          <th>Atualização</th>
                          <th>Cadastro</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {
                          if($data_calibration_end != ""){
  $data_calibration_end_2 = date('Y-m', strtotime($data_calibration_end));
  $data_after_calibration_2 = date('Y-m', strtotime($data_after));

  if($data_calibration_end_2 <= $data_after_calibration_2){
       $procedure = 2;
    
  }
}  

														  ?>
                        <tr>
                          <td ><?php  printf($rotina); ?> </td>
													  <td><?php printf($data_calibration_end); ?></td>

<?php  ?>
<td><?php if($periodicidade==5){printf("Diaria [1 Dias Uteis ]");}if($periodicidade==365){printf("Anual [365 Dias]");}if($periodicidade==180){printf("Semestral [180 Dias]");} if($periodicidade==30){printf("Mensal [30 Dias]");}if($periodicidade==1){printf("Diaria [1 Dias]");} if($periodicidade==7){printf("Semanal [7 Dias]");}if($periodicidade==14){printf("Bisemanal [14 Dias]");}if($periodicidade==21){printf("Trisemanal [21 Dias]");} if($periodicidade==28){printf("Quadrisemanal [28 Dias]");} if($periodicidade==60){printf("Bimestral [60 Dias]");}if($periodicidade==90){printf("Trimestral [90 Dias]");}if($periodicidade==120){printf("Quadrimestral [120 Dias]");}if($periodicidade==730){printf("Bianual [730 Dias]");} if($periodicidade==1460){printf("Quadrianual [1460 Dias]");}if($periodicidade==1095){printf("Trianual [1095 Dias]");}  ?></td>
<td><?php printf($data_after); ?></td>

<td><?php printf($nome); ?></td>
                              <td><?php printf($modelo); ?></td>
                            <td><?php printf($fabricante); ?></td>
                            <td><?php printf($codigo); ?></td>
                            <td><?php printf($serie); ?></td>
                             
                              <td><?php printf($instituicao); ?></td>
                                 <td><?php printf($area); ?></td>
                                 <td>  <?php printf($setor); ?></td>                                
                                 <td><?php printf($upgrade); ?></td>

                          <td><?php printf($reg_date); ?></td>

                          <td>
<?php if($procedure==1 && $surce == 1 ){ ?>
                  <a class="btn btn-app"  href="backend/maintenance-preventive-print-single-backend?routine=<?php printf($rotina); ?>&procedure=<?php printf($procedure); ?>&date=<?php printf($data_calibration_end); ?>"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
<?php }; ?>
<?php if($procedure==2  && $surce == 1){ ?>
                  <a class="btn btn-app"  href="backend/maintenance-preventive-print-single-2-backend?routine=<?php printf($rotina); ?>&procedure=<?php printf($procedure); ?>&date=<?php printf($data_calibration_end); ?>"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
<?php }; ?>
<?php if($procedure==1 && $surce == 2 ){ ?>
                  <a class="btn btn-app"  href="backend/maintenance-preventive-print-single-source-backend?routine=<?php printf($id_control); ?>&procedure=<?php printf($procedure); ?>&date=<?php printf($data_calibration_end); ?>"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
<?php }; ?>
<?php if($procedure==2  && $surce == 2){ ?>
                  <a class="btn btn-app"  href="backend/maintenance-preventive-print-single-2-source-backend?routine=<?php printf($id_control); ?>&procedure=<?php printf($procedure); ?>&date=<?php printf($data_calibration_end); ?>"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
<?php }; ?>


                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
