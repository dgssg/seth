<?php

include("../database/database.php");


//$con->close();


$query = "SELECT id, grupo FROM os_grupo_sla ";


if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $grupo);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
    $result = $stmt->get_result();
    $outp = $result->fetch_all(MYSQLI_ASSOC);

$json = json_encode($outp);


?>

<!DOCTYPE html>

<html lang="en">
  <head>
      <!-- The jQuery library is a prerequisite for all jqSuite products -->
      <script type="text/ecmascript" src="../../framework/framework/jquery.min.js"></script>
      <!-- We support more than 40 localizations -->
      <script type="text/ecmascript" src="../../framework/framework/grid.locale-en.js"></script>
      <!-- This is the Javascript file of jqGrid -->
      <script type="text/ecmascript" src="../../framework/framework/jquery.jqGrid.min.js"></script>
      <!-- This is the localization file of the grid controlling messages, labels, etc.
      <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
      <link rel="stylesheet" type="text/css" media="screen" href="../../framework/framework/jquery-ui.css" />
      <!-- The link to the CSS that the grid needs -->
   <!--   <link rel="stylesheet" type="text/css" media="screen" href="../../framework/framework/ui.jqgrid.css" /> -->
      <meta charset="utf-8" />
     
      <script type="text/javascript"></script>
      <link rel="stylesheet" type="text/css" href="../../framework/framework/ndhui.css" />
      
      
      
        <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="../../../js/jquery.min.js"></script> 
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="../../framework/framework/js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="../../framework/framework/js/trirand/i18n/grid.locale-en.js"></script>
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="../../framework/framework/css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="../../framework/framework/css/trirand/ui.jqgrid.css" />
    
 <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="../../../js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="../../../js/trirand/i18n/grid.locale-en.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="../../../js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> 
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="../../../css/trirand/ui.jqgrid-bootstrap.css" />
	<script>
		$.jgrid.defaults.width = 780;
		$.jgrid.defaults.styleUI = 'Bootstrap';
	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
   

    
    
   </head>


   <td align="right">
  <!-- <img id="ctl00_ContentPlaceHolder1_lst_ctl00_imgQRCode" src="http://chart.apis.google.com/chart?cht=qr&amp;chl=FACV001&amp;chs=82x82" style="border-width:0px;" />
-->
</tr>
   <body>
    <button id="csv">CSV</button>


      <script type="text/javascript" language="javascript" src="../../framework/framework/ndhui.js?0=0&0=0&0=0"></script>
      <table id="jqGrid"></table>
      <div id="jqGridPager"></div>
      <script type="text/javascript">
        //    var arquivo:
         $(document).ready(function () {
             
                var template = "<div style='margin-left:15px;'><div> ID <sup>*</sup>:</div><div> {id} </div>";
			template += "<div> Grupo: </div><div>{grupo} </div>";
	
			template += "<hr style='width:50%;'/>";
			template += "<div>  {sData} {cData}  </div></div>";
             
             $("#jqGrid").jqGrid({
              //  url:
              //   mtype: "GET",
                 datatype: "local",
                 data: <?php printf($json); ?>,
                 colModel: [
                     { label: 'Id', name: 'id', width: 100, editable: false, formoptions: { colpos: 1, rowpos: 1 }  },
                     { label: 'Grupo', name: 'grupo', width: 150, editable: true, formoptions: { colpos: 1, rowpos: 1 }  },
                  
                     {label: "Ação",name: "id",width: 100,  formatter: "actions", formatoptions: { keys: true,editOptions: { url: 'edit_localizacao.php'}, delOptions: {  url:'del_localizacao.php' }    },


                       editoptions: {
                            dataUrl:'/search/lastname',
                            type:"GET",
                            buildSelect: function(data) {
                                var response =  jQuery.parseJSON(data); //JSON data
                                var s = '<select>';
                                if (response && response.length) {
                                    s += '<option hidden="true">--- Select Lastname ---</option>';
                                    for (var i = 0, l=response.length; i<l ; i++) {
                                    var id = response[i].id;
                                    var val = response[i].value;
                                    // You can concatenate ID or any other string here
                                    //For example: var ri = response[i].id + response[i].value;
                                        s += '<option value="'+id+'">'+val+'</option>';
                                    }
                                }
                                return s + "</select>";
                            }
                         }
                     },
                //     { label: 'Ciclo', name: 'code', width: 150, editable: true, formoptions: { colpos: 2, rowpos: 1 } },
              //       { label: 'Status', name: 'married', width: 150, align: "center", search: false, editable: true, edittype: "checkbox", formatter: "checkbox", editoptions: { value: "1:0" }, formoptions: { colpos: 2, rowpos: 2 } }
                 ],
                 width: 400,
                 height:200,
                 rowNum: 10,
                 loadonce: true,
                  subGrid: true,
                 
                  subGridRowExpanded: function (subgridDivId, rowId) {
				$("#" + $.jgrid.jqID(subgridDivId)).html("<em>mais detalhes id=" + rowId + "</em>");
		     	},
                 viewrecords: true,
                 pager: "#jqGridPager"
             });

             $('#jqGrid').navGrid('#jqGridPager',
                 // The buttons to appear on the toolbar of the grid
                 { edit: true, add: true, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true },

                 // Options for the Edit Dialog
                 {
                     url: 'edit_localizacao.php',
                     editCaption: "The Edit Dialog",
                     template: template,
                     recreateForm: true,
					 checkOnUpdate : true,
					 checkOnSubmit : true,
					 beforeSubmit : function( postdata, form , oper) {
						 if(confirm('Are you sure you want to update this information?') ) {
							 // Do something
						 	 return [true,'/update'];
						 } else {
							return [false, 'Update failed!'];
						 }
					 },
                     afterSubmit: function () {
                         $(this).jqGrid("setGridParam", {datatype: 'json'});
                         return [true];
                     },
                     closeAfterEdit: true,
                     errorTextFormat: function (data) {
                         return 'Error: ' + data.responseText
                     }
                 },
                 // Options for the Add Dialog
                 {
                     url: '/insert',
                     addCaption: "Add Dialog",
                      template: template,
                     afterSubmit: function () {
                         $(this).jqGrid("setGridParam", {datatype: 'json'});
                         return [true];
                     },
                     closeAfterAdd: true,
                     recreateForm: true,
                     errorTextFormat: function (data) {
                         return 'Error: ' + data.responseText
                     }
                 },
                 // Options for the Delete Dialog
                 {
                     url: '/delete',
                     errorTextFormat: function (data) {
                         return 'Error: ' + data.responseText
                     }
             });
              $("#jqGrid").jqGrid('filterToolbar', { stringResult: true, searchOnEnter: true });
             $("#pdf").on("click", function(){
				$('#jqGrid').jqGrid('exportToPdf');
			});
			$("#excel").on("click", function(){
				$('#jqGrid').jqGrid('exportToExcel');
			});
			$("#csv").on("click", function(){
				$('#jqGrid').jqGrid('exportToCsv');
			});		
         });
      </script>
		</body>
 

 </html>

 <?php  } $stmt->close();?>
