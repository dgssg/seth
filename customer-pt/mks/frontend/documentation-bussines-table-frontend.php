<?php
include("database/database.php");

?>






                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Emissão</th>
                          <th>Validade</th>
                          <th>Visibilidade</th>
                         
                          <th>Atualização</th>
                          <th>Registro</th>
                           <th>Ação</th>
                        </tr>
                      </thead>


                      <tbody>
                         
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">

                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Novo Documento</h4>
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="backend/documentation-bussines-backend.php" method="post">

                       
 
                      

                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                          <div class="col-md-6 col-sm-6 ">
                            <input id="nome" class="form-control" type="text" name="nome"   >
                          </div>
                        </div>
                      
                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Emissão</label>
                          <div class="col-md-6 col-sm-6 ">
                            <input id="data_now" class="form-control" type="date" name="data_now"   >
                          </div>
                        </div>
                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Validade</label>
                          <div class="col-md-6 col-sm-6 ">
                            <input id="data_before" class="form-control" type="date" name="data_before"   >
                          </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" >Salvar</button>
                    </div>
        </form>
                  </div>
                </div>
              </div>
              <script language="JavaScript">
$(document).ready(function() {
    
    $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );


  // Define a URL da API que retorna os dados da query em formato JSON
  const apiUrl = 'table/table-search-documentation-bussines.php';

  

  // Usa a função fetch() para obter os dados da API em formato JSON
  fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
      // Mapeia os dados para o formato esperado pelo DataTables
      const novosDados = data.map(item => [
        ``,
        item.nome,
        item.data_now,
        item.data_before,
        item.view == "0" ? "Sim" : "Não",
        item.upgrade,
        item.reg_date,
     
  
                                  `   <a class="btn btn-app"   href="dropzone/bussines/${item.dropzone}" target="_blank"  onclick="new PNotify({
                                  title: 'Visualizar',
                                  text: 'Visualizar',
                                  type: 'info',
                                  styling: 'bootstrap3'
                                  });">
          <i class="fa fa-file"></i> Download
          </a>
                                  <a class="btn btn-app" href="documentation-bussines-edit?id=${item.id}" onclick=" new PNotify({
          title: 'Editar',
          text: 'Abrindo Edição',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-edit"></i> Editar
        </a>
        <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Deletar!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Deletando!',
              'Seu arquivo será excluído.',
              'success'
            ),
            window.location = 'backend/documentation-bussines-trash.php?id=${item.id}';
          }
        });">
          <i class="fa fa-trash"></i> Excluir
        </a>`
      ]);

      // Inicializa o DataTables com os novos dados
      const table = $('#datatable').DataTable();
      table.clear().rows.add(novosDados).draw();
      

      // Cria os filtros após a tabela ser populada
      $('#datatable').DataTable().columns([1, 2,3,4]).every(function(d) {
        var column = this;
        var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });

      var oTable = $('#datatable').dataTable();

// Avança para a próxima página da tabela
//oTable.fnPageChange('next');



    });


    // Armazenar a posição atual ao sair da página
$(window).on('beforeunload', function() {
  var table = $('#datatable').DataTable();
  var pageInfo = table.page.info();
  localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
});






});

                // Restaurar a posição ao retornar à página
                $(document).ready(function() {
                  var storedPosition = localStorage.getItem('paginationPosition');
                  if (storedPosition) {
                    var pageInfo = JSON.parse(storedPosition);
                    var table = $('#datatable').DataTable();
                    table.page(pageInfo.page).draw('page');
                  }
                });

</script>
