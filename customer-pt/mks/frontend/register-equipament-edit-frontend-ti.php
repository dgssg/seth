  <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
  <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");
$query = "SELECT equipamento_ti.contrato,equipamento_ti.vlr,equipamento_ti.nf_dropzone,instituicao.instituicao, instituicao_area.nome, equipamento_ti.id_fornecedor,equipamento_ti.data_calibration_end,equipamento_ti.id_calibration,equipamento_ti.data_calibration,equipamento_ti.upgrade,equipamento_ti.reg_date,equipamento_ti.obs,equipamento_familia_ti.anvisa,equipamento_ti.baixa,equipamento_ti.ativo,equipamento_ti.inventario,equipamento_ti.comodato,equipamento_ti.duplicidade,equipamento_ti.preventive,equipamento_ti.obsolescence,equipamento_ti.rfid,equipamento_ti.data_val,equipamento_ti.nf,equipamento_ti.data_fab,equipamento_ti.data_instal,equipamento_ti.serie,equipamento_ti.patrimonio,equipamento_ti.id, equipamento_familia_ti.nome, equipamento_familia_ti.modelo,equipamento_familia_ti.fabricante, equipamento_ti.codigo, instituicao_localizacao.nome FROM equipamento_ti   LEFT JOIN equipamento_familia_ti ON equipamento_familia_ti.id = equipamento_ti.id_equipamento_familia LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento_ti.id_instituicao_localizacao LEFT JOIN  fornecedor ON fornecedor.id = equipamento_ti.id_fornecedor INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento_ti.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($contrato,$vlr,$nf_dropzone,$unidade,$setor,$id_fornecedor,$data_calibration_end,$id_calibration,$data_calibration,$upgrade,$reg_date,$obs,$anvisa,$baixa,$ativo,$inventario,$comodato,$duplicidade,$preventive,$obsolecencia,$rfid,$data_val,$nf,$data_fab,$data_instal,$serie,$patrimonio,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }


}
?>
  <!-- 1 -->
  <link href="dropzone.css" type="text/css" rel="stylesheet" />

  <!-- 2 -->
  <script src="dropzone.min.js"></script>
  <!-- page content -->
  <div class="right_col" role="main">
      <div class="">
          <div class="page-title">
              <div class="title_left">
                  <h3> Alteração <small>Equipamento</small></h3>
              </div>


          </div>

          <div class="clearfix"></div>

          <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                  <div class="x_panel">
                      <div class="x_title">
                          <h2>Dados <small>Equipamento</small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                      aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item" href="#">Settings 1</a>
                                      <a class="dropdown-item" href="#">Settings 2</a>
                                  </div>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">

                          <form
                              action="backend/register-equipament-edit-dados-backend-ti.php?equipamento=<?php printf($codigoget); ?> "
                              method="post">

                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" value="<?php printf($nome); ?>" readonly="readonly"
                                          required="required" class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Modelo <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" value="<?php printf($modelo); ?>" readonly="readonly"
                                          required="required" class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Fabricante
                                      <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" value="<?php printf($fabricante); ?>" readonly="readonly"
                                          required="required" class="form-control ">
                                  </div>
                              </div>
                             
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"
                                          required="required" class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align"
                                      for="patrimonio">Patromonio <span>*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" id="patrimonio" name="patrimonio"
                                          value="<?php printf($patrimonio); ?>" class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="serie">N/S <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" id="serie" name="serie" value="<?php printf($serie); ?>"
                                          class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Data
                                      Instalação <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="date" id="data_instal" name="data_instal"
                                          value="<?php printf($data_instal); ?>" class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_fab">Fabricação
                                      <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" id="data_fab" name="data_fab"
                                          value="<?php printf($data_fab); ?>" class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_val">Garantia
                                      <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="date" id="data_val" name="data_val"
                                          value="<?php printf($data_val); ?>" class="form-control ">
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span
                                          class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>"
                                          class="form-control ">
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="vlr">Valor (R$) <span
                                          class="required"></span>
                                  </label>
                                  <div class="input-group col-md-6 col-sm-6">
                                      <input name="vlr" id="vlr" type="text"  value="<?php printf($vlr); ?>" class="form-control money2"
                                          onKeyPress="return(moeda(this,'.',',',event))">

                                  </div>
                              </div>


                              <div class="ln_solid"></div>

                              <label for="middle-name">Observação</label>

                              <textarea id="obs" class="form-control" name="obs" placeholder="<?php printf($obs); ?>"
                                  value="<?php printf($obs); ?>"><?php printf($obs); ?></textarea>



                              <div class="ln_solid"></div>
 
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align"
                                      for="fornecedor">Fornecedor<span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <select type="text" class="form-control has-feedback-right" name="fornecedor"
                                          id="fornecedor" placeholder="Fornecedor"
                                          value="<?php printf($id_fornecedor); ?>">
                                          <option value="">Selecione um fornecedor</option>

                                          <?php



										   $sql = "SELECT  id, empresa FROM fornecedor WHERE trash =1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_empresa,$empresa);
     while ($stmt->fetch()) {
     ?>
                                          <option value="<?php printf($id_empresa);?>	"
                                              <?php if($id_empresa==$id_fornecedor){ printf("selected");}?>>
                                              <?php printf($empresa);?> </option>
                                          <?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
                                      </select>
                                  </div>
                              </div>









                              <script>
                              $(document).ready(function() {
                                  $('#fornecedor').select2();
                              });
                              </script>

                              <div class="item form-group">
                                  <label for="middle-name"
                                      class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input readonly="readonly" type="text" class="form-control"
                                          value="<?php printf($reg_date); ?> ">
                                  </div>
                              </div>

                              <div class="item form-group">
                                  <label for="middle-name"
                                      class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input readonly="readonly" type="text" class="form-control"
                                          value="<?php printf($upgrade); ?> ">
                                  </div>
                              </div>

                              <div class="form-group row">
                                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                                  <div class="col-md-3 col-sm-3 ">
                                      <center>
                                          <button class="btn btn-sm btn-success" type="submit"
                                              onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });">Salvar
                                              Informações</button>
                                      </center>
                                  </div>
                              </div>


                          </form>


                      </div>
                  </div>
              </div>
          </div>
          <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                  <div class="x_panel">
                      <div class="x_title">
                          <h2>Dados <small>Localização</small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                      aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item" href="#">Settings 1</a>
                                      <a class="dropdown-item" href="#">Settings 2</a>
                                  </div>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">



                          <div class="item form-group">
                              <label class="col-form-label col-md-3 col-sm-3 label-align" for="localizacao">Localização
                                  <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 ">
                                  <input type="text" id="localizacao" name="localizacao"
                                      value="<?php printf($unidade); ?> - <?php printf($setor); ?> - <?php printf($localizacao); ?>"
                                      readonly="readonly" required="required" class="form-control ">
                              </div>
                          </div>


                          <div class="form-group row">
                              <label class="col-form-label col-md-3 col-sm-3 "></label>
                              <div class="col-md-3 col-sm-3 ">
                                  <center>
                                      <button class="btn btn-sm btn-info" data-toggle="modal"
                                          data-target=".bs-example-modal-lg5">Alterar</button>
                                  </center>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
          </div>

          <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                  <div class="x_panel">
                      <div class="x_title">
                          <h2>Dados <small>Parametrização</small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                      aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item" href="#">Settings 1</a>
                                      <a class="dropdown-item" href="#">Settings 2</a>
                                  </div>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">

                          <form
                              action="backend/register-equipament-edit-parameter-backend-ti.php?equipamento=<?php printf($codigoget); ?>"
                              method="post">




                              <div class="item form-group">
                                  <label for="middle-name"
                                      class="col-form-label col-md-3 col-sm-3 label-align">Baixa</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input readonly="readonly" type="text" class="form-control"
                                          value="<?php if($baixa == "0"){printf("Sim"); }?>">
                                  </div>
                              </div>

                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">Ativo</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="ativo" type="checkbox" class="js-switch"
                                                  <?php if($ativo == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">Inventario</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="inventario" type="checkbox" class="js-switch"
                                                  <?php if($inventario == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">Comodato</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="comodato" type="checkbox" class="js-switch"
                                                  <?php if($comodato == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">Duplicidade</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="duplicidade" type="checkbox" class="js-switch"
                                                  <?php if($duplicidade == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
                                                           <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">Depreciação</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="obsolecencia" type="checkbox" class="js-switch"
                                                  <?php if($obsolecencia == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
                               
                            <div class="item form-group">
                              <label class="col-form-label col-md-3 col-sm-3 label-align">Contrato</label>
                              <div class="col-md-6 col-sm-6 ">
                                <div class="">
                                  <label>
                                    <input name="contrato" type="checkbox" class="js-switch"
                                      <?php if($contrato == "0"){printf("checked"); }?> />
                                  </label>
                                </div>
                              </div>
                            </div>

                              <div class="form-group row">
                                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                                  <div class="col-md-3 col-sm-3 ">
                                      <center>
                                          <button class="btn btn-sm btn-success" type="submit"
                                              onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });">Salvar
                                              Informações</button>
                                      </center>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>


          <div class="clearfix"></div>

          <div class="clearfix"></div>
        
        <div class="x_panel">
          <div class="x_title">
            <h2>Inserir/Substituir NF</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                  aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            
            <form class="dropzone"
              action="backend/regdate-equipament-edit-nf-change-dropzone-upload-backend.php?id=<?php printf($id); ?>"
              method="post">
            </form>
            
            <form action="backend/equipament-edit-nf-upload-backend-ti.php?id=<?php printf($codigoget); ?>"
              method="post">
              <center>
                <button class="btn btn-sm btn-success" type="submit">Atualizar NF</button>
              </center>
            </form>
            
          </div>
        </div>
        

          <div class="clearfix"></div>

          <div class="row">
              <div class="col-md-12">
                  <div class="x_panel">
                      <div class="x_title">
                          <h2>Anexo Nota Fiscal <small> </small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                      aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item" href="#">Settings 1</a>
                                      <a class="dropdown-item" href="#">Settings 2</a>
                                  </div>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                          </ul>
                          <div class="clearfix"></div>
                      </div>
                      <div class="x_content">

                          <div class="row">
                              <div class="col-md-55">
                                  <?php if($nf_dropzone ==! ""){ ?>
                                  <div class="thumbnail">
                                      <div class="image view view-first">
                                          <img style="width: 100%; display: block;"
                                              src="dropzone/nf/<?php printf($nf_dropzone); ?>" alt="image" />
                                          <div class="mask">
                                              <p>Anexo</p>
                                              <div class="tools tools-bottom">
                                                  <a href="dropzone/nf/<?php printf($nf_dropzone); ?>" download><i
                                                          class="fa fa-download"></i></a>

                                              </div>
                                          </div>
                                      </div>
                                      <div class="caption">
                                          <p>Anexo Nota Fiscal</p>
                                      </div>
                                  </div>
                                  <?php } ?>
                              </div>







                          </div>
                      </div>
                  </div>
              </div>
          </div>


          <div class="clearfix"></div>

          <!-- Posicionamento -->

          <div class="x_panel">
              <div class="x_title">
                  <h2>Anexos Gerais</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal"
                              data-target=".bs-example-modal-lg2"><i class="fa fa-plus"></i></a>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                              aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                          </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">

                  <!--arquivo -->
                  <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_equipament_dropzone WHERE id_equipamento like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
                  <table class="table table-striped">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>#</th>
                              <th>Titulo</th>
                              <th>Registro</th>
                              <th>Atualização</th>
                              <th>Ação</th>

                          </tr>
                      </thead>
                      <tbody>
                          <?php  while ($stmt->fetch()) { ?>

                          <tr>
                              <th scope="row"><?php printf($row); ?></th>
                              <td><?php printf($id); ?></td>
                              <td><?php printf($titulo); ?></td>
                              <td><?php printf($reg_date); ?></td>
                              <td><?php printf($upgrade); ?></td>
                              <td> <a class="btn btn-app" href="dropzone/equipamento-dropzone/<?php printf($file); ?> "
                                      target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Anexo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                                      <i class="fa fa-file"></i> Anexo
                                  </a>
                                  <a class="btn btn-app" onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/equipamento-dropzone-edit-trash-ti.php?id=<?php printf($id); ?>&equipamento=<?php printf($codigoget); ?> ';
  }
})
">
                                      <i class="fa fa-trash"></i> Excluir
                                  </a>
                              </td>

                          </tr>
                          <?php  $row=$row+1; }
}   ?>
                      </tbody>
                  </table>




              </div>
          </div>

          <!-- Registro -->
          <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                  <div class="modal-content">

                      <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                      </div>

                      <div class="modal-body">

                          <!-- Registro forms-->
                          <form
                              action="backend/regdate-equipament-dropzone-backend-ti.php?id_equipamento=<?php printf($codigoget);?>"
                              method="post">
                              <div class="ln_solid"></div>


                              <div class="field item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                                          class="required">*</span></label>
                                  <div class="col-md-6 col-sm-6">
                                      <input class="form-control" class='date' type="text" name="titulo"
                                          required='required'>
                                  </div>
                              </div>




                              <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                  <button type="submit" class="btn btn-primary">Salvar Informações</button>

                              </div>
                      </div>
                      </form>
                      <form class="dropzone" action="backend/regdate-equipament-dropzone-upload-backend.php"
                          method="post">
                      </form>
                  </div>
              </div>
          </div>
          <!-- Registro -->

         
          <div class="clearfix"></div>



          <div class="x_panel">
              <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                              aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                          </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">

                  <a class="btn btn-app" href="register-equipament">
                      <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>

                 
 

              </div>
          </div>




      </div>
  </div>
  <!-- Fechamento -->
  <div class="modal fade bs-example-modal-lg5" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">

              <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel4">Alterar</h4>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
              </div>
              <div class="modal-body">
                  <h4>Alterar Localização</h4>
                  <!-- Registro forms-->
                  <form
                      action="backend/register-equipament-update-location-ti.php?equipamento=<?php printf($codigoget); ?>"
                      method="post">
                      <div class="ln_solid"></div>

                      <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="localizacao">Localização
                              <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <input type="text" id="localizacao" name="localizacao"
                                  value="<?php printf($localizacao); ?>" readonly="readonly" required="required"
                                  class="form-control ">
                          </div>
                      </div>

                      <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-left" name="instituicao"
                                  id="instituicao" style="width:350px;" placeholder="Unidade" required="required">
                                  <option> Selecione uma unidade</option>
                                  <?php



                                           $result_cat_post  = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
                              }
                         ?>

                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#instituicao').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>



                      <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="setor">Setor <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-right" name="area" id="area"
                                  style="width:350px;" placeholder="Area">
                                  <option value="">Selecione um Setor</option>
                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#area').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>

                      <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="area">Area <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-right" name="setor" id="setor"
                                  style="width:350px;" placeholder="Area" required>
                                  <option value="">Selecione uma Area</option>
                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#setor').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>
                         <div class="ln_solid"></div>
   <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Data
                                      Alteração <span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <input type="date" id="data_location" name="data_location"
                                          class="form-control ">
                                  </div>
                              </div>

                      <div class="ln_solid"></div>

                      <label for="obs">Justificativa:</label>
                      <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                          data-parsley-validation-threshold="10"></textarea>

                      <div class="ln_solid"></div>

                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                      </div>

              </div>
          </div>
      </div>
      </form>
  </div>
  <!-- Fechamen    to -->
  <script type="text/javascript">
$(document).ready(function() {
    $('#instituicao').change(function() {
        $('#area').select2({
            dropdownParent: $("#myModal")
        }).load('sub_categorias_post.php?instituicao=' + $('#instituicao').val());
    });
});
  </script>
  <script type="text/javascript">
$(document).ready(function() {
    $('#area').change(function() {
        $('#setor').select2({
            dropdownParent: $("#myModal")
        }).load('sub_categorias_post_setor.php?area=' + $('#area').val());
    });
});
  </script>

  <!-- compose -->
  <script src="https://assets.locawti.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

  <script src="https://assets.locawti.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
  <script type="text/javascript">
$("meuPrimeiroDropzone").dropzone({
    url: "upload.php"
});
Dropzone.options.meuPrimeiroDropzone = {
    paramName: "fileToUpload",
    dictDefaultMessage: "Arraste seus arquivos para cá!",
    maxFilesize: 300,
    accept: function(file, done) {
        if (file.name == "olamundo.png") {
            done("Arquivo não aceito.");
        } else {
            done();
        }
    }
}
  </script>


  <script language="javascript">
function moeda(a, e, r, t) {
    let n = "",
        h = j = 0,
        u = tamanho2 = 0,
        l = ajd2 = "",
        o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.FROMCharCode(o),
        -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
        h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
    ;
    for (l = ""; h < u; h++)
        -
        1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
        0 == (u = l.length) && (a.value = ""),
        1 == u && (a.value = "0" + r + "0" + l),
        2 == u && (a.value = "0" + r + l),
        u > 2) {
        for (ajd2 = "",
            j = 0,
            h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
                j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
            tamanho2 = ajd2.length,
            h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
  </script>