  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Biblioteca</h3>
              </div>


            </div>
    <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                                                 
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                  <a class="btn btn-app <?php echo $current_page == 'documentation' ? 'active' : ''; ?>" href="documentation">
                   <i class="fa fa-code"></i> Sistema
                 </a>

              
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-pop' ? 'active' : ''; ?>" href="documentation-pop">

                   <i class="fa fa-book"></i> POP
                 </a>
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-update' ? 'active' : ''; ?>" href="documentation-update">

                   <i class="fa fa-history"></i> Atualização
                 </a>
              
                                                 
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-label' ? 'active' : ''; ?>" href="documentation-label">

                   <i class="fa fa-fax"></i> Rotuladora
                 </a>
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-bussines' ? 'active' : ''; ?>" href="documentation-bussines">

                    <i class="fa fa-folder-open"></i> Documentação
                  </a>
                
                                    
               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-bell' ? 'active' : ''; ?>" href="documentation-bell">

                    <i class="fa fa-bell"></i> Central de Notificações
                  </a>
                
                                                
                                  <a class="btn btn-app <?php echo $current_page == 'documentation-library' ? 'active' : ''; ?>" href="documentation-library">

                    <i class="fa fa-book"></i> Biblioteca
                  </a> 
               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-connectivity' ? 'active' : ''; ?>" href="documentation-connectivity">

                    <i class="fa fa-code-fork"></i> Conectividade &amp; Recursos
                  </a>
                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-event-task' ? 'active' : ''; ?>" href="documentation-event-task">

                    <i class="fa fa-tasks"></i> Eventos &amp; Taferas
                  </a>                 
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-widget' ? 'active' : ''; ?>" href="documentation-widget">

                    <i class="fa fa-desktop"></i> Widget
                  </a>





                </div>
              </div>

 

                <div class="x_panel">
                       <div class="x_title">
                         <h2>Acervo Digital</h2>
                         <ul class="nav navbar-right panel_toolbox">
                           <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                           </li>
                           <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                 class="fa fa-wrench"></i></a>
                             <ul class="dropdown-menu" role="menu">
                               <li><a href="#">Settings 1</a>
                               </li>
                               <li><a href="#">Settings 2</a>
                               </li>
                             </ul>
                           </li>
                           <li><a class="close-link"><i class="fa fa-close"></i></a>
                           </li>
                         </ul>
                         <div class="clearfix"></div>
                       </div>
                       <div class="x_content">



                 <!-- PAGE CONTENT BEGINS -->

       								<div class="error-container">
       									<div class="well">
       										<h1 class="grey lighter smaller">
       											<span class="blue bigger-125">
       												<i class="ace-icon fa fa-random"></i>
       												Desenvolvimento
       											</span>
       											<br>
       										Recurso ainda indisponível nessa versão!
       										</h1>

       										<hr />
       										<h3 class="lighter smaller">
       											Mas estamos trabalhando
       											<i class="ace-icon fa fa-wrench icon-animated-wrench bigger-125"></i>
       											sobre ele!
       										</h3>

       										<div class="space"></div>

       										<div>
       											<h4 class="lighter smaller">Enquanto isso, tente um dos seguintes:</h4>

       											<ul class="list-unstyled spaced inline bigger-110 margin-15">
       												<li>
       													<i class="ace-icon fa fa-hand-o-right blue"></i>
       												Leia o documento
       												</li>

       												<li>
       													<i class="ace-icon fa fa-hand-o-right blue"></i>
       													Veja as ferramentas!
       												</li>

       												<li>
       													<i class="ace-icon fa fa-hand-o-right blue"></i>
       													Veja os avisos de atualização!
       												</li>
       											</ul>
       										</div>

       										<hr />
       										<div class="space"></div>

       										<div class="center">


       											<a href="dashboard" class="btn btn-primary">
       												<i class="ace-icon fa fa-tachometer"></i>
       												Dashboard
       											</a>
       										</div>
       									</div>
       								</div>

       								<!-- PAGE CONTENT ENDS -->




                       </div>
                     </div>



                    <!-- page content -->


















               </div>
                   </div>
               <!-- /page content -->

           <!-- /compose -->


        <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
         
