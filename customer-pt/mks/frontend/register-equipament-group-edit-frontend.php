<?php
include("database/database.php");
$codigoget = ($_GET["id"]);
$pagina = ($_GET["pagina"]);


$query = "SELECT id, nome, upgrade, reg_date,yr,id_class,id_categoria,id_mp_group,id_procedimento_1,id_procedimento_2,id_periodicidade,id_time,id_colaborador,id_pop,accessories FROM equipamento_grupo WHERE id = $codigoget";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id,$nome,$upgrade,$regdate,$yr,$id_class,$id_categoria,$id_mp_group,$id_procedimento_1,$id_procedimento_2,$id_periodicidade,$id_time,$id_colaborador,$id_pop,$accessories);
   while ($stmt->fetch()) {
   }
}


?>
  <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
  <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
  
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Editar <small>Grupo Equipamento</small></h3>
              </div>


            </div>

             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar<small>Grupo Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<form  action="backend/register-equipament-group-edit-backend.php?id=<?php printf($codigoget);?>" method="post">

<div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="nome" class="form-control" name="nome" type="text" value="<?php printf($nome); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estimativa de Vida</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="yr" class="form-control" name="yr" type="number" value="<?php printf($yr); ?>" >
                        </div>
                      </div>
 
   <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria</label>*
	     <div class="col-md-4 col-sm-4 ">
										<select type="text" class="form-control has-feedback-right" name="id_categoria" id="id_categoria"  placeholder="Categoria" >
										<option value="" disabled selected>Selecione uma Categoria</option>
										  	<?php



										   $sql = " SELECT id, nome FROM category WHERE ativo like '0' and trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	" <?php if($id == $id_categoria){ printf("selected"); } ?> ><?php printf($nome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a categoria "></span>

													</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#id_categoria').select2();
                                      });
                                 </script>
                                                        


															 <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">POP</label>
	     <div class="col-md-4 col-sm-4 ">
															 <select type="text" class="form-control has-feedback-right" name="id_pop" id="id_pop"  placeholder="pop" value="">
															 <option value="">Selecione um pop</option>
															 <?php





															 $sql = "SELECT  id, titulo  FROM documentation_pop ";


															 if ($stmt = $conn->prepare($sql)) {
															 $stmt->execute();
															 $stmt->bind_result($id,$titulo);
															 while ($stmt->fetch()) {
															 ?>
															 <option value="<?php printf($id);?>	" <?php if($id == $id_pop){ printf("selected");};?> <?php if($id == $id_pop){ printf("selected"); } ?>>  <?php printf($titulo);?>	</option>
															 <?php
															 // tira o resultado da busca da memória
															 }

															 						}
															 $stmt->close();

															 ?>
															 				</select>

															 </div>
															</div>

															 <script>
															 		$(document).ready(function() {
															 		$('#id_pop').select2();
															 			});
															  </script>
															       <div class="item form-group">
                                  <label class="col-form-label col-md-3 col-sm-3 label-align">Acessórios</label>
                                  <div class="col-md-6 col-sm-6 ">
                                      <div class="">
                                          <label>
                                              <input name="accessories" type="checkbox" class="js-switch"
                                                  <?php if($accessories == "0"){printf("checked"); }?> />
                                          </label>
                                      </div>
                                  </div>
                              </div>
  
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                       <input readonly="readonly" type="text"  class="form-control"  value="<?php printf($regdate); ?> ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                       <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                        </div>
                      </div>






			






										
											<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar Informações"/>

												</form>
														<div class="ln_solid"></div>



                  </div>
                </div>
              </div>
	       </div>

        





	  <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament-group?pagina=<?php printf($pagina); ?>">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                 
                </div>
              </div>




                </div>
              </div>
