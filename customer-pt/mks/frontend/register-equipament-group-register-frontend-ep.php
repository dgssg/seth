<div class="right_col" role="main">

    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>Grupo de Equipamento</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                    <div class="input-group">

                        <span class="input-group-btn">

                        </span>
                    </div>
                </div>
            </div>
        </div>
      <div class="x_panel">
        <div class="x_title">
          <h2>Menu</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          
          <a class="btn btn-app"  href="register-equipament-group-ep">
            <i class="glyphicon glyphicon-list-alt"></i> Grupo
          </a>
          <a class="btn btn-app"  href="register-equipament-group-register-ep">
            <i class="glyphicon glyphicon-plus"></i> Cadastro
          </a>
        
          
          
          
          
        </div>
      </div>

        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">



                <form action="backend/register-equipament-group-single-backend-ep.php" method="post">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span
                                class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" id="nome" name="nome" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Estimativa de Vida <span
                                class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="number" id="yr" name="yr" class="form-control">
                        </div>
                    </div>
                  
                                    <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria</label>*
	     <div class="col-md-4 col-sm-4 ">
										<select type="text" class="form-control has-feedback-right" name="id_categoria" id="id_categoria"  placeholder="Categoria" >
										<option value="" disabled selected>Selecione uma Categoria</option>
										  	<?php



										   $sql = " SELECT id, nome FROM category WHERE ativo like '0' and trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	" <?php if($id == $id_categoria){ printf("selected"); } ?> ><?php printf($nome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a categoria "></span>

													</div>
								      	</div>

								 <script>
                                    $(document).ready(function() {
                                    $('#id_categoria').select2();
                                      });
                                 </script>
                                                        


															 <div class="item form-group">
	     <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">POP</label>
	     <div class="col-md-4 col-sm-4 ">
															 <select type="text" class="form-control has-feedback-right" name="id_pop" id="id_pop"  placeholder="pop" value="">
															 <option value="">Selecione um pop</option>
															 <?php





															 $sql = "SELECT  id, titulo  FROM documentation_pop ";


															 if ($stmt = $conn->prepare($sql)) {
															 $stmt->execute();
															 $stmt->bind_result($id,$titulo);
															 while ($stmt->fetch()) {
															 ?>
															 <option value="<?php printf($id);?>	" <?php if($id == $id_pop){ printf("selected");};?> <?php if($id == $id_pop){ printf("selected"); } ?>>  <?php printf($titulo);?>	</option>
															 <?php
															 // tira o resultado da busca da memória
															 }

															 						}
															 $stmt->close();

															 ?>
															 				</select>

															 </div>
															</div>

															 <script>
															 		$(document).ready(function() {
															 		$('#id_pop').select2();
															 			});
															  </script>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar Informações</button>
                    </div>
                </form>




            </div>
        </div>



        <!-- page content -->




</div>
</div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
