   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
  <?php

// Create connection
setlocale(LC_ALL, 'pt_BR');

$query = "SELECT purchases.descr, purchases.id_purchases_category,purchases.id_fornecedor,purchases.is_category,purchases.id_solicitante, purchases.id_area,purchases.id_setor,purchases.id_unidade,purchases.id_equipamento,purchases.id_familia,purchases.id_grupo,purchases.technique,purchases.justification,purchases.vlr_nf,purchases.nf,purchases.frete,purchases.protocolo,purchases.data_e,purchases.data_a,purchases.data_b,purchases.data_c,purchases.data_d,purchases.id_status_purchases,purchases.id_purchases_category,purchases.id_fornecedor,category.id,purchases.obs,purchases.id_os, category.nome, fornecedor.empresa, purchases_category.nome, purchases.solicitacao, purchases_status.status,purchases.reg_date FROM purchases LEFT join category on category.id = purchases.is_category LEFT join fornecedor on fornecedor.id = purchases.id_fornecedor LEFT join purchases_category on purchases_category.id = purchases.id_purchases_category LEFT join purchases_status on purchases_status.id = purchases.id_status_purchases WHERE purchases.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($des,$id_purchases_category,$id_fornecedor,$is_category,$id_solicitante,$id_area,$id_setor,$id_unidade,$id_equipamento,$id_familia,$id_grupo,$technique,$justification,$vlr_nf,$nf,$frete,$protocolo,$data_e,$data_a,$data_b,$data_c,$data_d,$id_purchases_status,$id_purchases_category_query,$id_fornecedor_query,$is_category_id,$obs,$id_os, $is_category2, $id_fornecedor2, $id_purchases_category2, $solicitacao, $id_status_purchases,$reg_date);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }
 

}

  $vlr_nf = number_format($vlr_nf, 2, '.', ','); // Formata o valor como moeda (2 casas decimais, vírgula como separador decimal, ponto como separador de milhar)

$div_parametro = ($_GET["div_parametro"]);
$div_anexo = ($_GET["div_anexo"]);
$div_pagamentos = ($_GET["div_pagamentos"]);
$div_itens = ($_GET["div_itens"]);
$sweet_salve = ($_GET["sweet_salve"]);
$sweet_delet = ($_GET["sweet_delet"]);


if ($div_parametro == 1) {
  // Redirecionamento para a âncora "registro" usando JavaScript
  echo '<script>
      window.onload = function() {
          window.location.href = "#tag_parametro";
      };
  </script>';
      
  }

  if ($div_anexo == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        window.onload = function() {
            window.location.href = "#tag_anexo";
        };
    </script>';
        
    }

    if ($div_pagamentos == 1) {
      // Redirecionamento para a âncora "registro" usando JavaScript
      echo '<script>
          window.onload = function() {
              window.location.href = "#tag_pagamentos";
          };
      </script>';
          
      }

      if ($div_itens == 1) {
        // Redirecionamento para a âncora "registro" usando JavaScript
        echo '<script>
            window.onload = function() {
                window.location.href = "#tag_itens";
            };
        </script>';
            
        }
        if ($sweet_delet == 1) {
          // Redirecionamento para a âncora "registro" usando JavaScript
          echo '<script>
          Swal.fire({
              position: \'top-end\',
              icon: \'success\',
              title: \'Seu trabalho foi deletado!\',
              showConfirmButton: false,
              timer: 1500
          });
      </script>';
      
              
          }
          if ($sweet_salve == 1) {
              // Redirecionamento para a âncora "registro" usando JavaScript
              echo '<script>
              Swal.fire({
                  position: \'top-end\',
                  icon: \'success\',
                  title: \'Seu trabalho foi salvo!\',
                  showConfirmButton: false,
                  timer: 1500
              });
          </script>';
          
                  
              }
?>
 
 <div class="x_panel" id="tag_parametro">
                <div class="x_title">
                  <h2>Parâmetro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                       <form  action="backend/purchases-progress-edit-upgrade-open-backend.php?id=<?php printf($codigoget); ?> " method="post">
                   
                     
                      
                                    

    


                      <label for="solicitacao">Solicita&#231;&#227;o  :</label>
                          <textarea id="solicitacao" class="form-control" name="solicitacao" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($solicitacao); ?>" value="<?php printf($solicitacao); ?>"><?php printf($solicitacao); ?></textarea>

                            <label for="justification">Justificativa :</label>
                          <textarea id="justification" class="form-control" name="justification" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($justification); ?>" value="<?php printf($justification); ?>"><?php printf($justification); ?></textarea>

                            <label for="technique">Observação Técnica MP :</label>
                          <textarea id="technique" class="form-control" name="technique" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($technique); ?>" value="<?php printf($technique); ?>"><?php printf($technique); ?></textarea>


                            <label for="des">Descrição :</label>
                          <textarea id="des" class="form-control" name="des" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($des); ?>" value="<?php printf($des); ?>"><?php printf($des); ?></textarea>


                            <div class="ln_solid"></div>

                            <div class="item form-group">
             <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria</label>
                        <div class="col-md-4 col-sm-4 ">
										<select type="text" class="form-control has-feedback-right" name="categoria" id="categoria"  placeholder="Categoria">
										<option value="">Selecione uma Categoria</option>
										  	<?php
										  	
										  
										  	
										   $sql = " SELECT id, nome FROM category WHERE ativo like '0'  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"<?php if($id == $is_category){ printf("selected"); } ?> ><?php printf($nome);?>  	</option>
										 
                     <?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a categoria "></span>
										
										</div>
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#categoria').select2();
                                      });
                                 </script>
                      
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Fornecedor <span></span>
                        </label>
                        
                        <div class="col-md-4 col-sm-4 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_fornecedor" id="id_fornecedor" >
										   <option value="	">Selecione um fornecedor</option>
										   
										  
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  id, empresa FROM fornecedor  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$empresa);
     while ($stmt->fetch()) {
     ?>
								  	
<option value="<?php printf($id);?>	"<?php if($id == $id_fornecedor){ printf("selected"); } ?> ><?php printf($empresa);?>  	</option>
                       
                        <?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                         
								 <script>
                                    $(document).ready(function() {
                                    $('#id_fornecedor').select2();
                                      });
                                 </script>
                      <div class="item form-group">
             <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria de Manutenção</label>
                        <div class="col-md-4 col-sm-4 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_purchases_category" id="id_purchases_category" >
										   <option value="	">Selecione uma categoria</option>
										   
										  
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  id, nome FROM purchases_category  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
 
<option value="<?php printf($id);?>	"<?php if($id == $id_purchases_category){printf("selected");};?> ><?php printf($nome);?>  	</option>
									  
                      <?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                         
								 <script>
                                    $(document).ready(function() {
                                    $('#id_purchases_category').select2();
                                      });
                                 </script>


                            <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Solicitante ">Solicitante <span class="required" >*</span>
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="id_solicitante" id="id_solicitante"  placeholder="Solicitante"  >
        <option value="	">Selecione o Solicitante</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	"<?php if($id == $id_solicitante){ printf("selected"); } ?> ><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_solicitante').select2();
  });
  </script> 
                         
                          <?php
                            
                            
                            if( $id_unidade !== "" &&  $id_unidade !== null ){ 

                              
                            $sql = "SELECT id, instituicao FROM instituicao WHERE id = $id_unidade";
                            
                            
                            if ($stmt = $conn->prepare($sql)) {
                              $stmt->execute();
                              $stmt->bind_result($id,$instituicao);
                              while ($stmt->fetch()) {
                              }
                            }
                            }
                          ?>
                          
                          
                          
                          <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Unidade <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                              <input type="text"  value="<?php printf($instituicao); ?>" readonly="readonly" required="required" class="form-control ">
                            </div>
                          </div>

                          
                          <?php
                            
                            
                            if( $id_setor !== "" &&  $id_setor !== null  ){ 
                            $sql = "SELECT id, nome FROM instituicao_area WHERE id = $id_setor";
                            
                            
                            if ($stmt = $conn->prepare($sql)) {
                              $stmt->execute();
                              $stmt->bind_result($id,$instituicao_area);
                              while ($stmt->fetch()) {
                              }
                            }
                            }
                          ?>
                          
                          
                          
                          <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Setor <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                              <input type="text"  value="<?php printf($instituicao_area); ?>" readonly="readonly" required="required" class="form-control ">
                            </div>
                          </div>
                          
                          <?php
                            
                            
                            if( $id_area !== "" &&  $id_area !== null  ){ 
                            $sql = "SELECT id, nome FROM instituicao_localizacao WHERE id = $id_area";                            
                            
                            if ($stmt = $conn->prepare($sql)) {
                              $stmt->execute();
                              $stmt->bind_result($id,$instituicao_localizacao);
                              while ($stmt->fetch()) {
                              }
                            }
                            }
                          ?>
                          
                          
                          
                          <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Area <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                              <input type="text"  value="<?php printf($instituicao_localizacao); ?>" readonly="readonly" required="required" class="form-control ">
                            </div>
                          </div>                   

                       <div class="item form-group">
             <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status da Solicitação</label>
                        <div class="col-md-4 col-sm-4 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_purchases_status" id="id_purchases_status" >
										   <option value="">Selecione uma Status</option>
										   
										  
										  	<?php
										  	
										  
										  	
										   $sql = "SELECT  id, status FROM purchases_status  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"<?php if($id==$id_purchases_status){printf("selected");}?>><?php printf($nome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
                        </div>
                      </div>
                      
                         
								 <script>
                                    $(document).ready(function() {
                                    $('#id_purchases_status').select2();
                                      });
                                 </script>
                   <div class="ln_solid"></div>
                    
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_a">Data de Envio para a Diretoria <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_a" name="data_a" value="<?php printf($data_a); ?>"   class="form-control ">
                        </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_b">Data de Entrada do Pedido de Compra <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_b" name="data_b" value="<?php printf($data_b); ?>"   class="form-control ">
                        </div>
                      </div>
                      
                    <!--   <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_c">Data de Entrada do Pedido de Compra <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_c" name="data_c" value="<?php printf($data_c); ?>"   class="form-control ">
                        </div>
                      </div> -->
                      
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_d">Data de Chegada<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_d" name="data_d" value="<?php printf($data_d); ?>"   class="form-control ">
                        </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_purchases">Data da Nota Fiscal<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="date" id="data_e" name="data_e" value="<?php printf($data_e); ?>"   class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="protocolo">Protocolo <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="protocolo" name="protocolo" value="<?php printf($protocolo); ?>"   class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="frete">Frete <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="frete" name="frete" value="<?php printf($frete); ?>"   class="form-control ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>"   class="form-control ">

                        </div>
                      </div>
                      
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="vlr_nf">Valor Nota Fiscal <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                           <input name="vlr_nf" id="vlr_nf" type="text"   value="<?php printf($vlr_nf); ?>"  class="form-control  " onKeyPress="return(moeda(this,'.',',',event))">
                           <span class="input-group-btn">
                                             <button type="button"  onclick="vlrprevision()" class="btn btn-primary">Valor Global</button>
                                         </span>
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>

                      <label for="technique">Observa&#231;&#227;o:</label>
                          <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($obs); ?>" value="<?php printf($obs); ?>"><?php printf($obs); ?></textarea>

                            <div class="ln_solid"></div>
                      
                      
                  
    
                   <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center> 
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>
     
     
                                  </form>
                  
                </div>
              </div>
              
         
    <div class="clearfix"></div>

         
 
     <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- Posicionamento -->
    
	         <div class="x_panel" id="tag_anexo">
                <div class="x_title">
                  <h2>Anexos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                <!--arquivo -->
       <?php    
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_purchases_dropzone WHERE id_purchases like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);
  
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                          <th>Titulo</th>
                          <th>Registro</th>
                          <th>Atualização</th>
                         <th>Ação</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id); ?></td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($upgrade); ?></td>
                         <td> <a class="btn btn-app"   href="dropzone/compras/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Anexo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Anexo
                  </a>
                  </a>
                               <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/purchases-progress-dropzone-trash-backend.php?id=<?php printf($id); ?>&purchases=<?php printf($codigoget); ?>';
  }
})
">
                                        <i class="fa fa-trash"></i> Excluir</a>
                </td>
                          
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

                
              
                
                </div>
              </div>        
            
              <!-- Posicionamento -->
	       
	          <!-- Registro -->
               <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        
                        <div class="modal-body">
                           
                        <!-- Registro forms-->
                          <form action="backend/purchases-dropzone-backend.php?id=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>
                            
                      
                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                          class="required"></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="titulo" required='required'></div>
                    </div>
                    
               
                        
                        
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                          
                        </div>
                 </div>
             </form>
              <form  class="dropzone" action="backend/purchases-dropzone-upload-backend.php" method="post">
    </form >
     </div>
                    </div>
                  </div>
              <!-- Registro -->	       
	       
	       
            <div class="clearfix"></div>



            <div class="x_panel" id="tag_pagamentos">
                <div class="x_title">
                  <h2>Pagamentos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg5" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                <!--arquivo -->
       <?php    
$query="SELECT id, payment, vlr, data_payment FROM purchases_payment WHERE id_purchases like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$payment,$vlr,$data_payment);
  
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Pagamento</th>
                          <th>Valor</th>
                          <th>Data</th>
                       
                         <th>Ação</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($payment); ?></td>
                          <td><?php printf($vlr); ?></td>
                          <td><?php printf($data_payment); ?></td>
                         
                         <td>                   </a>
                               <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/purchases-progress-payment-trash-backend.php?id=<?php printf($id); ?>&purchases=<?php printf($codigoget); ?>';
  }
})
">
                                        <i class="fa fa-trash"></i> Excluir</a> 
                            <a class="btn btn-app" onclick="
                              Swal.fire({
                                title: 'Editar Registro',
                                html: `<form id='editForm'>
                                                             
                                <label for='payment'> Data de Pagamento:</label>
                                <input type='text' class='swal2-input' id='payment' name='payment' value='<?php echo htmlspecialchars($payment); ?>'>
                                
                                
                                <label for='vlr'> Data de Valor:</label>
                                <input type='text' class='swal2-input' id='vlr' name='vlr' value='<?php echo htmlspecialchars($vlr); ?>'>
                                
                                <label for='data_payment'>Data:</label>
                                <input type='date' class='swal2-input' id='data_payment' name='data_payment' value='<?php echo htmlspecialchars($data_payment); ?>'>
                                
                             
                                
                                
                                </form>`,
                                
                                icon: 'info',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Salvar Alterações'
                              }).then((result) => {
                                if (result.isConfirmed) {
                                  // Obtenha os dados do formulário
                                  var formData = new FormData(document.getElementById('editForm'));
                                  
                                  var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                  
                                  
                                  // Exemplo de código AJAX
                                  $.ajax({
                                    url: 'backend/purchases-progress-payment-edit-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                    type: 'POST',
                                    data: formData,
                                    processData: false,
                                    contentType: false,
                                    success: function(response) {
                                      
                                      Swal.fire('Alterações Salvas!', '', 'success');
                                      // Adicione aqui qualquer ação adicional após o sucesso
                                      location.href = location.origin + location.pathname + urlParams;
                                      
                                    },
                                    error: function(error) {
                                      console.error('Erro ao salvar alterações:', error);
                                      Swal.fire('Erro ao salvar alterações!', '', 'error');
                                    }
                                  });
                                }
                              });
                            ">
                              <i class="fa fa-edit"></i> Editar
                            </a>
                            
                          
                          </td>
                          
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

                
              
                
                </div>
              </div>        
            
              <!-- Posicionamento -->
	       
	          <!-- Registro -->
               <div class="modal fade bs-example-modal-lg5" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel5">Pagamento</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        
                        <div class="modal-body">
                           
                        <!-- Registro forms-->
                          <form action="backend/purchases-payment-backend.php?id=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>
                            
                      
                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Pagamento<span
                          class="required"></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="payment" ></div>
                    </div>

                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Valor<span
                          class="required"></span></label>
                      <div class="col-md-6 col-sm-6">
                      <input name="vlr" id="vlr" type="text" class="form-control money2" oninput="formatarMoeda(this)">
                    </div>
                    </div>

                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Data<span
                          class="required"></span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="date" name="data_payment" ></div>
                    </div>
                    

               
                        
                        
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                          
                        </div>
                 </div>
             </form>
             
     </div>
                    </div>
                  </div>
              <!-- Registro -->	       
	       
	       
            <div class="clearfix"></div>


            <div class="clearfix"></div>

  
       
                
              <!-- Posicionamento -->
	       
 <div class="x_panel" id="tag_itens">
                <div class="x_title">
                  <h2>Itens</h2>
                  <ul class="nav navbar-right panel_toolbox">
                         <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                <!--arquivo -->
       <?php    
          $query="SELECT
          purchases_itens.id,
          GROUP_CONCAT(DISTINCT 
            CONCAT_WS(' ',  equipamento.codigo,equipamento_familia.nome, equipamento_familia.fabricante, equipamento_familia.modelo)
            ORDER BY equipamento_familia.nome, equipamento_familia.fabricante, equipamento_familia.modelo
            SEPARATOR '\n'
          ) AS equipamentos,
          purchases_itens.iten, purchases_itens.un, purchases_itens.qtd, purchases_itens.vlr 
          FROM
          purchases_itens
          LEFT JOIN
          equipamento ON FIND_IN_SET(equipamento.id, purchases_itens.id_equipamento) 
          LEFT JOIN 
          equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia
          WHERE
          purchases_itens.id_purchases = $codigoget
          GROUP BY
          purchases_itens.id;

";
$row=1;
$global=0;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $equipamento, $iten, $un, $qtd, $vlr  );
 
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                                                   <th>Iten</th>
                          <th>Unidade</th>
                          <th>Quantidade</th>
                          <th>Valor</th>
                          <th>Total</th>
                         
                          <th>Ação</th>
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { 
$vlr_print=$vlr ;?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                         
                          <td><?php printf($iten); ?></td>
                          <td><?php printf($un); ?></td>
                           <td><?php printf($qtd); ?></td>
                           <td > <input type="text"   value="<?php printf($vlr_print); ?>" readonly="readonly" required="required" class="form-control  "  onKeyPress="return(moeda(this,'.',',',event))"> </td>

<?php 
  $vlr = trim($vlr);
$vlr_print=$vlr ;
//$recovery=strlen($vlr);
//$recovery=$recovery;
$vlr_print = str_replace('.', '', $vlr_print);
$vlr_print = str_replace(',', '.', $vlr_print);
//$vlr_print = number_format($vlr_print, 2, '.', '');
//$vlr_print= str_pad($vlr_print, $recovery, '0');
$total=$qtd*$vlr_print; 
//$total= str_pad($total, $recovery, '0');
//$recovery=strlen($total);
//$total = substr($total, -$recovery);
//$total = substr_replace($total, ',', ($recovery-3), 0);
//$total=sprintf("%2.2f",$total); 


//if($qtd == 1){
 // $total=$vlr ;
//}
$global=$total+$global;
//$global= str_pad($global, $recovery, '0');
//$recovery=strlen($global);
//$global = substr_replace($global, ',', ($recovery-3), 0);
//$global=sprintf("%2.2f",$global); 
?>
  <td > 
    
  
    <input type="text"   value="<?php                       $formatted = number_format($total, 2, ',', '.');
      echo '' . $formatted.' ';//echo money_format("%i", $total);?>" readonly="readonly" required="required" class="form-control  "  onKeyPress="return(moeda(this,'.',',',event))"> </td>
     
                            <td>
                          <!--  <a class="btn btn-app"   onclick="open('purchases-acquisition-itens-edit-itens?id=<?php printf($id); ?>','purchases-acquisition-itens-edit-itens?<?php printf($id); ?>','status=no,Width=320,Height=285');new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>-->
                              <a class="btn btn-app" onclick="
                                Swal.fire({
                                  title: 'Editar Registro',
                                  html: `<form id='editForm'>
                              
                                  
                                  
                                  <label for='iten'> Iten:</label>
                                  <input type='text' class='swal2-input' id='iten' name='iten' value='<?php echo htmlspecialchars($iten); ?>'>
                                  
                                  
                                  <label for='un'> Unidade:</label>
                                  <input type='text' class='swal2-input' id='un' name='un' value='<?php echo htmlspecialchars($un); ?>'>
                                  
                                  <label for='qtd'>Quantidade:</label>
                                  <input type='text' class='swal2-input' id='qtd' name='qtd' value='<?php echo htmlspecialchars($qtd); ?>'>
                                  
                                  <label for='vlr_print'> Valor:</label>
                                  <input type='text' class='swal2-input' id='vlr_print' name='vlr_print' value='<?php echo htmlspecialchars($vlr_print); ?>'>
                                  
                                  
                                  </form>`,
                                  
                                  icon: 'info',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: 'Salvar Alterações'
                                }).then((result) => {
                                  if (result.isConfirmed) {
                                    // Obtenha os dados do formulário
                                    var formData = new FormData(document.getElementById('editForm'));
                                    
                                    var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                    
                                    
                                    // Exemplo de código AJAX
                                    $.ajax({
                                      url: 'backend/purchases-acquisition-itens-edit-itens-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                      type: 'POST',
                                      data: formData,
                                      processData: false,
                                      contentType: false,
                                      success: function(response) {
                                        
                                        Swal.fire('Alterações Salvas!', '', 'success');
                                        // Adicione aqui qualquer ação adicional após o sucesso
                                        location.href = location.origin + location.pathname + urlParams;
                                        
                                      },
                                      error: function(error) {
                                        console.error('Erro ao salvar alterações:', error);
                                        Swal.fire('Erro ao salvar alterações!', '', 'error');
                                      }
                                    });
                                  }
                                });
                              ">
                                <i class="fa fa-edit"></i> Editar
                              </a>
                              
                               <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/purchases-progress-itens-trash-backend.php?id=<?php printf($id); ?>&purchases=<?php printf($codigoget); ?>';
  }
})
">
                                        <i class="fa fa-trash"></i> Excluir</a> 
                                      </td>
                        </tr>
                   <?php  $row=$row+1; }}   ?>
                      </tbody>
                    </table>

                
              
                
                </div>
              </div>
          
              <!-- Posicionamento -->
               <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Adicionar Itens</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        
                        
                        <div class="modal-body">
                           <form action="backend/purchases-acquisition-itens-backend.php?purchases=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>
                              
                             
                           
                              
                            <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="iten">Iten<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input name="iten" id="iten" type="text" class="form-control ">
                        </div>
                      </div>  
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="un">Unidade<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input name="un" id="un" type="text" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="qtd">Quantidade<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input name="qtd" id="qtd" type="number" class="form-control ">
                        </div>
                      </div> 
                              <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="vlr">Pre&#231;o<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                  <input name="vlr" id="vlr" type="text" class="form-control money2" oninput="formatarMoeda(this)">
                                </div>
                              </div>
                              
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>
              <!-- Posicionamento -->	       
   
        <div class="x_panel">
                <div class="x_title">
                  <h2>Dados</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  
            <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">Valor Global R$<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="global" name="global"  value="<?php                        $formatted = number_format($global, 2, ',', '.');
                            echo '' . $formatted.' ';//echo money_format("%i", $global); ?>" readonly="readonly"  class="form-control  " onKeyPress="return(moeda(this,'.',',',event))">
                        </div>
                      </div>       
             
               
            
                  
                </div>
              </div>  
              <script>
function vlrprevision() {

  //document.getElementById("date_start").innerHTML ="2022-04-18";
  //  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
  //  $('#date_start').val(date_start);
   var global = document.getElementById("global").value;
//  var  date_start = +$('#programada_data').val(programada_data);
  $('#vlr_nf').val(global);
      //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



}
</script>       
              <script type="text/javascript">
    $(document).ready(function(){
      $('#instituicao').change(function(){
        $('#area').select2().load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#setor').select2().load('sub_categorias_post_setor.php?area='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_categorias_post_equipament_setor.php?equipamento='+$('#equipamento').val());
      });
    });
    </script>

    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_equipament_setor_area.php?setor='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#instituicao').select2().load('sub_categorias_post_equipament_setor_area_unidade.php?area='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#instituicao').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_unidade_equipament.php?instituicao='+$('#instituicao').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
      });
    });
    </script>
    <script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.FROMCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script> 
                          <script>
                            function formatarMoeda(input) {
                              // Obtém o valor do campo de entrada
                              let valor = input.value;
                              
                              // Remove caracteres não numéricos
                              valor = valor.replace(/[^\d]/g, '');
                              
                              // Converte para número
                              valor = parseFloat(valor) / 100;
                              
                              // Formata como moeda usando Intl.NumberFormat
                              valor = new Intl.NumberFormat('pt-BR', {
                                style: 'currency',
                                currency: 'BRL'
                              }).format(valor);
                              
                              // Atualiza o valor no campo de entrada
                              input.value = valor;
                            }
                    </script>