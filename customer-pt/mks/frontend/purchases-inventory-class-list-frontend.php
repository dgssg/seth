<?php
include("database/database.php");
$query="SELECT id, nome FROM purchases_class ";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$nome);

?>
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Nome</th>
                   <th>Ação</th>


                 </tr>
               </thead>
               <tbody>
                      <?php  while ($stmt->fetch()) { ?>

                 <tr>
                   <th scope="row"><?php printf($row); ?></th>
                   <td><?php printf($nome); ?></td>
                     
                       <td><a class="btn btn-app"  href="purchases-inventory-manager-class-edit?id=<?php printf($id); ?>"onclick="new PNotify({
                                      title: 'Atualização',
                                      text: 'Atualização!',
                                      type: 'danger',
                                      styling: 'bootstrap3'
                                  });">
                          <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                        </a>
                        <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/purchases-inventory-manager-class-trash.php?id=<?php printf($id); ?>';
  }
})
">
                           <i class="glyphicon glyphicon-trash"></i> Excluir
                         </a></td>
                     </td>
                 </tr>
            <?php  $row=$row+1; }
}   ?>
               </tbody>
             </table>







        <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
             <div class="modal-dialog modal-lg">
               <div class="modal-content">

                 <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Itens</h4>
                   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                   </button>
                 </div>
                 <div class="modal-body">
                    <form action="backend/purchases-inventory-class-backend.php" method="post">
                      <div class="ln_solid"></div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="nome" class="form-control" type="text" name="nome"   >
                        </div>
                      </div>










                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                   <button type="submit" class="btn btn-primary" onclick="new PNotify({
                             title: 'Registrado',
                             text: 'Informações registrada!',
                             type: 'success',
                             styling: 'bootstrap3'
                         });" >Salvar Informações</button>
                 </div>

               </div>
             </div>
           </div>
       </form>

       <!-- -->
