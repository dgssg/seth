<?php

$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query="SELECT purchases_material.purchases_species,purchases_material.purchases_class,purchases_material.purchases_subclass,purchases_material.id, purchases_species.nome, purchases_class.nome, purchases_subclass.nome, purchases_material.cod, purchases_material.nome, purchases_material.unidade, purchases_material.qt_min, purchases_material.qt_max, purchases_material.qt_now, purchases_material.vlr, purchases_material.upgrade, purchases_material.reg_date FROM purchases_material INNER JOIN purchases_species ON purchases_species.id = purchases_material.purchases_species INNER JOIN purchases_class ON purchases_class.id = purchases_material.purchases_class INNER JOIN purchases_subclass ON purchases_subclass.id = purchases_material.purchases_subclass WHERE purchases_material.id = $codigoget";
$row=1;
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id_purchases_species,$id_purchases_class,$id_purchases_subclass,$id,$purchases_species,$purchases_class,$purchases_subclass,$cod,$nome,$unidade,$qt_min,$qt_max,$qt_now,$vlr,$upgrade,$reg_date);

while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
?>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <div class="right_col" role="main">

        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Edição</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                <div class="input-group">

                  <span class="input-group-btn">

                  </span>
                </div>
              </div>
            </div>
          </div>


       <div class="x_panel">
              <div class="x_title">
                <h2>Alerta</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

           <div class="alert alert-success alert-dismissible " role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Area de Edição!</strong> Todo alteração não é irreversível.
                </div>



              </div>
            </div>



           <!-- page content -->









            <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edição <small> Material</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a class="dropdown-item" href="#">Settings 1</a>
                        </li>
                        <li><a class="dropdown-item" href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form action="backend/purchases-inventory-manager-material-edit-upgrade-backend.php" method="post">

                  <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                      </div>
                    </div>



                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Espécie <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="purchases_species" id="purchases_species" required="required" placeholder="Espécie">
                      <option value="">Selecione uma Espécie</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM purchases_species";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            if ($row_cat_post['id'] == $id_purchases_species) {
                            echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'</option>';
                            }
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>

                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Class <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="purchases_class" id="purchases_class" required="required" placeholder="Class">
                      <option value="">Selecione uma Class</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM purchases_class";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            if ($row_cat_post['id'] == $id_purchases_class) {
                            echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'</option>';
                            }
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Class "></span>

                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">SubClass <span class="required"></span>
                      </label>
                      <div class="input-group col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-left" name="purchases_subclass" id="purchases_subclass" required="required" placeholder="SubClass">
                      <option value="">Selecione uma SubClass</option>
                          <?php



                          $result_cat_post  = "SELECT  id, nome FROM purchases_subclass";

                          $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                            if ($row_cat_post['id'] == $id_purchases_subclass) {
                            echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'</option>';
                            }
                            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                          }
                          ?>

                        </select>
                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a SubClass "></span>

                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="cod" class="form-control" type="text" name="cod"  value="<?php printf($cod); ?>">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="nome" class="form-control" type="text" name="nome" value="<?php printf($nome); ?>" >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="unidade" class="form-control" type="text" name="unidade" value="<?php printf($unidade); ?>"  >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade Mínimo</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="qt_min" class="form-control" type="number" name="qt_min" value="<?php printf($qt_min); ?>"  >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade Máximo</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="qt_max" class="form-control" type="number" name="qt_max" value="<?php printf($qt_max); ?>" >
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade Atual</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="qt_now" class="form-control" type="number" name="qt_now"  value="<?php printf($qt_now); ?>">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor Atual (R$)</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="vlr" class="form-control" type="text" name="vlr"  value="<?php printf($vlr); ?>" >
                      </div>
                    </div>









                              <div class="form-group row">
                     <label class="col-form-label col-md-3 col-sm-3 "></label>
                      <div class="col-md-3 col-sm-3 ">
                          <center>
                         <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});">Salvar Informações</button>
                       </center>
                      </div>
                    </div>


                                </form>






                </div>
              </div>
            </div>
          </div>



           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="purchases-inventory-manager">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>




              </div>
            </div>




      </div>
          </div>
      <!-- /page content -->

  <!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
