<?php
include("database/database.php");
$unidade = trim(($_GET["unidade"]));
$obsolescence_year = trim(($_GET["yr"]));

?>


<!--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->



<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>



<link rel="stylesheet" href="https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.css">
<style>
  .main-container {
    width: 795px;
    margin-left: auto;
    margin-right: auto;
  }
</style>











<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Analise de Depreciação de Patrimônio</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


    
          <a class="btn btn-app" href="obsolescence-analysis">
            <i class="fa fa-table"></i> Mobiliário
          </a>
          <a class="btn btn-app" href="obsolescence-analysis-ec">
            <i class="fa fa-heartbeat"></i> Engenharia Clínica
          </a>
          <a class="btn btn-app" href="obsolescence-analysis-ep">
            <i class="fa fa-building"></i> Engenharia Predial/Infraestrutura
          </a>
          <a class="btn btn-app" href="obsolescence-analysis-ti">
            <i class="fa fa-desktop"></i> Tecnologia da Informação
          </a>
          <a class="btn btn-app" href="obsolescence-analysis-ht">
            <i class="fa fa-bed"></i> Hotelaria
          </a>





  </div>
</div>


<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Avaliação <small> do patrimônio</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="dropdown-item" href="#">Parametro 1</a>
              </li>
              <li><a class="dropdown-item" href="#">Parametro 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">




        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
              <div class="x_title">


                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
                <form  action="backend/obsolescence-analysis-backend.php?pt=1" method="post"  >

                <div class="ln_solid"></div>
 
                                 
                                                                <div class="field item form-group">
                                
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Equipamento<span
                       > </span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento" required>
										  <option value="">Selecione o equipamento</option>
                      <?php


					$query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.obsolescence = 0 and equipamento.trash != 0 and equipamento.ativo = 0";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($fabricante);?> -  <?php printf($modelo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
                                       	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>
                    <div name="check" id="check"></div>
                  </div>


								      		    
                  </div>
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento').select2();
                                      });
                                 </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="text" class="form-control has-feedback-right" name="yr" id="yr"  placeholder="equipamento" required>
                        <option value="">Selecione o ano</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_year";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $yr);
                          while ($stmt->fetch()) {
                            
                            ?>
                            <option value="<?php printf($id);?>	" <?php if($id == $obsolescence_year){printf("selected");} ?>	><?php printf($yr);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                      <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Ano "></span>

                    </div>
                                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#yr').select2();
                  });
                  </script>

                
                  <div >
                    <div >
                      <h1>Analise de Depreciação</h1>
                      <br>
                      <span class="input-group-btn">
                        <span id="c5_result">Ano de Fabricação =</span>
                        <br>
                        <span id="c5_result_2">Tempo de Uso (%) =</span>
                        <br>
                        
                        <span id="c5_result_2_2">Estimativa de Vida =</span>
                        
                        <br>
                        
                        <span id="resultado"></span>
                        <br>
                        
                        <span id="c5_result_6">Aquisição/Depreciação/Taxa (R$)=</span>
                        
                      </span>
                    </div>
                  </div>
           
          <br>
                  
                  <textarea name="editor" id="editor"  placeholder="" value="" rows="100" cols="800"><?php echo htmlspecialchars_decode($obs);?>
                    
                    
                    
                    
                  </textarea>
                  <script type="importmap">
                    {
                      "imports": {
                        "ckeditor5": "https://cdn.ckeditor.com/ckeditor5/42.0.0/ckeditor5.js",
                        "ckeditor5/": "https://cdn.ckeditor.com/ckeditor5/42.0.0/"
                      }
                    }
                  </script>
                  <script type="module">
                    import {
                      ClassicEditor,
                      Essentials,
                      Paragraph,
                      Bold,
                      Italic,
                      Font,
                      Table,
                      TableToolbar,
                      PasteFromOffice,
                      Image,
                      ImageToolbar,
                      ImageCaption,
                      ImageStyle,
                      ImageResize,
                      ImageUpload,
                      ImageInsert,
                      List,
                      Alignment,
                      Heading,
                      FontSize,
                      WordCount
                      
                      
                    } from 'ckeditor5';
                    
                    ClassicEditor
                    .create(document.querySelector('#editor'), {
                      plugins: [
                        Essentials,
                        Paragraph,
                        Bold,
                        Italic,
                        Font,
                        Table,
                        TableToolbar,
                        PasteFromOffice,
                        Image,
                        ImageToolbar,
                        ImageCaption,
                        ImageStyle,
                        ImageResize,
                        ImageUpload,
                        ImageInsert,
                        List,
                        Alignment,
                        Heading,
                        FontSize,
                        WordCount
                        
                      ],
                      toolbar: [
                        'undo', 'redo', '|',
                        'heading', '|',
                        'bold', 'italic', '|',
                        'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', '|',
                        'alignment', '|',
                        'numberedList', 'bulletedList', '|',
                        'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
                        'imageUpload', 'imageInsert', '|', // Adicionando o botão ImageInsert na toolbar
                        'outdent', 'indent'
                      ],
                      fontSize: {
                        options: [
                          'tiny',
                          'small',
                          'default',
                          'big',
                          'huge',
                          '10',
                          '12',
                          '14',
                          '16',
                          '18',
                          '20',
                          '22',
                          '24',
                          '26',
                          '28',
                          '36',
                          '48',
                          '72'
                        ],
                        
                      }, 
                      image: {
                        toolbar: [
                          'imageTextAlternative', '|',
                          'imageStyle:full', 'imageStyle:side', '|',
                          'imageResize'
                        ]
                      },
                      table: {
                        contentToolbar: [
                          'tableColumn', 'tableRow', 'mergeTableCells'
                        ]
                      }
                    })
                    .then(editor => {
                      window.editor = editor;
                    })
                    .catch(error => {
                      console.error(error);
                    });
                  </script>
                  <!-- A friendly reminder to run on a server, remove this during the integration. -->
                  <script>
                    window.onload = function() {
                      if (window.location.protocol === "file:") {
                        alert("This sample requires an HTTP server. Please serve this file with a web server.");
                      }
                    };
                  </script>
                  
                  <script>
                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.
                    //	CKEDITOR.replace( 'editor1' );
                  </script>
                  
                  
                  
                  
                  
                  
                  <div class="ln_solid"></div>
                  

            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Valor Mercado (R$) <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="vlr_mkt" name="vlr_mkt"  class="form-control "  onKeyPress="return(moeda(this,'.',',',event))">
              </div>
            </div>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Valor de Aquisição (R$) <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="vlr_purchasse" name="vlr_purchasse"   class="form-control "  onKeyPress="return(moeda(this,'.',',',event))">
              </div>
            </div>

 

            <div class="item form-group ">
              <div class="col-md-10 col-sm-10 offset-md-3">

              
                 <button type="reset" onclick="clean()" class="btn btn-primary"onclick="new PNotify({
                  title: 'Limpado',
                  text: 'Todos os Campos Limpos',
                  type: 'info',
                  styling: 'bootstrap3'
                });" />Limpar</button>
                <input type="submit" class="btn btn-primary" onclick="new PNotify({
                  title: 'Registrado',
                  text: 'Informações registrada!',
                  type: 'success',
                  styling: 'bootstrap3'
                });" value="Salvar" />
              </div>
            </div>
          </div>
        </form>




      </div>
    </div>
  </div>
</div>
</div>



</div>
</div>
</div>
</div>
<!-- Contador de caracter -->
<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
<script type="text/javascript">
$("meuPrimeiroDropzone").dropzone({ url: "upload.php" });
Dropzone.options.meuPrimeiroDropzone = {
  paramName: "fileToUpload",
  dictDefaultMessage: "Arraste seus arquivos para cá!",
  maxFilesize: 300,
  accept: function(file, done) {
    if (file.name == "olamundo.png") {
      done("Arquivo não aceito.");
    } else {
      done();
    }
  }
}
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c1_result').load('c1.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c2_result').load('c2.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c3_result').load('c3.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c4_result').load('c4.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c5_result').load('c5.php?equipamento='+$('#equipamento').val());
    $('#c5_result_2').load('c5_2.php?equipamento='+$('#equipamento').val());
    $('#c5_result_2_2').load('c5_2_2.php?equipamento='+$('#equipamento').val());
    $('#c5_result_6').load('c5_6.php?equipamento='+$('#equipamento').val());
    $('#c5_result_7').load('c5_7.php?equipamento='+$('#equipamento').val());
    $('#c5_result_8').load('c5_8.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script>
  function obsolescence_check(){
    $('#check').load('sub_categorias_post_obsolescence_check.php?equipamento='+$('#equipamento').val()+'&yr='+$('#yr').val());
  }
function zerargrau() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change');
  $('#c1').select2().load('c1_zero.php');
  $('#c2').select2().load('c2_zero.php');
  $('#c3').select2().load('c3_zero.php');
  $('#c4').select2().load('c4_zero.php');
  $('#c5').select2().load('c5_zero.php');
  $('#c6').select2().load('c6_zero.php');
  $('#c7').select2().load('c7_zero.php');

}

</script>

<script>
function calcular() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change'


  $('#score').load('score.php?c1='+$('#c1').val()+'&c2='+$('#c2').val()+'&c3='+$('#c3').val()+'&c4='+$('#c4').val()+'&c5='+$('#c5').val()+'&c6='+$('#c6').val()+'&c7='+$('#c7').val());

}

</script>
<script>
function obsolescence() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change');
  $('#c1').select2().load('c1_equipament.php?equipamento='+$('#equipamento').val());
  $('#c2').select2().load('c2_equipament.php?equipamento='+$('#equipamento').val());
  $('#c3').select2().load('c3_equipament.php?equipamento='+$('#equipamento').val());
  $('#c4').select2().load('c4_equipament.php?equipamento='+$('#equipamento').val());
  $('#c5').select2().load('c5_equipament.php?equipamento='+$('#equipamento').val());
  $('#c6').select2().load('c6_equipament.php?equipamento='+$('#equipamento').val());
  $('#c7').select2().load('c7_equipament.php?equipamento='+$('#equipamento').val());
  $('#rooi').select2().load('rooi_equipament.php?equipamento='+$('#equipamento').val());
  $('#score').load('score_equipament.php?equipamento='+$('#equipamento').val());
  $('#obs').load('obs_equipament.php?equipamento='+$('#equipamento').val());
  $('#vlr_mkt').load('vlr_mkt_equipament.php?equipamento='+$('#equipamento').val());
  $('#vlr_purchasse').load('vlr_purchasse_equipament.php?equipamento='+$('#equipamento').val());
  $('#obs_purchasse').load('obs_purchasse_equipament.php?equipamento='+$('#equipamento').val());

  
}

</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>
								<script>
function clean() {
  $('#unidade').select2().load('reset_unidade.php');
  $('#area').select2().load('reset_area.php');
  $('#setor').select2().load('reset_setor.php');
  $('#equipamento').select2().load('reset_equipamento.php');


}

</script>
<script>
function anoatual() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change');
  $('#yr').select2().load('obsolescence_yr.php');
  
}

</script>
<script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.FROMCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>  
		
  