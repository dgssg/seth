  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Abertura de <small>Solicitação</small></h3>
              </div>

           
            </div>
       <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                     
                                    
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

<!-- Menu -->
 
          
                <a class="btn btn-app <?php echo $current_page == 'purchases-progress' ? 'active' : ''; ?>" href="purchases-progress">
                     <i class="fa fa-battery-empty"></i> Aberto
                  </a>
                  
                                   <a class="btn btn-app <?php echo $current_page == 'purchases-progress-approval' ? 'active' : ''; ?>" href="purchases-progress-approval">

                    <i class="fa fa-battery-quarter"></i> Aprovação
                  </a>
                                   <a class="btn btn-app <?php echo $current_page == 'purchases-progress-progress' ? 'active' : ''; ?>" href="purchases-progress-progress">

                    <i class="fa fa-battery-half"></i> Andamento
                  </a>

                                  <a class="btn btn-app <?php echo $current_page == 'purchases-progress-concluded' ? 'active' : ''; ?>" href="purchases-progress-concluded">

                    <i class="fa fa-battery-full"></i> Concluido
                  </a>
               
                
                  
                </div>
              </div>
              
              
              
              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:3; -moz-column-count:3; -webkit-column-count:3; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
        
              
                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Solicitação Concluida </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    
                <?php include 'frontend/purchases-progress-concluded-list-frontend.php';?>
                  
                </div>
              </div> 
	       
	        
                  
                  
                </div>
              </div>
              
              
  
              
  