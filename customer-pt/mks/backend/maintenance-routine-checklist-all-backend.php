<?php
	
	include("../database/database.php");
	include("../database/database-procedure.php");
	
	
	$open=0;
	$id_status=3;
	$status=1;
	$procedure=0;
	$procedure_mp=3;
	
	
	
	$query = "SELECT equipamento.data_instal,equipament_check.file,regdate_equipament_check_install.id_equipamento_check_install,regdate_equipament_check_install.id_user,regdate_equipament_check_install.id_signature,regdate_equipament_check_install.reg_date,usuario.nome,equipament_check_install.signature_admin,equipamento.id,maintenance_routine.periodicidade_after,maintenance_routine.data_after,maintenance_routine.time_after,maintenance_routine.id_pop,maintenance_routine.id_maintenance_procedures_after,maintenance_routine.id_maintenance_procedures_before,maintenance_routine.id_fornecedor,maintenance_routine.id_colaborador,maintenance_routine.id_category,maintenance_routine.habilitado,maintenance_routine.time_ms,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN equipament_check_install ON equipament_check_install.id_equipamento = equipamento.id INNER JOIN regdate_equipament_check_install on equipament_check_install.id = regdate_equipament_check_install.id_equipamento_check_install INNER JOIN usuario ON regdate_equipament_check_install.id_user = usuario.id LEFT JOIN equipament_check ON equipament_check.id_equipamento = equipamento.id";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($data_instal,$file,$id_equipamento_check_install,$id_user,$id_signature,$reg_date,$nome,$signature_admin,$id,$periodicidade_after,$data_after,$time_after,$id_pop,$id_maintenance_procedures_after,$id_maintenance_procedures_before,$id_fornecedor,$id_colaborador,$id_category,$habilitado,$time_ms,$routine,$data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
		
		while ($stmt->fetch()) {
			
	$origem = '../dropzone/equipament-check/'.$file; // Substitua pelo caminho do arquivo de origem
	$destino = '../dropzone/mp/'.$file; // Substitua pelo caminho de destino
	
	if (copy($origem, $destino)) {
		//echo 'Arquivo copiado com sucesso.';
	} else {
		//  echo 'Houve um erro ao copiar o arquivo.';
	}
	
	
	$stmt_procedure = $conn_procedure->prepare("INSERT INTO maintenance_preventive( id_routine, date_start,date_end,id_status,id_fornecedor,file,date_mp_start,date_mp_end,time_mp,id_mp,signature_admin,procedure_mp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
	//if(!$req){
	//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
	//    }
	$stmt_procedure->bind_param("ssssssssssss",$routine,$data_instal,$data_instal,$id_status,$id_fornecedor,$file,$data_instal,$data_instal,$time_ms,$id_colaborador,$signature_admin,$procedure);
	
	$execval = $stmt_procedure->execute();
	$last_id = $conn_procedure->insert_id;
	$stmt_procedure->close();
	
	$stmt_procedure = $conn_procedure->prepare("INSERT INTO  regdate_mp_signature (id_mp, id_user, id_signature) VALUES (?, ?, ?)");
	$stmt_procedure->bind_param("sss",$last_id,$id_user,$signature_admin);
	$execval = $stmt_procedure->execute();
	$stmt_procedure->close();
		}
	}
	echo "<script>alert('Alteração Registrado!');document.location='../maintenance-routine'</script>";

?>