 <?php

include("../database/database.php");

$codigoget= $_GET['id'];

$equipamento_grupo= $_POST['equipamento_grupo'];
$nome= $_POST['nome'];
$modelo= $_POST['modelo'];
$fabricante= $_POST['fabricante'];
$anvisa= $_POST['anvisa'];
$kpi= $_POST['kpi'];
$endoflife= $_POST['endoflife'];
$aquisicion= $_POST['aquisicion'];
$preventive= $_POST['preventive'];
$id_class= $_POST['id_class'];
$id_fornecedor= $_POST['id_fornecedor'];

$stmt = $conn->prepare("UPDATE equipamento_familia SET id_fornecedor = ? WHERE id= ?");
$stmt->bind_param("ss",$id_fornecedor,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE equipamento_familia SET id_class = ? WHERE id= ?");
$stmt->bind_param("ss",$id_class,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE equipamento_familia SET aquisicion = ? WHERE id= ?");
$stmt->bind_param("ss",$aquisicion,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET preventive = ? WHERE id= ?");
$stmt->bind_param("ss",$preventive,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET id_equipamento_grupo = ? WHERE id= ?");
$stmt->bind_param("ss",$equipamento_grupo,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET modelo = ? WHERE id= ?");
$stmt->bind_param("ss",$modelo,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET fabricante = ? WHERE id= ?");
$stmt->bind_param("ss",$fabricante,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET anvisa = ? WHERE id= ?");
$stmt->bind_param("ss",$anvisa,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET kpi = ? WHERE id= ?");
$stmt->bind_param("ss",$kpi,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE equipamento_familia SET endoflife = ? WHERE id= ?");
$stmt->bind_param("ss",$endoflife,$codigoget);
$execval = $stmt->execute();
echo "<script>alert('Edição Concluida!');document.location='../register-equipament-family.php?id=$codigoget'</script>";

header('Location: ../register-equipament-family.php?id='.$codigoget);

?>
