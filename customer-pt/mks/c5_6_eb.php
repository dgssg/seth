<?php 
$tdown = $_GET['equipamento'];
include("database/database.php");
date_default_timezone_set('America/Sao_Paulo');
$ano=date("Y");
$query = "SELECT  equipamento_eb.vlr,equipamento_eb.data_fab, equipamento_eb.id,equipamento_grupo_eb.yr, equipamento_familia_eb.preventive FROM equipamento_familia_eb INNER JOIN equipamento_eb on equipamento_eb.id_equipamento_familia = equipamento_familia_eb.id INNER JOIN equipamento_grupo_eb ON equipamento_familia_eb.id_equipamento_grupo = equipamento_grupo_eb.id  WHERE equipamento_eb.id = $tdown";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($vlr,$data_fab,$id,$year,$custo_manutencao_anual);
 while ($stmt->fetch()) {
 }
}
    $indice_inflacao = 0.10; // 10% de inflação acumulada
    
    // Calcula a diferença entre o ano atual e o ano de fabricação
    $diferenca_anos = $ano - $data_fab;
    
    if ($vlr == null) {
        $vlr = 1;
    }
    if ($custo_manutencao_anual == null) {
        $custo_manutencao_anual = 1;
    }
    // Calcula a depreciação anual
    $dp = ($diferenca_anos / $year) * $vlr;
    $anos_uso = $year - $diferenca_anos;
    $uso = ($dp / $vlr) * 100;
    
    echo '<strong>Valor de Aquisição (R$) ='.$vlr.'  </strong>';
    echo "<br>";
    echo '<strong>Depreciação Anual (R$) ='.$dp.'  </strong>';
    echo "<br>";
    echo '<strong>Taxa Depreciação Anual (%) ='.$uso.'  </strong>';
    echo "<br>";
    
    // Calcula o valor residual
    $valor_residual = abs($vlr - ($dp * $anos_uso));
    echo '<strong>Valor Residual (R$) ='.$valor_residual.'  </strong>';
    echo "<br>";
    
    // Calcula a vida útil restante
    $vida_util_restante = $anos_uso;
    echo '<strong>Vida Útil Restante (Anos) ='.$vida_util_restante.'  </strong>';
    echo "<br>";
    
    // Calcula a depreciação acumulada
    $depreciacao_acumulada = $dp * $anos_uso;
    echo '<strong>Depreciação Acumulada (R$) ='.$depreciacao_acumulada.'  </strong>';
    echo "<br>";
    
    // Calcula o valor contábil
    $valor_contabil = $vlr - $depreciacao_acumulada;
    echo '<strong>Valor Contábil (R$) ='.$valor_contabil.'  </strong>';
    echo "<br>";
    
    // Calcula a depreciação mensal
    $depreciacao_mensal = $dp / 12;
    echo '<strong>Depreciação Mensal (R$) ='.$depreciacao_mensal.'  </strong>';
    echo "<br>";
    
    // Calcula o percentual de depreciação acumulada
    $percentual_depreciacao_acumulada = ($depreciacao_acumulada / $vlr) * 100;
    echo '<strong>Percentual de Depreciação Acumulada (%) ='.$percentual_depreciacao_acumulada.'  </strong>';
    echo "<br>";
    
    // Calcula o valor de reposição ajustado pela inflação
    $valor_reposicao = $vlr * (1 + $indice_inflacao);
    echo '<strong>Valor de Reposição (R$) ='.$valor_reposicao.'  </strong>';
    echo "<br>";
    
    // Cálculo de Depreciação Acelerada pelo Método da Soma dos Dígitos
    $soma_digitos = ($year * ($year + 1)) / 2;
    $depreciacao_acelerada = ($vida_util_restante / $soma_digitos) * ($vlr - $valor_residual);
    echo '<strong>Depreciação Acelerada (R$) ='.$depreciacao_acelerada.'  </strong>';
    echo "<br>";
    
    // Índice de renovação (valor de reposição / valor contábil)
    $indice_renovacao = $valor_reposicao / ($valor_contabil == 0 ? 1 : $valor_contabil);
    echo '<strong>Índice de Renovação ='.$indice_renovacao.'  </strong>';
    echo "<br>";
    
    // Taxa interna de retorno do ativo (aproximada)
    $tir_aproximada = ($dp / ($valor_contabil == 0 ? 1 : $valor_contabil)) * 100;
    echo '<strong>Taxa Interna de Retorno Aproximada (%) ='.$tir_aproximada.'  </strong>';
    echo "<br>";
    
    $taxa_utilizacao = ($diferenca_anos / $year) * 100;
    echo '<strong>Taxa de Utilização (%) ='.$taxa_utilizacao.'  </strong>';
    echo "<br>";
    
    $taxa_desvalorizacao = 0.10; // Suponha uma taxa de 10% de desvalorização anual
    $valor_mercado_ajustado = $vlr * pow((1 - $taxa_desvalorizacao), $diferenca_anos);
    echo '<strong>Valor de Mercado Ajustado (R$) ='.$valor_mercado_ajustado.'  </strong>';
    echo "<br>";
    
    
    $tco = $vlr + ($custo_manutencao_anual * $diferenca_anos);
    echo '<strong>Custo Total de Propriedade (TCO) (R$) ='.$tco.'  </strong>';
    echo "<br>";
    
    $taxa_obsolescencia = ($diferenca_anos / $year) * 100;
    echo '<strong>Taxa de Obsolescência (%) ='.$taxa_obsolescencia.'  </strong>';
    echo "<br>";
    
    
    $valor_futuro = $vlr * pow(1 + $indice_inflacao, $year);
    echo '<strong>Valor Futuro Ajustado pela Inflação (R$) ='.$valor_futuro.'  </strong>';
    echo "<br>";
    
    
    $lucro_prejuizo = $vlr - $valor_contabil;
    echo '<strong>Lucro/Prejuízo na Venda (R$) ='.$lucro_prejuizo.'  </strong>';
    echo "<br>";
    
    $depreciacao_linear = $vlr / $year;
    echo '<strong>Depreciação Linear (R$) ='.$depreciacao_linear.'  </strong>';
    echo "<br>";

    
    echo "<br>";
?>
