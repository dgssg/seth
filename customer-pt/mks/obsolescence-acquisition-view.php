<?php


include("database/database.php");
date_default_timezone_set('America/Sao_Paulo');
$ano_data=date("Y");
$today=date("Y-m-d");
$mes_data=date("m");
$instituicao="";
$instituicao_area="";
$id_instituicao_localizacao="";
$equipamento="";
$equipamento_familia="";
$equipamento_grupo="";

$instituicao=$_POST['instituicao'];
$instituicao=trim($instituicao);

$instituicao_area=$_POST['area'];
$instituicao_area=trim($instituicao_area);

$id_instituicao_localizacao=$_POST['setor'];
$id_instituicao_localizacao=trim($id_instituicao_localizacao);

$equipamento=$_POST['equipamento'];
$equipamento=trim($equipamento);

$equipamento_familia = $_POST['equipamento_familia'];
$equipamento_familia = trim($equipamento_familia);

$equipamento_grupo = $_POST['equipamento_grupo'];
$equipamento_grupo = trim($equipamento_grupo);

$yr = $_POST['yr'];
$yr = trim($yr);










$filtro="";
$query = "SELECT obsolescence_c1.grau  AS 'c1',obsolescence_c2.grau  AS 'c2',obsolescence_c3.grau  AS 'c3',obsolescence_c4.grau  AS 'c4',obsolescence_c5.grau  AS 'c5',obsolescence_c6.grau  AS 'c6',obsolescence_c7.grau  AS 'c7',equipamento.id_instituicao_localizacao,obsolescence.score,obsolescence.obs,obsolescence_year.yr,obsolescence_rooi.rooi, equipamento_grupo.nome AS 'equipamento_grupo', equipamento.data_fab,equipamento.patrimonio,equipamento.serie,instituicao.instituicao,instituicao_area.custo AS 'custo_area',instituicao_area.nome AS 'area_nome',equipamento.id, equipamento_familia.nome AS 'equipamento', equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome AS 'setor_nome' FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON  instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN   instituicao_area on instituicao_area.id = instituicao_localizacao.id_area INNER JOIN  instituicao ON instituicao.id = instituicao_area.id_unidade INNER JOIN   equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo INNER JOIN obsolescence ON obsolescence.id_equipamento = equipamento.id INNER JOIN obsolescence_rooi ON obsolescence_rooi.id = obsolescence.id_rooi INNER JOIN obsolescence_year ON obsolescence_year.id = obsolescence.id_year INNER JOIN obsolescence_c1 ON obsolescence_c1.id = obsolescence.id_c1 INNER JOIN obsolescence_c2 ON obsolescence_c2.id = obsolescence.id_c2 INNER JOIN obsolescence_c3 ON obsolescence_c3.id = obsolescence.id_c3 INNER JOIN obsolescence_c4 ON obsolescence_c4.id = obsolescence.id_c4 INNER JOIN obsolescence_c5 ON obsolescence_c5.id = obsolescence.id_c5 INNER JOIN obsolescence_c6 ON obsolescence_c6.id = obsolescence.id_c6 INNER JOIN obsolescence_c7 ON obsolescence_c7.id = obsolescence.id_c7 WHERE equipamento.inventario like '0' ";

if($instituicao==!""){
  $query = " $query and  instituicao.id = '$instituicao'";
}

if($instituicao_area==!""){
  $query = " $query and  instituicao_area.id = '$instituicao_area'";
}

if($id_instituicao_localizacao==!""){
  $query = " $query and  instituicao_localizacao.id = '$id_instituicao_localizacao'";
}

if($equipamento==!""){
  $query = " $query and  equipamento.id = '$equipamento'";
}

if($equipamento_familia==!""){
  $query = " $query and  equipamento_familia.id = '$equipamento_familia'";
}

if($yr==!""){
  $query = " $query and  obsolescence_year.yr = '$yr'";
}

if($equipamento_grupo==!""){
  $query = " $query and  equipamento_grupo.id = '$equipamento_grupo'";
}


$sql = "SELECT  instituicao,logo,endereco,cep,bairro,cidade,estado,logo,cnpj FROM instituicao WHERE id = $instituicao ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
   $stmt->bind_result($unidade,$logo,$rua,$cep,$bairro,$cidade,$estado,$logo,$cnpj);
   while ($stmt->fetch()) {
   }
  }

  $sql = "SELECT  nome FROM instituicao_area WHERE id = $instituicao_area ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
   $stmt->bind_result($setor);
   while ($stmt->fetch()) {
   }
  }

  $sql = "SELECT  nome FROM instituicao_localizacao WHERE id = $id_instituicao_localizacao ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
   $stmt->bind_result($area);
   while ($stmt->fetch()) {
   }
  }



$query = " $query  ORDER BY  equipamento.codigo ASC;";

if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($c1,$c2,$c3,$c4,$c5,$c6,$c7,$id_instituicao_localizacao,$score,$obs,$yr,$rooi,$equipamento_grupo,$data_fab,$patrimonio,$serie,$instituicao,$custo_area,$area_nome,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);

  //header('Location: ../report-report-mc');
  ?>

  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />
    <!-- Bootstrap 3.3.5 -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    body {
      font-size: 12pt;
      font-family: Calibri;
      padding : 10px;
    }
    table {
      border: 1px solid black;
    }
    th {
      border: 1px solid black;
      padding: 5px;
      background-color:grey;
      color: white;
    }
    td {
      border: 1px solid black;
      padding: 5px;
    }
    input {
      font-size: 12pt;
      font-family: Calibri;
    }
  </style>
  <style>
    /* Estilos para o cabeçalho */
    header {
      padding: 20px;
      background-color: #f5f5f5;
      border-bottom: 1px solid #ddd;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    td {
      padding: 5px;
      border: 1px solid #ddd;
    } 

    #logo {
      width: 100px;
    }

    h1 {
      margin: 0;
      font-size: 24px;
    }

    #info {
      font-size: 14px;
    }
  </style>
</head>
<body>
  <a class="btn btn-app"  href="report-obsolescence-acquisition-viwer.php?query=<?php printf($query); ?>" target="_blank">
    <i class="fa fa-file-pdf-o"></i> PDF
  </a>
  <a class="btn btn-app"  id="btnExport">
    <i class="fa fa-file-excel-o"></i> Excel
  </a>
  <script type="text/javascript" language="JavaScript">

function printPage() {

  if (window.print) {

    agree = confirm("Deseja imprimir essa pagina ?");

    if (agree) {
      window.print();

      if (_GET("pg") != null)
      location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
      else
      history.go(-1);


      window.close();
    }



  }
}

function noPrint() {
  try {

    if (_GET("pg") != null)
    location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
    else
    history.go(-1);

    window.close();

  }
  catch (e) {
    alert();
    document.write("Error Message: " + e.message);
  }
}

function _GET(name) {
  var url = window.location.search.replace("?", "");
  var itens = url.split("&");

  for (n in itens) {
    if (itens[n].match(name)) {
      return decodeURIComponent(itens[n].replace(name + "=", ""));
    }
  }
  return null;
}





function visualizarImpressao() {

  var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

  document.body.insertAdjacentHTML("beforeEnd", Navegador);

  Navegador1.ExecWB(7, 1);

  Navegador1.outerHTML = "";

}



(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
    (i[r].q = i[r].q || []).push(arguments)
  }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-11247563-6', 'auto');
ga('send', 'pageview');

// -->
</script>

<style type="text/css">
page {
  /* background: white;*/
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  text-align: left;
}

page[size="A4"] {
  width: 21cm;
  /*height: 29.7cm;                */
}

page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  /* height: 21cm; */
}

@media print {
  body,
  page {
    margin: 0;
    box-shadow: 0;
    page-break-after:always;
  }

  .noprint {
    display: none;
  }
}




body {
  font-size: 11px;
  font-family: sans-serif;
}



.pagina_retrato {
  width: 720px;
}

.pagina_paisagem {
  width: 1025px;
}

.relatorio_filtro {
  padding: 10px 0px;
  border-collapse: collapse;
}

.relatorio_filtro tr td {
  border: 1px solid #000;
}


.relatorio_titulo {
  font-family: Times New Roman;
  font-size: 15px;
  color: Black;
  font-weight: bold;
  text-transform: uppercase;
}

.borda_celula {
  border: 1px solid #000;
  background-color: #fff;
}

.noprint {
  padding-bottom: 10px;
}
.right{
  float:right;
}

.left{
  float:left;
}
</style>

<style type="text/css" media="print">
/*
@page {
margin: 0.8cm;
}
*/
.pagina_retrato, .pagina_paisagem {
width: 100%;
}
 
</style>

<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script>
window.onload = function () {
JsBarcode(".barcode").init();
}
</script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />
<link href="../App_Themes/Tema1/StyleSheet.css" type="text/css" rel="stylesheet" />

<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEwODA5NzUyMjMPZBYCZg9kFgICAw9kFgICAQ9kFggCAw8PFgIeCEltYWdlVXJsBUd+L2ltYWdlcy9ob3NwaXRhaXMvaV9jcnV6dmVybWVsaGFicmFzaWxlaXJhLWZpbGlhbGRvZXN0YWRvZG9wYXJhbsOhLnBuZ2RkAgUPDxYCHgRUZXh0BQcwMS8yMDE5ZGQCCw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIeCkhlYWRlclRleHQFBzAxLzIwMTg8KwAFAQAWAh8EBQcwMi8yMDE4PCsABQEAFgIfBAUHMDMvMjAxODwrAAUBABYCHwQFBzA0LzIwMTg8KwAFAQAWAh8EBQcwNS8yMDE4PCsABQEAFgIfBAUHMDYvMjAxODwrAAUBABYCHwQFBzA3LzIwMTg8KwAFAQAWAh8EBQcwOC8yMDE4PCsABQEAFgIfBAUHMDkvMjAxODwrAAUBABYCHwQFBzEwLzIwMTg8KwAFAQAWAh8EBQcxMS8yMDE4PCsABQEAFgIfBAUHMTIvMjAxODwrAAUBABYCHwQFBzAxLzIwMTkWDQIGAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGDBQrAABkAg8PPCsAEQMADxYEHwJnHwNmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIfBAUHMDEvMjAxODwrAAUBABYCHwQFBzAyLzIwMTg8KwAFAQAWAh8EBQcwMy8yMDE4PCsABQEAFgIfBAUHMDQvMjAxODwrAAUBABYCHwQFBzA1LzIwMTg8KwAFAQAWAh8EBQcwNi8yMDE4PCsABQEAFgIfBAUHMDcvMjAxODwrAAUBABYCHwQFBzA4LzIwMTg8KwAFAQAWAh8EBQcwOS8yMDE4PCsABQEAFgIfBAUHMTAvMjAxODwrAAUBABYCHwQFBzExLzIwMTg8KwAFAQAWAh8EBQcxMi8yMDE4PCsABQEAFgIfBAUHMDEvMjAxORYNAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGAgYMFCsAAGQYAgUeY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSRncnYyDzwrAAwBCGZkBR5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJGdydjEPPCsADAEIZmS9tcluC/wCTfFh0PbdSxSO8nxQJa5LJqUDKFT3sJzurQ==" />
</div>

<div>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3164E9EE" />
</div>

<div class="noprint">
<center>
  <input type="button" value="Não Imprimir" onclick="noPrint();">
  <input type="button" value="Imprimir" onclick="printPage();" >
  <input type="button" onclick="visualizarImpressao();" value="Visualizar Impressão" >
</center>
</div>

  <header>
    <table>
      <tr>
      <td><img src="dropzone/logo/<?php printf($logo)?>" alt="Logo da empresa" id="logo"></td>
        <td>
          <center>
          <h1>Relatório de Obsolescência</h1>
          <h1>Ano de Referência:<?php printf($yr); ?> </h1>
          <p>Data da impressão: <?php printf($today); ?> <small>Sistema SETH</small></p>
          </center>
        </td>
        <td><img src="logo/clientelogo.png" alt="Logo da empresa" id="logo"></td>
      </tr>
      <tr>
        <td colspan="3" id="info">
          <table>
            <tr>
              <td>Unidade:</td>
              <td><?php printf($unidade); ?></td>
              <td>Área:</td>
              <td><?php printf($area); ?></td>
              <td>Setor:</td>
              <td><?php printf($setor); ?></td>
            </tr>
            <tr>
              <td>CEP:</td>
              <td><?php printf($cep); ?></td>
              <td>Rua:</td>
              <td><?php printf($rua); ?></td>
              <td>Bairro:</td>
              <td><?php printf($bairro); ?></td>
            </tr>
            <tr>
              <td>Cidade:</td>
              <td><?php printf($cidade); ?></td>
              <td>Estado:</td>
              <td><?php printf($estado); ?></td>
              <td>CNPJ:</td>
              <td><?php printf($cnpj); ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </header>

  <div id="dvData">
  <table style="border-collapse: collapse; width: 100%;">
      <thead>
        <tr>
          <th style="border: 1px solid black; padding: 5px;">#</th>
          <th style="border: 1px solid black; padding: 5px;">Equipamento</th>
          <th style="border: 1px solid black; padding: 5px;">Codigo</th>
          <th style="border: 1px solid black; padding: 5px;">N/S</th>
          <th style="border: 1px solid black; padding: 5px;">Pat.</th>
          <th style="border: 1px solid black; padding: 5px;">Fab.</th>
          <th style="border: 1px solid black; padding: 5px;">Unidade</th>
          <th style="border: 1px solid black; padding: 5px;">Setor</th>
          <th style="border: 1px solid black; padding: 5px;">Area</th>
          <th style="border: 1px solid black; padding: 5px;">Analise</th>
          <th style="border: 1px solid black; padding: 5px;">ROOI</th>
          <th style="border: 1px solid black; padding: 5px;">C1</th>
          <th style="border: 1px solid black; padding: 5px;">C2</th>
          <th style="border: 1px solid black; padding: 5px;">C3</th>
          <th style="border: 1px solid black; padding: 5px;">C4</th>
          <th style="border: 1px solid black; padding: 5px;">C5</th>
          <th style="border: 1px solid black; padding: 5px;">C6</th>
          <th style="border: 1px solid black; padding: 5px;">C7</th>
          <th style="border: 1px solid black; padding: 5px;">Pontuação</th>
          <th style="border: 1px solid black; padding: 5px;">Observação</th>





        </tr>
      </thead>


      <tbody>
        <?php   while ($stmt->fetch()) {   ?>
          <tr>
            <td><?php printf($id); ?> </td>
            <td><?echo htmlspecialchars_decode($nome);?> <?echo htmlspecialchars_decode($modelo); ?> <?echo htmlspecialchars_decode($fabricante); ?></td>
            <td><?php echo htmlspecialchars_decode($codigo); ?> </td>
            <td><?php echo htmlspecialchars_decode($serie); ?> </td>
            <td><?php echo htmlspecialchars_decode($patrimonio); ?> </td>
            <td><?php echo htmlspecialchars_decode($data_fab); ?> </td>
            <td><?php echo htmlspecialchars_decode($instituicao); ?> </td>
            <td><?php echo htmlspecialchars_decode($area_nome); ?> </td>
            <td><?php echo htmlspecialchars_decode($localizacao);?> </td>
            <td><?php echo htmlspecialchars_decode($yr);?> </td>
            <td><?php echo htmlspecialchars_decode($rooi);?> </td>
            <td><?php echo htmlspecialchars_decode($c1);?> </td>
            <td><?php echo htmlspecialchars_decode($c2);?> </td>
            <td><?php echo htmlspecialchars_decode($c3);?> </td>
            <td><?php echo htmlspecialchars_decode($c4);?> </td>
            <td><?php echo htmlspecialchars_decode($c5);?> </td>
            <td><?php echo htmlspecialchars_decode($c6);?> </td>
            <td><?php echo htmlspecialchars_decode($c7);?> </td>
            <td  <?php if($score <="2.5"){   ?> style="background-color: green" <?php    } if(($score >"2.5") && ($score <= "5.5")){?> style="background-color: yellow" <?php  }if($score > "5.5"){ ?> style="background-color: red" <?php   } ?> >
        

              <?php echo htmlspecialchars($score); ?> 
            </td> 
            <td><?php echo htmlspecialchars_decode($obs);?> </td>



          </tr>
        <?php   } }  ?>
      </tbody>
      <tfoot>
    <tr>
       
      <td colspan="20" style="text-align: left; font-weight: bold;"><b>Legenda:</b><br>
    
    <b style="background-color: green;">  Vida útil acima de 03 anos</b>
    <b style="background-color: yellow;"> Pode se tornar Obsoleto a qualquer momento</b>
    <b style="background-color: red;"> Obsoleto </b> <br>
C1 - Tempo de Uso |
C2 - Custo de Manutenção |
C3 - Disponibilidade do Equipamento |
C4 - Tecnologia do Equipamento |
C5 - Utilização do Equipamento |
C6 - Recursos Disponíveis na Manutenção |
C7 - Custo de Reforma </td>
    
    </tr>
  </tfoot>
    </table>
  </div>
  <!-- jQuery 2.1.4 -->
  <script src="../../framework/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="../../framework/bootstrap/js/bootstrap.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
  <script src="jquery.btechco.excelexport.js"></script>
  <script src="jquery.base64.js"></script>
  <!--<script>
  $(document).ready(function () {
  $("#btnExport").click(function () {
  $("#tblExport").btechco_excelexport({
  containerid: "tblExport"
  , datatype: $datatype.Table
  , filename: 'sample'
});
});
});
</script>
<script>
$("#btnExport").click(function (e) {
window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
e.preventDefault();
});
</script> -->
<script>
$(document).ready(function () {
  $("#btnExport").click(function (e) {
    e.preventDefault();
    var table_div = document.getElementById('dvData');
    // esse "\ufeff" é importante para manter os acentos
    var blobData = new Blob(['\ufeff'+table_div.outerHTML], { type: 'application/vnd.ms-excel' });
    var url = window.URL.createObjectURL(blobData);
    var a = document.createElement('a');
    a.href = url;
    a.download = 'SETH_obsolecencia'
    a.click();
  });
});
</script>

<script type="text/javascript">
$.ajaxSetup({ cache: false })

function limpaUrl() {     //função
    urlpg = $(location).attr('href');   //pega a url atual da página
    urllimpa = urlpg.split("?")[0]      //tira tudo o que estiver depois de '?'

    window.history.replaceState(null, null, urllimpa); //subtitui a url atual pela url limpa
  
}
limpaUrl();
</script>
</body>

</html>
