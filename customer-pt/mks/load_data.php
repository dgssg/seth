<?php
include("database/database.php");

$id_equipamento = $_GET['id_equipamento'];

if($id_equipamento !== "" && $id_equipamento !== null) { 
  $sql = "SELECT 
   equipamento.id_fornecedor ,
              equipamento_grupo.id_time, 
              equipamento_grupo.id_periodicidade,   
              equipamento_grupo.id_categoria, 
              equipamento_grupo.id_mp_group,
              equipamento_grupo.id_procedimento_1, 
              equipamento_grupo.id_procedimento_2, 
              equipamento_grupo.id_colaborador, 
              equipamento_grupo.id_pop
             
          FROM 
              equipamento
          LEFT JOIN 
              equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia 
          LEFT JOIN 
              equipamento_grupo ON equipamento_grupo.id = equipamento_familia.id_equipamento_grupo
          WHERE 
              equipamento.id = ?";

  if ($stmt = $conn->prepare($sql)) {
    $stmt->bind_param("i", $id_equipamento);
    $stmt->execute();
    $stmt->bind_result($id_fornecedor,$time, $periodicidade, $categoria, $group_routine, $procedimento_1, $procedimento_2, $colaborador, $pop);
    while ($stmt->fetch()) {
      // Output the data in JSON format
      echo json_encode(array(
        'id_fornecedor' => $id_fornecedor,
        'time' => $time,
        'periodicidade' => $periodicidade,
        'categoria' => $categoria,
        'group_routine' => $group_routine,
        'procedimento_1' => $procedimento_1,
        'procedimento_2' => $procedimento_2,
        'colaborador' => $colaborador,
        'pop' => $pop
      ));
    }
    $stmt->close();
  }
}
$conn->close();
?>
