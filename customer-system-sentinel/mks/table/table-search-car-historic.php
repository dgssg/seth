<?php
include("../database/database.php");
$query = "SELECT reg_date_alarm.id,reg_date_alarm.macadress,reg_date_alarm.reg_date,alarm_status.alarm,car.nome,car.codigo,unidade.nome as 'unidade',unidade_setor.nome as 'setor',unidade_area.nome as 'area' FROM reg_date_alarm LEFT JOIN alarm_status ON alarm_status.id = reg_date_alarm.id_alarm_status LEFT JOIN device ON device.macadress = reg_date_alarm.macadress LEFT JOIN car ON car.id = device.id_car LEFT JOIN unidade_area ON unidade_area.id = car.id_unidade_area LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade GROUP BY  reg_date_alarm.id  ORDER by reg_date_alarm.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
