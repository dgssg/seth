<?php
include("../database/database.php");
    $query = "SELECT 
    unidade_area.id, 
    MAX(unidade.nome) AS 'unidade', 
    MAX(unidade_setor.nome) AS 'setor',
    unidade_area.codigo,
    unidade_area.andar,
    unidade_area.nome,
    unidade_area.observacao,
    unidade_area.upgrade,
    MAX(unidade_area.reg_date) AS 'reg_date'
    FROM 
    unidade_area
    LEFT JOIN 
    unidade_setor ON unidade_area.id_unidade_setor = unidade_setor.id
    LEFT JOIN 
    unidade ON unidade_setor.id_unidade = unidade.id
    WHERE 
    unidade_area.trash = 1
    GROUP BY 
    unidade_area.id, 
    unidade_area.codigo,
    unidade_area.andar,
    unidade_area.nome,
    unidade_area.observacao,
    unidade_area.upgrade
";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
