<?php
	$sweet_salve = ($_GET["sweet_salve"]);
	$sweet_delet = ($_GET["sweet_delet"]);
	if ($sweet_delet == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
		Swal.fire({
				position: \'top-end\',
				icon: \'success\',
				title: \'Seu trabalho foi deletado!\',
				showConfirmButton: false,
				timer: 1500
		});
</script>';
		
		
	}
	if ($sweet_salve == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
				Swal.fire({
						position: \'top-end\',
						icon: \'success\',
						title: \'Seu trabalho foi salvo!\',
						showConfirmButton: false,
						timer: 1500
				});
		</script>';
		
		
	}
?>
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<!-- Switchery -->
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
<?php
	$codigoget = ($_GET["id"]);
	
	// Create connection
	include("database/database.php");// remover ../
	
	
	$query = "SELECT unidade_setor.id,unidade_setor.id_unidade,car.id,car.nome,car.codigo, car.id_unidade_area, car.id_device, car.seal_now,car.seal_before,car.id_status,car.serie,car.patrimonio,car.ativo,car.obs,car.img, car.upgrade, car.reg_date FROM car LEFT JOIN unidade_area  ON unidade_area.id = car.id_unidade_area LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor  where car.id = $codigoget";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($unidade_setor,$id_unidade,$id,$nome,$codigo, $id_unidade_area, $id_device, $seal_now,$seal_before,$id_status,$serie,$patrimonio,$ativo,$obs,$img, $upgrade, $reg_date);
		
		
		
		while ($stmt->fetch()) {
			
		}
	}
?>

<div class="right_col" role="main">
	
	<div class="">
		
		<div class="page-title">
			<div class="title_left">
				<h3>Edição</h3>
			</div>
			
			<div class="title_right">
				<div class="col-md-5 col-sm-5  form-group pull-right top_search">
					<div class="input-group">
						
						<span class="input-group-btn">
							
						</span>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Alerta</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<div class="alert alert-success alert-dismissible " role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<strong>Area de Edição!</strong> Todo alteração não é irreversível.
				</div>
				
				
				
			</div>
		</div>
		
		
		<!-- page content -->
		
		
		
		
		
		
		
		
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Cadastro <small>Carrinho de Emergência</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a class="dropdown-item" href="#">Settings 1</a>
									</li>
									<li><a class="dropdown-item" href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						

						<br />
						<form action="backend/register-car-edit-backend.php" method="post">
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
								<div class="col-md-6 col-sm-6 ">
									<input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
									
									
									
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Instituição</label>
								<div class="col-md-6 col-sm-6 ">
									<select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">
										<?php
											
											
											
											$result_cat_post  = "SELECT  id, nome FROM unidade where trash = 1";
											
											$resultado_cat_post = mysqli_query($conn, $result_cat_post);
											echo ' <option value=""> Selecione um setor</option>
';
											while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
												$selected = ($id_unidade == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['nome'].'</option>';
											}
										?>		
									</select>  
									<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
									
								</div>	
							</div>	
							
							<script>
								$(document).ready(function() {
									$('#instituicao').select2();
									
								});
								
							</script>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
								<div class="col-md-6 col-sm-6 ">
									<select type="text" class="form-control has-feedback-left" name="setor" id="setor"  placeholder="setor">
										<?php
											
											
											
											$result_cat_post  = "SELECT  id, nome FROM unidade_setor where trash = 1";
											
											$resultado_cat_post = mysqli_query($conn, $result_cat_post);
											echo ' <option value=""> Selecione um setor</option>
';
											while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
												$selected = ($unidade_setor == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['nome'].'</option>';
											}
										?>		
									</select>  
									<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um setor "></span>
									
								</div>	
							</div>	
							
							<script>
								$(document).ready(function() {
									$('#setor').select2();
									
									$('#instituicao').change(function() {
										$('#setor').select2().load('search/search-select-unidade-setor-change.php?id=' + $('#instituicao').val());
										
									});
								});
								
							</script>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Area</label>
								<div class="col-md-6 col-sm-6 ">
									<select type="text" class="form-control has-feedback-left" name="id_unidade_area" id="id_unidade_area"  placeholder="setor">
										<?php
											
											
											
											$result_cat_post  = "SELECT  id, nome FROM unidade_area where trash = 1";
											
											$resultado_cat_post = mysqli_query($conn, $result_cat_post);
											echo ' <option value=""> Selecione um setor</option>
';
											while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
												$selected = ($id_unidade_area == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['nome'].'</option>';
											}
										?>		
									</select>  
									<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um setor "></span>
									
								</div>	
							</div>	
							
							<script>
								$(document).ready(function() {
									$('#id_unidade_area').select2();
									
									$('#setor').change(function() {
										$('#id_unidade_area').select2().load('search/search-select-unidade-area-change.php?id=' + $('#setor').val());
										
									});
								});
								
							</script>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Dispositivo</label>
								<div class="col-md-6 col-sm-6 ">
									<select type="text" class="form-control has-feedback-left" name="id_device" id="id_device"  placeholder="Unidade">
										<?php
											
											
											
											$result_cat_post  = "SELECT  id, macadress FROM device where trash = 1";
											
											$resultado_cat_post = mysqli_query($conn, $result_cat_post);
											echo ' <option value=""> Selecione um Dispositivo</option>
';
											while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
												$selected = ($id_device == $row_cat_post['id']) ? 'selected' : ''; echo '<option value="'.$row_cat_post['id'].'" ' . $selected . ' >'.$row_cat_post['macadress'].'</option>';
											}
										?>		
									</select>  
									<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
									
								</div>	
							</div>	
							
							<script>
								$(document).ready(function() {
									$('#id_device').select2();
									
								});
								
							</script>
							
							
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($nome); ?>" >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($codigo); ?> " >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Serie</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="serie" class="form-control" type="text" name="serie"  value="<?php printf($serie); ?>" >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Patrimonio</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="patrimonio" class="form-control" type="text" name="patrimonio"  value="<?php printf($patrimonio); ?>" >
								</div>
							</div>
							<div class="clearfix"></div>
							<label for="message_mp">Observação :</label>
							<textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
								data-parsley-validation-threshold="10" value="<?php printf($obs); ?>"> <?php printf($obs); ?></textarea>
							

							
						 <div class="clearfix"></div>
							
							<div class="item form-group">
								<label class="col-form-label col-md-3 col-sm-3 label-align">Habilitar </label>
								<div class="col-md-6 col-sm-6 ">
									<div class="">
										<label>
											<input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="ativo"type="checkbox" class="js-switch" <?php if($ativo == "0"){printf("checked"); }?> />
										</label>
									</div>
								</div>
							</div>
							
							<div class="clearfix"></div>

							<div class="form-group row">
								<label class="col-form-label col-md-3 col-sm-3 "></label>
								<div class="col-md-3 col-sm-3 ">
									<center> 
										<button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
									</center>
								</div>
							</div>
							
							
						</form>
						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Imagem <small> </small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#">Settings 1</a>
									<a class="dropdown-item" href="#">Settings 2</a>
								</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						
						<div class="row">
							<div class="col-md-55">
								<?php if($img ==! "" ){ ?>
								<div class="thumbnail">
									<div class="image view view-first">
										<img style="width: 100%; display: block;" src="dropzone/car/<?php printf($img); ?>" alt="image" />
										<div class="mask">
											<p>Imagem</p>
											<div class="tools tools-bottom">
												<a href="dropzone/car/<?php printf($img); ?>" download><i class="fa fa-download"></i></a>
												
											</div>
										</div>
									</div>
									<div class="caption">
										<p>Imagem</p>
									</div>
								</div>
								<?php }; ?>
							</div>
							
							
							
							
							
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Trocar Imagem</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<form  class="dropzone" action="backend/register-car-upload-backend.php?id=<?php printf($codigoget); ?>" method="post">
					</form >
					
					
					
					</div>
					</div>

					
		<div class="x_panel">
			<div class="x_title">
				<h2>Ação</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<a class="btn btn-app"  href="register-car">
					<i class="glyphicon glyphicon-arrow-left"></i> Voltar
				</a>
				
				
				
			</div>
		</div>
		
		
		
		
	</div>
</div>
<!-- /page content -->

<!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>