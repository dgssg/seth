 <!-- page content -->

<?php
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }
?>
 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Usuario</small></h3>
              </div>

           
            </div>
             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Verificar Disponibilidade<small>Usuario</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<?php include 'frontend/register-account-register-check-frontend.php';?>



                  </div>
                </div>
              </div>
	       </div>
         <div class="x_panel">
                <div class="x_title">
                  <h2>Senha Padrão</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

             <div class="alert alert-success alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Senha Padrão!</strong> Ao criar um usuario novo ou resetar a senha, o sistema determina como  <strong>seth123</strong>.
                  </div>



                </div>
              </div>
              <div class="clearfix"></div>
            <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                   <a class="btn btn-app"  href="register-account">
                    <i class="glyphicon glyphicon-list-alt"></i> Consulta
                  </a>
                    <a class="btn btn-app"  href="register-account-new">
                    <i class="glyphicon glyphicon-plus"></i> Cadastro
                  </a>
            <!--      <a class="btn btn-app"  href="stakeholder/hospital/" target="_blank" >
                    <i class="fa fa-file-pdf-o"></i> Integração externa
                  </a>
                  <button id="copyButton">Copiar Integração para Area de Transferencia</button>
                  <button id="downloadButton">Download Atalho Integração externa</button>
                 <input type="hidden" name="mod" id="mod" value="<?php printf($key)?>" >
                 <!--  <canvas id="qrcode-canvas"></canvas>
<button onclick="gerarQRCode()">Gerar QR Code</button>
<a id="download-link" style="display: none;">Download QR Code</a> -->

                </div>
              </div>
            <div class="clearfix"></div>

           <!-- <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cadastro<small>Usuario</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<?php include 'frontend/register-account-register-frontend.php';?>



                  </div>
                </div>
              </div>
	       </div> -->
	       
         <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>

	        <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta<small>Usuario</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                                       
                    <div class="col-md-11 col-sm-11 ">
                      <div class="x_panel">
                        
                        <div class="x_content">
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="card-box table-responsive">
                                
                                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Nome</th>
                                      <th>Sobrenome</th>
                                      <th>Matricula</th>
                                      <th>Email</th>
                                      
                                      <th>Telefone</th>
                                      
                                      <th>Registro</th>
                                      
                                      <th>Ação</th>
                                      
                                    </tr>
                                  </thead>
                                  
                                  
                                  <tbody>
                                 
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    


                  </div>
                </div>
              </div>
	       </div>
                  
                  
                </div>
              </div>
<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script language="JavaScript">
  $(document).ready(function() {
    
    $('#datatable').dataTable( {
      "processing": true,
      "stateSave": true,
      "responsive": true,
           
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      }
      
      
      
    } );
    
    
    // Define a URL da API que retorna os dados da query em formato JSON
    const apiUrl = 'table/table-search-user.php';
    
    
    
    // Usa a função fetch() para obter os dados da API em formato JSON
    fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
      // Mapeia os dados para o formato esperado pelo DataTables
      const novosDados = data.map(item => [
        ``,
        item.nome,
        item.sobrenome,
        item.matricula,
        item.email,
       
        item.reg_date,
        
        `<a class="btn btn-app"  href="backend/register-account-reset-backend.php?id=${item.id}" >
        <i class="fa fa-refresh"></i> Resetar Senha
        </a>`,
        `<a class="btn btn-app" href="register-account-edit?id=${item.id}" onclick=" new PNotify({
          title: 'Editar',
          text: 'Abrindo Edição',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-edit"></i> Editar
        </a>
        <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Deletar!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Deletando!',
              'Seu arquivo será excluído.',
              'success'
            ),
            window.location = 'backend/register-account-trash-backend?id=${item.id}';
          }
        });">
          <i class="fa fa-trash"></i> Excluir
        </a>`
      ]);
      
      // Inicializa o DataTables com os novos dados
      const table = $('#datatable').DataTable();
      table.clear().rows.add(novosDados).draw();
      
      
      // Cria os filtros após a tabela ser populada
      $('#datatable').DataTable().columns([1, 2,3,4,5,6,7,8,9,10,11,12,13,14]).every(function(d) {
        var column = this;
        var theadname = $("#datatable th").eq([d]).text();
        var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
        var label = $('<label>' + theadname + ': </label>').appendTo(container);
        var select = $('<select  class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo('#userstable_filter').select2()
        .on('change', function() {
          var val = $.fn.dataTable.util.escapeRegex($(this).val());
          column.search(val ? '^' + val + '$' : '', true, false).draw();
        });
        
        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
      });
      
      var oTable = $('#datatable').dataTable();
      
      // Avança para a próxima página da tabela
      //oTable.fnPageChange('next');
      
      
      
    });
    
    
    // Armazenar a posição atual ao sair da página
    $(window).on('beforeunload', function() {
      var table = $('#datatable').DataTable();
      var pageInfo = table.page.info();
      localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
    });
    
    // Restaurar a posição ao retornar à página
    $(document).ready(function() {
      var storedPosition = localStorage.getItem('paginationPosition');
      if (storedPosition) {
        var pageInfo = JSON.parse(storedPosition);
        var table = $('#datatable').DataTable();
        table.page(pageInfo.page).draw('page');
      }
    });
    
    
    
    
  });
  
  
  
</script>
