<?php
	$sweet_salve = ($_GET["sweet_salve"]);
	$sweet_delet = ($_GET["sweet_delet"]);
	if ($sweet_delet == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
		Swal.fire({
				position: \'top-end\',
				icon: \'success\',
				title: \'Seu trabalho foi deletado!\',
				showConfirmButton: false,
				timer: 1500
		});
</script>';
		
		
	}
	if ($sweet_salve == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
				Swal.fire({
						position: \'top-end\',
						icon: \'success\',
						title: \'Seu trabalho foi salvo!\',
						showConfirmButton: false,
						timer: 1500
				});
		</script>';
		
		
	}
?>


<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>

<div class="right_col" role="main">
	
	<div class="">
		
		<div class="page-title">
			<div class="title_left">
				<h3>Registro</h3>
			</div>
			
			<div class="title_right">
				<div class="col-md-5 col-sm-5  form-group pull-right top_search">
					<div class="input-group">
						
						<span class="input-group-btn">
							
						</span>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Menu</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<a class="btn btn-app"  href="register-location-unit">
					<i class="glyphicon glyphicon-list-alt"></i> Consulta
				</a>
				<a class="btn btn-app"  href="register-location-unit-new">
					<i class="glyphicon glyphicon-plus"></i> Cadastro
				</a>
				
				
			</div>
		</div>
	
		
		
		<!-- page content -->
		
		
		
		
		
		
		
		
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Cadastro <small>da Unidade</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a class="dropdown-item" href="#">Settings 1</a>
									</li>
									<li><a class="dropdown-item" href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br />
						<form action="backend/register-location-unit-register-backend.php" method="post">
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
								<div class="col-md-6 col-sm-6 ">
									<input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($codigo); ?> " >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="instituicao" class="form-control" type="text" name="instituicao"  value="<?php printf($instituicao); ?> " >
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Site</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="site" class="form-control" type="text" name="site"  value="<?php printf($site); ?> " >
								</div>
							</div>
							<script>
								
								function limpa_formulário_cep() {
									//Limpa valores do formulário de cep.
									document.getElementById('rua').value=("");
									document.getElementById('bairro').value=("");
									document.getElementById('cidade').value=("");
									document.getElementById('uf').value=("");
									document.getElementById('ibge').value=("");
								}
								
								function meu_callback(conteudo) {
									if (!("erro" in conteudo)) {
										//Atualiza os campos com os valores.
										document.getElementById('rua').value=(conteudo.logradouro);
										document.getElementById('bairro').value=(conteudo.bairro);
										document.getElementById('cidade').value=(conteudo.localidade);
										document.getElementById('uf').value=(conteudo.uf);
										document.getElementById('ibge').value=(conteudo.ibge);
									} //end if.
									else {
										//CEP não Encontrado.
										limpa_formulário_cep();
										alert("CEP não encontrado.");
									}
								}
								
								function pesquisacep(valor) {
									
									//Nova variável "cep" somente com dígitos.
									var cep = valor.replace(/\D/g, '');
									
									//Verifica se campo cep possui valor informado.
									if (cep != "") {
										
										//Expressão regular para validar o CEP.
										var validacep = /^[0-9]{8}$/;
										
										//Valida o formato do CEP.
										if(validacep.test(cep)) {
											
											//Preenche os campos com "..." enquanto consulta webservice.
											document.getElementById('rua').value="...";
											document.getElementById('bairro').value="...";
											document.getElementById('cidade').value="...";
											document.getElementById('uf').value="...";
											document.getElementById('ibge').value="...";
											
											//Cria um elemento javascript.
											var script = document.createElement('script');
											
											//Sincroniza com o callback.
											script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
											
											//Insere script no documento e carrega o conteúdo.
											document.body.appendChild(script);
											
										} //end if.
										else {
											//cep é inválido.
											limpa_formulário_cep();
											alert("Formato de CEP inválido.");
										}
									} //end if.
									else {
										//cep sem valor, limpa formulário.
										limpa_formulário_cep();
									}
								};
								
							</script>
							<!-- Inicio do formulario -->
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CEP</label>
								<div class="col-md-6 col-sm-6 ">
									<input name="cep" type="text" id="cep"  maxlength="9"
										onblur="pesquisacep(this.value);"class="form-control"  value="<?php printf($cep); ?> ">
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Rua</label>
								<div class="col-md-6 col-sm-6 ">
									<input  name="endereco" type="text" id="rua" class="form-control"  value="<?php printf($endereco); ?> ">
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Bairro</label>
								<div class="col-md-6 col-sm-6 ">
									<input name="bairro" type="text" id="bairro"  class="form-control"  value="<?php printf($bairro); ?> ">
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cidade</label>
								<div class="col-md-6 col-sm-6 ">
									<input  name="cidade" type="text" id="cidade"  class="form-control"  value="<?php printf($cidade); ?> ">
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estado</label>
								<div class="col-md-6 col-sm-6 ">
									<input   name="estado" type="text" id="uf"  class="form-control"  value="<?php printf($estado); ?> ">
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">IBGE</label>
								<div class="col-md-6 col-sm-6 ">
									<input  name="ibge" type="text" id="ibge"  class="form-control"  value="<?php printf($ibge); ?> ">
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CNPJ</label>
								<div class="col-md-6 col-sm-6 ">
									<input  name="cnpj" type="text" id="cnpj"  class="form-control"  value="<?php printf($cnpj); ?> ">
								</div>
							</div>
							
						
							
							
							<div class="form-group row">
								<label class="col-form-label col-md-3 col-sm-3 "></label>
								<div class="col-md-3 col-sm-3 ">
									<center> 
										<button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
									</center>
								</div>
							</div>
							
							
						</form>
						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
		
		 					
					
					
					
					
					
					</div>
					</div>
					<!-- /page content -->
					
					<!-- /compose -->
					
					<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>