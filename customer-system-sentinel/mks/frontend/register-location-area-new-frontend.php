<?php
	$sweet_salve = ($_GET["sweet_salve"]);
	$sweet_delet = ($_GET["sweet_delet"]);
	if ($sweet_delet == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
		Swal.fire({
				position: \'top-end\',
				icon: \'success\',
				title: \'Seu trabalho foi deletado!\',
				showConfirmButton: false,
				timer: 1500
		});
</script>';
		
		
	}
	if ($sweet_salve == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
				Swal.fire({
						position: \'top-end\',
						icon: \'success\',
						title: \'Seu trabalho foi salvo!\',
						showConfirmButton: false,
						timer: 1500
				});
		</script>';
		
		
	}
?>
 

<div class="right_col" role="main">
	
	<div class="">
		
		<div class="page-title">
			<div class="title_left">
				<h3>Registro</h3>
			</div>
			
			<div class="title_right">
				<div class="col-md-5 col-sm-5  form-group pull-right top_search">
					<div class="input-group">
						
						<span class="input-group-btn">
							
						</span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Menu</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<a class="btn btn-app"  href="register-location-area">
					<i class="glyphicon glyphicon-list-alt"></i> Consulta
				</a>
				<a class="btn btn-app"  href="register-location-area-new">
					<i class="glyphicon glyphicon-plus"></i> Cadastro
				</a>
				
				
			</div>
		</div>
		 		
		
		
		<!-- page content -->
		
		
		
		
		
		
		
		
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Cadastro <small>da Area</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a class="dropdown-item" href="#">Settings 1</a>
									</li>
									<li><a class="dropdown-item" href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br />
						<form action="backend/register-location-unit-area-register-backend.php" method="post">
							
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
								<div class="col-md-6 col-sm-6 ">
									<input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
									<input id="id_unidade" class="form-control" name="id_unidade" type="hidden" value="<?php printf($id_unidade); ?>" >
									<input id="id_setor" class="form-control" name="id_setor" type="hidden" value="<?php printf($id_unidade); ?>" >

									
								</div>
							</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Instituição</label>
								<div class="col-md-6 col-sm-6 ">
									<select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">
										<option>Selecione uma Unidade</option>								
									</select>  
									<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
									
								</div>	
							</div>	
							
							<script>
								$(document).ready(function() {
									$('#instituicao').select2().load('search/search-select-unidade.php');
								});
								
							</script>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
								<div class="col-md-6 col-sm-6 ">
									<select type="text" class="form-control has-feedback-left" name="setor" id="setor"  placeholder="setor">
										<option>Selecione um Setor</option>	
									</select>  
									<span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um setor "></span>
									
								</div>	
							</div>	
							
							<script>
								$(document).ready(function() {
								 
									$('#setor').select2();
									$('#instituicao').change(function() {
										$('#setor').select2().load('search/search-select-unidade-setor-change.php?id=' + $('#instituicao').val());
										
									});
								});
								
							</script>							
							
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Andar</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="andar" class="form-control" type="text" name="andar"  value="<?php printf($andar); ?> " >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Area</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($nome); ?> " >
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observação</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="observacao" class="form-control" type="text" name="observacao"  value="<?php printf($observacao); ?> " >
								</div>
							</div>
							
						 							
							
							<div class="form-group row">
								<label class="col-form-label col-md-3 col-sm-3 "></label>
								<div class="col-md-3 col-sm-3 ">
									<center> 
										<button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
									</center>
								</div>
							</div>
							
							
						</form>
						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="x_panel">
			<div class="x_title">
				<h2>Ação</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<a class="btn btn-app"  href="register-location-area">
					<i class="glyphicon glyphicon-arrow-left"></i> Voltar
				</a>
				
				
				
			</div>
		</div>
		
		
		
		
	</div>
</div>
<!-- /page content -->

<!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>