<?php

include("../database/database.php");


$codigoget= $_POST['id'];
$meta= $_POST['meta'];
$value= $_POST['value'];
$interpretation= $_POST['interpretation'];
 


$stmt = $conn->prepare("UPDATE kpi SET meta = ? WHERE id= ?");
$stmt->bind_param("ss",$meta,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE kpi SET value = ? WHERE id= ?");
$stmt->bind_param("ss",$value,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE kpi SET interpretation = ? WHERE id= ?");
$stmt->bind_param("ss",$interpretation,$codigoget);
$execval = $stmt->execute();

 
header('Location: ../report-indicator-metas?sweet_salve=1');

?>
