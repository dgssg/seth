<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../../index.php");
	}
	$usuariologado=$_SESSION['usuario'];
	$instituicao =	$_SESSION['instituicao'];
	
	function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

$servername = "localhost";
$username = "cvheal47_root";
$port=3306;
$socket="";
$password = "cvheal47_root";
$db="cvheal47_seth_";
$dbname = "$db$instituicao";
$hoje = date('Y-m-d');
$ano = date('Y');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind
$stmt = $conn->prepare("INSERT INTO contrato (contrato_saas, termos_servico, politica_privacidade, usuario, ano, ip_address ) VALUES (?, ?, ?, ?, ?, ?)");
$stmt->bind_param("ssssss",$hoje,$hoje,$hoje,$usuariologado,$ano,$ipaddress);
$execval = $stmt->execute();

//echo "New records created successfully";

$stmt->close();
$conn->close();
	
 header('location:../customer/'.$instituicao.'/dashboard');

  ?>