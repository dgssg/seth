<?php
include("../database/database.php");
$query = "SELECT car.id_status,car.id, car.nome, car.codigo, car.seal_now, car.seal_before,car.serie,car.patrimonio,car.ativo, car.upgrade, car.reg_date,unidade_area.nome as 'area', unidade_setor.nome as 'setor', unidade.nome as 'unidade', device.nome as 'device', device.macadress,alarm_status.alarm FROM car LEFT JOIN unidade_area ON car.id_unidade_area = unidade_area.id LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade LEFT JOIN device ON car.id_device = device.id LEFT JOIN alarm_status ON alarm_status.id = car.id_status where car.trash = 1 and car.id_status != 3 GROUP BY  car.id ORDER by car.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
