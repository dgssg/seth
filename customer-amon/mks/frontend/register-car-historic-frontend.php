<?php
	$sweet_salve = ($_GET["sweet_salve"]);
	$sweet_delet = ($_GET["sweet_delet"]);
	if ($sweet_delet == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
		Swal.fire({
				position: \'top-end\',
				icon: \'success\',
				title: \'Seu trabalho foi deletado!\',
				showConfirmButton: false,
				timer: 1500
		});
</script>';
		
		
	}
	if ($sweet_salve == 1) {
		// Redirecionamento para a âncora "registro" usando JavaScript
		echo '<script>
				Swal.fire({
						position: \'top-end\',
						icon: \'success\',
						title: \'Seu trabalho foi salvo!\',
						showConfirmButton: false,
						timer: 1500
				});
		</script>';
		
		
	}
	
	
	function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
		$res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
		$con = 1;
		$var = '';
		while($con < $QuantidadeDeCaracteresDaChave ){
			$n = rand(0, 57); 
			if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
			}else{
				$var = $var.$n.$res[$n];
				$con++;
			}
		}
		    $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
	}
	//chamando a função.
	// echo chaveAlfaNumerica(5);
	//   echo nl2br("\r\n");
	/* A uniqid, like: 4b3403665fea6 
	printf("uniqid(): %s\r\n", uniqid());
	
	/* We can also prefix the uniqid, this the same as 
	* doing:
	*
	* $uniqid = $prefix . uniqid();
	* $uniqid = uniqid($prefix);
	
	printf("uniqid('php_'): %s\r\n", uniqid('php_'));
	
	/* We can also activate the more_entropy parameter, which is 
	* required on some systems, like Cygwin. This makes uniqid()
	* produce a value like: 4b340550242239.64159797
	
	printf("uniqid('', true): %s\r\n", uniqid('', true));
	*/
	class UUID {
		public static function v3($namespace, $name) {
			if(!self::is_valid($namespace)) return false;
			
			// Get hexadecimal components of namespace
			$nhex = str_replace(array('-','{','}'), '', $namespace);
			
			// Binary Value
			$nstr = '';
			
			// Convert Namespace UUID to bits
			for($i = 0; $i < strlen($nhex); $i+=2) {
				$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
			}
			
			// Calculate hash value
			$hash = md5($nstr . $name);
			
			return sprintf('%08s-%04s-%04x-%04x-%12s',
				
				// 32 bits for "time_low"
				substr($hash, 0, 8),
				
				// 16 bits for "time_mid"
				substr($hash, 8, 4),
				
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 3
				(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,
				
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
				
				// 48 bits for "node"
				substr($hash, 20, 12)
			);
		}
		
		public static function v4() {
			return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				
				// 32 bits for "time_low"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff),
				
				// 16 bits for "time_mid"
				mt_rand(0, 0xffff),
				
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand(0, 0x0fff) | 0x4000,
				
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand(0, 0x3fff) | 0x8000,
				
				// 48 bits for "node"
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
			);
		}
		
		public static function v5($namespace, $name) {
			if(!self::is_valid($namespace)) return false;
			
			// Get hexadecimal components of namespace
			$nhex = str_replace(array('-','{','}'), '', $namespace);
			
			// Binary Value
			$nstr = '';
			
			// Convert Namespace UUID to bits
			for($i = 0; $i < strlen($nhex); $i+=2) {
				$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
			}
			
			// Calculate hash value
			$hash = sha1($nstr . $name);
			
			return sprintf('%08s-%04s-%04x-%04x-%12s',
				
				// 32 bits for "time_low"
				substr($hash, 0, 8),
				
				// 16 bits for "time_mid"
				substr($hash, 8, 4),
				
				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 5
				(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
				
				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
				
				// 48 bits for "node"
				substr($hash, 20, 12)
			);
		}
		
		public static function is_valid($uuid) {
			return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
				'[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
		}
	}
	
	// Usage
	// Named-based UUID.
	
	$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
	$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
	
	// Pseudo-random UUID
	
	$v4uuid = UUID::v4();
	

?>
 
<?php
	$codigoget = ($_GET["id"]);
	
	// Create connection
	include("database/database.php");// remover ../
	
	
	$query = "SELECT car.id_status,car.id, car.nome, car.codigo, car.seal_now, car.seal_before,car.serie,car.patrimonio,car.ativo, car.upgrade, car.reg_date,unidade_area.nome as 'area', unidade_setor.nome as 'setor', unidade.nome as 'unidade', device.nome as 'device', device.macadress,alarm_status.alarm FROM car LEFT JOIN unidade_area ON car.id_unidade_area = unidade_area.id LEFT JOIN unidade_setor ON unidade_setor.id = unidade_area.id_unidade_setor LEFT JOIN unidade ON unidade.id = unidade_setor.id_unidade LEFT JOIN device ON car.id_device = device.id LEFT JOIN alarm_status ON alarm_status.id = car.id_status where car.trash = 1 and car.id = $codigoget";
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($id_status,$id, $nome, $codigo, $seal_now, $seal_before,$serie,$patrimonio,$ativo, $upgrade, $reg_date,$area, $setor, $unidade, $device, $macadress,$alarm);
		
		
		
		while ($stmt->fetch()) {
			
		}
	}
?>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
<div class="right_col" role="main">
	
	<div class="">
		
		<div class="page-title">
			<div class="title_left">
				<h3>Historico Carrinho de Emergencia</h3>
			</div>
			
			<div class="title_right">
				<div class="col-md-5 col-sm-5  form-group pull-right top_search">
					<div class="input-group">
						
						<span class="input-group-btn">
							
						</span>
					</div>
				</div>
			</div>
		</div>
		
		 
		
		
		
		
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Historico <small>Carrinho de Emergência</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a class="dropdown-item" href="#">Settings 1</a>
									</li>
									<li><a class="dropdown-item" href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<br />
						<input id="car" class="form-control" type="hidden" name="car"  value="<?php printf($codigoget); ?> " readonly >

						
													
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($unidade); ?> " readonly >
							</div>
						</div>
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Setor</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($setor); ?> " readonly >
							</div>
						</div>
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Area</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($area); ?> " readonly >
							</div>
						</div>
							
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Dispositivo</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($device); ?> " readonly >
							</div>
						</div>
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Chave</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($macadress); ?> " readonly >
							</div>
						</div>
							
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Carrinho</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($nome); ?> " readonly>
								</div>
							</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($codigo); ?> " readonly >
								</div>
							</div>
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Serie</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($serie); ?> " readonly >
							</div>
						</div>
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Patrimonio</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($patrimonio); ?> " readonly >
							</div>
						</div>
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Lacre Atual</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($seal_now); ?> " readonly >
							</div>
						</div>
						<div class="item form-group">
							<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Lacre Anterior</label>
							<div class="col-md-6 col-sm-6 ">
								<input id="nome" class="form-control" type="text" name="nome"  value="<?php printf($seal_before); ?> " readonly >
							</div>
						</div>
							<div class="item form-group">
								<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observação</label>
								<div class="col-md-6 col-sm-6 ">
									<input id="observacao" class="form-control" type="text" name="observacao"  value="<?php printf($observacao); ?> "readonly >
								</div>
							</div>
							
						 
							
							
							
						 						
						
						
						
						
						
					</div>
				</div>
			</div>
		</div>
		
		
		
		
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Filtro</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a class="dropdown-item" href="#">Settings 1</a>
									</li>
									<li><a class="dropdown-item" href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
						
						
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="clearfix"></div>
		
		<div class="row" style="display: block;">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Historico<small>Carrinho</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="#">Settings 1</a>
									<a class="dropdown-item" href="#">Settings 2</a>
								</div>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						
						<div class="col-md-12 col-sm-12 ">
							<div class="x_panel">
								
								<div class="x_content">
									<div class="row">
										<div class="col-sm-11">
											<div class="card-box table-responsive">
												
												
												<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
													<thead>
														<tr>
															<th>#</th>
															<th>Lacre Anterior</th>
															
															<th>Lacre Atual</th>
															<th>Usuario</th>
															
															<th>Observação</th>
															
															<th>Assinatura</th>
														 															<th>Registro</th>
															
															<th>Ação</th>
															
														</tr>
													</thead>
													
													
													<tbody>
														
														
													</tbody>
												</table>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
						
						
					</div>
				</div>
			</div>
		</div>

		
	 
		<div class="x_panel">
			<div class="x_title">
				<h2>Ação</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
							class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<a class="btn btn-app"  href="register-car">
					<i class="glyphicon glyphicon-arrow-left"></i> Voltar
				</a>
				
				
				
			</div>
		</div>
		
		
		
		
	</div>
</div>
<!-- /page content -->

<script language="JavaScript">
	$(document).ready(function() {
		
		$('#datatable').dataTable( {
			"processing": true,
			"stateSave": true,
			"responsive": true,
			
			
			
			
			
			"language": {
				"loadingRecords": "Carregando dados...",
				"processing": "Processando  dados...",
				"infoEmpty": "Nenhum dado a mostrar",
				"emptyTable": "Sem dados disponíveis na tabela",
				"zeroRecords": "Não há registros a serem exibidos",
				"search": "Filtrar registros:",
				"info": "Mostrando página _PAGE_ de _PAGES_",
				"infoFiltered": " - filtragem de _MAX_ registros",
				"lengthMenu": "Mostrar _MENU_ registros",
				
				"paginate": {
					"previous": "Página anterior",
					"next": "Próxima página",
					"last": "Última página",
					"first": "Primeira página",
					
					
					
				}
			}
			
			
			
		} );
		
		
		// Define a URL da API que retorna os dados da query em formato JSON
		const apiUrl = 'table/table-search-car-register-historic.php?id=' + $('#car').val();
		
		
		
		// Usa a função fetch() para obter os dados da API em formato JSON
		fetch(apiUrl)
		.then(response => response.json())
		.then(data => {
			// Mapeia os dados para o formato esperado pelo DataTables
			const novosDados = data.map(item => [
				``,
				item.seal_before,
				item.seal_now,
				item.nome,
				item.obs,
				
			 
				item.id_signature,
			 
				item.reg_date,
				
				`<a class="btn btn-app"  href="car-historic-view?id=${item.id}" target="_blank">
				<i class="fa fa-file"></i> Visualizar
				</a>`
			]);
			
			// Inicializa o DataTables com os novos dados
			const table = $('#datatable').DataTable();
			table.clear().rows.add(novosDados).draw();
			
			
			// Cria os filtros após a tabela ser populada
			$('#datatable').DataTable().columns([1, 2,3,6]).every(function(d) {
				var column = this;
				var theadname = $("#datatable th").eq([d]).text();
				var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
				var label = $('<label>' + theadname + ': </label>').appendTo(container);
				var select = $('<select  class="form-control my-1"><option value="">' +
					theadname + '</option></select>').appendTo('#userstable_filter').select2()
				.on('change', function() {
					var val = $.fn.dataTable.util.escapeRegex($(this).val());
					column.search(val ? '^' + val + '$' : '', true, false).draw();
				});
				
				column.data().unique().sort().each(function(d, j) {
					select.append('<option value="' + d + '">' + d + '</option>');
				});
			});
			
			var oTable = $('#datatable').dataTable();
			
			// Avança para a próxima página da tabela
			//oTable.fnPageChange('next');
			
			
			
		});
		
		
		// Armazenar a posição atual ao sair da página
		$(window).on('beforeunload', function() {
			var table = $('#datatable').DataTable();
			var pageInfo = table.page.info();
			localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
		});
		
		// Restaurar a posição ao retornar à página
		$(document).ready(function() {
			var storedPosition = localStorage.getItem('paginationPosition');
			if (storedPosition) {
				var pageInfo = JSON.parse(storedPosition);
				var table = $('#datatable').DataTable();
				table.page(pageInfo.page).draw('page');
			}
		});
		
		
		
		
	});
	
	
	
</script>


<!-- /compose -->

<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>