<?php include("database/database.php"); ?>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Perfil</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">
                 
                    
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Configuração<small>Perfil</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3  profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img src="logo/img.jpg" alt="..." class="img-circle profile_img" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
             
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="x_content">
                    <div class="row">
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-building-o"></i>
                          </div>
                          <div class="count"> 
                          <?php 
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM unidade "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?> </div>

                          <h3>Instituição</h3>
                          <p>Informação do sistema</p>
                        </div>
                      </div>
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-cog"></i>
                          </div>
                          <div class="count"> <?php 
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM car "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?> </div>

                          <h3>Carrinhos de Emergência</h3>
                          <p>Informação do sistema</p>
                        </div>
                      </div>
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-check-circle-o"></i>
                          </div>
                          <div class="count"> <?php 
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM device "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?> </div>

                          <h3> Dispositivo</h3>
                          <p>Informação do sistema</p>
                        </div>
                      </div>
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-qrcode"></i>
                          </div>
                          <div class="count"><?php 
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM reg_date_alarm "); 
                          $stmt->execute();
                          $stmt->bind_result($result);
                          while ( $stmt->fetch()) { 
                          printf($result);
                          }
                          ?> </div>

                          <h3>Alarmes</h3>
                          <p>Informação do sistema</p>
                        </div>
                      </div>
                    </div>
                         
                    </div>
                       </div>
                      </div>
                         </div>
                      </div>

                   
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mudar Senha</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <form class=""action="backend/profile-change-password-backend.php?usuario=<?php printf(	$_SESSION['id_user']) ?>" method="post">
                <fieldset>


            <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 ">Password</label>
                        <div class="col-md-12 col-sm-12 ">
                         <input type="password" placeholder="Password" id="password"  name="password" required>
                        </div>
                      </div>
            <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 ">Password</label>
                        <div class="col-md-12 col-sm-12 ">
                         <input type="password" placeholder="Confirm Password" id="confirm_password" required>
                        </div>
                      </div>

            <input type="submit" class="btn btn-primary" onclick="new PNotify({
                                    title: 'Registrado',
                                    text: 'Informações registrada!',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });" />
            </fieldset>
            </form>

                   

                  </div>
                </div>


<script>
function show() {
  var senha = document.getElementById("senha");
  if (senha.type === "password") {
    senha.type = "text";
  } else {
    senha.type = "password";
  }
}
</script>
<script >
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Senha divergentes");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

            
            
            
          </div>
        </div>
        <!-- /page content -->
