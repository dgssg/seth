<?php

include("../database/database.php");

$codigoget= $_POST['id'];
$instituicao= $_POST['instituicao'];
$endereco= $_POST['endereco'];
$site= $_POST['site'];
$codigo= $_POST['codigo'];

$cep= $_POST['cep'];
$bairro= $_POST['bairro'];
$cidade= $_POST['cidade'];
$estado= $_POST['estado'];
$ibge= $_POST['ibge'];
$cnpj= $_POST['cnpj'];

$stmt = $conn->prepare("UPDATE unidade SET codigo= ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET nome= ? WHERE id= ?");
$stmt->bind_param("ss",$instituicao,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET endereco= ? WHERE id= ?");
$stmt->bind_param("ss",$endereco,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET site= ? WHERE id= ?");
$stmt->bind_param("ss",$site,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade SET cep= ? WHERE id= ?");
$stmt->bind_param("ss",$cep,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET bairro= ? WHERE id= ?");
$stmt->bind_param("ss",$bairro,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET cidade= ? WHERE id= ?");
$stmt->bind_param("ss",$cidade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET estado= ? WHERE id= ?");
$stmt->bind_param("ss",$estado,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET ibge= ? WHERE id= ?");
$stmt->bind_param("ss",$ibge,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE unidade SET cnpj= ? WHERE id= ?");
$stmt->bind_param("ss",$cnpj,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../register-location-unit?sweet_salve=1');
?>