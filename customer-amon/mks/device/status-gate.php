<?php
    
    include("../database/database.php");
    
    $codigoget = ($_GET["macadress"]);
    
    $dataHoraAtual = date('Y-m-d H:i:s');


    $stmt = $conn->prepare("UPDATE device SET status_time = ? WHERE macadress = ?");
    $stmt->bind_param("ss",  $dataHoraAtual,$codigoget);
    $execval = $stmt->execute();
    $stmt->close();
    
?>