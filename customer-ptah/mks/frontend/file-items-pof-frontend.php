<?php



// Create connection

include("database/database.php");


?>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>  Itens <small>Orçamentário </small></h3>
      </div>


    </div>

    <div class="clearfix"></div>

    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Menu <small>Orçamentário</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings 1</a>
                  <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app"  href="file-items">
              <i class="glyphicon glyphicon-th-list"></i> EAP
            </a>
            <a class="btn btn-app"  href="file-items-eaq1">
              <i class="glyphicon glyphicon-th-list"></i> EAQ 1
            </a>
            <a class="btn btn-app"  href="file-items-eaq2">
              <i class="glyphicon glyphicon-th-list"></i> EAQ 2
            </a>
            <a class="btn btn-app"  href="file-items-poimbs">
              <i class="glyphicon glyphicon-th-list"></i> POI MBS
            </a>
            <a class="btn btn-app"  href="file-items-pof">
              <i class="glyphicon glyphicon-th-list"></i> POF
            </a>
            <a class="btn btn-app"  href="file-items-mof">
              <i class="glyphicon glyphicon-th-list"></i> MOF
            </a>

          </div>
        </div>
      </div>
    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Orçamentário</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


<?php include 'frontend/file-items-list-pof-frontend.php';?>



        </div>
      </div>


    </div>
  </div>
  <!-- Contador de caracter -->
  <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

  <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
  <script type="text/javascript">
  $("meuPrimeiroDropzone").dropzone({ url: "upload.php" });
  Dropzone.options.meuPrimeiroDropzone = {
    paramName: "fileToUpload",
    dictDefaultMessage: "Arraste seus arquivos para cá!",
    maxFilesize: 300,
    accept: function(file, done) {
      if (file.name == "olamundo.png") {
        done("Arquivo não aceito.");
      } else {
        done();
      }
    }
  }
  </script>
