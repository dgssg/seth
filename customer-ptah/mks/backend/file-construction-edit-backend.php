<?php

include("../database/database.php");

$id = $_POST['id'];
$cod = $_POST['cod'];
$orc = $_POST['orc'];
$des = $_POST['des'];
$cep = $_POST['cep'];
$adress = $_POST['adress'];
$district= $_POST['district'];
$city = $_POST['city'];
$state = $_POST['state'];
$ibge = $_POST['ibge'];
$id_customer = $_POST['customer'];
$construction_finish = $_POST['construction_finish'];
$legislation = $_POST['legislation'];
$BDI= $_POST['BDI'];
$ISS= $_POST['ISS'];
$BDI_explicit= $_POST['BDI_explicit'];
$lag_indice=$_POST['lag_indice'];
$id_type= $_POST['id_type'];
$id_status= $_POST['id_status'];



$stmt = $conn->prepare("UPDATE construction SET cod= ? WHERE id= ?");
$stmt->bind_param("ss",$cod,$id);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE construction SET orc= ? WHERE id= ?");
$stmt->bind_param("ss",$orc,$id);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE construction SET des= ? WHERE id= ?");
$stmt->bind_param("ss",$des,$id);
$execval = $stmt->execute();
$stmt->close();



echo "<script>alert('Movido para lixeira!');document.location='../file-construction'</script>";
header('Location: ../file-construction');
?>
