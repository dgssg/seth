<?php
include("database/database.php");
 session_start();
if(!isset($_SESSION['usuario'])){
	header ("Location: ../index.php");
}
if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
}
if( $_SESSION['mod'] != $mod ){
    header ("Location: ../index.php");
}
  $usuariologado=$_SESSION['usuario'];
  $instituicaologado=$_SESSION['instituicao'];
  $setorlogado=$_SESSION['setor'];

  // Adiciona a Função display_campo($nome_campo, $tipo_campo)
  //require_once "personalizacao_display.php";
$setorlogado=$_SESSION['setor'];

	// Adiciona a Função display_campo($nome_campo, $tipo_campo)
	//require_once "personalizacao_display.php";

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />

    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>

   <!-- Bootstrap -->
    <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../framework/build/css/custom.min.css" rel="stylesheet">

      <!-- PNotify -->
  <link href="../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">

 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
   <!-- <script src="jquery.min.js" ></script> -->
	 <!-- Datatables -->

 	<link href="../../framework/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
 	<link href="../../framework/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
 	<link href="../../framework/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
 	<link href="../../framework/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
 	<link href="../../framework/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
 	 <!-- Legenda OS -->
 		<link href="../../framework/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard" class="site_title"><i class="fa fa-500px"></i> <span>SETH Projeto</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="logo/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?php printf($usuariologado) ?></h2>
                <p class="glyphicon glyphicon-time" id="countdown"></p>
               <span>
              <script> document.write(new Date().toLocaleDateString()); </script>
              </span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

  <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Início <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                            <li><a href="dashboard">Dashboard</a></li>
                         <!-- <li><a href="index3.php">Dashboard3</a></li>  -->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i>Arquivos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="file-construction">Obras </a></li>
                      <li><a href="file-items">Itens Orçamentarios</a></li>
                      <li><a href="file-input">Insumos</a></li>
                      <li><a href="file-composition">Composição</a></li>
                      <li><a href="file-manufacturer">Cliente</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-table"></i>Orçamento <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="budget-synthetic">Sintetico</a></li>
                      <li><a href="budget-analytical">Analitico</a></li>
                      <li><a href="budget-input-abc-curve">Curva ABC Insumos</a></li>
                      <li><a href="budget-service-abc-curve">Curva ABC Serviço</a></li>
                      <li><a href="budget-bdi">BDI</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-table"></i> Cronograma <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="schedule-registration">Cadastro</a></li>
                      <li><a href="schedule-s-curve">Curva S</a></li>
                      <li><a href="schedule-gannt">Gannt</a></li>
                      <li><a href="schedule-physicist">Fisico</a></li>
                      <li><a href="schedule-physical-financial">Fisico Financeiro</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Recursos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                      <li><a href="resources-selectivity">Seletividade</a></li>
                      <li><a href="resources-administration">Administração</a></li>
                      <li><a href="resources-masonry">Alvenaria</a></li>
                      <li><a href="resources-luminotechnic">Luminotecnico</a></li>
                      <li><a href="resources-diagram">Diagrama</a></li>

                    </ul>
                  </li>


                  <li><a><i class="fa fa-bar-chart-o"></i> Lançamento  <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="launch-launch">Lançamento</a></li>
                      <li><a href="launch-mensure">Medição</a></li>

                    </ul>
                  </li>
                  <li><a><i class="fa fa-cart-arrow-down"></i> Banco &amp; Encargos<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="database-copel">Copel</a></li>
                        <li><a href="database-encargos">Encargos</a></li>
                        <li><a href="database-pini">Pini</a></li>
                        <li><a href="database-sbc">SBC</a></li>
                        <li><a href="database-sicro2">SICRO2</a></li>
                        <li><a href="database-sinapi">SINAPI</a></li>
                        <li><a href="database-stabile">STABILE</a></li>
                        <li><a href="database-tcpo">TCPO</a></li>
                        <li><a href="database-volare">Volare</a></li>


                    </ul>
                  </li>
                  <li><a><i class="fa fa-cart-arrow-down"></i> Memorial &amp; Documento<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="document-document">Documento</a></li>
                        <li><a href="document-leed">Leed</a></li>
                        <li><a href="document-normative">Normativa</a></li>
                        <li><a href="document-equivalence">Equivalencia</a></li>
                        <li><a href="document-liewin">Liewin</a></li>
                        <li><a href="document-h-equipament">H Equipamento</a></li>
                        <li><a href="document-h-time">H Hora</a></li>
                        <li><a href="document-price">Cotação</a></li>
                        <li><a href="document-wbs">WBS</a></li>


                    </ul>
                  </li>
                    <li><a><i class="fa fa-cart-arrow-down"></i> Projeto<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="project-definition">Definição</a></li>
                        <li><a href="project-planning">Planejamento</a></li>
                        <li><a href="project-execution">Execulsão</a></li>
                        <li><a href="project-control">Controle</a></li>
                        <li><a href="project-finalization">Finalização</a></li>



                    </ul>
                  </li>
                 <br>
                 <br>
                </ul>
              </div>





            </div>
            <!-- /sidebar menu -->
 <br>
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                <script>
                var goFS = document.getElementById("goFS");
                goFS.addEventListener("click", function() {
                  document.body.requestFullscreen();
                }, false);
              </script>

              </a>
              <a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="lockscreen">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="SPDA" href="spda/"target="_blank">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Dimencionamento" href="dimencionamento/"target="_blank">
                <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Modelagem" href="modelagem/"target="_blank">
                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
              </a>
               <a data-toggle="tooltip" data-placement="top" title="Projeto Eletrico Hospitalar" href="poh/"target="_blank">
                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
              </a>




         <!--       <a data-toggle="tooltip" data-placement="top" title="Projetos" href="http://www.seth.mksistemasbiomedicos.com.br/"target="_blank">
                <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
              </a> -->

            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
 <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                        <img src="logo/img.jpg" alt=""><?php printf($usuariologado) ?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="profile">   <i class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></i> Perfil </a>
                                             <a class="dropdown-item"  href="documentation">   <i class="glyphicon glyphicon-file pull-right" aria-hidden="true"></i> Documentos </a>

                      <a class="dropdown-item"  href="user">   <i class="glyphicon glyphicon-user pull-right" aria-hidden="true"></i> Usuario </a>
                    <!--    <a class="dropdown-item"  href="javascript:;">
                          <span class="badge bg-red pull-right">100%</span>
                          <span>Settings</span>
                        </a>
              <!--      <a class="dropdown-item"  href="javascript:;">Help</a> -->
                      <a class="dropdown-item"  href="login"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>

                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>

                      <span class="badge bg-green"> <?php include 'workflow/envelope.php';?> </span>
                    </a>


                    </li>
                      <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>

                      <span class="badge bg-red"> <?php include 'workflow/envelope-signature.php';?> </span>
                    </a>


                    </li>
                    <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>

                      <span class="badge bg-blue"> <?php include 'workflow/envelope-signature.php';?> </span>
                    </a>


                    </li>
                    <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>

                      <span class="badge bg-green"> <?php include 'workflow/envelope-signature.php';?> </span>
                    </a>


                    </li>


                    <!--
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <div class="text-center">
                          <a class="dropdown-item">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li> -->
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

        <!-- /top navigation -->
        <!-- page content -->


<?php include 'tools/tools-navegation.php';?>
<?php include 'api.php';?>

<?php include 'frontend/';?>


        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           ©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://mksistemasbiomedicos.com.br"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery
    <script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../framework/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../framework/build/js/custom.min.js"></script>




    <!-- jQuery-->
    <script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap-->
    <script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../framework/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../../framework/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../framework/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../framework/build/js/custom.min.js"></script>
 <!-- PNotify -->
   <script src="../../framework/vendors/pnotify/dist/pnotify.js"></script>
   <script src="../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
   <script src="../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>

   <script src="../../framework/assets/js/select2.min.js"></script>

      <!-- bootstrap-wysiwyg -->
    <script src="../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../../framework/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../../framework/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../../framework/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../../framework/vendors/starrr/dist/starrr.js"></script>
   <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>


	</body>
  <script language="JavaScript">

 
 history.pushState(null, null, document.URL);
 window.addEventListener('popstate', function () {
     history.pushState(null, null, document.URL);
 });
 </script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;
  var timer;
  var interval;

  function countdown() {

      seconds--;
    if (seconds < 0) {
		clearInterval(interval);
      // Change your redirection link here
      window.location = "https://seth.mksistemasbiomedicos.com.br";
    } else if (seconds === 60) {
      // Show warning 60 seconds before redirect
      var warning = document.createElement("div");
      warning.innerHTML = "O sistema será desconectado por motivos de segurança em 60 segundos. Clique aqui para continuar no sistema e reiniciar o tempo.";
      warning.style.backgroundColor = "yellow";
      warning.style.padding = "10px";
      warning.style.position = "fixed";
      warning.style.bottom = "0";
      warning.style.right = "0";
      warning.style.zIndex = "999";
      warning.addEventListener("click", function() {
        warning.style.display = "none";
        clearTimeout(seconds);
		seconds = 1080;
        countdown();
      });
      document.body.appendChild(warning);
	  countdown();
    } else {
      // Update remaining seconds
      document.getElementById("countdown").innerHTML = seconds;
      // Count down using javascript
	
      timer = setTimeout(countdown, 1000);
    }
  }

  // Run countdown function
  countdown();
  
  
  function stopCountdown() {
    clearInterval(seconds);
  }
 
    // Reset countdown on user interaction
	document.addEventListener("click", function() {
    clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });
  document.addEventListener("mousemove", function() {
	seconds--;
    //clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });

</script>

 </html>
