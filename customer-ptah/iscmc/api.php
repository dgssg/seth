<?php include 'api/api-dialogflow.php';?>    
<?php include 'api/api-clarity.php';?>
<?php include 'tools/tools-navegation.php';?>
<?php include 'api/api-url.php';?>
<script type="text/javascript">
$.ajaxSetup({ cache: false })

function limpaUrl() {     //função
    urlpg = $(location).attr('href');   //pega a url atual da página
    urllimpa = urlpg.split("?")[0]      //tira tudo o que estiver depois de '?'

    window.history.replaceState(null, null, urllimpa); //subtitui a url atual pela url limpa
  
}
limpaUrl();
</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>