<!DOCTYPE html>
<html>
<script src="myailib.js"></script>
<script src="myplotlib.js"></script>
<body>
<canvas id="myCanvas" width="400px" height="400px" style="width:100%;max-width:400px;border:1px solid black"></canvas>
<p>Train me to find the line of best fit:</p>
<p>
<button onclick="train(100)">100 times</button>
<button onclick="train(200)">200 times</button>
<button onclick="train(300)">300 times</button>
<button onclick="train(500)">500 times</button>
</p>

<div id="demo"></div>
<script>
// Create a Trainer Object
xArray = [32,53,61,47,59,55,52,39,48,52,45,54,44,58,56,48,44,60];
yArray = [31,68,62,71,87,78,79,59,75,71,55,82,62,75,81,60,82,97];
let myTrainer = new Trainer(xArray, yArray);

// Create a Plotter Object
let myPlotter = new XYPlotter("myCanvas");
myPlotter.transformXY();
myPlotter.transformMax(100, 100);

// Plot the Points
myPlotter.plotPoints(xArray.length, xArray, yArray, "blue");

function train(iter) {
  myTrainer.train(iter);
  // Display Guessed Results
  document.getElementById("demo").innerHTML = 
  "<p>Slope: " + myTrainer.weight.toFixed(2) + "</p>" +
  "<p>Bias:  " + myTrainer.bias.toFixed(2) + "</p>" +
  "<p>Cost:  " + myTrainer.cost.toFixed(2);
  myPlotter.plotLine(0, myTrainer.bias, myPlotter.xMax,myPlotter.xMax*(myTrainer.weight)+(myTrainer.bias), "black");
}

</script>

<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

<!-- Datatables -->
 <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
 <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
 <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
 <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
 <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
 <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
 <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
 <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- bootstrap-daterangepicker -->
 <script src="../../framework/vendors/moment/min/moment.min.js"></script>
 <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
 <!-- bootstrap-datetimepicker -->    
 <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
<script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
<script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>

<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
<script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
<script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
 <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

</body>
<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

<script language="JavaScript">
<!--
function verfonte()
{
if (event.button==2)
{
window.alert('Este recurso foi desativado')
}
}
document.onmousedown=verfonte
// -->
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
  history.pushState(null, null, document.URL);
});
</script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;

  function countdown() {
      seconds = seconds - 1;
      if (seconds < 0) {
          // Chnage your redirection link here
          window.location = "https://seth.mksistemasbiomedicos.com.br";
      } else {
          // Update remaining seconds
          document.getElementById("countdown").innerHTML = seconds;
          // Count down using javascript
          window.setTimeout("countdown()", 1000);
      }
  }

  // Run countdown function
  countdown();

</script>
<script  type="text/javascript">
$(function () {
             $('#myDatepicker').datetimepicker();
         });
 
 $('#myDatepicker2').datetimepicker({
     format: 'DD.MM.YYYY'
 });
 
 $('#myDatepicker3').datetimepicker({
     format: 'hh:mm '
 });
  $('#myDatepicker8').datetimepicker({
     format: 'hh:mm '
 });
 
 $('#myDatepicker4').datetimepicker({
     ignoreReadonly: true,
     allowInputToggle: true
 });

 $('#datetimepicker6').datetimepicker();
 
 $('#datetimepicker7').datetimepicker({
     useCurrent: false
 });
 
 $("#datetimepicker6").on("dp.change", function(e) {
     $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
 });
 
 $("#datetimepicker7").on("dp.change", function(e) {
     $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
 });
 
</script>
<script>
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="duallistbox_demo1[]"]').val());
 return false;
});
</script>
<script>
var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
$("#demoform").submit(function() {
 alert($('[name="instituicao_id"]').val());
 return false;
});
</script>


</html>

</html>
