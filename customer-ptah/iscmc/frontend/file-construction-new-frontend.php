<?php



// Create connection

include("database/database.php");


?>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
            <h3>  Gestão de <small>Obras </small></h3>
            </div>


          </div>

          <div class="clearfix"></div>

          <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Menu <small>Gestão</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="file-construction">
                    <i class="glyphicon glyphicon-filter"></i> Obras
                  </a>
                  <a class="btn btn-app"  href="file-construction-new">
                    <i class="glyphicon glyphicon-plus-sign"></i> Novo
                  </a>
                  <a class="btn btn-app"  href="file-construction-tools">
                    <i class="glyphicon glyphicon glyphicon-cog"></i> Parametro
                  </a>

                </div>
              </div>
            </div>
       </div>

<div class="x_panel">
              <div class="x_title">
                <h2>Obras</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <br />
                <form action="backend/file-construction-new-edit-backend.php" method="post">


                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="cod" class="form-control" type="text" name="cod"  >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Orçamento</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="orc" class="form-control" type="text" name="orc"  >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Descrição</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="des" class="form-control" type="text" name="des"  >
                    </div>
                  </div>
                  <script>

                  function limpa_formulário_cep() {
                    //Limpa valores do formulário de cep.
                    document.getElementById('adress').value=("");
                    document.getElementById('district').value=("");
                    document.getElementById('city').value=("");
                    document.getElementById('state').value=("");
                    document.getElementById('ibge').value=("");
                  }

                  function meu_callback(conteudo) {
                    if (!("erro" in conteudo)) {
                      //Atualiza os campos com os valores.
                      document.getElementById('adress').value=(conteudo.logradouro);
                      document.getElementById('district').value=(conteudo.district);
                      document.getElementById('city').value=(conteudo.localidade);
                      document.getElementById('state').value=(conteudo.uf);
                      document.getElementById('ibge').value=(conteudo.ibge);
                    } //end if.
                    else {
                      //CEP não Encontrado.
                      limpa_formulário_cep();
                      alert("CEP não encontrado.");
                    }
                  }

                  function pesquisacep(valor) {

                    //Nova variável "cep" somente com dígitos.
                    var cep = valor.replace(/\D/g, '');

                    //Verifica se campo cep possui valor informado.
                    if (cep != "") {

                      //Expressão regular para validar o CEP.
                      var validacep = /^[0-9]{8}$/;

                      //Valida o formato do CEP.
                      if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        document.getElementById('adress').value="...";
                        document.getElementById('district').value="...";
                        document.getElementById('city').value="...";
                        document.getElementById('state').value="...";
                        document.getElementById('ibge').value="...";

                        //Cria um elemento javascript.
                        var script = document.createElement('script');

                        //Sincroniza com o callback.
                        script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                        //Insere script no documento e carrega o conteúdo.
                        document.body.appendChild(script);

                      } //end if.
                      else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                      }
                    } //end if.
                    else {
                      //cep sem valor, limpa formulário.
                      limpa_formulário_cep();
                    }
                  };

                  </script>
                  <!-- Inicio do formulario -->
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CEP</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input name="cep" type="text" id="cep"   maxlength="9"
                      onblur="pesquisacep(this.value);"class="form-control"  >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Rua</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input name="adress"  type="text" id="adress" class="form-control" >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Bairro</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input name="district" type="text" id="district"  class="form-control"  >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cidade</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input name="city" type="text" id="city"  class="form-control" >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estado</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input  name="state" type="text" id="state"  class="form-control" >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">IBGE</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input name="ibge" type="text" id="ibge"  class="form-control"  >
                    </div>
                  </div>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cliente</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="text" class="form-control has-feedback-left" name="customer" id="customer"  placeholder="Cliente" >
                        <option value="">	Selecione Um Cliente</option>
                        <?php



                        $sql = "SELECT  id, customer FROM customer where trash = 1";


                        if ($stmt = $conn->prepare($sql)) {
                          $stmt->execute();
                          $stmt->bind_result($id,$customer);
                          while ($stmt->fetch()) {
                            ?>

                            <option value="<?php printf($id);?>	"><?php printf($customer);?>	</option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um cliente "></span>

                    </div>
                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#customer').select2();
                  });
                  </script>


                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align">Obra Finalizada</label>
                    <div class="col-md-6 col-sm-6 ">
                      <div class="">
                        <label>
                          <input name="construction_finish"type="checkbox" class="js-switch" <?php if($construction_finish == "0"){printf("checked"); }?> />
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Legislação</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="legislation" class="form-control" type="text" name="legislation"  >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">BDI</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="BDI" class="form-control" type="text" name="BDI"   >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">ISS</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input id="ISS" class="form-control" type="text" name="ISS"   >
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align">BDI Explicito</label>
                    <div class="col-md-6 col-sm-6 ">
                      <div class="">
                        <label>
                          <input name="BDI_explicit"type="checkbox" class="js-switch" <?php if($BDI_explicit == "0"){printf("checked"); }?> />
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Correção</label>
                    <div class="col-md-6 col-sm-6 ">
                      <input  type="text" name="lag_indice" class="form-control" >
                    </div>
                  </div>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="text" class="form-control has-feedback-left" name="id_type" id="id_type"  placeholder="Categoria" >
                        <option value="">	Selecione Uma Categoria</option>
                        <?php



                        $sql = "SELECT  id, type FROM construction_type ";


                        if ($stmt = $conn->prepare($sql)) {
                          $stmt->execute();
                          $stmt->bind_result($id,$type);
                          while ($stmt->fetch()) {
                            ?>

                            <option value="<?php printf($id);?>	"><?php printf($type);?>	</option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Categoria "></span>

                    </div>
                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#id_type').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="text" class="form-control has-feedback-left" name="id_status" id="id_status"  placeholder="status" >
                        <option value="">	Selecione Um Status</option>
                        <?php



                        $sql = "SELECT  id, status FROM construction_status ";


                        if ($stmt = $conn->prepare($sql)) {
                          $stmt->execute();
                          $stmt->bind_result($id,$status);
                          while ($stmt->fetch()) {
                            ?>

                            <option value="<?php printf($id);?>	"><?php printf($status);?>	</option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um status "></span>

                    </div>
                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#id_status').select2();
                  });
                  </script>



                  <div class="form-group row">
                    <label class="col-form-label col-md-3 col-sm-3 "></label>
                    <div class="col-md-3 col-sm-3 ">
                      <center>
                        <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                      </center>
                    </div>
                  </div>


                </form>











              </div>
            </div>


              </div>
            </div>
 <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
