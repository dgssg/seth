<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}

include("../../../database/database.php");

$id_os= $_GET['id_os'];

$rating = $_POST['rating'];


$comment=$_POST['comment'];
$status="1";



$stmt = $conn->prepare("UPDATE regdate_os_rating SET rating= ? WHERE id_os= ?");
$stmt->bind_param("ss",$rating,$id_os);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE regdate_os_rating SET comment= ? WHERE id_os= ?");
$stmt->bind_param("ss",$comment,$id_os);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE regdate_os_rating SET status= ? WHERE id_os= ?");
$stmt->bind_param("ss",$status,$id_os);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../os-rating');

?>