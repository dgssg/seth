<?php






namespace Phppot;

use Phppot\Model\FAQ;
$codigoget = ($_GET["laudo"]);
include("../../database/database.php");
$query = "SELECT tse.val,tse.id_responsavel,tse.id_colaborador,tse.codigo,tse.id_equipamento,tse.id_manufacture,equipamento_familia.id_equipamento_grupo,tse.temp,tse.hum,tse.obs,colaborador.primeironome,colaborador.ultimonome,colaborador.entidade,tse.procedure_cal,calibration_parameter_manufacture.nome,tse.id, tse.data_start, tse.val, tse.ven, tse.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM tse LEFT join equipamento on equipamento.id = tse.id_equipamento LEFT join equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT join calibration_parameter_manufacture on calibration_parameter_manufacture.id =  tse.id_manufacture LEFT join colaborador on colaborador.id = tse.id_colaborador WHERE tse.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($date_validation,$id_responsavel,$id_colaborador,$codigo_caliration,$id_equipamento,$id_manufacture,$id_equipamento_grupo,$temperatura,$umidade,$obs,$primeironome,$ultimonome,$entidade,$procedure_cal,$manufacture,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}

$query = "SELECT colaborador.primeironome,colaborador.ultimonome,colaborador.entidade FROM tse LEFT join colaborador on colaborador.id = tse.id_responsavel WHERE tse.id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($primeironome_responsavel,$ultimonome_responsavel,$entidade_responsavel );
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }

}

?>

<script src="./../../assets/js/inlineEdit_tse.js"></script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>  Laudos de <small>T.S.E</small></h3>
      </div>


    </div>

  
                <div class="clearfix"></div>

<div class="x_panel">
  <div class="x_title">
    <h2>Informações</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
          class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

      <div class="alert alert-success alert-dismissible " role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>

        <strong>Classe I</strong>: Peça ativa coberta por isolamento básico e aterramento de proteção<br>
        <strong>Classe II</strong>: Peça ativa coberta por isolamento duplo ou reforçado<br>
        <strong> Classe IP</strong>: Fonte de alimentação interna
      </div>
      <div class="alert alert-success alert-dismissible " role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>

        <strong> Tipo B</strong>: Peça aplicada no paciente aterrada. Sem sistemas de isolação elétrica (não apropriada para aplicação cardíaca)<br>
        <strong> Tipo BF</strong>: Peça aplicada no paciente fluindo (condutor de superfície). Isolação elétrica, partes aterradas ou acessíveis do equipamento<br>
        <strong>Tipo CF</strong>: Peça aplicada no paciente fluindo para uso em contato direto com o coração. Aumento da isolação das partes aterradas e outras partes acessíveis do equipamento, limitando a intensidade da possível corrente fluindo através do paciente.
      </div>



    </div>
  </div>



      <div class="x_panel">
        <div class="x_title">
          <h2>Elaboração de Laudo </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <form  action="backend/tse-report-update-report-copy-backend.php?laudo=<?php printf($codigoget);?>" method="post">

              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Laudo <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="id_laudo" name="id_laudo" value="<?php printf($id); ?>" readonly="readonly" required="required" class="form-control ">
                </div>
              </div>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Validade (Meses)</label>
                <div class="col-md-4 col-sm-4 ">
                  <input type="text" id="date_validation" name="date_validation"  value="<?php printf($date_validation); ?>"  class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Data Realização <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="date" id="data_start" name="data_start"readonly="readonly" value="<?php printf($data_start); ?>"  class="form-control ">
                </div>
              </div>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
                <div class="col-md-4 col-sm-4 ">
                  <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  >
                    <option value="	">Selecione a Empresa</option>
                    <?php



                    $sql = "SELECT  id, nome FROM calibration_parameter_manufacture ";


                    if ($stmt = $conn->prepare($sql)) {
                      $stmt->execute();
                      $stmt->bind_result($id,$nome);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>	"<?php if($id==$id_manufacture){printf("selected");};?>><?php printf($nome);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
                  </select>
                  <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>


                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#id_manufacture').select2();
              });
              </script>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Equipamento</label>
                <div class="col-md-4 col-sm-4 ">
                  <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
                    <option value="	">Selecione o equipamento</option>
                    <?php



                    $query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  LEFT JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao";

                    //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                    if ($stmt = $conn->prepare($query)) {
                      $stmt->execute();
                      $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao);
                      while ($stmt->fetch()) {
                        ?>
                        <option value="<?php printf($id);?>	"<?php if($id==$id_equipamento){printf("selected");};?>><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($fabricante);?> - <?php printf($modelo);?>	</option>
                        <?php
                        // tira o resultado da busca da memória
                      }

                    }
                    $stmt->close();

                    ?>
                  </select>
                  <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

                </div>
              </div>

              <script>
              $(document).ready(function() {
                $('#equipamento').select2();
              });
              </script>

              <div class="item form-group">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo Externo</label>
                <div class="col-md-4 col-sm-4 ">
                  <input id="codigo" class="form-control" type="text" name="codigo"  value="<?php printf($codigo_caliration);?>" >
                </div>
              </div>





            </div>
          </div>

          <div class="x_panel">
            <div class="x_title">
              <h2>Procedimento</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                    class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">


                <?php


                $query = "SELECT   pop, titulo, codigo FROM documentation_pop WHERE id_equipamento_grupo like '$id_equipamento_grupo' ";
                //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($pop, $titulo,$codigo);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                  }
                }

                $query = "SELECT   tse_class.nome, tse_aplicada.nome FROM tse_parameter_dados LEFT JOIN tse_class ON tse_class.id = tse_parameter_dados.tse_class LEFT JOIN tse_aplicada ON tse_aplicada.id = tse_parameter_dados.tse_aplicada WHERE id_equipamento_grupo LIKE '%$id_equipamento_grupo%' ";
                //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($tse_class, $tse_aplicada);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                  }
                }


                ?>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Procedimento</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  class="form-control" type="text"   value="<?php printf($codigo); ?> - <?php printf($titulo); ?>"  readonly="readonly">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Classe</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  class="form-control" type="text"   value="<?php printf($tse_class); ?>"  readonly="readonly">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Parte Aplicada</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input  class="form-control" type="text"   value="<?php printf($tse_aplicada); ?> "  readonly="readonly">
                  </div>
                </div>





              </div>
            </div>

            <div class="x_panel">
              <div class="x_title">
                <h2>Executantes</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Colaborador <span class="required" >*</span>
                    </label>
                    <div class="col-md-4 col-sm-4 ">
                      <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Colaborador" value="<?php printf($id_tecnico); ?>">
                        <option value="	">Selecione o Executante</option>


                        <?php



                        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  ";


                        if ($stmt = $conn->prepare($sql)) {
                          $stmt->execute();
                          $stmt->bind_result($id,$primeironome,$ultimonome);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"<?php if($id==$id_colaborador){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
                            <?php
                            // tira o resultado da busca da mem��ria
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                  </div>









                  <script>
                  $(document).ready(function() {
                    $('#id_colaborador').select2();
                  });
                  </script>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Responsável ">Responsável Tecnico <span class="required" >*</span>
                    </label>
                    <div class="col-md-4 col-sm-4 ">
                      <select type="text" class="form-control has-feedback-right" name="id_responsavel" id="id_responsavel"  placeholder="Responsavel" value="<?php printf($id_tecnico); ?>">
                        <option value="	">Selecione o Responsável</option>


                        <?php



                        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  ";


                        if ($stmt = $conn->prepare($sql)) {
                          $stmt->execute();
                          $stmt->bind_result($id,$primeironome,$ultimonome);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"<?php if($id==$id_responsavel){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
                            <?php
                            // tira o resultado da busca da mem��ria
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                  </div>









                  <script>
                  $(document).ready(function() {
                    $('#id_responsavel').select2();
                  });
                  </script>

                </div>
              </div>


              <div class="x_panel">
                <div class="x_title">
                  <h2>Atualizar</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <center>
                      <div class="compose-footer">
                        <button class="btn btn-sm btn-success" type="submit">Salvar</button>
                      </div>
                    </center>

                  </form>

                </div>
              </div>



              <div class="clearfix"></div>

              <div class="x_panel">
                <div class="x_title">
                  <h2>T.S.E</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php

                    $query = "SELECT   V1,V2,V3,V4,V5,V6,V7,V8,iCheck1, iCheck2, obs_iCheck1,obs_iCheck2 FROM tse_report WHERE id_calibration = $codigoget";
                    //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                    if ($stmt = $conn->prepare($query)) {
                      $stmt->execute();
                      $stmt->bind_result($V1,$V2,$V3,$V4,$V5,$V6,$V7,$V8,$iCheck1, $iCheck2,$obs_iCheck1,$obs_iCheck2);
                      while ($stmt->fetch()) {
                        //printf("%s, %s\n", $solicitante, $equipamento);
                      }
                    }
                    ?>
                    <form  action="backend/tse-report-update-procedure-backend.php?laudo=<?php printf($codigoget);?>" method="post">
                      <div  class="input-group input-group-sm  ">
                        <label class="input-group-addon" for="dataScaleX"> Inspeção Visual </label>
                        <input type="text" class="form-control" id="dataScaleX" name="obs_iCheck1" placeholder=" Observação  Inspeção Visual " value="<?php printf($obs_iCheck1); ?>" >
                        <label class="input-group-addon" for="dataScaleX">

                          <label > OK </label>
                          <input align="right" type="radio" class="flat" name="iCheck1"  size="5" value="1" <?php if($iCheck1 == "1"){printf("checked"); }?>>
                          <label > NOK </label>
                          <input align="right" type="radio" class="flat" name="iCheck1" size="5" value="2" <?php if($iCheck1 == "2"){printf("checked"); }?> >
                          <label > NA </label>
                          <input align="right" type="radio" class="flat" name="iCheck1"  size="5" value="3" <?php if($iCheck1 == "3"){printf("checked"); }?> >
                        </label>
                      </div>
                      <div  class="input-group input-group-sm  ">
                        <label class="input-group-addon" for="dataScaleX"> Teste Funcional </label>
                        <input type="text" class="form-control" id="dataScaleX" name="obs_iCheck2" placeholder="Observação Teste Funcional " value="<?php printf($obs_iCheck2); ?>" >
                        <label class="input-group-addon" for="dataScaleX">

                          <label > OK </label>
                          <input align="right" type="radio" class="flat" name="iCheck2"  size="5" value="1" <?php if($iCheck2 == "1"){printf("checked"); }?> >
                          <label > NOK </label>
                          <input align="right" type="radio" class="flat" name="iCheck2"  size="5" value="2" <?php if($iCheck2 == "2"){printf("checked"); }?> >
                          <label > NA </label>
                          <input align="right" type="radio" class="flat" name="iCheck2"  size="5" value="3" <?php if($iCheck2 == "3"){printf("checked"); }?> >
                        </label>
                      </div>

                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr>

                            <div class="alert alert-success alert-dismissible " role="alert">
                              <button type="button" class="close" data-dismiss="sucess" >
                              </button>
                              <strong>Resistência </strong>
                            </div>

                            <th class="table-header">Resistência ao aterramento de proteção</th>
                            <th class="table-header">Resistência do Isolamento</th>

                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          require_once ("Model/FAQ_tse.php");
                          $faq = new FAQ();
                          $faqResult = $faq->getFAQ();

                          foreach ($faqResult as $k => $v) {
                            if(($faqResult[$k]["id_calibration"] == $codigoget)) {
                              ?>
                              <tr class="table-row">


                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'r1','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["r1"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'r2','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["r2"]; ?></td>



                              </div>






                            </tr>
                            <?php
                          } }
                          ?>
                        </tbody>
                      </table>

                      <div class="alert alert-success alert-dismissible " role="alert">
                        <button type="button" class="close" data-dismiss="sucess" >
                        </button>
                        <strong>Corrente de Fuga</strong>
                      </div>
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr>


                            <th class="table-header">Corrente de vazamento de aterramento</th>
                            <th class="table-header">Corrente de toque</th>
                            <th class="table-header">Corrente de vazamento do paciente DC </th>
                            <th class="table-header">Corrente de vazamento do paciente AC</th>
                            <th class="table-header">Corrente de vazamento do paciente Aplicada</th>
                            <th class="table-header">Corrente auxiliar do paciente</th>
                            <th class="table-header">Corrente auxiliar do paciente DC</th>
                            <th class="table-header">Corrente auxiliar do paciente AC</th>


                          </tr>
                        </thead>
                        <tbody>
                          <tr class="table-row">


                            <td > <input type="text" class="form-control" readonly="readonly" id="status1"></td>
                            <td > <input type="text" class="form-control" readonly="readonly" id="status2"></td>
                            <td > <input type="text" class="form-control" readonly="readonly" id="status3"></td>
                            <td > <input type="text" class="form-control" readonly="readonly" id="status4"></td>
                            <td > <input type="text" class="form-control" readonly="readonly" id="status5"></td>
                            <td > <input type="text" class="form-control" readonly="readonly" id="status6"></td>
                            <td > <input type="text" class="form-control" readonly="readonly" id="status7"></td>
                            <td > <input type="text" class="form-control" readonly="readonly" id="status8"></td>





                          </tr>
                          <?php
                          require_once ("Model/FAQ_tse.php");
                          $faq = new FAQ();
                          $faqResult = $faq->getFAQ();

                          foreach ($faqResult as $k => $v) {
                            if(($faqResult[$k]["id_calibration"] == $codigoget)) {
                              ?>
                              <tr class="table-row">


                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v1','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v1"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v2','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v2"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v3','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v3"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v4','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v4"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v5','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v5"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v6','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v6"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v7','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v7"]; ?></td>
                                <td bgcolor="" contenteditable="true"
                                onBlur="saveToDatabase(this,'v8','<?php echo $faqResult[$k]["id"]; ?>')"
                                onClick="showEdit(this);" bgcolor="" ><?php echo $faqResult[$k]["v8"]; ?></td>




                              </tr>


                              <?php
                            } }
                            ?>
                          </tbody>
                        </table>

                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
                          <div class="col-md-4 col-sm-4 ">
                            <select type="text" class="form-control has-feedback-right" name="status" id="status"  placeholder="status">
                              <option value="	">Selecione o status</option>

                                  <option value="0"<?php if(0==$status){printf("selected");};?>>NC — Condições normais	</option>
                                    <option value="1"<?php if(1==$status){printf("selected");};?>>SFC — Condições de falha simples	</option>

                            </select>
                            <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o status "></span>

                          </div>
                        </div>

                        <script>
                        $(document).ready(function() {
                          $('#status').select2();
                        });
                        </script>

                        <center>
                          <div class="compose-footer">
                            <button class="btn btn-sm btn-success" type="submit">Salvar</button>
                          </div>
                        </center>

                      </form>

                    </div>


                
                <script type="text/javascript">
                $(document).ready(function(){
                  $('#tools_rg').change(function(){
                    $('#parameter_rg').load('../../sub_categorias_post_tools_rg.php?tools_rg='+$('#tools_rg').val());
                  });
                });
                </script>
                <script>
            function status_ready() {

              //document.getElementById("date_start").LEFTHTML ="2022-04-18";
              //  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
              //  $('#date_start').val(date_start);
               var date_start = document.getElementById("programada_data").value;
            //  var  date_start = +$('#programada_data').val(programada_data);
              $('#date_startsecond').val(date_start);

              $('#status1').load('../../sub_categorias_post_status_1.php?id='+$('#id_laudo').val());
                  //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



            }

            </script>
