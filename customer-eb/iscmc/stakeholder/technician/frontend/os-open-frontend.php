<?php
include("../../database/database.php");
session_start();
$setor = $_SESSION['setor'] ;
?>


 <!--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 



  <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>

        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ordem de Serviço</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Solicitação <small> Descreva nos campos abaixo</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


	<div class="clearfix"></div>
                <form  class="dropzone" action="backend/os-upload-backend.php" method="post">
                    </form >
                    		<div class="ln_solid"></div>
<!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
							 <form  action="backend/os-open-backend.php?anexo=<?php printf($anexo);?>" method="post">

               <div class="ln_solid"></div>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Unidade<span
                        ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="unidade" id="unidade"  placeholder="Unidade">
										  <option value="">Selecione uma Unidade</option>
                   <?php
                      

   $result_cat_post  = "SELECT instituicao.id, instituicao.instituicao FROM instituicao_area  LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id_area = instituicao_area.id LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE instituicao_localizacao.id IN  ($setor) group by instituicao.id";

   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
             while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
               echo '<option></option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
             }
                    ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma unidade "></span>

                  </div>

                  </div>

								 <script>
                                    $(document).ready(function() {
                                    $('#unidade').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Setor<span
                       ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Setor">
                    <option value="">Selecione um Setor</option>   
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Setor "></span>

                  </div>
                  </div>
                            

								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Area<span
                       ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
                    <option value="">Selecione uma Area</option>      
                  </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Area "></span>

                    </div>

                    </div>
                                
								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>
                                 

                                 <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Equipamento<span
                       >*</span></label>
                    <div class="col-md-6 col-sm-6">
                        <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento" required>
										  <option value="">Selecione o equipamento</option>
                      <?php


					$query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.id_instituicao_localizacao IN ($setor) and equipamento.trash = 1 OR equipamento.ativo = 0  AND equipamento.id_instituicao_localizacao IN ($setor) OR equipamento.baixa = 1 AND equipamento.id_instituicao_localizacao IN ($setor) ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($modelo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
                                       	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>
                  </div>


								      		    
                  </div>
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento').select2();
                                      });
                                 </script>

<div class="col-md-7 col-sm-7  form-group has-feedback">

</div>




                          <textarea id="solicitacao" class="form-control" name="solicitacao" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="Descreva nesse campo sua Solicita&#231;&#227;o " ><?php printf($solicitacao); ?></textarea>




									<div class="ln_solid"></div>
                  <div class="form-group row">
                     <label class="col-form-label col-md-3 col-sm-3 "></label>
                      <div class="col-md-3 col-sm-3 ">
                          <center>

                          <button type="reset" class="btn btn-primary"onclick="new PNotify({
																title: 'Limpado',
																text: 'Todos os Campos Limpos',
																type: 'info',
																styling: 'bootstrap3'
														});" />Limpar</button>
											<input  type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar"/>                       </center>
                      </div>
                    </div>
								
											</div>
										</form>





									</div>
								</div>
							</div>
						</div>
					</div>



             

          <script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_categorias_post_equipament_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_categorias_post_equipament_setor_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_categorias_post_equipament_setor_area_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>

    


             <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
             <script type="text/javascript">
$("meuPrimeiroDropzone").dropzone({ url: "upload.php" });
		 Dropzone.options.meuPrimeiroDropzone = {
   paramName: "fileToUpload",
   dictDefaultMessage: "Arraste seus arquivos para cá!",
   maxFilesize: 300,
   accept: function(file, done) {
    if (file.name == "olamundo.png") {
       done("Arquivo não aceito.");
   } else {
     done();
   }
 }
 }
		</script>
     
