<?php
include("../../database/database.php");
include("../../database/database_cal.php");
session_start();	

$usuario = $_SESSION['id_usuario'];
$setor = $_SESSION['setor'] ;

  $query = "SELECT id_colaborador,id_category FROM usuario where id = $usuario ";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_colaborador,$id_category);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }


  date_default_timezone_set('America/Sao_Paulo');
  $data= date(DATE_RFC2822);
  $query = "SELECT equipamento_familia.nome,equipamento.serie,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,tse.id, tse.data_start, tse.val, tse.ven, tse.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo FROM tse INNER JOIN equipamento on equipamento.id = tse.id_equipamento INNER JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE tse.trash =1";
  //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($fabricante,$serie,$instituicao,$area,$setor,$id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
    //while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
    //  }
    

 

?>



        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Teste de Segurança Elétrica</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
               <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                    <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                 
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Laudos  <small> Teste de Segurança Elétrica</small> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                       

				

						

     
<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Laudo</th>
                          
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>
                          
                          <th>Realização</th>
                          <th>Validade</th>
                          <th>vencido</th>
                          <th>Status</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          
                          <th>Ação</th>                          
                        </tr>
                      </thead>


                      <tbody>
                        <?php   while ($stmt->fetch()) {
                          if ($ven == 0) {
                            $new_date=date('Y-m-d', strtotime($data_start.' + '.$val.' months'));
                            $data_now=date("Y-m-d");
                            if ( $new_date < $data_now ) {
                              $ven = 1;
                              //  $stmt = $conn->prepare("UPDATE calibration SET ven = 1 WHERE id= ?");
                              //  $stmt->bind_param("s",$id);
                              //  $execval = $stmt->execute();
                            }
                          }
                        ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          
                          <td><?php printf($nome); ?> </td>
                          <td> <?php printf($modelo); ?></td>
                          <td> <?php printf($fabricante); ?></td>
                          <td><?php printf($codigo); ?> </td>
                          <td><?php printf($serie); ?> </td>
                          <td><?php printf($data_start); ?></td>
                          <td><?php printf($val); ?> Meses</td>
                          <td><?php if($ven ==0) {printf("Não");} if($ven ==1) {printf("Sim");}  ?></td>
                          <td><?php if($status ==0) {printf("Aprovado");} if($status ==1) {printf("Reprovado");}  ?> </td>
                          <td><?php printf($instituicao); ?></td>
                          <td><?php printf($area); ?></td>
                          <td>  <?php printf($setor); ?></td>
                          
                          
                          <td>
                            <!--     <a class="btn btn-app"  href="tse-report-edit?laudo=<?php printf($id); ?>" onclick="new PNotify({
                            title: 'Editar',
                            text: 'Abrindo Edição',
                            type: 'sucess',
                            styling: 'bootstrap3'
                            });">
                            <i class="fa fa-edit"></i> Editar
                            </a>-->
                         
                            
                            <!--      <a  class="btn btn-app" href="tse-report-viewer-page?laudo=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
                            title: 'Visualizar',
                            text: 'Visualizar Laudo!',
                            type: 'info',
                            styling: 'bootstrap3'
                            });" >
                            <i class="fa fa-file-o"></i> Visualizar v1
                            </a> -->
                            <a  class="btn btn-app" href="../../tse-report-viewer?laudo=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
                              title: 'Visualizar',
                              text: 'Visualizar Laudo!',
                              type: 'info',
                              styling: 'bootstrap3'
                            });" >
                              <i class="fa fa-file-pdf-o"></i> PDF
                            </a>
                           
                            <a  class="btn btn-app" href="../../tse-report-viewer-copy-print?laudo=<?php printf($id); ?>  "target="_blank" onclick="new PNotify({
                              title: 'Visualizar',
                              text: 'Visualizar Laudo!',
                              type: 'info',
                              styling: 'bootstrap3'
                            });" >
                              <i class="fa fa-print"></i> Imprimir
                            </a>
                          </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


							</div>
						</div>
					</div>
				</div>
					</div>


             </div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function () {
        this.api()
        .columns([0,2,3,4,5,6,7,8,9,10,11])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
  $(document).ready(function () {
    var table = $('#datatable').DataTable();
    
    var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });
    
    function clearSearchState() {
      var api = table.api();
      var settings = api.settings()[0];
      settings.oPreviousSearch.sSearch = "";
      settings.aoPreSearchCols.forEach(function (column) {
        column.sSearch = "";
      });
      api.draw();
    }
  });
</script>

   

  
 
  
   




