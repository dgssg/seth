<?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
     <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  PMOC <small> Plano de manutenção, operação e controle <small>Calibração</small></h3>
              </div>

           
            </div>
       
              
              <!--<div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Laudos
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-report">Cadastro Laudo</a>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-plan">Cadastro Plano de Ação</a>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-type">Cadastro Tipo de Laudo</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-report-plan">Relatorio Plano de Ação</a>
                    </div>
                  </div>

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Cadastro
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-register-unit">Unidade</a>
                      <a class="dropdown-item" href="utilities-pmoc-register-room">Ambiente Climatizado</a>
                      <a class="dropdown-item" href="utilities-pmoc-register-responsible">Responsável Técnico</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-register-owner">Proprietário</a>
                    </div>
                  </div>

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Relatorio
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-report-pmoc">PMOC</a>
                      <a class="dropdown-item" href="utilities-pmoc-report-mc">Manutenção Corretiva</a>
                       
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-report-mp-schedule">Cronograma MP</a>
                       <a class="dropdown-item" href="utilities-pmoc-report-mp-routine">Rotina MP</a>
                       <a class="dropdown-item" href="utilities-pmoc-report-mp-accompaniment">Acompanhamento MP</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="utilities-pmoc-report-inventory">Inventário</a>
                    </div>
                  </div>

                </div>
              </div>-->

              
              
        
              <div class="clearfix"></div>
               
	       
      <div class="clearfix"></div>

                
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filtro</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="dropdown-item" href="#">Settings 1</a>
              </li>
              <li><a class="dropdown-item" href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="userstable_filter" style="column-count:3; -moz-column-count:3; -webkit-column-count:3; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>        
       
      </div>
    </div>
  </div>
</div>



<div class="clearfix"></div>

  
              
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista</h2>
                  <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li> <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Cadastro</strong> </i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                 
                <?php include 'frontend/utilities-pmoc-register-owner-list-frontend.php';?>  

                </div>
              </div>
	        
                  
                  
                </div>
              </div>
              
              <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Cadastro</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       
    <div class="ln_solid"></div>
    <form action="backend/utilities-pmoc-register-owner-backend.php" method="post">
    <div class="ln_solid"></div>
 
    <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="nome" class="form-control" type="text" name="nome" >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Razão Social</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="empresa" class="form-control" type="text" name="empresa" >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CNPJ</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cnpj" class="form-control" type="text" name="cnpj" >
                        </div>
                      </div>
                        
                       
                       
                        <script>
    
    function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('uf').value=("");
            document.getElementById('ibge').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
            document.getElementById('ibge').value=(conteudo.ibge);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }
        
    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('cidade').value="...";
                document.getElementById('uf').value="...";
                document.getElementById('ibge').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };

    </script>
    <!-- Inicio do formulario -->
       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CEP</label>
                        <div class="col-md-6 col-sm-6 ">
                           <input name="cep" type="text" id="cep"  maxlength="9"
               onblur="pesquisacep(this.value);"class="form-control"  >
                        </div>
                      </div>
                        <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Rua</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input name="endereco" type="text" id="rua" class="form-control"  >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Bairro</label>
                        <div class="col-md-6 col-sm-6 ">
                         <input name="bairro" type="text" id="bairro"  class="form-control"  >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cidade</label>
                        <div class="col-md-6 col-sm-6 ">
                        <input name="cidade" type="text" id="cidade"  class="form-control"  >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Estado</label>
                        <div class="col-md-6 col-sm-6 ">
                        <input   name="estado" type="text" id="uf"  class="form-control"  value="<?php printf($estado); ?> ">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">IBGE</label>
                        <div class="col-md-6 col-sm-6 ">
                       <input  name="ibge" type="text" id="ibge"  class="form-control"  >
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Email</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="email" class="form-control" type="text" name="email"   >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Telefone</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="telefone" class="form-control" type="text" name="telefone"   >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Registro no Conselho de Classe </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="classe" class="form-control" type="text" name="classe"   >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CIC</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="CIC" class="form-control" type="text" name="CIC"   >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">CGC </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="CGC" class="form-control" type="text" name="CGC"   >
                        </div>
                      </div>
                      <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
            <div class="col-md-6 col-sm-6 ">
            <select type="text" class="form-control has-feedback-right" name="id_unidade" id="id_unidade"  placeholder="equipamento">
										  <option value="">Selecione uma Unidade</option>
										  	<?php
										  	
										  
										  	
										 $query = "SELECT id, instituicao from instituicao where trash = 1";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $laudo);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($laudo);?> </option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Tipo de laudo "></span>
										
										
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#id_unidade');
                                      });
                                 </script>
          </div>

 
            
				
									
									 
											
									 
 
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>
</div>

<script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
        initComplete: function () {
            this.api()
                .columns([1,2,3,4,5])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
        // Container para o título, o select e o alerta de filtro
        var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
        
        // Título acima do select
        var title = $('<label>' + theadname + '</label>').appendTo(container);
        
        // Container para o select
        var selectContainer = $('<div></div>').appendTo(container);
        
        var select = $('<select class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo(selectContainer).select2()
        .on('change', function() {
          var val = $.fn.dataTable.util.escapeRegex($(this).val());
          column.search(val ? '^' + val + '$' : '', true, false).draw();
          
          // Remove qualquer alerta existente
          container.find('.filter-alert').remove();
          
          // Se um valor for selecionado, adicionar o alerta de filtro
          if (val) {
            $('<div class="filter-alert">' +
              '<span class="filter-active-indicator">&#x25CF;</span>' +
              '<span class="filter-active-message">Filtro ativo</span>' +
              '</div>').appendTo(container);
          }
          
          // Remove o indicador do título da coluna
          $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
          
          // Se um valor for selecionado, adicionar o indicador no título da coluna
          if (val) {
            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
          }
        });
        
        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
        // Verificar se há filtros aplicados ao carregar a página
        var filterValue = column.search();
        if (filterValue) {
          select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
        }
      });
      
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                

                
        },
    });
});
</script>
              
  