<?php
include("database/database.php");

$codigoget = ($_GET["id"]);
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }
$sql = "SELECT obs,id,alert,titulo,date_alert,resum,identification,problem,action,history,recomentation,anexo, link_anvisa,equipamento_grupo,equipamento_familia,id_alert_phase,date_phase,conslusion,date_conslusion,observation,planing,signature_alert FROM alert WHERE id = $codigoget";
if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
      $stmt->bind_result($obs,$id,$alert,$titulo,$date_alert,$resum,$identification,$problem,$action,$history,$recomentation,$anexo, $link_anvisa,$equipamento_grupo, $equipamento_familia,$id_alert_phase,$date_phase,$conslusion,$date_conslusion,$observation,$planing,$signature_alert);
      while ($stmt->fetch()) {
        
        }
      }
      function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
        $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
        $con = 1;
        $var = '';
        while($con < $QuantidadeDeCaracteresDaChave ){
            $n = rand(0, 57); 
            if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
        }else{
            $var = $var.$n.$res[$n];
            $con++;
            }
        }
            $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
        }
        //chamando a função.
       // echo chaveAlfaNumerica(5);
     //   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
 * doing:
 *
 * $uniqid = $prefix . uniqid();
 * $uniqid = uniqid($prefix);
 
printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
 * required on some systems, like Cygwin. This makes uniqid()
 * produce a value like: 4b340550242239.64159797
 
printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
  public static function v3($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = md5($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 3
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function v4() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),

      // 16 bits for "time_mid"
      mt_rand(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  public static function v5($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = sha1($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 5
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function is_valid($uuid) {
    return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                      '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  }
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();
//echo $v4uuid; //chave da assinatura
?>

<div class="right_col" role="main">

<div class="">

  <div class="page-title">
    <div class="title_left">
      <h3>Alerta Anvisa</h3>
    </div>

    <div class="title_right">
      <div class="col-md-5 col-sm-5  form-group pull-right top_search">
        <div class="input-group">

          <span class="input-group-btn">

          </span>
        </div>
      </div>
    </div>
  </div>


<div class="x_panel">
      <div class="x_title">
        <h2>Editar Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

  


 <!--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->



  <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>



  <div class="clearfix"></div>
<!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
							 <form  action="backend/technological-surveillance-edit-backend.php" method="post">

               <div class="ln_solid"></div>



               <input type="hidden" id="id" name="id" class="form-control" value="<?php printf($codigoget);?>">

<div class="form-group row">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Alerta<span class="required"></span>
  </label>
  <div class="col-md-6 col-sm-6">
    <input type="text" id="alert" name="alert" class="form-control" value="<?php printf($alert);?>">
  </div>
</div>
<label for="middle-name">Titulo</label>

<textarea id="titulo" class="form-control" name="titulo" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php printf($titulo); ?>" value="<?php printf($titulo); ?>" ><?php printf($titulo); ?></textarea>



<div class="ln_solid"></div>


<div class="form-group row">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Data <span class="required"></span>
  </label>
  <div class="col-md-6 col-sm-6">
    <input type="text" id="date_alert" name="date_alert" class="form-control" value="<?php printf($date_alert); ?>">
  </div>
</div>




<div class="ln_solid"></div>
<label for="middle-name">Resumo</label>

<textarea id="resum" class="form-control" name="resum" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php printf($resum); ?>" value="<?php printf($resum); ?>" ><?php printf($resum); ?></textarea>



<div class="ln_solid"></div>
<div class="ln_solid"></div>
<label for="middle-name">Identificação do produto ou caso</label>

<textarea id="identification" class="form-control" name="identification" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php printf($identification); ?>" value="<?php printf($identification); ?>" ><?php printf($identification); ?></textarea>



<div class="ln_solid"></div>
<div class="ln_solid"></div>
<label for="middle-name">Problema</label>

<textarea id="problem" class="form-control" name="problem" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php echo $problem; ?>" value="<?php echo $problem; ?>" ><?php echo $problem; ?></textarea>



<div class="ln_solid"></div>
<div class="ln_solid"></div>
<label for="middle-name">Ação</label>

<textarea id="action" class="form-control" name="action" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php echo $action; ?>" value="<?php echo $action; ?>" ><?php echo $action; ?></textarea>



<div class="ln_solid"></div>
<div class="ln_solid"></div>
<label for="middle-name">Histórico</label>

<textarea id="history" class="form-control" name="history" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php echo $history; ?>" value="<?php echo $history; ?>" ><?php echo $history; ?></textarea>



<div class="ln_solid"></div>
<div class="ln_solid"></div>
<label for="middle-name">Recomendações</label>

<textarea id="recomentation" class="form-control" name="recomentation" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php echo $recomentation; ?>" value="<?php echo $recomentation; ?>" ><?php echo $recomentation; ?></textarea>



<div class="ln_solid"></div>
<div class="ln_solid"></div>
<label for="middle-name">Anexo</label>

<textarea id="anexo" class="form-control" name="anexo" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php printf($anexo); ?>" value="<?php printf($anexo); ?>" ><?php printf($anexo); ?></textarea>



<div class="ln_solid"></div>

<div class="item form-group">
     <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Equipamento Grupo<span class="required" ></span>
     </label>
     <div class="col-md-6 col-sm-6 ">
 <select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="Grupo">
      <option value="">Selecione um Grupo</option>
     <?php
   $sql = "SELECT  id, nome FROM equipamento_grupo  WHERE trash = 1 ";
                       if ($stmt = $conn->prepare($sql)) {
                   $stmt->execute();
                       $stmt->bind_result($id,$equipamento_grupo_nome);
                       while ($stmt->fetch()) {
                             ?>
                         <option value="<?php printf($id);?>" <?php if($id == $equipamento_grupo){ printf("selected"); }   ?>	 ><?php printf($equipamentoequipamento_grupo_nome_grupo);?>	</option>
     <?php
   // tira o resultado da busca da memória
   }
                         }
   $stmt->close();
   ?>
                     </select>
 <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
  </div>
     </div>
<script>
                 $(document).ready(function() {
                 $('#equipamento_grupo').select2();
                   });
              </script>

              


<div class="item form-group">
     <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Equipamento Familia<span class="required" ></span>
     </label>
     <div class="col-md-6 col-sm-6 ">
 <select type="text" class="form-control has-feedback-right" name="equipamento_familia" id="equipamento_familia"  placeholder="Grupo">
      <option value="">Selecione uma Familia</option>
     <?php
   $sql = "SELECT  id, nome,fabricante,modelo,img FROM equipamento_familia  WHERE trash = 1";
                       if ($stmt = $conn->prepare($sql)) {
                   $stmt->execute();
                       $stmt->bind_result($id,$nome_equipamento,$fabricante_equipamento,$modelo_equipamento,$img);
                       while ($stmt->fetch()) {
                             ?>
                         <option value="<?php printf($id);?>" <?php if($id == $equipamento_familia){ printf("selected"); }   ?>	><?php printf($nome_equipamento);?> - <?php printf($modelo_equipamento);?> - <?php printf($fabricante_equipamento);?>	</option>
     <?php
   // tira o resultado da busca da memória
   }
                         }
   $stmt->close();
   ?>
                     </select>
 <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
  </div>
     </div>

<script>
                 $(document).ready(function() {
                 $('#equipamento_familia').select2();
                   });
              </script>
          


          <div class="ln_solid"></div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align">Plano de Ação</label>
            <div class="col-md-6 col-sm-6 ">
              <div class="">
                <label>
                  <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="planing"type="checkbox" class="js-switch" <?php if($planing == "0"){printf("checked"); }?> />
                </label>
              </div>
            </div>
          </div>

                  <div class="ln_solid"></div>

                

<label for="middle-name"><label for="middle-name">Plano de Ação</label></label>

<textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php printf($obs); ?>" value="<?php printf($obs); ?>" ><?php printf($obs); ?></textarea>

<div class="ln_solid"></div>


<div class="form-group row">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Data Plano de Ação <span class="required"></span>
  </label>
  <div class="col-md-6 col-sm-6">
    <input type="date" id="date_phase" name="date_phase" class="form-control" value="<?php printf($date_phase); ?>">
  </div>
</div>



<div class="item form-group">
     <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Fase Plano<span class="required" ></span>
     </label>
     <div class="col-md-6 col-sm-6 ">
 <select type="text" class="form-control has-feedback-right" name="id_alert_phase" id="id_alert_phase"  placeholder="Grupo">
      <option value="">Selecione uma fase</option>
     <?php
   $sql = "SELECT  id, nome FROM alert_phase ";
                       if ($stmt = $conn->prepare($sql)) {
                   $stmt->execute();
                       $stmt->bind_result($id,$alert_phase);
                       while ($stmt->fetch()) {
                             ?>
                         <option value="<?php printf($id);?>" <?php if($id == $id_alert_phase){ printf("selected"); }   ?>	 ><?php printf($alert_phase);?>	</option>
     <?php
   // tira o resultado da busca da memória
   }
                         }
   $stmt->close();
   ?>
                     </select>
 <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
  </div>
     </div>
<script>
                 $(document).ready(function() {
                 $('#id_alert_phase').select2();
                   });
              </script>


<div class="ln_solid"></div>
<label for="middle-name"><label for="middle-name">Conclusão</label></label>

<textarea id="conslusion" class="form-control" name="conslusion" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php printf($conslusion); ?>" value="<?php printf($conslusion); ?>" ><?php printf($conslusion); ?></textarea>


        <div class="ln_solid"></div>

<div class="ln_solid"></div>


<div class="form-group row">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Data Conclusão<span class="required"></span>
  </label>
  <div class="col-md-6 col-sm-6">
    <input type="date" id="date_conslusion" name="date_conslusion" class="form-control" value="<?php printf($date_conslusion); ?>">
  </div>
</div>



<div class="ln_solid"></div>
<label for="middle-name"><label for="middle-name">Observação</label></label>

<textarea id="observation" class="form-control" name="observation" data-parsley-trigger="keyup"
data-parsley-validation-threshold="10" placeholder="<?php printf($observation); ?>" value="<?php printf($observation); ?>" ><?php printf($observation); ?></textarea>


        <div class="ln_solid"></div>




<div class="ln_solid"></div>
									<div class="item form-group ">
										<div class="col-md-10 col-sm-10 offset-md-3">

											<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar" />
													</div>
												</div>

										</form>





             <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
             <script type="text/javascript">
$("meuPrimeiroDropzone").dropzone({ url: "upload.php" });
		 Dropzone.options.meuPrimeiroDropzone = {
   paramName: "fileToUpload",
   dictDefaultMessage: "Arraste seus arquivos para cá!",
   maxFilesize: 300,
   accept: function(file, done) {
    if (file.name == "olamundo.png") {
       done("Arquivo não aceito.");
   } else {
     done();
   }
 }
 }
		</script>
    <!-- /compose -->
<script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>
    								<script>
function clean() {
  $('#unidade').select2().load('reset_unidade.php');
  $('#area').select2().load('reset_area.php');
  $('#setor').select2().load('reset_setor.php');
  $('#equipamento').select2().load('reset_equipamento.php');


}

</script>




      </div>
    </div>



   <!-- page content -->


   <div class="clearfix"></div>

<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Anexo <small> </small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="row">
          <div class="col-md-55">
            <?php if($anexo ==! "" ){ ?>
              <div class="thumbnail">
                <div class="image view view-first">
                  <img style="width: 100%; display: block;" src="dropzone/quarterly/<?php printf($anexo); ?>" alt="image" />
                  <div class="mask">
                    <p>Anexo</p>
                    <div class="tools tools-bottom">
                      <a href="dropzone/quarterly/<?php printf($anexo); ?>" download><i class="fa fa-download"></i></a>

                    </div>
                  </div>
                </div>
                <div class="caption">
                  <p>Anexo</p>
                </div>
              </div>
            <?php }; ?>
          </div>







        </div>
      </div>
    </div>
  </div>
</div>

<div class="x_panel">
                <div class="x_title">
                  <h2>Atualizar Anexo</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                 <form  class="dropzone" action="backend/pdca-quarterly-analysis-upload-backend.php" method="post">
    </form >

    <form action="backend/pdca-quarterly-analysis-update-backend.php?id=<?php printf($codigoget); ?>" method="post">
  <center>
  <button class="btn btn-sm btn-success" type="submit">Atualizar Anexo</button>
  </center>
  </form >

                </div>
              </div>
    


   <div class="clearfix"></div>
   <div class="clearfix"></div>



<div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Assinatura <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="backend/technological-surveillance-signature-backend.php?id=<?php printf($codigoget);?>" method="post">
          <div class="item form-group">
                        <label for="middle-name"
                            class="col-form-label col-md-3 col-sm-3 label-align">Chave da Assinatura</label>
                        <div class="col-md-6 col-sm-6 ">
                            <input class="form-control" type="text" value="<?php printf($signature_alert); ?> "
                                readonly="readonly">
                        </div>
                    </div>

<!-- Large modal --> <center>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Assinatura</button>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Assinatura</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <h4>Assinatura digital</h4>
                <p>Para Assinar digite o texto abaixo.</p>
               <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
                <p> <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> <span ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="assinature" name="assinature"   class="form-control " class="docs-tooltip" data-toggle="tooltip" title=" ">
                
              </div>
              <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >
              </div>
            </div>
            <script>
var chave = "<?php echo $chave; ?>"; // Obtém a chave do PHP e armazena na variável JavaScript
var assinaturaInput = document.getElementById("assinature");

assinaturaInput.addEventListener("blur", function () {
var assinatura = this.value;

if (assinatura !== chave) {
Swal.fire('A assinatura está incorreta!');
  // Aqui você pode realizar a ação desejada caso a assinatura seja divergente, como exibir uma mensagem de erro ou realizar algum redirecionamento.
} else {

  // Aqui você pode realizar a ação desejada caso a assinatura seja correta.
}
});
</script>
            </div></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Assinar</button>
              </div>

            </div>
          </div>
        </div>
       </center>
       
          </form>
        </div>
      </div>
    </div>
  </div>


  <div class="clearfix"></div>
  <div class="clearfix"></div>


<div class="x_panel">
    <div class="x_title">
      <h2>Assinatura</h2>
      <ul class="nav navbar-right panel_toolbox">
         
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      
    <!--arquivo -->
<?php    
$query="SELECT regdate_alert_signature.id,regdate_alert_signature.id_alert,regdate_alert_signature.id_user,regdate_alert_signature.id_signature,regdate_alert_signature.reg_date,usuario.nome FROM regdate_alert_signature INNER JOIN usuario ON regdate_alert_signature.id_user = usuario.id   WHERE regdate_alert_signature.id_alert like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$id_mp,$id_user,$id_signature,$reg_date,$id_user);

?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              
              <th>Usuário</th>
              <th>Assinatura</th>
               <th>Registro</th>
                <th>Ação</th>
            
            </tr>
          </thead>
          <tbody>
                 <?php  while ($stmt->fetch()) { ?>
                 
            <tr>
              <th scope="row"><?php printf($row); ?></th>
              <td><?php printf($id_user); ?></td>
              <td><?php printf($id_signature); ?></td>
              <td><?php printf($reg_date); ?></td>
              <td>                                                                <a class="btn btn-app" onclick="
                Swal.fire({
                  title: 'Tem certeza?',
                  text: 'Você não será capaz de reverter isso!',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Sim, Deletar!'
                }).then((result) => {
                  if (result.isConfirmed) {
                    Swal.fire(
                      'Deletando!',
                      'Seu arquivo será excluído.',
                      'success'
                    ),
                    window.location = 'backend/signature-alert-trash-backend.php?id=<?php printf($id); ?>&page=<?php printf($codigoget); ?>';
                  }
                })
              ">
                <i class="fa fa-trash"></i> Excluir
              </a>
              </td>

             
              
            </tr>
       <?php  $row=$row+1; }
}   ?>
          </tbody>
        </table>

    
  
    
    </div>
  </div>  
    <!-- compose -->


<div class="clearfix"></div>
             
            
  <div class="x_panel">
     <div class="x_title">
       <h2>Ação</h2>
       <ul class="nav navbar-right panel_toolbox">
         <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
         </li>
         <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
               class="fa fa-wrench"></i></a>
           <ul class="dropdown-menu" role="menu">
             <li><a href="#">Settings 1</a>
             </li>
             <li><a href="#">Settings 2</a>
             </li>
           </ul>
         </li>
         <li><a class="close-link"><i class="fa fa-close"></i></a>
         </li>
       </ul>
       <div class="clearfix"></div>
     </div>
     <div class="x_content">
       
       <a class="btn btn-app"  href="technological-surveillance">
         <i class="glyphicon glyphicon-arrow-left"></i> Voltar
       </a>
       
   
      
        
  <!--    ref="backend/os-progress-upgrade-close-frontend.php?os="
  
  <a class="btn btn-app"  href=""onclick="new PNotify({
                                                     title: 'Atualização',
                                                     text: 'Atualização de Abertura de O.S!',
                                                     type: 'danger',
                                                     styling: 'bootstrap3'
                                             });">
         <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
       </a> -->
     
    
       
     </div>
   </div>

  


  

</div>
  </div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

