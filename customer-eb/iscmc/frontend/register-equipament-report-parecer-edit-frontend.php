<?php
$codigoget = ($_GET["equipamento"]);
$id = ($_GET["id"]);


// Create connection

include("database/database.php");
$query = "SELECT equipamento.id,equipamento_familia.anvisa,equipamento.baixa,equipamento.ativo,equipamento.inventario,equipamento.comodato,equipamento.duplicidade,equipamento.preventive,equipamento.obsolescence,equipamento.rfid,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento.serie,equipamento.patrimonio,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao WHERE equipamento.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($equipamento,$anvisa,$baixa,$ativo,$inventario,$comodato,$duplicidade,$preventive,$obsolecencia,$rfid,$data_val,$nf,$data_fab,$data_instal,$serie,$patrimonio,$id_equipament, $nome, $modelo, $fabricante, $codigo, $localizacao);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }


}
$query = "SELECT parecer_equipamento.descritivo,parecer_equipamento.id, parecer_equipamento.titulo, parecer_equipamento.upgrade,parecer_equipamento.reg_date FROM parecer_equipamento WHERE parecer_equipamento.id = $id  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($descritivo,$id,$titulo,$upgrade,$reg_date);
    while ($stmt->fetch()) {
        //printf("%s, %s\n", $solicitante, $equipamento);
            }
}
?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>Equipamento</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Parecer <small>Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($nome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Modelo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text"  value="<?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Fabricante <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($fabricante); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
            
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"readonly="readonly"  required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="patrimonio">Patromonio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="patrimonio" name="patrimonio" value="<?php printf($patrimonio); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="serie">N/S <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="serie" name="serie" value="<?php printf($serie); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_instal" name="data_instal" value="<?php printf($data_instal); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_fab">Fabricação  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_fab" name="data_fab" value="<?php printf($data_fab); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_val">Garantia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_val" name="data_val" value="<?php printf($data_val); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">Observação <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="obs" name="obs" value="<?php printf($obs); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>



                  </div>
                </div>
              </div>
	       </div>


	     <div class="clearfix"></div>


        <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- Posicionamento -->
 <script src="https://clevert.com.br/lib/ckeditor/ckeditor.js"></script>
 <div class="clearfix"></div>

<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Cadastro<small>Parecer</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">


<form  action="backend/register-equipament-report-parecer-edit-backend.php" method="post">



<input type="hidden" class="form-control has-feedback-left" name="id" id="id" value="<?php printf($id); ?>" >
<input type="hidden" class="form-control has-feedback-left" name="id_equipamento" id="id_equipamento" value="<?php printf($codigoget); ?>" >    
                   



<div class="col-md-12 col-sm-12 form-group has-feedback">
<input type="text" class="form-control has-feedback-left" name="titulo" id="titulo"  placeholder="Titulo" value="<?php printf($titulo); ?>">
        <span class="fa fa-bookmark form-control-feedback left" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Digite o Titulo "></span>
        </div>




<textarea name="editor1" id="editor1" rows="100" cols="800" >

<?php printf($descritivo); ?>


                                    </textarea>
                                    <script>
                                            // Replace the <textarea id="editor1"> with a CKEditor
                                            // instance, using default configuration.
                                            CKEDITOR.replace( 'editor1' );
                                    </script>



<div class="ln_solid"></div>


                              
                                <input type="submit" class="btn btn-primary" onclick="new PNotify({
                                                    title: 'Registrado',
                                                    text: 'Informações registrada!',
                                                    type: 'success',
                                                    styling: 'bootstrap3'
                                            });" />

                                    </form>
                                            <div class="ln_solid"></div>



      </div>
    </div>
  </div>
</div>

	         
            <div class="clearfix"></div>
              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament-report?equipamento=<?php printf($codigoget); ?>">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
