<?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  PMOC <small> Plano de manutenção, operação e controle <small>Calibração</small></h3>
              </div>

           
            </div>
       
              
              <!--<div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Laudos
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-report">Cadastro Laudo</a>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-plan">Cadastro Plano de Ação</a>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-type">Cadastro Tipo de Laudo</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-report-plan">Relatorio Plano de Ação</a>
                    </div>
                  </div>

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Cadastro
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-register-unit">Unidade</a>
                      <a class="dropdown-item" href="utilities-pmoc-register-room">Ambiente Climatizado</a>
                      <a class="dropdown-item" href="utilities-pmoc-register-responsible">Responsável Técnico</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-register-owner">Proprietário</a>
                    </div>
                  </div>

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Relatorio
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-report-pmoc">PMOC</a>
                      <a class="dropdown-item" href="utilities-pmoc-report-mc">Manutenção Corretiva</a>
                       
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-report-mp-schedule">Cronograma MP</a>
                       <a class="dropdown-item" href="utilities-pmoc-report-mp-routine">Rotina MP</a>
                       <a class="dropdown-item" href="utilities-pmoc-report-mp-accompaniment">Acompanhamento MP</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="utilities-pmoc-report-inventory">Inventário</a>
                    </div>
                  </div>

                </div>
              </div>-->

              
              
        
              
                <div class="clearfix"></div>

                <div class="x_panel">
     	       
      <div class="clearfix"></div>   
               
	         <div class="x_panel">
                <div class="x_title">
                  <h2>Filtro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                   
                  
                <form action="backend/report-report-pmoc-backend.php" method="post"  target="_blank">

<div class="ln_solid"></div>





<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span class="required" >*</span>
  </label>
  <div class="input-group col-md-6 col-sm-6">
    <select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">
      <option value="">Selecione uma unidade</option>

      <?php



      $result_cat_post  = "SELECT  id, instituicao FROM instituicao where trash = 1";

      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
      }
      ?>

    </select>
    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

  </div>
</div>

<script>
$(document).ready(function() {
  $('#instituicao').select2();
});
</script>

<div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "></label>
              <div class="col-md-3 col-sm-3 ">
                <center>
                  <button class="btn btn-sm btn-success" type="submit">Gerar Relatorio</button>
                </center>
              </div>
            </div>
              
              
	        
                  
                  
                </div>
              </div>
              
  