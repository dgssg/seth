<?php
include("database/database.php");
$query="SELECT purchases_material.id, purchases_species.nome, purchases_class.nome, purchases_subclass.nome, purchases_material.cod, purchases_material.nome, purchases_material.unidade, purchases_material.qt_min, purchases_material.qt_max, purchases_material.qt_now, purchases_material.vlr, purchases_material.upgrade, purchases_material.reg_date FROM purchases_material INNER JOIN purchases_species ON purchases_species.id = purchases_material.purchases_species INNER JOIN purchases_class ON purchases_class.id = purchases_material.purchases_class INNER JOIN purchases_subclass ON purchases_subclass.id = purchases_material.purchases_subclass";
$row=1;
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$purchases_species,$purchases_class,$purchases_subclass,$cod,$nome,$unidade,$qt_min,$qt_max,$qt_now,$vlr,$upgrade,$reg_date);

  ?>
    <table id="datatable" class="table table-striped jambo_table bulk_action" style="width:100%">
    <thead>
      <tr>
        <th>#</th>
        <th>Categoria</th>
        <th>Grupo</th>
        <th>Familia</th>
        <th>Código Item</th>
        <th>Item</th>
        <th>Unidade</th>
        <th>Quantidade Estoque Mínimo</th>
        <th>Quantidade Estoque Máximo</th>
        <th>Quantidade Estoque Atual</th>
        <th>Valor Atual (R$)</th>



      </tr>
    </thead>
    <tbody>
      <?php  while ($stmt->fetch()) { ?>

        <tr  >
          <th scope="row"><?php printf($row); ?></th>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($purchases_species); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($purchases_class); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>> <?php printf($purchases_subclass); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($cod); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($nome); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($unidade); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($qt_min); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($qt_max); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($qt_now); ?></td>
          <td <?php if($qt_now > $qt_max){ printf("bgcolor='BLUE' color='WHITE'");}; if($qt_min > $qt_now){ printf("bgcolor='RED' color='WHITE'");}; ?>><?php printf($vlr); ?></td>


        </tr>
        <?php  $row=$row+1; }
      }   ?>
    </tbody>
  </table>







  <div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Itens</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="backend/purchases-inventory-material-backend.php" method="post">
            <div class="ln_solid"></div>

            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Espécie <span class="required"></span>
              </label>
              <div class="input-group col-md-6 col-sm-6">
                <select type="text" class="form-control has-feedback-left" name="purchases_species" id="purchases_species" required="required" placeholder="Espécie">
              <option value="">Selecione uma Espécie</option>
                  <?php



                  $result_cat_post  = "SELECT  id, nome FROM purchases_species";

                  $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                  while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                    echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                  }
                  ?>

                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>

              </div>
            </div>

            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Class <span class="required"></span>
              </label>
              <div class="input-group col-md-6 col-sm-6">
                <select type="text" class="form-control has-feedback-left" name="purchases_class" id="purchases_class" required="required" placeholder="Class">
              <option value="">Selecione uma Class</option>
                  <?php



                  $result_cat_post  = "SELECT  id, nome FROM purchases_class";

                  $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                  while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                    echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                  }
                  ?>

                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Class "></span>

              </div>
            </div>

            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">SubClass <span class="required"></span>
              </label>
              <div class="input-group col-md-6 col-sm-6">
                <select type="text" class="form-control has-feedback-left" name="purchases_subclass" id="purchases_subclass" required="required" placeholder="SubClass">
              <option value="">Selecione uma SubClass</option>
                  <?php



                  $result_cat_post  = "SELECT  id, nome FROM purchases_subclass";

                  $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                  while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                    echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                  }
                  ?>

                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a SubClass "></span>

              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="cod" class="form-control" type="text" name="cod"  >
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="nome" class="form-control" type="text" name="nome"  >
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="unidade" class="form-control" type="text" name="unidade"  >
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade Mínimo</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="qt_min" class="form-control" type="number" name="qt_min"  >
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade Máximo</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="qt_max" class="form-control" type="number" name="qt_max"  >
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade Atual</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="qt_now" class="form-control" type="number" name="qt_now"  >
              </div>
            </div>
            <div class="item form-group">
              <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor Atual (R$)</label>
              <div class="col-md-6 col-sm-6 ">
                <input id="vlr" class="form-control" type="text" name="vlr"  >
              </div>
            </div>










          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary" onclick="new PNotify({
                      title: 'Registrado',
                      text: 'Informações registrada!',
                      type: 'success',
                      styling: 'bootstrap3'
                  });" >Salvar Informações</button>
          </div>

        </div>
      </div>
    </div>
  </form>

  <!-- -->
