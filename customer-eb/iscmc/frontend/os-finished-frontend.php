


        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ordem de Serviço</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Menu</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                <style>
                  .btn.btn-app {
                    border: 2px solid transparent; /* Define a borda como transparente por padrão */
                    padding: 5px;
                    position: relative; /* Necessário para posicionar o pseudo-elemento */
                  }
                  
                  .btn.btn-app.active {
                    border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
                  }
                  
                  .btn.btn-app.active::after {
                    content: "";
                    position: absolute;
                    left: 0;
                    bottom: 0; /* Posição da linha no final do elemento */
                    width: 100%;
                    height: 3px; /* Espessura da linha */
                    background-color: #ffcc00; /* Cor da linha */
                  }
                  
                </style>
                <?php
                  $current_page = basename($_SERVER['REQUEST_URI'], ".php");
                ?>
                
                <!-- Menu -->
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-open' ? 'active' : ''; ?>" href="os-open">
                  <i class="fa fa-folder-open"></i> O.S Abertura
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-opened' ? 'active' : ''; ?>" href="os-opened">
                  
                  <i class="fa fa-play"></i>O.S Aberto 
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-progress' ? 'active' : ''; ?>" href="os-progress">
                  <i class="fa fa-cogs"></i>O.S Processo
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-signature' ? 'active' : ''; ?>" href="os-signature">
                  
                  <i class="fa fa-pencil"></i>O.S Assinatura
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-signature-user' ? 'active' : ''; ?>" href="os-signature-user">
                  
                  <i class="fa fa-user"></i>O.S Solicitante
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-finished' ? 'active' : ''; ?>" href="os-finished">
                  
                  <i class="fa fa-check-circle"></i>O.S Concluído
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-canceled' ? 'active' : ''; ?>" href="os-canceled">
                  
                  <i class="fa fa-times-circle"></i>O.S Cancelada
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-integration' ? 'active' : ''; ?>" href="os-integration">
                  
                  <i class="fa fa-code"></i>O.S Integração
                </a>
                
              </div>
            </div>
              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Solicitações <small> de Ordem de Serviço</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">








<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>#</th>
                        <th>OS</th>
                                                            <th>Anexo</th>
                                                             
                                                            <th>Data da solicitação</th>
                                                            <th>Prioridade</th>
                                                            <th>Codigo</th>
                                                            <th>Equipamento</th>
                                                            <th>Modelo</th>
                                                            <th>Fabricante</th>
                                                            <th>N/S</th>
                                                            <th>Unidade</th>
                                                            <th>Setor</th>
                                                            <th>Area</th>
                                                            <th>Solicitante</th>
                                                            <th>Solicitação</th>


                                                            <th>Posicionamento</th>
                                                            <th>Prazo SLA</th>

                                                            <th>Categoria</th>
                                                            <th>Serviço</th>
                          <th>Avaliação Fornecedor</th>
                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                      
                       
                                                            
                     
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


							</div>
						</div>
					</div>
				</div>
					</div>


             </div>
  <script type="text/javascript">

 
        $(document).ready(function() {

          $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );
  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-os-finished.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
  .then(data => {
    // Mapeia os dados para o formato esperado pelo DataTables
    const novosDados = data.map(item => [
      ``,
      item.id,
     
      (item.id_anexo || item.file) ? `
    ${item.id_anexo ? `<a href="dropzone/os/${item.id_anexo}" download><i class="fa fa-paperclip"></i></a>` : ""}
    ${item.file ? `<a href="dropzone/mc-dropzone/${item.file}" download><i class="fa fa-paperclip"></i></a>` : ""}
  ` : "",
      item.reg_date,
      item.prioridade,
      item.codigo,
      item.equipamento,
      item.modelo,
      item.fabricante,
      item.serie,
      item.instituicao,
      item.setor,
      item.area,
      item.usuario,
      item.id_solicitacao,
      item.os_posicionamento,
      item.sla_term,
      item.category,
      item.custom_service,
      item.assessment == "0" ? "Sim" : "Não",
      ` <a  class="btn btn-app" href="os-viewer?os=${item.id} "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  <a class="btn btn-app"  href="os-progress-upgrade?os=${item.id}"onclick="new PNotify({
                                 title: 'Atualização',
                                 text: 'Atualização de Abertura de O.S!',
                                 type: 'warning',
                                 styling: 'bootstrap3'
                             });">
                     <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                   </a>
                        <a class="btn btn-app" href="#" onclick="openReopenModal(${item.id});">
        <i class="glyphicon glyphicon-repeat"></i> Reabertura
      </a>

                    <a class="btn btn-app"  href="os-opened-close?os=${item.id}" onclick="new PNotify({
																title: 'Cancelamento de O.S',
																text: 'Cancelamento de Abertura de O.S',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-ban"></i> Cancelar
                  </a>
                  <a class="btn btn-app"  href="os-edit?os=${item.id}" onclick="new PNotify({
																title: 'Editar  O.S',
																text: 'Editar  O.S',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                  <a class="btn btn-app"  href="backend/os-assessment-backend.php?id=${item.id}" onclick="new PNotify({
																title: 'Avaliação',
																text: 'Avaliação de O.S',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-question"></i> Avaliação
                  </a>
           
                 
                  
                  <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/os-trash-backend?id=${item.id}';
  }
})
">
                                  <i class="fa fa-trash"></i> Excluir
                                </a>`
    ]);

// Adiciona as novas linhas ao DataTables e desenha a tabela
$('#datatable').DataTable().rows.add(novosDados).draw();


              // Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([1, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13, 15, 16, 17, 18]).every(function (d) {
        var column = this;
        var theadname = $("#datatable th").eq([d]).text();
        var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
        var label = $('<label>'+theadname+': </label>').appendTo(container); 
        var select = $('<select  class="form-control my-1"><option value="">' +
          theadname +'</option></select>').appendTo('#userstable_filter').select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });

        column.data().unique().sort().each(function (d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
      });
    });
});
    function openReopenModal(osId) {
      Swal.fire({
        title: "Justificativa da reabertura",
        html: `
      <input id="osId" type="hidden" value="${osId}" />
      <input id="justification" class="swal2-input" placeholder="Justificativa" autofocus>
    `,
        showCancelButton: true,
        confirmButtonText: "Salvar",
        showLoaderOnConfirm: true,
        preConfirm: async () => {
          const osId = document.getElementById("osId").value;
          const justification = document.getElementById("justification").value;
          
          try {
            // Enviar os dados para o backend
            const formData = new FormData();
            formData.append("osId", osId);
            formData.append("justification", justification);
            
            const backendUrl = "backend/os-reopening.php";
            const backendResponse = await fetch(backendUrl, {
              method: "POST",
              body: formData,
            });
            
            if (!backendResponse.ok) {
              throw new Error(`Backend request failed: ${backendResponse.statusText}`);
            }
            
            return { justification, osId };
          } catch (error) {
            Swal.showValidationMessage(`
          Request failed: ${error}
        `);
          }
        },
        allowOutsideClick: () => !Swal.isLoading(),
      }).then((result) => {
        if (result.isConfirmed) {
          const justification = result.value.justification;
          const osId = result.value.osId;
          
          Swal.fire({
            title: "Ordem de Serviço reaberta com sucesso!",
            text: `Justificativa: ${justification}`,
            confirmButtonText: "OK",
          });
        }
      });
    }
    
    function handleDeleteConfirmation(osId) {
      Swal.fire({
        title: 'Tem certeza?',
        text: 'Você não será capaz de reverter isso!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, Deletar!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Deletando!',
            'Seu arquivo será excluído.',
            'success'
          );
          window.location = `backend/os-trash-backend?id=${osId}`;
        }
      });
    }


</script>
<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>