<?php
include("database/database.php");
$query = "SELECT equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome, calibration_preventive.id_routine ,calibration_routine.id,calibration_routine.data_start, calibration_routine.periodicidade,calibration_routine.reg_date,calibration_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM calibration_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN calibration_preventive ON calibration_preventive.id_routine = calibration_routine.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE calibration_preventive.id_status like '3' group by calibration_preventive.id_routine";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($serie,$fabricante,$instituicao,$area,$setor,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);



?>


<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Rotina</th>

                          <th>Periodicidade</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          <th>Atualização</th>
                          <th>Cadastro</th>


                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($rotina); ?> </td>

                          <td><?php if($periodicidade==5){printf("Diaria [1 Dias Uteis ]");}if($periodicidade==365){printf("Anual [365 Dias]");}if($periodicidade==180){printf("Semestral [180 Dias]");} if($periodicidade==30){printf("Mensal [30 Dias]");}if($periodicidade==1){printf("Diaria [1 Dias]");} if($periodicidade==7){printf("Semanal [7 Dias]");}if($periodicidade==14){printf("Bisemanal [14 Dias]");}if($periodicidade==21){printf("Trisemanal [21 Dias]");} if($periodicidade==28){printf("Quadrisemanal [28 Dias]");} if($periodicidade==60){printf("Bimestral [60 Dias]");}if($periodicidade==90){printf("Trimestral [90 Dias]");}if($periodicidade==120){printf("Quadrimestral [120 Dias]");}if($periodicidade==730){printf("Bianual [730 Dias]");}
                            if($periodicidade==1460){printf("Quadrianual [1460 Dias]");}if($periodicidade==1095){printf("Trianual [1095 Dias]");} ?></td>

                          <td><?php printf($nome); ?></td>
                              <td><?php printf($modelo); ?></td>
                            <td><?php printf($fabricante); ?></td>
                            <td><?php printf($codigo); ?></td>
                            <td><?php printf($serie); ?></td>
                                                        <td><?php printf($instituicao); ?></td>
                               <td><?php printf($area); ?></td>
                               <td>  <?php printf($setor); ?></td>

                                <td><?php printf($upgrade); ?></td>

                          <td><?php printf($reg_date); ?></td>

                          <td>

                  <a class="btn btn-app"  href="flow-calibration-viwer-control?routine=<?php printf($rotina); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                   <a class="btn btn-app"  href="flow-calibration-close-edit?routine=<?php printf($rotina); ?>"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar MP',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-database"></i> Visualizar MP
                  </a>
                   <a class="btn btn-app"  href="flow-calibration-viwer-print-control?routine=<?php printf($rotina); ?>" target="_blank"  onclick="new PNotify({
																title: 'Imprimir',
																text: 'Imprimir Rotina',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
                  <a class="btn btn-app"  href="flow-calibration-open-tag-control?routine=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                    title: 'Visualizar',
                    text: 'Visualizar Etiqueta',
                    type: 'info',
                    styling: 'bootstrap3'
                  });">
                  <i class="fa fa-qrcode"></i> Etiqueta
                </a>
                <a class="btn btn-app"  href="flow-calibration-preventive-label-control?routine=<?php printf($rotina); ?>" target="_blank" onclick="new PNotify({
                             title: 'Etiqueta',
                             text: 'Abrir Etiqueta',
                             type: 'sucess',
                             styling: 'bootstrap3'
                         });">
                 <i class="fa fa-fax"></i> Rótulo
               </a>

                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
