<?php
$codigoget = ($_GET["bill"]);
$iten = ($_GET["iten"]);

// Create connection
include("database/database.php");// remover ../


$query="SELECT unidade_energia_fatura_itens.vlr, unidade_energia_fatura_itens.id_unidade_energia_type,unidade_energia_fatura_itens.id, unidade_energia_fatura.id, unidade_energia_type.type, unidade_energia_fatura_itens.vlr_kwh, unidade_energia_fatura_itens.consumo, unidade_energia_fatura_itens.reg_date, unidade_energia_fatura_itens.upgrade FROM  unidade_energia_fatura_itens INNER JOIN unidade_energia_fatura ON unidade_energia_fatura.id = unidade_energia_fatura_itens.id_unidade_energia_fatura INNER JOIN unidade_energia_type ON unidade_energia_type.id = unidade_energia_fatura_itens.id_unidade_energia_type where unidade_energia_fatura_itens.id= '$iten '";
$row=1;
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($vlr,$id_unidade_energia_type,$id,$id_fatura,$type,$vlr_kwh,$consumo,$reg_date,$upgrade);



  while ($stmt->fetch()) {

  }
}
?>

<div class="right_col" role="main">

  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>Edição</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <!-- page content -->










      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edição Item <small>da Fatura de Energia</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form action="backend/unidade-energia-fatura-itens-upgrade-backend.php?bill=<?php printf($codigoget); ?>&iten=<?php printf($iten); ?>" method="post">

                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Tipo de Consumo <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="type" id="type"  placeholder="Tipo de Consumo">

                      <?php



                      $result_cat_post  = "SELECT  id, type FROM unidade_energia_type ";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['type'].'</option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Tipo de Consumo "></span>

                  </div>
                </div>

                <script>
                $(document).ready(function() {
                  $('#type').select2();
                });
                </script>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor KWH</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="vlr_kwh" class="form-control" type="text" name="vlr_kwh"  value="<?php printf($vlr_kwh); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Consumo</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="consumo" class="form-control" type="text" name="consumo"  value="<?php printf($consumo); ?> " >
                  </div>
                </div>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="vlr" class="form-control" type="text" name="vlr"  value="<?php printf($vlr); ?> " >
                  </div>
                </div>






                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                  <div class="col-md-3 col-sm-3 ">
                    <center>
                      <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                    </center>
                  </div>
                </div>


              </form>






            </div>
          </div>
        </div>
      </div>



      <div class="x_panel">
        <div class="x_title">
          <h2>Ação</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app"  href="utilities-energy-bill-edit?bill=<?php printf($codigoget); ?>">
              <i class="glyphicon glyphicon-arrow-left"></i> Voltar
            </a>



          </div>
        </div>




      </div>
    </div>
    <!-- /page content -->

    <!-- /compose -->

    <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
