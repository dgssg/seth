<?php
$codigoget = ($_GET["id"]);

include("../database/database.php");// remover ../

$colaborador = $_POST['colaborador'];
$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];
$time = $_POST['time'];




$stmt = $conn->prepare("INSERT INTO regdate_mp_registro (id_mp, id_user,date_start,date_end, time) VALUES (?, ?, ?,?, ?)");
$stmt->bind_param("sssss",$codigoget,$colaborador,$date_start,$date_end,$time);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_start= ? WHERE id= ?");
$stmt->bind_param("ss",$date_start,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET date_mp_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET time_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$time,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$colaborador,$codigoget);
$execval = $stmt->execute();
$stmt->close();
 
    // ... (código anterior)
    
    // Após a execução bem-sucedida, prepare os dados para retornar
    $response_data = array(
        'date_start_ms' => $date_start,
        'date_end_ms' => $date_end,
        'time_mp' => $time,
        'id_tecnico' => $colaborador
    );
    
    // Envie os dados de volta como resposta em formato JSON
    header('Content-Type: application/json');
    echo json_encode($response_data);
    
    // ... (código posterior)

?>