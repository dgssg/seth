<?php

include("../database/database.php");

$codigoget = $_POST['id'];
$empresa = $_POST['empresa'];
$cnpj = $_POST['cnpj'];
$seguimento = $_POST['seguimento'];
$adress = $_POST['adress'];
$city = $_POST['city'];
$state = $_POST['state'];
$email = $_POST['email'];
$cep = $_POST['cep'];
$contato = $_POST['contato'];
$telefone_1 = $_POST['telefone_1'];
$telefone_2 = $_POST['telefone_2'];
$obervacao = $_POST['obervacao'];
$bairro = $_POST['bairro'];
$ibge = $_POST['ibge'];
$assessment = $_POST['assessment'];
	$stmt = $conn->prepare("UPDATE customer SET assessment = ? WHERE id= ?");
	$stmt->bind_param("ss",$assessment,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();

	$stmt = $conn->prepare("UPDATE customer SET email = ? WHERE id= ?");
	$stmt->bind_param("ss",$email,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET empresa = ? WHERE id= ?");
$stmt->bind_param("ss",$empresa,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET cnpj = ? WHERE id= ?");
$stmt->bind_param("ss",$cnpj,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET seguimento = ? WHERE id= ?");
$stmt->bind_param("ss",$seguimento,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET adress = ? WHERE id= ?");
$stmt->bind_param("ss",$adress,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET city = ? WHERE id= ?");
$stmt->bind_param("ss",$city,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET state = ? WHERE id= ?");
$stmt->bind_param("ss",$state,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET cep = ? WHERE id= ?");
$stmt->bind_param("ss",$cep,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET contato = ? WHERE id= ?");
$stmt->bind_param("ss",$contato,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET telefone_1 = ? WHERE id= ?");
$stmt->bind_param("ss",$telefone_1,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET telefone_2 = ? WHERE id= ?");
$stmt->bind_param("ss",$telefone_2,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET observacao = ? WHERE id= ?");
$stmt->bind_param("ss",$obervacao,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET bairro = ? WHERE id= ?");
$stmt->bind_param("ss",$bairro,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE customer SET ibge = ? WHERE id= ?");
$stmt->bind_param("ss",$ibge,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../register-customer');
?>