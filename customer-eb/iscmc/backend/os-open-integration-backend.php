<?php
	include("../database/database.php");
	date_default_timezone_set('America/Sao_Paulo');
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
	if($_SESSION['id_nivel'] != 1 ){
		header ("Location: ../index.php");
	}
	if($_SESSION['instituicao'] != $key ){
		header ("Location: ../index.php");
	}
	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
	
	$setor = $_SESSION['setor'];
	$usuario = $_SESSION['id_usuario'];
	$instituicao = $_POST['instituicao'];
	
	$equipamento = $_POST['equipamento'];
	$anexo=$_COOKIE['anexo'];
	
	$solicitacao=$_POST['solicitacao'];
	
	$restrictions=$_POST['restrictions'];
	$tecnico= $_POST['colaborador'];
	$fornecedor= $_POST['fornecedor'];
	$chamado= $_POST['chamado'];
	$prioridade= $_POST['prioridade'];
	$backlog= $_POST['backlog'];
	$standing= $_POST['standing'];
	$reg_date=$_POST['reg_date'];
	$id_category=$_POST['id_category'];
	$id_service=$_POST['id_service'];
	
	$id_number=$_POST['id_number'];
	$id_integration=$_POST['id_integration'];
	
	$status=2;
	$equipamento_query=trim($equipamento);
	$username_exist=0;
	$carimbo="1";
	$workflow="1";
	$os_open="1";
	$query = "SELECT os_mp_1 FROM tools";
	
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($posicionamento);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	 
		$query = "SELECT equipamento.id_instituicao_localizacao,equipamento.duplicidade,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.id like'$equipamento_query' ";
		
		//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
		if ($stmt = $conn->prepare($query)) {
			$stmt->execute();
			$stmt->bind_result($id_instituicao_localizacao,$duplicidade,$id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
			while ($stmt->fetch()) {
				$unidade=$unidade;
			}
		}
	
	$stmt = $conn->prepare("INSERT INTO os (id_unidade, id_usuario, id_equipamento, id_setor, id_solicitacao, id_anexo, id_status, id_posicionamento,id_carimbo,id_workflow,restrictions,os_open) VALUES (?,?, ?,?, ?, ?, ?, ?, ?, ?, ?,?)");
	$stmt->bind_param("ssssssssssss",$unidade,$usuario,$equipamento,$id_instituicao_localizacao,$solicitacao,$anexo,$status,$posicionamento,$carimbo,$workflow,$restrictions,$os_open);
	$execval = $stmt->execute();
	$last_id = $conn->insert_id;
	$stmt->close();
	//echo "New records created successfully";
	
	$query = "SELECT os_mp_2 FROM tools";
	
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($posicionamento);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$workflow=2;
	
	$stmt = $conn->prepare("UPDATE os SET id_category= ? WHERE id= ?");
	$stmt->bind_param("ss",$id_category,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE os SET id_servico= ? WHERE id= ?");
	$stmt->bind_param("ss",$id_service,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$prioridade=trim($prioridade);
	$stmt = $conn->prepare("UPDATE os SET id_tecnico= ? WHERE id= ?");
	$stmt->bind_param("ss",$tecnico,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE os SET id_fornecedor= ? WHERE id= ?");
	$stmt->bind_param("ss",$fornecedor,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE os SET id_prioridade= ? WHERE id= ?");
	$stmt->bind_param("ss",$prioridade,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE os SET id_chamado= ? WHERE id= ?");
	$stmt->bind_param("ss",$chamado,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE os SET id_sla= ? WHERE id= ?");
	$stmt->bind_param("ss",$prioridade,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE os SET time_backlog= ? WHERE id= ?");
	$stmt->bind_param("ss",$backlog,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	$stmt = $conn->prepare("UPDATE os SET standing= ? WHERE id= ?");
	$stmt->bind_param("ss",$standing,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	
	
	$sql = "SELECT  sla_primeiro FROM os_sla WHERE os_prioridade like '$prioridade'   ";
	if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
		$stmt->bind_result($sla_primeiro);
		while ($stmt->fetch()) {
			$sla_primeiro=$sla_primeiro;
		}
	}
	
	$date=date('Y-m-d H:i:s', strtotime($reg_date.' + '.$sla_primeiro.' minutes'));  
	
	$stmt = $conn->prepare("UPDATE os SET sla_term= ? WHERE id= ?");
	$stmt->bind_param("ss",$date,$last_id);
	$execval = $stmt->execute();
	$stmt->close(); 
	
	$status=2;
	$query = "SELECT os_mp_2 FROM tools";
	
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($posicionamento);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$workflow=2;
	
	$stmt = $conn->prepare("UPDATE os SET id_status= ? WHERE id= ?");
	$stmt->bind_param("ss",$status,$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	
	
	
	
	$query = "SELECT workflow FROM tools";
	
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($workflow);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	if($workflow == "0"){
		
		$query = "SELECT id,id_os_posicionamento FROM os_workflow WHERE id_os_status = 2 and id_os_prioridade = $prioridade";
		
		
		if ($stmt = $conn->prepare($query)) {
			$stmt->execute();
			$stmt->bind_result($workflow,$id_os_posicionamento);
			while ($stmt->fetch()) {
				//printf("%s, %s\n", $solicitante, $equipamento);
			}
		}
		
		$stmt = $conn->prepare("UPDATE os SET id_posicionamento= ? WHERE id= ?");
		$stmt->bind_param("ss",$id_os_posicionamento,$last_id);
		$execval = $stmt->execute();
		$stmt->close();
		
		
		$stmt = $conn->prepare("UPDATE os SET id_workflow= ? WHERE id= ?");
		$stmt->bind_param("ss",$workflow,$last_id);
		$execval = $stmt->execute();
		$stmt->close();
		
		$stmt = $conn->prepare("INSERT INTO regdate_os_posicionamento (id_os, id_posicionamento, id_status) VALUES (?, ?, ?)");
		$stmt->bind_param("sss",$last_id,$id_os_posicionamento,$status);
		$execval = $stmt->execute();
		
		$stmt = $conn->prepare("INSERT INTO regdate_os_workflow (id_os, id_workflow) VALUES (?, ?)");
		$stmt->bind_param("ss",$last_id,$workflow);
		$execval = $stmt->execute();
		$stmt->close();
		
		setcookie('anexo', null, -1);
		
	}
	$query = "SELECT email FROM tools";
	
	
	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($email);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	
	$stmt = $conn->prepare("UPDATE os_integration SET  transfer = 0 WHERE id= ?");
	$stmt->bind_param("s",$id_integration);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE os SET  integration = 0 WHERE id= ?");
	$stmt->bind_param("s",$last_id);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE os SET id_integration = ? WHERE id= ?");
	$stmt->bind_param("ss",$id_integration,$last_id);
	$execval = $stmt->execute();
	$stmt->close();


	echo "<script>document.location='../os-integration?sweet_salve=1'</script>";
?>