<?php
include("../database/database.php");
    $query = "SELECT unidade_gas_glp_fatura.id, unidade_gas_glp.unidade,unidade_gas_glp.cod,month.month, year.yr, unidade_gas_glp_fatura.v_nf,unidade_gas_glp.medidor,unidade_gas_glp_fatura.kwh,unidade_gas_glp_fatura.obs,unidade_gas_glp_fatura.reg_date,unidade_gas_glp_fatura.upgrade FROM unidade_gas_glp_fatura INNER JOIN unidade_gas_glp ON unidade_gas_glp.id = unidade_gas_glp_fatura.id_unidade_gas_glp INNER JOIN month
    ON month.id = unidade_gas_glp_fatura.id_mouth INNER JOIN year ON year.id = unidade_gas_glp_fatura.id_year WHERE unidade_gas_glp_fatura.trash != 0 order by unidade_gas_glp_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
