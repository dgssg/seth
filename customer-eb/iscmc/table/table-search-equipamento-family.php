<?php
include("../database/database.php");
$query = "SELECT equipamento_familia.img,equipamento_familia.manual_use,equipamento_familia.manual_tec,equipamento_grupo.nome AS 'grupo', equipamento_familia.id, equipamento_familia.id_equipamento_grupo, equipamento_familia.nome AS 'familia', equipamento_familia.fabricante, equipamento_familia.modelo, equipamento_familia.anvisa, equipamento_familia.img, equipamento_familia.upgrade, equipamento_familia.reg_date FROM equipamento_familia INNER JOIN equipamento_grupo on equipamento_grupo.id = equipamento_familia.id_equipamento_grupo WHERE equipamento_familia.trash = 1 order by equipamento_familia.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
