<?php
include("../database/database.php");
$query = "SELECT colaborador.primeironome,colaborador.ultimonome,instituicao_area.nome AS 'setor',documentation_hfmea.class, equipamento_grupo.nome, documentation_hfmea.id, documentation_hfmea.id_equipamento_grupo, documentation_hfmea.titulo, documentation_hfmea.ver, documentation_hfmea.file, documentation_hfmea.data_now, documentation_hfmea.data_before, documentation_hfmea.upgrade, documentation_hfmea.reg_date FROM documentation_hfmea  LEFT JOIN equipamento_grupo ON equipamento_grupo.id = documentation_hfmea.id_equipamento_grupo LEFT JOIN instituicao_area ON instituicao_area.id = documentation_hfmea.id_setor LEFT JOIN colaborador ON colaborador.id = documentation_hfmea.id_colaborador order by documentation_hfmea.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
