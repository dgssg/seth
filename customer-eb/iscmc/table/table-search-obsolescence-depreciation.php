<?php
include("../database/database.php");
$query=" SELECT instituicao.instituicao, instituicao_area.nome AS 'setor',instituicao_localizacao.nome AS 'area',equipamento.codigo,equipamento.serie,equipamento.id,equipamento_familia.nome AS 'equipamento',equipamento_familia.fabricante,equipamento_familia.modelo,obsolescence_year.yr FROM obsolescence_depreciation    LEFT JOIN obsolescence_year on obsolescence_year.id = obsolescence_depreciation.id_year LEFT JOIN equipamento on equipamento.id = obsolescence_depreciation.id_equipamento  LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao LEFT JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade   ";
// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
