<?php
include("../database/database.php");
$query = "SELECT technological_surveillance.id AS 'technological',alert.id as 'alert', equipamento.nf_dropzone,equipamento.serie,equipamento.comodato,equipamento.baixa,equipamento.ativo,instituicao.instituicao, instituicao_area.nome AS 'area',instituicao_localizacao.nome AS 'setor' ,equipamento.id, equipamento_familia.nome AS 'equipamento', equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN alert on alert.equipamento_familia = equipamento.id_equipamento_familia LEFT JOIN os on os.id_equipamento = equipamento.id LEFT JOIN maintenance_routine on maintenance_routine.id_equipamento = equipamento.id LEFT JOIN technological_surveillance on os.id = technological_surveillance.id_os and technological_surveillance.mp = 0  WHERE equipamento.trash = 1 group by equipamento.id order by equipamento.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
