<?php
// Verifica se a variável foi recebida via GET
if(isset($_GET['variable'])) {
    // Recebe a variável enviada via GET
    $myVariable = $_GET['variable'];
    include("database/database.php");

    // Prepara a consulta SQL com um espaço reservado (?) para o parâmetro
    $query = "SELECT id_mp FROM maintenance_preventive WHERE id = ?";

    // Prepara a consulta
    if ($stmt = $conn->prepare($query)) {
        // Bind do parâmetro na consulta preparada
        $stmt->bind_param("i", $myVariable);
        // Executa a consulta
        $stmt->execute();
        // Liga o resultado da consulta a uma variável
        $stmt->bind_result($mp);
        // Obtém o resultado da consulta
        $stmt->fetch();
        // Fecha a declaração
        $stmt->close();
    }

    // Prepara a consulta SQL para selecionar colaboradores
    $sql = "SELECT id, primeironome, ultimonome FROM colaborador WHERE trash != 0";

    // Executa a consulta
    if ($stmt = $conn->prepare($sql)) {
        $stmt->execute();
        $stmt->bind_result($id, $primeironome, $ultimonome);
        // Inicia o menu suspenso
        echo '<select name="colaborador" id="colaborador" class="form-control">';
        // Itera sobre os resultados da consulta
        while ($stmt->fetch()) {
            // Verifica se o ID do colaborador corresponde ao ID retornado pela consulta anterior
            $selected = ($id == $mp) ? "selected" : "";
            // Imprime a opção do menu suspenso
            printf('<option value="%d" %s>%s %s</option>', $id, $selected, $primeironome, $ultimonome);
        }
        // Fecha a tag select
        echo '</select>';
        // Fecha a declaração
        $stmt->close();
    } else {
        // Se a consulta falhar, exibe uma mensagem de erro
        echo "Erro ao executar a consulta.";
    }
}  
?>
