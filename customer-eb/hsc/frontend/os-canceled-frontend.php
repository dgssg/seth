<?php
include("database/database.php");
$query = "SELECT os.id_type,custom_service.nome,category.nome,equipamento_familia.fabricante,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo FROM os LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN custom_service on custom_service.id = os.id_servico LEFT JOIN category on category.id = os.id_category WHERE os.id_status like '4' order by os.id DESC";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id_type,$custom_service,$category,$equipamento_fabricante,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo);


?>



        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ordem de Serviço</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="x_panel">
              <div class="x_title">
                <h2>Menu</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                <style>
                  .btn.btn-app {
                    border: 2px solid transparent; /* Define a borda como transparente por padrão */
                    padding: 5px;
                    position: relative; /* Necessário para posicionar o pseudo-elemento */
                  }
                  
                  .btn.btn-app.active {
                    border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
                  }
                  
                  .btn.btn-app.active::after {
                    content: "";
                    position: absolute;
                    left: 0;
                    bottom: 0; /* Posição da linha no final do elemento */
                    width: 100%;
                    height: 3px; /* Espessura da linha */
                    background-color: #ffcc00; /* Cor da linha */
                  }
                  
                </style>
                <?php
                  $current_page = basename($_SERVER['REQUEST_URI'], ".php");
                ?>
                
                <!-- Menu -->
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-open' ? 'active' : ''; ?>" href="os-open">
                  <i class="fa fa-folder-open"></i> O.S Abertura
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-opened' ? 'active' : ''; ?>" href="os-opened">
                  
                  <i class="fa fa-play"></i>O.S Aberto 
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-progress' ? 'active' : ''; ?>" href="os-progress">
                  <i class="fa fa-cogs"></i>O.S Processo
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-signature' ? 'active' : ''; ?>" href="os-signature">
                  
                  <i class="fa fa-pencil"></i>O.S Assinatura
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-signature-user' ? 'active' : ''; ?>" href="os-signature-user">
                  
                  <i class="fa fa-user"></i>O.S Solicitante
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-finished' ? 'active' : ''; ?>" href="os-finished">
                  
                  <i class="fa fa-check-circle"></i>O.S Concluído
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-canceled' ? 'active' : ''; ?>" href="os-canceled">
                  
                  <i class="fa fa-times-circle"></i>O.S Cancelada
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'os-integration' ? 'active' : ''; ?>" href="os-integration">
                  
                  <i class="fa fa-code"></i>O.S Integração
                </a>
                
              </div>
            </div>
              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Solicitações <small> de Ordem de Serviço</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">








<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>#</th>
                          <th>OS</th>
                          <th>Solicitante</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          <th>Solicitação</th>
                          <th>Categoria</th>
                          <th>Serviço</th>

                          <th>Registro</th>
                          <th>
                            Tipo
                          </th>
                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {  
                              $row=$row+1;
                            ?>
                        <tr>
                        <td> <?php printf($row); ?> </td>
                          <td><?php printf($id_os); ?> </td>
                          <td><?php printf($id_usuario_nome); ?> <?php printf($id_usuario_nome); ?></td>
                          <td><?php printf($equipamento_nome); ?> </td>
                          <td> <?php printf($equipamento_modelo); ?></td>
                          <td> <?php printf($equipamento_fabricante); ?></td>
                           <td><?php printf($equipamento_codigo); ?> </td>
                           <td><?php printf($equipamento_numeroserie); ?> </td>
                           <td><?php printf($id_instituicao); ?></td>
                           <td><?php printf($id_area); ?></td>
                           <td>  <?php printf($id_localizacao); ?></td>
                          <td><?php printf($id_solicitacao); ?></td>

                          <td><?php printf($category); ?></td>

                          <td><?php printf($custom_service); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php if($id_type == 1){ printf("Predial");} ?><?php if($id_type == 2){ printf("Infraestrutura");} ?></td>
                          <td>





                  <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  <a class="btn btn-app"  href="os-progress-upgrade?os=<?php printf($id_os); ?>"onclick="new PNotify({
                                 title: 'Atualização',
                                 text: 'Atualização de Abertura de O.S!',
                                 type: 'warning',
                                 styling: 'bootstrap3'
                             });">
                     <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                   </a>
                   <a class="btn btn-app"  href="backend/os-reopening.php?os=<?php printf($id_os); ?>"onclick="new PNotify({
                                 title: 'Reabertura',
                                 text: 'Reabertura de O.S!',
                                 type: 'danger',
                                 styling: 'bootstrap3'
                             });">
                      <i class="glyphicon glyphicon-repeat"></i> Reabertura
                    </a>
                    <a class="btn btn-app"  href="os-opened-close?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento de O.S',
																text: 'Cancelamento de Abertura de O.S',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-ban"></i> Cancelar
                  </a>
              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                                      
                  <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/os-trash-backend?id=<?php printf($id_os); ?>';
  }
})
">
                                  <i class="fa fa-trash"></i> Excluir
                                </a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


							</div>
						</div>
					</div>
				</div>
					</div>


             </div>
  <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    },
        initComplete: function () {
            this.api()
            .columns([1,2,3,4,5,6,7,8,9,10,12,13])
                .every(function (d) {
                    var column = this;
                      var theadname = $("#datatable th").eq([d]).text();
        // Container para o título, o select e o alerta de filtro
        var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
        
        // Título acima do select
        var title = $('<label>' + theadname + '</label>').appendTo(container);
        
        // Container para o select
        var selectContainer = $('<div></div>').appendTo(container);
        
        var select = $('<select class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo(selectContainer).select2()
        .on('change', function() {
          var val = $.fn.dataTable.util.escapeRegex($(this).val());
          column.search(val ? '^' + val + '$' : '', true, false).draw();
          
          // Remove qualquer alerta existente
          container.find('.filter-alert').remove();
          
          // Se um valor for selecionado, adicionar o alerta de filtro
          if (val) {
            $('<div class="filter-alert">' +
              '<span class="filter-active-indicator">&#x25CF;</span>' +
              '<span class="filter-active-message">Filtro ativo</span>' +
              '</div>').appendTo(container);
          }
          
          // Remove o indicador do título da coluna
          $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
          
          // Se um valor for selecionado, adicionar o indicador no título da coluna
          if (val) {
            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
          }
        });
        
        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
        // Verificar se há filtros aplicados ao carregar a página
        var filterValue = column.search();
        if (filterValue) {
          select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
        }
      });
      
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });

 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                

                
        },
    });
});


</script>
