<?php
include("database/database.php");



?>

<form  action="backend/utilities-water-bill-register-backend.php" method="post">

  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_unidade">Unidade <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="id_unidade" id="id_unidade"  placeholder="Empresa">

        <?php



        $result_cat_post  = "SELECT  id, razao FROM unidade_agua ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['razao'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_unidade').select2();
  });
  </script>
  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="year">Ano <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="year" id="year"  placeholder="Ano">

        <?php



        $result_cat_post  = "SELECT  id, yr FROM year ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['yr'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#year').select2();
  });
  </script>


  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="mouth">Mês <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="mouth" id="mouth"  placeholder="Mês">

        <?php



        $result_cat_post  = "SELECT  id, month FROM month ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['month'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#mouth').select2();
  });
  </script>

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Consumo</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="consumo" class="form-control" type="text" name="consumo"  >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor</label>
    <div class="col-md-6 col-sm-6 ">
       <input id="p_fatura" class="form-control money2" type="text" name="p_fatura"   onKeyPress="return(moeda(this,'.',',',event))" >

    </div>
  </div>





  <button type="reset" class="btn btn-primary"onclick="new PNotify({
    title: 'Limpado',
    text: 'Todos os Campos Limpos',
    type: 'info',
    styling: 'bootstrap3'
  });" />Limpar</button>
  <input type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informações registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" value="Salvar"/>

</form>
<div class="ln_solid"></div>
<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
