<?php
setlocale(LC_ALL, 'pt_BR');

?>

<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
<?php
$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT unidade_telephony_rate.id_operation,unidade_telephony_operation.name,unidade_telephony_rate.id, unidade_telephony_rate.des,unidade_telephony_rate.vlr,unidade_telephony_rate.obs FROM unidade_telephony_rate INNER JOIN unidade_telephony_operation ON unidade_telephony_operation.id = unidade_telephony_rate.id_operation where unidade_telephony_rate.id = $codigoget  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id_operation,$operation,$id, $des,$vlr,$obs);

  while ($stmt->fetch()) {

  }
}
?>

<div class="right_col" role="main">

  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>Edição</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <!-- page content -->









      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edição <small>de Tarifa de Telefonia</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form action="backend/utilities-telephony-rate-edit-backend.php" method="post">
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                  </div>
                </div>

                <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Operadora de Telefonia</label>
            <div class="col-md-6 col-sm-6 ">
            <select type="text" class="form-control has-feedback-right" name="id_operation" id="id_operation"   >
										  <option value="">Selecione o tipo de Operadora</option>
										  	<?php
										  	
										  
										  	
										 $query = "SELECT id, cod, name from unidade_telephony_operation";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_query, $cod,$name);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_query);?>	" <?php if($id_query == $id_operation){printf("selected");}; ?>	><?php printf($cod);?> - <?php printf($name);?> </option>
										  	<?php
											// tira o resultado da busca da memória
											}	
    
                                            }
											$stmt->close();

											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Operadora de Telefonia "></span>
										
										
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#id_operation').select2();
                                      });
                                 </script>
          </div>

           
	      <label for="message_mp">Descrição da Tarifa:</label>
                          <textarea id="des" class="form-control" name="des" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($des);?>" value="<?php printf($des);?>"><?php printf($des);?>	</textarea>
                            <div class="ln_solid"></div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor Minuto</label>
    <div class="col-md-6 col-sm-6 ">
    <input name="vlr" id="vlr" type="text"   class="form-control "  value="<?php printf($vlr);?>" onKeyPress="return(moeda(this,'.',',',event))">
    </div>
  </div>
		
						
		


          <label for="message_mp">Observação:</label>
                          <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($obs);?>	" value="<?php printf($obs);?>	"><?php printf($obs);?>	</textarea>

                            <div class="ln_solid"></div>

                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                  <div class="col-md-3 col-sm-3 ">
                    <center>
                      <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                    </center>
                  </div>
                </div>


              </form>






            </div>
          </div>
        </div>
      </div>



      <div class="clearfix"></div>


      <div class="x_panel">
        <div class="x_title">
          <h2>Ação</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app"  href="utilities-telephony-rate">
              <i class="glyphicon glyphicon-arrow-left"></i> Voltar
            </a>



          </div>
        </div>




      </div>
    </div>
    <!-- /page content -->

    <!-- /compose -->

    <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.fromCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>  