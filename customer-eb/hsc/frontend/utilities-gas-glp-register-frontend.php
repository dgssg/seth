<?php
include("database/database.php");



?>

<form  action="backend/utilities-gas-glp-register-backend.php" method="post">

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="cod" class="form-control" type="text" name="cod" >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo do Tanque</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="tp_un" class="form-control" type="text" name="tp_un"  >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Capacidade do Tanque</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="cp_un" class="form-control" type="text" name="cp_un"   >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Numero de Tanques</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="nb_un" class="form-control" type="text" name="nb_un"   >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor Unitario do Gas GLP</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="vlr_un" class="form-control money2" type="text" name="vlr_un"  onKeyPress="return(moeda(this,'.',',',event))" >  >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data de Inicio do Valor</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="date_start" class="form-control" type="date" name="date_start"   >
    </div>
  </div>
  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_status">Status do Valor <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="id_status" id="id_status"  placeholder="Empresa">
<option value="0">Ativo</option>
        
<option value="1">Não Ativo</option>
  
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Status "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_status').select2();
  });
  </script>



  <button type="reset" class="btn btn-primary"onclick="new PNotify({
    title: 'Limpado',
    text: 'Todos os Campos Limpos',
    type: 'info',
    styling: 'bootstrap3'
  });" />Limpar</button>
  <input type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informações registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" value="Salvar"/>

</form>
<div class="ln_solid"></div>
<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
