<?php
include("database/database.php");

date_default_timezone_set('America/Sao_Paulo');
$ano=date("Y");
$today=date("Y-m-d");
$mes=date("m");
$query = "SELECT   calibration_parameter_tools_laudos.file,calibration_parameter_tools.id, calibration_parameter_tools.nome, calibration_parameter_tools.fabricante, calibration_parameter_tools.modelo, calibration_parameter_tools.serie, calibration_parameter_tools.patrimonio,calibration_parameter_tools.calibration,calibration_parameter_tools.date_calibration, calibration_parameter_tools.date_validation FROM calibration_parameter_tools LEFT JOIN calibration_parameter_tools_laudos ON calibration_parameter_tools_laudos.id_calibration_parameter_tools = calibration_parameter_tools.id and calibration_parameter_tools_laudos.date_validation =  calibration_parameter_tools.date_validation WHERE calibration_parameter_tools.trash != 0 GROUP BY calibration_parameter_tools.id ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($file,$id, $nome, $fabricante, $modelo, $serie, $patrimonio,$calibration, $date_calibration, $date_validation);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  } calibration_parameter_tools_laudos.file,


?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Anexo</th>
                          <th>Nome</th>
                          <th>Fabricante</th>
                          <th>Modelo</th>
                          <th>Nº Serie</th>
                          <th>Patrimonio</th>
                          <th>Nº Certificado</th>
                          <th>Calibração</th>
                          <th>Validação</th>
                          <th>Status</th>
                          <th>Ação</th>
                           </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td> <?php if($file==!""){?><a href="dropzone/analisador_simulador/<?php printf($file); ?> " download><i class="fa fa-paperclip"></i> </a>     <?php  }  ?></td>
                          <td><?php printf($nome); ?> </td>
                          <td><?php printf($fabricante); ?> </td>
                          <td><?php printf($modelo); ?> </td>
                          <td><?php printf($serie); ?> </td>
                           <td><?php printf($patrimonio); ?> </td>
                          <td><?php printf($calibration); ?> </td>
                           <td><?php printf($date_calibration); ?> </td>
                            <td><?php printf($date_validation); ?> </td>
                            <td><?php if($today > $date_validation ){ printf("Fora da Validade");}  if($today < $date_validation ){ printf("Dentro da Validade");}?></td> 




                          <td>
                   <a class="btn btn-app"  href="calibration-parameter-equipament-edit?tools=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                 <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/calibration-parameter-equipament-parameter-trash.php?id=<?php printf($id); ?>';
  }
})
">
                    <i class="fa fa-trash"></i> Excluir
                  </a>
                  <a class="btn btn-app"  href="calibration-parameter-equipament-report?tools=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Laudo',
																text: 'Adicionar Laudo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-plus"></i> Laudo
                  </a>
                 <a class="btn btn-app"  href="calibration-parameter-equipament-parameter?tools=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Parametro',
																text: 'Adicionar Parametro',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-plus"></i> Parametro
                  </a>
                  <a class="btn btn-app"  href="calibration-parameter-equipament-viewer?tools=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar procediemnto',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  <?php if($file==!""){?>
                   <a class="btn btn-app"   href="dropzone/analisador_simulador/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar procediemnto',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Laudo Físico
                  </a>
                  <?php  }  ?>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
