  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               <h3>  Estoque de <small>material</small></h3>
              </div>


            </div>
       <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

<!-- Menu -->
<a class="btn btn-app <?php echo $current_page == 'purchases-inventory' ? 'active' : ''; ?>" href="purchases-inventory">
  <i class="fa fa-level-up"></i> Entrada
</a>

<a class="btn btn-app <?php echo $current_page == 'purchases-inventory-output' ? 'active' : ''; ?>" href="purchases-inventory-output">
  <i class="fa fa-level-down"></i> Saída
</a>

<a class="btn btn-app <?php echo $current_page == 'purchases-inventory-stock' ? 'active' : ''; ?>" href="purchases-inventory-stock">
  <i class="fa fa-exchange"></i> Estoque
</a>





                </div>
              </div>
              <div class="row">
               <div class="col-md-12 col-sm-12 ">
                 <div class="x_panel">
                   <div class="x_title">
                     <h2>Legenda <small> de Estoque de Material</small></h2>
                     <ul class="nav navbar-right panel_toolbox">
                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                       </li>
                       <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                         <ul class="dropdown-menu" role="menu">
                           <li><a class="dropdown-item" href="#">Parametro 1</a>
                           </li>
                           <li><a class="dropdown-item" href="#">Parametro 2</a>
                           </li>
                         </ul>
                       </li>
                       <li><a class="close-link"><i class="fa fa-close"></i></a>
                       </li>
                     </ul>
                     <div class="clearfix"></div>
                   </div>
                   <div class="x_content">

                        <?php include 'frontend/purchases-inventory-stock-legend-frontend.php';?>

               </div>
               </div>
                </div>
                  </div>



            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                          aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="userstable_filter"
                      style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;">
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>


                <div class="clearfix"></div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Estoque de Material </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <?php include 'frontend/purchases-inventory-stock-list-frontend.php';?>

                </div>
              </div>




                </div>
              </div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').DataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      },
      initComplete: function() {
        this.api()
        .columns([1, 2, 3, 4, 5, 6,7,8,9,10])
        .every(function(d) {
          var column = this;
        var theadname = $("#datatable th").eq([d]).text();
                    
                    // Container para o título, o select e o alerta de filtro
                    var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
                    
                    // Título acima do select
                    var title = $('<label>' + theadname + '</label>').appendTo(container);
                    
                    // Container para o select
                    var selectContainer = $('<div></div>').appendTo(container);
                    
                    var select = $('<select class="form-control my-1"><option value="">' +
                        theadname + '</option></select>').appendTo(selectContainer).select2()
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column.search(val ? '^' + val + '$' : '', true, false).draw();
                        
                        // Remove qualquer alerta existente
                        container.find('.filter-alert').remove();
                        
                        // Se um valor for selecionado, adicionar o alerta de filtro
                        if (val) {
                            $('<div class="filter-alert">' +
                                '<span class="filter-active-indicator">&#x25CF;</span>' +
                                '<span class="filter-active-message">Filtro ativo</span>' +
                                '</div>').appendTo(container);
                        }
                        
                        // Remove o indicador do título da coluna
                        $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
                        
                        // Se um valor for selecionado, adicionar o indicador no título da coluna
                        if (val) {
                            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
                        }
                    });
                    
                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                    });
                    // Verificar se há filtros aplicados ao carregar a página
                    var filterValue = column.search();
                    if (filterValue) {
                        select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
                    }
                });

                // Adicionar a configuração inicial da DataTable
                table.on('init.dt', function() {
                    // Verifica se há filtros aplicados ao carregar a página
                    table.columns().every(function() {
                        var column = this;
                        var searchValue = column.search();
                        if (searchValue) {
                            var select = $('#userstable_filter select').eq(column.index());
                            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
                        }
                    });
                });
          
          column
          .data()
          .unique()
          .sort()
          .each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
       
        
        
      },
    });
  });
  
  
</script>