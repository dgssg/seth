<?php

include("database/database.php");


//$con->close();



?>
<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Parametrização <small>Tabelas do sistema</small></h3>
      </div>


    </div>
    
    <div class="x_panel">
                <div class="x_title">
                  <h2>Alerta</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

             <div class="alert alert-success alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Area de Edição!</strong> Após realizar uma ação utlize o botão [<i class="fa fa-refresh"></i> ] ao lado,
                         para atualizar informações alteradas.
                  </div>



                </div>
              </div>
    

    <div class="clearfix"></div>

    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Ordem de Serviço <small>Configuração</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg8"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Defeito</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg9"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Posicionamento</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg11"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Avaliação</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg14"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Grupo SLA</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg15"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  >SLA</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg16"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  >Prioridade</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg17"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  >Workflow</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg18"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  >Serviço</strong> </i></a>
                <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg19"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  >Notificação</strong> </i></a>



                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                  </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">


              <!-- start accordion -->
              <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                <div class="panel">
                  <a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                    <h4 class="panel-title">Carimbo</h4>
                  </a>
                  <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Carimbo</th>
                            <th>Img</th>
                            <th>Ação</th>

                          </tr>
                        </thead>
                        <tbody>
                          <?php $query = "SELECT id, carimbo,img FROM os_carimbo ";


                          if ($stmt = $conn->prepare($query)) {
                            $stmt->execute();
                            $stmt->bind_result($id, $carimbo,$img);
                            while ($stmt->fetch()) {
                              //printf("%s, %s\n", $solicitante, $equipamento);
                              //  }

                              ?>
                              <tr>

                                <th scope="row"> <?php   printf($id);   ?></th>
                                <td><?php   printf($carimbo);   ?></td>
                                <td><?php   printf($img);   ?></td>
                                <td>
                                  <td>
                                    <td><a class="btn btn-app"  href="carimbo/<?php printf($img); ?>" target="_blank" onclick="new PNotify({
                                      title: 'Visualizar',
                                      text: 'Visualizar!',
                                      type: 'info',
                                      styling: 'bootstrap3'
                                    });">
                                    <i class="glyphicon glyphicon glyphicon-file"></i> Visualizar
                                  </a>



                                </td>


                              </tr>
                            <?php       }
                          }
                          ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>


                <div class="panel">
                  <a class="panel-heading" role="tab" id="headingOne2" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne">
                    <h4 class="panel-title">Defeito</h4>
                  </a>
                  <div id="collapseOne2" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Defeito</th>
                            <th>Ação</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $query = "SELECT id, defeito FROM os_defeito ";


                          if ($stmt = $conn->prepare($query)) {
                            $stmt->execute();
                            $stmt->bind_result($id, $defeito);
                            while ($stmt->fetch()) {
                              //printf("%s, %s\n", $solicitante, $equipamento);
                              //  }


                              ?>
                              <tr>

                                <th scope="row"> <?php   printf($id);   ?></th>
                                <td><?php   printf($defeito);   ?></td>
                                <td>
                            
                                  <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Defeito:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($defeito); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-defeito-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                                <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-defeito-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                                <i class="glyphicon glyphicon-trash"></i> Excluir
                              </a>

                            </td>



                          </tr>
                        <?php       }
                      }
                      ?>


                    </tbody>
                  </table>
                </div>
              </div>
            </div>


            <div class="panel">
              <a class="panel-heading" role="tab" id="headingOne3" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">Grupo SLA</h4>
              </a>
              <div id="collapseOne3" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Grupo</th>
                        <th>Ação</th>


                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $query = "SELECT id, grupo FROM os_grupo_sla ";


                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id, $grupo);
                        while ($stmt->fetch()) {
                          //printf("%s, %s\n", $solicitante, $equipamento);
                          //  }


                          ?>
                          <tr>

                            <th scope="row"> <?php   printf($id);   ?></th>
                            <td><?php   printf($grupo);   ?></td>
                            <td>
                          <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Grupo:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($grupo); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-grupo-sla-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                        <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-sla-group-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                            <i class="glyphicon glyphicon-trash"></i> Excluir
                          </a>

                      </td>



                          </tr>
                        <?php       }
                      }
                      ?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>

   <!-- start accordion -->
   <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
        <div class="panel">
          <a class="panel-heading" role="tab" id="headingOne17" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne17" aria-expanded="true" aria-controls="collapseOne">
            <h4 class="panel-title">SLA</h4>
          </a>
          <div id="collapseOne17" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>

                    <th>Prioridade</th>
                    <th>Grupo SLA</th>
                    <th>1º SLA(minutos)</th>
                                       <th>
                      Ação
                    </th>


                  </tr>
                </thead>
                <tbody>
                  <?php $query = "SELECT os_sla.id, os_prioridade.prioridade, os_grupo_sla.grupo, os_sla.sla_primeiro,os_sla.sla_verde,os_sla.sla_amarelo FROM os_sla  LEFT JOIN  os_prioridade on os_prioridade.id = os_sla.os_prioridade LEFT JOIN os_grupo_sla on os_grupo_sla.id = os_sla.os_grupo_sla ";


                  if ($stmt = $conn->prepare($query)) {
                    $stmt->execute();
                    $stmt->bind_result($id, $prioridade,$grupo,$sla_primeiro,$sla_verde,$sla_amarelo);
                    while ($stmt->fetch()) {
                      //printf("%s, %s\n", $solicitante, $equipamento);
                      //  }

                      ?>
                      <tr>

                        <th scope="row"> <?php   printf($id);   ?></th>
                        <td><?php   printf($prioridade);   ?></td>
                        <td><?php   printf($grupo);   ?></td>
                        <td><?php   printf($sla_primeiro);   ?></td>
                                             <td>
                           <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>1º SLA(minutos):</label>
                                      <input type='number' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($sla_primeiro); ?>'>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-sla-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                        <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-sla-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                            <i class="glyphicon glyphicon-trash"></i> Excluir
                          </a>

                      </td>


                    </tr>
                  <?php       }
                }
                ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- start accordion -->

            <div class="panel">
              <a class="panel-heading" role="tab" id="headingOne4" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne4" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">Posicionamento</h4>
              </a>
              <div id="collapseOne4" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Status</th>
                        <th>Cor</th>
                        <th> Ação </th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php

                      $query = "SELECT id, status,cor FROM os_posicionamento ";


                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id, $status,$cor);
                        while ($stmt->fetch()) {
                          //printf("%s, %s\n", $solicitante, $equipamento);
                          //  }


                          ?>
                          <tr>

                            <th scope="row"> <?php   printf($id);   ?></th>
                            <td><?php   printf($status);   ?></td>

                            <td><?php   printf($cor);   ?></td>
                            <td>
                               <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Status:</label>
                                      <input type='text' class='swal2-input' id='status' name='status'  value='<?php echo htmlspecialchars($status); ?> '>
                                      <input type='color' value='<?php  echo htmlspecialchars($cor); ?>' id='cor' name='cor' class='form-control' list='minha-paleta-de-cores' />
            <datalist id='minha-paleta-de-cores'>
  <option value='#FF0000'>Vermelho</option>
  <option value='#FFA500'>Laranja</option>
  <option value='#FFFF00'>Amarelo</option>
  <option value='#008000'>Verde Escuro</option>
  <option value='#00FF00'>Verde</option>
  <option value='#00FFFF'>Ciano</option>
  <option value='#0000FF'>Azul</option>
  <option value='#800080'>Roxo</option>
  <option value='#FFC0CB'>Rosa</option>
  <option value='#8B4513'>Marrom</option>
  <option value='#808080'>Cinza</option>
  <option value='#000000'>Preto</option>
  <option value='#FF69B4'>Rosa Choque</option>
  <option value='#FFD700'>Dourado</option>
  <option value='#ADD8E6'>Azul Claro</option>
  <option value='#9370DB'>Lilás</option>
  <option value='#6B8E23'>Verde Oliva</option>
  <option value='#FA8072'>Salmão</option>
  <option value='#B0E0E6'>Azul Claro Acinzentado</option>
  <option value='#F5DEB3'>Bege</option>
</datalist>
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-posicionamento-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>
                            <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-posicionamento-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                            <i class="glyphicon glyphicon-trash"></i> Excluir
                          </a>

                        </td>

                      </tr>
                    <?php       }
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="panel">
          <a class="panel-heading" role="tab" id="headingOne5" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne5" aria-expanded="true" aria-controls="collapseOne">
            <h4 class="panel-title">Prioridade</h4>
          </a>
          <div id="collapseOne5" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
            <div class="panel-body">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Prioridade</th>
                    <th>Emergencial</th>
                    <th>
                      Ação
                    </th>

                  </tr>
                </thead>
                <tbody>
                  <?php

                  $query = "SELECT id, prioridade,emergencial FROM os_prioridade ";


                  if ($stmt = $conn->prepare($query)) {
                    $stmt->execute();
                    $stmt->bind_result($id, $prioridade,$emergencial);
                    while ($stmt->fetch()) {
                      //printf("%s, %s\n", $solicitante, $equipamento);
                      //  }


                      ?>
                      <tr>

                        <th scope="row"> <?php   printf($id);   ?></th>
                        <td><?php   printf($prioridade);   ?></td>

                        <td><?php  if ($emergencial == "0") {
                          printf("Não");
                        }
                        if ($emergencial == "1") {
                          printf("Sim");
                        }
                        ?></td>
                        <td>
                           <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Prioridade:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($prioridade); ?>'>
                                      <select  class='swal2-input' id='emergencial' name='emergencial' >
										<option value='0'> Não </option>
										<option value='1'> Sim </option>
                                        </select>
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-prioridade-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                        <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-prioridade-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                                <i class="glyphicon glyphicon-trash"></i> Excluir
                              </a>


                      </td>

                    </tr>
                  <?php       }
                }
                ?>


              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne6" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne6" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Status</h4>
        </a>
        <div id="collapseOne6" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Status</th>
                  <th>Cor</th>

                </tr>
              </thead>
              <tbody>
                <?php


                $query = "SELECT id, status,cor FROM os_status ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $status,$cor);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }


                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($status);   ?></td>

                      <td><?php   printf($cor);   ?></td>

                    </tr>
                  <?php       }
                }
                ?>


              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne7" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne7" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Workflow</h4>
        </a>
        <div id="collapseOne7" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Workflow</th>
                  <th>Prioridade</th>
                  <th>Status</th>
                  <th>Posicionamento</th>
                  <th>Ação</th>

                </tr>
              </thead>
              <tbody>
                <?php


                $query = "SELECT os_workflow.id, os_workflow.workflow, os_prioridade.prioridade, os_status.status,os_posicionamento.status FROM os_workflow  LEFT JOIN os_prioridade ON os_prioridade.id = os_workflow.id_os_prioridade LEFT JOIN os_status ON os_status.id = os_workflow.id_os_status LEFT JOIN os_posicionamento ON os_posicionamento.id = os_workflow.id_os_posicionamento";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $workflow,$id_os_prioridade,$id_os_status,$id_os_posicionamento);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }


                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($workflow);   ?></td>
                      <td><?php   printf($id_os_prioridade);   ?></td>
                      <td><?php   printf($id_os_status);   ?></td>
                      <td><?php   printf($id_os_posicionamento);   ?></td>
                      <td>
                          <a class="btn btn-app"    onclick="open('tools-os-workflow-edit-backend.php?id=<?php printf($id); ?>','tools-os-workflow-edit-backend.php?<?php printf($id); ?>','status=no,Width=450,Height=300'); new PNotify({
                             title: 'Editar',
                                    text: 'Editar!',
                                    type: 'info',
                            styling: 'bootstrap3'
                          });">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                        <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-workflow-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                                <i class="glyphicon glyphicon-trash"></i> Excluir
                              </a>


                      </td>



                    </tr>
                  <?php       }
                }
                ?>


              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne8" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne8" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Backlog</h4>
        </a>
        <div id="collapseOne8" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Backlog</th>
                  <th>Tempo</th>
                  <th>
                    Ação
                  </th>


                </tr>
              </thead>
              <tbody>
                <?php


                $query = "SELECT id, backlog,time_backlog,upgrade,reg_date FROM os_backlog ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $backlog,$time,$upgrade,$reg_date);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }


                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($backlog);   ?></td>
                      <td><?php   printf($time);   ?></td>
                      <td>
                        <td>
                           <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Backlog:</label>
                                      <input type='time' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($time); ?>'>
                                
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-backlog-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  


                      </td>

                    </td>



                  </tr>
                <?php       }
              }
              ?>


            </tbody>
          </table>
        </div>
      </div>
    </div>





    <div class="panel">
      <a class="panel-heading" role="tab" id="headingOne9" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne9" aria-expanded="true" aria-controls="collapseOne">
        <h4 class="panel-title">Avaliação</h4>
      </a>
      <div id="collapseOne9" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Conceito</th>
                <th>
                  Ação
                </th>



              </tr>
            </thead>
            <tbody>
              <?php


              $query = "SELECT id, rating FROM os_rating ";


              if ($stmt = $conn->prepare($query)) {
                $stmt->execute();
                $stmt->bind_result($id, $rating);
                while ($stmt->fetch()) {
                  //printf("%s, %s\n", $solicitante, $equipamento);
                  //  }


                  ?>
                  <tr>

                    <th scope="row"> <?php   printf($id);   ?></th>
                    <td><?php   printf($rating);   ?></td>
                    <td>
                       <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Conceito:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-rating-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                    <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-rating-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                    <i class="glyphicon glyphicon-trash"></i> Excluir
                  </a>

                </td>




              </tr>
            <?php       }
          }
          ?>


        </tbody>
      </table>
    </div>
  </div>
</div>




<div class="panel">
      <a class="panel-heading" role="tab" id="headingOne19" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne19" aria-expanded="true" aria-controls="collapseOne">
        <h4 class="panel-title">Serviço</h4>
      </a>
      <div id="collapseOne19" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Nome</th>
                <th>
                  Ação
                </th>



              </tr>
            </thead>
            <tbody>
              <?php


              $query = "SELECT id, nome FROM custom_service ";


              if ($stmt = $conn->prepare($query)) {
                $stmt->execute();
                $stmt->bind_result($id, $nome);
                while ($stmt->fetch()) {
                  //printf("%s, %s\n", $solicitante, $equipamento);
                  //  }


                  ?>
                  <tr>

                    <th scope="row"> <?php   printf($id);   ?></th>
                    <td><?php   printf($nome);   ?></td>
                    <td>
                      <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Serviço:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-service-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                    <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-custom-service-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                    <i class="glyphicon glyphicon-trash"></i> Excluir
                  </a>

                </td>




              </tr>
            <?php       }
          }
          ?>


        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- end of accordion -->

<div class="panel">
      <a class="panel-heading" role="tab" id="headingOne19" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne20" aria-expanded="true" aria-controls="collapseOne">
        <h4 class="panel-title">Notificação</h4>
      </a>
      <div id="collapseOne20" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Nome</th>
                <th>
                  Ação
                </th>



              </tr>
            </thead>
            <tbody>
              <?php


              $query = "SELECT id, nome FROM compliance ";


              if ($stmt = $conn->prepare($query)) {
                $stmt->execute();
                $stmt->bind_result($id, $nome);
                while ($stmt->fetch()) {
                  //printf("%s, %s\n", $solicitante, $equipamento);
                  //  }


                  ?>
                  <tr>

                    <th scope="row"> <?php   printf($id);   ?></th>
                    <td><?php   printf($nome);   ?></td>
                    <td>
                        <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Notificação:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-os-compliance-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                    <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-os-custom-compliance-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                    <i class="glyphicon glyphicon-trash"></i> Excluir
                  </a>

                </td>




              </tr>
            <?php       }
          }
          ?>


        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- end of accordion -->


</div>
</div>
</div>
</div>

<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Geral <small>Configuração</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"   ><i class="fa fa-plus"><strong style="color: #f0ffff" > Categoria</strong> </i></a>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


          <!-- start accordion -->
          <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
            <div class="panel">
              <a class="panel-heading" role="tab" id="headingOne11" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne11" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">Categoria</h4>
              </a>
              <div id="collapseOne11" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Categoria</th>
                        <th>Ativo</th>
                        <th>Ação</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php $query = "SELECT id, nome,ativo,upgrade,reg_date,trash FROM category WHERE trash = 1 ";


                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id, $nome,$ativo,$upgrade,$regdate,$trash);
                        while ($stmt->fetch()) {
                          //printf("%s, %s\n", $solicitante, $equipamento);
                          //  }

                          ?>
                          <tr>

                            <th scope="row"> <?php   printf($id);   ?></th>
                            <td><?php   printf($nome);   ?></td>
                            <td><?php  if($ativo=="0"){ printf("Sim");}
                            if($ativo=="1"){ printf("Não");} ?></td>
                        <td>    
                        <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Categoria:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($nome); ?>'>
                                      <select  class='swal2-input' id='ativo' name='ativo' >
										<option value='1'> Não </option>
										<option value='0'> Sim </option>
                                        </select>
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-category-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a> 
                            <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-trash-category-backend.php?id=<?php printf($id); ?>';
  }
})
">
                          <i class="glyphicon glyphicon-trash"></i> Excluir
                        </a></td>


                      </tr>
                    <?php       }
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


      


<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Equipamento <small>Configuração</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg5"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Movimentação</strong> </i></a>
            <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg6"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Motivo</strong> </i></a>
            <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg7"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Status</strong> </i></a>
           <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg13"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Baixa</strong> </i></a>
           <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg22"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Modo Instalação</strong> </i></a>
           <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg23"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Check List</strong> </i></a>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


          <!-- start accordion -->
          <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
            <div class="panel">
              <a class="panel-heading" role="tab" id="headingOne12" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne12" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">Movimentação</h4>
              </a>
              <div id="collapseOne12" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Movimentação</th>
                        <th>
                          Ação
                        </th>


                      </tr>
                    </thead>
                    <tbody>
                      <?php $query = "SELECT * FROM equipament_exit_mov ";


                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id, $nome,$upgrade,$regdate);
                        while ($stmt->fetch()) {
                          //printf("%s, %s\n", $solicitante, $equipamento);
                          //  }

                          ?>
                          <tr>

                            <th scope="row"> <?php   printf($id);   ?></th>
                            <td><?php   printf($nome);   ?></td>
                            <td>
                              <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Movimentação:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-mov-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  

                            <a class="btn btn-app" onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-mov-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                            <i class="glyphicon glyphicon-trash"></i> Excluir
                          </a>
                        </td>



                      </tr>
                    <?php       }
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

   
    <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne13" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne13" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Motivo</h4>
        </a>
        <div id="collapseOne13" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Motivo</th>
                  <th>
                    Ação
                  </th>


                </tr>
              </thead>
              <tbody>
                <?php $query = "SELECT * FROM equipament_exit_reason ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $nome,$upgrade,$regdate);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }

                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($nome);   ?></td>
                      <td>
                         <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Motivo:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-reason-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                      <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-reason-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                  </td>


                </tr>
              <?php       }
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
 <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne18" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne18" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Baixa</h4>
        </a>
        <div id="collapseOne18" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Baixa</th>
                  <th>
                    Ação
                  </th>


                </tr>
              </thead>
              <tbody>
                <?php $query = "SELECT * FROM equipament_disable_status ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $nome);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }

                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($nome);   ?></td>
                      <td>
                        <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Baixa:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-disable-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>  
                      <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-disable-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                  </td>


                </tr>
              <?php       }
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- start accordion -->
<div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
  <div class="panel">
    <a class="panel-heading" role="tab" id="headingOne14" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne14" aria-expanded="true" aria-controls="collapseOne">
      <h4 class="panel-title">Status</h4>
    </a>
    <div id="collapseOne14" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Status</th>
              <th>
                Ação
              </th>


            </tr>
          </thead>
          <tbody>
            <?php $query = "SELECT * FROM equipament_exit_status ";


            if ($stmt = $conn->prepare($query)) {
              $stmt->execute();
              $stmt->bind_result($id, $nome,$upgrade,$regdate);
              while ($stmt->fetch()) {
                //printf("%s, %s\n", $solicitante, $equipamento);
                //  }

                ?>
                <tr>

                  <th scope="row"> <?php   printf($id);   ?></th>
                  <td><?php   printf($nome);   ?></td>
                  <td>
                    <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Status:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-status-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a> 
                  <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-status-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="glyphicon glyphicon-trash"></i> Excluir
                </a>
              </td>


            </tr>
          <?php       }
        }
        ?>

      </tbody>
    </table>
  </div>
</div>
</div>
</div>
<div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne32" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne32" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Modo Instalação</h4>
        </a>
        <div id="collapseOne32" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Modo Instalação</th>
                  <th>
                    Ação
                  </th>


                </tr>
              </thead>
              <tbody>
                <?php $query = "SELECT * FROM equipament_check_mod ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $nome);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }

                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($nome);   ?></td>
                      <td>
                      <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Modo Instalação:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-check-mod-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a> 
                      <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-check-mod-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                  </td>


                </tr>
              <?php       }
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne33" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne33" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Check List</h4>
        </a>
        <div id="collapseOne33" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Check list</th>
                  <th>
                    Ação
                  </th>


                </tr>
              </thead>
              <tbody>
                <?php $query = "SELECT id, nome FROM equipament_check_list ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $nome);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }

                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($nome);   ?></td>
                      <td>
                       <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Check list:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-check-list-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a> 
                      <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-check-list-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                  </td>


                </tr>
              <?php       }
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>
</div>

<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Equipamento Familia<small>Configuração</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg24"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Tipo</strong> </i></a>
            <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg25"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Avaliação</strong> </i></a>
            <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg26"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Motivo</strong> </i></a>

            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


          <!-- start accordion -->
          <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
            <div class="panel">
              <a class="panel-heading" role="tab" id="headingOne35" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne35" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">Tipo</h4>
              </a>
              <div id="collapseOne35" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tipo</th>
                        <th>
                          Ação
                        </th>


                      </tr>
                    </thead>
                    <tbody>
                      <?php $query = "SELECT id, nome FROM equipamento_assessment_type ";


                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id, $nome);
                        while ($stmt->fetch()) {
                          //printf("%s, %s\n", $solicitante, $equipamento);
                          //  }

                          ?>
                          <tr>

                            <th scope="row"> <?php   printf($id);   ?></th>
                            <td><?php   printf($nome);   ?></td>
                            <td>
                              <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Tipo:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-assessment-type-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a> 

                            <a class="btn btn-app" onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-assessment-type-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                            <i class="glyphicon glyphicon-trash"></i> Excluir
                          </a>
                        </td>



                      </tr>
                    <?php       }
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

   
    <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne36" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne36" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Avaliação</h4>
        </a>
        <div id="collapseOne36" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Tipo</th>
                  <th>Avaliação</th>
                  <th>
                    Ação
                  </th>


                </tr>
              </thead>
              <tbody>
                <?php $query = "SELECT equipamento_assessment_av.id,equipamento_assessment_type.nome,equipamento_assessment_av.nome FROM equipamento_assessment_av LEFT JOIN equipamento_assessment_type ON equipamento_assessment_type.id = equipamento_assessment_av.id_equipamento_assessment_type ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $typo,$nome);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }

                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($typo);   ?></td>
                      <td><?php   printf($nome);   ?></td>
                      <td>
                        <a class="btn btn-app"    onclick="open('tools-equipamento-assessment-av-edit-backend.php?id=<?php printf($id); ?>','tools-equipamento-assessment-av-edit-backend.php?<?php printf($id); ?>','status=no,Width=450,Height=300'); new PNotify({
                          title: 'Editar',
                                    text: 'Editar!',
                                    type: 'info',
                          styling: 'bootstrap3'
                        });">
                        <i class="fa fa-edit"></i> Editar
                      </a>
                      <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-assessment-av-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                  </td>


                </tr>
              <?php       }
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
 <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne37" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne37" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Motivo</h4>
        </a>
        <div id="collapseOne37" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Motivo</th>
                  <th>
                    Ação
                  </th>


                </tr>
              </thead>
              <tbody>
                <?php $query = "SELECT * FROM assessment ";


                if ($stmt = $conn->prepare($query)) {
                  $stmt->execute();
                  $stmt->bind_result($id, $nome);
                  while ($stmt->fetch()) {
                    //printf("%s, %s\n", $solicitante, $equipamento);
                    //  }

                    ?>
                    <tr>

                      <th scope="row"> <?php   printf($id);   ?></th>
                      <td><?php   printf($nome);   ?></td>
                      <td>
                        <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Motivo:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-equipamento-assessment-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>
                      <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-equipamento-assessment-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                      <i class="glyphicon glyphicon-trash"></i> Excluir
                    </a>
                  </td>


                </tr>
              <?php       }
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>



</div>
</div>
</div>
</div>
<!-- FIM Equipamento Familia-->

<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Fornecedor <small>Configuração</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg3"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Serviço</strong> </i></a>
            <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg4"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Questionário</strong> </i></a>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


          <!-- start accordion -->
          <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
            <div class="panel">
              <a class="panel-heading" role="tab" id="headingOne15" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne15" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">Serviço</h4>
              </a>
              <div id="collapseOne15" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Serviço</th>
                        <th>
                          Ação
                        </th>


                      </tr>
                    </thead>
                    <tbody>
                      <?php $query = "SELECT * FROM fornecedor_service ";


                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id, $nome,$upgrade,$regdate);
                        while ($stmt->fetch()) {
                          //printf("%s, %s\n", $solicitante, $equipamento);
                          //  }

                          ?>
                          <tr>

                            <th scope="row"> <?php   printf($id);   ?></th>
                            <td><?php   printf($nome);   ?></td>
                            <td>
                             <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Serviço:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($rating); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-fornecedor-service-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>
                            <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-fornecedor-service-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                            <i class="glyphicon glyphicon-trash"></i> Excluir
                          </a>
                        </td>


                      </tr>
                    <?php       }
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


      <!-- start accordion -->
      <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
        <div class="panel">
          <a class="panel-heading" role="tab" id="headingOne16" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne16" aria-expanded="true" aria-controls="collapseOne">
            <h4 class="panel-title">Questionário</h4>
          </a>
          <div id="collapseOne16" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Serviço</th>
                    <th>Pergunta</th>
                    <th>
                      Ação
                    </th>


                  </tr>
                </thead>
                <tbody>
                  <?php $query = "SELECT fornecedor_question.id,fornecedor_service.service, fornecedor_question.question FROM fornecedor_question INNER JOIN fornecedor_service on fornecedor_service.id = fornecedor_question.id_fornecedor_service";


                  if ($stmt = $conn->prepare($query)) {
                    $stmt->execute();
                    $stmt->bind_result($id, $service,$question);
                    while ($stmt->fetch()) {
                      //printf("%s, %s\n", $solicitante, $equipamento);
                      //  }

                      ?>
                      <tr>

                        <th scope="row"> <?php   printf($id);   ?></th>
                        <td><?php   printf($service);   ?></td>
                        <td><?php   printf($question);   ?></td>
                        <td>
                          <a class="btn btn-app"    onclick="open('tools-fornecedor-question-edit-backend.php?id=<?php printf($id); ?>','tools-fornecedor-question-edit-backend.php?<?php printf($id); ?>','status=no,Width=450,Height=300'); new PNotify({
                           title: 'Editar',
                                    text: 'Editar!',
                                    type: 'info',
                            styling: 'bootstrap3'
                          });">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                        <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/tools-fornecedor-question-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                        <i class="glyphicon glyphicon-trash"></i> Excluir
                      </a>
                    </td>



                  </tr>
                <?php       }
              }
              ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>
</div>


          <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Central de Notificação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     
                   
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                      </ul>
                      <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        
                        
                        <!-- start accordion -->
                        <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                          <div class="panel">
                            <a class="panel-heading" role="tab" id="headingOne15" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne29" aria-expanded="true" aria-controls="collapseOne">
                              <h4 class="panel-title">Notificação</h4>
                            </a>
                            <div id="collapseOne29" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Origem</th>
                                      <th>Alerta/Dias</th>
                                      <th>Habilitado</th>
                                      <th>
                                        Ação
                                      </th>
                                      
                                      
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $query = "SELECT id,nome,alert,ativo FROM documentation_notification ";
                                      
                                      
                                      if ($stmt = $conn->prepare($query)) {
                                        $stmt->execute();
                                        $stmt->bind_result($id,$nome,$alert,$ativo);
                                        while ($stmt->fetch()) {
                                          //printf("%s, %s\n", $solicitante, $equipamento);
                                          //  }
                                          
                                    ?>
                                    <tr>
                                      
                                      <th scope="row"> <?php   printf($id);   ?></th>
                                      <td><?php   printf($nome);   ?></td>
                                      <td><?php   printf($alert);   ?></td>
                                      <td><?php echo ($ativo == 0) ? "Sim" : "Não"; ?></td>

                                      <td>
                                        <a class="btn btn-app" onclick="
                                          Swal.fire({
                                            title: 'Editar Registro',
                                            html: `<form id='editForm'>
                                            <label for='status_posicionamento'>Origem:</label>
                                            <input type='text' class='swal2-input'  value='<?php echo htmlspecialchars($nome); ?> 'readonly>
                                            
                                            
                                            <label for='alert'>Alerta:</label>
                                            <input type='text' class='swal2-input' id='alert' name='alert' value='<?php echo htmlspecialchars($alert); ?>'>
                                            
                                            
                                            <label for='ativo'> Ativo:</label>
                                            <select class='swal2-input' id='ativo' name='ativo'>
                                            <option value='0' <?php echo ($ativo === 'sim') ? 'selected' : ''; ?>>Sim</option>
                                            <option value='1' <?php echo ($ativo === 'nao') ? 'selected' : ''; ?>>Não</option>
                                            </select>

                                            
                                           
                                            
                                                                                     
                                            
                                            </form>`,
                                            
                                            icon: 'info',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Salvar Alterações'
                                          }).then((result) => {
                                            if (result.isConfirmed) {
                                              // Obtenha os dados do formulário
                                              var formData = new FormData(document.getElementById('editForm'));
                                              
                                              var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                              
                                              
                                              // Exemplo de código AJAX
                                              $.ajax({
                                                url: 'backend/tools-edit-notification-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                                type: 'POST',
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                success: function(response) {
                                                  
                                                  Swal.fire('Alterações Salvas!', '', 'success');
                                                  // Adicione aqui qualquer ação adicional após o sucesso
                                                  location.href = location.origin + location.pathname + urlParams;
                                                  
                                                },
                                                error: function(error) {
                                                  console.error('Erro ao salvar alterações:', error);
                                                  Swal.fire('Erro ao salvar alterações!', '', 'error');
                                                }
                                              });
                                            }
                                          });
                                        ">
                                          <i class="fa fa-edit"></i> Editar
                                        </a>                                     
                                      </td>
                                      
                                      
                                    </tr>
                                    <?php       }
                                      }
                                    ?>
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        
                                               
                      </div>
                      </div>
                      </div>
                      </div>
          
          <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Calendario</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    
                    
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                      </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  
                  <!-- start accordion -->
                  <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                      <a class="panel-heading" role="tab" id="headingOne55" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne55" aria-expanded="true" aria-controls="collapseOne">
                        <h4 class="panel-title">Integração</h4>
                      </a>
                      <div id="collapseOne55" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Origem</th>
                                <th>Habilitado</th>
                                <th>
                                  Ação
                                </th>
                                
                                
                              </tr>
                            </thead>
                            <tbody>
                              <?php $query = "SELECT id,nome,alert,ativo FROM documentation_notification ";
                                
                                
                                if ($stmt = $conn->prepare($query)) {
                                  $stmt->execute();
                                  $stmt->bind_result($id,$nome,$alert,$ativo);
                                  while ($stmt->fetch()) {
                                    //printf("%s, %s\n", $solicitante, $equipamento);
                                    //  }
                                    
                              ?>
                              <tr>
                                
                                <th scope="row"> <?php   printf($id);   ?></th>
                                <td><?php   printf($nome);   ?></td>
                               
                                <td><?php echo ($ativo == 0) ? "Sim" : "Não"; ?></td>
                                
                                <td>
                                  <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='status_posicionamento'>Origem:</label>
                                      <input type='text' class='swal2-input'  value='<?php echo htmlspecialchars($nome); ?> 'readonly>
                                      
                                      
                                      <label for='alert'>Alerta:</label>
                                      <input type='text' class='swal2-input' id='alert' name='alert' value='<?php echo htmlspecialchars($alert); ?>'>
                                      
                                      
                                      <label for='ativo'> Ativo:</label>
                                      <select class='swal2-input' id='ativo' name='ativo'>
                                      <option value='0' <?php echo ($ativo === 'sim') ? 'selected' : ''; ?>>Sim</option>
                                      <option value='1' <?php echo ($ativo === 'nao') ? 'selected' : ''; ?>>Não</option>
                                      </select>
                                      
                                      
                                      
                                      
                                      
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-notification-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>                                     
                                </td>
                                
                                
                              </tr>
                              <?php       }
                                }
                              ?>
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  
                </div>
              </div>
            </div>
          </div>
          
        <div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Manutenção Preventiva <small>Configuração</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg44"   ><i class="fa fa-plus"><strong style="color: #f0ffff" > Grupo</strong> </i></a>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


          <!-- start accordion -->
          <div class="accordion" id="accordion43" role="tablist" aria-multiselectable="true">
            <div class="panel">
              <a class="panel-heading" role="tab" id="accordion43" data-toggle="collapse" data-parent="#accordion43" href="#accordion43" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">Grupo</h4>
              </a>
              <div id="accordion43" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nome</th>
                        
                        <th>Ação</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php $query = "SELECT id, nome  FROM maintenance_group ";


                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id, $nome );
                        while ($stmt->fetch()) {
 

                          ?>
                          <tr>

                            <th scope="row"> <?php   printf($id);   ?></th>
                            <td><?php   printf($nome);   ?></td>
                     <td> 
                      <td>
                                  <a class="btn btn-app" onclick="
                                    Swal.fire({
                                      title: 'Editar Registro',
                                      html: `<form id='editForm'>
                                      <label for='Nome'>Nome:</label>
                                      <input type='text' class='swal2-input' id='nome' name='nome'  value='<?php echo htmlspecialchars($nome); ?> '>
                                      
                                        
                                      
                                      </form>`,
                                      
                                      icon: 'info',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Salvar Alterações'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        // Obtenha os dados do formulário
                                        var formData = new FormData(document.getElementById('editForm'));
                                        
                                        var urlParams = '?purchases=' + encodeURIComponent('<?php echo $codigoget; ?>');
                                        
                                        
                                        // Exemplo de código AJAX
                                        $.ajax({
                                          url: 'backend/tools-edit-mp-group-backend.php?id=<?php printf($id); ?> ',  // Substitua 'seu_backend_aqui.php' pelo caminho do seu backend
                                          type: 'POST',
                                          data: formData,
                                          processData: false,
                                          contentType: false,
                                          success: function(response) {
                                            
                                            Swal.fire('Alterações Salvas!', '', 'success');
                                            // Adicione aqui qualquer ação adicional após o sucesso
                                            location.href = location.origin + location.pathname + urlParams;
                                            
                                          },
                                          error: function(error) {
                                            console.error('Erro ao salvar alterações:', error);
                                            Swal.fire('Erro ao salvar alterações!', '', 'error');
                                          }
                                        });
                                      }
                                    });
                                  ">
                                    <i class="fa fa-edit"></i> Editar
                                  </a>   
                                  <a class="btn btn-app"  onclick="
                                    Swal.fire({
                                      title: 'Tem certeza?',
                                      text: 'Você não será capaz de reverter isso!',
                                      icon: 'warning',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Sim, Deletar!'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        Swal.fire(
                                          'Deletando!',
                                          'Seu arquivo será excluído.',
                                          'success'
                                        ),
                                        window.location = 'backend/tools-mp-group-trash-backend.php?id=<?php printf($id); ?>';
                                      }
                                    })
                                  ">
                                    <i class="glyphicon glyphicon-trash"></i> Excluir
                                  </a>
                                </td>
                     </td>
                       


                      </tr>
                    <?php       }
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Automação <small>Configuração</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form action="backend/update-tools-backend.php" method="post">
                      <?php $query = "SELECT id, os_rasch, os_rating,os_hfmea, mp,mc,upgrade,operation,os,workflow,email,menu,osupgrade,os_admin,mc_external,mc_internal,scheduled,spare_parts,service_os,os_rasch_single,cal,cal_val,cal_manufacture,prioridade_os,category_os,cal_responsabile,os_mp_1,os_mp_2,os_mp_3,os_mp_4,os_mp_5,quarterly,os_user,email_adm,return_mp,return_type,documentation,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11  FROM tools WHERE id = 1";
                        
                        
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $os_rasch,$os_rating,$os_hfmea,$mp,$mc,$upgrade,$operation,$os,$workflow,$email,$menu,$osupgrade,$os_admin,$mc_external,$mc_internal,$scheduled,$spare_parts,$service_os,$os_rasch_single,$cal,$cal_val,$cal_manufacture,$prioridade_os,$category_os,$cal_responsabile,$os_mp_1,$os_mp_2,$os_mp_3,$os_mp_4,$os_mp_5, $quarterly,$os_user,$email_adm,$return_mp,$return_type,$documentation,$t1,$t2,$t3,$t4,$t5,$t6,$t7,$t8,$t9,$t10,$t11  );
                          while ($stmt->fetch()) {
                            
                          }
                        }
                      ?>
                      <!-- Automação -->
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Assinatura Eletrônica</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="os_rasch"type="checkbox" class="js-switch" <?php if($os_rasch == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Assinatura Eletrônica Unica</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="os_rasch_single"type="checkbox" class="js-switch" <?php if($os_rasch_single == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Pesquisa de Satisfação</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="os_rating"type="checkbox" class="js-switch" <?php if($os_rating == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Temperatura, Umidade e de Concentração de Poluente M.P</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="mp"type="checkbox" class="js-switch" <?php if($mp == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Temperatura e Umidade Calibração</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="mc"type="checkbox" class="js-switch" <?php if($mc == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Desabilitar abertura de O.S</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="os"type="checkbox" class="js-switch" <?php if($os == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilitar Workflow</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="workflow"type="checkbox" class="js-switch" <?php if($workflow == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilitar Email</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="email"type="checkbox" class="js-switch" <?php if($email == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Inicializar Menu Recolhido</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="menu"type="checkbox" class="js-switch" <?php if($menu == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Acesso a O.S Admin</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="os_admin"type="checkbox" class="js-switch" <?php if($os_admin == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Impressão Automatica Calibração</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="cal"type="checkbox" class="js-switch" <?php if($cal == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Ordem de Serviço Técnico </label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <select class="form-control has-feedback-left" name="os_user" id="os_user" >
                                <option>Selecione uma Opção</option>
                                <option value="1" <?php if($os_user == 1){ printf("selected");}?>  > Colaborador</option>
                                <option value="2" <?php if($os_user == 2){ printf("selected");}?>  > Categoria</option>
                                
                                
                              </select>                </label>
                          </div>
                        </div>
                      </div>
                      <script>
                        $(document).ready(function() {
                          $('#os_user').select2();
                        });
                      </script>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Devolutiva Manutenção Preventiva</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="return_mp"type="checkbox" class="js-switch" <?php if($return_mp == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="cal_responsabile" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Empresa de Calibração">Devolutiva Manutenção Preventiva
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="return_type" id="return_type"  >
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM maintenance_return ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>  
                            <option value="<?php printf($id);?>" <?php if($id == $return_type){ printf("selected"); }; ?> > <?php printf($nome);?> </option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                            
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opção "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#return_type').select2();
                        });
                      </script>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Email Administrativo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input  name="email_adm"type="text"  class="form-control" value = "<?php printf($email_adm); ?>" />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Validade Laudo Calibração (Meses)</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input  name="cal_val"type="number"  class="form-control" value = "<?php printf($cal_val); ?>" />
                            </label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="cal_responsabile" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Empresa de Calibração">Responsável Tecnico de Calibração 
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="cal_responsabile" id="cal_responsabile"  >
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, primeironome,ultimonome FROM colaborador  where trash = 1 ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$primeironome,$ultimonome);
                                while ($stmt->fetch()) {
                            ?>  
                            <option value="<?php printf($id);?>" <?php if($id == $cal_responsabile){ printf("selected"); }; ?> > <?php printf($primeironome);?> <?php printf($ultimonome);?>		</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#cal_responsabile').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="cal_manufacture" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Empresa de Calibração">Empresa de Calibração 
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="cal_manufacture" id="cal_manufacture"  placeholder="Unidade">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM calibration_parameter_manufacture  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $cal_manufacture){ printf("selected"); }; ?> > <?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#cal_manufacture').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Erro por operação">Erro por operação (indicador ordem de serviço)
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="operation" id="operation"  placeholder="Unidade">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, defeito FROM os_defeito  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $operation){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#operation').select2();
                        });
                      </script>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Erro por operação">Analise Trimestral
                        </label>
                        
                        <div class="col-md-6 col-sm-6 ">
                          <select multiple="multiple" data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="quarterly[]" id="quarterly"  placeholder="Unidade">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              $stmt = $conn->prepare(" SELECT COUNT(id) FROM os_defeito ");
                              $stmt->execute();
                              $stmt->bind_result($result);
                              while ( $stmt->fetch()) {
                                
                              }
                              $sql = "SELECT  id, defeito FROM os_defeito";
                              
                              $id_unidade=explode(",",$quarterly);
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$instituicao);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id); ?> "<?php for($i = 0; $i <= $result; $i++){ if($id_unidade[$i]==$id){printf("selected");}} ?>	><?php printf($instituicao);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#quarterly').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="osupgrade" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Correcao de solicitação de Serviço">Correção de solicitação de Serviço
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="osupgrade" id="osupgrade"  placeholder="Poscicionamento">
                            <option value=""> Selecione  uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, status FROM os_posicionamento  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $osupgrade){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#osupgrade').select2();
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="mc_external" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="">Indicador Manutenção externa<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="mc_external" id="mc_external"  placeholder="">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM purchases_category  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $mc_external){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opcao "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#mc_external').select2();
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="mc_internal" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="">Indicador Manutenção interna<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="mc_internal" id="mc_internal"  placeholder="">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM purchases_category  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $mc_internal){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opcao "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#mc_internal').select2();
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="scheduled" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="">Indicador Manutenção programada<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="scheduled" id="scheduled"  placeholder="">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM purchases_category  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $scheduled){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opcao "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#scheduled').select2();
                        });
                      </script>
                      
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="spare_parts" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="">Indicador Manutenção reposição<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="spare_parts" id="spare_parts"  placeholder="">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM purchases_category  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $spare_parts){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opcao "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#spare_parts').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="service_os" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="">Tipo de Serviço O.S<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="service_os" id="service_os"  placeholder="">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM custom_service  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $service_os){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opcao "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#service_os').select2();
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="prioridade_os" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="">Prioridade O.S<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="prioridade_os" id="prioridade_os"  placeholder="">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, prioridade FROM os_prioridade  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $prioridade_os){ printf("selected"); }; ?> > <?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opcao "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#prioridade_os').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="category_os" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="">Tipo Categoria Serviço O.S<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="category_os" id="category_os"  placeholder="">
                            <option value=""> Selecione uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, nome FROM category WHERE trash = 1  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $service_os){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma opcao "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#category_os').select2();
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_mp_1" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Poscicionamento">posicionamento para Aguardando Manutenção
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="os_mp_1" id="os_mp_1"  placeholder="Poscicionamento">
                            <option value=""> Selecione  uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, status FROM os_posicionamento  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $os_mp_1){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um poscicionamento "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#os_mp_1').select2();
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_mp_2" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Poscicionamento">posicionamento para Em Analise
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="os_mp_2" id="os_mp_2"  placeholder="Poscicionamento">
                            <option value=""> Selecione  uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, status FROM os_posicionamento  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $os_mp_2){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um poscicionamento "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#os_mp_2').select2();
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_mp_3" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Poscicionamento">posicionamento para Ordem de serviço Cancelada
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="os_mp_3" id="os_mp_3"  placeholder="Poscicionamento">
                            <option value=""> Selecione  uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, status FROM os_posicionamento  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $os_mp_3){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um poscicionamento "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#os_mp_3').select2();
                        });
                      </script>
                      
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_mp_4" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Poscicionamento">Posicionamento para Ordem de serviço em execução
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="os_mp_4" id="os_mp_4"  placeholder="Poscicionamento">
                            <option value=""> Selecione  uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, status FROM os_posicionamento  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $os_mp_4){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um poscicionamento "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#os_mp_4').select2();
                        });
                      </script>
                      
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="os_mp_5" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Poscicionamento">Posicionamento para Ordem de serviço Atualizada
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select data-toggle="tooltip" data-placement="right" title="Tooltip right" type="text" class="form-control has-feedback-right" name="os_mp_5" id="os_mp_5"  placeholder="Poscicionamento">
                            <option value=""> Selecione  uma opção</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, status FROM os_posicionamento  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$defeito);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>" <?php if($id == $os_mp_5){ printf("selected"); }; ?> > <?php printf($defeito);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um poscicionamento "></span>
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#os_mp_5').select2();
                        });
                      </script>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade Energia Elétrica</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t1"type="checkbox" class="js-switch" <?php if($t1 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade Sistema de Agua</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t2"type="checkbox" class="js-switch" <?php if($t2 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade GAS GLP</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t3"type="checkbox" class="js-switch" <?php if($t3 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade GAS Medicinal</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t4"type="checkbox" class="js-switch" <?php if($t4 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade GAS Natural</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t5" type="checkbox" class="js-switch" <?php if($t5 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade Telefonia</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t6" type="checkbox" class="js-switch" <?php if($t6 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                             <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade Diesel</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t7" type="checkbox" class="js-switch" <?php if($t7 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade Geral</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t8" type="checkbox" class="js-switch" <?php if($t8 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                             <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade Nobreak</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t9" type="checkbox" class="js-switch" <?php if($t9 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade Sistema Fotovoltaica</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t10" type="checkbox" class="js-switch" <?php if($t10 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilita Utilidade PMOC</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input data-toggle="tooltip" data-placement="right" title="Tooltip right" name="t11" type="checkbox" class="js-switch" <?php if($t11 == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                          <center>
                            <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                          </center>
                        </div>
                      </div>
                      
                      
                    </form>
                  </div>
                </div>
              </div>
            </div>



<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Logo Documentação<small> </small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="row">
          <div class="col-md-55">

            <div class="thumbnail">
              <div class="image view view-first">
                <img style="width: 100%; display: block;" src="logo/clientelogo.png" alt="image" />
                <div class="mask">
                  <p>Logo Documentação</p>
                  <div class="tools tools-bottom">
                    <a href="logo/clientelogo.png" download><i class="fa fa-download"></i></a>

                  </div>
                </div>
              </div>
              <div class="caption">
                <p>Logo</p>
              </div>
            </div>

          </div>







        </div>
      </div>
    </div>
  </div>
</div>



<div class="x_panel">
  <div class="x_title">
    <h2>Trocar Logo Documentação</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
          class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="alert alert-danger alert-dismissible " role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Atenção!</strong> Utilizar arquivos com a seguinte extensão ".png".
      </div>

      <form  class="dropzone" action="backend/tools-logo-upload-backend.php" method="post">
      </form >



    </div>
  </div>


  <div class="row">
    <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Logo Sistema<small> </small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="row">
            <div class="col-md-55">

              <div class="thumbnail">
                <div class="image view view-first">
                  <img style="width: 100%; display: block;" src="logo/clientelogo.png" alt="image" />
                  <div class="mask">
                    <p>Logo Sistema</p>
                    <div class="tools tools-bottom">
                      <a href="logo/img.jpg" download><i class="fa fa-download"></i></a>

                    </div>
                  </div>
                </div>
                <div class="caption">
                  <p>Logo</p>
                </div>
              </div>

            </div>

          </div>

        </div>
      </div>
    </div>
  </div>











  <div class="x_panel">
    <div class="x_title">
      <h2>Trocar Logo Sistema</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
            class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="alert alert-danger alert-dismissible " role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <strong>Atenção!</strong> Utilizar arquivos com a seguinte extensão ".img".
        </div>
        <form>

        </form>
        <form  class="dropzone" action="backend/tools-logo-sistema-upload-backend.php" method="post">
        </form >



      </div>
    </div>

  </div>
</div>

<!-- Modal adcionar itens de Configuração'-->

<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Categoria</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-category-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>


<!-- -->

<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Tecnovigilância</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-technological-backend.php" method="post">
          <div class="ln_solid"></div>


          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="cod" class="form-control" type="text" name="cod"  >
            </div>
          </div>

          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<div class="modal fade bs-example-modal-lg24" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Tipo</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-equipamento-assessment-type-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>
<div class="modal fade bs-example-modal-lg25" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Avaliação</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-equipamento-assessment-av-backend.php" method="post">
          <div class="ln_solid"></div>


          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Tipo <span class="required"></span>
            </label>
            <div class="input-group col-md-6 col-sm-6">
              <select type="text" class="form-control has-feedback-left" name="id_equipamento_assessment_type" id="id_equipamento_assessment_type" required="required" >
                <option value="">Selecione uma Serviço</option>
                <?php



                $result_cat_post  = "SELECT  id, nome FROM equipamento_assessment_type";

                $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                  echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                }
                ?>

              </select>
              <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>

            </div>
          </div>

          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Avaliação</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<div class="modal fade bs-example-modal-lg26" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Motivo</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-assessment-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Motivo</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Serviço</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-fornecedor-service-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Serviço</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>



<div class="modal fade bs-example-modal-lg19" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Notificação</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-os-compliance-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Notificação</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Questionário</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-fornecedor-question-backend.php" method="post">
          <div class="ln_solid"></div>

          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Serviço <span class="required"></span>
            </label>
            <div class="input-group col-md-6 col-sm-6">
              <select type="text" class="form-control has-feedback-left" name="service" id="service" required="required" placeholder="Espécie">
                <option value="">Selecione uma Serviço</option>
                <?php



                $result_cat_post  = "SELECT  id, service FROM fornecedor_service";

                $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                  echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['service'].'</option>';
                }
                ?>

              </select>
              <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>

            </div>
          </div>





          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Pergunta</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="question" class="form-control" type="text" name="question"  >
            </div>
          </div>








        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>


<!-- -->
<!-- -->

<div class="modal fade bs-example-modal-lg5" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Movimentação</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-equipamento-mov-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Movimentação</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->

<!-- -->

<div class="modal fade bs-example-modal-lg6" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Motivo</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-equipamento-reason-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Motivo</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<!-- -->

<div class="modal fade bs-example-modal-lg7" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Status</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-equipamento-status-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<!-- -->

<div class="modal fade bs-example-modal-lg8" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Defeito</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-os-defeito-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Defeito</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<!-- -->

<div class="modal fade bs-example-modal-lg9" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Posicionamento</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-os-posicionamento-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Posicionamento</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<!-- -->

<div class="modal fade bs-example-modal-lg11" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Avaliação</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-os-rating-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Avaliação</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->

<div class="modal fade bs-example-modal-lg12" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Carimbo </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        <form  class="dropzone" action="backend/tools-carimbo-upload-backend.php" method="post">
        </form >
        <div class="ln_solid"></div>
        <form action="backend/tools-exchange-carimbo-backend.php" method="post">
          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Carimbo <span class="required"></span>
            </label>
            <div class="input-group col-md-6 col-sm-6">
              <select type="text" class="form-control has-feedback-left" name="carimbo" id="carimbo" required="required" placeholder="Espécie">
                <option value="">Selecione uma Carimbo</option>
                <?php



                $result_cat_post  = "SELECT * FROM os_carimbo";

                $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                  echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['carimbo'].'</option>';
                }
                ?>

              </select>
              <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Espécie "></span>

            </div>
          </div>

          <div class="ln_solid"></div>







        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<div class="modal fade bs-example-modal-lg13" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Baixa </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/equipamento-disable-register-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Baixa</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>
     

          <div class="ln_solid"></div>







        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<div class="modal fade bs-example-modal-lg14" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Grupo SLA </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/grupo-sla-register-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grupo</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>
     

          <div class="ln_solid"></div>







        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<div class="modal fade bs-example-modal-lg15" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel"> SLA </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/sla-register-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">SLA</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="sla_primeiro" class="form-control" type="number" name="sla_primeiro"  >
            </div>
          </div>

                

          <div class="ln_solid"></div>


          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Prioridade</label>
            <div class="col-md-6 col-sm-6 ">
            <select class="form-control " name="os_prioridade" id="os_prioridade"  type="text" >
          <?php $query="SELECT id, prioridade,emergencial FROM os_prioridade ";
          $row=1;
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $prioridade,$emergencial);
            while ($stmt->fetch()) {
              ?>

              <option value="<?php printf($id); ?>" <?php if($os_prioridade == $id){ printf("selected");}?>><?php printf($prioridade); ?></option>
            <?php                     }
          }
          ?>

        </select>
                  </div>
          </div>

          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grupo SLA</label>
            <div class="col-md-6 col-sm-6 ">
            <select class="form-control " name="os_grupo_sla" id="os_grupo_sla"  type="text" >
          <?php $query="SELECT id, grupo FROM os_grupo_sla ";
          $row=1;
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
              $stmt->bind_result($id, $grupo);
            while ($stmt->fetch()) {
              ?>

              <option value="<?php printf($id); ?>" <?php if($os_grupo_sla == $id){ printf("selected");}?>><?php printf($grupo); ?></option>
            <?php                     }
          }
          ?>

        </select>            </div>
          </div>
     

          <div class="ln_solid"></div>







        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<div class="modal fade bs-example-modal-lg16" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Prioridade </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/os-prioridade-register-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Prioridade</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="prioridade" class="form-control" type="text" name="prioridade"  >
            </div>
          </div>
     
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align">Emergencial</label>
            <div class="col-md-6 col-sm-6 ">
              <div class="">
                <label>
                  <input name="emergencial"type="checkbox" class="js-switch"  value="0"  />
                </label>
              </div>
            </div>
          </div>
          <div class="ln_solid"></div>







        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>


<div class="modal fade bs-example-modal-lg17" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Workflow </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/os-workflow-register-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Workflow</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="workflow" class="form-control" type="text" name="workflow"  >
            </div>
          </div>

          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Prioridade</label>
            <div class="col-md-6 col-sm-6 ">
            <select class="form-control " name="os_prioridade" id="os_prioridade"  type="text" >
          <?php $query="SELECT id, prioridade,emergencial FROM os_prioridade ";
          $row=1;
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $prioridade,$emergencial);
            while ($stmt->fetch()) {
              ?>

              <option value="<?php printf($id); ?>" <?php if($id_os_prioridade == $id){ printf("selected");}?>><?php printf($prioridade); ?></option>
            <?php                     }
          }
          ?>

        </select>            </div>
          </div>

          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status</label>
            <div class="col-md-6 col-sm-6 ">
            <select class="form-control " name="os_status" id="os_status"  type="text" >
          <?php $query="SELECT id, status,cor FROM os_status ";
          $row=1;
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $status,$cor);
            while ($stmt->fetch()) {
              ?>

              <option value="<?php printf($id); ?>" <?php if($id_os_status == $id){ printf("selected");}?>><?php printf($status); ?></option>
            <?php                     }
          }
          ?>

        </select>            </div>
          </div>

          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Poscionamento</label>
            <div class="col-md-6 col-sm-6 ">
            <select class="form-control " name="os_posicionamento" id="os_posicionamento"  type="text" >
          <?php $query="SELECT id, status,cor FROM os_posicionamento ";
          $row=1;
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $status,$cor);
            while ($stmt->fetch()) {
              ?>

              <option value="<?php printf($id); ?>" <?php if($id_os_posicionamento == $id){ printf("selected");}?>><?php printf($status); ?></option>
            <?php                     }
          }
          ?>

        </select>            </div>
          </div>
     
     
     
     
          <div class="ln_solid"></div>







        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>

<!-- -->
<div class="modal fade bs-example-modal-lg18" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Serviço </h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        
        <div class="ln_solid"></div>
        <form action="backend/os-custom-service-register-backend.php" method="post">
     
     
       <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

          




        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>
<div class="modal fade bs-example-modal-lg22" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modo Instalação</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-equipamento-check-mod-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>
<div class="modal fade bs-example-modal-lg23" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Check List</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-equipamento-check-list-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>
<!-- -->

      
                                
                                <div class="modal fade bs-example-modal-lg44" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Grupo</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="backend/tools-add-mp-group-backend.php" method="post">
          <div class="ln_solid"></div>




          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">nome</label>
            <div class="col-md-6 col-sm-6 ">
              <input id="nome" class="form-control" type="text" name="nome"  >
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary" onclick="new PNotify({
            title: 'Registrado',
            text: 'Informações registrada!',
            type: 'success',
            styling: 'bootstrap3'
          });" >Salvar Informações</button>
        </div>

      </div>
    </div>
  </div>
</form>
                                <!-- -->

<!-- /page content -->
        