

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Utilidades  &amp; Recursos <small>Agua</small></h3>
              </div>


            </div>

              <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">





                       <a class="btn btn-app"  href="utilities-water" onclick="new PNotify({
																title: 'Agua',
																text: 'Empresa de Agua',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-building-o"></i> Empresa de Agua
                  </a>
                    <a  class="btn btn-app" href="utilities-water-unit" onclick="new PNotify({
																title: 'Agua',
																text: 'Unidade de Agua',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-dashboard"></i> Unidade de Agua
                  </a>
                  <a  class="btn btn-app" href="utilities-water-bill" onclick="Agua PNotify({
																title: 'Infraestrutura',
																text: 'Fatura de Agua',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Fatura de Agua
                  </a>

                   <a  class="btn btn-app" href="utilities-water-supply" onclick="new PNotify({
																title: 'Agua',
																text: 'Abastecimento de Agua',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-hospital-o"></i> Abastecimento de Agua
                  </a>
                   <a  class="btn btn-app" href="utilities-water-chart" onclick="new PNotify({
																title: 'Agua',
																text: 'Grafico de Agua',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-bar-chart"></i> Grafico de Agua
                  </a>

                </div>
              </div>

              

             <div class="clearfix"></div>


            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grafico <small>de Agua </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<?php include 'frontend/utilities-water-chart-table-frontend.php';?>



                  </div>
                </div>
              </div>
	       </div>

            <div class="clearfix"></div>

         <!--   <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Unidade Consumidora<small>de Agua </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<?php //include 'frontend/utilities-water-unit-frontend.php';?>



                  </div>
                </div>
              </div>
	       </div>


            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fatura<small> de Agua </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



<?php //clude 'frontend/utilities-water-bill-frontend.php';?>


                  </div>
                </div>
              </div>
	       </div>

	         <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Abastecimento<small> de Agua </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



<?php //include 'frontend/utilities-water-supply-frontend.php';?>


                  </div>
                </div>
              </div>
	       </div>

	        <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Graficos  &amp; Relatorios<small> de Agua </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



<?php //include 'frontend/';?>


                  </div>
                </div>
              </div>
	       </div> -->




                </div>
              </div>
