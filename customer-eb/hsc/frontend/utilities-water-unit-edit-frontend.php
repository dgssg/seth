<?php
$codigoget = ($_GET["unit"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT *FROM unidade_agua where id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$razao,$adress,$cod_consu,$medidor,$unidade,$id_empresa,$obs,$reg_date,$upgrade);



  while ($stmt->fetch()) {

  }
}
?>

<div class="right_col" role="main">

  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>Edição</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <!-- page content -->









      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edição <small>de Empresa</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form action="backend/utilities-water-unit-edit-backend.php" method="post">
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                  </div>
                </div>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Razão</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="razao" class="form-control" type="text" name="razao"  value="<?php printf($razao); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="cod_consu" class="form-control" type="text" name="cod_consu"  value="<?php printf($cod_consu); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Medidor</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="medidor" class="form-control" type="text" name="medidor"  value="<?php printf($medidor); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="unidade" class="form-control" type="text" name="unidade"  value="<?php printf($unidade); ?> " >
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_empresa">Empresa <span class="required"></span>
                  </label>
                  <div class="input-group col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-left" name="id_empresa" id="id_empresa"  placeholder="Empresa">

                      <?php



                      $result_cat_post  = "SELECT  id, nome FROM unidade_agua_empresa where trash = 1";

                      $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                      while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                        if($id_empresa == $row_cat_post['id']){
                          echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'</option>';

                        }
                        echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                      }
                      ?>

                    </select>
                    <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                  </div>
                </div>

                <div class="ln_solid"></div>


<label for="middle-name">Observação</label>

<textarea id="obs" class="form-control" name="obs"
    data-parsley-trigger="keyup" data-parsley-validation-threshold="10"
    placeholder="<?php printf($obs); ?>"
    value="<?php printf($obs); ?>" ><?php printf($obs); ?></textarea>



<div class="ln_solid"></div>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input readonly="readonly" type="text"  class="form-control"  value="<?php printf($reg_date); ?> ">
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input readonly="readonly"  type="text"   class="form-control"  value="<?php printf($upgrade); ?> ">
                  </div>
                </div>


                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                  <div class="col-md-3 col-sm-3 ">
                    <center>
                      <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                    </center>
                  </div>
                </div>


              </form>






            </div>
          </div>
        </div>
      </div>



      <div class="clearfix"></div>


      <div class="x_panel">
        <div class="x_title">
          <h2>Ação</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app"  href="utilities-water-unit">
              <i class="glyphicon glyphicon-arrow-left"></i> Voltar
            </a>



          </div>
        </div>




      </div>
    </div>
    <!-- /page content -->

    <!-- /compose -->

    <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
