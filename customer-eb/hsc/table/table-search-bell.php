<?php
include("../database/database.php");
    $query = "SELECT
    regdate_os_posicionamento.id_os AS id,
    regdate_os_posicionamento.data_end AS data,
    'Posicionamento O.S' AS origem
    FROM regdate_os_posicionamento
    WHERE regdate_os_posicionamento.data_end IS NOT NULL
    UNION ALL
    SELECT
    documentation_bussines.nome AS id,
    documentation_bussines.data_before AS data,
    'Documentação Empresa' AS origem
    FROM documentation_bussines
    WHERE documentation_bussines.data_before IS NOT NULL
    UNION ALL
    SELECT
    documentation_hfmea.titulo AS id,
    documentation_hfmea.data_before AS data,
    'Documentação HFMEA' AS origem
    FROM documentation_hfmea
    WHERE documentation_hfmea.data_before IS NOT NULL
    UNION ALL
    SELECT
    documentation_pop.titulo AS id,
    documentation_pop.data_after AS data,
    'Documentação POP' AS origem
    FROM documentation_pop
    WHERE documentation_pop.data_after IS NOT NULL
    UNION ALL
    SELECT
    documentation_suport.titulo AS id,
    documentation_suport.data_before AS data,
    'Documentação Contingência' AS origem
    FROM documentation_suport
    WHERE documentation_suport.data_before IS NOT NULL
    UNION ALL
    SELECT
    calibration_parameter_tools.nome AS id,
    calibration_parameter_tools.date_validation AS data,
    'Equipamento Calibração' AS origem
    FROM calibration_parameter_tools
    WHERE calibration_parameter_tools.date_validation IS NOT NULL
    UNION ALL
    SELECT
    quarterly_analysis.analysis AS id,
    quarterly_analysis.data_after AS data,
    'Analise Trimestral' AS origem
    FROM quarterly_analysis
    WHERE quarterly_analysis.data_after IS NOT NULL
    UNION ALL
    SELECT
    contracts_period.id_contracts AS id,
    contracts_period.date_end AS data,
    'Contratos' AS origem
    FROM contracts_period
    WHERE contracts_period.date_end IS NOT NULL
    UNION ALL
    SELECT
    equipamento.codigo AS id,
    equipamento.data_val AS data,
    'Equipamento' AS origem
    FROM equipamento
    WHERE equipamento.data_val IS NOT NULL;
";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
