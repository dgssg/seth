<?php
include("../database/database.php");
$query = "SELECT colaborador.primeironome,colaborador.ultimonome,instituicao_area.nome AS 'setor',documentation_suport.class, equipamento_grupo.nome, documentation_suport.id, documentation_suport.id_equipamento_grupo, documentation_suport.titulo, documentation_suport.ver, documentation_suport.file, documentation_suport.data_now, documentation_suport.data_before, documentation_suport.upgrade, documentation_suport.reg_date FROM documentation_suport  LEFT JOIN equipamento_grupo ON equipamento_grupo.id = documentation_suport.id_equipamento_grupo LEFT JOIN instituicao_area ON instituicao_area.id = documentation_suport.id_setor LEFT JOIN colaborador ON colaborador.id = documentation_suport.id_colaborador order by documentation_suport.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
