<?php
include("../database/database.php");
    $query = "SELECT id, unidade, cod, medidor, fornecimento, obs, file, ativo, trash, fabricante, modelo, serie, patrimonio, tipo, caracteristica, tensao, potencia, reg_date, upgrade FROM unidade_seei order by id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
