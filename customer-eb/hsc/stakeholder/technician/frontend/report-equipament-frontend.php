<?php

include("../../database/database.php");

session_start();
$setor = $_SESSION['setor'] ;
//$con->close();


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Equipamentos<small></small></h3>
              </div>


            </div>



            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                    <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                 
                   
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            

            

               <div class="x_panel">
                <div class="x_title">
                  <h2>Equipamentos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  
                </div>
              </div>
           
<?php

$query = "SELECT documentation_hfmea.id,equipamento_familia.manual_use,maintenance_preventive.file,equipamento.comodato,equipamento.baixa,equipamento.ativo,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN maintenance_routine ON maintenance_routine.id_equipamento = equipamento.id LEFT JOIN maintenance_preventive ON maintenance_preventive.id_routine =maintenance_routine.id LEFT JOIN documentation_hfmea ON documentation_hfmea.id_equipamento_grupo = equipamento_familia.id_equipamento_grupo WHERE equipamento.id_instituicao_localizacao in ($setor) GROUP BY equipamento.codigo ORDER BY maintenance_preventive.reg_date DESC ";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($documentation_hfmea,$manual_use,$file,$comodato,$baixa,$ativo,$instituicao,$area,$setor,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">


                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Equipamento</th>
                          <th>Codigo</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          <th>Baixa</th>
                          <th>Ativo</th>
                          <th>Comodato</th>
                          <th>Prontuario</th>
                          <th>Documentos</th>

                          <th>Manual</th>
                         
                          

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($nome); ?> <?php printf($modelo); ?> <?php printf($fabricante); ?></td>
                           <td><?php printf($codigo); ?> </td>
                           <td><?php printf($instituicao); ?></td>
                           <td><?php printf($area); ?></td>
                           <td>  <?php printf($setor); ?></td>
      <td> <?php if($baixa=="0"){printf("Sim");}else{printf("Não");} ?> </td>
       <td> <?php if($ativo=="0"){printf("Sim");}else{printf("Não");} ?> </td>
       <td> <?php if($comodato=="0"){printf("Sim");} else{printf("Não");}?> </td>


       <td>  <a class="btn btn-app"  href="../../register-equipament-prontuario?equipamento=<?php printf($id); ?>" target="_blank" onclick="new PNotify({
                                  title: 'Prontuario',
                                  text: 'Abrir Prontuario',
                                  type: 'sucess',
                                  styling: 'bootstrap3'
                                });">
                                <i class="fa fa-file-pdf-o "></i> 
                              </a></td>



                            <td>
                              <?php if ($file != "") {

                                ?>
                                <a class="btn btn-app"  href="../../dropzone/mp/<?php printf($file); ?>" target="_blank" onclick="new PNotify({
                                  title: 'Laudo',
                                  text: 'Abrir Laudo',
                                  type: 'sucess',
                                  styling: 'bootstrap3'
                                });">
                                <i class="fa fa-file-text-o "></i> Laudo
                              </a>
                            <?php } ?>





                          </td>
                          <td>
                            <?php if ($manual_use != "") {

                              ?>
                              <a class="btn btn-app"  href="../../dropzone/manual/<?php printf($manual_use); ?>" target="_blank" onclick="new PNotify({
                                title: 'Manual',
                                text: 'Abrir Manual',
                                type: 'sucess',
                                styling: 'bootstrap3'
                              });">
                              <i class="fa fa-file-text-o "></i> Manual
                            </a>
                          <?php } ?>





                        </td>
                                           </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>



                </div>
              </div>

               

        <!-- /page content -->
        <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
        initComplete: function () {
            this.api()
                .columns([1,2,3,4,5,6,7,8])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
                    var container = $('<div class="filter-title"></div>')
                                    .appendTo('#userstable_filter');
                    var label = $('<label>'+theadname+': </label>')
                                .appendTo(container); 
                    var select = $('<select  class="form-control my-1"><option value="">' +
        theadname +'</option></select>')
                        .appendTo( '#userstable_filter'  ).select2()
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
 
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });

                
        },
    });
});
</script>