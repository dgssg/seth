<?php

include("../database/database.php");

$codigoget= $_GET['id'];
$tools= $_GET['tools'];
$certificado= $_POST['certificado'];
$date_calibration= $_POST['date_calibration'];
$date_validation= $_POST['date_validation'];
$anexo=$_COOKIE['anexo'];


$stmt = $conn->prepare("UPDATE calibration_parameter_tools_laudos SET certificado = ? WHERE id= ?");
$stmt->bind_param("ss",$certificado,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools_laudos SET date_calibration = ? WHERE id= ?");
$stmt->bind_param("ss",$date_calibration,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools_laudos SET date_validation = ? WHERE id= ?");
$stmt->bind_param("ss",$date_validation,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools_laudos SET file = ? WHERE id= ?");
$stmt->bind_param("ss",$anexo,$codigoget);
$execval = $stmt->execute();

echo "<script>alert('Atualizado!');document.location='../calibration-parameter-equipament-report?tools=$tools'</script>";


?>
