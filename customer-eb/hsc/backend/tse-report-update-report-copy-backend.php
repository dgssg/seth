<?php

include("../database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
	$usuariologado=$_SESSION['id_usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];
$codigoget = ($_GET["laudo"]);

$codigo= $_POST['codigo'];
$id_equipamento= $_POST['equipamento'];
$data_start= $_POST['data_start'];
$val= $_POST['date_validation'];
$id_manufacture= $_POST['id_manufacture'];
$id_colaborador= $_POST['id_colaborador'];
$id_responsavel= $_POST['id_responsavel'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];
$obs= $_POST['obs'];
$equipament_tools= $_POST['equipament_tools'];
	
	$obs_iCheck1= $_POST['obs_iCheck1'];
	$obs_iCheck2= $_POST['obs_iCheck2'];
	$iCheck1= $_POST['iCheck1'];
	$iCheck2= $_POST['iCheck2'];
	$status= $_POST['status'];
	$tse_obs= $_POST['tse_obs'];
	$tse_type= $_POST['tse_type'];
	$tse_assessment= $_POST['tse_assessment'];
	$v4uuid = $_POST['v4uuid'];
	$assinature = $_POST['assinature'];
	$chave= $_POST['chave'];

	
	$stmt = $conn->prepare("UPDATE tse SET tse_obs = ? WHERE id= ?");
	$stmt->bind_param("ss",$tse_obs,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE tse SET tse_type = ? WHERE id= ?");
	$stmt->bind_param("ss",$tse_type,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE tse SET tse_assessment = ? WHERE id= ?");
	$stmt->bind_param("ss",$tse_assessment,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	
	
	$stmt = $conn->prepare("UPDATE tse_report SET obs_iCheck1 = ? WHERE id_calibration= ?");
	$stmt->bind_param("ss",$obs_iCheck1,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE tse_report SET obs_iCheck2 = ? WHERE id_calibration= ?");
	$stmt->bind_param("ss",$obs_iCheck2,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE tse_report SET iCheck1 = ? WHERE id_calibration= ?");
	$stmt->bind_param("ss",$iCheck1,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE tse_report SET iCheck2 = ? WHERE id_calibration= ?");
	$stmt->bind_param("ss",$iCheck2,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	

	$stmt = $conn->prepare("UPDATE tse SET id_tools = ? WHERE id= ?");
	$stmt->bind_param("ss",$val,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();

$stmt = $conn->prepare("UPDATE tse SET val = ? WHERE id= ?");
$stmt->bind_param("ss",$val,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE tse SET id_manufacture = ? WHERE id= ?");
$stmt->bind_param("ss",$id_manufacture,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE tse SET id_colaborador = ? WHERE id= ?");
$stmt->bind_param("ss",$id_colaborador,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE tse SET id_responsavel = ? WHERE id= ?");
$stmt->bind_param("ss",$id_responsavel,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE tse SET codigo = ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();
$stmt->close();


	if(trim("$chave") == trim("$assinature")){ 
		
	
		
		$stmt = $conn->prepare("INSERT INTO  regdate_tse_signature (id_tse, id_user, id_signature) VALUES (?, ?, ?)");
		$stmt->bind_param("sss",$codigoget,$usuariologado,$v4uuid);
		$execval = $stmt->execute();
		$stmt->close();
		
		 
	}
echo "<script>alert('Atualizado!');document.location='../calibration-equipament-edit?laudo=$codigoget'</script>";

?>
