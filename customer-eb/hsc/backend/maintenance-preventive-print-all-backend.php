<?php
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
// Create connection

//$conn_procedure = mysqli_connect("localhost","cvheal47_root","cvheal47_root","cvheal47_building_iscmc");
include("../database/database.php");
include("../database/database-procedure.php");
$id_status=1;
$status=1;
$procedure=1;
$mp=0;
$query = "SELECT equipamento.data_calibration_end, maintenance_control.procedure_mp, maintenance_control.data_after, maintenance_control.id_routine, maintenance_control.data_start, instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_routine.id, maintenance_routine.data_start, maintenance_routine.periodicidade, maintenance_routine.reg_date, maintenance_routine.upgrade, equipamento.codigo, equipamento_familia.nome, equipamento_familia.modelo,  '1' as source,maintenance_control.id
FROM maintenance_routine
INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento
INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id
INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade
INNER JOIN maintenance_control ON maintenance_control.id_routine = maintenance_routine.id
WHERE maintenance_control.data_after <= '$today ' AND maintenance_control.status = 0 AND maintenance_routine.habilitado = 0 and maintenance_routine.mp = $mp

UNION ALL

SELECT equipamento.data_calibration_end, maintenance_control_2.procedure_mp, maintenance_control_2.data_after, maintenance_control_2.id_routine, maintenance_control_2.data_start, instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_routine.id, maintenance_routine.data_start, maintenance_routine.periodicidade, maintenance_routine.reg_date, maintenance_routine.upgrade, equipamento.codigo, equipamento_familia.nome, equipamento_familia.modelo,  '2' as source,maintenance_control_2.id
FROM maintenance_routine
INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento
INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id
INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade
LEFT JOIN maintenance_control_2 ON maintenance_control_2.id_routine = maintenance_routine.id
WHERE maintenance_control_2.data_after <= '$today ' AND maintenance_control_2.status = 0 AND maintenance_routine.habilitado = 0 and maintenance_routine.mp = $mp;

";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($data_calibration_end,$procedure,$data_after,$routine_control,$date_control,$instituicao,$area,$setor,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo,$surce,$id_control);

    while ($stmt->fetch()) {


    if($data_calibration_end != ""){
      $data_calibration_end = date('Y-m', strtotime($data_calibration_end));
      $data_after_calibration = date('Y-m', strtotime($data_after));
    
  if($data_calibration_end <= $data_after_calibration){
 
    $procedure=2;
    

  }
    }

    $stmt_procedure = $conn_procedure->prepare("INSERT INTO maintenance_preventive( id_routine, date_start,id_status,procedure_mp,mp) VALUES (?,?,?,?,?)");
    $stmt_procedure->bind_param("sssss",$rotina,$data_after,$id_status,$procedure,$mp);
    $execval = $stmt_procedure->execute();
    $last_id = $conn_procedure->insert_id;
    $stmt_procedure->close();
    $stmt_procedure = $conn_procedure->prepare("INSERT INTO maintenance_print( id_routine) VALUES (?)");
    $stmt_procedure->bind_param("s",$rotina);

    $execval = $stmt_procedure->execute();
    $stmt_procedure->close();

if($surce == 1){ 
    $stmt_procedure = $conn_procedure->prepare("UPDATE maintenance_control SET status = ? WHERE id_routine= ?");
    $stmt_procedure->bind_param("ss",$status,$rotina);
    $execval = $stmt_procedure->execute();
    $stmt_procedure->close();
}
if($surce == 2){ 
  $stmt_procedure = $conn_procedure->prepare("UPDATE maintenance_control_2 SET status = ? WHERE id_routine= ?");
  $stmt_procedure->bind_param("ss",$status,$rotina);
  $execval = $stmt_procedure->execute();
  $stmt_procedure->close();
}

  }
}


 ?>
<script type="text/javascript">
       window.open('../maintenance-preventive-viwer-all', '_blank');
</script>
<script type="text/javascript">
       window.open('../maintenance-preventive-open-all', '_blank');
</script>
 <script type="text/javascript">
setTimeout(function(){var ww = window.open(window.location, '_self'); ww.close(); }, 1000);
</script>
