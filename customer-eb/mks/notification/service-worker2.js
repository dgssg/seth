self.addEventListener('activate', function(event) {
    event.waitUntil(
        self.registration.showNotification('Bem-vindo', {
            body: 'Você se inscreveu para notificações push!',
            icon: 'path_to_your_icon.png'
        })
    );
});
