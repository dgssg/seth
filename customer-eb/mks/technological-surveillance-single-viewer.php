
<?php

$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../
$query = " SELECT
technological_surveillance.obervacao,
equipamento_familia.nome AS 'familia',
equipamento_familia.modelo,
equipamento_familia.fabricante,
equipamento.codigo,
technological_surveillance.mp,
technological_surveillance.id,
technological_surveillance.id_os,
technological_surveillance.descricao,
technological_surveillance_list.nome,
technological_surveillance.status_tec,
technological_surveillance.acao_tec,
technological_surveillance.data_inicio,
technological_surveillance.data_fim,
(
    SELECT GROUP_CONCAT(regdate_technological_dropzone.file SEPARATOR ';')
    FROM regdate_technological_dropzone
    WHERE  regdate_technological_dropzone.id_technological = technological_surveillance.id
) AS 'id_anexo2'
FROM
technological_surveillance
LEFT JOIN technological_surveillance_list ON technological_surveillance_list.id = technological_surveillance.id_technological
LEFT JOIN os ON os.id = technological_surveillance.id_os AND technological_surveillance.mp = 0
LEFT JOIN maintenance_preventive ON maintenance_preventive.id = technological_surveillance.id_os AND technological_surveillance.mp = 1
LEFT JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine AND technological_surveillance.mp = 1
LEFT JOIN equipamento ON equipamento.id = os.id_equipamento AND technological_surveillance.mp = 0
OR equipamento.id = maintenance_routine.id_equipamento AND technological_surveillance.mp = 1
OR equipamento.id = technological_surveillance.id_os AND technological_surveillance.mp IS NULL
LEFT JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia  WHERE technological_surveillance.id = $codigoget
ORDER BY
technological_surveillance.id DESC ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($obervacao,$equipamento_nome,$equipamento_modelo,$equipamento_fabricante,$equipamento_codigo, $mp, $id,$id_os,$descricao,$risco,$status,$acao,$data_inicio,$data_fim,$anexo);
 
while ($stmt->fetch()) {

}
}

?>
 <script type="text/javascript" language="JavaScript">

    function printPage() {

        if (window.print) {

            agree = confirm("Deseja imprimir essa pagina ?");

            if (agree) {
                window.print();

                if (_GET("pg") != null)
                    location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
                else
                    history.go(-1);


                window.close();
            }



        }
    }

    function noPrint() {
        try {

            if (_GET("pg") != null)
                location.href = _GET("pg") + ".aspx?tp=" + _GET("tp");
            else
                history.go(-1);

            window.close();

        }
        catch (e) {
            alert();
            document.write("Error Message: " + e.message);
        }
    }

    function _GET(name) {
        var url = window.location.search.replace("?", "");
        var itens = url.split("&");

        for (n in itens) {
            if (itens[n].match(name)) {
                return decodeURIComponent(itens[n].replace(name + "=", ""));
            }
        }
        return null;
    }





    function visualizarImpressao() {

        var Navegador = "<object id='Navegador1' width='0' height='0' classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>";

        document.body.insertAdjacentHTML("beforeEnd", Navegador);

        Navegador1.ExecWB(7, 1);

        Navegador1.outerHTML = "";

    }



    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-11247563-6', 'auto');
    ga('send', 'pageview');

    // -->
    </script>

    <style type="text/css">
        page {
            /* background: white;*/
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            text-align: left;
        }

            page[size="A4"] {
                width: 21cm;
                /*height: 29.7cm;                */
            }

                page[size="A4"][layout="portrait"] {
                    width: 29.7cm;
                    /* height: 21cm; */
                }

        @media print {
            body,
            page {
                margin: 0;
                box-shadow: 0;
                page-break-after:always;
            }

            .noprint {
                display: none;
            }
        }




        body {
            font-size: 11px;
            font-family: sans-serif;
        }



        .pagina_retrato {
            width: 720px;
        }

        .pagina_paisagem {
            width: 1025px;
        }

        .relatorio_filtro {
            padding: 10px 0px;
            border-collapse: collapse;
        }

            .relatorio_filtro tr td {
                border: 1px solid #000;
            }


        .relatorio_titulo {
            font-family: Times New Roman;
            font-size: 15px;
            color: Black;
            font-weight: bold;
            text-transform: uppercase;
        }

        .borda_celula {
            border: 1px solid #000;
            background-color: #fff;
        }

        .noprint {
            padding-bottom: 10px;
        }
    </style>

    <style type="text/css" media="print">
        /*
        @page {
            margin: 0.8cm;
        }
        */
        .pagina_retrato, .pagina_paisagem {
            width: 100%;
        }
    </style>

		<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
		<script>
				window.onload = function () {
						JsBarcode(".barcode").init();
				}
		</script>

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous" />
<link href="../App_Themes/Tema1/StyleSheet.css" type="text/css" rel="stylesheet" />

<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEwODA5NzUyMjMPZBYCZg9kFgICAw9kFgICAQ9kFggCAw8PFgIeCEltYWdlVXJsBUd+L2ltYWdlcy9ob3NwaXRhaXMvaV9jcnV6dmVybWVsaGFicmFzaWxlaXJhLWZpbGlhbGRvZXN0YWRvZG9wYXJhbsOhLnBuZ2RkAgUPDxYCHgRUZXh0BQcwMS8yMDE5ZGQCCw88KwARAwAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIeCkhlYWRlclRleHQFBzAxLzIwMTg8KwAFAQAWAh8EBQcwMi8yMDE4PCsABQEAFgIfBAUHMDMvMjAxODwrAAUBABYCHwQFBzA0LzIwMTg8KwAFAQAWAh8EBQcwNS8yMDE4PCsABQEAFgIfBAUHMDYvMjAxODwrAAUBABYCHwQFBzA3LzIwMTg8KwAFAQAWAh8EBQcwOC8yMDE4PCsABQEAFgIfBAUHMDkvMjAxODwrAAUBABYCHwQFBzEwLzIwMTg8KwAFAQAWAh8EBQcxMS8yMDE4PCsABQEAFgIfBAUHMTIvMjAxODwrAAUBABYCHwQFBzAxLzIwMTkWDQIGAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGDBQrAABkAg8PPCsAEQMADxYEHwJnHwNmZAEQFg0CAQICAgMCBAIFAgYCBwIIAgkCCgILAgwCDRYNPCsABQEAFgIfBAUHMDEvMjAxODwrAAUBABYCHwQFBzAyLzIwMTg8KwAFAQAWAh8EBQcwMy8yMDE4PCsABQEAFgIfBAUHMDQvMjAxODwrAAUBABYCHwQFBzA1LzIwMTg8KwAFAQAWAh8EBQcwNi8yMDE4PCsABQEAFgIfBAUHMDcvMjAxODwrAAUBABYCHwQFBzA4LzIwMTg8KwAFAQAWAh8EBQcwOS8yMDE4PCsABQEAFgIfBAUHMTAvMjAxODwrAAUBABYCHwQFBzExLzIwMTg8KwAFAQAWAh8EBQcxMi8yMDE4PCsABQEAFgIfBAUHMDEvMjAxORYNAgYCBgIGAgYCBgIGAgYCBgIGAgYCBgIGAgYMFCsAAGQYAgUeY3RsMDAkQ29udGVudFBsYWNlSG9sZGVyMSRncnYyDzwrAAwBCGZkBR5jdGwwMCRDb250ZW50UGxhY2VIb2xkZXIxJGdydjEPPCsADAEIZmS9tcluC/wCTfFh0PbdSxSO8nxQJa5LJqUDKFT3sJzurQ==" />
</div>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3164E9EE" />
</div>

        <div class="noprint">
            <center>
                <input type="button" value="Não Imprimir" onclick="noPrint();">
                <input type="button" value="Imprimir" onclick="printPage();" >
                 <input type="button" onclick="visualizarImpressao();" value="Visualizar Impressão" >
            </center>
        </div>
         
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="pt-br" />
    <meta http-equiv="refresh" content="7200" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	


    
    <title>SETH</title>
    
	<link rel='stylesheet' type='text/css' href='../../framework/viewer/./css/style.css' />
<link rel='stylesheet' type='text/css' href='../../framework/viewer/./css/jquery-ui-1.8.11.custom.css' />

	<link href="../../framework/viewer/bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
	<link href="../../framework/viewer/bootstrap/css/cus-icons.css" rel="stylesheet"> 
	<link href="../../framework/viewer/bootstrap/css/datatables.bootstrap.css" rel="stylesheet">
	<link href="../../framework/viewer/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<link href="../../framework/viewer/bootstrap/css/bootstrap_custom.css" rel="stylesheet">
	<link href="../../framework/viewer/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet">
	<link href="../../framework/viewer/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../../framework/viewer/css/jquery.countdown.css">
	<link rel="stylesheet" href="../../framework/viewer/bootstrap/css/animate.css">
	
	<script type="text/javascript">
		var CI_ROOT = '../../framework/viewer/index.php';    	 
    </script>
    <script type='text/javascript' src='../../framework/viewer/./js/jquery-1.11.1.min.js'></script>
<script type='text/javascript' src='../../framework/viewer/./js/jquery.dataTables.min.js'></script>
<script type='text/javascript' src='../../framework/viewer/./js/jquery.blockUI.js'></script>
<script type='text/javascript' src='../../framework/viewer/./js/about.js'></script>

    
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../framework/viewer/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    </head>
    	<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
		<script>
				window.onload = function () {
						JsBarcode(".barcode").init();
				}
		</script>
		
    <body>

            <div class="container">
            
            <!--  Topo -->
           
            <!-- Fim do Topo -->

            <!--  Logo -->
            <div class="row">
	        
	               
	                
	              
	          
            </div>
            <!-- Fim do Logo -->
            
            <!--  Menu -->             
           
            <!--  Fim do Menu -->
            
            
            
            

            <link rel="stylesheet" href="../../framework/viewer/css/pdf.css">







<div id="view_content">

    
	<div class="formulario">

		<div class="row" style="display: none;" id="painelComentarios">
			
			
		 <form class="form-horizontal" role="form" id="frm1" name="frm1" action="../../framework/viewer/index.php/comentario/add/5" method="post">
		 
		<div class="panel panel-info">
		  <div class="panel-heading text-center"><i class="cus-comments"></i> <strong>Comentários</strong></div>
		  
		  
		  <div class="panel-body" style="padding-bottom: 10px;">
		  
			  <div style="overflow-y: auto; height: 125px; line-height: 90%;" class="text-justify">
			   			  
			  </div>
			  
			  			  
			  <div class="row" style="padding-top: 10px;">
			  	<div class="col-md-11 text-center">
			  		<textarea class="form-control" name="campoComentario" id="campoComentario" rows="2" style="width: 100%;" maxlength="100"></textarea>
			  	</div>
			  	<div class="help-block text-left small"><span id="caracteresRestantes" class="text-danger"></span></div>
			  </div>
			  
		  </div>
		  
		  <div class="panel-footer">
		  
		  <div class="row">
		  	<div class="col-md-6 text-center">
		  		<button type="button" class="btn btn-default btn-sm" id="btnComentariosClose"><span class="glyphicon glyphicon glyphicon-remove"></span> Fechar</button>
		  	</div>
		  	
		  	<div class="col-md-6 text-center">
		  		<button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon glyphicon-ok"></span> Comentar</button>
		  	</div>
		  
		  </div>
		  
		  </div>
		  
		  
		  
		</div>
		
		  </form>
	</div>
	
	
	<div >
	
	
<div style="padding-bottom: 20px;">
				<table width="100%" style="vertical-align: bottom;">
				<tr>
			<td align="left"><img src="dropzone/logo/<?php printf($logo)?>" style="width:40%"/></td>
			<td align="right"><img src="logo/clientelogo.png" style="width:20%"/></td>
				</tr>
				</table>
				</div><div  style="position: absolute;  text-align: right; margin-top:300px; margin-left: 630px;">
						<img src="carimbo/carimbo_urgente_2.png" width="45px"/> 
					</div><div class="conteudo" style="min-height:900px; font-size:17px;">
				<table style="border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td align="center"><strong>Evento Sentinela  N&ordm; <?php printf($codigoget)?></strong> </td> <td align="center"><strong><svg class="barcode"
																			            jsbarcode-format="auto"
																			            jsbarcode-value="<?php printf($codigoget)?>"
																			            jsbarcode-textmargin="0"
																			            jsbarcode-fontoptions="bold"
																			            jsbarcode-width="2"
																			            jsbarcode-height="40"
																			            jsbarcode-displayValue="false"
																			             >
																			    </svg></strong></td>
</tr>

</tbody>
</table>

<p><b><center> <h2><small>Dados do equipamento:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td width="50%"> <strong> Equipamento:</strong> <?php printf($equipamento_nome)?> </td>
<td><strong>Codigo:</strong> <?php printf($equipamento_codigo)?></td>
</tr>
<tr>
<td><strong>Modelo:</strong> <?php printf($equipamento_modelo); ?></td>
<td><strong>Fabricante:</strong> <?php printf($equipamento_fabricante); ?></td>
</tr>
<tr>
<td><strong> N&ordm; Serie:</strong> <?php printf($equipamento_numeroserie); ?></td>
<td><strong>Patrimonio:</strong> <?php printf($equipamento_patrimonio); ?></td>
</tr>
</tbody>
</table>

<p><b><center> <h2><small>Dados da Tecnovigilancia:</small></h2> </center></b></p>

<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td width="50%"> <strong> Descrição:</strong> <?php printf($descricao)?> </td>
<td><strong>Risco:</strong> <?php printf($risco)?></td>
</tr>
<tr>
<td><strong>Ação:</strong> <?php printf($acao); ?></td>
</tr>
<tr>
<td><strong>Data Inicio:</strong> <?php printf($data_inicio); ?></td>
<td><strong>Data Fim:</strong> <?php printf($data_fim); ?></td>
</tr>
</tbody>
</table>
<table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
<tbody>
<tr>
<td><strong>Observação:</strong> <?php printf($obervacao); ?></td>
</tr>
</tbody>
</table>

<p><b><center> <h2><small>Dados de Assinatura Eletronica:</small></h2> </center></b></p>
              <!--arquivo -->
       <?php
$query="SELECT regdate_technological_surveillance_signature.id_technological_surveillance,regdate_technological_surveillance_signature.id_user,regdate_technological_surveillance_signature.id_signature,regdate_technological_surveillance_signature.reg_date,usuario.nome FROM regdate_technological_surveillance_signature INNER JOIN usuario ON regdate_technological_surveillance_signature.id_user = usuario.id   WHERE regdate_technological_surveillance_signature.id_technological_surveillance like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_equipamento_check_install,$id_user,$id_signature,$reg_date,$id_user);

?>
                     <table style="margin-top: 15px; border: solid 1px black; background-color: #fff; color: black; border-collapse: collapse;" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>

                          <th>Usuário</th>
                          <th>Assinatura</th>
                           <th>Registro</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id_user); ?></td>
                          <td><?php printf($id_signature); ?></td>
                          <td><?php printf($reg_date); ?></td>



                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

			</div>
			
			<table width="100%" style="vertical-align: top;font-family:'Times New Roman',Times,serif; font-size: 11px;">
			<tr>
				<td align="center" colspan="2">
					<div style="padding-top: 20px;"><strong>Copyright © 2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados.</strong><br>www.mksistemasbiomedicos.com.br</div>
				</td>
			</tr>
			<tr>
				<td style="font-size: 9px" align="left">MK-01-ra-1.0</td>	
				<td align="right"><!-- pagina --></td>
			</tr>
		</table>
	
			
	</div>	

	</div>
	<!-- fim da div formulario -->
	
	<div class="row" style="padding-top: 15px">
    
	    

    </div>
	
</div>
<!-- fim da div  view_content -->





 <script type="text/javascript">

 $(document).ready(function(){
	    $("#btnComentarios").click(function(){
	        $("#painelComentarios").toggle();
	    });

	    $("#btnComentariosClose").click(function(){
	        $("#painelComentarios").toggle();
	    });


	    $("#caracteresRestantes").text((100 - $('#campoComentario').val().length));

        $("#caracteresRestantes").hide();
	 		
		$("#campoComentario").focusin(function() {
		    $("#caracteresRestantes").show();
		}).focusout(function () {
		    $("#caracteresRestantes").fadeOut("medium");
		});

		$('#campoComentario').on('keyup',function(){
			   var charCount = $(this).val().length;
			   var restante = 100 - charCount;
				$("#caracteresRestantes").text(restante);
		});
	});
 </script>	
            
            </div>
            <!--  Fim do container-->

  
            <!--  Rodape -->
           <section id="footer" class="section footer">
			<div class="container">
				<div class="row animated opacity mar-bot20" data-andown="fadeIn" data-animation="animation">
					<div class="col-sm-12 align-center">
	                    <ul class="social-network social-circle">
	                    	<!-- 
	                        <li><a href="#" class="icoRss" title="Rss"><i class="fa fa-rss"></i></a></li>
	                        <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
	                         
	                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
	                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
	                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
	                        -->
	                        
	                    </ul>				
					</div>
				</div>
	
				<div class="row align-center copyright">
						<div class="col-sm-12"><p>Copyright © 2013-2021 MK Sistemas - by MK Sistemas. Todos os direitos reservados. <a href="http://www.mksistemasbiomedicos.com.br" target="_blank"style="color:#037E45; text-decoration: none; font-weight:bold;">www.mksistemasbiomedicos.com.br</a></p></div>
				</div>
			</div>

			</section>
            <!--  Fim do Rodape  -->
           
           <div id="modalDialog" style="display:none; min-height: 300px;">
				<div class="title">
					SETH				</div>
				<div class="close"><a href="#" id="bt_cancelar"> X </a></div>
				<div class="text">
					
<div id="Layer1" style="position: relative; width:100%; height:200px; overflow: auto; text-align: justify; padding-left: 3px; padding-right: 10px;">
	<br>
	The SETH System for you.
	<br><br>

	We solve the problem of adverse events in health institutions, using our own technology, integrating processes and steps with prevention, provision and quality, patient safety, surgery safety, reduction of expenses with materials in the production process, time optimization with manual processes and visual, greater safety in the traceability stages, reduction of losses due to expiration date, containment of the suspension of the surgical procedure..

	<br>

	

		<br><br><br><br>
</div>

<div style="text-align: right; position: relative; top:4px; z-index: 9; padding-top: 8px; color:#888">
	MK Sistemas<br>
		<a href="https://www.mksistemasbiomedicos.com.br" target="_blank">https://www.mksistemasbiomedicos.com.br</a>
</div>

				</div>
				<div class="foot"></div>
			</div> 
			
			<div class="modal fade" id="modalSobre" tabindex="-1" role="dialog" aria-labelledby="modalSobre" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
			        <h4 class="modal-title" id="exampleModalLabel">
			        	SETH			        </h4>
			      </div>
			      <div class="modal-body">
			        
<div id="Layer1" style="position: relative; width:100%; height:200px; overflow: auto; text-align: justify; padding-left: 3px; padding-right: 10px;">
	<br>
	The SETH System for you.
	<br><br>

	We solve the problem of adverse events in health institutions, using our own technology, integrating processes and steps with prevention, provision and quality, patient safety, surgery safety, reduction of expenses with materials in the production process, time optimization with manual processes and visual, greater safety in the traceability stages, reduction of losses due to expiration date, containment of the suspension of the surgical procedure..

	<br>

	

		<br><br><br><br>
</div>

<div style="text-align: right; position: relative; top:4px; z-index: 9; padding-top: 8px; color:#888">
	MK Sistemas<br>
		<a href="https://www.mksistemasbiomedicos.com.br" target="_blank">https://www.mksistemasbiomedicos.com.br</a>
</div>

			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			      </div>
			    </div>
			  </div>
			</div>
						
			 
		
			
			
			<div class="modal fade" id="modalAguarde" tabindex="-1" role="dialog" aria-labelledby="modalAguarde" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			    
			      <div class="modal-header">
			        <h4 class="modal-title" id="exampleModalLabel">
			        	Aguarde
			        </h4>
			      </div>
			      
			      <div class="modal-body">
			     		<p> Enviando... </p>
			        	<div class="progress">
			        		
			        		 
						  <div class="progress-bar progress-bar-striped active" id="bar" role="progressbar" aria-valuemin="0" aria-valuenow="1" aria-valuemax="90" style="width: 0%;">
						    <span class="sr-only">45% Complete</span>
						  </div>
						  
						</div>
			      </div>

			    </div>
			  </div>
			</div>
      
	         <script src="../../framework/viewer/bootstrap/js/bootstrap.min.js"></script>
	         <script src="../../framework/viewer/bootstrap/js/datatables.bootstrap.js"></script>
	         <script src="../../framework/viewer/bootstrap/js/bootstrap-select.min.js"></script>
	         
	         <script src="../../framework/viewer/js/countdown/jquery.countdown.js"></script>
			 <script src="../../framework/viewer/js/countdown/jquery.countdown-pt-BR.js"></script>
		
	         <script type="text/javascript">

	         	 $('[data-toggle="popover"]').popover();
	        
		         $('span.countdown').countdown({until: 7183, compact: true, 
								        	    layout: '{hnn}h{sep}{mnn}m{sep}{snn}s',
								        	    expiryUrl: "../../framework/viewer/"
			        	    				});
	
		         $("a.btn").popover();

		         $("a.input-group-addon").popover();

		         $( "#emblema" ).hover(function() {
		        	 $( this ). toggleClass('animated rubberBand');
		        });

		        // $("#modalAguarde").modal('show');
		         
		        //--- Fale conosco ---//
				$('#contactForm').submit(function() {
				    var pass = true;
				    //some validations
				
				    if(pass == false){
				        return false;
				    }
				    $("#modalFaleconosco").modal('hide');
				    $("#modalAguarde").modal('show');
				
					var bar = $('.progress-bar');
					var val = 0;

					(function myLoop (i) {
        	    	        setTimeout(function () {
        	    	            console.log(val);
        	    	            val += 10;
        	    	            bar.attr('aria-valuenow', val);
        	    	            bar.css('width', val + '%');
        	    	            if (--i) myLoop(i);
        	    	        }, 500)
        	    	    })(100);

	        	    return true;
	        	});
				 //--- Fim do Fale conosco ---//	
	         </script>

<script type="text/javascript">
$.ajaxSetup({ cache: false })

function limpaUrl() {     //função
    urlpg = $(location).attr('href');   //pega a url atual da página
    urllimpa = urlpg.split("?")[0]      //tira tudo o que estiver depois de '?'

    window.history.replaceState(null, null, urllimpa); //subtitui a url atual pela url limpa
  
}
limpaUrl();
</script>
         
    </body>
</html>
