<?php
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");

// Inclui arquivos e conecta ao banco de dados
include("../database/database.php");
include("../database/database-procedure.php");

$id_status = 1;
$status = 1;
$procedure = 1;

$query = "SELECT equipamento.data_calibration_end, maintenance_control.procedure_mp, maintenance_control.data_after, maintenance_control.id_routine, maintenance_control.data_start, instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_routine.id, maintenance_routine.data_start, maintenance_routine.periodicidade, maintenance_routine.reg_date, maintenance_routine.upgrade, equipamento.codigo, equipamento_familia.nome, equipamento_familia.modelo,  '1' as source, maintenance_control.id
FROM maintenance_routine
INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento
INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id
INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade
INNER JOIN maintenance_control ON maintenance_control.id_routine = maintenance_routine.id
WHERE maintenance_control.data_after <= '$today' AND maintenance_control.status = 1 AND maintenance_routine.habilitado = 0

UNION ALL

SELECT equipamento.data_calibration_end, maintenance_control_2.procedure_mp, maintenance_control_2.data_after, maintenance_control_2.id_routine, maintenance_control_2.data_start, instituicao.instituicao, instituicao_area.nome, instituicao_localizacao.nome, maintenance_routine.id, maintenance_routine.data_start, maintenance_routine.periodicidade, maintenance_routine.reg_date, maintenance_routine.upgrade, equipamento.codigo, equipamento_familia.nome, equipamento_familia.modelo,  '2' as source, maintenance_control_2.id
FROM maintenance_routine
INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento
INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id
INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade
LEFT JOIN maintenance_control_2 ON maintenance_control_2.id_routine = maintenance_routine.id
WHERE maintenance_control_2.data_after <= '$today' AND maintenance_control_2.status = 0 AND maintenance_routine.habilitado = 0;";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($data_calibration_end, $procedure, $data_after, $routine_control, $date_control, $instituicao, $area, $setor, $rotina, $data_start, $periodicidade, $reg_date, $upgrade, $codigo, $nome, $modelo, $surce, $id_control);

    while ($stmt->fetch()) {
        echo "Original procedure: $procedure\n";  // Depuração
printf("<br>");
        if ($data_calibration_end != "") {
            $data_calibration_end = date('Y-m', strtotime($data_calibration_end));
            $data_after_calibration = date('Y-m', strtotime($data_after));

            echo "Data Calibration End: $data_calibration_end\n";  // Depuração
            printf("<br>");
            echo "Data After Calibration: $data_after_calibration\n";  // Depuração
            printf("<br>");

            if ($data_calibration_end <= $data_after_calibration) {
                echo "Data Calibration End <= Data After Calibration\n";  // Depuração
                printf("<br>");
                     $procedure = 2;
                    echo "Procedure set to 2\n";  // Depuração
                    printf("<br>");
                
            }
        }

        echo "Final procedure: $procedure\n";  // Depuração
printf("<br>");
   
 
     
    }
}
?>
