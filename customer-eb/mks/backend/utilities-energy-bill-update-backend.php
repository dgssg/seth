<?php

include("../database/database.php");
$codigoget = ($_GET["bill"]);
$read_before  = $_POST['read_before'];
$read_after  = $_POST['read_after'];
$registro = $_POST['registro'];
$n_nf = $_POST['n_nf'];
$d_nf = $_POST['d_nf'];



$stmt = $conn->prepare("UPDATE unidade_energia_fatura SET read_before = ? WHERE id= ?");
$stmt->bind_param("ss",$read_before,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_energia_fatura SET read_after = ? WHERE id= ?");
$stmt->bind_param("ss",$read_after,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_energia_fatura SET registro = ? WHERE id= ?");
$stmt->bind_param("ss",$registro,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_energia_fatura SET n_nf = ? WHERE id= ?");
$stmt->bind_param("ss",$n_nf,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_energia_fatura SET d_nf = ? WHERE id= ?");
$stmt->bind_param("ss",$d_nf,$codigoget);
$execval = $stmt->execute();
$stmt->close();





header('Location: ../utilities-energy-bill-edit?bill='.$codigoget);
?>
