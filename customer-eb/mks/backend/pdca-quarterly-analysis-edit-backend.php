<?php

include("../database/database.php");

$codigoget = $_POST['id'];
$id_quarterly = $_POST['id_quarterly'];
$id_kpi_year = $_POST['id_kpi_year'];
$analysis = $_POST['analysis'];
$action = $_POST['action'];
$id_colaborador = $_POST['id_colaborador'];
$data_after = $_POST['data_after'];
$data_execution = $_POST['data_execution'];
$id_instituicao = $_POST['id_instituicao'];



$stmt = $conn->prepare("UPDATE quarterly_analysis SET id_quarterly = ? WHERE id= ?");
$stmt->bind_param("ss",$id_quarterly,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE quarterly_analysis SET id_kpi_year = ? WHERE id= ?");
$stmt->bind_param("ss",$id_kpi_year,$codigoget);
$execval = $stmt->execute();


$stmt = $conn->prepare("UPDATE quarterly_analysis SET analysis = ? WHERE id= ?");
$stmt->bind_param("ss",$analysis,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE quarterly_analysis SET action = ? WHERE id= ?");
$stmt->bind_param("ss",$action,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE quarterly_analysis SET id_colaborador = ? WHERE id= ?");
$stmt->bind_param("ss",$id_colaborador,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE quarterly_analysis SET data_after = ? WHERE id= ?");
$stmt->bind_param("ss",$data_after,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE quarterly_analysis SET data_execution = ? WHERE id= ?");
$stmt->bind_param("ss",$data_execution,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE quarterly_analysis SET id_instituicao = ? WHERE id= ?");
$stmt->bind_param("ss",$id_instituicao,$codigoget);
$execval = $stmt->execute();







//echo "New records created successfully";



echo "<script>alert('Atualizado!');document.location='../pdca-quarterly-analysis-edit?id=$codigoget'</script>";
?>
