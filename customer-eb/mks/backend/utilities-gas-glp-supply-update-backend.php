<?php

include("../database/database.php");

$id_unidade= $_POST['id_unidade'];
$date_supply= $_POST['date_supply'];
$nb= $_POST['nb'];
$qtd= $_POST['qtd'];
$vlr= $_POST['vlr'];
$nf= $_POST['nf'];
 


$stmt = $conn->prepare("UPDATE unidade_gas_glp_supply SET id_unidade = ? WHERE id= ?");
$stmt->bind_param("ss",$id_unidade,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_gas_glp_supply SET date_supply = ? WHERE id= ?");
$stmt->bind_param("ss",$date_supply,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_gas_glp_supply SET nb = ? WHERE id= ?");
$stmt->bind_param("ss",$nb,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_gas_glp_supply SET qtd = ? WHERE id= ?");
$stmt->bind_param("ss",$qtd,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_gas_glp_supply SET vlr = ? WHERE id= ?");
$stmt->bind_param("ss",$vlr,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_gas_glp_supply SET nf = ? WHERE id= ?");
$stmt->bind_param("ss",$nf,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../utilities-gas-glp-supply');
?>
