<?php

include("../database/database.php");

$codigoget = $_POST['id'];
$nome = $_POST['nome'];
$fabricante = $_POST['fabricante'];
$modelo = $_POST['modelo'];
$serie = $_POST['serie'];
$patrimonio = $_POST['patrimonio'];

$id_setor=$_POST['duallistbox_demo1'];
foreach($id_setor as $id_setor){
    $id_setor=trim($id_setor);
    if($id_setor==!""){
    $idcategoria=$id_setor.",".$idcategoria;
    }
}
$id_setor=substr($idcategoria, 0, -1);
// $id_setor= $idcategoria;

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$nome,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET fabricante = ? WHERE id= ?");
$stmt->bind_param("ss",$fabricante,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET modelo = ? WHERE id= ?");
$stmt->bind_param("ss",$modelo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET serie = ? WHERE id= ?");
$stmt->bind_param("ss",$serie,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET patrimonio = ? WHERE id= ?");
$stmt->bind_param("ss",$patrimonio,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET id_equipamento_grupo = ? WHERE id= ?");
$stmt->bind_param("ss",$id_setor,$codigoget);
$execval = $stmt->execute();
$stmt->close();

header('Location: ../calibration-parameter-equipament');
?>
