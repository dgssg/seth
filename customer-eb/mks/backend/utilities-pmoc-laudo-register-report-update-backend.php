<?php


include("../database/database.php");

$codigoget = ($_GET["id"]);



$laudo = $_POST['laudo'];
$tipo_laudo= $_POST['tipo_laudo'];
$data_laudo= $_POST['data_laudo'];
$obs = $_POST['obs'];
$anexo=$_COOKIE['anexo'];

 
$stmt = $conn->prepare("UPDATE pmoc_report SET laudo = ? WHERE id= ?");
$stmt->bind_param("ss",$laudo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE pmoc_report SET id_laudo = ? WHERE id= ?");
$stmt->bind_param("ss",$tipo_laudo,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE pmoc_report SET data_laudo = ? WHERE id= ?");
$stmt->bind_param("ss",$data_laudo,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE pmoc_report SET obs = ? WHERE id= ?");
$stmt->bind_param("ss",$obs,$codigoget);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
header('Location: ../utilities-pmoc-laudo-register-report');
?>