<?php

include("../database/database.php");

$stmt = $conn->prepare("UPDATE anvisa SET read_open = 0");
$execval = $stmt->execute();
$stmt->close();

header('Location: ../technological-surveillance?sweet_salve=1');
?>
