<?php

include("../../database/database.php");


//$con->close();


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>POP <small></small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            

            

               <div class="x_panel">
                <div class="x_title">
                  <h2>POP</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


               <?php

$query = "SELECT regdate_pop_dropzone.file,documentation_pop.titulo,documentation_pop.codigo,equipamento_grupo.nome, documentation_pop.id, documentation_pop.id_equipamento_grupo, documentation_pop.file, documentation_pop.pop, documentation_pop.data_now, documentation_pop.data_after, documentation_pop.upgrade, documentation_pop.reg_date FROM documentation_pop INNER JOIN equipamento_grupo on equipamento_grupo.id = documentation_pop.id_equipamento_grupo INNER JOIN regdate_pop_dropzone ON regdate_pop_dropzone.id_pop = documentation_pop.id ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($dropzone,$titulo,$codigo,$grupo,$id, $id_equipamento_grupo, $file, $pop, $data_now, $data_after, $upgrade, $reg_date);
  


?>

 
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Codigo</th>
                          <th>Grupo</th>
                          <th>Titulo</th>
                          <th>Revisão</th> 
                          <th>Proxima</th>
                          
                        
                         
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td ><?php printf($codigo); ?> </td>
                         
                            
                              <td><?php printf($grupo); ?></td>
                                <td><?php printf($titulo); ?></td>
                                
                          <td><?php printf($data_now); ?></td>
                            <td><?php printf($data_after); ?></td>
                      
                          <td>  
                 
                
                 
                    <?php if($dropzone==!""){?>
                   <a class="btn btn-app"   href="../../dropzone/pop/<?php printf($dropzone); ?> " target="_blank"  onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar procediemnto',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-file"></i> Download
                  </a>
                  <?php  }  ?>
                  
                
                
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
           



                </div>
              </div>

                </div>
              </div>


        <!-- /page content -->
