

 <?php

$codigoget = ($_GET["routine"]);

// Create connection

include("../../database/database.php");

$query = "SELECT mp FROM tools";


          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($mp);
            while ($stmt->fetch()) {
              //printf("%s, %s\n", $solicitante, $equipamento);
            }
          }


$query = "SELECT maintenance_preventive.id_mp,maintenance_routine.id_maintenance_procedures_before,maintenance_routine.signature_digital, maintenance_routine.digital, maintenance_routine.id_category,maintenance_preventive.id_mp,maintenance_preventive.id_fornecedor,maintenance_preventive.date_end,maintenance_preventive.file,maintenance_preventive.time_mp,maintenance_preventive.obs_tc,maintenance_preventive.obs_mp,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id where maintenance_preventive.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_tecnico,$id_maintenance_procedures_before,$signature_digital,$digital,$id_category,$id_tecnico,$id_fornecedor,$date_end,$id_anexo,$time_mp,$obs_tc,$obs_mp,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
}
}

?>
<input type="hidden" id="codigoget" name="codigoget" value="<?php printf($codigoget); ?>" >

  <!-- 1 -->
<link href="../../dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="../../dropzone.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Manutenção <small>Preventiva</small> Predial</h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Procedimento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?> " readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="categoria" name="categoria" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="time_ms"  class="form-control" type="text" name="time_ms"  value="<?php printf($time_ms); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade (dias)</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="habilitado" class="form-control" type="text" name="habilitado"  value="<?php printf($periodicidade); ?>" readonly="readonly">
                        </div>
                      </div>



                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>



                  </div>
                </div>
              </div>
	       </div>

<div class="x_panel">
                <div class="x_title">
                  <h2>Fechamento</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


              <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Programada</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="programada_data" class="form-control" type="text" name="programada_data"  value="<?php printf($programada); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Abertura</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($date_start_ms); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Fechamento</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($date_end); ?>" readonly="readonly">
                        </div>
                      </div>


                </div>
              </div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Executante</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li> <a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo -->
       <?php
$query="SELECT regdate_mp_registro.id,colaborador.ultimonome,regdate_mp_registro.id_mp, regdate_mp_registro.id_user,regdate_mp_registro.date_start,regdate_mp_registro.date_end,regdate_mp_registro.time,regdate_mp_registro.reg_date, regdate_mp_registro.upgrade, colaborador.id, colaborador.primeironome FROM regdate_mp_registro INNER JOIN colaborador on regdate_mp_registro.id_user = colaborador.id WHERE regdate_mp_registro.id_mp like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($regdate_mp_registro,$ultimonome,$id_mp,$id_user,$date_start,$date_end,$time,$reg_date,$upgrade,$id,$usuario);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Executante</th>
                          <th>Data de inicio</th>
                          <th>Data de Termino</th>
                          <th>Tempo</th>
                            <th>Ação</th>


                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($usuario); ?> <?php printf($ultimonome); ?></td>
                          <td><?php printf($date_start); ?></td>
                          <td><?php printf($date_end); ?></td>
                           <td><?php printf($time); ?></td>
                           <td>  </td>

                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>




                </div>
              </div>

                <!-- Registro -->
               <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Registro</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                        <!-- Registro forms-->
                          <form action="backend/maintenance-preventive-shut-down-register-backend.php?routine=<?php printf($codigoget);?>" method="post">



                            <div class="field item form-group">
                          <label class="col-form-label col-md-3 col-sm-3  label-align">Colaborador<span
                              class="required">*</span></label>
                          <div class="col-md-6 col-sm-6">
                         	 <select type="text" class="form-control has-feedback-right" name="colaborador" id="colaborador"  placeholder="Colaborador"



											?>
										  	<?php
										     $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE  trash != 0 and  id_categoria like '%$id_category%' ";
                                            if ($stmt = $conn->prepare($sql)) {
	                                    	$stmt->execute();
                                            $stmt->bind_result($id,$primeironome,$ultimonome);
                                             while ($stmt->fetch()) {
                                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                            </div>
                            </div>
                            <script>
                                    $(document).ready(function() {
                                    $('#').select2();
                                      });
                            </script>

                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Data Inicial<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="date" name="date_start"  id="date_start"required='required'></div>
                        <span class="input-group-btn">
                                             <button type="button" onclick="dateStartmp()"class="btn btn-primary">Utilizar Data de Abertura</button>
                                         </span>
                    </div>

                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Data Final<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="date" name="date_end" id="date_end"required='required'></div>
                        <span class="input-group-btn">
                                             <button type="button" onclick="dateStart()"class="btn btn-primary">Utilizar Data de Inicio</button>
                                         </span>
                    </div>

                 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="time" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Tempo <span></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="time" name="time"  class="form-control "   pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99">
                          <span class="input-group-btn">
                                               <button type="button" onclick="datetimemp()"class="btn btn-primary">Utilizar Tempo da Rotina</button>
                                           </span>
                        </div>
                      </div>



                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>
              <!-- Registro -->



    <div class="x_panel">
                <div class="x_title">
                  <h2>Anexos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg3" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo -->
       <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM regdate_mp_dropzone WHERE id_mp like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                          <th>Titulo</th>
                          <th>Registro</th>
                          <th>Atualização</th>
                         <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id); ?></td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($upgrade); ?></td>
                         <td> <a class="btn btn-app"   href="dropzone/mp-dropzone/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Anexo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Anexo
                  </a>
                  
                </td>

                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>




                </div>
              </div>

                <!-- Registro -->
               <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>

                        <div class="modal-body">

                        <!-- Registro forms-->
                          <form action="backend/regdate_mp_dropzone-backend.php?id_mp=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>


                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="titulo" required='required'></div>
                    </div>




                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>

                        </div>
                 </div>
             </form>
              <form  class="dropzone" action="backend/regdate_mp_dropzone-upload-backend.php" method="post">
    </form >
     </div>
                    </div>
                  </div>
              <!-- Registro -->


 <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Anexo <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row">



                   <form  class="dropzone" action="backend/maintenance-preventive-shut-down-upload-backend.php" method="post">
    </form >




                    </div>
                  </div>
                </div>
              </div>
            </div>

            <?php if($signature_digital == "0"){ ?> 
              <?php
    function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
        $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
        $con = 1;
        $var = '';
        while($con < $QuantidadeDeCaracteresDaChave ){
            $n = rand(0, 57); 
            if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
        }else{
            $var = $var.$n.$res[$n];
            $con++;
            }
        }
            $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
        }
        //chamando a função.
       // echo chaveAlfaNumerica(5);
     //   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
 * doing:
 *
 * $uniqid = $prefix . uniqid();
 * $uniqid = uniqid($prefix);
 
printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
 * required on some systems, like Cygwin. This makes uniqid()
 * produce a value like: 4b340550242239.64159797
 
printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
  public static function v3($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = md5($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 3
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function v4() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),

      // 16 bits for "time_mid"
      mt_rand(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  public static function v5($namespace, $name) {
    if(!self::is_valid($namespace)) return false;

    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);

    // Binary Value
    $nstr = '';

    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2) {
      $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }

    // Calculate hash value
    $hash = sha1($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',

      // 32 bits for "time_low"
      substr($hash, 0, 8),

      // 16 bits for "time_mid"
      substr($hash, 8, 4),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 5
      (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      // 48 bits for "node"
      substr($hash, 20, 12)
    );
  }

  public static function is_valid($uuid) {
    return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                      '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  }
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();
//echo $v4uuid; //chave da assinatura
?> 
  <div class="x_panel">
                <div class="x_title">
                  <h2>Assinatura</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                <!--arquivo -->
       <?php    
$query="SELECT regdate_mp_signature.id_mp,regdate_mp_signature.id_user,regdate_mp_signature.id_signature,regdate_mp_signature.reg_date,usuario.nome FROM regdate_mp_signature INNER JOIN usuario ON regdate_mp_signature.id_user = usuario.id   WHERE regdate_mp_signature.id_mp like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_mp,$id_user,$id_signature,$reg_date,$id_user);
  
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th>Usuario</th>
                          <th>Assinatura</th>
                           <th>Registro</th>
                        
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id_user); ?></td>
                          <td><?php printf($id_signature); ?></td>
                          <td><?php printf($reg_date); ?></td>
                         
                         
                          
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

                
              
                
                </div>
              </div>  
                <!-- compose -->
   

<div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Assinatura <small>da Manutenção Preventiva</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                
 <!-- Large modal --> <center>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Assinatura</button>

                  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Assinatura de Manutenção Preventiva</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <h4>Assinatura digital</h4>
                          <p>Para Assinar digite o texto abaixo.</p>
                         <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
                         <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="backend/mp-signature-backend.php?id=<?php printf($codigoget);?>" method="post">

                          <p> <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> <span ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="assinature" name="assinature"  required="required" class="form-control " class="docs-tooltip" data-toggle="tooltip" title=" ">
                          
                        </div>
                        <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "> </label>
                        <div class="col-md-9 col-sm-9 ">
                          <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "> </label>
                        <div class="col-md-9 col-sm-9 ">
                          <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >
                        </div>
                      </div>
                      </div></p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Assinar</button>
                        </div>

                      </div>
                      </form>
                    </div>
                  </div>
                 </center>
                 
                    
                  </div>
                </div>
              </div>
            </div>
  <?php }?> 


  <?php if($digital == "0"){
?> 
<div class="x_panel">
<div class="x_title">
  <h2>Procedimento Digital</h2>
  <ul class="nav navbar-right panel_toolbox">
    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
    </li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
          class="fa fa-wrench"></i></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="#">Settings 1</a>
        </li>
        <li><a href="#">Settings 2</a>
        </li>
      </ul>
    </li>
    <li><a class="close-link"><i class="fa fa-close"></i></a>
    </li>
  </ul>
  <div class="clearfix"></div>
</div>
<div class="x_content">
<div class="ln_solid"></div>


<?php if($digital == "0"){ ?> 
<center>
  <button type="button" class="btn btn-secondary" id="checkAll2" >Limpar</button>
  <button type="button" class="btn btn-secondary" id="checkOK" >Check OK</button>
  <button type="button" class="btn btn-secondary" id="checkNOK" >Check NOK</button>
  <button type="button" class="btn btn-secondary" id="checkNA" >Check NA</button>
</center>


 
<script>
  document.getElementById("checkAll").addEventListener("click", function() {
    var radios = document.querySelectorAll('input[type="radio"]');
    for (var i = 0; i < radios.length; i++) {
      radios[i].checked = true;
    }
  });
</script>


<script>
  document.getElementById("checkAll2").addEventListener("click", function() {
    var radios = document.querySelectorAll('input[type="radio"]');
    for (var i = 0; i < radios.length; i++) {
      radios[i].checked = false;
    }
  });
</script>
<script>
          document.getElementById("checkOK").addEventListener("click", function() {
         //   var okRadio = document.getElementById("ok_' . $row . '");
         //   okRadio.checked = true;
         var radios = document.querySelectorAll('input[type="radio"]');
  for (var i = 0; i < radios.length; i++) {
    if (radios[i].value === "ok") {
      radios[i].checked = true;
    }
  }
          });
</script>

<script>
          document.getElementById("checkNOK").addEventListener("click", function() {
            var radios = document.querySelectorAll('input[type="radio"]');
  for (var i = 0; i < radios.length; i++) {
    if (radios[i].value === "nok") {
      radios[i].checked = true;
    }
  }
          });
        </script>
        <script>
          document.getElementById("checkNA").addEventListener("click", function() {
            var radios = document.querySelectorAll('input[type="radio"]');
  for (var i = 0; i < radios.length; i++) {
    if (radios[i].value === "na") {
      radios[i].checked = true;
    }
  }
          });
        </script>


              <?php
$table = "";
$query="SELECT id, id_maintenance_procedures, classe, position, procedures FROM maintenance_procedures_itens where id_maintenance_procedures like '$id_maintenance_procedures_before' order by position ASC";
$row=1;
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id,$id_maintenance_procedures,$class,$column,$procedures);
  
  $table = '<table class="table table-striped jambo_table bulk_action" id="tabelaOrigem">
  <thead>
    <tr>
      <th>#</th>
      <th>Procedimento</th>
    </tr>
  </thead>
  <tbody>';
  
  while ($stmt->fetch()) { 
    $table .= '<tr>
      <th scope="row">' . $row . '</th>
      <td>
      <center> <b> ' . (($class==1) ? htmlspecialchars_decode($procedures) : "") . ' </b> </center> ';
       if ($class == 2) {
      $table .= htmlspecialchars_decode($procedures) . '
        <div align="right" >
        <label> OK</label> <input type="radio" class="flat" name="iCheck' . $row . '" id="ok_' . $row . '" value="ok" >
        <label> NOK</label>  <input type="radio" class="flat" name="iCheck' . $row . '"  id="nok_' . $row . '" value="nok"> 
        <label> NA</label>   <input type="radio" class="flat" name="iCheck' . $row . '" id="na_' . $row . '" value="na"> 
        </div>
     ';
    } else if ($class == 3) {
      $table .= '<div  class="input-group input-group-sm  ">
        <label class="input-group-addon" for="dataScaleX">' . htmlspecialchars_decode($procedures) . '</label>
        <input type="text" class="form-control" id="dataScaleX"placeholder="" name="ve1_' . $row . '" id="ve1_' . $row . '"value="">
        <label class="input-group-addon" for="dataScaleX">' . htmlspecialchars_decode($procedures) . '</label>
        <input type="text" class="form-control" id="dataScaleX"placeholder="" name="ve2_' . $row . '" id="ve2_' . $row . '"value="">
        <label class="input-group-addon" for="dataScaleX">' . htmlspecialchars_decode($procedures) . '</label>
        <input type="text" class="form-control" id="dataScaleX"placeholder="" name="ve3_' . $row . '" id="ve3_' . $row . '" value="">
        <label class="input-group-addon" for="dataScaleX">

        <label > OK </label>
        <input align="right" type="radio" class="flat" size="5" name="iCheck' . $row . '" id="ok_' . $row . '" value="ok" > 
        <label > NOK </label>
        <input align="right" type="radio" class="flat" size="5" name="iCheck' . $row . '" id="nok_' . $row . '" value="nok" > 
        <label > NA </label>
        <input align="right" type="radio" class="flat" size="5" name="iCheck' . $row . '" id="na_' . $row . '" value="na" > 
         </label>
      </div>
    </td>

  </tr>';
   } $row=$row+1; }
}  
$table .= '</tbody>
</table>';
echo $table;
        ?> 
    <!--    <button  id="saveButton" class="btn btn-primary">Salvar Procedimento</button> -->
        <span class="input-group-btn">
                                           </span>
 <script >
function tabela() {

  //Capturando o conteúdo da tabela
var table = document.getElementById("tabelaDestino").innerHTML;
//document.getElementById("tabela").innerHTML = tableHTML;

//var tableHTML = document.getElementById("tabela").innerHTML;

//var table = document.getElementById("codigoget").value;


//var tableData = tableContent;
//$('#table').val(tableContent);
//var tabelaOrigem = document.getElementById("tabelaOrigem");
  //  var tabelaDestino = document.getElementById("tabelaDestino");
    
 //   var conteudoTabela = "<tr>";
  //  for (var i = 0; i < tabelaOrigem.rows.length; i++) {
  //    conteudoTabela += "<td>" + tabelaOrigem.rows[i].cells[0].children[0].value + "</td></tr>";
   // }
  //  tabelaDestino.innerHTML = conteudoTabela;
  //var table = document.getElementById("tabelaDestino").innerHTML;
  //document.getElementById("table").value = tableData;
 // $('#tabelaDestino').html(conteudoTabela);
  //$('#table').html(conteudoTabela);

  $('#table').val(table);
 
  }
  </script>
  <script>
      $(document).ready(function() {
        $('#botaoSalvar').click(function() {
          var conteudoTabela = "";
          var valorInput = "";
          $('#tabelaOrigem tr').each(function() {
           conteudoTabela += "<tr>" + $(this).html() + "</tr>"; 
      var linha = "<tr>";
   
      $(this).find('td').each(function() {
        var input = $(this).find('input');
        if (input.length > 0) {

        linha += "<td><label>"+ $(this).find('label').eq(0).text() + "</label><input type='" + input.attr('type') + "' name='" + input.attr('name') + "' id='" + input.attr('id') + "' value='" + input.val() + "'";
        if ($(this).find('input:eq(0)').is(':checked')) {
        linha += "checked";
          }
          linha += " >"+ $(this).find('label').eq(1).text() + "<input type='" + input.attr('type') + "' value='" + input.val() + "'";
          if ($(this).find('input:eq(1)').is(':checked')) {
        linha += "checked";
          }
         linha +=  ">" + $(this).find('label').eq(2).text() + "<input type='" + input.attr('type') + "' value='" + input.val() + "'";
         if ($(this).find('input:eq(2)').is(':checked')) {
        linha += "checked";
          } 
        linha += "  ></td>";
        }
      });
    
      linha += "</tr>";
     
      conteudoTabela += linha;
          });
          $('#table').val(conteudoTabela);
        //  $('#tabelaDestino').html(conteudoTabela);
        });
      });
    </script>
  <script>
 // document.getElementById("botaoSalvar").addEventListener("click", function() {
 //   var tabelaOrigem = document.getElementById("tabelaOrigem");
 //   var tabelaDestino = document.getElementById("tabelaDestino");
    
  //  for (var i = 0; i < tabelaOrigem.rows.length; i++) {
  //    var novaLinha = tabelaDestino.insertRow(-1);
  //    var novaCelula = novaLinha.insertCell(0);
  //    novaCelula.innerHTML = tabelaOrigem.rows[i].cells[0].children[0].value;
  //  }
 // });
</script>

<script >
  document.getElementById("saveButton").addEventListener("click", function(){

  //Capturando o conteúdo da tabela
var tableHTML = document.getElementById("tabela").innerHTML;
var table = document.getElementById("codigoget").value;
$('#table').val(tableHTML);

//Enviando o conteúdo da tabela para o PHP via POST
var xhr = new XMLHttpRequest();
xhr.open("POST", "saveTable.php", true);
xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
xhr.send("tableHTML=" + tableHTML + "&tableId=" + tableId);
});
  </script>
 
 <div class="ln_solid"></div>
                      <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <!-- <button type="button" onclick="tabela()"class="btn btn-primary">Salvar Procedimento </button> -->
                            <button id="botaoSalvar" class="btn btn-primary"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });">Salvar</button>

                          </center>
                        </div>
                      </div>  
                      <div class="ln_solid"></div>
                      <table id="tabelaDestino"></table>          

<?php } ?> 



    <div class="ln_solid"></div>
</div>
</div>
    <?php
  }
  ?> 

              <div class="x_panel">
                <div class="x_title">
                  <h2>Fechamento de Manutenção preventiva</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                	 <form  action="backend/maintenance-preventive-shut-down-backend.php?id=<?php printf($codigoget);?>" method="post">

            <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="fornecedor" class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">Fornecedor <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 " >
                         	<select type="text" class="form-control has-feedback-right" name="fornecedor" id="fornecedor"  placeholder="Fornecedor" value="<?php printf($id_fornecedor); ?>" >
				<option> Selecione um Fornecedor	</option>
										  	<?php



										   $sql = "SELECT  id, empresa FROM fornecedor where trash =1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_empresa,$empresa);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_empresa);?>	"><?php printf($empresa);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                                      </div>
                                      
                                     <script type="text/javascript">
    $(document).ready(function(){
        $('#fornecedor').change(function(){
      var id_fornecedor = document.getElementById("fornecedor").value;
         $('#res').html(id_fornecedor);
       
        });
    });
    </script>
      <input type="hidden" name="assessment" id="assessment">
    <script type="text/javascript">
    $(document).ready(function(){
        $('#fornecedor').change(function(){
            $('#assessment').load('sub_categorias_post_assessment_manufacture.php?fornecedor='+$('#fornecedor').val());
        });
    });
    </script>
								 

                                      <button type="button" onclick="javascript:window.open ('assessment_manufacture.php?id_fornecedor=<?php echo $id_empresa; ?>&id=<?php printf($codigoget) ?>&assessment=2', 'janela1', 'width=850, height=850, top=100, left=100') ;return false"class="btn btn-primary" >Avaliar</button>
                                    </div>











								 <script>
                                    $(document).ready(function() {
                                    $('#fornecedor').select2();
                                      });
                                 </script>

						

                       <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Inicial<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="date" name="date_start" id="date_startsecond"required='required'value="<?php printf($date_start_ms); ?>"></div>
                        <span class="input-group-btn">
                                             <button type="button"  onclick="dateStartmpsecond()" class="btn btn-primary">Utilizar Data de Abertura</button>
                                         </span>
                    </div>

                      <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Date Final<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="date" name="date_end" id="date_endsecond" required='required'value="<?php printf($date_end_ms); ?>"></div>
                        <span class="input-group-btn">
                                             <button type="button"  onclick="dateStartsecond()" class="btn btn-primary">Utilizar Data de Inicio</button>
                                         </span>
                    </div>

                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="backlog" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Tempo de Execução <span></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="timepicker1" name="time"  class="form-control " value="<?php printf($time_mp); ?>"  pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99">
                          <span class="input-group-btn">
                                               <button type="button" onclick="datetimempsecond()"class="btn btn-primary">Utilizar Tempo da Rotina</button>
                                           </span>
                        </div>
                      </div>



                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Colaborador</label>
                        <div class="col-md-6 col-sm-6 ">
                         	 <select type="text" class="form-control has-feedback-right" name="colaborador" id="colaborador2"  placeholder="Colaborador">



												<option> Selecione um colaborador	</option>
										  	<?php
										     $sql = "SELECT  id, primeironome,ultimonome FROM colaborador where trash =1 and id_categoria like '%$id_category%'";
                                            if ($stmt = $conn->prepare($sql)) {
	                                    	$stmt->execute();
                                            $stmt->bind_result($id,$primeironome,$ultimonome);
                                             while ($stmt->fetch()) {
                                            ?>
                            <option value="<?php printf($id);?>	"<?php if($id==$id_tecnico){printf("selected");}; ?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                            </div>
                            </div>
                            <script>
                                    $(document).ready(function() {
                                    $('#colaborador2').select2();
                                      });
                            </script>




                          <label for="message_mp">Observação de MP :</label>
                          <textarea id="message_mp" class="form-control" name="message_mp" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($obs_mp); ?>" value="<?php printf($obs_mp); ?>"><?php printf($obs_mp); ?></textarea>

                       <label for="message_tc">Observação Técnica :</label>
                          <textarea id="message_tc" class="form-control" name="message_tc" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10" placeholder="<?php printf($obs_tc); ?>"value="<?php printf($obs_tc); ?>"><?php printf($obs_tc); ?></textarea>

                             <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome"> <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="routine" name="routine" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control " hidden>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="periodicidade" class="form-control" type="text" name="periodicidade"  value="<?php printf($periodicidade); ?>" readonly="readonly" hidden>
                        </div>
                      </div>

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria"><span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="programada" name="programada" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly"hidden>
                        </div>
                      </div>

                      <div class="ln_solid"></div>

                      <?php if($mp == "0"){ ?> 

           <div class="item form-group ">

           <label  >Temperatura  </label>

               <input name="temp" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#26B99A" data-skin="tron" data-thickness=".2" value="25.5" data-step=".1">
            <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($temp); ?> " > -->


           <label >Umidade  </label>

               <input name="hum" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#34495E" data-skin="tron" data-thickness=".2" value="65.5" data-step=".1">
            <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($hum); ?> " > -->
            
              <label >UFC  </label>
              
              <input name="ufc" data-max="100" class="knob" data-width="110" data-height="120" data-displayPrevious=true data-fgColor="#48495A" data-skin="tron" data-thickness=".2" value="65.5" data-step=".1">
              <!-- <input id="carga" class="form-control" type="number" name="carga"  value="<?php printf($ufc); ?> " > -->

         </div>

             <?php }?> 


           
             <input type="hidden" id="table" name="table"   class="form-control" value="">     
 


             <div class="ln_solid"></div>
                      <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                  </form>


                  </div>
              </div>
              <div class="x_panel">
                                                <div class="x_title">
                                                  <h2>Avalição Fornecedor</h2>
                                                  <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                                        class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                          <li><a href="#">Settings 1</a>
                                                          </li>
                                                          <li><a href="#">Settings 2</a>
                                                          </li>
                                                        </ul>
                                                      </li>
                                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                      </li>
                                                    </ul>
                                                    <div class="clearfix"></div>
                                                  </div>
                                                  <div class="x_content">

                                                  
                <!--arquivo -->
       <?php    
$query="SELECT fornecedor.empresa,fornecedor_assessment.reg_date, fornecedor_question.question, fornecedor_assessment_question.id_question,fornecedor_assessment_question.avaliable, fornecedor_service.service, fornecedor_assessment.id_service,fornecedor_assessment.assessment,fornecedor_assessment.number_assessment, fornecedor_assessment.data_assessment, fornecedor_assessment.obs from fornecedor_assessment LEFT JOIN fornecedor_service ON fornecedor_service.id = fornecedor_assessment.id_service LEFT JOIN fornecedor_assessment_question ON fornecedor_assessment_question.id_assessment = fornecedor_assessment.id LEFT JOIN fornecedor_question ON fornecedor_question.id = fornecedor_assessment_question.id_question LEFT JOIN fornecedor ON fornecedor.id = fornecedor_assessment.id_fornecedor WHERE fornecedor_assessment.number_assessment = $codigoget and  fornecedor_assessment.assessment = 2 ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($empresa,$reg_date,$question,$id_question,$avaliable,$service,$id_service,$assessment,$number_assessment,$data_assessment,$obs);
  
?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th>Fornecedor</th>
            <th>Fornecimento</th>
            <th>Data avaliação</th>
            <th>Data do Serviço avaliado</th>
            <th>Tipo do serviço</th>
            <th>Numero do serviço</th>
            <th>Avaliação</th>
            <th>Conceito</th>
            <th>Observação</th>

                        
                        
                        
                        
                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>
                             
                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($empresa); ?> </td>
              <td><?php printf($service); ?></td>
              <td><?php printf($reg_date); ?> </td>
              <td><?php printf($data_assessment); ?> </td>
              <td><?php  switch ($assessment) {
                case '1':
                print("Ordem de Serviço");
                break;
                case '2':
                print("Manutenção Preventiva");
                break;
                case '3':
                print("Calibração");
                break;


              } ?></td>
              <td><?php printf($number_assessment); ?></td>
              <td><?php printf($question); ?></td>
              <td><?php switch ($avaliable) {
                case '1':
                print("Bom");
                break;
                case '2':
                print("Regular");
                break;
                case '3':
                print("Ruim");
                break; }
                ?></td>
                <td><?php printf($obs); ?> </td>
                         
                         
                          
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>

                                          

                                        </div>
                                      </div>
              <table id="tabelaDestino"></table>    
              <div class="x_panel">
                 <div class="x_title">
                   <h2>Ação</h2>
                   <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                           class="fa fa-wrench"></i></a>
                       <ul class="dropdown-menu" role="menu">
                         <li><a href="#">Settings 1</a>
                         </li>
                         <li><a href="#">Settings 2</a>
                         </li>
                       </ul>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                   </ul>
                   <div class="clearfix"></div>
                 </div>
                 <div class="x_content">

                   <a class="btn btn-app"  href="maintenance-preventive">
                     <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                   </a>

         

                 </div>
               </div>

             </div>
           </div>













        <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
             <script type="text/javascript">
$("meuPrimeiroDropzone").dropzone({ url: "upload.php" });
		 Dropzone.options.meuPrimeiroDropzone = {
   paramName: "fileToUpload",
   dictDefaultMessage: "Arraste seus arquivos para cá!",
   maxFilesize: 300,
   accept: function(file, done) {
    if (file.name == "olamundo.png") {
       done("Arquivo não aceito.");
   } else {
     done();
   }
 }
 }
		</script>
    <script>
function dateStartmpsecond() {

  //document.getElementById("date_start").innerHTML ="2022-04-18";
  //  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
  //  $('#date_start').val(date_start);
   var date_start = document.getElementById("programada_data").value;
//  var  date_start = +$('#programada_data').val(programada_data);
  $('#date_startsecond').val(date_start);
      //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



}
function dateStartsecond() {
 var date_end = document.getElementById("date_startsecond").value;
//  var  date_end = +$('#date_start').val(date_start);

  $('#date_endsecond').val(date_end);
}
</script>
<script>
function dateStartmp() {

//document.getElementById("date_start").innerHTML ="2022-04-18";
//  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
//  $('#date_start').val(date_start);
var date_start = document.getElementById("programada_data").value;
//  var  date_start = +$('#programada_data').val(programada_data);
$('#date_start').val(date_start);
  //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



}
function dateStart() {
var date_end = document.getElementById("date_start").value;
//  var  date_end = +$('#date_start').val(date_start);

$('#date_end').val(date_end);
}
</script>
<script>
function datetimemp() {

//document.getElementById("date_start").innerHTML ="2022-04-18";
//  $('#equipamento').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());
//  $('#date_start').val(date_start);
var time_ms = document.getElementById("time_ms").value;
//  var  date_start = +$('#programada_data').val(programada_data);
$('#time').val(time_ms);
  //  $('#date_start').load('sub_categorias_post_setor_equipament.php?area='+$('#area').val());



}
function datetimempsecond() {
var time_ms = document.getElementById("time_ms").value;
//  var  date_end = +$('#date_start').val(date_start);

$('#timepicker1').val(time_ms);
}
</script>
