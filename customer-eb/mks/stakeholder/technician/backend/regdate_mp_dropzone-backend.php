<?php


include("../../../database/database.php");


$id_mp = $_GET['id_mp'];

$titulo = $_POST['titulo'];
$anexo=$_COOKIE['anexo'];

$stmt = $conn->prepare("INSERT INTO regdate_mp_dropzone (id_mp, file,titulo) VALUES (?, ?, ?)");
$stmt->bind_param("sss",$id_mp,$anexo,$titulo);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
header('Location: ../maintenance-preventive-edit?sweet_salve=1&routine='.$id_mp);
?>