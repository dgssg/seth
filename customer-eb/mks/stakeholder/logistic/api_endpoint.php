<?php

// Incluir o arquivo de conexão com o banco de dados
include("../../database/database.php");

 
    $sql = "
    SELECT 
        DATE(start) as date, 
        GROUP_CONCAT(title SEPARATOR ', ') as titles, 
        COUNT(*) as count 
    FROM events_logistica
    GROUP BY DATE(start)
";
    $result = $conn->query($sql);
    
    // Verificar se houve resultados
    $data = [];
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $data[] = [
                "date" => $row['date'], // Data formatada como YYYY-MM-DD
                "titles" => explode(', ', $row['titles']), // Lista de títulos dos eventos
                "count" => (int)$row['count'], // Número de eventos no dia
            ];
        }
    }
    // Retornar os dados no formato JSON
    echo json_encode($data);
    
    // Fechar a conexão com o banco
    $conn->close();
?>

