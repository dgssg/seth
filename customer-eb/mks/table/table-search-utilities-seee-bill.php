<?php
include("../database/database.php");
    $query = "SELECT unidade_seee_fatura.id, unidade_seee.unidade,unidade_seee.cod,month.month, year.yr, unidade_seee_fatura.v_nf,unidade_seee.medidor,unidade_seee_fatura.kwh,unidade_seee_fatura.obs,unidade_seee_fatura.reg_date,unidade_seee_fatura.upgrade FROM unidade_seee_fatura INNER JOIN unidade_seee ON unidade_seee.id = unidade_seee_fatura.id_unidade_seee INNER JOIN month
    ON month.id = unidade_seee_fatura.id_mouth INNER JOIN year ON year.id = unidade_seee_fatura.id_year WHERE unidade_seee_fatura.trash != 0 order by unidade_seee_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
