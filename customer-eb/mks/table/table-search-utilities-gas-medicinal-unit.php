<?php
include("../database/database.php");
$query = "SELECT id, unidade, cod, medidor, fornecimento, obs, file, ativo, trash, gas, mpa, qtd, capacidade, suprimento, upgrade, reg_date, pontos from unidade_gas_medicina where trash != 0 order by id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
