<?php

include("../../database/database.php");


//$con->close();


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Empresa <small></small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            

            

               <div class="x_panel">
                <div class="x_title">
                  <h2>Documentos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <?php

$query = "SELECT
    db.id,
    db.nome,
    db.data_now,
    db.data_before,
    db.upgrade,
    db.reg_date,
    db.view,
    rdz.file AS dropzone
FROM
    documentation_bussines db
LEFT JOIN (
    SELECT
        id_bussines,
        file,
        reg_date,
        @rn := IF(@prev_id = id_bussines, @rn + 1, 1) AS rn,
        @prev_id := id_bussines
    FROM (
        SELECT
            id_bussines,
            file,
            reg_date
        FROM
            regdate_bussines_dropzone
        ORDER BY
            id_bussines, reg_date DESC
    ) AS subquery,
    (SELECT @prev_id := NULL, @rn := 0) AS init
    ORDER BY
        id_bussines, reg_date DESC
) AS rdz ON rdz.id_bussines = db.id AND rdz.rn = 1
where db.view = 0
ORDER BY 
    db.id DESC;
  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id, $nome,$data_now,$data_before,$upgrade,$reg_date,$view,$dropzone);


?>

<div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Emissão</th>
                          <th>Validade</th>
                          
                           <th>Ação</th>
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($nome); ?> </td>
  
                          <td><?php printf($data_now); ?> </td>
                          <td><?php printf($data_before); ?></td>
                            

                          <td>



                          <?php if($dropzone==!""){?>
                   <a class="btn btn-app"   href="../../dropzone/bussines/<?php printf($dropzone); ?> " target="_blank"  onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-file"></i> Download
                  </a>
                  <?php  }  ?>
                  
               
                
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                  
                </div>
              </div>
           



                </div>
              </div>

               

        <!-- /page content -->
