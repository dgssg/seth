<?php
include("../database/database.php");
$query = "SELECT fornecedor_assessment.id_fornecedor,fornecedor.empresa,fornecedor_assessment.reg_date, fornecedor_question.question, fornecedor_assessment_question.id_question,fornecedor_assessment_question.avaliable, fornecedor_service.service, fornecedor_assessment.id_service,fornecedor_assessment.assessment,fornecedor_assessment.number_assessment, fornecedor_assessment.data_assessment, fornecedor_assessment.obs FROM fornecedor_assessment LEFT JOIN fornecedor_service ON fornecedor_service.id = fornecedor_assessment.id_service LEFT JOIN fornecedor_assessment_question ON fornecedor_assessment_question.id_assessment = fornecedor_assessment.id LEFT JOIN fornecedor_question ON fornecedor_question.id = fornecedor_assessment_question.id_question LEFT JOIN fornecedor ON fornecedor.id = fornecedor_assessment.id_fornecedor WHERE fornecedor_question.id > 0  GROUP BY fornecedor.empresa ORDER BY fornecedor_assessment.data_assessment  DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
