<?php
include("../database/database.php");
$query = "SELECT documentation_graduation.id,documentation_graduation.titulo,documentation_graduation.ativo,documentation_graduation.upgrade,documentation_graduation.reg_date,category.nome,equipamento_familia.nome AS 'equipamento',equipamento_familia.fabricante,equipamento_familia.modelo,documentation_graduation.dropzone FROM documentation_graduation  LEFT JOIN category ON category.id = documentation_graduation.id_categoria LEFT JOIN equipamento_familia ON equipamento_familia.id = documentation_graduation.id_equipamento_familia order by documentation_graduation.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
