<?php
include("../database/database.php");
    $query = "SELECT unidade_solar_fatura.id, unidade_solar.unidade,unidade_solar.cod,month.month, year.yr, unidade_solar_fatura.v_nf,unidade_solar.medidor,unidade_solar_fatura.kwh,unidade_solar_fatura.obs,unidade_solar_fatura.reg_date,unidade_solar_fatura.upgrade FROM unidade_solar_fatura INNER JOIN unidade_solar ON unidade_solar.id = unidade_solar_fatura.id_unidade_solar INNER JOIN month
    ON month.id = unidade_solar_fatura.id_mouth INNER JOIN year ON year.id = unidade_solar_fatura.id_year WHERE unidade_solar_fatura.trash != 0 order by unidade_solar_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
