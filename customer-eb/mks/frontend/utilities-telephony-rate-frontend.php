

 <!-- page content -->
 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Utilidades  &amp; Recursos <small>Telefonia</small></h3>
              </div>


            </div>
            <div class="x_panel">
                <div class="x_title">
                  <h2>Alerta</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

               <?php include 'frontend/utilities-alerta-frontend.php?utilities=9';?>

                </div>
              </div>

            <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">





                  <a class="btn btn-app"  href="utilities-telephony" onclick="new PNotify({
																title: 'Energia',
																text: 'Unidade de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-dashboard"></i> Unidade de Telefonia
                  </a>
                    <a  class="btn btn-app" href="utilities-telephony-bill" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Fatura de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-bill-itens" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Itens da Fatura de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-mod" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Modalidade de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-operation" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Operador de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-ratio" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Rateio de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-rate" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Tarifa de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-chart" onclick="new PNotify({
																title: 'Energia',
																text: 'Graficos de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-bar-chart"></i> Graficos de Telefonia
                  </a>

                </div>
              </div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Cadastro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

               <?php include 'frontend/utilities-telephony-rate-register-frontend.php';?>


                </div>
              </div>

              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="userstable_filter" style="column-count:3; -moz-column-count:3; -webkit-column-count:3; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

 
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tarifa de Operadora <small>de Telefonia</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<?php include 'frontend/utilities-telephony-rate-table-frontend.php';?>



                  </div>
                </div>
              </div>
	       </div>


      <!--      <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fatura<small> de Energia Elétrica </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



<?php //include 'frontend/utilities-energy-bill-frontend.php';?>


                  </div>
                </div>
              </div>
	       </div>

	        <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Graficos  &amp; Relatorios<small> de Energia Elétrica </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



<?php // include 'frontend/';?>


                  </div>
                </div>
              </div>
	       </div>  -->




                </div>
              </div>
              <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
        initComplete: function () {
            this.api()
            .columns([1,2,3])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
        // Container para o título, o select e o alerta de filtro
        var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
        
        // Título acima do select
        var title = $('<label>' + theadname + '</label>').appendTo(container);
        
        // Container para o select
        var selectContainer = $('<div></div>').appendTo(container);
        
        var select = $('<select class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo(selectContainer).select2()
        .on('change', function() {
          var val = $.fn.dataTable.util.escapeRegex($(this).val());
          column.search(val ? '^' + val + '$' : '', true, false).draw();
          
          // Remove qualquer alerta existente
          container.find('.filter-alert').remove();
          
          // Se um valor for selecionado, adicionar o alerta de filtro
          if (val) {
            $('<div class="filter-alert">' +
              '<span class="filter-active-indicator">&#x25CF;</span>' +
              '<span class="filter-active-message">Filtro ativo</span>' +
              '</div>').appendTo(container);
          }
          
          // Remove o indicador do título da coluna
          $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
          
          // Se um valor for selecionado, adicionar o indicador no título da coluna
          if (val) {
            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
          }
        });
        
        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
        // Verificar se há filtros aplicados ao carregar a página
        var filterValue = column.search();
        if (filterValue) {
          select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
        }
      });
      
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                

                
        },
    });
});
</script>
