

 <!-- page content -->
 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Utilidades  &amp; Recursos <small>Telefonia</small></h3>
              </div>


            </div>
            <div class="x_panel">
                <div class="x_title">
                  <h2>Alerta</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

               <?php include 'frontend/utilities-alerta-frontend.php?utilities=9';?>

                </div>
              </div>

            <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">





                  <a class="btn btn-app"  href="utilities-telephony" onclick="new PNotify({
																title: 'Energia',
																text: 'Unidade de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-dashboard"></i> Unidade de Telefonia
                  </a>
                    <a  class="btn btn-app" href="utilities-telephony-bill" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Fatura de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-bill-itens" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Itens da Fatura de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-mod" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Modalidade de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-operation" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Operador de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-ratio" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Rateio de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-rate" onclick="new PNotify({
																title: 'Energia',
																text: 'Fatura de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-text-o"></i> Tarifa de Telefonia
                  </a>
                  <a  class="btn btn-app" href="utilities-telephony-chart" onclick="new PNotify({
																title: 'Energia',
																text: 'Graficos de Energia',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-bar-chart"></i> Graficos de Telefonia
                  </a>

                </div>
              </div>

            
          
            <div class="clearfix"></div>

 
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grafico<small>de Telefonia</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<?php include 'frontend/utilities-telephony-chart-table-frontend.php';?>



                  </div>
                </div>
              </div>
	       </div>


     




                </div>
              </div>
            