<?php
$codigoget = ($_GET["os"]);

// Create connection
include("database/database.php");// remover ../

?> 

          <form action="backend/report-report-routine-pmoc-backend.php" method="post"  target="_blank">

            <div class="ln_solid"></div>





            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a institui&ccedil;&atilde;o ">Institui&ccedil;&atilde;o <span class="required" >*</span>
              </label>
              <div class="input-group col-md-6 col-sm-6">
                <select type="text" class="form-control has-feedback-left" name="instituicao" id="instituicao"  placeholder="Unidade">
                  <option value="">Selecione uma unidade</option>

                  <?php



                  $result_cat_post  = "SELECT  id, instituicao FROM instituicao where trash = 1";

                  $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                  while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                    echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
                  }
                  ?>

                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

              </div>
            </div>

            <script>
            $(document).ready(function() {
              $('#instituicao').select2();
            });
            </script>



            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
              </label>
              <div class="input-group col-md-6 col-sm-6">
                <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
                  <option value="">Selecione um Setor</option>
                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


              </div>
            </div>

            <script>
            $(document).ready(function() {
              $('#area').select2();
            });
            </script>

            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Area <span class="required"></span>
              </label>
              <div class="input-group col-md-6 col-sm-6">
                <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Area">
                  <option value="">Selecione uma Area</option>
                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Area "></span>


              </div>
            </div>

            <script>
            $(document).ready(function() {
              $('#setor').select2();
            });
            </script>



            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Equipamento <span class="required" ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento">
                  <option value="">Selecione o equipamento</option>
                  <?php



                  $query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id from equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade where equipamento.pmoc = 0";

                  //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
                  if ($stmt = $conn->prepare($query)) {
                    $stmt->execute();
                    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
                    while ($stmt->fetch()) {
                      ?>
                      <option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($fabricante);?>- <?php printf($modelo);?>	</option>
                      <?php
                      // tira o resultado da busca da memória
                    }

                  }
                  $stmt->close();

                  ?>
                </select>
                <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>

              </div>
            </div>

            <script>
            $(document).ready(function() {
              $('#equipamento').select2();
            });
            </script>

            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Equipamento Familia<span class="required" ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <select type="text" class="form-control has-feedback-right" name="equipamento_familia" id="equipamento_familia"  placeholder="Grupo">
                  <option value="">Selecione uma Familia</option>
                  <?php
                  $sql = "SELECT  id, nome,fabricante,modelo,img FROM equipamento_familia ";
                  if ($stmt = $conn->prepare($sql)) {
                    $stmt->execute();
                    $stmt->bind_result($id,$nome_equipamento,$fabricante_equipamento,$modelo_equipamento,$img);
                    while ($stmt->fetch()) {
                      ?>
                      <option value="<?php printf($id);?>	"><?php printf($nome_equipamento);?> <?php printf($modelo_equipamento);?> <?php printf($fabricante_equipamento);?>	</option>
                      <?php
                      // tira o resultado da busca da memória
                    }
                  }
                  $stmt->close();
                  ?>
                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
              </div>
            </div>

            <script>
            $(document).ready(function() {
              $('#equipamento_familia').select2();
            });
            </script>



            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento ">Equipamento Grupo<span class="required" ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="Grupo">
                  <option value="">Selecione um Grupo</option>
                  <?php
                  $sql = "SELECT  id, nome FROM equipamento_grupo ";
                  if ($stmt = $conn->prepare($sql)) {
                    $stmt->execute();
                    $stmt->bind_result($id,$equipamento_grupo);
                    while ($stmt->fetch()) {
                      ?>
                      <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
                      <?php
                      // tira o resultado da busca da memória
                    }
                  }
                  $stmt->close();
                  ?>
                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
              </div>
            </div>
            <script>
            $(document).ready(function() {
              $('#equipamento_grupo').select2();
            });
            </script>

            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Categoria ">Categoria<span class="required" ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <select type="text" class="form-control has-feedback-right" name="categoria" id="categoria"  placeholder="Categoria">
                  <option value="">Selecione uma Categoria</option>
                  <?php



                  $sql = " SELECT id, nome FROM category where ativo like '0' and trash = 1 ";


                  if ($stmt = $conn->prepare($sql)) {
                    $stmt->execute();
                    $stmt->bind_result($id,$nome);
                    while ($stmt->fetch()) {
                      ?>
                      <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                      <?php
                      // tira o resultado da busca da memória
                    }

                  }
                  $stmt->close();

                  ?>
                </select>
                <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
              </div>
            </div>

            <script>
            $(document).ready(function() {
              $('#categoria').select2();
            });
            </script>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma periodicidade">Periodicidade<span class="required" ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
              <select class="form-control has-feedback-right" tabindex="-1" name="periodicidade" id="periodicidade">
                <option value="">Selecione uma periodicidade</option>
                <option value="1">Diaria [1 dia]</option>
                <option value="7">Semanal [7 dias]</option>
                <option value="14">Bisemanal [14 dias]</option>
                <option value="21">Trisemanal [21 dias]</option>
                <option value="28">Quadrisemanal [28 dias]</option>
                <option value="30">Mensal [30 dias]</option>
                <option value="60">Bimensal [60 dias]</option>
                <option value="90">Trimestral [90 dias]</option>
                <option value="120">Quadrimestral [120 dias]</option>
                <option value="180">Semestral [180 dias]</option>
                <option value="365">Anual [365 dias]</option>
                <option value="730">Bianual [730 dias]</option>

              </select>
              <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Grupo de Equipamento "></span>
            </div>
          </div>


            <script>
                        $(document).ready(function() {
                        $('#periodicidade').select2();
                          });
                     </script>




            <br>


            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "></label>
              <div class="col-md-3 col-sm-3 ">
                <center>
                  <button class="btn btn-sm btn-success" type="submit">Gerar Relatorio</button>
                </center>
              </div>
            </div>
 







    </div>
  </div>
  <!-- /page content -->
  <script type="text/javascript">
  $(document).ready(function(){
    $('#instituicao').change(function(){
      $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
    });
  });
  </script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#area').change(function(){
      $('#setor').load('sub_categorias_post_setor.php?area='+$('#area').val());
    });
  });
  </script>
