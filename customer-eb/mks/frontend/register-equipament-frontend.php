 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Equipamento</small></h3>
              </div>


            </div>

 <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                   <a class="btn btn-app"  href="register-equipament">
                    <i class="glyphicon glyphicon-list-alt"></i> Equipamentos
                  </a>
                    <a class="btn btn-app"  href="register-equipament-register">
                    <i class="glyphicon glyphicon-plus"></i> Cadastro
                  </a>

                  <a class="btn btn-app"  href="register-equipament-group">
                    <i class="fa fa-cog"></i> Grupo
                  </a>

                  <a class="btn btn-app"  href="register-equipament-family">
                    <i class="fa fa-cogs"></i> Familia
                  </a>
                  <a class="btn btn-app"  href="register-equipament-family-movement">
                    <i class="fa fa-eject"></i> Saida de Equipamento
                  </a>
                  <a class="btn btn-app"  href="register-equipament-family-movement-list">
                    <i class="fa fa-list-ol"></i> Historico de Saida de Equipamento
                  </a>
                  <a class="btn btn-app"  href="register-equipament-form">
                    <i class="fa fa-list-ol"></i> Formulario
                  </a>
                  <a class="btn btn-app"  href="register-equipament-location-register">
                    <i class="fa fa-cogs"></i> Localização
                  </a>

                </div>
              </div>

              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>



	       <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta <small> Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                <!--    <li>
  <a id="btn-carregar" class="badge bg-green pull-right">
    <i class="fa fa-plus">
      <strong style="color: #f0ffff">Carregar</strong>
    </i>
  </a>
</li> -->

                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

<!-- consulta -->
<?php include 'frontend/register-equipament-consulta-frontend.php';?>



                  </div>
                </div>
              </div>
	       </div>

	     

          <!--   <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ação <small> Menu</small> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                  <button type="button" class="btn btn-round btn-primary">Familia de Equipamento</button>
                  <button type="button" class="btn btn-round btn-success">Grupo de Equipamento</button>




                  </div>
                </div>
              </div>
	       </div> -->


                </div>
              </div>


        <!-- /page content -->
      