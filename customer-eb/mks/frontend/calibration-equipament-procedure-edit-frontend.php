<?php
include("database/database.php");
$codigoget = ($_GET["id"]);
$query = "SELECT tse_parameter_dados.tse_class,tse_parameter_dados.tse_aplicada, tse_parameter_dados.habilitado,tse_parameter_dados.id_equipamento_grupo, tse_parameter_dados.id, tse_parameter_dados.name,tse_parameter_dados.cod,tse_parameter_dados.reg_date,tse_parameter_dados.upgrade FROM tse_parameter_dados WHERE tse_parameter_dados.id = $codigoget  ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($tse_class,$tse_aplicada,$habilitado,$id_categoria,$id, $name, $codigo, $reg_date, $upgrade);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}

?>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>Procedimento</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="x_panel">
              <div class="x_title">
                <h2>Informações</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="alert alert-success alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>

                    <strong>Classe I</strong>: Peça ativa coberta por isolamento básico e aterramento de proteção<br>
                    <strong>Classe II</strong>: Peça ativa coberta por isolamento duplo ou reforçado<br>
                    <strong> Classe IP</strong>: Fonte de alimentação interna
                  </div>
                  <div class="alert alert-success alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>

                    <strong> Tipo B</strong>: Peça aplicada no paciente aterrada. Sem sistemas de isolação elétrica (não apropriada para aplicação cardíaca)<br>
                    <strong> Tipo BF</strong>: Peça aplicada no paciente fluindo (condutor de superfície). Isolação elétrica, partes aterradas ou acessíveis do equipamento<br>
                    <strong>Tipo CF</strong>: Peça aplicada no paciente fluindo para uso em contato direto com o coração. Aumento da isolação das partes aterradas e outras partes acessíveis do equipamento, limitando a intensidade da possível corrente fluindo através do paciente.
                  </div>



                </div>
              </div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Procedimento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                 <form action="backend/calibration-equipament-procedure-edit-backend.php" method="post">

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($name); ?>"  required="required" class="form-control ">
                        </div>
                      </div>


                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Info</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="info" class="form-control" type="text" name="info"  value="<?php printf($info); ?>" >
                        </div>
                      </div>

                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilitado</label>
                        <div class="col-md-6 col-sm-6 ">
                           <div class="">
                            <label>
                              <input name="habilitado"type="checkbox" class="js-switch" <?php if($habilitado == "0"){printf("checked"); }?> />
                            </label>
                          </div>

                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cod" class="form-control" type="text" name="cod"  value="<?php printf($codigo); ?>" >
                        </div>
                      </div>

                      <div class="item form-group">
                                            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grupo Equipamento</label>
                                            <div class="col-md-6 col-sm-6 ">
                                        <select  multiple="multiple" type="text" class="form-control has-feedback-right" name="id_categoria[]" id="id_categoria"  placeholder="Categoria">
                                          <?php
                          $stmt = $conn->prepare(" SELECT COUNT(id) FROM equipamento_grupo ");
                                              $stmt->execute();
                                              $stmt->bind_result($result2);
                                              while ( $stmt->fetch()) {

                                              }
                        $sql = "SELECT  id, nome FROM equipamento_grupo WHERE trash =1";
                      $id_categoria=explode(",",$id_categoria);
                      $explode2=0;

                      if ($stmt = $conn->prepare($sql)) {
                        $stmt->execute();
                         $stmt->bind_result($id,$nome);
                         while ($stmt->fetch()) {
                         ?>
                        <option value="<?php printf($id); ?> "<?php for($i = 0; $i <= $result2; $i++){ if($id_categoria[$i]==$id){printf("selected");}} ?>	><?php printf($nome);?>	</option>
                                            <?php

                                          // tira o resultado da busca da memória
                                          }

                                                                }
                                          $stmt->close();

                                          ?>

                                                            </select>
                                        <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a categoria "></span>

                                        </div>
                                            </div>

                                     <script>
                                                        $(document).ready(function() {
                                                        $('#id_categoria').select2();
                                                          });
                                                     </script>

                                                     <div class="form-group row">
                                                       <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Classe <span class="required"></span>
                                                       </label>
                                                       <div class="input-group col-md-6 col-sm-6">
                                                         <select type="text" class="form-control has-feedback-left" name="tse_class" id="tse_class" required="required" placeholder="Unidade">
                                                       <option value="">Selecione uma Classe</option>
                                                           <?php



                                                           $result_cat_post  = "SELECT  id, nome FROM tse_class ";

                                                           $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                                                           while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                                             if ($row_cat_post['id'] == $tse_class) {
                                                           echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'</option>';
                                                             }
                                                             echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                                                           }
                                                           ?>

                                                         </select>
                                                         <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                                                       </div>
                                                     </div>

                                                     <script>
                                                     $(document).ready(function() {
                                                       $('#tse_class').select2();
                                                     });
                                                     </script>

                                                     <div class="form-group row">
                                                       <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Aplicada <span class="required"></span>
                                                       </label>
                                                       <div class="input-group col-md-6 col-sm-6">
                                                         <select type="text" class="form-control has-feedback-left" name="tse_aplicada" id="tse_aplicada" required="required" placeholder="Unidade">
                                                       <option value="">Selecione uma Aplicada</option>
                                                           <?php



                                                           $result_cat_post  = "SELECT  id, nome FROM tse_aplicada ";

                                                           $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                                                           while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                                             if ($row_cat_post['id'] == $tse_aplicada) {
                                                           echo '<option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['nome'].'</option>';
                                                             }
                                                             echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                                                           }
                                                           ?>

                                                         </select>
                                                         <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                                                       </div>
                                                     </div>

                                                     <script>
                                                     $(document).ready(function() {
                                                       $('#tse_aplicada').select2();
                                                     });
                                                     </script>




                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
                             title: 'Salvar',
                             text: 'Salvar Informações!',
                             type: 'success',
                             styling: 'bootstrap3'
                           });">Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                                  </form>



                  </div>
                </div>
              </div>
	       </div>


            <div class="clearfix"></div>








             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="calibration-equipament-procedure">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                </div>
              </div>




                </div>
              </div>
