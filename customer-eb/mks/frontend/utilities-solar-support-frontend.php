<?php

include("database/database.php");


//$con->close();


?>

<!-- page content -->
       <div class="right_col" role="main">
         <div class="">
           <div class="page-title">
             <div class="title_left">
                <h3>Contingência <small>Sistema de Energia Fotovoltaica</small>solar</h3>
             </div>


           </div>

           <div class="clearfix"></div>

            

             <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>


             <div class="x_panel">
                <div class="x_title">
                  <h2>Plano de Contingência</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                         <th>#</th>
                          <th>Contingência</th>
                         
                          <th>Titulo</th>
                         
                          <th>Setor</th>
                          <th>Responsável</th>
                          
                          <th>Data</th>
                          <th>Revisão</th>

                  
                          <th>Recurso</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
              
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">

                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Salvar Contingência</h4>
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                   
                      <form action="backend/utilities-solar-suport-edit-suport-backend.php" method="post">

                     
                         <div class="ln_solid"></div>


                     

                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                          <div class="col-md-6 col-sm-6 ">
                            <input id="titulo" class="form-control" type="text" name="titulo"   >
                          </div>
                        </div>

                        
                      
                      





  
                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data</label>
                          <div class="col-md-6 col-sm-6 ">
                            <input id="data_now" class="form-control" type="date" name="data_now"   >
                          </div>
                        </div>
                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Proxima Revisão</label>
                          <div class="col-md-6 col-sm-6 ">
                            <input id="data_before" class="form-control" type="date" name="data_before"   >
                          </div>
                        </div>









                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                   <button type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" >Salvar</button> 
                    </div>
        </form>
                  </div>
                </div>
              </div>
              
            


                              </div>
                            </div>

                </div>
              </div>
                 

           
              <script language="JavaScript">
$(document).ready(function() {
    
$('#datatable').dataTable( {
        "processing": true,
            "stateSave": true,
            responsive: true,
        
         

"language": {
  "loadingRecords": "Carregando dados...",
  "processing": "Processando  dados...",
  "infoEmpty": "Nenhum dado a mostrar",
  "emptyTable": "Sem dados disponíveis na tabela",
  "zeroRecords": "Não há registros a serem exibidos",
  "search": "Filtrar registros:",
  "info": "Mostrando página _PAGE_ de _PAGES_",
  "infoFiltered": " - filtragem de _MAX_ registros",
  "lengthMenu": "Mostrar _MENU_ registros",
  
  "paginate": {
    "previous": "Página anterior",
    "next": "Próxima página",
    "last": "Última página",
    "first": "Primeira página",
 

    
  }
}



      } );


// Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-utilities-solar-suport.php';



// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
.then(response => response.json())
.then(data => {
  // Mapeia os dados para o formato esperado pelo DataTables
  const novosDados = data.map(item => [
    ``,
    item.id,    
    item.titulo,
    item.setor,
    item.primeironome,
    item.data_now,
    item.data_before,
  
    `<a class="btn btn-app"  href="utilities-solar-suport-viewer?id=${item.id}" target="_blank">
    <i class="fa fa-file-pdf-o"></i> Visualizar 1
    </a>
`,
    `<a class="btn btn-app" href="utilities-solar-suport-edit?id=${item.id}" onclick=" new PNotify({
      title: 'Editar',
      text: 'Abrindo Edição',
      type: 'success',
      styling: 'bootstrap3'
    });">
      <i class="fa fa-edit"></i> Editar
    </a>
    <a class="btn btn-app" onclick="Swal.fire({
      title: 'Tem certeza?',
      text: 'Você não será capaz de reverter isso!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, Deletar!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deletando!',
          'Seu arquivo será excluído.',
          'success'
        ),
        window.location = 'backend/utilities-solar-suport-trash.php?id=${item.id}';
      }
    });">
      <i class="fa fa-trash"></i> Excluir
    </a>`
  ]);

  // Inicializa o DataTables com os novos dados
  const table = $('#datatable').DataTable();
  table.clear().rows.add(novosDados).draw();
  

  // Cria os filtros após a tabela ser populada
  $('#datatable').DataTable().columns([1, 2,3,4]).every(function(d) {
    var column = this;
    var theadname = $("#datatable th").eq([d]).text();
    var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
    var label = $('<label>' + theadname + ': </label>').appendTo(container);
    var select = $('<select  class="form-control my-1"><option value="">' +
      theadname + '</option></select>').appendTo('#userstable_filter').select2()
      .on('change', function() {
        var val = $.fn.dataTable.util.escapeRegex($(this).val());
        column.search(val ? '^' + val + '$' : '', true, false).draw();
      });

    column.data().unique().sort().each(function(d, j) {
      select.append('<option value="' + d + '">' + d + '</option>');
    });
  });

  var oTable = $('#datatable').dataTable();

// Avança para a próxima página da tabela
//oTable.fnPageChange('next');



});


// Armazenar a posição atual ao sair da página
$(window).on('beforeunload', function() {
var table = $('#datatable').DataTable();
var pageInfo = table.page.info();
localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
});

// Restaurar a posição ao retornar à página
$(document).ready(function() {
var storedPosition = localStorage.getItem('paginationPosition');
if (storedPosition) {
var pageInfo = JSON.parse(storedPosition);
var table = $('#datatable').DataTable();
table.page(pageInfo.page).draw('page');
}
});


});


</script>   

       <!-- /page content -->
                        