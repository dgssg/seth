<?php
include("database/database.php");
$query = "SELECT maintenance_group.nome AS 'grupo',maintenance_routine.habilitado,maintenance_routine.id_maintenance_procedures_before, equipamento_familia.fabricante,equipamento.serie,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN maintenance_group ON maintenance_group.id = maintenance_routine.group_routine where maintenance_routine.trash =1 and  maintenance_routine.mp = 1" ;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($grupo,$habilitado,$id_maintenance_procedures_before,$fabricante,$serie,$instituicao,$area,$setor,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);



?>


<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Grupo</th>
                          <th>Rotina</th>
                          <th>Data Inicio</th>
                          <th>Periodicidade</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>


                          <th>Atualização</th>
                          <th>Cadastro</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          <th>Procedimento</th>
                          <th>Habilitado</th>
                          <th>Ação</th>


                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                        <td><?php printf($grupo); ?> </td>
                          <td><?php printf($rotina); ?> </td>
                          <td><?php printf($data_start); ?> </td>
                          <td><?php if($periodicidade==5){printf("Diaria [1 Dias Uteis ]");}if($periodicidade==365){printf("Anual [365 Dias]");}if($periodicidade==180){printf("Semestral [180 Dias]");} if($periodicidade==30){printf("Mensal [30 Dias]");}if($periodicidade==1){printf("Diaria [1 Dias]");} if($periodicidade==7){printf("Semanal [7 Dias]");}if($periodicidade==14){printf("Bisemanal [14 Dias]");}if($periodicidade==21){printf("Trisemanal [21 Dias]");} if($periodicidade==28){printf("Quadrisemanal [28 Dias]");} if($periodicidade==60){printf("Bimestral [60 Dias]");}if($periodicidade==90){printf("Trimestral [90 Dias]");}if($periodicidade==120){printf("Quadrimestral [120 Dias]");}if($periodicidade==730){printf("Bianual [730 Dias]");}  ?></td>
                          <td><?php printf($nome); ?> </td>
                          <td> <?php printf($modelo); ?></td>
                          <td> <?php printf($fabricante); ?></td>
                           <td><?php printf($codigo); ?> </td>
                           <td><?php printf($serie); ?> </td>
 
                                <td><?php printf($upgrade); ?></td>

                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($instituicao); ?></td>
                             <td><?php printf($area); ?></td>
                             <td>  <?php printf($setor); ?></td>
                             <td>  <?php if($id_maintenance_procedures_before == null){ printf("Não"); } if($id_maintenance_procedures_before != null){ printf("Sim"); } ?></td>
                             <td>  <?php if($habilitado  != "0"){ printf("Não"); } if($habilitado == "0"){ printf("Sim"); } ?></td>

                          <td>
                   <a class="btn btn-app"  href="maintenance-routine-edit?routine=<?php printf($rotina); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>


                  <a class="btn btn-app"  href="maintenance-routine-viwer?routine=<?php printf($rotina); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar procediemnto',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                   <a class="btn btn-app"  href="backend/maintenance-routine-retweet-backend.php?routine=<?php printf($rotina); ?>" target="_blank"   onclick="new PNotify({
																title: 'Manutenção Preventiva',
																text: 'Manutenção Preventiva Retroativo',
																type: 'warning',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-retweet"></i> MP Retroativo
                  </a>
                   <a class="btn btn-app"  href="backend/maintenance-routine-fast-forward-backend.php?routine=<?php printf($rotina); ?>" target="_blank"   onclick="new PNotify({
																title: 'Manutenção Preventiva',
																text: 'Manutenção Preventiva Antecipada',
																type: 'warning',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-fast-forward"></i> MP Antecipada
                  </a>
                  <a class="btn btn-app"  href="maintenance-routine-reschedule?routine=<?php printf($rotina); ?>"  onclick="new PNotify({
                               title: 'Manutenção Preventiva',
                               text: 'Manutenção Preventiva Reprogramada',
                               type: 'warning',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-clock-o"></i> MP Reprogramada
                 </a>
                 <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/maintenance-routine-trash-backend.php?id=<?php printf($rotina); ?>';
  }
})
">
                <i class="fa fa-trash"></i> Excluir</a>
              
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
