<?php
include("database/database.php");

?>


    <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>     

   <div class="right_col" role="main">
       
          <div class="">
              
            <div class="page-title">
              <div class="title_left">
                <h3>Familia Equipamento</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                   <a class="btn btn-app"  href="register-equipament-family">
                    <i class="glyphicon glyphicon-list-alt"></i> Familias
                  </a>
                    <a class="btn btn-app"  href="register-equipament-family-register">
                    <i class="glyphicon glyphicon-plus"></i> Cadastro
                  </a>
                  <a class="btn btn-app"  href="backend/register-equipament-family-pop.php">
                    <i class="glyphicon glyphicon-refresh"></i> Atualização POP
                  </a>

                 


                </div>
              </div>
       
         <div class="x_panel">
                <div class="x_title">
                  <h2>Cadastro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                 <form  class="dropzone" action="backend/equipament-group-upload-backend.php?anexo=<?php printf($anexo);?>" method="post">    </form >   
          <form  action="backend/register-equipament-family-backend.php" method="post">
     	<div class="ln_solid"></div>
                           
       
								  
                                   <div class="form-group ">
                                        <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="grupo">Grupo <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6">
      
										<select type="text" class="form-control has-feedback-right" name="equipamento_grupo" id="equipamento_grupo"  placeholder="equipamento_grupo">
										  <option value="	">Selecione o Grupo de  Equipamento</option>
										  	<?php
										  $sql = "SELECT  id, nome FROM equipamento_grupo WHERE trash = 1";
                                          if ($stmt = $conn->prepare($sql)) {
		                                  $stmt->execute();
                                          $stmt->bind_result($id,$equipamento_grupo);
                                          while ($stmt->fetch()) {
                                                ?>
                                            <option value="<?php printf($id);?>	"><?php printf($equipamento_grupo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}	
                                            }
											$stmt->close();
											?>
	                                     	</select>  
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>
										
										
								      	</div>	
								      	
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento_grupo').select2();
                                      });
                                 </script>
                             </div>
                            <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome_familia">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6">
                              <input type="text" id="nome_familia" name="nome_familia" class="form-control">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="fabricante_familia">Fabricante <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6">
                              <input type="text" id="fabricante_familia" name="fabricante_familia" class="form-control">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="modelo_familia">Modelo <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6">
                              <input type="text" id="modelo_familia" name="modelo_familia" class="form-control">
                            </div>
                          </div>
            <div class="form-group row">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="anvisa_familia">Anvisa <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6">
                              <input type="text" id="anvisa_familia" name="anvisa_familia" class="form-control">
                            </div>
                          </div>
                   
                        
        
     

      <div class="compose-footer">
       	<input type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" value="Salvar"/>
      </div>
      
      </form>
     
    
                 
                  
                </div>
              </div>
       
        
            
             <!-- page content -->
       
           

            

          
            
            
            
            
            
                      
                  
                    
                   
                  </div>
                </div>
            
         
            
            
           
 <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  
                  <a class="btn btn-app"  href="register-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  
                 
                  
                </div>
              </div> 
            

            
       
        <!-- /page content -->
       
    <!-- /compose -->
    

 <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
  