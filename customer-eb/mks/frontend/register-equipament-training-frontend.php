  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");
$query = "SELECT equipamento_familia.anvisa,equipamento.baixa,equipamento.ativo,equipamento.inventario,equipamento.comodato,equipamento.duplicidade,equipamento.preventive,equipamento.obsolescence,equipamento.rfid,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento.serie,equipamento.patrimonio,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao WHERE equipamento.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($anvisa,$baixa,$ativo,$inventario,$comodato,$duplicidade,$preventive,$obsolecencia,$rfid,$data_val,$nf,$data_fab,$data_instal,$serie,$patrimonio,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }


}
?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Treinamento <small>Equipamento</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Dados <small>Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($nome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Modelo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text"  value="<?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Fabricante <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($fabricante); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
              
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"readonly="readonly"  required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="patrimonio">Patromonio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="patrimonio" name="patrimonio" value="<?php printf($patrimonio); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="serie">N/S <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="serie" name="serie" value="<?php printf($serie); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_instal" name="data_instal" value="<?php printf($data_instal); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_fab">Fabricação  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_fab" name="data_fab" value="<?php printf($data_fab); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_val">Garantia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_val" name="data_val" value="<?php printf($data_val); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">Observação <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="obs" name="obs" value="<?php printf($obs); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>



                  </div>
                </div>
              </div>
	       </div>


  <div class="clearfix"></div>


        <!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
 <!-- Posicionamento -->

	         <div class="x_panel">
                <div class="x_title">
                  <h2>Anexos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2" ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo -->
       <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM equipament_training WHERE id_equipamento like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                          <th>Titulo</th>
                          <th>Registro</th>
                          <th>Atualização</th>
                         <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id); ?></td>
                          <td><?php printf($titulo); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td><?php printf($upgrade); ?></td>
                         <td> <a class="btn btn-app"   href="dropzone/equipament-training/<?php printf($file); ?> " target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Anexo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Anexo
                  </a>
                  <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/equipamento-dropzone-training-trash.php?id=<?php printf($id); ?>&equipamento=<?php printf($codigoget); ?> ';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir
                  </a>
                </td>

                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>




                </div>
              </div>

              <!-- Posicionamento -->

	          <!-- Registro -->
               <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>

                        <div class="modal-body">

                        <!-- Registro forms-->
                          <form action="backend/equipament-training-dropzone-backend.php?id_equipamento=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>


                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                          class="required">*</span></label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='date' type="text" name="titulo" required='required'></div>
                    </div>




                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary">Salvar Informações</button>

                        </div>
                 </div>
             </form>
              <form  class="dropzone" action="backend/equipament-training-dropzone-upload-backend.php" method="post">
    </form >
     </div>
                    </div>
                  </div>
              <!-- Registro -->


            <div class="clearfix"></div>




              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
