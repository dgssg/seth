  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Gerenciamento de <small>material</small></h3>
              </div>


            </div>
       <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                 <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

                  <!-- Menu -->
                  <a class="btn btn-app <?php echo $current_page == 'purchases-inventory-manager-categoria' ? 'active' : ''; ?>" href="purchases-inventory-manager-categoria">
                    <i class="fa fa-tags"></i> Categoria <!-- Ícone de tags representa bem categorias -->
                  </a>
                  
                  <a class="btn btn-app <?php echo $current_page == 'purchases-inventory-manager-grupo' ? 'active' : ''; ?>" href="purchases-inventory-manager-grupo">
                    <i class="fa fa-object-group"></i> Grupo <!-- Ícone de grupos/objetos empilhados -->
                  </a>
                  
                  <a class="btn btn-app <?php echo $current_page == 'purchases-inventory-manager-familia' ? 'active' : ''; ?>" href="purchases-inventory-manager-familia">
                    <i class="fa fa-archive"></i> Familia <!-- Ícone de arquivo/caixa representa bem estoque -->
                  </a>
                  
                  <a class="btn btn-app <?php echo $current_page == 'purchases-inventory-manager' ? 'active' : ''; ?>" href="purchases-inventory-manager">
                    <i class="fa fa-cubes"></i> Material <!-- Ícone de cubos ou caixas empilhadas para materiais -->
                  </a>
                  



                </div>
              </div>






                <div class="clearfix"></div>

               

              <div class="x_panel">
               <div class="x_title">
                 <h2>Grupo de Material </h2>
                 <ul class="nav navbar-right panel_toolbox">
                        <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg2"  ><i class="fa fa-plus"></i></a>
                   <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                   </li>
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                         class="fa fa-wrench"></i></a>
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Settings 1</a>
                       </li>
                       <li><a href="#">Settings 2</a>
                       </li>
                     </ul>
                   </li>
                   <li><a class="close-link"><i class="fa fa-close"></i></a>
                   </li>
                 </ul>
                 <div class="clearfix"></div>
               </div>
               <div class="x_content">

               <?php include 'frontend/purchases-inventory-class-list-frontend.php';?>

               </div>
             </div>

             




                </div>
              </div>
                          