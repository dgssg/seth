<?php
include("database/database.php");
include("database/database_cal.php");
$codigoget = ($_GET["hfmea"]);
$id = ($_GET["id"]);
$query = "SELECT documentation_suport.id_unidade,documentation_suport.id_setor,documentation_suport.id_colaborador,colaborador.primeironome,colaborador.ultimonome,instituicao_area.nome,documentation_suport.class, equipamento_grupo.nome, documentation_suport.id, documentation_suport.id_equipamento_grupo, documentation_suport.titulo, documentation_suport.ver, documentation_suport.file, documentation_suport.data_now, documentation_suport.data_before, documentation_suport.upgrade, documentation_suport.reg_date FROM documentation_suport  LEFT JOIN equipamento_grupo ON equipamento_grupo.id = documentation_suport.id_equipamento_grupo LEFT JOIN instituicao_area ON instituicao_area.id = documentation_suport.id_setor LEFT JOIN colaborador ON colaborador.id = documentation_suport.id_colaborador WHERE documentation_suport.id = $codigoget";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id_unidade,$id_setor,$id_colaborador,$primeironome,$ultimonome,$instituicao_area,$class,$nome,$id_hfmea, $id_equipamento_grupo,$titulo,$ver,$file,$data_now,$data_before,$upgrade,$reg_date);


   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
  $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
  $con = 1;
  $var = '';
  while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
  }else{
      $var = $var.$n.$res[$n];
      $con++;
      }
  }
      $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
 // echo chaveAlfaNumerica(5);
//   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = md5($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 3
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function v4() {
return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

// 32 bits for "time_low"
mt_rand(0, 0xffff), mt_rand(0, 0xffff),

// 16 bits for "time_mid"
mt_rand(0, 0xffff),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand(0, 0x0fff) | 0x4000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand(0, 0x3fff) | 0x8000,

// 48 bits for "node"
mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
);
}

public static function v5($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = sha1($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function is_valid($uuid) {
return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();

//echo $v4uuid; //chave da assinatura
?>
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>Contingência</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Contingência</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

               

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="suport" class="form-control" name="suport" type="hidden" value="<?php printf($codigoget); ?> " readonly="readonly">
                        </div>
                      </div>

                   
                   <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"  id="titulo" name="titulo"  value="<?php printf($titulo); ?>" readonly="readonly">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Versão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="ver" class="form-control" type="text" name="ver"  value="<?php printf($ver); ?>" readonly="readonly">
                        </div>
                      </div>

                    
                      <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="id_unidade" id="id_unidade"  placeholder="Setor" disabled>
        <option value="">Selecione uma Unidade</option>
        <?php



$sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";


if ($stmt = $conn->prepare($sql)) {
$stmt->execute();
$stmt->bind_result($id,$instituicao);
while ($stmt->fetch()) {
?>

<option value="<?php printf($id);?>	" <?php if($id == $id_unidade){ printf("selected");} ?> ><?php printf($instituicao);?>	</option>
 <?php
// tira o resultado da busca da memória
}

                     }
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Unidade "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#instituicao').select2();
  });
  </script>
                      <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Setor" disabled>
        <option value="">Selecione um Setor</option>
        <?php



$sql = "SELECT  id, codigo,nome FROM instituicao_area WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
  $stmt->bind_result($id,$codigo,$nome);
  while ($stmt->fetch()) {
    ?>
    <option value="<?php printf($id);?>	" <?php if($id == $id_setor){printf("selected");};?>><?php printf($codigo);?> <?php printf($nome);?>	</option>
    <?php
    // tira o resultado da busca da mem��ria
  }

}
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#area').select2();
  });
  </script>
<div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Responsável 
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Responsável" disabled>
        <option value="	">Selecione o Responsável</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	" <?php if($id == $id_colaborador){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_colaborador').select2();
  });
  </script>
  
  

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_now"  type="date" name="data_now"  value="<?php printf($data_now); ?>" class="form-control"readonly="readonly">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da Proxima Revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_before"  type="date" name="data_before"  value="<?php printf($data_before); ?>" class="form-control"readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                       
                         </center>
                        </div>
                      </div>


                               


                  </div>
                </div>
              </div>
	       </div>


            <div class="clearfix"></div>



          <div class="x_panel">
                <div class="x_title">
                  <h2>Contingências</h2>
                  <ul class="nav navbar-right panel_toolbox">
                   
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo-->
       <?php
          $query="SELECT documentation_suport_itens_action.id,
          documentation_suport_itens_action.obs,
          documentation_suport_itens_action.id_mod,
          GROUP_CONCAT(DISTINCT instituicao_area.nome) AS areas,
          GROUP_CONCAT(DISTINCT fornecedor.empresa) AS empresas,
          GROUP_CONCAT(DISTINCT fornecedor.contato) AS contatos,
          GROUP_CONCAT(DISTINCT fornecedor.telefone_1) AS telefone_1,
          GROUP_CONCAT(DISTINCT fornecedor.telefone_2) AS telefone_2
          FROM documentation_suport_itens_action
          LEFT JOIN instituicao_area ON FIND_IN_SET(instituicao_area.id, documentation_suport_itens_action.id_setor)
          LEFT JOIN fornecedor ON FIND_IN_SET(fornecedor.id, documentation_suport_itens_action.id_fornecedor)
          WHERE documentation_suport_itens_action.id =  $id group by documentation_suport_itens_action.id;
";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$obs_action,$action,$setor_action,$id_fornecedor_action,$contato_action,$telefone1_action,$telefone2_action);
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}
      
$query="SELECT
          documentation_suport_itens.id_grupo,
GROUP_CONCAT(DISTINCT instituicao.instituicao ORDER BY instituicao.instituicao ASC) AS unidade,

GROUP_CONCAT(DISTINCT instituicao_area.id ORDER BY instituicao_area.id ASC) AS areas,
COUNT(equipamento.id),
documentation_suport_itens.id,
equipamento_grupo.nome,
fornecedor.empresa,
fornecedor.contato,
fornecedor.telefone_1,
fornecedor.telefone_2,
documentation_suport_itens.obs
FROM documentation_suport_itens
LEFT JOIN equipamento_grupo ON equipamento_grupo.id = documentation_suport_itens.id_grupo
LEFT JOIN fornecedor ON fornecedor.id = documentation_suport_itens.id_fornecedor
LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id
LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id
LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
LEFT JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
LEFT JOIN instituicao ON instituicao.id = instituicao_localizacao.id_unidade
WHERE documentation_suport_itens.id = $codigoget
GROUP BY equipamento_grupo.id
ORDER BY documentation_suport_itens.id ASC;
";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_grupo,$unidade,$setor,$count,$id_cont,$id_grupo,$id_fornecedor,$contato,$telefone1,$telefone2,$obs);
  while ($stmt->fetch()) {
    //printf("%s, %s\n", $solicitante, $equipamento);
  }
}
        ?>                        <form action="backend/documentation-suport-insert-iten-itens-edit-backend.php?id=<?php printf($id);?>" method="post">
          <div class="ln_solid"></div>
          
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
            <div class="col-md-6 col-sm-6 ">
              <input id="suport" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
            <div class="col-md-6 col-sm-6 ">
              <input id="suport" class="form-control" name="suport" type="hidden" value="<?php printf($codigoget); ?> " >
            </div>
          </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Modo <span
              class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <select type="text" class="form-control has-feedback-left" name="id_acao"
                id="id_acao" style="width:350px;" placeholder="Unidade" required="required">
                <option> Selecione uma Modo</option>
                <option value="1"> Interno entre setores do grupo</option>
                
                <option  value="2"> Externo com Fornecedor</option>
                
                
              </select>
              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
              
            </div>
          </div>
          
          <script>
            $(document).ready(function() {
              $('#id_acao<?php printf($id_cont); ?>').select2()
              });
            });
          </script>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Locação <span
              class="required"></span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <select multiple="multiple"  type="text" class="form-control has-feedback-left" name="id_fornecedor[]"
                id="id_fornecedor" style="width:350px;" placeholder="Unidade" >
                <option> Selecione uma Fornecedor</option>
                <?php
                  
                  $result_cat_post  = "SELECT  id, empresa FROM fornecedor WHERE trash = 1";
                  
                  $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                  while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                    echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['empresa'].'</option>';
                  }
                ?>
                
              </select>
              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
              
            </div>
          </div>
          
          <script>
            $(document).ready(function() {
              $('#id_fornecedor').select2()
              });
            });
          </script>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
              class="required"></span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <select  multiple="multiple" type="text" class="form-control has-feedback-left" name="id_setor[]"
                id="id_setor" style="width:350px;" placeholder="Unidade" >
                <option> Selecione um Setor</option>
                <?php
                  
					$result_cat_post = "SELECT id, nome FROM instituicao_area WHERE id IN ($setor)";
                  
                  $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                  while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                    echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                  }
                ?>
                
              </select>
              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
              
            </div>
          </div>
          
          <script>
            $(document).ready(function() {
              $('#id_setor').select2();
            });
          </script>
          
          
          
          <label for="risco">Ação:</label>
          <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
            data-parsley-validation-threshold="10"><?php printf($obs_action);?></textarea> 
          
          
          
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary" onclick="new PNotify({
              title: 'Registrado',
              text: 'Informações registrada!',
              type: 'success',
              styling: 'bootstrap3'
            });">Salvar Informações</button>
          </div>
          
                </div>
          </div>
           <div class="clearfix"></div>
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="documentation-suport-edit?id=<?php
                    printf($codigoget);
                  ?>">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>

           