<?php
include("database/database.php");
$query = "SELECT unidade_disel.name,unidade_disel_supply.id,unidade_disel_supply.id_unidade_disel,unidade_disel_supply.id_year,unidade_disel_supply.id_month,unidade_disel_supply.nf,unidade_disel_supply.date_nf,unidade_disel_supply.read_start,unidade_disel_supply.read_end,unidade_disel_supply.qtd,unidade_disel_supply.vlr_t,unidade_disel_supply.vlr_un  FROM unidade_disel_supply LEFT JOIN unidade_disel ON unidade_disel.id =  unidade_disel_supply.id_unidade_disel ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($name,$id,$id_unidade_disel,$id_year,$id_mouth,$nf,$date_nf,$read_start,$read_end,$qtd,$vlr_t,$vlr_un);
  //while ($stmt->fetch()) {
  //printf("%s, %s\n", $solicitante, $equipamento);
  //  }


  ?>

  <div class="col-md-11 col-sm-11 ">
    <div class="x_panel">

      <div class="x_content">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box table-responsive">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Instalacao</th>
                    <th>Ano</th>
                    <th>Mes</th>
                    <th>Nota Fiscal	</th>
                    <th>Data NF	</th>
                    <th>Leitura Inicial	</th>
                    <th>Leitura Final	</th>
                    <th>Quantidade (L)	</th>
                    <th>Valor Unitário (R$)	</th>
                    <th>Valor Total (R$)</th>
                    <th>Ação</th>

                  </tr>
                </thead>


                <tbody>
                  <?php   while ($stmt->fetch()) {   ?>
                    <tr>
                      <td><?php printf($id); ?> </td>
                      <td><?php printf($name); ?> </td>
                      <td><?php printf($id_year); ?> </td>
  
                      <td><?php printf($id_mouth); ?> </td>

                      <td><?php printf($nf); ?> </td>
                      <td><?php printf($date_nf); ?> </td>
  
                      <td><?php printf($read_start); ?> </td>

                      <td><?php printf($read_end); ?> </td>
                      <td><?php printf($qtd); ?> </td>
  
                      <td><?php printf($vlr_t); ?> </td>
  
                      <td><?php printf($vlr_un); ?> </td>
  
                      <td>
                <!--        <a class="btn btn-app"  href="utilities-disel-supply-edit?id=<?php printf($id); ?>" onclick="new PNotify({
                          title: 'Editar',
                          text: 'Abrindo Edição',
                          type: 'sucess',
                          styling: 'bootstrap3'
                        });">
                        <i class="fa fa-edit"></i> Editar
                      </a> -->
                   
                    <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/utilities-disel-supply-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                 <i class="fa fa-trash"></i> Excluir</a>
                    <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
                    title: 'Download',
                    text: 'Download O.S',
                    type: 'info',
                    styling: 'bootstrap3'
                  });">
                  <i class="fa fa-download"></i> Download
                </a> -->
              </td>
            </tr>
          <?php   } }  ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
