<?php
include("database/database.php");
$query = "SELECT id,nome,sobrenome,matricula,email,telefone,reg_date FROM usuario   WHERE trash = 1";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome,$sobrenome,$matricula,$email,$telefone,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
 

?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Usuário</th>
                          <th>Matricula</th>
                          <th>Email</th>
                          
                          <th>Telefone</th>
                          
                          <th>Registro</th>
                         
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($nome); ?> <?php printf($sobrenome); ?> </td>
                          <td><?php printf($matricula); ?></td>
                            <td><?php printf($email); ?></td>
                              <td><?php printf($telefone); ?></td>
                                
                          <td><?php printf($reg_date); ?></td>
                      
                          <td>  
                   <a class="btn btn-app"  href="register-account-edit?account=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                  
                  <a class="btn btn-app"  href="backend/register-account-reset-backend.php?account=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Senha',
																text: 'Senha O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-refresh"></i> Resetar Senha
                  </a> 
                    <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-account-trash-backend?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
           