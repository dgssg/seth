 <?php

include("database/database.php");


//$con->close();


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Widgets <small></small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                                                 
  <style>
 .btn.btn-app {
  border: 2px solid transparent; /* Define a borda como transparente por padrão */
  padding: 5px;
  position: relative; /* Necessário para posicionar o pseudo-elemento */
}

.btn.btn-app.active {
  border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
}

.btn.btn-app.active::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0; /* Posição da linha no final do elemento */
  width: 100%;
  height: 3px; /* Espessura da linha */
  background-color: #ffcc00; /* Cor da linha */
}

  </style>
              <?php
$current_page = basename($_SERVER['REQUEST_URI'], ".php");
?>

            

                

              
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-pop' ? 'active' : ''; ?>" href="documentation-pop">

                   <i class="fa fa-book"></i> POP
                 </a>
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-update' ? 'active' : ''; ?>" href="documentation-update">

                   <i class="fa fa-history"></i> Atualização
                 </a>
              
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-hfmea' ? 'active' : ''; ?>" href="documentation-hfmea">

                   <i class="fa fa-map"></i> HFMEA
                 </a>
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-label' ? 'active' : ''; ?>" href="documentation-label">

                   <i class="fa fa-fax"></i> Rotuladora
                 </a>
               
                                   <a class="btn btn-app <?php echo $current_page == 'documentation-bussines' ? 'active' : ''; ?>" href="documentation-bussines">

                    <i class="fa fa-folder-open"></i> Documentação
                  </a>
                
                                  
               
                                    <a class="btn btn-app <?php echo $current_page == 'documentation-bell' ? 'active' : ''; ?>" href="documentation-bell">

                    <i class="fa fa-bell"></i> Central de Notificações
                  </a>
                
                                                
                                                 
                                                     
                                    




                </div>
              </div>

          
          
          <!-- Widgets -->
            <div class="x_panel">
              <div class="x_title">
                <h2>Widgets</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                
                <?php
                  
                  include 'widget/widget-01.php';
                  include 'widget/widget-02.php';
                  include 'widget/widget-03.php';
                  include 'widget/widget-04.php';

                  
            
                ?>
                
                <!-- PAGE CONTENT ENDS -->  

          
          

                </div>
              </div>


        <!-- /page content -->
            