<?php
include("database/database.php");
$query = "SELECT *FROM documentation_pop_goup  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id, $nome);


?>






                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Ação</th>
                          
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($nome); ?> </td>
                          <td>  
                 
                
                  <a class="btn btn-app"  href="documentation-pop-group-edit.php?pop=<?php printf($id); ?>"  onclick="new PNotify({
                               title: 'Editar',
                               text: 'Editar POP',
                               type: 'info',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-edit"></i> Editar
                 </a> 
             
                   <a class="btn btn-app"  onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/documentation-pop-group-trash.php?pop=<?php printf($id); ?>';
  }
})
">
                   <i class="fa fa-trash"></i> Excluir
                 </a>
            
               
               
                 </td>
                         
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">

                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Novo Grupo POP</h4>
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="backend/documentation-pop-group-backend.php" method="post">

                       

                      

                        <div class="item form-group">
                          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nome</label>
                          <div class="col-md-6 col-sm-6 ">
                            <input id="nome" class="form-control" type="text" name="nome"   >
                          </div>
                        </div>
                      
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});" >Salvar</button>
                    </div>
        </form>
                  </div>
                </div>
              </div>
