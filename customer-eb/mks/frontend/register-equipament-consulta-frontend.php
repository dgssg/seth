<?php
include("database/database.php");

?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">


                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                   

                        <th>#</th>
                        <th>Codigo</th>
                        <th>Equipamento</th>
                        <th>Modelo</th>
                        <th>Fabricante</th>
                        <th>N/S</th>
                        <th>Unidade</th>
                        
                        <th>Setor</th>
                        <th>Area</th>
                        <th>Baixa</th>
                        <th>Ativo</th>
                        <th>Comodato</th>
                       
                        <th>Ação</th>
                          <th>Documentos</th>
                          <th>Formulario</th>
                          <th>Recursos</th>
                        

                        </tr>
                      </thead>

                     
                      <tbody>
                      
                     
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
              <script language="JavaScript">
 let paginaAtual = 0;

 $(document).ready(function() {
  $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );

  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'table/table-search-equipamento.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
    .then(data => {
      const novosDados = data.map(item => {
      let badgeHTML = '';
      let badgeHTMLBaixa = '';
      let badgeHTMLcheckist= '';
      if (item.nf_dropzone != null) {
        badgeHTML = "<span class='badge badge-success'>OK</span>";
      }
      if (item.checklist != null) {
          badgeHTMLcheckist = "<span class='badge badge-success'>OK</span>";
        }
      if (item.baixa == 0) {
          badgeHTMLBaixa = "<span class='badge badge-success'>OK</span>";
        }
      return [
        ``,
  item.codigo,
  item.equipamento,
  item.modelo,
  item.fabricante,
  item.serie,
  item.instituicao,
  item.area,
 
  item.setor,
  item.baixa == "0" ? "Sim" : "Não",
  item.ativo == "0" ? "Sim" : "Não",
  item.comodato == "0" ? "Sim" : "Não",
 
  `<a class="btn btn-app btn-editar" href="register-equipament-edit?equipamento=${item.id}" onclick="new PNotify({
          title: 'Editar',
          text: 'Abrindo Edição',
          type: 'sucess',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-edit"></i> Editar
        </a>  <a class="btn btn-app"  href="register-equipament-tag?equipamento=${item.id}" target="_blank" onclick="new PNotify({
																title: 'Etiqueta',
																text: 'Abrir Etiqueta',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-qrcode"></i> Etiqueta
                  </a><a class="btn btn-app"  href="register-equipament-label?equipamento=${item.id}" target="_blank" onclick="new PNotify({
                               title: 'Etiqueta',
                               text: 'Abrir Etiqueta',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-fax"></i> Rótulo
                 </a>
                 <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/register-equipament-trash-backend?id=${item.id}';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>`,
                   // botão de edição para cada linha
                   ` <a class="btn btn-app"  href="register-equipament-record?equipamento=${item.id}" target="_blank" onclick="new PNotify({
																title: 'Ficha',
																text: 'Abrir Ficha',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Ficha
                  </a>
                   <a class="btn btn-app"  href="register-equipament-prontuario?equipamento=${item.id}" target="_blank" onclick="new PNotify({
																title: 'Prontuario',
																text: 'Abrir Prontuario',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-o"></i> Prontuario
                  </a>
                    <a class="btn btn-app"  href="register-equipament-disable?equipamento=${item.id}" onclick="new PNotify({
																title: 'Baixa',
																text: 'Baixa Equipamento',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-trash"></i> Baixa
${badgeHTMLBaixa}
                  </a>
                  <a class="btn btn-app" href="dropzone/nf/${item.nf_dropzone}" target="_blank" onclick="new PNotify({
      title: 'Prontuario',
      text: 'Abrir Prontuario',
      type: 'success',
      styling: 'bootstrap3'
    });">
      <i class="fa fa-file-o"></i> Nota Fiscal
  ${badgeHTML}
    </a>` 
                  ,
                  `<a class="btn btn-app"  href="register-equipament-check?equipamento=${item.id}" onclick="new PNotify({
																title: 'Check List',
																text: 'Abrir Check List',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> CheckList
${badgeHTMLcheckist}
                  </a>
                     <a class="btn btn-app"  href="register-equipament-training?equipamento=${item.id}" onclick="new PNotify({
																title: 'Treinamento',
																text: 'Abrir Treinamento',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> Treinamento
                  </a>
                     <a class="btn btn-app"  href="register-equipament-report?equipamento=${item.id}" onclick="new PNotify({
																title: 'Parecer',
																text: 'Abrir Parecer',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> Parecer
                  </a>
                     <a class="btn btn-app"  href="register-equipament-exit?equipamento=${item.id}" onclick="new PNotify({
																title: 'Saida',
																text: 'Abrir Saida',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i> Saida
                  </a>
                     <a class="btn btn-app"  href="register-equipament-compliance?equipamento=${item.id}" onclick="new PNotify({
																title: 'Conformidade',
																text: 'Abrir Conformidade',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-text-o "></i>Conformidade
                  </a>`,
                  ` <a class="btn btn-app"  href="register-equipament-timeline?equipamento=${item.id}" onclick="new PNotify({
																title: 'Timeline',
																text: 'Abrir Timeline',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-hourglass"></i>Linha do Tempo
                  </a>
                  <a class="btn btn-app"  href="register-equipament-location?equipamento=${item.id}" onclick="new PNotify({
																title: 'Localização',
																text: 'Abrir Localização',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-cogs"></i>Localização
                  </a>`
      ];

      });

    // Adiciona as novas linhas ao DataTables e desenha a tabela
    $('#datatable').DataTable().rows.add(novosDados).draw();

 // Retorna à página atual após a exclusão

  $('#datatable').DataTable().page(paginaAtual).draw('page');

// Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([1,2,3,4,5,6,7,8,9,10,11]).every(function (d) {
        var column = this;
        var theadname = $("#datatable th").eq([d]).text();
        var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
        var label = $('<label>'+theadname+': </label>').appendTo(container); 
        var select = $('<select  class="form-control my-1"><option value="">' +
          theadname +'</option></select>').appendTo('#userstable_filter').select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });

        column.data().unique().sort().each(function (d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
      });
    });
});




</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#btn-carregar').click(function() {
    $.ajax({
      url: 'loading/load_equipamento.php', // substitua pelo nome do seu arquivo PHP que busca os itens restantes da tabela
      type: 'POST', // ou 'GET', dependendo da sua implementação
      data: {
        offset: 10 // substitua pelo número de itens que você já carregou
      },
      success: function(data) {
        $('#datatable').append(data); // adicione os novos itens à tabela
      }
    });
  });
});

</script>

