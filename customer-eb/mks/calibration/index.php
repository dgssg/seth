<?php





namespace Phppot;

use Phppot\Model\FAQ;


?>
<html>
<head>
<title>PHP MySQL Inline Editing using jQuery Ajax</title>
<link href="./assets/CSS/style.css" type="text/css" rel="stylesheet" />
<script src="./vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="./assets/js/inlineEdit.js"></script>
</head>
<body>
    <table class="tbl-qa">
        <thead>
            <tr>
                <th class="table-header" width="10%">Q.No.</th>
                <th class="table-header">Laudo</th>
                <th class="table-header">VE</th>
                <th class="table-header">VL-1</th>
                <th class="table-header">VL-2</th>
                <th class="table-header">VL-3</th>
                <th class="table-header">VL-AVG</th>
                <th class="table-header">Erro</th>
                <th class="table-header">DP</th>
                <th class="table-header">IA</th>
                <th class="table-header">IB</th>
                <th class="table-header">IC</th>
                <th class="table-header">K</th>
                <th class="table-header">IE</th>
                <th class="table-header">Status</th>
            </tr>
        </thead>
        <tbody>
<?php
require_once ("Model/FAQ.php");
$faq = new FAQ();
$faqResult = $faq->getFAQ();

foreach ($faqResult as $k => $v) {
    ?>
			  <tr class="table-row">
                <td><?php echo $k+1; ?></td>
                <td contenteditable="true"
                    onBlur="saveToDatabase(this,'id_calibration','<?php echo $faqResult[$k]["id"]; ?>')"
                    onClick="showEdit(this);" ><?php echo $faqResult[$k]["id_calibration"]; ?></td>
                    <td bgcolor="#FFF00" contenteditable="true"
                        onBlur="saveToDatabase(this,'ve','<?php echo $faqResult[$k]["id"]; ?>')"
                        onClick="showEdit(this);" bgcolor="#FFF00" ><?php echo $faqResult[$k]["ve"]; ?></td>
                <td bgcolor="#FFF00" contenteditable="true"
                    onBlur="saveToDatabase(this,'vl_1','<?php echo $faqResult[$k]["id"]; ?>')"
                    onClick="showEdit(this);" bgcolor="#FFF00" ><?php echo $faqResult[$k]["vl_1"]; ?></td>
                <td bgcolor="#FFF00" contenteditable="true"
                        onBlur="saveToDatabase(this,'vl_2','<?php echo $faqResult[$k]["id"]; ?>')"
                        onClick="showEdit(this);" bgcolor="#FFF00" ><?php echo $faqResult[$k]["vl_2"]; ?></td>
                <td bgcolor="#FFF00" contenteditable="true"
                            onBlur="saveToDatabase(this,'vl_3','<?php echo $faqResult[$k]["id"]; ?>')"
                            onClick="showEdit(this);" bgcolor="#FFF00" ><?php echo $faqResult[$k]["vl_3"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'vl_avg','<?php echo $faqResult[$k]["id"]; ?>')"onClick="showEdit(this);"><?php echo $faqResult[$k]["vl_avg"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'erro','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["erro"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'dp','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["dp"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'ia','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ia"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'ib','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ib"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'ic','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ic"]; ?></td>
<td bgcolor="#FFF00" contenteditable="true" onBlur="saveToDatabase(this,'k','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);" bgcolor="#FFF00" ><?php echo $faqResult[$k]["k"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'ie','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["ie"]; ?></td>
<td contenteditable="false" onBlur="saveToDatabase(this,'status','<?php echo $faqResult[$k]["id"]; ?>')" onClick="showEdit(this);"><?php echo $faqResult[$k]["status"]; ?></td>








            </tr>
		<?php
}
?>
		  </tbody>
    </table>
</body>
</html>
