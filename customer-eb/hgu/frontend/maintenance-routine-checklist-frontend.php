<?php
$codigoget = ($_GET["routine"]);

// Create connection

include("database/database.php");
$query = "SELECT equipamento.id,maintenance_routine.periodicidade_after,maintenance_routine.data_after,maintenance_routine.time_after,maintenance_routine.id_pop,maintenance_routine.id_maintenance_procedures_after,maintenance_routine.id_maintenance_procedures_before,maintenance_routine.id_fornecedor,maintenance_routine.id_colaborador,maintenance_routine.id_category,maintenance_routine.habilitado,maintenance_routine.time_ms,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id where maintenance_routine.id like'$codigoget'";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($equipamento,$periodicidade_after,$data_after,$time_after,$id_pop,$id_maintenance_procedures_after,$id_maintenance_procedures_before,$id_fornecedor,$id_colaborador,$id_category,$habilitado,$time_ms,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
 while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
//  }
}
}

?>

<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>  Checklist <small>Manutenção Preventiva</small></h3>
            </div>


          </div>

          <div class="clearfix"></div>

          <div class="row" style="display: block;">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Cabeçalho <small>Procedimento</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <?php
                    
                   
             
                    $query = "SELECT instituicao.logo,instituicao.instituicao, instituicao_area.nome,equipamento_familia.img,equipamento.obs,equipamento.data_instal,equipamento.data_fab,equipamento.patrimonio,equipamento.serie,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome from equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade where equipamento.id like '$equipamento'";
                    
                    //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
                    if ($stmt = $conn->prepare($query)) {
                      $stmt->execute();
                      $stmt->bind_result($logo,$instituicao,$area,$img,$obs,$data_instal,$data_fab,$patrimonio,$serie,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
                      while ($stmt->fetch()) {
                        //printf("%s, %s\n", $solicitante, $equipamento);
                      }
                    }
                    
                  ?>
                                
<form action="backend/maintenance-routine-checklist-backend.php?routine=<?php printf($codigoget);?>" 
  method="post">

<div class="ln_solid"></div>
              <div class="item form-group">
                      <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="routine" name="routine" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                      </div>
                    </div>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Tempo <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <input type="text" id="time_mp" name="time_mp" value="<?php printf($time_ms); ?>" readonly="readonly" required="required" class="form-control ">
    </div>
  </div>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
    </div>
  </div>



<div class="ln_solid"></div>
                               <small>Data da abertura da Manutenção de instalação</small>



                               <div class="item form-group">
                                 <label class="col-form-label col-md-3 col-sm-3 label-align" for="date_start">Data Programada<span ></span>
                                 </label>
                                 <div class="col-md-6 col-sm-6 ">
                                   <input type="text" id="date_start" name="date_start" required="required"   value="<?php printf($data_instal); ?>" class="form-control" readonly="readonly">
                                 </div>
                               </div>




                                          <div class="ln_solid"></div>
  <div class="ln_solid"></div>
  <small>Data inicial da Manutenção de instalação</small>
  
  
  
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="date_start">Data Programada<span ></span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <input type="text" id="date_mp_start" name="date_mp_start" required="required"  value="<?php printf($data_instal); ?>" class="form-control" readonly="readonly" >
    </div>
  </div>
  
  
  
  

  <div class="ln_solid"></div>
  <small>Data final da Manutenção de instalação</small>
  
  
  
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="date_start">Data Programada<span ></span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <input type="text" id="date_end_start" name="date_end_start" required="required" value="<?php printf($data_instal); ?>" class="form-control" readonly="readonly">
    </div>
  </div>
  
  
  
  
  <div class="ln_solid"></div>



  <?php
    $query="SELECT id, file, titulo FROM equipament_check WHERE id_equipamento like '$equipamento'";
    $row=1;
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($id,$file,$titulo);
      while ($stmt->fetch()) {
      }
    }
  ?>
 
<div class="ln_solid"></div>
 
        <?php if($file ==! ""){ ?>
        <div class="thumbnail">
          <div class="image view view-first">
            <img style="width: 100%; display: block;"
              src="dropzone/equipament-check/<?php printf($file); ?>" alt="image" />
            <div class="mask">
              <p>Anexo</p>
              <div class="tools tools-bottom">
                <a href="dropzone/equipament-check/<?php printf($file); ?>" download><i
                  class="fa fa-download"></i></a>
                
              </div>
            </div>
          </div>
          <div class="caption">
            <p>Anexo Instalação</p>
          </div>
        </div>
        <?php } ?>
     
      
      
      <input id="file" class="form-control" type="hidden" name="file"  value="<?php printf($file); ?>" readonly="readonly">
      
      
   
  <?php
    $query="SELECT regdate_equipament_check_install.id_equipamento_check_install,regdate_equipament_check_install.id_user,regdate_equipament_check_install.id_signature,regdate_equipament_check_install.reg_date,usuario.nome FROM regdate_equipament_check_install INNER JOIN usuario ON regdate_equipament_check_install.id_user = usuario.id   WHERE regdate_equipament_check_install.id_equipamento_check_install like '$id'";
    $row=1;
    if ($stmt = $conn->prepare($query)) {
      $stmt->execute();
      $stmt->bind_result($id_equipamento_check_install,$id_user,$id_signature,$reg_date_chek,$id_user_name);
      while ($stmt->fetch()) {
      }
    }
  ?>
  <div class="ln_solid"></div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Assinatura</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="signature_admin" class="form-control" type="text" name="signature_admin"  value="<?php printf($id_signature); ?>" readonly="readonly">
      <input id="id_user" class="form-control" type="hidden" name="id_user"  value="<?php printf($id_user); ?>" readonly="readonly">
      <input id="reg_date" class="form-control" type="hidden" name="reg_date"  value="<?php printf($reg_date_chek); ?>" readonly="readonly">
      <input id="id_fornecedor" class="form-control" type="hidden" name="id_fornecedor"  value="<?php printf($id_fornecedor); ?>" readonly="readonly">
      <input id="id_colaborador" class="form-control" type="hidden" name="id_colaborador"  value="<?php printf($id_colaborador); ?>" readonly="readonly">
    </div>
  </div>
                     <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                      </div>
                    </div>
                     <div class="item form-group">
                      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                      <div class="col-md-6 col-sm-6 ">
                        <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                      </div>
                    </div>
<div class="ln_solid"></div>
           <div class="form-group row">
                     <label class="col-form-label col-md-3 col-sm-3 "></label>
                      <div class="col-md-3 col-sm-3 ">
                          <center>
                         <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                       </center>
                      </div>
                    </div>
</form>
                </div>
              </div>
            </div>
       </div>





            <!-- Posicionamento -->



           <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <a class="btn btn-app"  href="maintenance-routine">
                  <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                </a>



            <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
                              title: 'Cancelamento',
                              text: 'Cancelamento de Abertura de O.S!',
                              type: 'error',
                              styling: 'bootstrap3'
                          });">
                  <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                </a>
                  <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
                              title: 'Visualizar',
                              text: 'Visualizar O.S!',
                              type: 'info',
                              styling: 'bootstrap3'
                          });" >
                  <i class="fa fa-file-pdf-o"></i> Visualizar
                </a> -->

              </div>
            </div>




              </div>
            </div>
