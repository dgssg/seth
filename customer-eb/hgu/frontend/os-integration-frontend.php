  <?php
$codigoget = ($_GET["equipamento"]);
      $sweet_salve = ($_GET["sweet_salve"]);
      $sweet_delet = ($_GET["sweet_delet"]);
// Create connection

include("database/database.php");
      if ($sweet_delet == 1) {
         // Redirecionamento para a âncora "registro" usando JavaScript
         echo '<script>
      Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi deletado!\',
            showConfirmButton: false,
            timer: 1500
      });
</script>';
         
         
      }
      if ($sweet_salve == 1) {
         // Redirecionamento para a âncora "registro" usando JavaScript
         echo '<script>
            Swal.fire({
                  position: \'top-end\',
                  icon: \'success\',
                  title: \'Seu trabalho foi salvo!\',
                  showConfirmButton: false,
                  timer: 1500
            });
      </script>';
         
         
      }
      
?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                  <h3>Integração de Ordem de Serviço</h3>

              </div>


            </div>
   
            <div class="clearfix"></div>
            <div class="x_panel">
               <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                           class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                           <li><a href="#">Settings 1</a>
                           </li>
                           <li><a href="#">Settings 2</a>
                           </li>
                        </ul>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a>
                     </li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  
                  <style>
                     .btn.btn-app {
                        border: 2px solid transparent; /* Define a borda como transparente por padrão */
                        padding: 5px;
                        position: relative; /* Necessário para posicionar o pseudo-elemento */
                     }
                     
                     .btn.btn-app.active {
                        border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
                     }
                     
                     .btn.btn-app.active::after {
                        content: "";
                        position: absolute;
                        left: 0;
                        bottom: 0; /* Posição da linha no final do elemento */
                        width: 100%;
                        height: 3px; /* Espessura da linha */
                        background-color: #ffcc00; /* Cor da linha */
                     }
                     
                  </style>
                  <?php
                     $current_page = basename($_SERVER['REQUEST_URI'], ".php");
                  ?>
                  
                  <!-- Menu -->
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-open' ? 'active' : ''; ?>" href="os-open">
                     <i class="fa fa-folder-open"></i> O.S Abertura
                  </a>
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-opened' ? 'active' : ''; ?>" href="os-opened">
                     
                     <i class="fa fa-play"></i>O.S Aberto 
                  </a>
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-progress' ? 'active' : ''; ?>" href="os-progress">
                     <i class="fa fa-cogs"></i>O.S Processo
                  </a>
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-signature' ? 'active' : ''; ?>" href="os-signature">
                     
                     <i class="fa fa-pencil"></i>O.S Assinatura
                  </a>
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-signature-user' ? 'active' : ''; ?>" href="os-signature-user">
                     
                     <i class="fa fa-user"></i>O.S Solicitante
                  </a>
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-finished' ? 'active' : ''; ?>" href="os-finished">
                     
                     <i class="fa fa-check-circle"></i>O.S Concluído
                  </a>
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-canceled' ? 'active' : ''; ?>" href="os-canceled">
                     
                     <i class="fa fa-times-circle"></i>O.S Cancelada
                  </a>
                  
                  
                  <a class="btn btn-app <?php echo $current_page == 'os-integration' ? 'active' : ''; ?>" href="os-integration">
                     
                     <i class="fa fa-code"></i>O.S Integração
                  </a>
                  
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
               <div class="col-md-12 col-sm-12 ">
                  <div class="x_panel">
                     <div class="x_title">
                        <h2>Filtro</h2>
                        <ul class="nav navbar-right panel_toolbox">
                           <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                           </li>
                           <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                 aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <ul class="dropdown-menu" role="menu">
                                 <li><a class="dropdown-item" href="#">Settings 1</a>
                                 </li>
                                 <li><a class="dropdown-item" href="#">Settings 2</a>
                                 </li>
                              </ul>
                           </li>
                           <li><a class="close-link"><i class="fa fa-close"></i></a>
                           </li>
                        </ul>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                        <div id="userstable_filter"
                           style="column-count:8; -moz-column-count:8; -webkit-column-count:8; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;">
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>


                <div class="x_panel">
                       <div class="x_title">
                         <h2>Integração</h2>
                         <ul class="nav navbar-right panel_toolbox">
                           <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                           </li>
                           <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                 class="fa fa-wrench"></i></a>
                             <ul class="dropdown-menu" role="menu">
                               <li><a href="#">Settings 1</a>
                               </li>
                               <li><a href="#">Settings 2</a>
                               </li>
                             </ul>
                           </li>
                           <li><a class="close-link"><i class="fa fa-close"></i></a>
                           </li>
                         </ul>
                         <div class="clearfix"></div>
                       </div>
                       <div class="x_content">



                 <!-- PAGE CONTENT BEGINS -->
                           <div class="col-md-12 col-sm-12 ">
                              <div class="x_panel">
                                 
                                 <div class="x_content">
                                    <div class="row">
                                       <div class="col-sm-12">
                                          <div class="card-box table-responsive">
                                             
                                             <table id="datatable"
                                                class="table table-striped table-bordered dt-responsive nowrap"
                                                style="width:100%">
                                                <thead>
                                                   
                                                   <tr>
                                                      <th>#</th>
                                                      <th>Integração</th>
                                                      <th>OS</th>
                                                      <th>Solicitação</th>
                                                       
                                                      <th>Ação</th>
                                                      
                                                   </tr>
                                                </thead>
                                                
                                                
                                                <tbody>
                                                </tbody>
                                             </table> 
                                             </div>
                                          </div>
                                                
                                 </div>
                              </div>
                        </div>
                  </div>
      

                                                
					<!-- PAGE CONTENT ENDS -->

                        </div>
               </div>


            


                    <!-- page content -->


















               <!-- /page content -->

           <!-- /compose -->


        <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
                                                <script type="text/javascript">
                                                   
                                                   
                                                   
                                                   $(document).ready(function() {
                                                      
                                                      $('#datatable').dataTable( {
                                                         "processing": true,
                                                         "stateSave": true,
                                                         responsive: true,
                                                         
                                                         
                                                         
                                                         "language": {
                                                            "loadingRecords": "Carregando dados...",
                                                            "processing": "Processando  dados...",
                                                            "infoEmpty": "Nenhum dado a mostrar",
                                                            "emptyTable": "Sem dados disponíveis na tabela",
                                                            "zeroRecords": "Não há registros a serem exibidos",
                                                            "search": "Filtrar registros:",
                                                            "info": "Mostrando página _PAGE_ de _PAGES_",
                                                            "infoFiltered": " - filtragem de _MAX_ registros",
                                                            "lengthMenu": "Mostrar _MENU_ registros",
                                                            
                                                            "paginate": {
                                                               "previous": "Página anterior",
                                                               "next": "Próxima página",
                                                               "last": "Última página",
                                                               "first": "Primeira página",
                                                               
                                                               
                                                               
                                                            }
                                                         }
                                                         
                                                         
                                                         
                                                      } );
                                                      // Define a URL da API que retorna os dados da query em formato JSON
                                                      const apiUrl = 'table/table-search-os-integration.php';
                                                      
                                                      // Usa a função fetch() para obter os dados da API em formato JSON
                                                      fetch(apiUrl)
                                                      .then(response => response.json())
                                                      .then(data => {
                                                         // Mapeia os dados para o formato esperado pelo DataTables
                                                         const novosDados = data.map(item => [
                                                            ``,
                                                            item.nome,
                                                                              item.id_number,
                                                            
                                                            item.obs,
                                                          
                                                            `                           <a class="btn btn-app"  href="os-integration-open?id=${item.id}"onclick="new PNotify({
                                                   title: 'Atualização',
                                                   text: 'Atualização de Abertura de O.S!',
                                                   type: 'warning',
                                                   styling: 'bootstrap3'
                                             });">
                                 <i class="glyphicon glyphicon-floppy-open"></i> Abrir
                              </a>            <a class="btn btn-app" onclick="Swal.fire({
               title: 'Tem certeza?',
               text: 'Você não será capaz de reverter isso!',
               icon: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Sim, Deletar!'
            }).then((result) => {
               if (result.isConfirmed) {
                  Swal.fire(
                     'Deletando!',
                     'Seu arquivo será excluído.',
                     'success'
                  ),
                  window.location = 'backend/os-integration-trash-backend.php?id=${item.id}';
               }
            });">
               <i class="fa fa-trash"></i> Excluir
            </a>`
                                                         ]);
                                                         
                                                         // Adiciona as novas linhas ao DataTables e desenha a tabela
                                                         $('#datatable').DataTable().rows.add(novosDados).draw();
                                                         
                                                         
                                                         // Cria os filtros após a tabela ser populada
                                                         $('#datatable').DataTable().columns([1, 2, 3]).every(function (d) {
                                                            var column = this;
                                                            var theadname = $("#datatable th").eq([d]).text();
                                                            var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
                                                            var label = $('<label>'+theadname+': </label>').appendTo(container); 
                                                            var select = $('<select  class="form-control my-1"><option value="">' +
                                                               theadname +'</option></select>').appendTo('#userstable_filter').select2()
                                                            .on('change', function () {
                                                               var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                                               column.search(val ? '^' + val + '$' : '', true, false).draw();
                                                            });
                                                            
                                                            column.data().unique().sort().each(function (d, j) {
                                                               select.append('<option value="' + d + '">' + d + '</option>');
                                                            });
                                                         });
                                                      });
                                                   });
                                                   
                                                   
                                                </script>
                                                   