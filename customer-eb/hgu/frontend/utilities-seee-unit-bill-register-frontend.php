
<style>
  * {
    box-sizing: border-box;
  }
  
  body {
    background-color: #f1f1f1;
  }
  
  #regForm {
    /*  background-color: #ffffff;
    margin: 100px auto;
    font-family: Raleway;
    padding: 5px;
    width: 70%;
    min-width: 200px;*/
  }
  
  h1 {
    text-align: center;  
  }
  
  input {
    padding: 10px;
    width: 100%;
    font-size: 17px;
    font-family: ariel;
    border: 1px solid #aaaaaa;
  }
  
  /* Mark input boxes that gets an error on validation: */
  input.invalid {
    background-color: #ffdddd;
  }
  
  /* Hide all steps by default: */
  .tab {
    display: none;
  }
  
  button {
    background-color: #04AA6D;
    color: #ffffff;
    border: none;
    padding: 10px 20px;
    font-size: 17px;
    font-family: Raleway;
    cursor: pointer;
  }
  
  button:hover {
    opacity: 0.8;
  }
  
  #prevBtn {
    background-color: #bbbbbb;
  }
  
  /* Make circles that indicate the steps of the form: */
  .step {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none;  
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
    
  }
  
  .step.active {
    opacity: 1;
  }
  
  /* Mark the steps that are finished and valid: */
  .step.finish {
    background-color: #04AA6D;
  }
</style>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Sistema de Energia Elétrica de Emergência</small>SEEE</h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

         <a class="btn btn-app"  href="utilities-seee-supply-search">
            <i class="glyphicon glyphicon-list-alt"></i> Consulta
          </a>

          <a class="btn btn-app"  href="utilities-seee-supply-register">
            <i class="glyphicon glyphicon-plus"></i> Cadastro
          </a>

         



                  </div>
                </div>
              </div>
	       </div>
                   <!-- 1 -->
  <link href="dropzone.css" type="text/css" rel="stylesheet" />
  
  <!-- 2 -->
  <script src="dropzone.min.js"></script>
                <form class="dropzone"
                  action="backend/utilities-upload-backend.php?id=<?php printf($id); ?>"
                  method="post">
                </form>
                
            <div class="clearfix"></div>
             
              
            <form id="regForm" action="backend/utilities-seee-unit-bill-register-backend.php">
              <input id="row"  type="hidden" name="row" value="1">

              <!-- One "tab" for each step in the form: -->
              <div class="tab">Cabeçalho:
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                  <div class="col-md-6 col-sm-6 ">
                    <select type="text" class="form-control has-feedback-left" name="id_unidade_energia" id="id_unidade_energia"  >
                      <option value="">Selecione uma opção</option>
                      <?php
                        
                    
                        $result_cat_post  = "SELECT  id, unidade, cod, medidor FROM unidade_seee where trash != 0 ";
                        
                        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['unidade'].' - '.$row_cat_post['cod'].' - '.$row_cat_post['medidor'].'</option>';
                        }
                      ?>
                    </select>
                                      
                  </div>
                </div>
                
                <script>
                  $(document).ready(function() {
                    $('#id_unidade_energia').select2();
                  });
                </script>
                
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano (Ref)</label>
                  <div class="col-md-6 col-sm-6 ">
                    <select type="text" class="form-control has-feedback-left" name="id_year" id="id_year"  >
                      <option value="">Selecione uma opção</option>
                      <?php
                        
                        
                        
                        $result_cat_post  = "SELECT  id, yr FROM year ";
                        
                        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['yr'].'</option>';
                        }
                      ?>
                    </select>
                    
                  </div>
                </div>
                
                <script>
                  $(document).ready(function() {
                    $('#id_year').select2();
                  });
                </script>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Mês (Ref)</label>
                  <div class="col-md-6 col-sm-6 ">
                    <select type="text" class="form-control has-feedback-left" name="id_mouth" id="id_mouth"  >
                      <option value="">Selecione uma opção</option>
                      <?php
                        
                        
                        
                        $result_cat_post  = "SELECT  id, month FROM month ";
                        
                        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['month'].'</option>';
                        }
                      ?>
                    </select>
                    
                  </div>
                </div>
                
                <script>
                  $(document).ready(function() {
                    $('#id_mouth').select2();
                  });
                </script>
                
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Leitura Anterior</label>
                  <div class="col-md-4 col-sm-4 ">
                    <input id="read_before"  type="date" name="read_before" >
                  </div>
                </div>
                
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Leitura Atual</label>
                  <div class="col-md-4 col-sm-4 ">
                    <input id="registro"  type="date" name="registro" >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Proxima Leitura</label>
                  <div class="col-md-4 col-sm-4 ">
                    <input id="read_after"  type="date" name="read_after" >
                  </div>
                </div>
                
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Dias</label>
                  <div class="col-md-4 col-sm-4 ">
                    <input id="d_nf"  type="number" name="d_nf" value="<?php printf($d_nf); ?>" >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nota Fiscal</label>
                  <div class="col-md-4 col-sm-4 ">
                    <input id="n_nf"  type="number" name="n_nf" value="<?php printf($n_nf); ?>" >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor</label>
                  <div class="col-md-4 col-sm-4 ">
                    <input id="v_nf"  type="text" name="v_nf" value="<?php printf($v_nf); ?>" >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Consumo</label>
                  <div class="col-md-4 col-sm-4 ">
                    <input id="kwh"  type="number" name="kwh" value="<?php printf($kwh); ?>" >
                  </div>
                </div>
                
                
                  
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observa&ccedil;&atilde;o </label>
                  <div class="col-md-4 col-sm-4 ">
                    
                    
                    <textarea  rows="5" cols="5" id="obs"  class="form-control" name="obs" data-parsley-trigger="keyup"
                      data-parsley-validation-threshold="2" ></textarea>
                    
                  </div>
                </div>
                
       
                             
                <div class="ln_solid"></div>
                
             
                
                           
              </div>
              <div class="tab">Itens da fatura:
                
                <!--<button id="compose" class="btn btn-sm btn-success btn-block" type="button">Equipamento</button> -->
                <div class="row">
                  <div class="col-md-12 col-sm-12  ">
                    <div class="x_panel">
                      <div class="x_title">
                     
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="badge bg-green pull-right" value="Linha" onClick="javascript:novaLinha()"  ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Nova Linha</strong> </i></a>
                            <a class="badge bg-green pull-right"  onClick="javascript:calcular('')"   ><i class="fa fa-plus"><strong   style="color: #f0ffff"  > Calcular</strong> </i></a>
                                                      
                            
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Settings 1</a>
                                <a class="dropdown-item" href="#">Settings 2</a>
                              </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                            </ul>
                            <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                             
                              
                              
                              <table id="tabela_itens" >
                                <th class="table-header" >Item</th>
                                <th class="table-header">Unid</th>
                                <th class="table-header">Qtd</th>
                                <th class="table-header">Vlr. Unit.</th>
                                <th class="table-header">Valor</th>

                                
                                <tr>
                                                                     
          <th tabindex="1" > <input  class="form-control " type="text" id="va_1" name="va_1" > </th>
          <th tabindex="2" > <select  class="form-control " type="text" id="vb_1" name="vb_1"> <option value = "1">Unidade</option><option value="2">KWh</option></select> </th>
          <th tabindex="3" > <input  class="form-control " type="number" id="vc_1" name="vc_1"> </th>
          <th tabindex="4" > <input  class="form-control " type="number" id="vd_1" name="vd_1"> </th>
          <th tabindex="5" > <input  class="form-control " type="number" id="ve_1" name="ve_1"> </th>
                     
                                  
                                </tr>
                                
                              
                                
                              </table>
                              
                              
                            </div>
                            </div>
                            </div>
                            </div>
                               
                
              </div> 
              
              <div style="overflow:auto;">
                <div style="float:right;">
                  <button type="reset" onclick="clean()" class="btn btn-primary"onclick="new PNotify({
                    title: 'Limpado',
                    text: 'Todos os Campos Limpos',
                    type: 'info',
                    styling: 'bootstrap3'
                  });" />Limpar</button>
                  <button type="button" id="prevBtn" onclick="nextPrev(-1)">Voltar</button>
                  <button type="button" id="nextBtn" onclick="nextPrev(1)">Avançar</button>
                </div>
              </div>
              <!-- Circles which indicates the steps of the form: -->
              <div style="text-align:center;margin-top:40px;">
                <span class="step"></span>
                <span class="step"></span>
                
              </div>
            </form>
            
                        <script>
              var currentTab = 0; // Current tab is set to be the first tab (0)
              showTab(currentTab); // Display the current tab
              
              function showTab(n) {
                // This function will display the specified tab of the form...
                var x = document.getElementsByClassName("tab");
                x[n].style.display = "block";
                //... and fix the Previous/Next buttons:
                if (n == 0) {
                  document.getElementById("prevBtn").style.display = "none";
                } else {
                  document.getElementById("prevBtn").style.display = "inline";
                }
                if (n == (x.length - 1)) {
                  document.getElementById("nextBtn").innerHTML = "Salvar";
                } else {
                  document.getElementById("nextBtn").innerHTML = "Avançar";
                }
                //... and run a function that will display the correct step indicator:
                fixStepIndicator(n)
              }
              
              function nextPrev(n) {
                // This function will figure out which tab to display
                var x = document.getElementsByClassName("tab");
                // Exit the function if any field in the current tab is invalid:
                if (n == 1 && !validateForm()) return false;
                // Hide the current tab:
                x[currentTab].style.display = "none";
                // Increase or decrease the current tab by 1:
                currentTab = currentTab + n;
                // if you have reached the end of the form...
                if (currentTab >= x.length) {
                  // ... the form gets submitted:
                  document.getElementById("regForm").submit();
                  return false;
                }
                // Otherwise, display the correct tab:
                showTab(currentTab);
              }
              
              function validateForm() {
                // This function deals with validation of the form fields
                var x, y, i, valid = true;
                x = document.getElementsByClassName("tab");
                y = x[currentTab].getElementsByTagName("input");
                // A loop that checks every input field in the current tab:
                //  for (i = 0; i < y.length; i++) {
                // If a field is empty...
                //  if (y[i].value == "") {
                // add an "invalid" class to the field:
                //   y[i].className += " invalid";
                // and set the current valid status to false
                //   valid = false;
                //  }
                // }
                // If the valid status is true, mark the step as finished and valid:
                if (valid) {
                  document.getElementsByClassName("step")[currentTab].className += " finish";
                }
                return valid; // return the valid status
              }
              
              function fixStepIndicator(n) {
                // This function removes the "active" class of all steps...
                var i, x = document.getElementsByClassName("step");
                for (i = 0; i < x.length; i++) {
                  x[i].className = x[i].className.replace(" active", "");
                }
                //... and adds the "active" class on the current step:
                x[n].className += " active";
              }
            </script>
   
            
       
           
             
           
           
            
            
          
            <script>
              const input = document.querySelector('input[name="temp"]');
              
              input.addEventListener('change', function() {
                const value = this.value.replace(',', '.');
                this.value = value;
              });
              const input = document.querySelector('input[name="hum"]');
              
              input.addEventListener('change', function() {
                const value = this.value.replace(',', '.');
                this.value = value;
              });
              
            </script>
           
              
              
              
          </div>
          
          
        </div>

<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>
<script type="text/javascript">
  var tabela = document.getElementById('tabela').val();

  var p = 0;

  
  function novaLinha(){
    //  tabelarow = document.getElementById(name);
    var tabelarow = "tabela_itens";
    var contarow = "row";

     conta = document.getElementById("row").value; 
    
    
    conta++;
    
    
    
    var tabela = document.getElementById(tabelarow); // Substitua 'sua-tabela' pelo ID da sua tabela
    var row = tabela.insertRow(-1); // Insere uma nova linha no final da tabela
    
    var partes = [
      "<input type='text' class='form-control' id='va_" + conta + "' name='va_" + conta + "' >",
      "<select  class='form-control ' id='vb_" + conta + "' name='vb_" + conta + "'> <option value = '1'>Unidade</option><option value='2'>KWh</option></select>",
      "<input type='number' class='form-control' id='vc_" + conta + "' name='vc_" + conta + "'>",
      "<input type='number' class='form-control' id='vd_" + conta + "' name='vd_" + conta + "'>",
      "<input type='number' class='form-control' id='ve_" + conta + "' name='ve_" + conta + "'>"
    ];
    
    
    for (var i = 0; i < partes.length; i++) {
      var cell = row.insertCell(i);
      cell.innerHTML = partes[i];
    }
    //row.id = "tabela_linha_" + conta;  // Adiciona o ID à nova linha
    
    $('#row').val(conta);
  }
  function removeLinha(id){
    
    teste = document.getElementById(id);
    teste.parentNode.parentNode.removeChild(teste.parentNode);
  }
  
  
  

  
     
  function calcular() {
    conta2 = document.getElementById("row").value; 
    for (let i = 1; i <= conta2; i++) {
      var vc = "vc_" + i;
      var vd = "vd_" + i;
      var ve = "ve_" + i;
      var vc_number = document.getElementById(vc).value; 
      var vd_number = document.getElementById(vd).value; 
      var ve_number = vc_number * vd_number;
      document.getElementById(ve).value = ve_number;
    }
  }

    
  
  
</script>