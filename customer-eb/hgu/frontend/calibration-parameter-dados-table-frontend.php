<?php
include("database/database.php");
$query = "SELECT   calibration_parameter_dados.id, equipamento_grupo.nome,  calibration_parameter_dados.upgrade, calibration_parameter_dados.reg_date FROM calibration_parameter_dados INNER JOIN equipamento_grupo on  equipamento_grupo.id = calibration_parameter_dados.id_equipamento_grupo ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $name,$upgrade,$reg_date);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
 

?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                     <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Grupo</th>
                          <th>Atualização</th>
                          <th>Cadastro</th>
                          <th>Ação</th>
                           </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($name); ?> </td>
                          <td> <?php printf($upgrade); ?></td>
                         <td> <?php printf($reg_date); ?></td>
                           
                            
                           
                          <td>  
                 
                 
                  <a class="btn btn-app"  href="calibration-parameter-dados-add?dados=<?php printf($id); ?>"  onclick="new PNotify({
																title: 'Adicionar',
																text: 'Adicionar Dados',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-plus"></i> Adicionar
                  </a>  
                  <a class="btn btn-app"  href="calibration-parameter-dados-view?dados=<?php printf($id); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Dados',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>  
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
           