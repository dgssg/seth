
 
  
  <!-- 1 -->
  <link href="dropzone.css" type="text/css" rel="stylesheet" />
  
  <!-- 2 -->
  <script src="dropzone.min.js"></script>
  <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Gás GLP</small> </h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

         <a class="btn btn-app"  href="utilities-gas-glp-unit-search">
            <i class="glyphicon glyphicon-list-alt"></i> Consulta
          </a>

          <a class="btn btn-app"  href="utilities-gas-glp-unit-register">
            <i class="glyphicon glyphicon-plus"></i> Cadastro
          </a>

         



                  </div>
                </div>
              </div>
	       </div>
         
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cadastro <small> Dados</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <div class="clearfix"></div>
                    <form class="dropzone" action="backend/utilities-upload-backend.php" method="post">
                    </form>
                    <div class="ln_solid"></div>
                    <!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
                    <form action="backend/utilities-gas-glp-unit-backend.php" method="post">
                      
                      
                      
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="unidade" class="form-control" type="text" name="unidade"  value="<?php printf($unidade); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cod" class="form-control" type="text" name="cod"  value="<?php printf($cod); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Medidor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="medidor" class="form-control" type="text" name="medidor"  value="<?php printf($medidor); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Fornecimento</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="fornecimento" class="form-control" type="text" name="fornecimento"  value="<?php printf($fornecimento); ?> " >
                        </div>
                      </div>
                    
                      
                      <div class="ln_solid"></div>
                      
                      
                      <label for="middle-name">Observação</label>
                      
                      <textarea id="obs" class="form-control" name="obs"
                        data-parsley-trigger="keyup" data-parsley-validation-threshold="10"
                        placeholder="<?php printf($obs); ?>"
                        value="<?php printf($obs); ?>" ></textarea>
                      
                       
                      
                      <div class="ln_solid"></div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-9 col-sm-9 ">
                          <center>
                            
                            <button type="reset" onclick="clean()" class="btn btn-primary" onclick="new PNotify({
                              title: 'Limpado',
                              text: 'Todos os Campos Limpos',
                              type: 'info',
                              styling: 'bootstrap3'
                            });" />Limpar </button>
                            <input type="submit" name="submit1" class="btn btn-primary" onclick="new PNotify({
                              title: 'Registrado',
                              text: 'Informações registrada!',
                              type: 'success',
                              styling: 'bootstrap3'
                            });" value="Salvar" />
                       
                                                     </center>
                        </div>
                      </div>
                      
                      </div>
                    </form>                    
                    
                    
                    
                    
                    
                    
                     
                    
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
        </div>

<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>