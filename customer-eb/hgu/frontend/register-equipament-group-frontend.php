<div class="right_col" role="main">

    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>Grupo de Equipamento</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                    <div class="input-group">

                        <span class="input-group-btn">

                        </span>
                    </div>
                </div>
            </div>
        </div>


        <div class="x_panel">
            <div class="x_title">
                <h2>Cadastro</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">



                <form action="backend/register-equipament-group-single-backend.php" method="post">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span
                                class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" id="nome" name="nome" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Estimativa de Vida <span
                                class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="number" id="yr" name="yr" class="form-control">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar Informações</button>
                    </div>
                </form>




            </div>
        </div>



        <!-- page content -->



        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Filtro</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Settings 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id="userstable_filter"
                            style="column-count:5; -moz-column-count:5; -webkit-column-count:5; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;">
                        </div>


                    </div>
                </div>
            </div>
        </div>






        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista <small>de grupo</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="#">Settings 1</a>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />







                        <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nome</th>
                                                            <th>Estimativa de Vida</th>
                                                            <th>Cadastro</th>
                                                            <th>Atualização</th>
                                                            
                                                            <th>Familias</th>
                                                            <th>Descritivos</th>

                                                            <th>Ação</th>

                                                        </tr>
                                                    </thead>


                                                    <tbody>



                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
















</div>
</div>



<div class="x_panel">
    <div class="x_title">
        <h2>Ação</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <a class="btn btn-app" href="register-equipament">
            <i class="glyphicon glyphicon-arrow-left"></i> Voltar
        </a>
        <a class="btn btn-app" href="report-equipament-out-family" target="_blank">
            <i class="glyphicon glyphicon-file"></i> Sem Familia
        </a>



    </div>
</div>




</div>
</div>
<!-- /page content -->

<!-- /compose -->


<script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script language="JavaScript">
$(document).ready(function() {
    
    $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );


  // Define a URL da API que retorna os dados da query em formato JSON
  const apiUrl = 'table/table-search-equipamento-grupo.php';

  

  // Usa a função fetch() para obter os dados da API em formato JSON
  fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
      // Mapeia os dados para o formato esperado pelo DataTables
      const novosDados = data.map(item => [
        ``,
        item.nome,
        item.yr,
        item.reg_date,
        item.upgrade,
        `<a class="btn btn-app"  href="report-equipament-in-family?id=${item.id}" target="_blank">
          <i class="glyphicon glyphicon-file"></i> Familias
        </a>`,
        `<a class="btn btn-app"  href="register-equipament-group-des?id=${item.id}" >
          <i class="glyphicon glyphicon-pencil"></i> Descritivo
        </a>`,
        `<a class="btn btn-app" href="register-equipament-group-edit?id=${item.id}" onclick=" new PNotify({
          title: 'Editar',
          text: 'Abrindo Edição',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-edit"></i> Editar
        </a>
        <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Deletar!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Deletando!',
              'Seu arquivo será excluído.',
              'success'
            ),
            window.location = 'backend/register-equipament-group-trash-backend?id=${item.id}';
          }
        });">
          <i class="fa fa-trash"></i> Excluir
        </a>`
      ]);

      // Inicializa o DataTables com os novos dados
      const table = $('#datatable').DataTable();
      table.clear().rows.add(novosDados).draw();
      

      // Cria os filtros após a tabela ser populada
      $('#datatable').DataTable().columns([1, 2]).every(function(d) {
        var column = this;
        var theadname = $("#datatable th").eq([d]).text();
        var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
        var label = $('<label>' + theadname + ': </label>').appendTo(container);
        var select = $('<select  class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo('#userstable_filter').select2()
          .on('change', function() {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });

        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
      });

      var oTable = $('#datatable').dataTable();

// Avança para a próxima página da tabela
//oTable.fnPageChange('next');



    });


    // Armazenar a posição atual ao sair da página
$(window).on('beforeunload', function() {
  var table = $('#datatable').DataTable();
  var pageInfo = table.page.info();
  localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
});






});

  // Restaurar a posição ao retornar à página
  $(document).ready(function() {
    var storedPosition = localStorage.getItem('paginationPosition');
    if (storedPosition) {
      var pageInfo = JSON.parse(storedPosition);
      var table = $('#datatable').DataTable();
      table.page(pageInfo.page).draw('page');
    }
  });

</script>