<?php
include("database/database.php");
$query = "
SELECT unidade_gas_medicina_ratio.id,unidade_gas_medicina.des, instituicao.instituicao, instituicao_area.nome,unidade_gas_medicina_ratio.ratio FROM unidade_gas_medicina_ratio left JOIN instituicao on instituicao.id = unidade_gas_medicina_ratio.id_unidade left join unidade_gas_medicina on unidade_gas_medicina.id = unidade_gas_medicina_ratio.id_gas_medicina left join instituicao_area on instituicao_area.id = unidade_gas_medicina_ratio.id_setor";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id, $des,$instituicao, $setor, $ratio );
  //while ($stmt->fetch()) {
  //printf("%s, %s\n", $solicitante, $equipamento);
  //  }


  ?>

  <div class="col-md-11 col-sm-11 ">
    <div class="x_panel">

      <div class="x_content">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box table-responsive">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Gás Medicinal</th>
                    <th>Unidade</th>
                 
                    <th>Setor</th>
                    <th>Percentual</th>
                    <th>Ação</th>

                  </tr>
                </thead>


                <tbody>
                  <?php   while ($stmt->fetch()) {   ?>
                    <tr>
                      <td><?php printf($id); ?> </td>
                      <td><?php printf($des); ?> </td>
                      <td><?php printf($instituicao); ?></td>
                      <td><?php printf($setor); ?></td>
                      <td><?php printf($ratio); ?></td>


                      <td>
                         
                   
                    <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/utilities-gas-medicina-ratio-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                 <i class="fa fa-trash"></i> Excluir</a>
                    <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
                    title: 'Download',
                    text: 'Download O.S',
                    type: 'info',
                    styling: 'bootstrap3'
                  });">
                  <i class="fa fa-download"></i> Download
                </a> -->
              </td>
            </tr>
          <?php   } }  ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
