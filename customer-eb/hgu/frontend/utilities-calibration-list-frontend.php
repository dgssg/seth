<?php
include("database/database.php");
$query = "SELECT calibration.id, calibration.data_start, calibration.val, calibration.ven, calibration.status, equipamento.codigo, equipamento_familia.nome,equipamento_familia.fabricante,equipamento_familia.modelo from calibration inner join equipamento on equipamento.id = calibration.id_equipamento inner join equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$data_start,$val,$ven,$status,$codigo,$nome,$fabricante,$modelo );
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
 


?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>Laudo</th>
                          <th>Equipamento</th>
                          <th>Realização</th>
                          <th>Validade</th>
                          <th>vencido</th>
                           <th>Status</th>
                          
                         
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($codigo); ?> <?php printf($nome); ?> <?php printf($fabricante); ?> <?php printf($modelo); ?></td>
                          <td><?php printf($data_start); ?></td>
                            <td><?php printf($val); ?> Meses</td>
                            <td><?php if($ven ==0) {printf("Não");} if($ven ==1) {printf("Sim");}  ?></td>
                            <td><?php if($status ==0) {printf("Aprovado");} if($status ==1) {printf("Reprovado");}  ?> </td>
                        
                          
                      
                          <td>  
                   <a class="btn btn-app"  href="calibration-report-edit?laudo=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                  
           <a  class="btn btn-app" href="calibration-report-viewer?laudo=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar Laudo!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                  
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>