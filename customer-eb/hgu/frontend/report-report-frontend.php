<?php

include("database/database.php");


//$con->close();


?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Relatorio <small> do sistema</small></h3>
      </div>


    </div>

    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tabela <small>de Relatório</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings 1</a>
                  <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <!-- start accordion -->
            <div class="accordion"  aria-multiselectable="true">
              <div class="panel">

                <a class="panel-heading"  href="report-report-mc" >
                  <h4 class="panel-title">Manutenção Corretiva</h4>
                </a>

              </div>


              <div class="panel">
                <a class="panel-heading"  href="report-report-mp" >
                  <h4 class="panel-title">Manutenção Preventiva</h4>
                </a>

              </div>

              <div class="panel">
                <a class="panel-heading"  href="report-report-calibration" >
                  <h4 class="panel-title">Calibração</h4>
                </a>

              </div>






              <div class="panel">
                <a class="panel-heading"  href="report-report-equipament" >
                  <h4 class="panel-title">Inventario</h4>
                </a>

              </div>

              <div class="panel">
                <a class="panel-heading"  href="report-report-horaire" >
                  <h4 class="panel-title">Cronograma Preventiva</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-horaire-calibration" >
                  <h4 class="panel-title">Cronograma Calibração</h4>
                </a>

              </div>


              <div class="panel">
                <a class="panel-heading"  href="report-report-hierarchy " >
                  <h4 class="panel-title">Diagrama de Setor</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-manufacture" >
                  <h4 class="panel-title">Avaliação de Fornecedor</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-tse" >
                  <h4 class="panel-title">Teste de Segurança Elétrica</h4>
                </a>

              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-technological-surveillance" >
                  <h4 class="panel-title">Tecnovigilância</h4>
                </a>

              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-purchases" >
                  <h4 class="panel-title">Compras</h4>
                </a>

              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-obsolescence" >
                  <h4 class="panel-title">Obsolecência</h4>
                </a>

              </div>

              <div class="panel">
                <a class="panel-heading"  href="report-report-routine" >
                  <h4 class="panel-title">Rotina</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-calibration-report" >
                  <h4 class="panel-title">Laudos</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="register-equipament-family-movement-list" >
                  <h4 class="panel-title">Historico de Saida de Equipamento</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="register-equipament-form" >
                  <h4 class="panel-title">Formularios</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-bi" >
                  <h4 class="panel-title">Business intelligence</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-costs" >
                  <h4 class="panel-title">Custos</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-pdca-quarterly-analysis" >
                  <h4 class="panel-title">Analise Trimestral Avaliação</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-pdca-contracts" >
                  <h4 class="panel-title">Contratos</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-complice-list" >
                  <h4 class="panel-title">Não Conformidade</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-pdca-analysis" >
                  <h4 class="panel-title">Analise Trimestral O.S</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-anvisa-analysis" >
                  <h4 class="panel-title">Relatório Anvisa </h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-annvisa-analysis-equipament" >
                  <h4 class="panel-title">Relatório Anvisa Parque Tecnologico</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-pdca-quarterly-analysis-purchases" >
                  <h4 class="panel-title">Analise Trimestral Compras</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-disable-equipament" >
                  <h4 class="panel-title">Relatório Baixa Equipamento</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-report-equipament" >
                  <h4 class="panel-title">Relatório Parecer Tecnico de Equipamento</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-assessment-os" >
                  <h4 class="panel-title">Relatório de Avaliação de Ordem de Serviço</h4>
                </a>

              </div>
              <div class="panel">
                <a class="panel-heading"  href="report-report-equipament-calibration" >
                  <h4 class="panel-title">Relatório de Analisador e Simulador</h4>
                </a>
                
              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-anvisa-family" >
                  <h4 class="panel-title">Relatório de Registro Anvisa de Familia de Equipamentos</h4>
                </a>
                
              </div>
                <div class="panel">
                <a class="panel-heading"  href="report-report-anvisa-family-group" >
                  <h4 class="panel-title">Relatório de Registro Anvisa de Familia e Grupo de Equipamentos</h4>
                </a>
                
              </div>
                
                <div class="panel">
                <a class="panel-heading"  href="report-report-documentation-graduation" >
                  <h4 class="panel-title">Relatório de Registro Educação Continuada</h4>
                </a>
                
              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-documentation-support" >
                  <h4 class="panel-title">Relatório de Registro Plano de Contingência</h4>
                </a>
                
              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-documentation-bell" >
                  <h4 class="panel-title">Relatório de Registro de Notificações</h4>
                </a>
                
              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-documentation-hfmea" >
                  <h4 class="panel-title">Relatório de Registro de HFMEA</h4>
                </a>
                
              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-documentation-pop" >
                  <h4 class="panel-title">Relatório de Registro de POP</h4>
                </a>
                
              </div>
                <div class="panel">
                <a class="panel-heading"  href="report-report-equipament-group-des" >
                  <h4 class="panel-title">Relatório de Registro de Descritivo</h4>
                </a>
                
              </div>
               
                <div class="panel">
                <a class="panel-heading"  href="report-report-equipament-check-list" >
                  <h4 class="panel-title">Relatório de Registro de Check List de Instalação</h4>
                </a>
                
              </div>
                  <div class="panel">
                <a class="panel-heading"  href="report-report-equipament-training" >
                  <h4 class="panel-title">Relatório de Registro de Treinamento</h4>
                </a>
                
              </div>
             
                  <div class="panel">
                <a class="panel-heading"  href="report-report-os-integration" >
                  <h4 class="panel-title">Relatório de Ordem de Serviço Integração</h4>
                </a>
                
              </div>
                
                  <div class="panel">
                <a class="panel-heading"  href="report-report-mp-control-return" >
                  <h4 class="panel-title">Relatório de Devolutiva Manutenção Preventiva</h4>
                </a>
                
              </div>
               
                  <div class="panel">
                <a class="panel-heading"  href="report-report-equipament-in-family" >
                  <h4 class="panel-title">Relatório de Registro de Equipamento da Familia</h4>
                </a>
                
              </div>
               
                  <div class="panel">
                <a class="panel-heading"  href="report-report-group-out-family" >
                  <h4 class="panel-title">Relatório de Registro de Grupo de Equipamento Sem Familias</h4>
                </a>
                
              </div>
              
              <div class="panel">
                <a class="panel-heading"  href="report-report-family-in-group" >
                  <h4 class="panel-title">Relatório de Registro de Familias do grupo de Equipamento</h4>
                </a>
                
              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-family-out-equipament" >
                  <h4 class="panel-title">Relatório de Registro de Familias Sem Equipamento</h4>
                </a>
                
              </div>
               <div class="panel">
                <a class="panel-heading"  href="report-report-procedure-out-itens" >
                  <h4 class="panel-title">Relatório de Registro de Procedimento Sem Itens</h4>
                </a>
                
              </div>
             
            <div class="panel">
              <a class="panel-heading"  href="report-report-purchases-inventory-in" >
                <h4 class="panel-title">Relatório de Entrada de Material</h4>
              </a>
              
            </div>
            <div class="panel">
              <a class="panel-heading"  href="report-report-purchases-inventory-out" >
                <h4 class="panel-title">Relatório de Saida de Material</h4>
              </a>
              
            </div>
            <div class="panel">
              <a class="panel-heading"  href="report-report-purchases-inventory-stock" >
                <h4 class="panel-title">Relatório de Estoque de Material</h4>
              </a>
              
            </div>
            

            </div>









            


            </div>
            <!-- end of accordion -->


          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tabela <small>de Relatório</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings 1</a>
                  <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <!-- start accordion -->
            <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Manutenção Corretiva</h4>
                </a>
                <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date FROM report WHERE typology like 'mc' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a  href="report/report-indicator-<?php   printf($report);   ?>" download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne2" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Manutenção Preventiva</h4>
                </a>
                <div id="collapseOne2" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date  FROM report WHERE typology like 'mp' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"   role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a  href="report/report-indicator-<?php   printf($report);   ?>" download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne3" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Administrativo</h4>
                </a>
                <div id="collapseOne3" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date  FROM report WHERE typology like 'adm' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a  href="report/report-indicator-<?php   printf($report);   ?>" download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne4" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne4" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Equipamento</h4>
                </a>
                <div id="collapseOne4" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date  FROM report WHERE typology like 'equip' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne5" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne5" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Geral</h4>
                </a>
                <div id="collapseOne5" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date  FROM report WHERE typology like 'all' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a  href="report/report-indicator-<?php   printf($report);   ?>" download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne6" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne6" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Solicitações</h4>
                </a>
                <div id="collapseOne6" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date  FROM report WHERE typology like 'mat' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a  href="report/report-indicator-<?php   printf($report);   ?>" download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>


              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne7" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne7" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Tecnovigilancia</h4>
                </a>
                <div id="collapseOne7" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date  FROM report WHERE typology like 'tec' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a  href="report/report-indicator-<?php   printf($report);   ?>" download role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>



              <div class="panel">
                <a class="panel-heading" role="tab" id="headingOne8" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne8" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Fabricante</h4>
                </a>
                <div id="collapseOne8" class="panel-collapse collapse in" role="tabpane1" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Relatório</th>
                          <th>Nome</th>
                          <th>Codigo</th>
                          <th>Versão</th>
                          <th>Visualizar</th>
                          <th>Download</th>


                        </tr>
                      </thead>
                      <tbody>
                        <?php $query = "SELECT id, report, name, typology, link, codigo, version, upgrade, reg_date  FROM report WHERE typology like 'fab' ";


                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $report, $name, $typology, $link, $codigo, $version, $upgrade, $reg_date);
                          while ($stmt->fetch()) {
                            //printf("%s, %s\n", $solicitante, $equipamento);
                            //  }

                            ?>
                            <tr>

                              <th scope="row"> <?php   printf($id);   ?></th>
                              <td><?php   printf($report);   ?></td>
                              <td><?php   printf($name);   ?></td>
                              <td><?php   printf($codigo);   ?></td>
                              <td><?php   printf($version);   ?></td>
                              <td><a href="report/report-indicator-<?php   printf($report);   ?>" target="_blank"  role="button" aria-expanded="false"><i class="fa fa-eye"></i></a></td>
                              <td><a  href="report/report-indicator-<?php   printf($report);   ?>" download  role="button" aria-expanded="false"><i class="fa fa-download"></i></a></td>


                            </tr>
                          <?php       }
                        }
                        ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>



            </div>
            <!-- end of accordion -->


          </div>
        </div>
      </div>
    </div>




  </div>
</div>


<!-- /page content -->
