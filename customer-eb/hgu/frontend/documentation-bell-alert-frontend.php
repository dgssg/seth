<?php

include("database/database.php");


//$con->close();


?>

<!-- page content -->
       <div class="right_col" role="main">
         <div class="">
           <div class="page-title">
             <div class="title_left">
               <h3>Central de Notificações <small></small></h3>
             </div>


           </div>

           <div class="clearfix"></div>

            <div class="x_panel">
              <div class="x_title">
                <h2>Ação</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                      class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                
                <style>
                  .btn.btn-app {
                    border: 2px solid transparent; /* Define a borda como transparente por padrão */
                    padding: 5px;
                    position: relative; /* Necessário para posicionar o pseudo-elemento */
                  }
                  
                  .btn.btn-app.active {
                    border: 2px solid #ffcc00; /* Altera a cor da borda para o menu selecionado */
                  }
                  
                  .btn.btn-app.active::after {
                    content: "";
                    position: absolute;
                    left: 0;
                    bottom: 0; /* Posição da linha no final do elemento */
                    width: 100%;
                    height: 3px; /* Espessura da linha */
                    background-color: #ffcc00; /* Cor da linha */
                  }
                  
                </style>
                <?php
                  $current_page = basename($_SERVER['REQUEST_URI'], ".php");
                ?>
                
                
                
                <a class="btn btn-app <?php echo $current_page == 'documentation' ? 'active' : ''; ?>" href="documentation">
                  <i class="fa fa-code"></i> Sistema
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-pop' ? 'active' : ''; ?>" href="documentation-pop">
                  
                  <i class="fa fa-book"></i> POP
                </a>
                <a class="btn btn-app <?php echo $current_page == 'documentation-update' ? 'active' : ''; ?>" href="documentation-update">
                  
                  <i class="fa fa-history"></i> Atualização
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-hfmea' ? 'active' : ''; ?>" href="documentation-hfmea">
                  
                  <i class="fa fa-map"></i> HFMEA
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-label' ? 'active' : ''; ?>" href="documentation-label">
                  
                  <i class="fa fa-fax"></i> Rotuladora
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-bussines' ? 'active' : ''; ?>" href="documentation-bussines">
                  
                  <i class="fa fa-folder-open"></i> Documentação
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-support' ? 'active' : ''; ?>" href="documentation-support">
                  
                  <i class="fa fa-support"></i> Contingência
                </a>
                
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-bell' ? 'active' : ''; ?>" href="documentation-bell">
                  
                  <i class="fa fa-bell"></i> Central de Notificações
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-graduation' ? 'active' : ''; ?>" href="documentation-graduation">
                  
                  <i class="fa fa-graduation-cap"></i> Educação Continuada
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-library' ? 'active' : ''; ?>" href="documentation-library">
                  
                  <i class="fa fa-book"></i> Biblioteca
                </a> 
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-connectivity' ? 'active' : ''; ?>" href="documentation-connectivity">
                  
                  <i class="fa fa-code-fork"></i> Conectividade &amp; Recursos
                </a>
                
                <a class="btn btn-app <?php echo $current_page == 'documentation-event-task' ? 'active' : ''; ?>" href="documentation-event-task">
                  
                  <i class="fa fa-tasks"></i> Eventos &amp; Taferas
                </a>                 
                <a class="btn btn-app <?php echo $current_page == 'documentation-widget' ? 'active' : ''; ?>" href="documentation-widget">
                  
                  <i class="fa fa-desktop"></i> Widget
                </a>
                
                
                
                
                
                
              </div>
            </div>
             <div class="clearfix"></div>
            <div class="clearfix"></div>
            
            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Menu</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <a class="btn btn-app"  href="documentation-bell-alert">
                      <i class="glyphicon glyphicon-list-alt"></i> Historico
                    </a>
                    
                    <a class="btn btn-app"  href="documentation-bell">
                      <i class="glyphicon glyphicon-plus"></i> Notificações
                    </a>
                    
                        <a class="btn btn-app"  href="documentation-bell-cogs">
                      <i class="glyphicon glyphicon-cog"></i> Configuração
                    </a>           
                    
                    
                                 
                    
                    
                    
                  </div>
                </div>
              </div>
            </div>
            

            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>


             <div class="x_panel">
                <div class="x_title">
                  <h2>Central de Notifcaição</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Origem</th>
                        <th>Referencia</th>
                        <th>Data Vencimento</th>
                        
                        <th>Ação</th>
                        
                        
                      </tr>
                    </thead>
                    
                    
                    <tbody>
                      
                      
                      
                      
                    </tbody>
                  </table>


                              </div>
                            </div>

                </div>
              </div>
                   <!-- end of accordion -->






                   <script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').dataTable({
      "processing": true,
      "stateSave": true,
      responsive: true,
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
        }
      }
    });

    const apiUrl = 'table/table-search-bell-alert.php';

    fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        const novosDados = data.map(item => [
          ``,
          item.origem,
          item.ref,
          item.data,
          `
          
           <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Abrir!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Notificação!',
              'Abrir Notificação!.',
              'success'
            ),
            window.location = 'central-bell?ref=${item.ref}&data=${item.data}&origem=${item.origem}';
          }
        });">
                               <i class="fa fa-folder-open"></i>  Abrir Notificação
                                </a>
          <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Abrir!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Notificação!',
              'Sua notificação marcado como lido.',
              'success'
            ),
            window.location = 'backend/documentation-bell-alert-read-backend.php?id=${item.id}';
          }
        });">
                                <i class="fa fa-bell"></i> Marcar como Lido
                                </a>`
        ]);

        $('#datatable').DataTable().rows.add(novosDados).draw();

        $('#datatable').DataTable().columns([1, 2, 3]).every(function(d) {
          var column = this;
          var theadname = $("#datatable th").eq([d]).text();
          var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
          var label = $('<label>' + theadname + ': </label>').appendTo(container);
          var select = $('<select class="form-control my-1"><option value="">' + theadname + '</option></select>')
            .appendTo('#userstable_filter').select2()
            .on('change', function() {
              var val = $.fn.dataTable.util.escapeRegex($(this).val());
              column.search(val ? '^' + val + '$' : '', true, false).draw();
            });

          column.data().unique().sort().each(function(d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
      });
  });
</script>


       <!-- /page content -->
                        