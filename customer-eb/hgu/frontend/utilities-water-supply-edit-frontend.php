<?php
$codigoget = ($_GET["id"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT *FROM unidade_agua_abastecimento where id = $codigoget";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($id, $date, $abastecimento, $valor,$obs,$upgrade,$reg_date);
  while ($stmt->fetch()) {
  //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }

  ?>

<div class="right_col" role="main">

  <div class="">

    <div class="page-title">
      <div class="title_left">
        <h3>Edição</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Alerta</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Area de Edição!</strong> Todo alteração não é irreversível.
          </div>



        </div>
      </div>



      <!-- page content -->









      <div class="row">
        <div class="col-md-12 col-sm-12 ">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edição <small>de Abastecimento</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form action="backend/utilities-water-supply-edit-backend.php" method="post">
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="id" class="form-control" name="id" type="hidden" value="<?php printf($id); ?> " >
                  </div>
                </div>

                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="date_start" class="form-control" type="text" name="date_start"  value="<?php printf($date); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Abastecimento</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="abastecimento" class="form-control" type="text" name="abastecimento"  value="<?php printf($abastecimento); ?> " >
                  </div>
                </div>
                <div class="item form-group">
                  <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor</label>
                  <div class="col-md-6 col-sm-6 ">
                    <input id="p_abastecimento" class="form-control" type="text" name="p_abastecimento"  value="<?php printf($valor); ?> " >
                  </div>
                </div>


                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 "></label>
                  <div class="col-md-3 col-sm-3 ">
                    <center>
                      <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                    </center>
                  </div>
                </div>


              </form>






            </div>
          </div>
        </div>
      </div>



      <div class="clearfix"></div>
      <div class="x_panel">
        <div class="x_title">
          <h2>Observação</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="ln_solid"></div>
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "></label>
              <div class="col-md-3 col-sm-3 ">
                <center>
                  <button id="compose" class="btn btn-sm btn-success btn-block" class="form-control "type="button">Observação</button>
                </center>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 " class="docs-tooltip" data-toggle="tooltip" title="Observação "></label>
              <div class="col-md-6 col-sm-6">
                <input type="text" class="form-control" readonly="readonly" value="<?php printf($obs); ?>">
              </div>
            </div>



          </div>
        </div>

      <div class="x_panel">
        <div class="x_title">
          <h2>Ação</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <a class="btn btn-app"  href="utilities-water-supply">
              <i class="glyphicon glyphicon-arrow-left"></i> Voltar
            </a>



          </div>
        </div>




      </div>
    </div>
    <!-- /page content -->

    <!-- compose -->
    <div class="compose col-md-6  ">
      <div class="compose-header">
        Observação
        <button type="button" class="close compose-close">
          <span>×</span>
        </button>
      </div>
      <form  action="backend/utilities-water-supply-edit-comment-backend.php?id=<?php printf($codigoget);?>" method="post">
        <div class="compose-body">
          <div id="alerts"></div>


          <input type="text" id="editor" name="editor" size="90"class="editor-wrapper">


        </div>

        <div class="compose-footer">
          <button class="btn btn-sm btn-success" type="submit">Salvar</button>
        </div>
      </form>
    </div>
    <!-- /compose -->
    <!-- /compose -->




    <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    
     