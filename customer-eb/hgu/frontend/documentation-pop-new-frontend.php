<?php

include("database/database.php");


//$con->close();


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Documentos & POP <small></small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="documentation">
                    <i class="fa fa-code"></i> Sistema
                  </a>

                  <a class="btn btn-app"  href="documentation-pop">
                    <i class="fa fa-book"></i> POP
                  </a>
                  <a class="btn btn-app"  href="documentation-update">
                    <i class="fa fa-history"></i> Atualização
                  </a>
                  <a class="btn btn-app"  href="documentation-hfmea">
                    <i class="fa fa-map"></i> HFMEA
                  </a>
                  <a class="btn btn-app"  href="documentation-label">
                    <i class="fa fa-fax"></i> Rotuladora
                  </a>
                  <a class="btn btn-app"  href="documentation-bussines">
                    <i class="fa fa-folder-open"></i> Documentação
                  </a>



                </div>
              </div>

              <div class="clearfix"></div>

<div class="row" style="display: block;">
  <div class="col-md-12 col-sm-12  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

<a class="btn btn-app"  href="documentation-pop">
<i class="glyphicon glyphicon-list-alt"></i> POP's
</a>

<a class="btn btn-app"  href="documentation-pop-group">
<i class="glyphicon glyphicon-plus"></i> Grupo POP
</a>

<a class="btn btn-app"  href="documentation-pop-new">
<i class="glyphicon glyphicon-plus"></i>Cadastro POP
</a>




<?php //include 'frontend/maintenance-routine-register-frontend.php';?>




      </div>
    </div>
  </div>
</div>

               <div class="x_panel">
                <div class="x_title">
                  <h2>Cadastro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


                   <?php include 'frontend/documentation-pop-register-frontend.php';?>



                </div>
              </div>
              <div class="clearfix"></div>
          
            

                </div>
              </div>

      
        <!-- /page content -->
