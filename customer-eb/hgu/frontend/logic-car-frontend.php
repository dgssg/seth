
       <div class="right_col" role="main">
         <div class="">
           <div class="page-title">
             <div class="title_left">
               <h3>Logistica de Transporte <small></small></h3>
             </div>


           </div>

           <div class="clearfix"></div>
 
            

            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:2; -moz-column-count:2; -webkit-column-count:2; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>


             <div class="x_panel">
                <div class="x_title">
                  <h2>Solicitações de Transporte</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Solicitação</th>
                        <th>Solicitante</th>
                        <th>Data Inicial</th>
                        <th>Data Final</th>
                        <th>Descrição</th>
                        <th>Ação</th>
                        
                      </tr>
                    </thead>
                    
                    
                    <tbody>
                      
                      
                      
                      
                    </tbody>
                  </table>


                              </div>
                            </div>

                </div>
              </div>
                   <!-- end of accordion -->






                        <script type="text/javascript">
                          
                          
                          
                          $(document).ready(function() {
                            
                            $('#datatable').dataTable( {
                              "processing": true,
                              "stateSave": true,
                              responsive: true,
                              
                              
                              
                              "language": {
                                "loadingRecords": "Carregando dados...",
                                "processing": "Processando  dados...",
                                "infoEmpty": "Nenhum dado a mostrar",
                                "emptyTable": "Sem dados disponíveis na tabela",
                                "zeroRecords": "Não há registros a serem exibidos",
                                "search": "Filtrar registros:",
                                "info": "Mostrando página _PAGE_ de _PAGES_",
                                "infoFiltered": " - filtragem de _MAX_ registros",
                                "lengthMenu": "Mostrar _MENU_ registros",
                                
                                "paginate": {
                                  "previous": "Página anterior",
                                  "next": "Próxima página",
                                  "last": "Última página",
                                  "first": "Primeira página",
                                  
                                  
                                  
                                }
                              }
                              
                              
                              
                            } );
                            // Define a URL da API que retorna os dados da query em formato JSON
                            const apiUrl = 'table/table-search-car.php';
                            
                            // Usa a função fetch() para obter os dados da API em formato JSON
                            fetch(apiUrl)
                            .then(response => response.json())
                            .then(data => {
                              // Mapeia os dados para o formato esperado pelo DataTables
                              const novosDados = data.map(item => [
                                ``,
                                item.id,
                                item.nome,
                                item.start,
                                item.end,
                                item.title,
                                `           <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Abrir!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Notificação!',
              'Aceitar Solicitação!.',
              'success'
            ),
              window.location = 'backend/logic-car-open-backend.php?id=${item.id}';
          }
        });">
                                <i class="fa fa-folder-open"></i>  Aceitar Solicitação
                                </a>
          <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Abrir!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Notificação!',
              'Recusar Solicitação.',
              'success'
            ),
            window.location = 'backend/logic-car-close-backend.php?id=${item.id}';
          }
        });">
                                <i class="fa fa-close"></i> Recusar Solicitação
                                </a>`,
                                                          
                              ]);
                              
                              // Adiciona as novas linhas ao DataTables e desenha a tabela
                              $('#datatable').DataTable().rows.add(novosDados).draw();
                              
                              
                              // Cria os filtros após a tabela ser populada
                              $('#datatable').DataTable().columns([1, 2, 3,4]).every(function (d) {
                                var column = this;
                                  var theadname = $("#datatable th").eq([d]).text();
  
  // Container para o título, o select e o alerta de filtro
  var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
  
  // Título acima do select
  var title = $('<label>' + theadname + '</label>').appendTo(container);
  
  // Container para o select
  var selectContainer = $('<div></div>').appendTo(container);
  
  var select = $('<select class="form-control my-1"><option value="">' +
    theadname + '</option></select>').appendTo(selectContainer).select2()
  .on('change', function() {
    var val = $.fn.dataTable.util.escapeRegex($(this).val());
    column.search(val ? '^' + val + '$' : '', true, false).draw();
    
    // Remove qualquer alerta existente
    container.find('.filter-alert').remove();
    
    // Se um valor for selecionado, adicionar o alerta de filtro
    if (val) {
      $('<div class="filter-alert">' +
        '<span class="filter-active-indicator">&#x25CF;</span>' +
        '<span class="filter-active-message">Filtro ativo</span>' +
        '</div>').appendTo(container);
    }
    
    // Remove o indicador do título da coluna
    $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
    
    // Se um valor for selecionado, adicionar o indicador no título da coluna
    if (val) {
      $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
    }
  });
  
  column.data().unique().sort().each(function(d, j) {
    select.append('<option value="' + d + '">' + d + '</option>');
  });
  var filterValue = column.search();
  if (filterValue) {
    select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
  }
  
  
      });
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      }); 
                              
                            });
                          });
                          
                          
                        </script>

       <!-- /page content -->
                        