<?php
 
include("../database/database.php");

$codigoget = ($_GET["id"]);

$message_mp = $_POST['message_mp'];
$message_tc = $_POST['message_tc'];
$message_ne = $_POST['message_ne'];



$status="2";

$stmt = $conn->prepare("UPDATE maintenance_preventive SET id_status= ? WHERE id= ?");
$stmt->bind_param("ss",$status,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$message_mp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_tc= ? WHERE id= ?");
$stmt->bind_param("ss",$message_tc,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE maintenance_preventive SET obs_ne= ? WHERE id= ?");
$stmt->bind_param("ss",$message_ne,$codigoget);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
header('Location: ../maintenance-preventive-infra');
?>