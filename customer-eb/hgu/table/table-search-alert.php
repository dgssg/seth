<?php
include("../database/database.php");
$query = "SELECT
equipamento_familia.nome AS 'familia',
equipamento_familia.modelo,
equipamento_familia.fabricante,
equipamento.codigo,
technological_surveillance.mp,
technological_surveillance.id,
technological_surveillance.id_os,
technological_surveillance.descricao,
technological_surveillance_list.nome,
technological_surveillance.status_tec,
technological_surveillance.acao_tec,
technological_surveillance.data_inicio,
technological_surveillance.data_fim,
(
    SELECT GROUP_CONCAT(regdate_technological_dropzone.file SEPARATOR ';')
    FROM regdate_technological_dropzone
    WHERE regdate_technological_dropzone.id_technological = technological_surveillance.id
) AS 'id_anexo2'
FROM
technological_surveillance
LEFT JOIN technological_surveillance_list ON technological_surveillance_list.id = technological_surveillance.id_technological
LEFT JOIN os ON os.id = technological_surveillance.id_os AND technological_surveillance.mp = 0
LEFT JOIN maintenance_preventive ON maintenance_preventive.id = technological_surveillance.id_os AND technological_surveillance.mp = 1
LEFT JOIN maintenance_routine ON maintenance_routine.id = maintenance_preventive.id_routine AND technological_surveillance.mp = 1
LEFT JOIN equipamento ON equipamento.id = os.id_equipamento AND technological_surveillance.mp = 0
OR equipamento.id = maintenance_routine.id_equipamento AND technological_surveillance.mp = 1
OR equipamento.id = technological_surveillance.id_os AND technological_surveillance.mp IS NULL
LEFT JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia
ORDER BY
technological_surveillance.id DESC
";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
