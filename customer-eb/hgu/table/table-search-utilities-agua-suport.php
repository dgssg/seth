<?php
include("../database/database.php");
$query = "SELECT colaborador.primeironome,colaborador.ultimonome,instituicao_area.nome AS 'setor',unidade_agua_suport.class, equipamento_grupo.nome, unidade_agua_suport.id, unidade_agua_suport.id_equipamento_grupo, unidade_agua_suport.titulo, unidade_agua_suport.ver, unidade_agua_suport.file, unidade_agua_suport.data_now, unidade_agua_suport.data_before, unidade_agua_suport.upgrade, unidade_agua_suport.reg_date FROM unidade_agua_suport  LEFT JOIN equipamento_grupo ON equipamento_grupo.id = unidade_agua_suport.id_equipamento_grupo LEFT JOIN instituicao_area ON instituicao_area.id = unidade_agua_suport.id_setor LEFT JOIN colaborador ON colaborador.id = unidade_agua_suport.id_colaborador order by unidade_agua_suport.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
