<?php
include("../database/database.php");
    $query = "SELECT unidade_seei_fatura.id, unidade_seei.unidade,unidade_seei.cod,month.month, year.yr, unidade_seei_fatura.v_nf,unidade_seei.medidor,unidade_seei_fatura.kwh,unidade_seei_fatura.obs,unidade_seei_fatura.reg_date,unidade_seei_fatura.upgrade FROM unidade_seei_fatura INNER JOIN unidade_seei ON unidade_seei.id = unidade_seei_fatura.id_unidade_seei INNER JOIN month
    ON month.id = unidade_seei_fatura.id_mouth INNER JOIN year ON year.id = unidade_seei_fatura.id_year WHERE unidade_seei_fatura.trash != 0 order by unidade_seei_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
