<?php
include("../database/database.php");
$query = "SELECT id, primeironome, ultimonome,email,telefone,cargo,matricula,nascimento,entidade,reg_date FROM colaborador WHERE trash = 1 ORDER by id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
