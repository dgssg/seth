<?php
include("../database/database.php");
$query = "SELECT equipamento.pmoc,equipamento.serie,equipamento.comodato,equipamento.baixa,equipamento.ativo,instituicao.instituicao, instituicao_area.nome AS 'area',instituicao_localizacao.nome AS 'setor' ,equipamento.id, equipamento_familia.nome AS 'equipamento', equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo from equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia  INNER JOIN instituicao_localizacao on instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade where equipamento.trash = 1 order by equipamento.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
