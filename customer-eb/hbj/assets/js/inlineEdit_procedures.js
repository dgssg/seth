function showEdit(editableObj) {
	$(editableObj).css("background", "#FFF");
}

function saveToDatabase(editableObj, column, id) {
	$(editableObj)
			.css("background", "#FFF url(./images/loaderIcon.gif) no-repeat center right 5px");
	
	var data;
	if ($(editableObj).prop("tagName") == "SELECT") {
		data = 'column=' + column + '&editval=' + $(editableObj).val() + '&id=' + id;
	} else {
		data = 'column=' + column + '&editval=' + editableObj.innerHTML + '&id=' + id;
	}

	$.ajax({
		url : "./ajax-end-point/save-edit-procedure.php",
		type : "POST",
		data : data,
		success : function(data) {
			$(editableObj).css("background", "#FDFDFD");
		}
	});
}
