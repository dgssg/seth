<?php
// Verifica se a variável foi recebida via GET
if(isset($_GET['variable'])) {
    // Recebe a variável enviada via GET
    $myVariable = $_GET['variable'];
    include("database/database.php");
    
    // Prepara a consulta SQL com um espaço reservado (?) para o parâmetro
    $query = "SELECT date_mp_start FROM maintenance_preventive WHERE id = ?";
    
    // Prepara a consulta
    if ($stmt = $conn->prepare($query)) {
        // Bind do parâmetro na consulta preparada
        $stmt->bind_param("i", $myVariable);
        // Executa a consulta
        $stmt->execute();
        // Liga o resultado da consulta a uma variável
        $stmt->bind_result($mp);
        // Obtém o resultado da consulta
        $stmt->fetch();
        // Fecha a declaração
        $stmt->close();
    }

    // Retorna a data inicial
    echo $mp;
}
?>
