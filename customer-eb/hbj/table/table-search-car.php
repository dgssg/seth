<?php
include("../database/database.php");
$query = "SELECT events_logistica_request.id, events_logistica_request.title, events_logistica_request.start, events_logistica_request.end, usuario.nome FROM events_logistica_request LEFT JOIN usuario ON usuario.id = events_logistica_request.id_user WHERE events_logistica_request.read_open = 1 order by events_logistica_request.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
