<?php
include("../database/database.php");
    $query = "SELECT unidade_disel_fatura.id, unidade_disel.unidade,unidade_disel.cod,month.month, year.yr, unidade_disel_fatura.v_nf,unidade_disel.medidor,unidade_disel_fatura.kwh,unidade_disel_fatura.obs,unidade_disel_fatura.reg_date,unidade_disel_fatura.upgrade FROM unidade_disel_fatura INNER JOIN unidade_disel ON unidade_disel.id = unidade_disel_fatura.id_unidade_disel INNER JOIN month
    ON month.id = unidade_disel_fatura.id_mouth INNER JOIN year ON year.id = unidade_disel_fatura.id_year WHERE unidade_disel_fatura.trash != 0 order by unidade_disel_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
