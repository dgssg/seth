<?php
include("../database/database.php");
    $query = "SELECT unidade_energia_fatura.id, unidade_energia.unidade,unidade_energia.cod,month.month, year.yr, unidade_energia_fatura.v_nf,unidade_energia.medidor,unidade_energia_fatura.kwh,unidade_energia_fatura.obs,unidade_energia_fatura.reg_date,unidade_energia_fatura.upgrade FROM unidade_energia_fatura INNER JOIN unidade_energia ON unidade_energia.id = unidade_energia_fatura.id_unidade_energia INNER JOIN month
    ON month.id = unidade_energia_fatura.id_mouth INNER JOIN year ON year.id = unidade_energia_fatura.id_year WHERE unidade_energia_fatura.trash != 0 order by unidade_energia_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
