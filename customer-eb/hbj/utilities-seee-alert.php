<?php
include("database/database.php");
	session_start();
	if(!isset($_SESSION['usuario'])){
		header ("Location: ../index.php");
	}
	if($_SESSION['id_nivel'] != 1 ){
    header ("Location: ../index.php");
}
if($_SESSION['instituicao'] != $key ){
    header ("Location: ../index.php");
} if( $_SESSION['mod'] != $mod ){
    header ("Location: ../index.php");
}
	require_once 'auth.php';
	require_once 'permissions.php';
	// Verificar se o usuário é admin
	checkPermission('1', $pdo);
	// Verificar se o usuário está autenticado
	if (!isset($_SESSION['session_token'])) {
		header('Location: ../../index.php');
		exit;
	}
	
	// Valida o token de sessão
	$stmt = $conn->prepare("SELECT * FROM usuario WHERE session_token = ? AND is_active = TRUE");
	$stmt->bind_param('s', $_SESSION['session_token']);
	$stmt->execute();
	$result = $stmt->get_result();
	$user = $result->fetch_assoc();

	if (!$user) {
		// Token inválido ou sessão expirada
		session_destroy();
		header('Location: ../../index.php');
		exit;
	}
	
	$query = "SELECT t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11 FROM tools";

	if ($stmt = $conn->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($t1,$t2,$t3,$t4,$t5,$t6,$t7,$t8,$t9,$t10,$t11);
		while ($stmt->fetch()) {
			//printf("%s, %s\n", $solicitante, $equipamento);
		}
	}
	$usuariologado=$_SESSION['usuario'];
	$instituicaologado=$_SESSION['instituicao'];
	$setorlogado=$_SESSION['setor'];

	// Adiciona a Função display_campo($nome_campo, $tipo_campo)
	//require_once "personalizacao_display.php";

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../framework/images/favicon.ico" type="image/ico" />

    <title>SETH-| Sistema de Engenharia Tecnica Hospitalar</title>
<style>
	.nav-item .info-number {
		position: relative;
		display: inline-flex; /* Mantém o ícone e a badge alinhados */
		align-items: center;
		justify-content: center;
		width: 24px; /* Ajuste conforme o tamanho do ícone */
		height: 24px; /* Ajuste conforme o tamanho do ícone */
		text-decoration: none;
		padding: 0;
	}
	
	.nav-item .info-number i {
		font-size: 24px; /* Tamanho do ícone */
		color: #333; /* Cor do ícone */
		cursor: pointer;
	}
	
	.nav-item .info-number .badge {
		position: absolute;
		top: -5px;
		right: -10px;
		background-color: #ff0000; /* Cor da badge */
		color: #fff;
		font-size: 12px;
		padding: 3px 6px;
		border-radius: 50%;
		pointer-events: none; /* Badge não clicável */
	}

</style>


   <!-- Bootstrap -->
    <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="../../framework/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../framework/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../framework/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../../framework/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="../../framework/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../../framework/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../../framework/build/css/custom.min.css" rel="stylesheet">
    
      <!-- PNotify -->
  <link href="../../framework/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="../../framework/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
  <link href="../../framework/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">

 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
   <!-- <script src="jquery.min.js" ></script> -->
    <!-- Datatables -->
    
    <link href="../../framework/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../framework/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
  </head>

   <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard" class="site_title"><i class="fa fa-500px"></i> <span>SETH Predial</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="logo/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?php printf($usuariologado) ?></h2>
                <p class="glyphicon glyphicon-time" id="countdown"></p>
               <span>
              <script> document.write(new Date().toLocaleDateString()); </script>
              </span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>Geral</h3>
							<ul class="nav side-menu">
								<li><a><i class="fa fa-home"></i> Início <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="dashboard" title="Painel de supervisão">Dashboard</a></li>
										
										
									</ul>
								</li>
								<li><a><i class="fa fa-edit"></i> Ordem de Serviço <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="os-open" title="Abrir nova ordem de serviço">Abrir OS </a></li>
										<li><a href="os-opened" title="Gerenciar ordem de serviço solicitadas">Aberto </a></li>
										<!--      <li><a href="form_validation.php">Buscar</a></li> -->
										<li><a href="os-progress" title="Gerenciar ordem de serviço em andamento">Em processo</a></li>
										<!--  <li><a href="os-concluded">Fechada</a></li> -->
										<li><a href="os-signature" title="Gerenciar ordem de serviço para assinatura eletrônica">Assinatura</a></li>
										<li><a href="os-signature-user" title="Gerenciar ordem de serviço para assinatura eletrônica do solicitante">Solicitante</a></li>
										<li><a href="os-finished" title="Gerenciar ordem de serviço concluida">Concluida</a></li>
										<li><a href="os-canceled" title="Gerenciar ordem de serviço cancelada">Canceleda</a></li>
										<li><a href="os-integration" title="Gerenciar ordem de Integração">Integração</a></li>
										<!--     <li><a href="form_upload.php">Form Upload</a></li> -->
										<!--    <li><a href="form_buttons.php">Form Buttons</a></li> -->
									</ul>
								</li>
								<li><a><i class="fa fa-desktop"></i> Cadastro &amp; Consulta <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<!--<li><a href="general_elements.php">Instituição</a></li>-->

										<li><a>Localização<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-location-unit" title="Cadastrar ou gerenciar Unidades">Unidade</a>
												</li>
												<li><a href="register-location-area" title="Cadastrar ou gerenciar Setores">Setor</a>
												</li>
												<li><a href="register-location-sector" title="Cadastrar ou gerenciar Area">Area</a>
												</li>
											</ul>
										</li>
										<li><a>Equipamento<span class="fa fa-chevron-down"></span></a>
											<ul class="nav child_menu">
												<li class="sub_menu"><a href="register-equipament-group" title="Cadastrar ou gerenciar Grupo de Equipamento">Grupo de Equipamento</a>
												</li>
												<li><a href="register-equipament-family" title="Cadastrar ou gerenciar Familia de Equipamento">Familia de Equipamento</a>
												</li>
												<li><a href="register-equipament" title="Cadastrar ou gerenciar Equipamento">Equipamento</a>
												</li>
											</ul>
										</li>

										<li><a href="register-user" title="Cadastrar ou gerenciar Colaborador">Colaborador</a></li>
										<li><a href="register-manufacture" title="Cadastrar ou gerenciar Fornecedor">Fornecedor</a></li>
										<li><a href="register-account" title="Cadastrar ou gerenciar Usuario">Usuario</a></li>
										<!--   <li><a href="widgets.php">Widgets</a></li> -->
										<!--   <li><a href="invoice.php">Invoice</a></li> -->
										<!--    <li><a href="inbox.php">Inbox</a></li> -->
										<li><a href="calendar" title="Cadastrar ou gerenciar Datas">Calendario</a></li>
									</ul>
								</li>
								<!--    <li><a><i class="fa fa-search-plus"></i> Consulta <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
								<li><a href="general_elements_consulta.php">Instituição</a></li>
								<li><a href="media_gallery_consulta.php">Localização</a></li>
								<li><a href="typography_consulta.php">Equipamento</a></li>
								<li><a href="icons_consulta.php">Colaborador</a></li>
								<li><a href="glyphicons_Consulta.php">Fornecedor</a></li>
								<!--   <li><a href="widgets.php">Widgets</a></li> -->
								<!--   <li><a href="invoice.php">Invoice</a></li> -->
								<!--    <li><a href="inbox.php">Inbox</a></li> -->

								<!--    </ul>
							</li> -->
							<li><a><i class="fa fa-table"></i> Manutenção Preventiva <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a>Preventiva Predial<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="maintenance-preventive" title="Gerenciar Manutenção Preventiva Aberta">M.P. Aberta</a>
											</li>
											<li><a href="maintenance-preventive-before" title="Gerenciar Manutenção Preventiva Atrasada">M.P. Atrasada</a>	</li>
											<li><a href="maintenance-preventive-close" title="Gerenciar Manutenção Preventiva Fechada">M.P. Fechada</a>	</li>
											<li><a href="maintenance-preventive-print" title="Gerenciar Impressão de Manutenção Preventiva">M.P. Impressão</a>	</li>
											<li><a href="maintenance-preventive-print-register" title="Gerenciar Registro de Impressão de Manutenção Preventiva">M.P. Registro Impressão</a>	</li>
											<li><a href="maintenance-preventive-control" title="Gerenciar Manutenção Preventiva">M.P. Controle </a>	</li>
										</ul>
									</li>
									<li><a>Preventiva Infraestrutura<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="maintenance-preventive-infra" title="Gerenciar Manutenção Preventiva Aberta">M.P. Aberta</a>
											</li>
											<li><a href="maintenance-preventive-before-infra" title="Gerenciar Manutenção Preventiva Atrasada">M.P. Atrasada</a>	</li>
											<li><a href="maintenance-preventive-close-infra" title="Gerenciar Manutenção Preventiva Fechada">M.P. Fechada</a>	</li>
											<li><a href="maintenance-preventive-print-infra" title="Gerenciar Impressão de Manutenção Preventiva">M.P. Impressão</a>	</li>
											<li><a href="maintenance-preventive-print-register-infra" title="Gerenciar Registro de Impressão de Manutenção Preventiva">M.P. Registro Impressão</a>	</li>
											<li><a href="maintenance-preventive-control-infra" title="Gerenciar Manutenção Preventiva">M.P. Controle </a>	</li>
										</ul>
									</li>

									<!--<li><a href="fechamento.php">Fechamento</a></li>
									<li><a href="imprimir.php">Imprimir</a></li> -->
									<li><a href="maintenance-procedures" title="Cadastrar ou gerenciar Procedimento">Procedimento Predial</a></li>
									<li><a href="maintenance-procedures-infra" title="Cadastrar ou gerenciar Procedimento">Procedimento Infraestrutura</a></li>
									<li><a href="maintenance-routine" title="Cadastrar ou gerenciar Rotinas">Rotina Predial</a></li>
									<li><a href="maintenance-routine-infra" title="Cadastrar ou gerenciar Rotinas">Rotina Infraestrutura</a></li>
									<li><a href="maintenance-preventive-control-return" title="Gerenciar Retorno de Manutenção Preventiva">M.P. Retorno </a></li>
									<!--<li><a href="historico.php">Historico</a></li>-->
								</ul>
							</li>
							
									
													<li><a><i class="fa fa-bar-chart-o"></i> Indicador &amp; Relatório <span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
									<li><a href="report-indicator-metas" title="Gerenciamento e visualizar Metas Indicador ">Metas</a></li>
									<li><a>Indicador<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-indicator-mc" title="Gerenciamento e visualizar Indicador Manutenção Corretiva">Manutenção Corretiva</a></li>
											<li><a href="report-indicator-mp" title="Gerenciamento e visualizar Indicador Manutenção Preventiva">Manutenção Preventiva </a>	</li>
											<li><a href="report-indicator-calibration" title="Gerenciamento e visualizar Indicador Calibração">Calibração </a>	</li>
											<li><a href="report-indicator" title="Gerenciamento e visualizar Indicador">Indicadores </a>	</li>
											 </ul>
									</li>
									<li><a>Relatórios<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="report-report-mc" title="Gerenciamento e visualizar Relatório Manutenção Corretiva">Manutenção Corretiva</a></li>
											<li><a href="report-report-mp" title="Gerenciamento e visualizar Relatório Manutenção Preventiva">Manutenção Preventiva </a>	</li>
											<li><a href="report-report-calibration" title="Gerenciamento e visualizar Relatório Calibração">Calibração</a>	</li>
											<li><a href="report-report-equipament" title="Gerenciamento e visualizar Relatório Inventario">Inventario</a>	</li>
											<li><a href="report-report-horaire" title="Gerenciamento e visualizar Relatório Cronograma Preventiva">Cronograma Preventiva</a>	</li>
											<li><a href="report-report-horaire-calibration" title="Gerenciamento e visualizar Relatório Cronograma Calibração">Cronograma Calibração</a>	</li>
											<li><a href="report-report-hierarchy" title="Gerenciamento e visualizar Relatório Diagrama de Setor">Diagrama de Setor</a>	</li>
											<li><a href="report-report-manufacture" title="Gerenciamento e visualizar Relatório  Avaliação de Fornecedor">Avaliação de Fornecedor</a>	</li>
											<li><a href="report-report-tse" title="Gerenciamento e visualizar Relatório Teste de Segurança Elétrica">Teste de Segurança Elétrica</a>	</li>
											
											<li><a href="report-report-purchases" title="Gerenciamento e visualizar Relatório Compras">Compras</a>	</li>
											<li><a href="report-report-obsolescence" title="Gerenciamento e visualizar Relatório Relatório de Avaliação de Equipamento">Relatório de Avaliação de Equipamento</a>	</li>
											<li><a href="report-report-routine" title="Gerenciamento e visualizar Relatório Rotina">Rotina</a>	</li>
											<li><a href="report-report-calibration-report" title="Gerenciamento e visualizar Relatório Laudos de Calibração">Laudos de Calibração</a>	</li>
											<li><a href="register-equipament-family-movement-list" title="Gerenciamento e visualizar Historico de Saida de Equipamento">Historico de Saida de Equipamento</a>	</li>
<li><a href="register-equipament-form" title="Gerenciamento e visualizar Formularios">Formularios</a>	</li>
<li><a href="report-report" title="Gerenciamento e visualizar Relatório">Relatório</a>	</li>


										
										
										</ul>
									</li>

									<!--  <li><a href="morisjs.php">Moris JS</a></li> -->
									<!-- <li><a href="echarts.php">ECharts</a></li> -->
									<!-- <li><a href="other_charts.php">Other Charts</a></li> -->
								</ul>
							</li>
							<li><a><i class="fa fa-cart-arrow-down"></i> Compras <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a>Solicitação<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-acquisition" title="Abertura de Solititação com Ordem de Serviço">Solicitação com O.S</a>
											</li>
											<li><a href="purchases-acquisition-single" title="Abertura de Solititação Sem Ordem de Serviço">Solicitação Sem O.S</a>
											</li>

										</ul>
									</li>
									<li><a>Inventario<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-inventory" title="Gerenciamento e visualizar Entrada de Material">Entrada de Material</a></li>
											<li><a href="purchases-inventory-output" title="Gerenciamento e visualizar Saida de Material">Saida de Material</a></li>
											<li><a href="purchases-inventory-stock" title="Gerenciamento e visualizar Estoque de Material">Estoque de Material</a></li>
											<li><a href="purchases-inventory-manager" title="Gerenciamento e visualizar Parametrização">Gerenciar</a></li>
										</ul>
									</li>

									<li><a>Solicitação<span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu">
											<li class="sub_menu"><a href="purchases-progress" title="Gerenciamento de Solititação em Aberto">Solicitação Aberto</a></li>
											<li><a href="purchases-progress" title="Gerenciamento de Solititação em Aprovação">Solicitação Aprovação</a></li>
											<li><a href="purchases-progress-progress" title="Gerenciamento de Solititação em Andamento">Solicitação Andamento</a></li>
											<li><a href="purchases-progress-concluded" title="Gerenciamento de Solititação em Concluido">Solicitação Concluido</a></li>
										</ul>
									</li>

								</ul>
							</li>
							<!--  <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
							<ul class="nav child_menu">
							<li><a href="fixed_sidebar.php">Fixed Sidebar</a></li>
							<li><a href="fixed_footer.php">Fixed Footer</a></li>
						</ul>
					</li> -->
				</ul>
			</div>
			<div class="menu_section">
				<h3>Calibração</h3>
				<ul class="nav side-menu">
					<li><a><i class="fa fa-bug"></i> Calibração <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a>Laudos<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="calibration-report" title="Gerenciamento de laudos">Lista de Laudos</a></li>
									<li><a href="calibration-report-new-wizard" title="Cadastro de novo laudo">Novo Laudo</a></li>
									
								</ul>
							</li>

							<li><a href="calibration-parameter-equipament" title="Cadastro e gerenciamento de equipamento teste">Analisador &amp; Simulador</a></li>
							<li><a>Parametrização<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li class="sub_menu"><a href="calibration-parameter" title="Cadastro e Gerenciamento de Empresa">Empresa</a></li>
									<li><a href="calibration-parameter-dados" title="Cadastro e gerenciamento de Parametrização de Laudo">Parâmetro Laudo</a></li>
									<li><a href="calibration-parameter-equipament" title="Cadastro e gerenciamento de equipamento teste">Analisador &amp; Simulador</a></li>

								</ul>
							</li>
							
						<!--    <li><a href="contacts.php">Contacts</a></li>
						<li><a href="profile.php">Profile</a></li> -->
					</ul>
				</li>
				

				

</div>
<div class="menu_section">
	<h3>Melhoria contínua  </h3>
	<ul class="nav side-menu">
		<li><a><i class="fa fa-cube"></i> Análise Trimestral <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="pdca-quarterly-analysis" title="Elaboração de Análise Trimestral">Análise</a></li>
				<li><a href="pdca-quarterly-report" title="Relatorio de Análise Trimestral">Relatorio</a></li>
				
				
			</ul>
		</li>
		<li><a><i class="fa fa-database"></i> Contratos <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a href="pdca-contracts" title="Cadastro de Contratos">Contratos</a></li>
				<li><a href="pdca-contracts-configure" title="Gerenciamento de Contratos">Configuracoes</a></li>
				
				
			</ul>
		</li>
	 
		
	</ul>
</div>
					<div class="menu_section">
						<h3>Orçamento  </h3>
						<ul class="nav side-menu">
							
							<li><a><i class="fa fa-calculator"></i> Orçamento <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a href="budget" title="Cadastro e Gerenciamento de Orçamentos">Orçamentos</a></li>
									<li><a href="register-customer" title="Cadastrar ou gerenciar Clientes Orçamento">Clientes Orçamento</a></li>
									 									
									
								</ul>
							</li>
							
							
						</ul>
					</div>
					<div class="menu_section">
						<h3>Planejamento Estratégico</h3>
						<ul class="nav side-menu">
							<li>
								<a href="obsolescence-prevision-provision" title="Cadastro e Gerenciamento de Planejamento Estratégico">
									<i class="fa fa-calendar"></i> Planejamento Estratégico <span class=""></span>
								</a>
							</li>
							
							
						</ul>
					</div>
					 
					<div class="menu_section">
						<h3>Depreciação</h3>
						<ul class="nav side-menu">
							<li>
								<a href="obsolescence-depreciation" title="Cadastro e Gerenciamento de Planejamento Depreciação">
									<i class="fa fa-line-chart"></i> Planejamento Depreciação <span class=""></span>
								</a>
							</li>
							
							
						</ul>
					</div>
					 
					<div class="menu_section">
						<h3>Logística  </h3>
						<ul class="nav side-menu">
							 
							<li><a><i class="fa fa-car"></i> Transporte <span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu">
									<li><a href="logic-car" title="Cadastro de Solicitação">Solicitação</a></li>
									<li><a href="logic-car-calendar" title="Gerenciamento de agenda">Agenda</a></li>
									<li><a href="logic-car-search" title="Consulta de agenda">Consulta Agenda</a></li>
									
									
								</ul>
							</li>
							
							
						</ul>
					</div>




<div class="menu_section">
	<h3>Utilidades &amp; Recursos</h3>
	<ul class="nav side-menu">

<?php if($t1 == "0"){  ?>
		<li><a><i class="fa fa-bolt"></i> Energia Elétrica <span class="fa fa-chevron-down"></span></a>				<ul class="nav child_menu">
					<li><a>Unidade Consumidora<span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li class="sub_menu"><a href="utilities-energy-unit-search" title="Consulta de Unidade Consumidora">Consulta</a></li>
							<li><a href="utilities-energy-unit-register" title="Cadastro de Unidade Consumidora">Cadastro</a></li>
							
						</ul>
					</li>
					<li><a>Fatura<span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li class="sub_menu"><a href="utilities-energy-bill-search" title="Consulta de Faturas">Consulta</a></li>
							<li><a href="utilities-energy-bill-register" title="Lançamento de nova fatura">Lançamento</a></li>
							
						</ul>
					</li>
                    <li><a href="utilities-energy-report" title="Relatorio de  Energia Elétrica">Relatorios</a></li>
					<li><a href="utilities-energy-support" title="Contingência de  Energia Elétrica">Contingência</a></li>
					<li><a href="utilities-energy-alert" title="Alertas de  Energia Elétrica">Alertas</a></li>
					
					
					
				</ul>
			</li>
			<?php }  ?>
			<?php if($t2 == "0"){  ?>
			<li><a><i class="fa fa-tint"></i> Sistemas de Água <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Unidades<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-water-unit-search" title="Consulta de Unidade Água">Consulta</a></li>
						<li><a href="utilities-water-unit-register" title="Cadastro de Unidade Água">Cadastro</a></li>
						
					</ul>
				</li>
				<li><a>Fatura<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-water-bill-search" title="Consulta de Faturas">Consulta</a></li>
						<li><a href="utilities-water-bill-register" title="Lançamento de nova fatura">Lançamento</a></li>
						
					</ul>
				</li>
			
				<li><a href="utilities-water-report" title="Relatorio de  Sistemas de Água ">Relatorios</a></li>
				<li><a href="utilities-water-support" title="Contingência de  Sistemas de Água ">Contingência</a></li>
				<li><a href="utilities-water-alert" title="Alertas de Sistemas de Água">Alertas</a></li>
				
				
 			</ul>
		</li>
					<?php }  ?>
		<?php if($t3 == "0"){  ?>
					<li><a><i class="fa fa-fire"></i> Gás GLP <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Instalação<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-gas-glp-unit-search" title="Consulta de Instalação Gás GLP">Consulta</a></li>
						<li><a href="utilities-gas-glp-unit-register" title="Cadastro de Instalação Gás GLP">Cadastro</a></li>
						
					</ul>
				</li>
				
				<li><a>Abastecimento<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-gas-glp-supply-search" title="Consulta de Abastecimento">Consulta</a></li>
						<li><a href="utilities-gas-glp-supply-register" title="Lançamento de nova Abastecimento">Lançamento</a></li>
						
					</ul>
				</li>
				<li><a href="utilities-gas-glp-report" title="Relatorio de Gás GLP ">Relatorios</a></li>
				<li><a href="utilities-gas-glp-support" title="Contingência de Gás GLP ">Contingência</a></li>
				<li><a href="utilities-gas-glp-alert" title="Alertas de Gás GLP">Alertas</a></li>
				
				
			</ul>
		</li>
					<?php }  ?>
		<?php if($t4 == "0"){  ?>
					<li><a><i class="fa fa-heartbeat"></i> Gás Medicinal <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Instalação<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-gas-medicinal-unit-search" title="Consulta de Instalação Gás Medicinal">Consulta</a></li>
						<li><a href="utilities-gas-medicinal-unit-register" title="Cadastro de Instalação Gás Medicinal">Cadastro</a></li>
						
					</ul>
				</li>
				
				<li><a>Abastecimento<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-gas-medicinal-supply-search" title="Consulta de Abastecimento">Consulta</a></li>
						<li><a href="utilities-gas-medicinal-supply-register" title="Lançamento de nova Abastecimento">Lançamento</a></li>
						
					</ul>
				</li>
				
				<li><a href="utilities-gas-medicinal-report" title="Relatorio de Gás Medicinal ">Relatorios</a></li>
				<li><a href="utilities-gas-medicinal-support" title="Contingência de Gás Medicinal ">Contingência</a></li>
				<li><a href="utilities-gas-medicinal-alert" title="Alertas de Gás Medicinal">Alertas</a></li>
				
				
			</ul>
		</li>
						<?php }  ?>
		<?php if($t5 == "0"){  ?>
						<li><a><i class="fa fa-heart"></i> Gás Natural <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Instalação<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-gas-natural-unit-search" title="Consulta de Instalação Gás Natural">Consulta</a></li>
						<li><a href="utilities-gas-natural-unit-register" title="Cadastro de Instalação Gás Natural">Cadastro</a></li>
						
					</ul>
				</li>
				
				<li><a>Abastecimento<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-gas-natural-supply-search" title="Consulta de Abastecimento">Consulta</a></li>
						<li><a href="utilities-gas-natural-supply-register" title="Lançamento de nova Abastecimento">Lançamento</a></li>
						
					</ul>
				</li>
				
				<li><a href="utilities-gas-natural-report" title="Relatorio de Gás Natural ">Relatorios</a></li>
				<li><a href="utilities-gas-natural-support" title="Contingência de Gás Natural ">Contingência</a></li>
				<li><a href="utilities-gas-natural-alert" title="Alertas de Gás Natural">Alertas</a></li>
				
				
			</ul>
		</li>	
							
								<?php }  ?>
		<?php if($t6 == "0"){  ?>	
								<li><a><i class="fa fa-headphones"></i> Telefonia <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Central<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-telefonia-unit-search" title="Consulta de Central">Consulta</a></li>
						<li><a href="utilities-telefonia-unit-register" title="Cadastro de Central">Cadastro</a></li>
						
					</ul>
				</li>
				<li><a>Fatura<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-telefonia-bill-search" title="Consulta de Faturas">Consulta</a></li>
						<li><a href="utilities-telefonia-bill-register" title="Lançamento de nova fatura">Lançamento</a></li>
						
					</ul>
				</li>
			
				<li><a href="utilities-telefonia-report" title="Relatorio de Telefonia">Relatorios</a></li>
				<li><a href="utilities-telefonia-support" title="Contingência de Telefonia">Contingência</a></li>
				<li><a href="utilities-telefonia-alert" title="Alertas de Telefonia">Alertas</a></li>
				
				
				
				
			</ul>
		</li>
									<?php }  ?>
		<?php if($t7 == "0"){  ?>
									<li><a><i class="fa fa-heart-o"></i> Diesel <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Instalação<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-disel-unit-search" title="Consulta de Instalação Diesel">Consulta</a></li>
						<li><a href="utilities-disel-unit-register" title="Cadastro de Instalação Diesel">Cadastro</a></li>
						
					</ul>
				</li>
				
				<li><a>Abastecimento<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-disel-supply-search" title="Consulta de Abastecimento">Consulta</a></li>
						<li><a href="utilities-disel-supply-register" title="Lançamento de nova Abastecimento">Lançamento</a></li>
						
					</ul>
				</li>
				<li><a href="utilities-disel-report" title="Relatorio de Disel ">Relatorios</a></li>
				<li><a href="utilities-disel-support" title="Contingência de Disel ">Contingência</a></li>
				<li><a href="utilities-disel-alert" title="Alertas de Disel">Alertas</a></li>
				
				
			</ul>
		</li>
										
											<?php }  ?>
		<?php if($t8 == "0"){  ?>
		
											<li><a data-toggle="tooltip" data-placement="top" title="Gerenciamento de Gerador"><i class="fa fa-cog"></i> Gerador <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Gerador<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-seee-unit-search" title="Consulta de Gerador">Consulta</a></li>
						<li><a href="utilities-seee-unit-register" title="Cadastro de Gerador">Cadastro</a></li>
						
					</ul>
				</li>
				
				<li><a>Produção<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-seee-supply-search" title="Consulta de Produção">Consulta</a></li>
						<li><a href="utilities-seee-supply-register" title="Lançamento de nova Produção">Produção</a></li>
						
					</ul>
				</li>
				<li><a href="utilities-seee-report" title="Relatorio de Sistema de Energia Elétrica de Emergência ">Relatorios</a></li>
				<li><a href="utilities-seee-support" title="Contingência de Sistema de Energia Elétrica de Emergência ">Contingência</a></li>
				<li><a href="utilities-seee-alert" title="Alertas de Sistema de Energia Elétrica de Emergência">Alertas</a></li>
				
				
			</ul>
		</li>	
		<?php }  ?>
		<?php if($t9 == "0"){  ?>
		<li><a data-toggle="tooltip" data-placement="top" title="Gerenciamento de Nobreak"><i class="fa fa-cog"></i> Nobreak <span class="fa fa-chevron-down"></span></a>

			<ul class="nav child_menu">
				<li><a>Nobreak<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-seei-unit-search" title="Consulta de Nobreak">Consulta</a></li>
						<li><a href="utilities-seei-unit-register" title="Cadastro de Nobreak">Cadastro</a></li>
						
					</ul>
				</li>
				
				<li><a>Bateria<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-seei-supply-search" title="Consulta de Bateria">Consulta</a></li>
						<li><a href="utilities-seei-supply-register" title="Lançamento de nova Bateria">Bateria</a></li>
						
					</ul>
				</li>
				<li><a href="utilities-seei-report" title="Relatorio de Sistema de Energia Elétrica Ininterrupto ">Relatorios</a></li>
				<li><a href="utilities-seei-support" title="Contingência de Sistema de Energia Elétrica Ininterrupto ">Contingência</a></li>
					<li><a href="utilities-seei-alert" title="Alertas de Sistema de Energia Elétrica Ininterrupto">Alertas</a></li>
				
				
			</ul>
		</li>	
		<?php }  ?>
		<?php if($t10 == "0"){  ?>
		<li><a><i class="fa fa-sun-o"></i> Sistema Fotovoltaica <span class="fa fa-chevron-down"></span></a>			<ul class="nav child_menu">
				<li><a>Gerador<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-solar-unit-search" title="Consulta de Instalação Sistema Fotovoltaica">Consulta</a></li>
						<li><a href="utilities-solar-unit-register" title="Cadastro de Instalação Sistema Fotovoltaica">Cadastro</a></li>
						
					</ul>
				</li>
				
				<li><a>Produção<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-solar-supply-search" title="Consulta de Produção">Consulta</a></li>
						<li><a href="utilities-solar-supply-register" title="Lançamento de nova Produção">Lançamento</a></li>
						
					</ul>
				</li>
				<li><a href="utilities-solar-report" title="Relatorio de Sistema Fotovoltaica ">Relatorios</a></li>
				<li><a href="utilities-solar-support" title="Contingência de Sistema Fotovoltaica ">Contingência</a></li>
				<li><a href="utilities-solar-alert" title="Alertas de Sistema Fotovoltaica">Alertas</a></li>
				
				
			</ul>
		</li>
										
													
																
																
																		<?php }  ?>
		<?php if($t11 == "0"){  ?>
																		<li><a data-toggle="tooltip" data-placement="top" title="Plano de manutenção, operação e controle"><i class="fa fa-ge"></i> PMOC <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
				<li><a>Laudos<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-pmoc-report-report" title="Consulta de Gerenciamento de Laudos PMOC">Laudos</a></li>
						<li><a href="utilities-pmoc-report-type" title="Consulta e Gerenciamento de Tipos de Laudos PMOC">Tipo de Laudo</a></li>
						<li><a href="utilities-pmoc-report-pdca" title="Consulta e Gerenciamento de Plano de Ação PMOC">Plano de Ação</a></li>
						<li><a href="utilities-pmoc-report-reports" title="Consulta e Gerenciamento de Relatorio PMOC">Relatorio</a></li>
						
					</ul>
				</li>
				<li><a>Cadastro<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-pmoc-register-unit" title="Consulta e Gerenciamento de Unidades PMOC">Unidade</a></li>
						<li><a href="utilities-pmoc-register-room" title="Consulta e Gerenciamento de Ambiente Climatizado PMOC">Ambiente Climatizado</a></li>
						<li><a href="utilities-pmoc-register-tecnic" title="Consulta e Gerenciamento de Responsável Tecnico PMOC">Responsável Tecnico</a></li>
						<li><a href="utilities-pmoc-register-oner" title="Consulta e Gerenciamento de Proprietario PMOC">Proprietario</a></li>
						
					</ul>
				</li>
				<li><a>Relatorio<span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">
						<li class="sub_menu"><a href="utilities-pmoc-pmoc-report" title="Relatorio PMOC">PMOC</a></li>
						<li><a href="utilities-pmoc-pmoc-mc" title="Relatorio Manutenção Corretiva">Manutenção Corretiva</a></li>
						<li><a href="utilities-pmoc-pmoc-mp" title="Relatorio Manutenção Preventiva">Manutenção Preventiva</a></li>
						<li><a href="utilities-pmoc-pmoc-timeline" title="Relatorio Cronograma">Cronograma</a></li>
						<li><a href="utilities-pmoc-pmoc-routine" title="Relatorio Rotinas">Rotinas</a></li>
						<li><a href="utilities-pmoc-pmoc-inventory" title="Relatorio Iventario">Iventario</a></li>
						
					</ul>
				</li>
				
				 
			 		 
						 
						
					</ul>
				</li>
				<?php }  ?>
				
			</ul>
		</li>
			


																				<!--    <li><a href="contacts.php">Contacts</a></li>
																				<li><a href="profile.php">Profile</a></li> -->


																				<!--      <li><a href="plain_page.php">Plain Page</a></li>
																				<li><a href="login.php">Login Page</a></li>
																				<li><a href="pricing_tables.php">Pricing Tables</a></li> -->
																			</ul>
																		</li>
																		<!--      <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
																		<ul class="nav child_menu">
																		<li><a href="#level1_1">Level One</a>
																		<li><a>Level One<span class="fa fa-chevron-down"></span></a>
																		<ul class="nav child_menu">
																		<li class="sub_menu"><a href="level2.php">Level Two</a>
																	</li>
																	<li><a href="#level2_1">Level Two</a>
																</li>
																<li><a href="#level2_2">Level Two</a>
															</li>
														</ul>
													</li>
													<li><a href="#level1_2">Level One</a>
												</li>
											</ul>
										</li>
										<li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
									</ul> -->

								</div>


								<!--    <div class="menu_section">
								<h3>Manuais &amp; ADMINISTRATIVO</h3>
								<ul class="nav side-menu">
								<li><a href="../contratosphp"target="_blank"><i class="fa fa-pencil-square-o"></i> Manuais Usuario </span></a>
								<!--    <ul class="nav child_menu">
								<li><a href="e_commerce.php">Novo</a></li>
								<li><a href="projects.php">Contratos</a></li>
								<li><a href="project_detail.php">Gestao</a></li>
								<li><a href="contacts.php">Contacts</a></li>
								<li><a href="profile.php">Profile</a></li>
							</ul> -->
							<!--       </li>
							<li><a  href="../sigcon"target="_blank"><i class="fa fa-check-square-o"></i> Manuais Tecnico </span></a>
							<!--    <ul class="nav child_menu">
							<li><a href="e_commerce.php">novo</a></li>
							<li><a href="projects.php">Formularios</a></li>
							<!--    <li><a href="contacts.php">Contacts</a></li>
							<li><a href="profile.php">Profile</a></li>
						</ul> -->
						<!--        </li>
						<li><a href="../geradoc"target="_blank"><i class="fa fa-dropbox"></i> Dropzone </span></a>
						<!--    <ul class="nav child_menu">
						<li><a href="e_commerce.php">Arquivos</a></li>
						<li><a href="projects.php">Documentos</a></li>
						<li><a href="contacts.php">Contacts</a></li>
						<li><a href="profile.php">Profile</a></li>
					</ul>-->
					<!--      </li>
					
					<!--    <ul class="nav child_menu">
					<li><a href="e_commerce.php">Arquivos</a></li>
					<li><a href="projects.php">Documentos</a></li>
					<li><a href="contacts.php">Contacts</a></li>
					<li><a href="profile.php">Profile</a></li>
				</ul>
			</li>

		</div> -->

	</div>
<!-- /sidebar menu -->
<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
	<a data-toggle="tooltip" data-placement="top" title="Perfil" href="profile">
		<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="Maximizar"id="goFS" >

		<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
		<script>
		var goFS = document.getElementById("goFS");
		goFS.addEventListener("click", function() {
			document.body.requestFullscreen();
		}, false);
		</script>

	</a>
	<a data-toggle="tooltip" data-placement="top" title="Bloquear"  href="lockscreen">
		<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="Logout" href="login">
		<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
	</a>
	<!--     <a data-toggle="tooltip" data-placement="top" title="Contrato" href="http://contrato.mksistemasbiomedicos.com.br/"target="_blank">
	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
</a>
<a data-toggle="tooltip" data-placement="top" title="Formulario" href="http://licitacao.mksistemasbiomedicos.com.br/"target="_blank">
<span class="glyphicon glyphicon-check" aria-hidden="true"></span>
</a>
<a data-toggle="tooltip" data-placement="top" title="Arquivo" href="http://file.mksistemasbiomedicos.com.br/"target="_blank">
<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
</a> -->
 
 
 
 
<!--       <a data-toggle="tooltip" data-placement="top" title="Projetos" href="http://www.project.mksistemasbiomedicos.com.br/"target="_blank">
<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
</a> -->

</div>
<!-- /menu footer buttons -->
</div>
</div>
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                        <img src="logo/img.jpg" alt=""><?php printf($usuariologado) ?>
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="profile">   <i class="glyphicon glyphicon-cog pull-right" aria-hidden="true"></i> Perfil </a>
                        <a class="dropdown-item"  href="documentation">   <i class="glyphicon glyphicon-file pull-right" aria-hidden="true"></i> Documentos </a>
                    <!--    <a class="dropdown-item"  href="javascript:;">
                          <span class="badge bg-red pull-right">100%</span>
                          <span>Settings</span>
                        </a>
              <!--      <a class="dropdown-item"  href="javascript:;">Help</a> -->
                           <a class="dropdown-item"  href="login"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>
				 


               <li role="presentation" class="nav-item dropdown open">
					<a  class="info-number" data-toggle="dropdown" aria-expanded="false" >
						<i class=""></i>
						
					</a>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<a href="documentation-bell" class="info-number" title="Novas Notificações">
						<i class="fa fa-bell-o"></i>
						<span class="badge badge-dark"><?php include 'api/api-bell-notification.php'; ?></span>
					</a>
				</li>
				 
				<li role="presentation" class="nav-item dropdown open">
					<a  class="info-number" data-toggle="dropdown" aria-expanded="false" >
						<i class=""></i>
						
					</a>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<a href="os-integration" class="info-number" title="Novas Ordem de Serviço de Integração">
						<i class="fa fa-code-fork"></i>
					<span class="badge bg-blue"> <?php include 'api/api-os-integration.php';?> </span>
					</a>
				</li> 				

				<li role="presentation" class="nav-item dropdown open">
					<a  class="info-number" data-toggle="dropdown" aria-expanded="false" >
						<i class=""></i>
						
					</a>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<a href="os-opened" class="info-number" title="Novas Ordem de Serviços">
						<i class="fa fa-envelope-o"></i>
						<span class="badge bg-green"> <?php include 'workflow/envelope.php';?> </span>
					</a>
				</li>	 
				<li role="presentation" class="nav-item dropdown open">
					<a  class="info-number" data-toggle="dropdown" aria-expanded="false" >
						<i class=""></i>
						
					</a>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<a href="os-signature" class="info-number" title="Novas Assinaturas de Ordem de Serviços">
						<i class="fa fa-envelope-o"></i>
					<span class="badge bg-red"> <?php include 'workflow/envelope-signature.php';?> </span>
					</a>
				</li>
					<li role="presentation" class="nav-item dropdown open">
					<a  class="info-number" data-toggle="dropdown" aria-expanded="false" >
						<i class=""></i>
						
					</a>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<a id="compose" class="dropdown-toggle info-number" title="Alerta de Recusros & Utilidades">
						<i class="fa  fa-exclamation-triangle"></i>
						<span class="badge bg-orange"> <?php include 'api/api-utilities.php';?> </span>
					</a>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<a  class="info-number" data-toggle="dropdown" aria-expanded="false" >
						<i class=""></i>
						
					</a>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<a href="logic-car" class="info-number" title="Novas Solicitações de transporte">
						<i class="fa fa-car"></i>
						<span class="badge bg-orange"> <?php include 'api/api-logic-car.php';?> </span>
					</a>
				</li> 	

                    
                     
                    <!--
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image">  <img src="../logo/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <div class="text-center">
                          <a class="dropdown-item">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li> -->
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>

        <!-- /top navigation -->
        <!-- page content -->
<?php include 'api.php';?>   
<?php include 'frontend/utilities-seee-alert-frontend.php';?>
        
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           ©2020 All Rights Reserved. MK Sistemas. Privacy and Terms<a href="https://mksistemasbiomedicos.com.br"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
					<div class="compose col-md-6  ">
						<div class="compose-header">
							
							Lista de Alertas
							<button type="button" class="close compose-close">
								<span>×</span>
							</button>
						</div>
						<div class="compose-body">
							<div id="alerts"></div>
							<div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
								
							
								
							</div>
						<h3><?php include 'api/api-utilities-list.php';?></h3>
						</div>
						<div class="compose-footer">
							
						</div>
					</div>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery
    <script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../../framework/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../framework/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../framework/build/js/custom.min.js"></script>




    <!-- jQuery-->
    <script src="../../framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap
    <script src="../../framework/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../../framework/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../../framework/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../../framework/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../../framework/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../../framework/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../../framework/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../../framework/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../../framework/vendors/Flot/jquery.flot.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.time.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../../framework/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../../framework/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../../framework/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../../framework/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../../framework/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../../framework/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../../framework/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../../framework/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../../framework/build/js/custom.min.js"></script>
 <!-- PNotify -->
   <script src="../../framework/vendors/pnotify/dist/pnotify.js"></script>
   <script src="../../framework/vendors/pnotify/dist/pnotify.buttons.js"></script>
   <script src="../../framework/vendors/pnotify/dist/pnotify.nonblock.js"></script>
   
   <script src="../../framework/assets/js/select2.min.js"></script>
   
      <!-- bootstrap-wysiwyg -->
    <script src="../../framework/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../../framework/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../../framework/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../../framework/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../../framework/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../../framework/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../../framework/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../../framework/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../../framework/vendors/starrr/dist/starrr.js"></script>
   <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
     <!-- Datatables -->
    <script src="../../framework/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../framework/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../framework/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../../framework/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../../framework/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../../framework/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../../framework/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../../framework/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../../framework/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../../framework/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../../framework/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../../framework/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../../framework/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../../framework/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../../framework/vendors/pdfmake/build/vfs_fonts.js"></script>
 <!-- jQuery Knob -->
    <script src="../../framework/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- bootstrap-daterangepicker -->
    <script src="../../framework/vendors/moment/min/moment.min.js"></script>
    <script src="../../framework/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="../../framework/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- duall list box -->  
 <script hrf="../../framework/duallistbox/scr/bootstrap-duallistbox.css"></script>
  <script src="../../framework/duallistbox/scr/jquery.bootstrap-duallistbox.js"></script>
  
   <script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.css"></script>
  <script hrf="../../framework/duallistbox/dist/bootstrap-duallistbox.min.css"></script>
   <script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.js"></script>
  <script src="../../framework/duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
    <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

	</body>
  <script language="JavaScript">
 
 history.pushState(null, null, document.URL);
 window.addEventListener('popstate', function () {
     history.pushState(null, null, document.URL);
 });
 </script>
<script type="text/javascript">

  // Total seconds to wait
  var seconds = 1080;
  var timer;
  var interval;

  function countdown() {

      seconds--;
    if (seconds < 0) {
		clearInterval(interval);
      // Change your redirection link here
      window.location = "https://seth.mksistemasbiomedicos.com.br";
    } else if (seconds === 60) {
      // Show warning 60 seconds before redirect
      var warning = document.createElement("div");
      warning.innerHTML = "O sistema será desconectado por motivos de segurança em 60 segundos. Clique aqui para continuar no sistema e reiniciar o tempo.";
      warning.style.backgroundColor = "yellow";
      warning.style.padding = "10px";
      warning.style.position = "fixed";
      warning.style.bottom = "0";
      warning.style.right = "0";
      warning.style.zIndex = "999";
      warning.addEventListener("click", function() {
        warning.style.display = "none";
        clearTimeout(seconds);
		seconds = 1080;
        countdown();
      });
      document.body.appendChild(warning);
	  countdown();
    } else {
      // Update remaining seconds
      document.getElementById("countdown").innerHTML = seconds;
      // Count down using javascript
	
      timer = setTimeout(countdown, 1000);
    }
  }

  // Run countdown function
  countdown();
  
  
  function stopCountdown() {
    clearInterval(seconds);
  }
 
    // Reset countdown on user interaction
	document.addEventListener("click", function() {
    clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });
  document.addEventListener("mousemove", function() {
	seconds--;
    //clearTimeout(seconds);
	 seconds = 1080;
  //  countdown();
  });

</script>

 <script  type="text/javascript">
   $(function () {
                $('#myDatepicker').datetimepicker();
            });
    
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm '
    });
     $('#myDatepicker8').datetimepicker({
        format: 'hh:mm '
    });
    
    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();
    
    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });
    
    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    
    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
    
</script>
<script>
  var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox();
  $("#demoform").submit(function() {
    alert($('[name="duallistbox_demo1[]"]').val());
    return false;
  });
</script>
<script>
  var demo1 = $('select[name="instituicao_id"]').bootstrapDualListbox();
  $("#demoform").submit(function() {
    alert($('[name="instituicao_id"]').val());
    return false;
  });
</script>


 </html>
