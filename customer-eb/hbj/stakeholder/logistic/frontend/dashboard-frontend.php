  <div class="right_col" role="main">
          <div class="">
            <div class="row" style="display: inline-block;">
            <div class="top_tiles">

              <div class="alert alert-block alert-success">
                <button type="button" class="close" data-dismiss="alert">
                  <i class="ace-icon fa fa-times"></i>
                </button>

                <i class="ace-icon fa fa-check red"></i>

                Bem vindo ao
                <strong class="black">
                  SETH
                  <small>(v1.4)</small>
                </strong>,
sistema de Engenharia Clinica Tecnica Hospitalar <a href="https://www.mksistemasbiomedicos.com.br">MK Sistemas Biomédicos</a> .
              </div>
             
                 
                  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                    google.charts.load("current", { packages: ["calendar"] });
                    google.charts.setOnLoadCallback(drawChart);
                    
                    function drawChart() {
                      // Buscar dados do endpoint PHP
                      fetch("api_endpoint.php") // Substitua pelo caminho correto
                      .then((response) => response.json())
                      .then((data) => {
                        // Criar a DataTable para o gráfico
                        var dataTable = new google.visualization.DataTable();
                        dataTable.addColumn({ type: "date", id: "Date" });
                        dataTable.addColumn({ type: "number", id: "Event Count" });
                        dataTable.addColumn({ type: "string", role: "tooltip" }); // Adiciona o tooltip
                        
                        // Processar os dados para o formato necessário
                        const rows = data.map((item) => [
                          new Date(item.date), // Data formatada como objeto Date
                          item.count,          // Número de eventos no dia
                          item.titles.join(", "), // Títulos concatenados para o tooltip
                        ]);

                        
                        dataTable.addRows(rows);
                        
                        // Configurar o gráfico
                        var options = {
                          title: "Eventos",
                          height: 350,
                          tooltip: { isHtml: true }, // Permitir tooltips personalizados
                        };                        
                        var chart = new google.visualization.Calendar(
                          document.getElementById("calendar_basic")
                        );
                        
                        // Renderizar o gráfico
                        chart.draw(dataTable, options);
                      })
                      .catch((error) =>
                        console.error("Erro ao carregar os dados do calendário:", error)
                      );
                    }
                  </script>
               
                  <div id="calendar_basic" style="width: 1000px; height: 350px;"></div>
 
              
            </div>
            
            <div class="row" style="display: block;">
          <div class="col-md-5 col-sm-5  ">
                         <div class="x_panel">
                           <div class="x_title">
                             <h2>Ultima Atualização<small> de</small> Calendario</h2>
                             <ul class="nav navbar-right panel_toolbox">
                               <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                               </li>
                               <li class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                     <a class="dropdown-item" href="#">Settings 1</a>
                                     <a class="dropdown-item" href="#">Settings 2</a>
                                   </div>
                               </li>
                               <li><a class="close-link"><i class="fa fa-close"></i></a>
                               </li>
                             </ul>
                             <div class="clearfix"></div>
                           </div>
                           <div class="x_content">

                             <table class="table table-striped">
                               <thead>
                                 <tr>
                                   <th class="column-title">#</th>
                                                                      <th class="column-title">Evento</th>

                                   <th class="column-title">Data</th>

                                  </tr>
                               </thead>
                               <tbody>
                                 <?php   
                                  $query = "SELECT title, start
FROM events_logistica
WHERE DATE(start) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY);
";
          //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
          if ($stmt = $conn->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($title,$start);

                                 while ($stmt->fetch()) { ?>
                               <tr>
                                 <th class="column-title" scope="row"></th>
                                 <th class="column-title"> <?php printf($title); ?></th>
                                  <th class="column-title"> <?php printf($start); ?></th>

 

                                 </tr>
                               <?php
                            }}
                              ?>


                               </tbody>
                             </table>

                           </div>
                         </div>
                       </div>
            
            
            
          </div>
    
 
              </div>
              <!-- end of weather widget -->
              
            </div>
            
            
         
        <!-- /page content -->
        