<?php
  include("../../database/database.php");
  session_start();
  $setor = $_SESSION['setor'] ;
  $usuario = $_SESSION['id_usuario'];
$query = "SELECT os.id_type,regdate_os_posicionamento.data_end,os.sla_register,os.sla_pm,os.pm,regdate_os_dropzone.file, custom_service.nome,category.nome,equipamento_familia.fabricante,os_sla.sla_verde,os_sla.sla_amarelo,os.sla_term,os_grupo_sla.grupo,os_posicionamento.cor,os_prioridade.prioridade,os.id, os.id_solicitacao,os.id_anexo,os.reg_date,os.upgrade,instituicao.instituicao,usuario.nome,usuario.sobrenome,instituicao_localizacao.nome, instituicao_area.nome, os_status.status, os_posicionamento.status,os_carimbo.carimbo,os_workflow.workflow, equipamento.codigo,equipamento.patrimonio,equipamento.serie, equipamento_familia.nome, equipamento_familia.modelo,os.assessment,os.integration,os.id_integration FROM os LEFT JOIN regdate_os_posicionamento ON regdate_os_posicionamento.id_os = os.id LEFT JOIN instituicao ON instituicao.id = os.id_unidade LEFT JOIN usuario on usuario.id = os.id_usuario LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = os.id_setor LEFT JOIN instituicao_area on instituicao_area.id = instituicao_localizacao.id_area LEFT JOIN os_status on os_status.id = os.id_status LEFT JOIN os_posicionamento on os_posicionamento.id = os.id_posicionamento LEFT JOIN os_carimbo ON os_carimbo.id = os.id_carimbo LEFT JOIN os_workflow on os_workflow.id = os.id_workflow LEFT JOIN equipamento on equipamento.id = os.id_equipamento LEFT JOIN equipamento_familia on equipamento_familia.id = equipamento.id_equipamento_familia LEFT JOIN os_prioridade on os_prioridade.id = os.id_prioridade LEFT JOIN os_sla on os_sla.id = os.id_sla LEFT JOIN os_grupo_sla on os_grupo_sla.id = os_sla.os_grupo_sla LEFT JOIN custom_service on custom_service.id = os.id_servico LEFT JOIN category on category.id = os.id_category LEFT JOIN regdate_os_dropzone ON regdate_os_dropzone.id_os = os.id WHERE os.id_status like '5' and os.id_usuario in ($usuario)  GROUP BY os.id DESC,os.id DESC";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
$stmt->bind_result($id_type,$regdate_os_posicionamento,$sla_register,$sla_pm,$pm,$id_file,$custom_service,$category,$equipamento_fabricante,$sla_verde,$sla_amarelo,$sla_term,$grupo,$color,$prioridade,$id_os,$id_solicitacao,$id_anexo,$reg_date,$upgrade,$id_instituicao,$id_usuario_nome,$id_usuario_sobrenome,$id_localizacao,$id_area,$id_status,$id_posicionamento,$id_carimbo,$id_workflow,$equipamento_codigo,$equipamento_patrimonio,$equipamento_numeroserie,$equipamento_nome,$equipamento_modelo,$assessment,$integration,$id_integration);
 

?>


        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ordem de Serviço</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
              <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                      <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
        
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Solicitações <small> de Ordem de Serviço</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                       

				

						

     
<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                        <th>#</th>
                          <th>OS</th>
                          <th>Solicitante</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>N/S</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          <th>Solicitação</th>
                          <th>Categoria</th>
                          <th>Serviço</th>

                          <th>Registro</th>
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {  
                              $row=$row+1;
                            ?>
                        <tr>
                        <td> <?php printf($row); ?> </td>
                        <td><?php printf($id_os); ?> </td>
                          <td><?php printf($id_usuario_nome); ?> <?php printf($id_usuario_nome); ?></td>
                          <td><?php printf($equipamento_nome); ?> </td>
                          <td> <?php printf($equipamento_modelo); ?></td>
                          <td> <?php printf($equipamento_fabricante); ?></td>
                           <td><?php printf($equipamento_codigo); ?> </td>
                           <td><?php printf($equipamento_numeroserie); ?> </td>
                           <td><?php printf($id_instituicao); ?></td>
                           <td><?php printf($id_area); ?></td>
                           <td>  <?php printf($id_localizacao); ?></td>

                          <td><?php printf($id_solicitacao); ?></td>

                          <td><?php printf($category); ?></td>

                          <td><?php printf($custom_service); ?></td>
                          <td><?php printf($reg_date); ?></td>
                          <td> 
                           
                
           <a class="btn btn-app"  href="os-signature-signature?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Assinatura',
																text: 'Assinar de O.S',
																type: 'success',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-pencil"></i> Assinar
                  </a>
                  
                
                  <a  class="btn btn-app" href="../../os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                 
                   
                                   </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


							</div>
						</div>
					</div>
				</div>
					</div>


             </div>
           
    <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    },
        initComplete: function () {
            this.api()
                .columns([1,2,3,4,5,6,7,8,9,10,12,13])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
                    var container = $('<div class="filter-title"></div>')
                                    .appendTo('#userstable_filter');
                    var label = $('<label>'+theadname+': </label>')
                                .appendTo(container); 
                    var select = $('<select  class="form-control my-1"><option value="">' +
        theadname +'</option></select>')
                        .appendTo( '#userstable_filter'  ).select2()
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
 
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });

                
        },
    });
});


</script>

  
 
  
   




