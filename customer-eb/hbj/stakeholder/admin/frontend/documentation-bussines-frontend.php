<?php

include("../../database/database.php");


//$con->close();


?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Empresa <small></small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            

            

               <div class="x_panel">
                <div class="x_title">
                  <h2>Documentos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <?php

$query = "SELECT  documentation_bussines.id, documentation_bussines.nome, documentation_bussines.data_now,documentation_bussines.data_before,  MAX(regdate_bussines_dropzone.file) FROM documentation_bussines LEFT JOIN  regdate_bussines_dropzone ON  regdate_bussines_dropzone.id_bussines = documentation_bussines.id  WHERE documentation_bussines.view = 0 and  documentation_bussines.id  GROUP BY  regdate_bussines_dropzone.id_bussines   DESC   ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id, $nome,$data_now,$data_before,$dropzone);


?>

<div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Emissão</th>
                          <th>Validade</th>
                          
                           <th>Ação</th>
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>

                          <td><?php printf($nome); ?> </td>
  
                          <td><?php printf($data_now); ?> </td>
                          <td><?php printf($data_before); ?></td>
                            

                          <td>



                          <?php if($dropzone==!""){?>
                   <a class="btn btn-app"   href="../../dropzone/bussines/<?php printf($dropzone); ?> " target="_blank"  onclick="new PNotify({
                                title: 'Visualizar',
                                text: 'Visualizar',
                                type: 'info',
                                styling: 'bootstrap3'
                            });">
                    <i class="fa fa-file"></i> Download
                  </a>
                  <?php  }  ?>
                  
               
                
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

                  
                </div>
              </div>
           



                </div>
              </div>

               

        <!-- /page content -->
