<?php
// Conexão com o banco de dados
$conn = new mysqli("localhost", "cvheal47_root", "cvheal47_root", "cvheal47_building_mks");

// Verifica a conexão
if ($conn->connect_error) {
    die("Conexão falhou: " . $conn->connect_error);
}

// Consulta para obter as notificações
$sql = "SELECT title, notif_msg FROM notif ORDER BY publish_date DESC LIMIT 5";
$result = $conn->query($sql);

// Array para armazenar as notificações
$notifications = array();

if ($result->num_rows > 0) {
    // Armazena as notificações em um array
    while($row = $result->fetch_assoc()) {
        $notifications[] = $row;
    }
}

// Fecha conexão
$conn->close();

// Retorna as notificações em formato JSON
echo json_encode($notifications);
?>
