<?php
include("database/database.php");
$query = "SELECT id,ind, activity, people, area, charge,reg_date from  pmoc_room ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$ind,$activity,$people,$area,$charge,$reg_date );
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
 


?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Identificação</th>
                           <th>Atividade</th>
                           <th>Ocupantes</th>
                           <th>Climatizado</th>
                           <th>Carga</th>
                           <th>Registro</th>
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($ind); ?> </td>
                          <td><?php printf($activity); ?> </td>
                          <td><?php printf($people); ?> </td>
                          <td><?php printf($area); ?> </td>
                          <td><?php printf($charge); ?> </td>
                          <td><?php printf($reg_date); ?> </td>
                      
                          <td>  
               
                          <a class="btn btn-app"  href="utilities-pmoc-register-room-edit?id=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
          <!--     <a href="backend/register-location-unit-trash-backend?id=<?php printf($id); ?>" id="id-btn-dialog2" class="btn btn-app" data-toggle="modal" data-target=".bs-example-modal-sm"> <i class="fa fa-trash"></i> Excluir</a>
                    
                  </a> -->
               <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/utilities-pmoc-register-room-trash-backend?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                 
         
                  
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>