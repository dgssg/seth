  <?php
$codigoget = ($_GET["tools"]);

// Create connection
include("database/database.php");// remover ../


$query = "SELECT  *FROM calibration_parameter_tools WHERE id like '$codigoget' ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $fabricante, $modelo, $serie, $patrimonio,$calibration, $date_calibration, $date_validation, $id_equipamento_grupo, $upgrade, $reg_date,$trash);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
?>

   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Analisador &amp; Simulador </h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Descrição <small>Analisador &amp; Simulador</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />


                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($nome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="fabricante">Fabricante<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="fabricante" name="fabricante" required="required" class="form-control"  value="<?php printf($fabricante); ?> " readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Modelo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"   value="<?php printf($modelo); ?>" readonly="readonly">
                        </div>
                      </div>


                      <div class="ln_solid"></div>


                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Numero de Serie</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"   value="<?php printf($serie); ?> " readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Patrimonio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"   value="<?php printf($equipamento_patrimonio); ?> " readonly="readonly">
                        </div>
                      </div>
                       <div class="ln_solid"></div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Certificado</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input class="form-control" type="text"   value="<?php printf($calibration); ?> " readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Calibração</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input class="form-control" type="text"   value="<?php printf($date_calibration); ?> " readonly="readonly">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Validade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"   value="<?php printf($date_validation); ?> " readonly="readonly">
                        </div>
                      </div>


                        <div class="ln_solid"></div>
                           <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"   value="<?php printf($upgrade); ?> " readonly="readonly">
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"   value="<?php printf($reg_date); ?> " readonly="readonly">
                        </div>
                      </div>

                  </div>
                </div>
              </div>
            </div>


             <!-- page content -->


          <div class="x_panel">
                <div class="x_title">
                  <h2>Parâmetro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                         <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo -->
       <?php
$query="SELECT *FROM  calibration_parameter_tools_dados WHERE `id_calibration_parameter_tools` LIKE $codigoget ";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $id_calibration_parameter_tools, $nome, $unidade, $incerteza, $resolução,$upgrade,$reg_date);

?>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>#</th>
                          <th>Parametro</th>
                          <th>Unidade</th>
                          <th>Incerteza Herdada</th>
                          <th>Resolução </th>

                          <th>Ação</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id); ?></td>
                          <td><?php printf($nome); ?></td>
                          <td><?php echo htmlspecialchars_decode($unidade); ?></td>
                          <td><?php printf($incerteza); ?></td>
                          <td><?php printf($resolução); ?></td>

                         <td>  
                   <a class="btn btn-app"  href="calibration-parameter-equipament-parameter-edit?id=<?php printf($id); ?>&tools=<?php printf($codigoget); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                  
              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                      <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/calibration-parameter-equipament-parameter-trash.php?id=<?php printf($id); ?>';
  }
})
">
                  <i class="fa fa-trash"></i> Excluir</a>
                  </td>
                        </tr>
                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>




                </div>
              </div>

            <div class="clearfix"></div>







             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="calibration-parameter-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  <a class="btn btn-app"  href="calibration-parameter-equipament-edit?tools=<?php printf($codigoget); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                  
                  <a class="btn btn-app"  href="calibration-parameter-equipament-report?tools=<?php printf($codigoget); ?>"  onclick="new PNotify({
																title: 'Laudo',
																text: 'Adicionar Laudo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-plus"></i> Laudo
                  </a>
                 <a class="btn btn-app"  href="calibration-parameter-equipament-parameter?tools=<?php printf($codigoget); ?>"  onclick="new PNotify({
																title: 'Parametro',
																text: 'Adicionar Parametro',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-plus"></i> Parametro
                  </a>
                   
    



                </div>
              </div>




        </div>
            </div>
        <!-- /page content -->
        <!-- compose -->
         <!-- Fechamento -->
               <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel4">Adicionar</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                         <h4>Parametro</h4>
                           <!-- Registro forms-->
                          <form action="backend/calibration-parameter-equipament-parameter-backend.php?tools=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>



                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="dado">Parametro <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" name="nome"  required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" name="unidade"  class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Incerteza Herdada <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" name="i_h"  class="form-control " onchange="alteraPonto($(this))">
                        </div>
                      </div>
                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Resolução <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" name="r_p"   class="form-control " onchange="alteraPonto($(this))">
                        </div>
                      </div>



                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary" onclick="new PNotify({
																title: 'Registrado',
																text: 'Informações registrada!',
																type: 'success',
																styling: 'bootstrap3'
														});">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>
              <!-- Fechamen    to -->

                <!-- compose -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

                <script type="text/javascript">
    function alteraPonto(valorInput){
    alert("Valor original: " + valorInput.val());
    alert("Valor com ponto: " + valorInput.val().replace(",", "."));
    $(valorInput).val(valorInput.val().replace(",", "."));
}
</script>
