
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nº</th>
                          
                          
                          <th>O.S</th>
                          <th>Solicitação</th>
                          
                          <th>Data</th>
                          <th>Descrição</th>
                          <th>Atualização</th>
                          <th>Fornecedor</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          <th>Codigo</th>
                          <th>Serie</th>
                          <th>Recursos</th>
                          
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                   
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>

<script language="JavaScript">
  $(document).ready(function() {
    
    $('#datatable').dataTable( {
      "processing": true,
      "stateSave": true,
      "responsive": true,
      scrollX: true,   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<        
      scrollY: 700,   //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      scrollCollapse: true,
      responsive: true,
      colReorder: false,
      keys: true,
      select: true,
      
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      }
      
      
      
    } );
    
    
    // Define a URL da API que retorna os dados da query em formato JSON
    const apiUrl = 'table/table-search-purchases.php?status=1';
    
    
    
    // Usa a função fetch() para obter os dados da API em formato JSON
    fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
      // Mapeia os dados para o formato esperado pelo DataTables
      const novosDados = data.map(item => [
        '',
        item.id,
        item.id_os,
        item.solicitacao,
        item.data_purchases,
        item.descr,
        item.upgrade,
        item.empresa,
        item.nome,
        item.modelo,
        item.fabricante,
        item.codigo,
        item.serie,
        `<a class="btn btn-app"  href="purchases-viewer?purchases=${item.id}" target="_blank">
          <i class="glyphicon glyphicon-file"></i> Visualizar
        </a>`,
        `<a class="btn btn-app" href="purchases-progress-edit?purchases=${item.id}" onclick=" new PNotify({
          title: 'Editar',
          text: 'Abrindo Edição',
          type: 'success',
          styling: 'bootstrap3'
        });">
          <i class="fa fa-edit"></i> Editar
        </a>
        <a class="btn btn-app" onclick="Swal.fire({
          title: 'Tem certeza?',
          text: 'Você não será capaz de reverter isso!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, Deletar!'
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire(
              'Deletando!',
              'Seu arquivo será excluído.',
              'success'
            ),
            window.location = 'backend/purchases-progress-trash-backend.php?purchases=${item.id}';
          }
        });">
          <i class="fa fa-trash"></i> Excluir
        </a>`
      ]);
      
      // Inicializa o DataTables com os novos dados
      const table = $('#datatable').DataTable();
      table.clear().rows.add(novosDados).draw();
      
      
      // Cria os filtros após a tabela ser populada
      $('#datatable').DataTable().columns([1,2, 4,6,7,8,9,10,11,12]).every(function(d) {
        var column = this;
        var theadname = $("#datatable th").eq([d]).text();
        // Container para o título, o select e o alerta de filtro
        var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
        
        // Título acima do select
        var title = $('<label>' + theadname + '</label>').appendTo(container);
        
        // Container para o select
        var selectContainer = $('<div></div>').appendTo(container);
        
        var select = $('<select class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo(selectContainer).select2()
        .on('change', function() {
          var val = $.fn.dataTable.util.escapeRegex($(this).val());
          column.search(val ? '^' + val + '$' : '', true, false).draw();
          
          // Remove qualquer alerta existente
          container.find('.filter-alert').remove();
          
          // Se um valor for selecionado, adicionar o alerta de filtro
          if (val) {
            $('<div class="filter-alert">' +
              '<span class="filter-active-indicator">&#x25CF;</span>' +
              '<span class="filter-active-message">Filtro ativo</span>' +
              '</div>').appendTo(container);
          }
          
          // Remove o indicador do título da coluna
          $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
          
          // Se um valor for selecionado, adicionar o indicador no título da coluna
          if (val) {
            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
          }
        });
        
        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
        // Verificar se há filtros aplicados ao carregar a página
        var filterValue = column.search();
        if (filterValue) {
          select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
        }
      });
      
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
      var oTable = $('#datatable').dataTable();
      
      // Avança para a próxima página da tabela
      //oTable.fnPageChange('next');
      
      
      
    });
    
    
    // Armazenar a posição atual ao sair da página
    $(window).on('beforeunload', function() {
      var table = $('#datatable').DataTable();
      var pageInfo = table.page.info();
      localStorage.setItem('paginationPosition', JSON.stringify(pageInfo));
    });
    
  
    
    
    
    
  });
  
  // Restaurar a posição ao retornar à página
  $(document).ready(function() {
    var storedPosition = localStorage.getItem('paginationPosition');
    if (storedPosition) {
      var pageInfo = JSON.parse(storedPosition);
      var table = $('#datatable').DataTable();
      table.page(pageInfo.page).draw('page');
    }
  });
  
</script>