<?php
include("database/database.php");



?>

<form  action="backend/utilities-gas-glp-supply-register-backend.php" method="post">

  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Instalação de Gas GLP</label>
    <div class="col-md-6 col-sm-6 ">
    <select type="text" class="form-control has-feedback-left" name="id_unidade" id="id_unidade"  placeholder="Unidade">
      <?php

 

$result_cat_post  = "SELECT  id, cod FROM unidade_gas_glp where id_status = 0 ";

$resultado_cat_post = mysqli_query($conn, $result_cat_post);
while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
  echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['cod'].'</option>';
}
?>
    </select>
    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_unidade').select2();
  });
  </script>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Abastecimento</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="date_supply" class="form-control" type="date" name="date_supply"  >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nº Pacientes/Dia</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="nb" class="form-control" type="text" name="nb"   >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade Abastecida </label>
    <div class="col-md-6 col-sm-6 ">
      <input id="qtd" class="form-control" type="text" name="qtd"   >
    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor do Abastecimento</label>
    <div class="col-md-6 col-sm-6 ">
       <input id="vlr" class="form-control money2" type="text" name="vlr"   onKeyPress="return(moeda(this,'.',',',event))" >

    </div>
  </div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Número da Nota Fiscal</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="nf" class="form-control" type="text" name="nf"   >
    </div>
  </div>


  <button type="reset" class="btn btn-primary"onclick="new PNotify({
    title: 'Limpado',
    text: 'Todos os Campos Limpos',
    type: 'info',
    styling: 'bootstrap3'
  });" />Limpar</button>
  <input type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informações registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" value="Salvar"/>

</form>
<div class="ln_solid"></div>
