
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Codigo</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                        
                          <th>N/S</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>

                          <th>Ano</th>
                          <th>Pontuação</th>
                          <th>ROOI</th>
                        
                        
                          
                          
                         
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
              </div>
  <script type="text/javascript">
 
        $(document).ready(function() {
  // Define a URL da API que retorna os dados da query em formato JSON
const apiUrl = 'api/api-search-obsolescence.php';

// Usa a função fetch() para obter os dados da API em formato JSON
fetch(apiUrl)
  .then(response => response.json())
  .then(data => {
    // Mapeia os dados para o formato esperado pelo DataTables
    const novosDados = data.map(item => [
      ``,
      item.codigo,
      item.equipamento,
      item.modelo,
      item.fabricante,
      item.serie,
      item.instituicao,
      item.setor,
      item.area,
      item.yr,
      item.score,
      item.rooi,
    
      `   <a class="btn btn-app"  href="obsolescence-analysis-edit?id=${item.id}" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                  
           <a  class="btn btn-app" href="obsolescence-analysis-viwer?id=${item.id}"target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
           
                                      
                  <a class="btn btn-app"     onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/obsolescence-trash-backend?id=${item.id}';
  }
})
">
                                  <i class="fa fa-trash"></i> Excluir
                                </a>`
    ]);

// Adiciona as novas linhas ao DataTables e desenha a tabela
$('#datatable').DataTable().rows.add(novosDados).draw();


              // Cria os filtros após a tabela ser populada
$('#datatable').DataTable().columns([1,2,3,4,5,6,7,8,9,10,11]).every(function (d) {
        var column = this;
        var theadname = $("#datatable th").eq([d]).text();
        var container = $('<div class="filter-title"></div>').appendTo('#userstable_filter');
        var label = $('<label>'+theadname+': </label>').appendTo(container); 
        var select = $('<select  class="form-control my-1"><option value="">' +
          theadname +'</option></select>').appendTo('#userstable_filter').select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });

        column.data().unique().sort().each(function (d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
      });
    });
});
</script>