  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");
$query = "SELECT equipamento_familia.anvisa,equipamento.baixa,equipamento.ativo,equipamento.inventario,equipamento.comodato,equipamento.duplicidade,equipamento.preventive,equipamento.obsolescence,equipamento.rfid,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento.serie,equipamento.patrimonio,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao WHERE equipamento.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($anvisa,$baixa,$ativo,$inventario,$comodato,$duplicidade,$preventive,$obsolecencia,$rfid,$data_val,$nf,$data_fab,$data_instal,$serie,$patrimonio,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }


}
?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> Registro <small>Equipamento</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Dados <small>Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($nome); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Modelo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text"  value="<?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Fabricante <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" value="<?php printf($fabricante); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                  
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"readonly="readonly"  required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="patrimonio">Patromonio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="patrimonio" name="patrimonio" value="<?php printf($patrimonio); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="serie">N/S <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="serie" name="serie" value="<?php printf($serie); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Codigo <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_instal" name="data_instal" value="<?php printf($data_instal); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_fab">Fabricação  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_fab" name="data_fab" value="<?php printf($data_fab); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_val">Garantia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="data_val" name="data_val" value="<?php printf($data_val); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">Observação <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="obs" name="obs" value="<?php printf($obs); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>



                  </div>
                </div>
              </div>
	       </div>


	  <div class="clearfix"></div>


 <!-- Posicionamento -->
 <div class="x_panel">
                                          <div class="x_title">
                                            <h2>Não Conformidade</h2>
                                            <ul class="nav navbar-right panel_toolbox">

                                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                              </li>
                                              <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                                  class="fa fa-wrench"></i></a>
                                                  <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Settings 1</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                  </ul>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                              </ul>
                                              <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">

                                              <!--arquivo -->


                                              <!--arquivo -->
                                              <?php
                                              $query=" SELECT compliance.nome,equipament_compliance.id, equipament_compliance.id_os, equipament_compliance.file, equipament_compliance.titulo, equipament_compliance.upgrade, equipament_compliance.reg_date, equipament_compliance.date_compliance, equipament_compliance.id_compliance, equipament_compliance.when_compliance, equipament_compliance.how_compliance, equipament_compliance.obs_compliance, equipament_compliance.responsible_compliance, equipament_compliance.date_quality, equipament_compliance.analysis_quality, equipament_compliance.date_conclusion, equipament_compliance.conclusion_compliance, equipament_compliance.id_responsible FROM  equipament_compliance  INNER JOIN compliance ON compliance.id = equipament_compliance.id_compliance INNER JOIN os ON os.id = equipament_compliance.id_os WHERE os.id_equipamento like '$codigoget'";
                                              $row=1;
                                              if ($stmt = $conn->prepare($query)) {
                                                $stmt->execute();
                                                $stmt->bind_result($nome,$id, $id_os, $file, $titulo, $upgrade, $reg_date, $date_compliance, $id_compliance, $when_compliance, $how_compliance, $obs_compliance, $responsible_compliance, $date_quality, $analysis_quality, $date_conclusion, $conclusion_compliance, $id_responsible);

                                                ?>
                                                <table class="table table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th>#</th>
                                                      <th>Fato</th>
                                                      <th>Resumo</th>
                                                      <th>Data</th>
                                                      <th>Anexo</th>
                                                      <th>Ação</th>


                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <?php  while ($stmt->fetch()) { ?>

                                                      <tr>
                                                        <th scope="row"><?php printf($row); ?></th>
                                                        <td><?php printf($nome); ?></td>
                                                        <td><?php printf($how_compliance); ?></td>
                                                        <td><?php printf($date_compliance); ?></td>
                                                        <td> <?php if($file==!""){?><a href="dropzone/equipament-compliance/<?php printf($file); ?> " download><i class="fa fa-paperclip"></i> </a>     <?php  }  ?></td>
                                                        <td>
                                                          
                                                        
                                                        <a  class="btn btn-app" href="os-complice-viewer?os=<?php printf($id); ?> "target="_blank" onclick="new PNotify({
                                                title: 'Visualizar',
                                                text: 'Visualizar Movimentação!',
                                                type: 'info',
                                                styling: 'bootstrap3'
                                              });" >
                                              <i class="fa fa-file-pdf-o"></i> Visualizar
                                            </a>
                                            
                                                        
                                                       
                                                       
                                                        
                                                        
                                                        
                                                        </td>

                                                          


                                                        </tr>
                                                        <?php  $row=$row+1; }
                                                      }   ?>
                                                    </tbody>
                                                  </table>






                                                  <!--arquivo -->
                                                </div>
                                              </div>



            <div class="clearfix"></div>




              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
