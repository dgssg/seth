  <?php
$codigoget = ($_GET["routine"]);

// Create connection

include("database/database.php");
$query = "SELECT maintenance_routine.group_routine,maintenance_routine.app,maintenance_routine.return_user,maintenance_routine.signature_digital,maintenance_routine.digital,maintenance_routine.periodicidade_after,maintenance_routine.data_after,maintenance_routine.time_after,maintenance_routine.id_pop,maintenance_routine.id_maintenance_procedures_after,maintenance_routine.id_maintenance_procedures_before,maintenance_routine.id_fornecedor,maintenance_routine.id_colaborador,maintenance_routine.id_category,maintenance_routine.habilitado,maintenance_routine.time_ms,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id where maintenance_routine.id like'$codigoget'";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($group_routine,$app,$return_user,$signature_digital,$digital,$periodicidade_after,$data_after,$time_after,$id_pop,$id_maintenance_procedures_after,$id_maintenance_procedures_before,$id_fornecedor,$id_colaborador,$id_category,$habilitado,$time_ms,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
}
}
$new_date=date('Y-m-d', strtotime($data_start));
?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>Rotina</small> Infraestrutura</h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Procedimento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
 <form action="backend/maintenance-routine-edit-infra-backend.php?routine=<?php printf($codigoget);?>" method="post">

 <div class="ln_solid"></div>
                <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Rotina <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($rotina); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Equipamento <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="nome" name="nome" value="<?php printf($codigo); ?> - <?php printf($nome); ?> - <?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Habilitado</label>
                        <div class="col-md-6 col-sm-6 ">
                           <div class="">
                            <label>
                              <input name="habilitado"type="checkbox" class="js-switch" <?php if($habilitado == "0"){printf("checked"); }?> />
                            </label>
                          </div>

                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Procedimento Digital</label>
                        <div class="col-md-6 col-sm-6 ">
                           <div class="">
                            <label>
                              <input name="digital"type="checkbox" class="js-switch" <?php if($digital == "0"){printf("checked"); }?> />
                            </label>
                          </div>

                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Assinatura Digital</label>
                        <div class="col-md-6 col-sm-6 ">
                           <div class="">
                            <label>
                              <input name="signature_digital"type="checkbox" class="js-switch" <?php if($signature_digital == "0"){printf("checked"); }?> />
                            </label>
                          </div>

                        </div>
                      </div>

    <div class="item form-group">
      <label class="col-form-label col-md-3 col-sm-3 label-align">Aplicativo</label>
      <div class="col-md-6 col-sm-6 ">
        <div class="">
          <label>
            <input name="app"type="checkbox" class="js-switch" <?php if($app == "0"){printf("checked"); }?> />
          </label>
        </div>
        
      </div>
    </div>
    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Grupo de Rotina</label>
      <div class="col-md-6 col-sm-6 ">
        <select type="text" class="form-control has-feedback-right" name="group_routine" id="group_routine"  placeholder="Grupo de Rotina" >
          <option value="">Selecione uma opção</option>
          <?php
            
            
            
            $sql = " SELECT id, nome FROM maintenance_group  ";
            
            
            if ($stmt = $conn->prepare($sql)) {
              $stmt->execute();
              $stmt->bind_result($id,$nome);
              while ($stmt->fetch()) {
          ?>
          <option value="<?php printf($id);?>	"<?php if( $id == $group_routine){ printf("selected");} ?>><?php printf($nome);?>	 </option>
          <?php
            // tira o resultado da busca da memória
            }
            
            }
            $stmt->close();
            
          ?>
        </select>
        <span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a opção "></span>
        
      </div>
      
      
      <script>
        $(document).ready(function() {
          $('#group_routine').select2();
        });
      </script>
    </div>
    <div class="item form-group">
      <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Devolutiva</label>
      <div class="col-md-6 col-sm-6 ">
        <select type="text" class="form-control has-feedback-right" name="return_user" id="return_user"  placeholder="Devolutiva" >
          <option value="">Selecione uma opção</option>
          <?php
            
            
            
            $sql = " SELECT id, nome,sobrenome FROM usuario WHERE  trash = 1 ";
            
            
            if ($stmt = $conn->prepare($sql)) {
              $stmt->execute();
              $stmt->bind_result($id,$nome,$sobrenome);
              while ($stmt->fetch()) {
          ?>
          <option value="<?php printf($id);?>	"<?php if( $id == $return_user){ printf("selected");} ?>><?php printf($nome);?>	<?php printf($sobrenome);?> </option>
          <?php
            // tira o resultado da busca da memória
            }
            
            }
            $stmt->close();
            
          ?>
        </select>
        <span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a opção "></span>
        
      </div>
      
      
      <script>
        $(document).ready(function() {
          $('#return_user').select2();
        });
      </script>
    </div>
    

             			  <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Categoria</label>
                        <div class="col-md-6 col-sm-6 ">
										<select type="text" class="form-control has-feedback-right" name="categoria" id="categoria"  placeholder="Categoria">
										<option value="">Selecione uma Categoria</option>
										  	<?php



										   $sql = " SELECT id, nome FROM category where ativo like '0' and trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$nome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"<?php if($id == $id_category){ printf("selected");} ?>><?php printf($nome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
										<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a categoria "></span>

											</div>


								 <script>
                                    $(document).ready(function() {
                                    $('#categoria').select2();
                                      });
                                 </script>
                                 </div>

                                <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Colaborador</label>
                        <div class="col-md-6 col-sm-6 ">

                            <label>
                         	<select type="text" class="form-control has-feedback-right" name="colaborador" id="colaborador"  placeholder="Colaborador" >
										  <option value="">Selecione um Colaborador</option>

										  	<?php



										   $sql = "SELECT  id, primeironome,ultimonome FROM colaborador where trash =1 and id_categoria like '%$id_category%'";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$primeironome,$ultimonome);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"  <?php if($id == $id_colaborador){ printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
	                                     	<span class="fa fa-bookmark form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um colaborador "></span>
                                        </div>
                                        </div>

								 <script>
                                    $(document).ready(function() {
                                    $('#colaborador').select2();
                                      });
                                 </script>
                       <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Fornecedor</label>
                        <div class="col-md-6 col-sm-6 ">

                            <label>
                      <select type="text" class="form-control has-feedback-right" name="fornecedor" id="fornecedor"  placeholder="Fornecedor" value="<?php printf($id_fornecedor); ?>">
									 <option value="">Selecione um fornecedor</option>

										  	<?php



										   $sql = "SELECT  id, empresa FROM fornecedor where trash =1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_empresa,$empresa);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_empresa);?>	" <?php if($id_empresa == $id_fornecedor){ printf("selected");};?>><?php printf($empresa);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#fornecedor').select2();
                                      });
                                 </script>
 <div class="ln_solid"></div>
                                 <small>Procedimento com Laudo dentro da Validade</small>

                                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Procedimento 1º</label>
                        <div class="col-md-6 col-sm-6 ">

                            <label>
                                 	<select type="text" class="form-control has-feedback-right" name="procedimento_1" id="procedimento_1"  placeholder="Procedimento" value="">
									 <option value="">Selecione um procedimento</option>
										<?php





										   $sql = "SELECT  id, name, codigo FROM maintenance_procedures where trash =1 ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_procedimento,$procedimento,$codigo);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_procedimento);?>	" <?php if($id_procedimento == $id_maintenance_procedures_before){ printf("selected");};?>><?php printf($codigo);?> - <?php printf($procedimento);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                        </div>

                      	 <script>
                                    $(document).ready(function() {
                                    $('#procedimento_1').select2();
                                      });
                                 </script>

<div class="item form-group">
                                 <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio<span class="required">*</span>
                                 </label>
                                 <div class="col-md-6 col-sm-6 ">
                                   <input type="text" required="required" class="form-control"  value="<?php printf($new_date); ?>" readonly="readonly">
                                 </div>
                               </div>

                               <div class="item form-group">
                                 <label class="col-form-label col-md-3 col-sm-3 label-align" for="categoria">Data Inicio Programada/Reprogramada<span class="required">*</span>
                                 </label>
                                 <div class="col-md-6 col-sm-6 ">
                                   <input type="text" required="required" class="form-control"  value="<?php printf($data_start); ?>" readonly="readonly">
                                 </div>
                               </div>

                                   <div class="item form-group">
                                   <label class="col-form-label col-md-3 col-sm-3 label-align" for="backlog" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Tempo <span></span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 ">
                                     <input type="text" id="timepicker1" name="time_ms"  class="form-control " value="<?php printf($time_ms); ?>"  pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="99:99">
                                   </div>
                                 </div>

                                <div class="item form-group">
                                   <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Periodicidade</label>
                                   <div class="col-md-6 col-sm-6 ">
                                     <select class="form-control has-feedback-right" tabindex="-1" name="periodicidade" id="periodicidade">
                                       <option value="">Selecione uma periodicidade</option>
                                       <option value="1"<?php if($periodicidade == 1){ printf("selected");};?>>Diaria [ 1 Dias]</option>
                                       <option value="5"<?php if($periodicidade == 5){ printf("selected");};?>>Diaria [ 1 Dias Uteis]</option>
                                       <option value="7"<?php if($periodicidade == 7){ printf("selected");};?>>Semanal  [ 7 Dias]</option>
                                       <option value="14"<?php if($periodicidade == 14){ printf("selected");};?>>Bisemanal [ 14 Dias]</option>
                                       <option value="21"<?php if($periodicidade == 21){ printf("selected");};?>>Trisemanal [ 21 Dias]</option>
                                       <option value="28"<?php if($periodicidade == 28){ printf("selected");};?>>Quadrisemanal [ 28 Dias]</option>
                                       <option value="30"<?php if($periodicidade == 30){ printf("selected");};?>>Mensal [ 30 Dias]</option>
                                       <option value="60"<?php if($periodicidade == 60){ printf("selected");};?>>Bimensal [ 60 Dias]</option>
                                       <option value="90"<?php if($periodicidade == 90){ printf("selected");};?>>Trimestral [ 90 Dias]</option>
                                       <option value="120"<?php if($periodicidade == 120){ printf("selected");};?>>Quadrimestral [ 120 Dias]</option>
                                       <option value="180"<?php if($periodicidade == 180){ printf("selected");};?>>Semestral [ 180 Dias]</option>
                                       <option value="365"<?php if($periodicidade == 365){ printf("selected");};?>>Anual [ 365 Dias]</option>
                                       <option value="730"<?php if($periodicidade == 730){ printf("selected");};?>>Bianual [ 730 Dias]</option>

                                     </select>
                                     	<span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Periodicidade "></span>

                                   </div>
                                </div>
                                 <script>
                                               $(document).ready(function() {
                                               $('#periodicidade').select2();
                                                 });
                                            </script>
                                            <div class="ln_solid"></div>
                                                                           <small>Procedimento com Laudo fora da Validade</small>

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Procedimento 2º</label>
                        <div class="col-md-6 col-sm-6 ">

                            <label>
                         	<select type="text" class="form-control has-feedback-right" name="procedimento_2" id="procedimento_2"  placeholder="Procedimento" value="">
									 <option value="">Selecione um procedimento</option>
										<?php





										   $sql = "SELECT  id, name, codigo FROM maintenance_procedures where trash =1";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_procedimento,$procedimento,$codigo);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_procedimento);?>	" <?php if($id_procedimento == $id_maintenance_procedures_after){ printf("selected");};?>><?php printf($codigo);?> - <?php printf($procedimento);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
	                                     	</div>
                        </div>

                      	 <script>
                                    $(document).ready(function() {
                                    $('#procedimento_2').select2();
                                      });
                                 </script>

                              

                                    <div class="ln_solid"></div>


                                  <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">POP </label>
                        <div class="col-md-6 col-sm-6 ">

                            <label>
                         	<select type="text" class="form-control has-feedback-right" name="pop" id="pop"  placeholder="pop" value="">
									 <option value="">Selecione um pop</option>
										<?php





										   $sql = "SELECT  id, titulo  FROM documentation_pop ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id,$titulo);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	" <?php if($id == $id_pop){ printf("selected");};?>>  <?php printf($titulo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
	                                     	</div>
                        </div>

                      	 <script>
                                    $(document).ready(function() {
                                    $('#pop').select2();
                                      });
                                 </script>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>
 <div class="ln_solid"></div>
             <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>
</form>
                  </div>
                </div>
              </div>
	       </div>





              <!-- Posicionamento -->



             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="maintenance-routine">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>
                  <a class="btn btn-app"  href="maintenance-routine-viwer?routine=<?php printf($codigoget); ?>" target="_blank"  onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar procediemnto',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a>
                   <a class="btn btn-app"  href="backend/maintenance-routine-retweet-backend.php?routine=<?php printf($codigoget); ?>" target="_blank"   onclick="new PNotify({
																title: 'Manutenção Preventiva',
																text: 'Manutenção Preventiva Retroativo',
																type: 'warning',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-retweet"></i> MP Retroativo
                  </a>
                   <a class="btn btn-app"  href="backend/maintenance-routine-fast-forward-backend.php?routine=<?php printf($codigoget); ?>" target="_blank"   onclick="new PNotify({
																title: 'Manutenção Preventiva',
																text: 'Manutenção Preventiva Antecipada',
																type: 'warning',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-fast-forward"></i> MP Antecipada
                  </a>
                  <a class="btn btn-app"  href="maintenance-routine-reschedule?routine=<?php printf($codigoget); ?>"  onclick="new PNotify({
                               title: 'Manutenção Preventiva',
                               text: 'Manutenção Preventiva Reprogramada',
                               type: 'warning',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-clock-o"></i> MP Reprogramada
                 </a>
                 <a class="btn btn-app"  href="maintenance-routine-edit?routine=<?php $agle = $codigoget+1; printf($agle); ?>" onclick="new PNotify({
																title: 'Proximo',
																text: 'Abrindo Proximo',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-angle-right"></i> Proximo
                  </a>
                  <a class="btn btn-app"  href="maintenance-routine-edit?routine=<?php $agle = $codigoget-1; printf($agle); ?>" onclick="new PNotify({
																title: 'Anterior',
																text: 'Abrindo Anterior',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-angle-left"></i> Anterior
                  </a>


              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>
                              