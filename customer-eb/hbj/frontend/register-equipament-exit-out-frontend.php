<?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");
$query = "SELECT equipamento_familia.anvisa,equipamento.baixa,equipamento.ativo,equipamento.inventario,equipamento.comodato,equipamento.duplicidade,equipamento.preventive,equipamento.obsolescence,equipamento.rfid,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento.serie,equipamento.patrimonio,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao WHERE equipamento.id like '$codigoget'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($anvisa,$baixa,$ativo,$inventario,$comodato,$duplicidade,$preventive,$obsolecencia,$rfid,$data_val,$nf,$data_fab,$data_instal,$serie,$patrimonio,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
 while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  }


}
?>
   <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Saida Equipamento</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
 <div class="clearfix"></div>
 <div class="row" style="display: block;">
   <div class="col-md-12 col-sm-12  ">
     <div class="x_panel">
       <div class="x_title">
         <h2>Dados <small>Equipamento</small></h2>
         <ul class="nav navbar-right panel_toolbox">
           <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
           </li>
           <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
             <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                 <a class="dropdown-item" href="#">Settings 1</a>
                 <a class="dropdown-item" href="#">Settings 2</a>
               </div>
           </li>
           <li><a class="close-link"><i class="fa fa-close"></i></a>
           </li>
         </ul>
         <div class="clearfix"></div>
       </div>
       <div class="x_content">



           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Nome <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" value="<?php printf($nome); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Modelo <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text"  value="<?php printf($modelo); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
             <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Fabricante <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" value="<?php printf($fabricante); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
       
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="codigo">Codigo <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="codigo" name="codigo" value="<?php printf($codigo); ?>"readonly="readonly"  required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="patrimonio">Patromonio <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="patrimonio" name="patrimonio" value="<?php printf($patrimonio); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="serie">N/S <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="serie" name="serie" value="<?php printf($serie); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_instal">Codigo <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="data_instal" name="data_instal" value="<?php printf($data_instal); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_fab">Fabricação  <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="data_fab" name="data_fab" value="<?php printf($data_fab); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="data_val">Garantia <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="data_val" name="data_val" value="<?php printf($data_val); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">NF <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="nf" name="nf" value="<?php printf($nf); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>
           <div class="item form-group">
             <label class="col-form-label col-md-3 col-sm-3 label-align" for="obs">Observação <span class="required">*</span>
             </label>
             <div class="col-md-6 col-sm-6 ">
               <input type="text" id="obs" name="obs" value="<?php printf($obs); ?>" readonly="readonly" required="required" class="form-control ">
             </div>
           </div>



       </div>
     </div>
   </div>
</div>

         <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Saida <small>de Equipamento</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />

                  <form  action="backend/register-equipament-exit-out-backend.php?equipamento=<?php printf($codigoget); ?> " method="post">

                        <div class="item form-group">

                        <div class="col-md-6 col-sm-6 ">
                          <input id="id_equipamento" class="form-control" type="hidden" name="id_equipamento" readonly="readonly" value"<?php printf($id_equipamento); ?>" placeholder="<?php printf($id_equipamento); ?>" >
                        </div>
                      </div>

 <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_equipament_exit_mov" class="docs-tooltip" data-toggle="tooltip" title="Selecione uma movimentação ">Movimentação <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_equipament_exit_mov" id="id_equipament_exit_mov"   >

										  	<?php



										    $sql = "SELECT  id, nome FROM equipament_exit_mov ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_equipament_exit_mov,$mov);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_equipament_exit_mov);?>	"><?php printf($mov);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#id_equipament_exit_mov').select2();
                                      });
                                 </script>


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_equipament_exit_reason" class="docs-tooltip" data-toggle="tooltip" title="Selecione uma motivo ">Motivo <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_equipament_exit_reason" id="id_equipament_exit_reason"   >

										  	<?php



										    $sql = "SELECT  id, nome FROM equipament_exit_reason ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_equipament_exit_reason,$reason);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_equipament_exit_reason);?>	"><?php printf($reason);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#id_equipament_exit_reason').select2();
                                      });
                                 </script>

                         <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="fornecedor" class="docs-tooltip" data-toggle="tooltip" title="Selecione o Fornecedor ">Fornecedor <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="fornecedor" id="fornecedor"  placeholder="Fornecedor" >

										  	<?php



										   $sql = "SELECT  id, empresa FROM fornecedor  ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_empresa,$empresa);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_empresa);?>	"><?php printf($empresa);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#fornecedor').select2();
                                      });
                                 </script>


                     <div class="ln_solid"></div>
                     <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Responsavel</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="responsavel" class="form-control" type="text" name="responsavel"   >
                        </div>
                      </div>

                     <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="telefone" class="docs-tooltip" data-toggle="tooltip" title="Tempo para backlog ">Telefone <span></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="telefone" name="telefone"  class="form-control " value="<?php printf($telefone); ?>"  pattern="\d{3}\-\d{3}\-\d{4}" class="form-control telephone" data-mask="(99)-99999-9999">
                        </div>
                      </div>

                    <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Envio</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="date_send" class="form-control" type="date" name="date_send"  >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data Retorno</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="date_return" class="form-control" type="date" name="date_return"   >
                        </div>
                      </div>

                        <div class="ln_solid"></div>
                          <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="id_equipament_exit_status" class="docs-tooltip" data-toggle="tooltip" title="Selecione um Status ">Status <span aria-hidden="true"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                         	<select type="text" class="form-control has-feedback-right" name="id_equipament_exit_status" id="id_equipament_exit_status"   >

										  	<?php



										    $sql = "SELECT  id, nome FROM equipament_exit_status ";


if ($stmt = $conn->prepare($sql)) {
		$stmt->execute();
     $stmt->bind_result($id_equipament_exit_status,$status);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id_equipament_exit_status);?>	"><?php printf($status);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
	                                     	</select>
                        </div>
                      </div>









								 <script>
                                    $(document).ready(function() {
                                    $('#id_equipament_exit_status').select2();
                                      });
                                 </script>

                                  <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Observação</label>
                        <div class="col-md-6 col-sm-6 ">
                        	<textarea  type="text" class="form-control has-feedback-left" id="obs" name="obs" placeholder="Observação" data-ls-module="charCounter" maxlength="1000" data-parsley-maxlength="1000" data-parsley-trigger="keyup" data-parsley-maxlength="1000" data-parsley-minlength-message="Vamos! Você precisa inserir um comentário de pelo menos 20 caracteres."
                            data-parsley-validation-threshold="20"> </textarea>
                        </div>
                      </div>






                    <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit"  onclick="new PNotify({ title: 'Registrado', text: 'Informações registrada!', type: 'success', styling: 'bootstrap3' });" >Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                                  </form>




                  </div>
                </div>
              </div>
            </div>

             <!-- page content -->



            <div class="clearfix"></div>







             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="register-equipament-exit?equipamento=<?php printf($codigoget); ?>">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>




             <!--    ref="backend/os-progress-upgrade-close-frontend.php?os=<?php printf($codigoget); ?>"

             <a class="btn btn-app"  href=""onclick="new PNotify({
																title: 'Atualização',
																text: 'Atualização de Abertura de O.S!',
																type: 'danger',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-open"></i> Atualizar
                  </a> -->





                </div>
              </div>







        </div>
            </div>
        <!-- /page content -->
         <!-- compose -->
