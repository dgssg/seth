<?php
include("database/database.php");
$query = "SELECT pmoc_plan.reg_date,pmoc_plan.id,pmoc_report.laudo,pmoc_plan.id_report, pmoc_plan.data_laudo, pmoc_plan.conformidade, pmoc_plan.acao, data_pre, data_ex, resp from  pmoc_plan inner join pmoc_report on pmoc_report.id = pmoc_plan.id_report ";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($reg_date,$id,$pmoc_report,$laudo,$data_laudo,$conformidade,$acao,$data_pre,$data_ex,$resp );
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
 


?>
<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Laudo</th>
                           <th>Data Coleta</th>
                           <th>Data Prevista</th>
                           <th>Data Execusão</th>
                           <th>Responsável</th>
                           <th>Registro</th>
                          <th>Ação</th>
                           
                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($pmoc_report); ?> </td>
                          <td><?php printf($data_laudo); ?> </td>
                          <td><?php printf($data_pre); ?> </td>
                          <td><?php printf($data_ex); ?> </td>
                          <td><?php printf($resp); ?> </td>
                          <td><?php printf($reg_date); ?> </td>
                      
                          <td>  
               
                          <a class="btn btn-app"  href="utilities-pmoc-laudo-register-plan-edit?id=<?php printf($id); ?>" onclick="new PNotify({
                          title: 'Editar',
                          text: 'Abrindo Edição',
                          type: 'sucess',
                          styling: 'bootstrap3'
                        });">
                        <i class="fa fa-edit"></i> Editar
                      </a>
                   
                    <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/utilities-pmoc-laudo-register-plan-trash-backend.php?id=<?php printf($id); ?>';
  }
})
">
                 <i class="fa fa-trash"></i> Excluir</a>
         
                  
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>