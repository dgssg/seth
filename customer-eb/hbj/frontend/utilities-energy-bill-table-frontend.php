<?php
include("database/database.php");
$query = "SELECT unidade_energia_fatura.id, unidade_energia.uniade,month.month, year.yr, unidade_energia_fatura.v_nf FROM unidade_energia_fatura INNER JOIN unidade_energia ON unidade_energia.id = unidade_energia_fatura.id_unidade_energia INNER JOIN month
 ON month.id = unidade_energia_fatura.id_mouth INNER JOIN year ON year.id = unidade_energia_fatura.id_year WHERE unidade_energia_fatura.trash = 1";
//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $id_unidade_energia, $mes, $ano, $v_nf);
   //while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }


?>

<div class="col-md-11 col-sm-11 ">
                <div class="x_panel">

                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">

                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Unidade</th>
                          <th>Mês</th>
                          <th>Ano</th>
                          <th>Valor</th>

                          <th>Ação</th>

                        </tr>
                      </thead>


                      <tbody>
                          <?php   while ($stmt->fetch()) {   ?>
                        <tr>
                          <td><?php printf($id); ?> </td>
                          <td><?php printf($id_unidade_energia); ?> </td>
                          <td><?php printf($mes); ?></td>
                            <td><?php printf($ano); ?></td>
                              <td><?php printf($v_nf); ?></td>



                          <td>
                   <a class="btn btn-app"  href="utilities-energy-bill-edit?bill=<?php printf($id); ?>" onclick="new PNotify({
																title: 'Editar',
																text: 'Abrindo Edição',
																type: 'sucess',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-edit"></i> Editar
                  </a>
                   <a class="btn btn-app"  href="utilities-energy-bill-view?bill=<?php printf($id); ?>" target="_blank" onclick="new PNotify({
																title: 'Visualziar',
																text: 'Visualziar',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-file"></i> Visualizar
                  </a>
                    


                  <a class="btn btn-app"    onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/utilities-energy-bill-trash-backend.php?bill=<?php printf($id); ?>';
  }
})
">
                 <i class="fa fa-trash"></i> Excluir</a>
              <!--    <a class="btn btn-app"  href="" download onclick="new PNotify({
																title: 'Download',
																text: 'Download O.S',
																type: 'info',
																styling: 'bootstrap3'
														});">
                    <i class="fa fa-download"></i> Download
                  </a> -->
                  </td>
                        </tr>
                        <?php   } }  ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>
