<?php
include("database/database.php");
$query = "SELECT maintenance_group.nome AS 'grupo',equipamento.serie,equipamento_familia.fabricante,instituicao.instituicao, instituicao_area.nome,instituicao_localizacao.nome, maintenance_control.id,maintenance_control.id_routine,maintenance_control.data_start,maintenance_control.data_after,maintenance_control.status,maintenance_control.procedure_mp,maintenance_control.reg_date,maintenance_control.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo  FROM maintenance_control INNER JOIN maintenance_routine ON maintenance_routine.id = maintenance_control.id_routine LEFT JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  LEFT JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id  LEFT JOIN instituicao_localizacao on instituicao_localizacao.id = maintenance_routine.id_area LEFT JOIN instituicao_area ON  instituicao_area.id = instituicao_localizacao.id_area  LEFT JOIN instituicao ON instituicao.id = instituicao_area.id_unidade LEFT JOIN maintenance_group ON maintenance_group.id = maintenance_routine.group_routine where  maintenance_routine.mp = 0 and maintenance_routine.trash = 1 order by maintenance_routine.id DESC";
if ($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($grupo,$serie,$fabricante,$instituicao,$area,$setor,$id,$rotina_id,$data_start, $data_after,$status,$procedure_mp,$reg_date,$upgrade,$codigo,$nome,$modelo);



  ?>


  <div class="col-md-11 col-sm-11 ">
    <div class="x_panel">

      <div class="x_content">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box table-responsive">

              <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                         
                      <th>Unidade</th>
                      <th>Setor</th>
                      <th>Area</th> 

                    <th>Rotina</th>
                    <th>Grupo</th>
                    <th>Programada</th>
                    <th>Proxima</th>
                    <th>Status</th>
                    <th>Procedimento</th>
                    <th>Atualização</th>
                    <th>Cadastro</th>

                    <th>Docuentação</th>
                    <th>Controle M.P</th>
                    <th>Cronograma M.P</th>
                    <th>Ação</th>

                  </tr>
                </thead>


                <tbody>
                  <?php   while ($stmt->fetch()) {   ?>
                    <tr>
                      <td ><?php printf($id); ?> </td>
                                                   
                              <td><?php printf($instituicao); ?></td>
                                 <td><?php printf($area); ?></td>
                                 <td>  <?php printf($setor); ?></td>
                      <td ><?php printf($rotina_id); ?> </td>
                       <td ><?php printf($grupo); ?> </td>
                      <td ><?php printf($data_start); ?> </td>
                      <td ><?php printf($data_after); ?> </td>
                      <td><?php if($status==0){printf("Programada");}if($status==1){printf("Aberta");} ?></td>
                        <td><?php printf($procedure_mp); ?></td>
                      <td><?php printf($upgrade); ?></td>
                      <td><?php printf($reg_date); ?></td>


                      <td>

                        <a class="btn btn-app"  href="maintenance-preventive-viwer-control-predial?routine=<?php printf($rotina_id); ?>" target="_blank"  onclick="new PNotify({
                          title: 'Visualizar',
                          text: 'Visualizar Rotina',
                          type: 'info',
                          styling: 'bootstrap3'
                        });">
                        <i class="fa fa-file-pdf-o"></i> Visualizar
                      </a>

                      <a class="btn btn-app"  href="maintenance-preventive-open-tag-control-predial?routine=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                        title: 'Visualizar',
                        text: 'Visualizar Etiqueta',
                        type: 'info',
                        styling: 'bootstrap3'
                      });">
                      <i class="fa fa-qrcode"></i> Etiqueta
                    </a>


                    <a class="btn btn-app"  href="maintenance-preventive-viwer-print-control-predial?routine=<?php printf($rotina_id); ?>" target="_blank"  onclick="new PNotify({
                      title: 'Imprimir',
                      text: 'Imprimir Rotina',
                      type: 'info',
                      styling: 'bootstrap3'
                    });">
                    <i class="fa fa-print"></i> Imprimir
                  </a>
                  <a class="btn btn-app"  href="maintenance-preventive-label-control-predial?routine=<?php printf($rotina_id); ?>" target="_blank" onclick="new PNotify({
                               title: 'Etiqueta',
                               text: 'Abrir Etiqueta',
                               type: 'sucess',
                               styling: 'bootstrap3'
                           });">
                   <i class="fa fa-fax"></i> Rótulo
                 </a>
                </td>
                    <td>
                  <a class="btn btn-app"  href="backend/maintenance-routine-retweet-backend.php?routine=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                    title: 'Manutenção Preventiva',
                    text: 'Manutenção Preventiva Retroativo',
                    type: 'warning',
                    styling: 'bootstrap3'
                  });">
                  <i class="fa fa-retweet"></i> M.P Retroativo
                </a>
                <a class="btn btn-app"  href="backend/maintenance-routine-fast-forward-backend.php?routine=<?php printf($rotina_id); ?>" target="_blank"   onclick="new PNotify({
                  title: 'Manutenção Preventiva',
                  text: 'Manutenção Preventiva Antecipada',
                  type: 'warning',
                  styling: 'bootstrap3'
                });">
                <i class="fa fa-fast-forward"></i> M.P Antecipada
              </a>
              <a class="btn btn-app"  href="maintenance-routine-reschedule-predial?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                           title: 'Manutenção Preventiva',
                           text: 'Manutenção Preventiva Reprogramada',
                           type: 'warning',
                           styling: 'bootstrap3'
                       });">
               <i class="fa fa-clock-o"></i> Reprogramar M.P
             </a>
             <a class="btn btn-app"  href="backend/maintenance-routine-update-date.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                          title: 'Atualização',
                          text: 'Atualização Preventiva Reprogramada',
                          type: 'warning',
                          styling: 'bootstrap3'
                      });">
              <i class="fa fa-calendar"></i> Atualização Data
            </a>
            <a class="btn btn-app"  href="maintenance-routine-new-predial?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                         title: 'Nova',
                         text: 'Nova M.P',
                         type: 'sucess',
                         styling: 'bootstrap3'
                     });">
             <i class="fa fa-plus-square-o"></i> Nova M.P
           </a>
          </td>
          <td>
            <a class="btn btn-app"  href="backend/maintenance-routine-horaire-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                         title: 'Atualização',
                         text: 'Atualização Cronograma',
                         type: 'warning',
                         styling: 'bootstrap3'
                     });">
             <i class="fa fa-calendar-o"></i> Atualização Cronograma
           </a>

        </td>
              <td>
            
            <a class="btn btn-app"  href="backend/maintenance-routine-status-open-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                        title: 'Aberto',
                        text: 'Status M.P Aberto',
                        type: 'warning',
                        styling: 'bootstrap3'
                    });">
           <i class="fa fa-folder-open"></i> Aberto</a>
           <a class="btn btn-app"  href="backend/maintenance-routine-status-reschedule-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                       title: 'Programada',
                       text: 'Status M.P Programada',
                       type: 'warning',
                       styling: 'bootstrap3'
                   });">
          <i class="fa fa-folder"></i> Programado</a>

          <a class="btn btn-app"  href="backend/maintenance-routine-procedure-open-one.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                        title: 'Procedimento',
                        text: 'Procedimento M.P 1',
                        type: 'warning',
                        styling: 'bootstrap3'
                    });">
           <i class="fa fa-angle-up"></i> Procedimento 1</a>
           <a class="btn btn-app"  href="backend/maintenance-routine-procedure-two-backend.php?routine=<?php printf($rotina_id); ?>"  onclick="new PNotify({
                       title: 'Procedimento',
                       text: 'Procedimento M.P 2',
                       type: 'warning',
                       styling: 'bootstrap3'
                   });">
          <i class="fa fa-angle-double-up"></i> Procedimento 2</a>
          
          
            </td>
          </tr>
        <?php   } }  ?>
      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>
</div>
