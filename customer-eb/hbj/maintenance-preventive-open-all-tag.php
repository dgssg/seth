<?php

$instituicaologado = $_POST['instituicaologado'];

$nome = $_POST['nome'];
$modelo = $_POST['modelo'];
$codigo = $_POST['codigo'];
$rotina = $_POST['rotina'];
$programada= $_POST['programada'];
$periodicidade= $_POST['periodicidade'];
 ?>
    <center>


      <div style="width: 7cm; height: 3.8cm; overflow: hidden; text-align: center; line-height: 12px; margin: 2px;" class="quebra">
          <table style="width: 100%; height: 100%; font-size: 10px; font-weight: bold; font-family: Calibri,'Segoe UI','Trebuchet MS'; padding-top: 0px; padding-bottom: 0px; padding-left: 2px; padding-right: 10px;">
              <tr>
<span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblArea" style="font-size:15px;font-weight:bold;">  </span>
                  <td style="vertical-align: middle; margin: 1px; padding: 1px;">

                       <img src="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=www.seth.mksistemasbiomedicos.com.br/customer-ec/<?php printf($instituicaologado); ?>/maintenance-preventive-viwer?id=<?php printf($codigoget); ?>" alt="MK Sistemas Biomedicos" width="100" />

                  </td>
                  <td style="vertical-align: middle; text-align: center; margin: 0px; padding: 0px; padding-top: 0px;">
                      <!--<br />
                      <br />-->
                      <!--<span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblDescricao" style="font-size:12px;font-weight:bold;"> <?php printf($nome)?></span><br />
                      <span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblMarcaModelo" style="font-size:12px;font-weight:bold;"> <?php printf($modelo)?></span>
                      <br />-->

                      <br />
                         <span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblMarcaModelo" style="font-size:18px;font-weight:bold;"> <?php printf($codigo)?></span>
                      <br />
                      <br />
                      <span style="font-size: 15px; font-weight: bold;">Realizada:</span><br />
                      <span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblCodigo" style="font-size:16px;font-weight:bold;"><?php 	 echo date("m-Y", strtotime($programada)); ?></span><br />
                      <span style="font-size: 15px; font-weight: bold;">Proxima:</span><br />
                      <span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblCodigo" style="font-size:16px;font-weight:bold;"><?php

                      if($periodicidade==365){

                        $data_after=date('Y-m-d', strtotime($programada.' + 12 months'));

                      }

                      if($periodicidade==180){

                        $data_after=date('Y-m-d', strtotime($programada.' + 6 months'));

                      }

                      if($periodicidade==30){

                        $data_after=date('Y-m-d', strtotime($programada.' + 1 months'));

                      }

                      if($periodicidade==1){

                        $data_after=date('Y-m-d', strtotime($programada.' + 1 days'));

                      }
                      if($periodicidade==5){

                        date_default_timezone_set('America/Sao_Paulo');
                        function getListaDiasFeriado($ano = null) {
                        
                            if ($ano === null) {
                                $ano = intval(date('Y'));
                            }
                        
                            $pascoa = easter_date($ano); // retorna data da pascoa do ano especificado
                            $diaPascoa = date('j', $pascoa);
                            $mesPacoa = date('n', $pascoa);
                            $anoPascoa = date('Y', $pascoa);
                        
                            $feriados = [
                                // Feriados nacionais fixos
                                mktime(0, 0, 0, 1, 1, $ano),   // Confraternização Universal
                                mktime(0, 0, 0, 4, 21, $ano),  // Tiradentes
                                mktime(0, 0, 0, 5, 1, $ano),   // Dia do Trabalhador
                                mktime(0, 0, 0, 9, 7, $ano),   // Dia da Independência
                                mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida
                                mktime(0, 0, 0, 11, 2, $ano),  // Todos os santos
                                mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica
                                mktime(0, 0, 0, 12, 25, $ano), // Natal
                                //
                                // Feriados variaveis
                                mktime(0, 0, 0, $mesPacoa, $diaPascoa - 48, $anoPascoa), // 2º feria Carnaval
                                mktime(0, 0, 0, $mesPacoa, $diaPascoa - 47, $anoPascoa), // 3º feria Carnaval 
                                mktime(0, 0, 0, $mesPacoa, $diaPascoa - 2, $anoPascoa),  // 6º feira Santa  
                                mktime(0, 0, 0, $mesPacoa, $diaPascoa, $anoPascoa),      // Pascoa
                                mktime(0, 0, 0, $mesPacoa, $diaPascoa + 60, $anoPascoa), // Corpus Christ
                            ];
                        
                            sort($feriados);
                        
                            $listaDiasFeriado = [];
                            foreach ($feriados as $feriado) {
                                $data = date('Y-m-d', $feriado);
                                $listaDiasFeriado[$data] = $data;
                            }
                        
                            return $listaDiasFeriado;
                        }
                        
                        function isFeriado($data) {
                            $listaFeriado = getListaDiasFeriado(date('Y', strtotime($data)));
                            if (isset($listaFeriado[$data])) {
                                return true;
                            }
                        
                            return false;
                        }
                        
                        function getDiasUteis($aPartirDe, $quantidadeDeDias = 30) {
                            $dateTime = new DateTime($aPartirDe);
                        
                            $listaDiasUteis = [];
                            $contador = 0;
                            while ($contador < $quantidadeDeDias) {
                                $dateTime->modify('+1 weekday'); // adiciona um dia pulando finais de semana
                                $data = $dateTime->format('Y-m-d');
                                if (!isFeriado($data)) {
                                    $listaDiasUteis[] = $data;
                                    $contador++;
                                }
                            }
                        
                            return $listaDiasUteis;
                        }
                        $today = $programada;
                        //$today = "2023-01-06";
                        
                        $listaDiasUteis = getDiasUteis($today, 15);
                        $ultimoDia = end($listaDiasUteis);
                        
                        //echo "<pre>";
                        //print_r($listaDiasUteis);
                        //echo "</pre>";
                        
                        //echo "ULTIMO DIA: " . $ultimoDia;
                        //print_r("\n");
                        //echo " DIA: " . $today;
                        //print_r("\n");
                        $i=0;
                        $key=true;
                        do {
                          if($listaDiasUteis[$i] > $today ){
                          //	echo " DIA Proximo: " . $listaDiasUteis[$i] ;
                            $DiasUteis = $listaDiasUteis[$i];
                            $key=false;
                          }
                          else
                          {
                          $i	= $i +1;
                          
                          }
                        } while($key);
                        
                          $data_after = $DiasUteis;
                        
                      }

                      if($periodicidade==7){

                        $data_after=date('Y-m-d', strtotime($programada.' + 7 days'));


                      }

                      if($periodicidade==14){

                        $data_after=date('Y-m-d', strtotime($programada.' + 14 days'));


                      }

                      if($periodicidade==21){

                        $data_after=date('Y-m-d', strtotime($programada.' + 21 days'));


                      }

                      if($periodicidade==28){

                        $data_after=date('Y-m-d', strtotime($programada.' + 28 days'));


                      }

                      if($periodicidade==60){

                        $data_after=date('Y-m-d', strtotime($programada.' + 2 months'));


                      }

                      if($periodicidade==90){

                        $data_after=date('Y-m-d', strtotime($programada.' + 3 months'));


                      }

                      if($periodicidade==120){

                        $data_after=date('Y-m-d', strtotime($programada.' + 4 months'));


                      }

                      if($periodicidade==730){

                        $data_after=date('Y-m-d', strtotime($programada.' + 24 months'));


                      }
                       echo date("m-Y", strtotime($data_after));
                        ?></span><br />
                       <br />
<span id="ctl00_ContentPlaceHolder1_lst_ctl00_lblArea" style="font-size:15px;font-weight:bold;"> MANUTENÇÃO PREVENTIVA <?php printf($rotina); ?> </span>
                  </td>
              </tr>
          </table>
      </div>


    </center>



    </form>





     <script type="text/javascript">

         //jQuery(document).ready(function () {
         function pageLoad() {





        }
        // });
    </script>
    <!-- END JAVASCRIPTS -->


</body>
</html>
