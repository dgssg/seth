<?php

include("../database/database.php");

 
$codigoget= $_POST['id'];
$unidade= $_POST['unidade'];
$cod= $_POST['cod'];
$medidor= $_POST['medidor'];
$fornecimento= $_POST['fornecimento'];
$obs= $_POST['obs'];
$fabricante= $_POST['fabricante'];
$modelo= $_POST['modelo'];
$volt= $_POST['volt'];
$amper= $_POST['amper'];
$type= $_POST['type'];
$conector= $_POST['conector'];
$registro= $_POST['registro'];
$fase= $_POST['fase'];
$serie= $_POST['serie'];
$ativo= $_POST['ativo'];

	$stmt = $conn->prepare("UPDATE unidade_agua SET unidade= ? WHERE id= ?");
	$stmt->bind_param("ss",$unidade,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_agua SET cod= ? WHERE id= ?");
	$stmt->bind_param("ss",$cod,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_agua SET medidor= ? WHERE id= ?");
	$stmt->bind_param("ss",$medidor,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_agua SET fornecimento= ? WHERE id= ?");
	$stmt->bind_param("ss",$fornecimento,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_agua SET obs= ? WHERE id= ?");
	$stmt->bind_param("ss",$obs,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	 
	$stmt = $conn->prepare("UPDATE unidade_agua SET ativo= ? WHERE id= ?");
	$stmt->bind_param("ss",$ativo,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	

	echo "<script>document.location='../utilities-water-unit-search?sweet_salve=1'</script>";
?>