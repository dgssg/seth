<?php

include("../database/database.php");

$id= ($_GET['id']);
$ih= ($_GET['ih']);
$ve= ($_GET['ve']);
$vl_1= ($_GET['vl_1']);
$vl_2= ($_GET['vl_2']);
$vl_3= ($_GET['vl_3']);
$ibn=$ih;  //Esse cálculo só se aplica se a Incerteza for do tipo B, e o tipo for, (resolução ou outras), então é a estimativa convertida dividida pela distribuição

$vl_avg= ($vl_1 + $vl_2 + $vl_3)/3; //Valor medio

$erro=abs($ve-$vl_avg); //Erro

$dp=sqrt((pow(($vl_1-$vl_avg),2)+pow(($vl_2-$vl_avg),2)+pow(($vl_3-$vl_avg),2))/3); // Desvio Padrão

$ia=$dp/(sqrt(3)); // Incerteza do Tipo A / Repetitividade


$ib= $ibn; //É a raiz quadrada das somas das incertezas do tipo B ao quadrado.

$ic=sqrt((pow($ib,2)+pow($ia,2))); //É a raiz quadrada da soma da incerteza do tipo B ao quadrado mais a incerteza do tipo A ao quadrado.

$ie=$ic*$k;//É a Incerteza Combinada vezes o fator de Abrangência.
$erro_ld=trim($erro_ld);
//if($erro_ld=="0"){
 $ap= $ie+$vl_avg;
 $ac=$vl_avg+$d_1;


 if($ap<$ac){
     $status=1;
 }
 if($ap>$ac){
     $status=0;
 }


$stmt = $conn->prepare("UPDATE calibration_report SET vl_avg = ? WHERE id= ?");
//if(!$req){
  //    echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
   //}
$stmt->bind_param("si",$vl_avg,$id);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET erro = ? WHERE id= ?");
$stmt->bind_param("si",$erro,$id);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET dp = ? WHERE id= ?");
$stmt->bind_param("si",$dp,$id);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ia = ? WHERE id= ?");
$stmt->bind_param("si",$ia,$id);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ie = ? WHERE id= ?");
$stmt->bind_param("si",$ie,$id);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET k = ? WHERE id= ?");
$stmt->bind_param("si",$k,$id);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ib = ? WHERE id= ?");
$stmt->bind_param("si",$ib,$id);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ic = ? WHERE id= ?");
$stmt->bind_param("si",$ic,$id);
$execval = $stmt->execute();
$stmt->close();
//$stmt = $conn->prepare("UPDATE calibration_report SET id_calibration_parameter_tools = ? WHERE id= ?");
//$stmt->bind_param("si",$row_cat_post['id']_calibration_parameter_tools,$id);
//$execval = $stmt->execute();
//$stmt->close();
//$stmt = $conn->prepare("UPDATE calibration_report SET id_calibration_parameter_tools_dados = ? WHERE id= ?");
//$stmt->bind_param("si",$tools,$id);
//$execval = $stmt->execute();
//$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_report SET status = ? WHERE id= ?");
$stmt->bind_param("si",$status,$id);
$execval = $stmt->execute();
$stmt->close();

echo "<script>window.close();</script>";

?>
