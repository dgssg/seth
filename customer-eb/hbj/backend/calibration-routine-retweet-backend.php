<?php

date_default_timezone_set('America/Sao_Paulo');



$codigoget= $_GET['routine'];
include("../database/database.php");

$id_status=1;
$procedure_mp=1;

$query = "SELECT calibration_routine.id,calibration_routine.data_start, calibration_routine.periodicidade,calibration_routine.reg_date,calibration_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM calibration_routine INNER JOIN equipamento ON equipamento.id = calibration_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id WHERE calibration_routine.id like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
   $stmt->execute();
   $stmt->bind_result($rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
   }
}
$stmt->close();
$date=$data_start;
$today = date("Y-m-d");


//echo " <br />";

switch($periodicidade){
   case 1:
       do{
    //  echo " <br />";
//   printf($date);
//    echo " <br />";

 $stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 1 days'));



} while($date < $today);
       break;
       case 5:
        do{
     //  echo " <br />";
 //   printf($date);
//    echo " <br />";

  $stmt = $conn->prepare("INSERT INTO maintenance_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
 //      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
function getListaDiasFeriado($ano = null) {

  if ($ano === null) {
      $ano = intval(date('Y'));
  }

  $pascoa = easter_date($ano); // retorna data da pascoa do ano especificado
  $diaPascoa = date('j', $pascoa);
  $mesPacoa = date('n', $pascoa);
  $anoPascoa = date('Y', $pascoa);

  $feriados = [
      // Feriados nacionais fixos
      mktime(0, 0, 0, 1, 1, $ano),   // Confraternização Universal
      mktime(0, 0, 0, 4, 21, $ano),  // Tiradentes
      mktime(0, 0, 0, 5, 1, $ano),   // Dia do Trabalhador
      mktime(0, 0, 0, 9, 7, $ano),   // Dia da Independência
      mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida
      mktime(0, 0, 0, 11, 2, $ano),  // Todos os santos
      mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica
      mktime(0, 0, 0, 12, 25, $ano), // Natal
      //
      // Feriados variaveis
      mktime(0, 0, 0, $mesPacoa, $diaPascoa - 48, $anoPascoa), // 2º feria Carnaval
      mktime(0, 0, 0, $mesPacoa, $diaPascoa - 47, $anoPascoa), // 3º feria Carnaval 
      mktime(0, 0, 0, $mesPacoa, $diaPascoa - 2, $anoPascoa),  // 6º feira Santa  
      mktime(0, 0, 0, $mesPacoa, $diaPascoa, $anoPascoa),      // Pascoa
      mktime(0, 0, 0, $mesPacoa, $diaPascoa + 60, $anoPascoa), // Corpus Christ
  ];

  sort($feriados);

  $listaDiasFeriado = [];
  foreach ($feriados as $feriado) {
      $data = date('Y-m-d', $feriado);
      $listaDiasFeriado[$data] = $data;
  }

  return $listaDiasFeriado;
}

function isFeriado($data) {
  $listaFeriado = getListaDiasFeriado(date('Y', strtotime($data)));
  if (isset($listaFeriado[$data])) {
      return true;
  }

  return false;
}

function getDiasUteis($aPartirDe, $quantidadeDeDias = 30) {
  $dateTime = new DateTime($aPartirDe);

  $listaDiasUteis = [];
  $contador = 0;
  while ($contador < $quantidadeDeDias) {
      $dateTime->modify('+1 weekday'); // adiciona um dia pulando finais de semana
      $data = $dateTime->format('Y-m-d');
      if (!isFeriado($data)) {
          $listaDiasUteis[] = $data;
          $contador++;
      }
  }

  return $listaDiasUteis;
}
$today2 = $date;
//$today = "2023-01-06";

$listaDiasUteis = getDiasUteis($today2, 15);
$ultimoDia = end($listaDiasUteis);

//echo "<pre>";
//print_r($listaDiasUteis);
//echo "</pre>";

//echo "ULTIMO DIA: " . $ultimoDia;
//print_r("\n");
//echo " DIA: " . $today;
//print_r("\n");
$i=0;
$key=true;
do {
 if($listaDiasUteis[$i] > $today2 ){
 //	echo " DIA Proximo: " . $listaDiasUteis[$i] ;
    $DiasUteis = $listaDiasUteis[$i];
    $key=false;
 }
 else
 {
 $i	= $i +1;
 
 }
} while($key);

$date = $DiasUteis;


  // $date=date('Y-m-d', strtotime($date.' + 1 days'));



} while($date < $today);
        break;
   case 7:
       do{
    //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 7 days'));



} while($date < $today);
       break;
   case 14:
        do{
    //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 14 days'));



} while($date < $today);
       break;
   case 21:
       do{
   //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 21 days'));



} while($date < $today);
       break;
   case 28:
       do{
   //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 28 days'));


} while($date < $today);
       break;
   case 30:

          do{
  //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 1 months'));


} while($date < $today);
       break;
   case 60:
       do{
   //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 2 months'));


} while($date < $today);
       break;
   case 90:
         do{
   //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 3 months'));


} while($date < $today);
       break;
   case 120:
         do{
   //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 4 months'));


} while($date < $today);

       break;
   case 180:

       do{
 //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 6 months'));
} while($date < $today);

       break;
   case 365:
         do{
   //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 12 months'));


} while($date < $today);
       break;
   case 730:
        do{
   //  echo " <br />";
//   printf($date);
//    echo " <br />";

$stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
//if(!$req){
//      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
//    }
$stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);

$execval = $stmt->execute();
$stmt->close();
  $date=date('Y-m-d', strtotime($date.' + 24 months'));


} while($date < $today);
       break;
  case 1095:
    do{
      //  echo " <br />";
      //   printf($date);
      //    echo " <br />";
      
      $stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
      //if(!$req){
      //      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
      //    }
      $stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);
      
      $execval = $stmt->execute();
      $stmt->close();
      $date=date('Y-m-d', strtotime($date.' + 36 months'));
      
      
    } while($date < $today);
    break;
  case 1460:
    do{
      //  echo " <br />";
      //   printf($date);
      //    echo " <br />";
      
      $stmt = $conn->prepare("INSERT INTO calibration_preventive( id_routine, date_start,id_status,procedure_mp) VALUES (?,?,?,?)");
      //if(!$req){
      //      echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";
      //    }
      $stmt->bind_param("ssss",$codigoget,$date,$id_status,$procedure_mp);
      
      $execval = $stmt->execute();
      $stmt->close();
      $date=date('Y-m-d', strtotime($date.' + 48 months'));
      
      
    } while($date < $today);
    break;
}




echo "<script>window.close();</script>";

//header ("Location: ../index.php");

?>
