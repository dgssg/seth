<?php

include("../database/database.php");

$codigoget = ($_GET["id"]);
$fornecedor = $_POST['fornecedor'];
$date_mp_start = $_POST['date_start'];
$date_mp_end = $_POST['date_end'];
$time_mp = $_POST['time'];
$colaborador = $_POST['colaborador'];
$message_mp = $_POST['message_mp'];
$message_tc = $_POST['message_tc'];
$routine = $_POST['routine'];
$periodicidade = $_POST['periodicidade'];
$programada= $_POST['programada'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];


$anexo=$_COOKIE['anexo'];

$status="3";
$status2=0;

$stmt = $conn->prepare("UPDATE calibration_preventive SET id_fornecedor= ? WHERE id= ?");
$stmt->bind_param("ss",$fornecedor,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET date_mp_start= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_start,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET date_mp_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET time_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$time_mp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET id_status= ? WHERE id= ?");
$stmt->bind_param("ss",$status,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET file= ? WHERE id= ?");
$stmt->bind_param("ss",$anexo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET id_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$colaborador,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET obs_mp= ? WHERE id= ?");
$stmt->bind_param("ss",$message_mp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET obs_tc= ? WHERE id= ?");
$stmt->bind_param("ss",$message_tc,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET date_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET temp= ? WHERE id= ?");
$stmt->bind_param("ss",$temp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET hum= ? WHERE id= ?");
$stmt->bind_param("ss",$hum,$codigoget);
$execval = $stmt->execute();
$stmt->close();


if($periodicidade==365){

  $data_after=date('Y-m-d', strtotime($programada.' + 12 months'));

}

if($periodicidade==180){

  $data_after=date('Y-m-d', strtotime($programada.' + 6 months'));

}

if($periodicidade==30){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 months'));

}

if($periodicidade==1){

  $data_after=date('Y-m-d', strtotime($programada.' + 1 days'));

}
if($periodicidade==5){

  date_default_timezone_set('America/Sao_Paulo');
  function getListaDiasFeriado($ano = null) {
  
      if ($ano === null) {
          $ano = intval(date('Y'));
      }
  
      $pascoa = easter_date($ano); // retorna data da pascoa do ano especificado
      $diaPascoa = date('j', $pascoa);
      $mesPacoa = date('n', $pascoa);
      $anoPascoa = date('Y', $pascoa);
  
      $feriados = [
          // Feriados nacionais fixos
          mktime(0, 0, 0, 1, 1, $ano),   // Confraternização Universal
          mktime(0, 0, 0, 4, 21, $ano),  // Tiradentes
          mktime(0, 0, 0, 5, 1, $ano),   // Dia do Trabalhador
          mktime(0, 0, 0, 9, 7, $ano),   // Dia da Independência
          mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida
          mktime(0, 0, 0, 11, 2, $ano),  // Todos os santos
          mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica
          mktime(0, 0, 0, 12, 25, $ano), // Natal
          //
          // Feriados variaveis
          mktime(0, 0, 0, $mesPacoa, $diaPascoa - 48, $anoPascoa), // 2º feria Carnaval
          mktime(0, 0, 0, $mesPacoa, $diaPascoa - 47, $anoPascoa), // 3º feria Carnaval 
          mktime(0, 0, 0, $mesPacoa, $diaPascoa - 2, $anoPascoa),  // 6º feira Santa  
          mktime(0, 0, 0, $mesPacoa, $diaPascoa, $anoPascoa),      // Pascoa
          mktime(0, 0, 0, $mesPacoa, $diaPascoa + 60, $anoPascoa), // Corpus Christ
      ];
  
      sort($feriados);
  
      $listaDiasFeriado = [];
      foreach ($feriados as $feriado) {
          $data = date('Y-m-d', $feriado);
          $listaDiasFeriado[$data] = $data;
      }
  
      return $listaDiasFeriado;
  }
  
  function isFeriado($data) {
      $listaFeriado = getListaDiasFeriado(date('Y', strtotime($data)));
      if (isset($listaFeriado[$data])) {
          return true;
      }
  
      return false;
  }
  
  function getDiasUteis($aPartirDe, $quantidadeDeDias = 30) {
      $dateTime = new DateTime($aPartirDe);
  
      $listaDiasUteis = [];
      $contador = 0;
      while ($contador < $quantidadeDeDias) {
          $dateTime->modify('+1 weekday'); // adiciona um dia pulando finais de semana
          $data = $dateTime->format('Y-m-d');
          if (!isFeriado($data)) {
              $listaDiasUteis[] = $data;
              $contador++;
          }
      }
  
      return $listaDiasUteis;
  }
  $today = $programada;
  //$today = "2023-01-06";
  
  $listaDiasUteis = getDiasUteis($today, 15);
  $ultimoDia = end($listaDiasUteis);
  
  //echo "<pre>";
  //print_r($listaDiasUteis);
  //echo "</pre>";
  
  //echo "ULTIMO DIA: " . $ultimoDia;
  //print_r("\n");
  //echo " DIA: " . $today;
  //print_r("\n");
  $i=0;
  $key=true;
  do {
    if($listaDiasUteis[$i] > $today ){
    //	echo " DIA Proximo: " . $listaDiasUteis[$i] ;
      $DiasUteis = $listaDiasUteis[$i];
      $key=false;
    }
    else
    {
    $i	= $i +1;
    
    }
  } while($key);
  
    $data_after = $DiasUteis;
  
}

if($periodicidade==7){

  $data_after=date('Y-m-d', strtotime($programada.' + 7 days'));


}

if($periodicidade==14){

  $data_after=date('Y-m-d', strtotime($programada.' + 14 days'));


}

if($periodicidade==21){

  $data_after=date('Y-m-d', strtotime($programada.' + 21 days'));


}

if($periodicidade==28){

  $data_after=date('Y-m-d', strtotime($programada.' + 28 days'));


}

if($periodicidade==60){

  $data_after=date('Y-m-d', strtotime($programada.' + 2 months'));


}

if($periodicidade==90){

  $data_after=date('Y-m-d', strtotime($programada.' + 3 months'));


}

if($periodicidade==120){

  $data_after=date('Y-m-d', strtotime($programada.' + 4 months'));


}

if($periodicidade==730){

  $data_after=date('Y-m-d', strtotime($programada.' + 24 months'));


}
  if($periodicidade==1095){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 36 months'));
    
    
  }
  if($periodicidade==1460){
    
    $data_after=date('Y-m-d', strtotime($programada.' + 48 months'));
    
    
  }

$query="SELECT procedure_mp FROM calibration_preventive WHERE id = $codigoget";
if($stmt = $conn->prepare($query)) {
  $stmt->execute();
  $stmt->bind_result($procedure_mp);
  while ($stmt->fetch()) {
    $procedure_mp=$procedure_mp;
  }
}
switch ($procedure_mp) {
  case '1':
  $stmt = $conn->prepare("UPDATE calibration_control SET data_after = ? WHERE id_routine= ?");
  $stmt->bind_param("ss",$data_after,$routine);
  $execval = $stmt->execute();
  $stmt->close();

  $stmt = $conn->prepare("UPDATE calibration_control SET status = ? WHERE id_routine= ?");
  $stmt->bind_param("ss",$status2,$routine);
  $execval = $stmt->execute();
  $stmt->close();
  break;





}




setcookie('anexo', null, -1);
header('Location: ../flow-calibration');
?>
