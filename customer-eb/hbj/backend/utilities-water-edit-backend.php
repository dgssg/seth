<?php

include("../database/database.php");

$codigoget = $_POST['id'];
$empresa = $_POST['empresa'];
$cnpj = $_POST['cnpj'];
$seguimento = $_POST['seguimento'];
$adress = $_POST['adress'];
$city = $_POST['city'];
$state = $_POST['state'];
$cep = $_POST['cep'];
$contato = $_POST['contato'];
$telefone_1 = $_POST['telefone_1'];
$telefone_2 = $_POST['telefone_2'];
$obervacao = $_POST['obervacao'];
$bairro = $_POST['bairro'];
$ibge = $_POST['ibge'];




$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET nome = ? WHERE id= ?");
$stmt->bind_param("ss",$empresa,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET cnpj = ? WHERE id= ?");
$stmt->bind_param("ss",$cnpj,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET seguimento = ? WHERE id= ?");
$stmt->bind_param("ss",$seguimento,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET adress = ? WHERE id= ?");
$stmt->bind_param("ss",$adress,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET city = ? WHERE id= ?");
$stmt->bind_param("ss",$city,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET state = ? WHERE id= ?");
$stmt->bind_param("ss",$state,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET cep = ? WHERE id= ?");
$stmt->bind_param("ss",$cep,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET contato = ? WHERE id= ?");
$stmt->bind_param("ss",$contato,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET telefone_1 = ? WHERE id= ?");
$stmt->bind_param("ss",$telefone_1,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET telefone_2 = ? WHERE id= ?");
$stmt->bind_param("ss",$telefone_2,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET observacao = ? WHERE id= ?");
$stmt->bind_param("ss",$obervacao,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET bairro = ? WHERE id= ?");
$stmt->bind_param("ss",$bairro,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE unidade_agua_empresa SET ibge = ? WHERE id= ?");
$stmt->bind_param("ss",$ibge,$codigoget);
$execval = $stmt->execute();
$stmt->close();

echo "<script>alert('Atualizado!');document.location='../utilities-water'</script>";
 
?>
