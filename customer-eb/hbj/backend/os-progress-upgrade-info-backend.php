<?php

include("../database/database.php");
$codigoget = ($_GET["os"]);
$tecnico= $_POST['colaborador'];
$fornecedor= $_POST['fornecedor'];
$chamado= $_POST['chamado'];
$prioridade= $_POST['prioridade'];
$backlog= $_POST['backlog'];
$id_category=$_POST['id_category'];
$id_service=$_POST['id_service'];
$standing= $_POST['standing'];
$room_close= $_POST['room_close'];

$stmt = $conn->prepare("UPDATE os SET room_close= ? WHERE id= ?");
$stmt->bind_param("ss",$room_close,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET id_category= ? WHERE id= ?");
$stmt->bind_param("ss",$id_category,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET id_servico= ? WHERE id= ?");
$stmt->bind_param("ss",$id_service,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET id_tecnico= ? WHERE id= ?");
$stmt->bind_param("ss",$tecnico,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET id_fornecedor= ? WHERE id= ?");
$stmt->bind_param("ss",$fornecedor,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET id_prioridade= ? WHERE id= ?");
$stmt->bind_param("ss",$prioridade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET id_sla= ? WHERE id= ?");
$stmt->bind_param("ss",$prioridade,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET id_chamado= ? WHERE id= ?");
$stmt->bind_param("ss",$chamado,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET time_backlog= ? WHERE id= ?");
$stmt->bind_param("ss",$backlog,$codigoget);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE os SET standing= ? WHERE id= ?");
$stmt->bind_param("ss",$standing,$codigoget);
$execval = $stmt->execute();
$stmt->close();

//header('Location: ../os-progress-upgrade?os='.$codigoget);
echo "<script>document.location='../os-progress-upgrade?sweet_salve=1&div_abertura=1&os=$codigoget'</script>";

?>