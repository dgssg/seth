<?php
include("database/database.php");
include("database/database_cal.php");
$codigoget = ($_GET["id"]);
$query = "SELECT unidade_gas_glp_suport.id_unidade,unidade_gas_glp_suport.id_setor,unidade_gas_glp_suport.id_colaborador,colaborador.primeironome,colaborador.ultimonome,instituicao_area.nome,unidade_gas_glp_suport.class, equipamento_grupo.nome, unidade_gas_glp_suport.id, unidade_gas_glp_suport.id_equipamento_grupo, unidade_gas_glp_suport.titulo, unidade_gas_glp_suport.ver, unidade_gas_glp_suport.file, unidade_gas_glp_suport.data_now, unidade_gas_glp_suport.data_before, unidade_gas_glp_suport.upgrade, unidade_gas_glp_suport.reg_date FROM unidade_gas_glp_suport  LEFT JOIN equipamento_grupo ON equipamento_grupo.id = unidade_gas_glp_suport.id_equipamento_grupo LEFT JOIN instituicao_area ON instituicao_area.id = unidade_gas_glp_suport.id_setor LEFT JOIN colaborador ON colaborador.id = unidade_gas_glp_suport.id_colaborador WHERE unidade_gas_glp_suport.id = $codigoget";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
       $stmt->bind_result($id_unidade,$id_setor,$id_colaborador,$primeironome,$ultimonome,$instituicao_area,$class,$nome,$id, $id_equipamento_grupo,$titulo,$ver,$file,$data_now,$data_before,$upgrade,$reg_date);


   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
   }
}
  
  $sweet_salve = ($_GET["sweet_salve"]);
  $sweet_delet = ($_GET["sweet_delet"]);
  if ($sweet_delet == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
    Swal.fire({
        position: \'top-end\',
        icon: \'success\',
        title: \'Seu trabalho foi deletado!\',
        showConfirmButton: false,
        timer: 1500
    });
</script>';
    
    
  }
  if ($sweet_salve == 1) {
    // Redirecionamento para a âncora "registro" usando JavaScript
    echo '<script>
        Swal.fire({
            position: \'top-end\',
            icon: \'success\',
            title: \'Seu trabalho foi salvo!\',
            showConfirmButton: false,
            timer: 1500
        });
    </script>';
    
    
  }
function chaveAlfaNumerica($QuantidadeDeCaracteresDaChave){
  $res = implode('', range('A', 'z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxiz
  $con = 1;
  $var = '';
  while($con < $QuantidadeDeCaracteresDaChave ){
      $n = rand(0, 57); 
      if (($n == 26) || ($n == 27) || ($n == 28) || ($n == 29) || ($n == 30) || ($n == 31)){
  }else{
      $var = $var.$n.$res[$n];
      $con++;
      }
  }
      $var = str_replace(['i', 'l'], '', $var);

    return substr($var, 0, $QuantidadeDeCaracteresDaChave);
  }
  //chamando a função.
 // echo chaveAlfaNumerica(5);
//   echo nl2br("\r\n");
/* A uniqid, like: 4b3403665fea6 
printf("uniqid(): %s\r\n", uniqid());

/* We can also prefix the uniqid, this the same as 
* doing:
*
* $uniqid = $prefix . uniqid();
* $uniqid = uniqid($prefix);

printf("uniqid('php_'): %s\r\n", uniqid('php_'));

/* We can also activate the more_entropy parameter, which is 
* required on some systems, like Cygwin. This makes uniqid()
* produce a value like: 4b340550242239.64159797

printf("uniqid('', true): %s\r\n", uniqid('', true));
*/
class UUID {
public static function v3($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = md5($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 3
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function v4() {
return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

// 32 bits for "time_low"
mt_rand(0, 0xffff), mt_rand(0, 0xffff),

// 16 bits for "time_mid"
mt_rand(0, 0xffff),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 4
mt_rand(0, 0x0fff) | 0x4000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
mt_rand(0, 0x3fff) | 0x8000,

// 48 bits for "node"
mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
);
}

public static function v5($namespace, $name) {
if(!self::is_valid($namespace)) return false;

// Get hexadecimal components of namespace
$nhex = str_replace(array('-','{','}'), '', $namespace);

// Binary Value
$nstr = '';

// Convert Namespace UUID to bits
for($i = 0; $i < strlen($nhex); $i+=2) {
$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
}

// Calculate hash value
$hash = sha1($nstr . $name);

return sprintf('%08s-%04s-%04x-%04x-%12s',

// 32 bits for "time_low"
substr($hash, 0, 8),

// 16 bits for "time_mid"
substr($hash, 8, 4),

// 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

// 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

// 48 bits for "node"
substr($hash, 20, 12)
);
}

public static function is_valid($uuid) {
return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
}

// Usage
// Named-based UUID.

$v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
$v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');

// Pseudo-random UUID

$v4uuid = UUID::v4();

//echo $v4uuid; //chave da assinatura
?>
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>
   <link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>  Alteração <small>Contingência</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cabeçalho <small>Contingência</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                 <form action="backend/utilities-gas-glp-suport-edit-backend.php?id=<?php printf($codigoget);?>" method="post">

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="suport" class="form-control" name="suport" type="hidden" value="<?php printf($codigoget); ?> " >
                        </div>
                      </div>

                   
                   <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Titulo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input  class="form-control" type="text"  id="titulo" name="titulo"  value="<?php printf($titulo); ?>" >
                        </div>
                      </div>

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Versão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="ver" class="form-control" type="text" name="ver"  value="<?php printf($ver); ?>" >
                        </div>
                      </div>

                    
                      <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Unidade <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="id_unidade" id="id_unidade"  placeholder="Setor">
        <option value="">Selecione uma Unidade</option>
        <?php



$sql = "SELECT  id, instituicao FROM instituicao WHERE trash = 1";


if ($stmt = $conn->prepare($sql)) {
$stmt->execute();
$stmt->bind_result($id,$instituicao);
while ($stmt->fetch()) {
?>

<option value="<?php printf($id);?>	" <?php if($id == $id_unidade){ printf("selected");} ?> ><?php printf($instituicao);?>	</option>
 <?php
// tira o resultado da busca da memória
}

                     }
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Unidade "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_unidade').select2();
  });
  </script>
                      <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nf">Setor <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-right" name="$id_setor" id="$id_setor"  placeholder="Setor">
        <option value="">Selecione um Setor</option>
        <?php



$sql = "SELECT  id, codigo,nome FROM instituicao_area WHERE trash = 1 ";


if ($stmt = $conn->prepare($sql)) {
  $stmt->execute();
  $stmt->bind_result($id,$codigo,$nome);
  while ($stmt->fetch()) {
    ?>
    <option value="<?php printf($id);?>	" <?php if($id == $id_setor){printf("selected");};?>><?php printf($codigo);?> <?php printf($nome);?>	</option>
    <?php
    // tira o resultado da busca da mem��ria
  }

}
$stmt->close();

?>
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione Setor "></span>


    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#$id_setor').select2();
  });
  </script>
<div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Colaborador ">Responsável 
    </label>
    <div class="col-md-4 col-sm-4 ">
      <select type="text" class="form-control has-feedback-right" name="id_colaborador" id="id_colaborador"  placeholder="Responsável" >
        <option value="	">Selecione o Responsável</option>


        <?php



        $sql = "SELECT  id, primeironome,ultimonome FROM colaborador WHERE trash = 1 ";


        if ($stmt = $conn->prepare($sql)) {
          $stmt->execute();
          $stmt->bind_result($id,$primeironome,$ultimonome);
          while ($stmt->fetch()) {
            ?>
            <option value="<?php printf($id);?>	" <?php if($id == $id_colaborador){printf("selected");};?>><?php printf($primeironome);?> <?php printf($ultimonome);?>	</option>
            <?php
            // tira o resultado da busca da mem��ria
          }

        }
        $stmt->close();

        ?>
      </select>
    </div>
  </div>









  <script>
  $(document).ready(function() {
    $('#id_colaborador').select2();
  });
  </script>
  
  

                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_now"  type="date" name="data_now"  value="<?php printf($data_now); ?>" class="form-control" >
                        </div>
                      </div>

                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data da Proxima Revisão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="data_before"  type="date" name="data_before"  value="<?php printf($data_before); ?>" class="form-control" >
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cadastro" class="form-control" type="text" name="cadastro"  value="<?php printf($reg_date); ?>" readonly="readonly">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="upgrade" class="form-control" type="text" name="upgrade"  value="<?php printf($upgrade); ?>" readonly="readonly">
                        </div>
                      </div>

                       <div class="form-group row">
                       <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-3 col-sm-3 ">
                            <center>
                           <button class="btn btn-sm btn-success" type="submit" onclick="new PNotify({
                                     title: 'Registrado',
                                     text: 'Informações registrada!',
                                     type: 'success',
                                     styling: 'bootstrap3'
                                 });">Salvar Informações</button>
                         </center>
                        </div>
                      </div>


                                  </form>



                  </div>
                </div>
              </div>
	       </div>


            <div class="clearfix"></div>



          <div class="x_panel">
                <div class="x_title">
                  <h2>Contingências</h2>
                  <ul class="nav navbar-right panel_toolbox">
                       <li><a class="badge bg-green pull-right" data-toggle="modal" data-target=".bs-example-modal-lg1"  ><i class="fa fa-plus"></i></a>
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <!--arquivo-->
       <?php
$query="SELECT
           
GROUP_CONCAT(DISTINCT instituicao.instituicao ORDER BY instituicao.instituicao ASC) AS unidade,

GROUP_CONCAT(DISTINCT instituicao_area.id ORDER BY instituicao_area.id ASC) AS areas,
COUNT( DISTINCT equipamento.id),
unidade_gas_glp_suport_itens.id,
equipamento_grupo.nome,
fornecedor.empresa,
fornecedor.contato,
fornecedor.telefone_1,
fornecedor.telefone_2,
unidade_gas_glp_suport_itens.obs
FROM unidade_gas_glp_suport_itens
LEFT JOIN equipamento_grupo ON equipamento_grupo.id = unidade_gas_glp_suport_itens.id_grupo
LEFT JOIN fornecedor ON fornecedor.id = unidade_gas_glp_suport_itens.id_fornecedor
LEFT JOIN equipamento_familia ON equipamento_familia.id_equipamento_grupo = equipamento_grupo.id
LEFT JOIN equipamento ON equipamento.id_equipamento_familia = equipamento_familia.id
LEFT JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao
LEFT JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area
LEFT JOIN instituicao ON instituicao.id = instituicao_localizacao.id_unidade
WHERE unidade_gas_glp_suport_itens.id_documentation_suport LIKE $codigoget
GROUP BY equipamento_grupo.id
ORDER BY unidade_gas_glp_suport_itens.id ASC;
";
$row=1;
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($unidade,$setor,$count,$id_cont,$id_grupo,$id_fornecedor,$contato,$telefone1,$telefone2,$obs);

?>
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                     
                          <th>Grupo</th>
                          <th>Quantidade</th>
                          <th>Iten</th>
                          
                    
                          

                          

                         
                          <th>Edição</th>

                        </tr>
                      </thead>
                      <tbody>
                             <?php  while ($stmt->fetch()) { ?>

                        <tr>
                          <th scope="row"><?php printf($row); ?></th>
                          <td><?php printf($id_grupo); ?></td>
                          <td><?php printf($count); ?></td>
                          <td>
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                              <thead>
                                <tr>
                                  <th>Ação</th>
                                  
                                  <th>Local</th>
                                  <th>Edição</th>

                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                  $query_cal="SELECT unidade_gas_glp_suport_itens_action.id,
                                  unidade_gas_glp_suport_itens_action.obs,
                                  unidade_gas_glp_suport_itens_action.id_mod,
                                  GROUP_CONCAT(DISTINCT instituicao_area.nome) AS areas,
                                  GROUP_CONCAT(DISTINCT fornecedor.empresa) AS empresas,
                                  GROUP_CONCAT(DISTINCT fornecedor.contato) AS contatos,
                                  GROUP_CONCAT(DISTINCT fornecedor.telefone_1) AS telefone_1,
                                  GROUP_CONCAT(DISTINCT fornecedor.telefone_2) AS telefone_2
                                  FROM unidade_gas_glp_suport_itens_action
                                  LEFT JOIN instituicao_area ON FIND_IN_SET(instituicao_area.id, unidade_gas_glp_suport_itens_action.id_setor)
                                  LEFT JOIN fornecedor ON FIND_IN_SET(fornecedor.id, unidade_gas_glp_suport_itens_action.id_fornecedor)
                                  WHERE unidade_gas_glp_suport_itens_action.id_documentation_suport_itens =  $id_cont group by unidade_gas_glp_suport_itens_action.id
                                  
";
                                  $row=1;
                                  if ($stmt_cal = $conn_cal->prepare($query_cal)) {
                                    $stmt_cal->execute();
                                    $stmt_cal->bind_result($id,$obs_action,$action,$setor_action,$id_fornecedor_action,$contato_action,$telefone1_action,$telefone2_action);
                                    
                                 while ($stmt_cal->fetch()) { ?>
                                <tr>
                                <td><?php printf($obs_action); ?></td>
                                <?php
                                  if($action == 1){
                                ?>
                                <td><?php printf($setor_action); ?></td>
                                <?php
                                }
                                ?>
                                <?php
                                  if($action == 2){
                                ?>
                                <td><?php printf($id_fornecedor_action); ?> - <?php printf($contato_action); ?> - <?php printf($telefone1_action); ?> -<?php printf($telefone2_action); ?> </td>
                                <?php
                                  }
                                ?>
                                  <td>                            <a class="btn btn-app"   onclick="
                                    Swal.fire({
                                      title: 'Tem certeza?',
                                      text: 'Você não será capaz de reverter isso!',
                                      icon: 'warning',
                                      showCancelButton: true,
                                      confirmButtonColor: '#3085d6',
                                      cancelButtonColor: '#d33',
                                      confirmButtonText: 'Sim, Deletar!'
                                    }).then((result) => {
                                      if (result.isConfirmed) {
                                        Swal.fire(
                                          'Deletando!',
                                          'Seu arquivo será excluído.',
                                          'success'
                                        ),
                                        window.location = 'backend/utilities-gas-glp-suport-itens-action-trash.php?id=<?php printf($id); ?>&hfmea=<?php printf($codigoget); ?>';
                                      }
                                    })
                                  ">
                                    <i class="glyphicon glyphicon-trash"></i> Excluir
                                  </a>
                                 <!--   <a class="btn btn-app" href="utilities-gas-glp-suport-itens-itens-edit?id=<?php printf($id); ?>&hfmea=<?php printf($codigoget); ?>" onclick=" new PNotify({
                                      title: 'Editar',
                                      text: 'Abrindo Edição',
                                      type: 'success',
                                      styling: 'bootstrap3'
                                    });">
                                      <i class="fa fa-edit"></i> Editar
                                    </a> -->

</td>
                                </tr>
                                <?php }?>
                                <?php }?>
                                
                              </tbody>
                            </table>  
                          </td>
                          

                      
                       
                          <td>
<a  class="btn btn-app" data-toggle="modal" data-target=".bs-example-modal-lg1<?php printf($id_cont); ?>"  onclick="new PNotify({
title: 'Ação',
text: 'Ação',
type: 'info',
styling: 'bootstrap3'
});" >
<i class="fa fa-plus"></i> Ação
</a>
                            <a class="btn btn-app"   onclick="
                  Swal.fire({
  title: 'Tem certeza?',
  text: 'Você não será capaz de reverter isso!',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deletando!',
      'Seu arquivo será excluído.',
      'success'
    ),
window.location = 'backend/utilities-gas-glp-suport-itens-trash.php?id=<?php printf($id_cont); ?>&hfmea=<?php printf($codigoget); ?>';
  }
})
">
                               <i class="glyphicon glyphicon-trash"></i> Excluir
                             </a>
                         <!--   <a class="btn btn-app" href="utilities-gas-glp-suport-itens-edit?id=<?php printf($id_cont); ?>&hfmea=<?php printf($codigoget); ?>" onclick=" new PNotify({
                              title: 'Editar',
                              text: 'Abrindo Edição',
                              type: 'success',
                              styling: 'bootstrap3'
                            });">
                              <i class="fa fa-edit"></i> Editar
                            </a> -->
                           
                          </td>
                        </tr>


<div class="modal fade bs-example-modal-lg1<?php printf($id_cont); ?>" tabindex="-1" role="dialog" aria-hidden="true"  id="myModal1<?php printf($id_cont); ?>">
<div class="modal-dialog modal-lg">
<div class="modal-content">

<div class="modal-header">
<h4 class="modal-title">Itens <?php printf($id_grupo); ?> </h4>
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
</button>
</div>
<div class="modal-body">
<form action="backend/utilities-gas-glp-suport-insert-iten-itens-backend.php?id=<?php printf($id_cont);?>" method="post">
<div class="ln_solid"></div>

<div class="item form-group">
<label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
<div class="col-md-6 col-sm-6 ">
<input id="suport" class="form-control" name="id" type="hidden" value="<?php printf($id_cont); ?> " >
</div>
</div>
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
    <div class="col-md-6 col-sm-6 ">
      <input id="suport" class="form-control" name="suport" type="hidden" value="<?php printf($codigoget); ?> " >
    </div>
  </div>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Modo <span
      class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <select type="text" class="form-control has-feedback-left" name="id_acao"
        id="id_acao<?php printf($id_cont); ?>" style="width:350px;" placeholder="Unidade" required="required">
        <option> Selecione uma Modo</option>
        <option value="1"> Interno entre setores do grupo</option>

        <option  value="2"> Externo com Fornecedor</option>
        
        
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"
        class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
      
    </div>
  </div>
  
  <script>
    $(document).ready(function() {
      $('#id_acao<?php printf($id_cont); ?>').select2({
        dropdownParent: $("#myModal1<?php printf($id_cont); ?>")
      });
    });
  </script>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Locação <span
      class="required"></span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <select multiple="multiple"  type="text" class="form-control has-feedback-left" name="id_fornecedor[]"
        id="id_fornecedor<?php printf($id_cont); ?>" style="width:350px;" placeholder="Unidade" >
        <option> Selecione uma Fornecedor</option>
        <?php
          
          $result_cat_post  = "SELECT  id, empresa FROM fornecedor WHERE trash = 1";
          
          $resultado_cat_post = mysqli_query($conn_cal, $result_cat_post);
          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['empresa'].'</option>';
          }
        ?>
        
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"
        class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
      
    </div>
  </div>
  
  <script>
    $(document).ready(function() {
      $('#id_fornecedor<?php printf($id_cont); ?>').select2({
        dropdownParent: $("#myModal1<?php printf($id_cont); ?>")
      });
    });
  </script>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Setor <span
      class="required"></span>
    </label>
    <div class="col-md-6 col-sm-6 ">
      <select  multiple="multiple" type="text" class="form-control has-feedback-left" name="id_setor[]"
        id="id_setor<?php printf($id_cont); ?>" style="width:350px;" placeholder="Unidade" >
        <option> Selecione um Setor</option>
        <?php
          
          $result_cat_post  = "SELECT  id,nome FROM instituicao_area WHERE id in($setor)";
          
          $resultado_cat_post = mysqli_query($conn_cal, $result_cat_post);
          while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
            echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
          }
        ?>
        
      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"
        class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>
      
    </div>
  </div>
  
  <script>
    $(document).ready(function() {
      $('#id_setor<?php printf($id_cont); ?>').select2({
        dropdownParent: $("#myModal1<?php printf($id_cont); ?>")
      });
    });
  </script>

  
  
  <label for="risco">Ação:</label>
  <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
    data-parsley-validation-threshold="10"></textarea> 
 


  </div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
<button type="submit" class="btn btn-primary" onclick="new PNotify({
title: 'Registrado',
text: 'Informações registrada!',
type: 'success',
styling: 'bootstrap3'
});">Salvar Informações</button>
</div>

</div>
</div>
</div>
</form>

                   <?php  $row=$row+1; }
}   ?>
                      </tbody>
                    </table>





                </div>
              </div>


               <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true"  id="myModal">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h4 class="modal-title">Itens</h4>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                           <form action="backend/utilities-gas-glp-suport-insert-itens-backend.php?id=<?php printf($codigoget);?>" method="post">
                             <div class="ln_solid"></div>

                             <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"></label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="suport" class="form-control" name="suport" type="hidden" value="<?php printf($codigoget); ?> " >
                        </div>
                      </div>

                      <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Grupo <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-left" name="id_grupo"
                                  id="id_grupo" style="width:350px;" placeholder="Unidade" required="required">
                                  <option> Selecione uma Grupo</option>
                                  <?php



                                           $result_cat_post  = "SELECT  id, nome FROM equipamento_grupo WHERE trash = 1";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['nome'].'</option>';
                              }
                         ?>

                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#id_grupo').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>
                     <!--     <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Locação <span
                                  class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 ">
                              <select type="text" class="form-control has-feedback-left" name="id_fornecedor"
                                  id="id_fornecedor" style="width:350px;" placeholder="Unidade" required="required">
                                  <option> Selecione uma unidade</option>
                                  <?php



                                           $result_cat_post  = "SELECT  id, empresa FROM fornecedor WHERE trash = 1";

                    $resultado_cat_post = mysqli_query($conn, $result_cat_post);
                              while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
                                echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['empresa'].'</option>';
                              }
                         ?>

                              </select>
                              <span class="fa fa-building form-control-feedback right" aria-hidden="true"
                                  class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

                          </div>
                      </div>

                      <script>
                      $(document).ready(function() {
                          $('#id_fornecedor').select2({
                              dropdownParent: $("#myModal")
                          });
                      });
                      </script>



<label for="risco">Ação:</label>
                          <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                            data-parsley-validation-threshold="10"></textarea> -->



                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                          <button type="submit" class="btn btn-primary" onclick="new PNotify({
                                    title: 'Registrado',
                                    text: 'Informações registrada!',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });">Salvar Informações</button>
                        </div>

                      </div>
                    </div>
                  </div>
              </form>

              <!-- -->


              <!-- Posicionamento -->


              <div class="clearfix"></div>



<div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Assinatura <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="backend/utilities-gas-glp-support-signature-backend.php?id=<?php printf($codigoget);?>" method="post">
          <div class="item form-group">
                        <label for="middle-name"
                            class="col-form-label col-md-3 col-sm-3 label-align">Chave da Assinatura</label>
                        <div class="col-md-6 col-sm-6 ">
                            <input class="form-control" type="text" value="<?php printf($signature_alert); ?> "
                                readonly="readonly">
                        </div>
                    </div>

<!-- Large modal --> <center>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Assinatura</button>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Assinatura</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <h4>Assinatura digital</h4>
                <p>Para Assinar digite o texto abaixo.</p>
               <?php $chave=chaveAlfaNumerica(5); echo "$chave" ?>
                <p> <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="assinature"> <span ></span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="assinature" name="assinature"   class="form-control " class="docs-tooltip" data-toggle="tooltip" title=" ">
                
              </div>
              <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($v4uuid);?>" id="v4uuid" name="v4uuid" >
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-3 "> </label>
              <div class="col-md-9 col-sm-9 ">
                <input type="hidden" class="form-control"  value=" <?php  printf($chave);?>" id="chave" name="chave" >
              </div>
            </div>
            <script>
var chave = "<?php echo $chave; ?>"; // Obtém a chave do PHP e armazena na variável JavaScript
var assinaturaInput = document.getElementById("assinature");

assinaturaInput.addEventListener("blur", function () {
var assinatura = this.value;

if (assinatura !== chave) {
Swal.fire('A assinatura está incorreta!');
  // Aqui você pode realizar a ação desejada caso a assinatura seja divergente, como exibir uma mensagem de erro ou realizar algum redirecionamento.
} else {

  // Aqui você pode realizar a ação desejada caso a assinatura seja correta.
}
});
</script>
            </div></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Assinar</button>
              </div>

            </div>
          </div>
        </div>
       </center>
       
          </form>
        </div>
      </div>
    </div>
  </div>


  <div class="clearfix"></div>
  <div class="clearfix"></div>


<div class="x_panel">
    <div class="x_title">
      <h2>Assinatura</h2>
      <ul class="nav navbar-right panel_toolbox">
         
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      
    <!--arquivo -->
<?php    
$query="SELECT unidade_gas_glp_regdate_support_signature.id,unidade_gas_glp_regdate_support_signature.id_support,unidade_gas_glp_regdate_support_signature.id_user,unidade_gas_glp_regdate_support_signature.id_signature,unidade_gas_glp_regdate_support_signature.reg_date,usuario.nome FROM unidade_gas_glp_regdate_support_signature INNER JOIN usuario ON unidade_gas_glp_regdate_support_signature.id_user = usuario.id   WHERE unidade_gas_glp_regdate_support_signature.id_support like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$id_mp,$id_user,$id_signature,$reg_date,$id_user);

?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              
              <th>Usuário</th>
              <th>Assinatura</th>
               <th>Registro</th>
                <th>Ação</th>
            
            </tr>
          </thead>
          <tbody>
                 <?php  while ($stmt->fetch()) { ?>
                 
            <tr>
              <th scope="row"><?php printf($row); ?></th>
              <td><?php printf($id_user); ?></td>
              <td><?php printf($id_signature); ?></td>
              <td><?php printf($reg_date); ?></td>
              <td>                                                                <a class="btn btn-app" onclick="
                Swal.fire({
                  title: 'Tem certeza?',
                  text: 'Você não será capaz de reverter isso!',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Sim, Deletar!'
                }).then((result) => {
                  if (result.isConfirmed) {
                    Swal.fire(
                      'Deletando!',
                      'Seu arquivo será excluído.',
                      'success'
                    ),
                    window.location = 'backend/utilities-gas-glp-signature-support-trash-backend.php?id=<?php printf($id); ?>&page=<?php printf($codigoget); ?>';
                  }
                })
              ">
                <i class="fa fa-trash"></i> Excluir
              </a>
              </td>

             
              
            </tr>
       <?php  $row=$row+1; }
}   ?>
          </tbody>
        </table>

    
  
    
    </div>
  </div>  
    <!-- compose -->
    <div class="clearfix"></div>

<!-- Posicionamento -->

<div class="x_panel">
    <div class="x_title">
        <h2>Anexos Gerais</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="badge bg-green pull-right" data-toggle="modal"
                    data-target=".bs-example-modal-lg2"><i class="fa fa-plus"></i></a>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <!--arquivo -->
        <?php
$query="SELECT id, file, titulo, reg_date, upgrade FROM unidade_gas_glp_regdate_suport_dropzone WHERE id_suport like '$codigoget'";
$row=1;
if ($stmt = $conn->prepare($query)) {
$stmt->execute();
$stmt->bind_result($id,$file,$titulo,$reg_date,$upgrade);

?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Titulo</th>
                    <th>Registro</th>
                    <th>Atualização</th>
                    <th>Ação</th>

                </tr>
            </thead>
            <tbody>
                <?php  while ($stmt->fetch()) { ?>

                <tr>
                    <th scope="row"><?php printf($row); ?></th>
                    <td><?php printf($id); ?></td>
                    <td><?php printf($titulo); ?></td>
                    <td><?php printf($reg_date); ?></td>
                    <td><?php printf($upgrade); ?></td>
                    <td> <a class="btn btn-app" href="dropzone/suport/<?php printf($file); ?> "
                            target="_blank" onclick="new PNotify({
                      title: 'Visualizar',
                      text: 'Visualizar Anexo',
                      type: 'info',
                      styling: 'bootstrap3'
                  });">
                            <i class="fa fa-file"></i> Anexo
                        </a>
                        <a class="btn btn-app" onclick="
        Swal.fire({
title: 'Tem certeza?',
text: 'Você não será capaz de reverter isso!',
icon: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Sim, Deletar!'
}).then((result) => {
if (result.isConfirmed) {
Swal.fire(
'Deletando!',
'Seu arquivo será excluído.',
'success'
),
window.location = 'backend/utilities-gas-glp-suport-dropzone-trash-backend.php?id=<?php printf($id); ?>&id_suport=<?php printf($codigoget); ?> ';
}
})
">
                            <i class="fa fa-trash"></i> Excluir
                        </a>
                    </td>

                </tr>
                <?php  $row=$row+1; }
}   ?>
            </tbody>
        </table>




    </div>
</div>

<!-- Registro -->
<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">Anexo</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <!-- Registro forms-->
                <form
                    action="backend/utilities-gas-glp-support-analysis-backend.php?id=<?php printf($codigoget);?>"
                    method="post">
                    <div class="ln_solid"></div>


                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3  label-align">Titulo<span
                                class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" class='date' type="text" name="titulo"
                                required='required'>
                        </div>
                    </div>




                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar Informações</button>

                    </div>
            </div>
            </form>

            <form class="dropzone" action="backend/utilities-gas-glp-support-dropzone-upload-backend.php"
                method="post">
            </form>
        </div>
    </div>
</div>
<!-- Registro -->
<div class="clearfix"></div>

<div class="clearfix"></div>
             <div class="x_panel">
                <div class="x_title">
                  <h2>Ação</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="utilities-gas-glp-support">
                    <i class="glyphicon glyphicon-arrow-left"></i> Voltar
                  </a>



              <!--    <a class="btn btn-app"  href="backend/os-opened-close-backend.php?os=<?php printf($id_os); ?>" onclick="new PNotify({
																title: 'Cancelamento',
																text: 'Cancelamento de Abertura de O.S!',
																type: 'error',
																styling: 'bootstrap3'
														});">
                    <i class="glyphicon glyphicon-floppy-save"></i> Cancelar
                  </a>
                    <a  class="btn btn-app" href="os-viewer?os=<?php printf($id_os); ?> "target="_blank" onclick="new PNotify({
																title: 'Visualizar',
																text: 'Visualizar O.S!',
																type: 'info',
																styling: 'bootstrap3'
														});" >
                    <i class="fa fa-file-pdf-o"></i> Visualizar
                  </a> -->

                </div>
              </div>




                </div>
              </div>

              <script type="text/javascript">
    $(document).ready(function(){
      $('#datatable').dataTable( {
		        "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    }



		      } );

        $('#instituicao').change(function(){
            $('#area').load('sub_categorias_post.php?instituicao='+$('#instituicao').val());
        });
    });

    

    </script>
              