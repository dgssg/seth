  <?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>
 
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  PMOC <small> Plano de manutenção, operação e controle <small>Calibração</small></h3>
              </div>

           
            </div>
       
              
              <!--<div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Laudos
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-report">Cadastro Laudo</a>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-plan">Cadastro Plano de Ação</a>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-register-type">Cadastro Tipo de Laudo</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-laudo-report-plan">Relatorio Plano de Ação</a>
                    </div>
                  </div>

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Cadastro
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-register-unit">Unidade</a>
                      <a class="dropdown-item" href="utilities-pmoc-register-room">Ambiente Climatizado</a>
                      <a class="dropdown-item" href="utilities-pmoc-register-responsible">Responsável Técnico</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-register-owner">Proprietário</a>
                    </div>
                  </div>

                  
                  <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      Relatorio
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="utilities-pmoc-report-pmoc">PMOC</a>
                      <a class="dropdown-item" href="utilities-pmoc-report-mc">Manutenção Corretiva</a>
                       
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="utilities-pmoc-report-mp-schedule">Cronograma MP</a>
                       <a class="dropdown-item" href="utilities-pmoc-report-mp-routine">Rotina MP</a>
                       <a class="dropdown-item" href="utilities-pmoc-report-mp-accompaniment">Acompanhamento MP</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="utilities-pmoc-report-inventory">Inventário</a>
                    </div>
                  </div>

                </div>
              </div>-->

              
              
        
              
                <div class="clearfix"></div>
               
               	       
      <div class="clearfix"></div>
               
	         <div class="x_panel">
                <div class="x_title">
                  <h2>Filtro</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                   
                  
                       <?php include 'frontend/utilities-pmoc-laudo-report-plan-filter.php';?>

                </div>
              </div>
              
             
	        
                  
                  
                </div>
              </div>
              
  