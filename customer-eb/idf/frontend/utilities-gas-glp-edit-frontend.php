
<?php
  $codigoget = ($_GET["id"]);
  
  // Create connection
  include("database/database.php");// remover ../
  
  
  $query = "SELECT id, unidade, cod, medidor, fornecimento, obs, file, ativo, trash, t_recipiente, t_service, qtd, capacidade, vaporizador, mpa, protec, upgrade, reg_date FROM unidade_gas_glp WHERE id = $codigoget";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $unidade, $cod, $medidor, $fornecimento, $obs, $file, $ativo, $trash, $t_recipiente, $t_service, $qtd, $capacidade, $vaporizador, $mpa, $protec, $upgrade, $reg_date );

    while ($stmt->fetch()) {
      
    }
  }
?>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

  <!-- 1 -->
  <link href="dropzone.css" type="text/css" rel="stylesheet" />
  
  <!-- 2 -->
  <script src="dropzone.min.js"></script>
  <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Gás </small>GLP</h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

          
         
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar Registro <small> Dados</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
              
                    <div class="ln_solid"></div>
                    <!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
                    <form action="backend/utilities-gas-glp-unit-edit-backend.php" method="post">
                      
                      <input id="id" class="form-control" type="hidden" name="id"  value="<?php printf($id); ?> " >
                      
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="unidade" class="form-control" type="text" name="unidade"  value="<?php printf($unidade); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cod" class="form-control" type="text" name="cod"  value="<?php printf($cod); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Medidor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="medidor" class="form-control" type="text" name="medidor"  value="<?php printf($medidor); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Fornecimento</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="fornecimento" class="form-control" type="text" name="fornecimento"  value="<?php printf($fornecimento); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de Recipiente</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="t_recipiente" class="form-control" type="text" name="t_recipiente"  value="<?php printf($t_recipiente); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de Serviço</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="t_service" class="form-control" type="text" name="t_service"  value="<?php printf($t_service); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="qtd" class="form-control" type="text" name="qtd"  value="<?php printf($qtd); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Capacidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="capacidade" class="form-control" type="text" name="capacidade"  value="<?php printf($capacidade); ?> " >
                        </div>
                      </div>
                     <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Vaporizador</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="vaporizador" class="form-control" type="text" name="vaporizador"  value="<?php printf($vaporizador); ?> " >
                        </div>
                      </div>
                         <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Pressão</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="mpa" class="form-control" type="text" name="mpa"  value="<?php printf($mpa); ?> " >
                        </div>
                      </div>
                              <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Proteção</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="protec" class="form-control" type="text" name="protec"  value="<?php printf($protec); ?> " >
                        </div>
                      </div>
          
          
                     
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Ativo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input name="ativo" type="checkbox" class="js-switch"
                                <?php if($ativo == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      
                      
                      <label for="middle-name">Observação</label>
                      
                      <textarea id="obs" class="form-control" name="obs"
                        data-parsley-trigger="keyup" data-parsley-validation-threshold="10"
                        placeholder="<?php printf($obs); ?>"
                        value="<?php printf($obs); ?>" ><?php printf($obs); ?></textarea>
                      <div class="ln_solid"></div>

                      <div class="item form-group">
                        <label for="middle-name"
                          class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input readonly="readonly" type="text" class="form-control"
                            value="<?php printf($reg_date); ?> ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name"
                          class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input readonly="readonly" type="text" class="form-control"
                            value="<?php printf($upgrade); ?> ">
                        </div>
                      </div>

                      
                      <div class="ln_solid"></div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-9 col-sm-9 ">
                          <center>
                            
                            <button type="reset" onclick="clean()" class="btn btn-primary" onclick="new PNotify({
                              title: 'Limpado',
                              text: 'Todos os Campos Limpos',
                              type: 'info',
                              styling: 'bootstrap3'
                            });" />Limpar </button>
                            <input type="submit" name="submit1" class="btn btn-primary" onclick="new PNotify({
                              title: 'Registrado',
                              text: 'Informações registrada!',
                              type: 'success',
                              styling: 'bootstrap3'
                            });" value="Salvar" />
                       
                                                     </center>
                        </div>
                      </div>
                      
                      </div>
                    </form>                    
                    
                    
                    
                    
                    
                    
                     
                    
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Imagem <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                          aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <div class="row">
                      <div class="col-md-55">
                        <?php if($file ==! ""){ ?>
                        
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;"
                              src="dropzone/utilities/<?php printf($file); ?>" alt="image" />
                            <div class="mask">
                              <p>Imagem</p>
                              <div class="tools tools-bottom">
                                <a href="dropzone/utilities/<?php printf($file); ?>" download><i
                                  class="fa fa-download"></i></a>
                                
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>Imagem</p>
                          </div>
                        </div>
                        <?php } ?>
                        
                      </div>
                      
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
            
            <div class="x_panel">
              <div class="x_title">
                <h2>Inserir/Substituir</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                      aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                <form class="dropzone"
                  action="backend/utilities-upload-backend.php?id=<?php printf($id); ?>"
                  method="post">
                </form>
                
                <form action="backend/utilities-upload-gas-glp-backend.php?id=<?php printf($codigoget); ?>"
                  method="post">
                  <center>
                    <button class="btn btn-sm btn-success" type="submit">Atualizar Imagem</button>
                  </center>
                </form>
                
              </div>
            </div>
            

            
          </div>
          
          
        </div>

<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>