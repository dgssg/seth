<?php
include("database/database.php");



?>

<form  action="backend/utilities-telephony-bill-register-backend.php" method="post">

  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Unidade <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="unidade" id="unidade"  placeholder="Unidade">
<option > Selecione uma Unidade</option> 
        <?php



        $result_cat_post  = "SELECT  id, unidade FROM unidade_telephony ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['unidade'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#unidade').select2();
  });
  </script>

  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="year">Ano <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="year" id="year"  placeholder="Ano">
      <option > Selecione um Ano</option> 

        <?php



        $result_cat_post  = "SELECT  id, yr FROM year ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['yr'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#year').select2();
  });
  </script>


  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="mouth">Mês <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="mouth" id="mouth"  placeholder="Mês">
      <option > Selecione uma Mês</option> 

        <?php



        $result_cat_post  = "SELECT  id, month FROM month ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['month'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#mouth').select2();
  });
  </script>




  <button type="reset" class="btn btn-primary"onclick="new PNotify({
    title: 'Limpado',
    text: 'Todos os Campos Limpos',
    type: 'info',
    styling: 'bootstrap3'
  });" />Limpar</button>
  <input type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informações registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" value="Salvar"/>

</form>
<div class="ln_solid"></div>
