<?php
include("database/database.php");
$unidade = trim(($_GET["unidade"]));
$obsolescence_year = trim(($_GET["yr"]));

?>


<!--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->



<!-- 1 -->
<link href="dropzone.css" type="text/css" rel="stylesheet" />

<!-- 2 -->
<script src="dropzone.min.js"></script>















<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Analise de Obsolescência de Equipamento</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5  form-group pull-right top_search">
          <div class="input-group">

            <span class="input-group-btn">

            </span>
          </div>
        </div>
      </div>
    </div>


    <div class="x_panel">
      <div class="x_title">
        <h2>Menu</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">


          <!--    <a class="btn btn-app"  href="#" onclick="new PNotify({
          title: 'Obsolescência',
          text: 'Obsolescência',
          type: 'info',
          styling: 'bootstrap3'
        });">
        <i class="fa fa-circle-o-notch"></i> Zerar Grau
      </a> -->
      <button type="button" onclick="zerargrau()"class="btn btn-primary" onclick="new PNotify({
        title: 'Zerar',
        text: 'Informações Zeradas!',
        type: 'success',
        styling: 'bootstrap3'
      });" >Zerar Grau</button>


      <button type="button" onclick="obsolescence()"class="btn btn-primary" onclick="new PNotify({
        title: 'Obsolescência',
        text: 'Obsolescência',
        type: 'info',
        styling: 'bootstrap3'
      });">
      Ultima Avaliação
    </button>

    <button type="button" onclick="anoatual()"class="btn btn-primary" onclick="new PNotify({
        title: 'Ano Atual',
        text: 'Ano Atual',
        type: 'info',
        styling: 'bootstrap3'
      });">
      Ano Atual
    </button>


  </div>
</div>


<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Avaliação <small> de equipamento</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="dropdown-item" href="#">Parametro 1</a>
              </li>
              <li><a class="dropdown-item" href="#">Parametro 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">




        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
              <div class="x_title">


                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
                <form  action="backend/obsolescence-analysis-backend.php?unidade=<?php printf($unidade);?>" method="post"  >

                <div class="ln_solid"></div>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Unidade<span
                        ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="unidade" id="unidade"  placeholder="Unidade">
										  <option value="">Selecione uma Unidade</option>
                   <?php
                      

   $result_cat_post  = "SELECT instituicao.id, instituicao.instituicao FROM instituicao WHERE trash = 1 ";

   $resultado_cat_post = mysqli_query($conn, $result_cat_post);
             while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
              if($unidade == $row_cat_post['id']){
                echo '<option></option><option value="'.$row_cat_post['id'].'" selected>'.$row_cat_post['instituicao'].'</option>';
        
              }
               echo '<option></option><option value="'.$row_cat_post['id'].'">'.$row_cat_post['instituicao'].'</option>';
             }
                    ?>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma unidade "></span>

                  </div>

                  </div>

								 <script>
                                    $(document).ready(function() {
                                    $('#unidade').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Setor<span
                       ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="setor" id="setor"  placeholder="Setor">
										<option value="">Selecione um Setor</option>
                      </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione um Setor "></span>

                  </div>
                  </div>
                            

								 <script>
                                    $(document).ready(function() {
                                    $('#setor').select2();
                                      });
                                 </script>

<div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Area<span
                       ></span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="area" id="area"  placeholder="Area">
                    <option value="">Selecione uma Area</option>   
                  </select>
                      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione uma Area "></span>

                    </div>

                    </div>
                                
								 <script>
                                    $(document).ready(function() {
                                    $('#area').select2();
                                      });
                                 </script>
                                 
                                 <button type="button" onclick="obsolescence_check()"class="btn btn-primary">
      Verificar Avaliação
    </button>
                                 <div class="field item form-group">
                                
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Equipamento<span
                       > </span></label>
                    <div class="col-md-6 col-sm-6">
                    <select type="text" class="form-control has-feedback-right" name="equipamento" id="equipamento"  placeholder="equipamento" required>
										  <option value="">Selecione o equipamento</option>
                      <?php


					$query = "SELECT equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome, instituicao_area.nome, instituicao.id FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area INNER JOIN instituicao ON instituicao.id = instituicao_area.id_unidade WHERE equipamento.obsolescence = 0 and equipamento.trash != 0 and equipamento.ativo = 0";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id, $nome, $modelo, $fabricante, $codigo, $localizacao, $area,$unidade);
     while ($stmt->fetch()) {
     ?>
<option value="<?php printf($id);?>	"><?php printf($codigo);?> - <?php printf($nome);?> - <?php printf($fabricante);?> -  <?php printf($modelo);?>	</option>
										  	<?php
											// tira o resultado da busca da memória
											}

                                            }
											$stmt->close();

											?>
                                       	</select>
										<span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Equipamento "></span>
                    <div name="check" id="check"></div>
                  </div>


								      		    
                  </div>
								 <script>
                                    $(document).ready(function() {
                                    $('#equipamento').select2();
                                      });
                                 </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="text" class="form-control has-feedback-right" name="yr" id="yr"  placeholder="equipamento" required>
                        <option value="">Selecione o ano</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_year";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $yr);
                          while ($stmt->fetch()) {
                            
                            ?>
                            <option value="<?php printf($id);?>	" <?php if($id == $obsolescence_year){printf("selected");} ?>	><?php printf($yr);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                      <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Ano "></span>

                    </div>
                    <span class="input-group-btn">
                      <span id="c5_result">Ano de Fabricação =</span>
                      <br>
                      <span id="c5_result_2">Tempo de Uso (%) =</span>
                      <br>

                      <span id="c5_result_2_2">Estimativa de Vida =</span>

                    </span>
                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#yr').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempo de Uso</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="number" class="form-control has-feedback-right" name="c1" id="c1"  placeholder="equipamento" oninput="myFunction(this.value)">
                        <option value="0">Selecione o grau</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_c1";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $c,$grau);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($grau);?> - <?php printf($c);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>


                    </div>
                    <span class="input-group-btn">
                      <span id="c1_result">TDown (%)=</span>


                    </span>

                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#c1').select2();
                  });
                  </script>



                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Custo de Manutenção</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="number" class="form-control has-feedback-right" name="c2" id="c2"  placeholder="equipamento" oninput="myFunction(this.value)">
                        <option value="0">Selecione o grau</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_c2";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $c,$grau);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($grau);?> - <?php printf($c);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                    <span class="input-group-btn">
                      <span id="c2_result">COSR (%)=</span>


                    </span>

                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#c2').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Disponibilidade do Equipamento</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="number" class="form-control has-feedback-right" name="c3" id="c3"  placeholder="equipamento" oninput="myFunction(this.value)">
                        <option value="0">Selecione o grau</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_c3";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $c,$grau);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($grau);?> - <?php printf($c);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                    <span class="input-group-btn">
                      <span id="c3_result">TUp (%)=</span>


                    </span>

                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#c3').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tecnologia do Equipamento</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="number" class="form-control has-feedback-right" name="c4" id="c4"  placeholder="equipamento" oninput="myFunction(this.value)">
                        <option value="0">Selecione o grau</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_c4";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $c,$grau);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($grau);?> - <?php printf($c);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                    <span class="input-group-btn">
                      <span id="c4_result">End of Life =</span>


                    </span>

                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#c4').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Utilização do Equipamento</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="number" class="form-control has-feedback-right" name="c5" id="c5"  placeholder="equipamento" oninput="myFunction(this.value)">
                        <option value="0">Selecione o grau</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_c5";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $c,$grau);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($grau);?> - <?php printf($c);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                    <span class="input-group-btn">
                      <span id="resultado"></span>
                      <span id="c5_result_6">Aquisição/Depreciação/Taxa (R$)=</span>
                                          
                    </span>

                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#c5').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Recursos Disponíveis na Manutenção</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="number" class="form-control has-feedback-right" name="c6" id="c6"  placeholder="equipamento" oninput="myFunction(this.value)">
                        <option value="0">Selecione o grau</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_c6";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $c,$grau);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($grau);?> - <?php printf($c);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                    <span class="input-group-btn">
                      <span id="resultado"></span>
                      <span id="c5_result_7">Custo M.P (R$)=</span>
                      

                    </span>

                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#c6').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Custo de Reforma</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="number" class="form-control has-feedback-right" name="c7" id="c7"  placeholder="equipamento" oninput="myFunction(this.value)">
                        <option value="0">Selecione o grau</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_c7";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $c,$grau);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($grau);?> - <?php printf($c);?></option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                    </div>
                    <span class="input-group-btn">
                      <span id="resultado"></span>
                    <span id="c5_result_8">Validade/Situação:</span>
                      <div class='modal fade' id='anvisaModal' tabindex='-1' aria-labelledby='anvisaModalLabel' aria-hidden='true'>
                        <div class='modal-dialog modal-lg'>
                          <div class='modal-content'>
                            <div class='modal-header'>
                              <h5 class='modal-title' id='anvisaModalLabel'>Dados Anvisa</h5>
                              <button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
                            </div>
                            <div class='modal-body'>
                              <!-- Container para o conteúdo AJAX -->
                              <div id='ajaxContent'></div>
                            </div>
                            <div class='modal-footer'>
                              <button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Fechar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- O restante do seu conteúdo HTML -->
                      
                      <script>
                        document.addEventListener('DOMContentLoaded', function() {
                          // Adiciona um ouvinte de evento de clique ao link da Anvisa
                          document.getElementById('anvisaLink').addEventListener('click', function(event) {
                            // Impede o comportamento padrão do link
                            event.preventDefault();
                            
                            // Obtém o id da Anvisa do atributo de dados
                            var anvisaId = this.getAttribute('data-anvisa-id');
                            
                            // Carrega o conteúdo da página usando AJAX
                            var xhr = new XMLHttpRequest();
                            xhr.onreadystatechange = function() {
                              if (xhr.readyState === 4 && xhr.status === 200) {
                                // Atualiza o conteúdo do modal com a resposta AJAX
                                document.getElementById('ajaxContent').innerHTML = xhr.responseText;
                                
                                // Abre o modal
                                var myModal = new bootstrap.Modal(document.getElementById('anvisaModal'));
                                myModal.show();
                              }
                            };
                            xhr.open('GET', 'report-equipament-family-anvisa?id=' + anvisaId, true);
                            xhr.send();
                          });
                        });
                      </script>
                      
                      <!-- O restante do seu conteúdo HTML -->
                      
                      
                      <script>
                        function openAnvisaPage() {
                          var anvisaValue = document.getElementById('anvisa').value;
                          
                          // Verifique se o campo não está vazio
                          if (anvisaValue.trim() !== '') {
                            // Construa a URL com o valor do campo
                            var url = 'https://consultas.anvisa.gov.br/#/genericos/q/?numeroRegistro=' + encodeURIComponent(anvisaValue);
                            
                            // Abra a URL em uma nova janela do navegador
                            window.open(url, '_blank');
                          } else {
                            // Trate o caso em que o campo está vazio ou contém apenas espaços em branco
                            alert('O campo ANVISA está vazio. Insira um valor antes de prosseguir.');
                          }
                        }
                      </script>

                    </span>

                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#c7').select2();
                  });
                  </script>

                  <div class="item form-group">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">ROOI</label>
                    <div class="col-md-6 col-sm-6 ">
                      <select type="text" class="form-control has-feedback-right" name="rooi" id="rooi"  placeholder="equipamento">
                        <option value="	">Selecione o rooi</option>
                        <?php



                        $query = "SELECT *FROM obsolescence_rooi";

                        //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
                        if ($stmt = $conn->prepare($query)) {
                          $stmt->execute();
                          $stmt->bind_result($id, $rooi);
                          while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	">Grau: <?php printf($rooi);?> </option>
                            <?php
                            // tira o resultado da busca da memória
                          }

                        }
                        $stmt->close();

                        ?>
                      </select>
                      <span class="fa fa-barcode form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o Ano "></span>

                    </div>
                  </div>

                  <script>
                  $(document).ready(function() {
                    $('#rooi').select2();
                  });
                  </script>
                  <!--
                  <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Pontuação <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                <output type="number" id="score" name="score"  readonly="readonly" required="required" class="form-control " for=" score c1 c2 c3 c4 c5 c6 c7">
              </div>
            </div> readonly -->

            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Pontuação <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 " id="score">
              <input class="knob" data-width="120" data-height="120" data-min="-100" data-displayPrevious=true data-fgColor="<?php if($score <="2.5"){printf("#26B99A");} if(($score >"2.5") && ($score <= "5.5")){printf("#FFFF00");}if($score > "5.5"){printf("#FF0000");} ?>" data-step="1"  data-skin="tron" datadata-linecap=round  type="number" id="score" name="score" readonly value="<?php printf($score);?>" >

                </div>


              </div>

              <p id="demo"></p>

              <script>

              function myFunction() {
                var x1 = document.getElementById("c1").value;
                var x2 = document.getElementById("c2").value;
                var x3 = document.getElementById("c3").value;
                var x4 = document.getElementById("c4").value;
                var x5 = document.getElementById("c5").value;
                var x6 = document.getElementById("c6").value;
                var x7 = document.getElementById("c7").value;
                var scorepoint =  parseInt(x1) + parseInt(x2) + parseInt(x3) + parseInt(x4) + parseInt(x5) + parseInt(x6) + parseInt(x7);

                document.getElementById("demo").innerHTML = "You wrote: " + scorepoint;
              }
            </script>
            <label for="message_mp">Observação :</label>
            <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
            data-parsley-validation-threshold="10" placeholder=""></textarea>
            <br>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Valor Mercado (R$) <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="vlr_mkt" name="vlr_mkt"  class="form-control "  onKeyPress="return(moeda(this,'.',',',event))">
              </div>
            </div>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="nome">Valor de Aquisição (R$) <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="vlr_purchasse" name="vlr_purchasse"   class="form-control "  onKeyPress="return(moeda(this,'.',',',event))">
              </div>
            </div>

            <label for="message_mp">Observação de Aquisição :</label>
            <textarea id="obs_purchasse" class="form-control" name="obs_purchasse" data-parsley-trigger="keyup"
            data-parsley-validation-threshold="10" placeholder=""></textarea>
            <br>


            <div class="item form-group ">
              <div class="col-md-10 col-sm-10 offset-md-3">

                <button type="button" class="btn btn-primary" onclick="calcular()" onclick="new PNotify({
                  title: 'Calcular',
                  text: 'Calcular os Campos',
                  type: 'success',
                  styling: 'bootstrap3'
                });" />Calcular</button>
                 <button type="reset" onclick="clean()" class="btn btn-primary"onclick="new PNotify({
                  title: 'Limpado',
                  text: 'Todos os Campos Limpos',
                  type: 'info',
                  styling: 'bootstrap3'
                });" />Limpar</button>
                <input type="submit" class="btn btn-primary" onclick="new PNotify({
                  title: 'Registrado',
                  text: 'Informações registrada!',
                  type: 'success',
                  styling: 'bootstrap3'
                });" value="Salvar" />
              </div>
            </div>
          </div>
        </form>




      </div>
    </div>
  </div>
</div>
</div>



</div>
</div>
</div>
</div>
<!-- Contador de caracter -->
<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

<script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
<script type="text/javascript">
$("meuPrimeiroDropzone").dropzone({ url: "upload.php" });
Dropzone.options.meuPrimeiroDropzone = {
  paramName: "fileToUpload",
  dictDefaultMessage: "Arraste seus arquivos para cá!",
  maxFilesize: 300,
  accept: function(file, done) {
    if (file.name == "olamundo.png") {
      done("Arquivo não aceito.");
    } else {
      done();
    }
  }
}
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c1_result').load('c1.php?equipamento='+$('#equipamento').val()+'&ano='+$('#yr').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c2_result').load('c2.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c3_result').load('c3.php?equipamento='+$('#equipamento').val()+'&ano='+$('#yr').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c4_result').load('c4.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#equipamento").change(function(){
    $('#c5_result').load('c5.php?equipamento='+$('#equipamento').val());
    $('#c5_result_2').load('c5_2.php?equipamento='+$('#equipamento').val());
    $('#c5_result_2_2').load('c5_2_2.php?equipamento='+$('#equipamento').val());
    $('#c5_result_6').load('c5_6.php?equipamento='+$('#equipamento').val());
    $('#c5_result_7').load('c5_7.php?equipamento='+$('#equipamento').val());
    $('#c5_result_8').load('c5_8.php?equipamento='+$('#equipamento').val());
  });
});
</script>
<script>
  function obsolescence_check(){
    $('#check').load('sub_categorias_post_obsolescence_check.php?equipamento='+$('#equipamento').val()+'&yr='+$('#yr').val());
  }
function zerargrau() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change');
  $('#c1').select2().load('c1_zero.php');
  $('#c2').select2().load('c2_zero.php');
  $('#c3').select2().load('c3_zero.php');
  $('#c4').select2().load('c4_zero.php');
  $('#c5').select2().load('c5_zero.php');
  $('#c6').select2().load('c6_zero.php');
  $('#c7').select2().load('c7_zero.php');

}

</script>

<script>
function calcular() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change'


  $('#score').load('score.php?c1='+$('#c1').val()+'&c2='+$('#c2').val()+'&c3='+$('#c3').val()+'&c4='+$('#c4').val()+'&c5='+$('#c5').val()+'&c6='+$('#c6').val()+'&c7='+$('#c7').val());

}

</script>
<script>
function obsolescence() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change');
  $('#c1').select2().load('c1_equipament.php?equipamento='+$('#equipamento').val());
  $('#c2').select2().load('c2_equipament.php?equipamento='+$('#equipamento').val());
  $('#c3').select2().load('c3_equipament.php?equipamento='+$('#equipamento').val());
  $('#c4').select2().load('c4_equipament.php?equipamento='+$('#equipamento').val());
  $('#c5').select2().load('c5_equipament.php?equipamento='+$('#equipamento').val());
  $('#c6').select2().load('c6_equipament.php?equipamento='+$('#equipamento').val());
  $('#c7').select2().load('c7_equipament.php?equipamento='+$('#equipamento').val());
  $('#rooi').select2().load('rooi_equipament.php?equipamento='+$('#equipamento').val());
  $('#score').load('score_equipament.php?equipamento='+$('#equipamento').val());
  $('#obs').load('obs_equipament.php?equipamento='+$('#equipamento').val());
  $('#vlr_mkt').load('vlr_mkt_equipament.php?equipamento='+$('#equipamento').val());
  $('#vlr_purchasse').load('vlr_purchasse_equipament.php?equipamento='+$('#equipamento').val());
  $('#obs_purchasse').load('obs_purchasse_equipament.php?equipamento='+$('#equipamento').val());

  
}

</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#unidade').change(function(){
        $('#setor').select2().load('sub_categorias_post.php?instituicao='+$('#unidade').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#setor').change(function(){
        $('#area').select2().load('sub_categorias_post_setor.php?area='+$('#setor').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#area').change(function(){
        $('#equipamento').select2().load('sub_categorias_post_equipament.php?setor='+$('#area').val());
      });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#area').select2().load('sub_setor.php?equipamento='+$('#equipamento').val());
      
      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#setor').select2().load('sub_area.php?equipamento='+$('#equipamento').val());

      });
    });
    $(document).ready(function(){
      $('#equipamento').change(function(){
        $('#unidade').select2().load('sub_unidade.php?equipamento='+$('#equipamento').val());

      });
    });
   
    </script>
								<script>
function clean() {
  $('#unidade').select2().load('reset_unidade.php');
  $('#area').select2().load('reset_area.php');
  $('#setor').select2().load('reset_setor.php');
  $('#equipamento').select2().load('reset_equipamento.php');


}

</script>
<script>
function anoatual() {
  //  $('#c1').select2().val('0').trigger('change,select2');
  //$('#c1').select2().val('1');
  //$("#c1").val('1');
  //  $("#c1").select2().trigger('change');
  $('#yr').select2().load('obsolescence_yr.php');
  
}

</script>
<script language="javascript">   
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.FROMCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>  
		
  