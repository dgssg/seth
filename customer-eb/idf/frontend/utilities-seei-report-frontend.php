

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Relatório <small>Sistema de Energia Elétrica Ininterrupto</small>SEEI</h3>
      </div>


    </div>

    

    <div class="clearfix"></div>

    <div class="row" style="display: block;">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Tabela <small>de Relatório</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Settings 1</a>
                  <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


             
            <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
              <div class="panel">
                <a   data-toggle="modal" data-target=".bs-example-modal-lg1 " class="panel-heading" role="tab" id="headingOne1"  href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="panel-title">Relatório Estatístico e Analítico</h4>
                </a>
                <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                </div>
              </div>
            </div>
        
            
            
            
            <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true" id="myModal">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Relatório Estatístico e Analítico</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <h4>Filtro</h4>
                    <!-- Registro forms-->
                    <form action="report/report-utilities-seei.php" target="_blank" method="post">
                      
                      <input id="id" class="form-control" name="id" type="hidden" value="1" >
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;"  class="form-control has-feedback-right" name="year" id="year"  placeholder="Ano">
                            <option value="">Selecione o ano</option>
                            <?php
                              
                              $sql = " SELECT id, yr FROM year  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#year').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Mês<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;" class="form-control has-feedback-right" name="mouth" id="mouth"  placeholder="Familia">
                            <option value="">Selecione um Mês</option>
                            <?php
                              
                              
                              
                              $sql = " SELECT id, month FROM month  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                        </div>
                      </div>
                      <script>
                        $(document).ready(function() {
                          $('#mouth').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Unidade ">Unidade <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;"  class="form-control has-feedback-right" name="unidade" id="unidade"  placeholder="Unidade">
                            <option value=""> Selecione uma unidade</option>
                            <?php
                              
                              
                              
                              $sql = " SELECT id, unidade ,cod,medidor FROM unidade_seei  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome,$cod,$medidor );
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?> - <?php printf($cod);?> - <?php printf($medidor);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#unidade').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      
                      <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary">Visualizar</button>
                      </div>
                      
                    
                    </form>
                  </div>
                </div>
              </div>
             </div>
              
            <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-hidden="true" id="myModal2">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Relatório Analítico</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <h4>Filtro</h4>
                    <!-- Registro forms-->
                    <form action="report/report-utilities-seei-2.php" target="_blank" method="post">
                      
                      <input id="id" class="form-control" name="id" type="hidden" value="1" >
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;"  class="form-control has-feedback-right" name="year1" id="year1"  placeholder="Ano">
                            <option value="">Selecione o ano</option>
                            <?php
                              
                              $sql = " SELECT id, yr FROM year  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#year1').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Mês<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;" class="form-control has-feedback-right" name="mouth1" id="mouth1"  placeholder="Familia">
                            <option value="">Selecione um Mês</option>
                            <?php
                              
                              
                              
                              $sql = " SELECT id, month FROM month  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                        </div>
                      </div>
                      <script>
                        $(document).ready(function() {
                          $('#mouth1').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Unidade ">Unidade <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;"  class="form-control has-feedback-right" name="unidade1" id="unidade1"  placeholder="Unidade">
                            <option value=""> Selecione uma unidade</option>
                            <?php
                              
                              
                              
                              $sql = " SELECT id, unidade ,cod,medidor FROM unidade_seei  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome,$cod,$medidor );
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?> - <?php printf($cod);?> - <?php printf($medidor);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#unidade1').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      
                      <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary">Visualizar</button>
                      </div>
                      
                      
                    </form>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-hidden="true" id="myModal3">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Relatório Técnico</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <h4>Filtro</h4>
                    <!-- Registro forms-->
                    <form action="report/report-utilities-seei-3.php" target="_blank" method="post">
                      
                      <input id="id" class="form-control" name="id" type="hidden" value="1" >
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Ano</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;"  class="form-control has-feedback-right" name="year2" id="year2"  placeholder="Ano">
                            <option value="">Selecione o ano</option>
                            <?php
                              
                              $sql = " SELECT id, yr FROM year  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#year2').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Mês<span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;" class="form-control has-feedback-right" name="mouth2" id="mouth2"  placeholder="Familia">
                            <option value="">Selecione um Mês</option>
                            <?php
                              
                              
                              
                              $sql = " SELECT id, month FROM month  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                        </div>
                      </div>
                      <script>
                        $(document).ready(function() {
                          $('#mouth2').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="colaborador" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Unidade ">Unidade <span class="required" ></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" style="width:350px;"  class="form-control has-feedback-right" name="unidade2" id="unidade2"  placeholder="Unidade">
                            <option value=""> Selecione uma unidade</option>
                            <?php
                              
                              
                              
                              $sql = " SELECT id, unidade ,cod,medidor FROM unidade_seei  ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome,$cod,$medidor );
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?> - <?php printf($cod);?> - <?php printf($medidor);?>	</option>
                            <?php
                              // tira o resultado da busca da memória
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          
                        </div>
                        
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#unidade2').select2({
                            dropdownParent: $("#myModal")
                          });
                        });
                      </script>
                      
                      
                      
                      <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary">Visualizar</button>
                      </div>
                      
                      
                    </form>
                  </div>
                </div>
              </div>
            </div>
                  <!-- end of accordion -->
                  
                  
                </div>
              </div>
            </div>
      
      
    </div>
  </div>
</div>

<!-- /page content -->
  