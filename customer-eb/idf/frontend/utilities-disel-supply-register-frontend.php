<?php
include("database/database.php");



?>

<form  action="backend/utilities-disel-supply-register-backend.php" method="post">

  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="unidade">Instalação <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="id_unidade_disel" id="id_unidade_disel"  placeholder="Unidade">
<option > Selecione uma Instalação</option> 
        <?php



        $result_cat_post  = "SELECT  id, name FROM unidade_disel  ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['name'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#id_unidade_disel').select2();
  });
  </script>

  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="year">Ano <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="id_year" id="id_year"  placeholder="Ano">
      <option > Selecione um Ano</option> 

        <?php



        $result_cat_post  = "SELECT  id, yr FROM year ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['yr'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#year').select2();
  });
  </script>


  <div class="form-group row">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="mouth">Mês <span class="required"></span>
    </label>
    <div class="input-group col-md-6 col-sm-6">
      <select type="text" class="form-control has-feedback-left" name="id_mouth" id="id_mouth"  placeholder="Mês">
      <option > Selecione uma Mês</option> 

        <?php



        $result_cat_post  = "SELECT  id, month FROM month ";

        $resultado_cat_post = mysqli_query($conn, $result_cat_post);
        while($row_cat_post = mysqli_fetch_assoc($resultado_cat_post) ) {
          echo '<option value="'.$row_cat_post['id'].'">'.$row_cat_post['month'].'</option>';
        }
        ?>

      </select>
      <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a unidade "></span>

    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('#mouth').select2();
  });
  </script>


   
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Leitura Inicial</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="read_start" class="form-control" type="text" name="read_start" >
    </div>
  </div>
   
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Leitura Final</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="read_end" class="form-control" type="text" name="read_end" >
    </div>
  </div>
   
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantidade (Litros)</label>
    <div class="col-md-6 col-sm-6 ">
      <input id="qtd" class="form-control" type="text" name="qtd" >
    </div>
  </div>
   
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor Total (R$) </label>
    <div class="col-md-6 col-sm-6 ">
      <input id="vlr_t" class="form-control" type="text" name="vlr_t" >
    </div>
  </div>
   
  <div class="item form-group">
    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Valor Unitário (R$) </label>
    <div class="col-md-6 col-sm-6 ">
      <input id="vlr_un" class="form-control" type="text" name="vlr_un" >
    </div>
  </div>
   


  <button type="reset" class="btn btn-primary"onclick="new PNotify({
    title: 'Limpado',
    text: 'Todos os Campos Limpos',
    type: 'info',
    styling: 'bootstrap3'
  });" />Limpar</button>
  <input type="submit" class="btn btn-primary" onclick="new PNotify({
    title: 'Registrado',
    text: 'Informações registrada!',
    type: 'success',
    styling: 'bootstrap3'
  });" value="Salvar"/>

</form>
<div class="ln_solid"></div>
