 
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Consulta de Solicitação de Transporte</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
       
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 ">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Filtro</h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a class="dropdown-item" href="#">Settings 1</a>
                                  </li>
                                  <li><a class="dropdown-item" href="#">Settings 2</a>
                                  </li>
                                </ul>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                            <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                            
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

	<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 ">
						<div class="x_panel">
							<div class="x_title">


								<div class="clearfix"></div>
							</div>
							<div class="x_content">
         
                    		<div class="ln_solid"></div>
 
                
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">                  <thead>
                    <tr>
                      <th class="column-title">#</th>
                      <th class="column-title">Evento</th>
                      
                      <th class="column-title">Data Inicio</th>
                      <th class="column-title">Data Fim</th>
                      <th class="column-title">Ação</th>

                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php   
                      $query = "SELECT id,title, start,end 
                      FROM events_logistica ";
                      //$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os where id_status like '1'  ";
                      if ($stmt = $conn->prepare($query)) {
                        $stmt->execute();
                        $stmt->bind_result($id,$title,$start,$end);
                        
                      while ($stmt->fetch()) { ?>
                    <tr>
                      <th class="column-title" scope="row"></th>
                      <th class="column-title"> <?php printf($title); ?></th>
                      <th class="column-title"> <?php printf($start); ?></th>
                      <th class="column-title"> <?php printf($end); ?></th>
                      <th class="column-title">
                        <a class="btn btn-app" onclick="Swal.fire({
title: 'Tem certeza?',
                      text: 'Você não será capaz de reverter isso!',
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Sim, Excluir!'
                      }).then((result) => {
                      if (result.isConfirmed) {
                      Swal.fire(
                      'Notificação!',
                      'Sua Solicitação sera excluido.',
                      'success'
                      ),
                      window.location = 'backend/logic-car-trash-backend.php?id=<?php printf($id); ?>';
                      }
                      });">
                      <i class="fa fa-close"></i> Excluir Solicitação
                      </a>
                      </th>
                      
                    </tr>
                    <?php
                      }}
                    ?>
                    
                    
                  </tbody>
                </table>



									</div>
								</div>
							</div>
						</div>
					</div>
        </div>


                                  <!-- Contador de caracter -->
    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js" type="text/javascript"></script>

    <script src="https://assets.locaweb.com.br/locastyle/3.10.1/javascripts/locastyle.js"></script>
        
     
<script type="text/javascript">
  $(document).ready(function () {
    $('#datatable').DataTable({
      initComplete: function () {
        this.api()
        .columns([1,2,3])
        .every(function (d) {
          var column = this;
          var theadname = $("#datatable th").eq([d]).text();
          var container = $('<div class="filter-title"></div>')
          .appendTo('#userstable_filter');
          var label = $('<label>'+theadname+': </label>')
          .appendTo(container); 
          var select = $('<select  class="form-control my-1"><option value="">' +
            theadname +'</option></select>')
          .appendTo( '#userstable_filter'  ).select2()
          .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            
            column.search(val ? '^' + val + '$' : '', true, false).draw();
          });
          
          column
          .data()
          .unique()
          .sort()
          .each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
          });
        });
        
        
      },
    });
  });
</script>
