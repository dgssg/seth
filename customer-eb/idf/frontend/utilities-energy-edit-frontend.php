
<?php
  $codigoget = ($_GET["id"]);
  
  // Create connection
  include("database/database.php");// remover ../
  
  
  $query = "SELECT id,unidade,cod,medidor,fornecimento,obs,file,fabricante,modelo,volt,amper,type,conector,registro,fase,ativo,reg_date,upgrade,trash,serie from unidade_energia where id = $codigoget";
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$unidade,$cod,$medidor,$fornecimento,$obs,$file,$fabricante,$modelo,$volt,$amper,$type,$conector,$registro,$fase,$ativo,$reg_date,$upgrade,$trash,$serie);

    while ($stmt->fetch()) {
      
    }
  }
?>
<link href="../../framework/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
<script src="../../framework/vendors/switchery/dist/switchery.min.js"></script>

  <!-- 1 -->
  <link href="dropzone.css" type="text/css" rel="stylesheet" />
  
  <!-- 2 -->
  <script src="dropzone.min.js"></script>
  <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Registro  &amp; Consulta <small>Energia Elétrica</small></h3>
              </div>

           
            </div>
            
             <div class="clearfix"></div>

          
         
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editar Registro <small> Dados</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
              
                    <div class="ln_solid"></div>
                    <!--<object style="width:100%; height:500px"   type="text/html" data="os.php"> </object> -->
                    <form action="backend/utilities-energy-unit-edit-backend.php" method="post">
                      
                      <input id="id" class="form-control" type="hidden" name="id"  value="<?php printf($id); ?> " >
                      
                      
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Unidade</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="unidade" class="form-control" type="text" name="unidade"  value="<?php printf($unidade); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Codigo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="cod" class="form-control" type="text" name="cod"  value="<?php printf($cod); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Medidor</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="medidor" class="form-control" type="text" name="medidor"  value="<?php printf($medidor); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Fabricante</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="fabricante" class="form-control" type="text" name="fabricante"  value="<?php printf($fabricante); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Modelo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="modelo" class="form-control" type="text" name="modelo"  value="<?php printf($modelo); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Serie</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="serie" class="form-control" type="text" name="serie"  value="<?php printf($serie); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tensao</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="volt" class="form-control" type="text" name="volt"  value="<?php printf($volt); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Corrente</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input id="amper" class="form-control" type="text" name="amper"  value="<?php printf($amper); ?> " >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Fornecimento</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="fornecimento"
                            id="fornecimento" >
                            <option value="">Selecione uma opção</option>
                            <option value="BT-Convencional" <?php if($fornecimento == "BT-Convencional"){ printf("selected"); }; ?> >BT-Convencional</option>
                              <option value="MT-Convencional" <?php if($fornecimento == "MT-Convencional"){ printf("selected"); }; ?> >MT-Convencional</option>
                              <optgroup label="AT - Alta Tensão">
                                <option value="A1" <?php if($fornecimento == "A1"){ printf("selected"); }; ?>>AT - A1</option>
                                <option value="A2" <?php if($fornecimento == "A2"){ printf("selected"); }; ?>>AT - A2</option>
                                <option value="A3" <?php if($fornecimento == "A3"){ printf("selected"); }; ?>>AT - A3</option>
                                <option value="A4" <?php if($fornecimento == "A4"){ printf("selected"); }; ?>>AT - A4</option>
                                <option value="A4-Azul" <?php if($fornecimento == "A4-Azul"){ printf("selected"); }; ?>>AT - A4 - Azul</option>
                                <option value="A4-Verde" <?php if($fornecimento == "A4-Verde"){ printf("selected"); }; ?>>AT - A4 - Verde</option>
                              </optgroup>
                            </select>
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#fornecimento').select2();
                        });
                      </script>
                     
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de Medição</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="type"
                            id="type" >
                            <option value="">Selecione uma opção</option>
                            <option value="Energia-Ativa" <?php if($type == "Energia-Ativa"){ printf("selected"); }; ?>>Energia Ativa</option>
                            <option value="Energia-Ativa-e-Reativa" <?php if($type == "Energia-Ativa-e-Reativa"){ printf("selected"); }; ?>>Energia Ativa e Reativa</option>
                           
                          </select>
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#type').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de ligações</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="conector"
                            id="conector" >
                            <option value="">Selecione uma opção</option>
                          
                              <option value="BT-Monofásico" <?php if($conector == "BT-Monofásico"){ printf("selected"); }; ?>>BT - Monofásico (Fase + Neutro)</option>
                              <option value="BT-Bifásico-com-Neutro" <?php if($conector == "BT-Bifásico-com-Neutro"){ printf("selected"); }; ?>>BT - Bifásico com Neutro (Fase + Fase + Neutro)</option>
                              <option value="BT-Bifásico-sem-Neutro" <?php if($conector == "BBT-Bifásico-sem-Neutro"){ printf("selected"); }; ?>>BT - Bifásico sem Neutro (Fase + Fase)</option>
                              <option value="BT-Trifásico-sem-Neutro" <?php if($conector == "BT-Trifásico-sem-Neutro"){ printf("selected"); }; ?>>BT - Trifásico sem o Neutro (Fase + Fase + Fase)</option>
                              <option value="BT-Trifásico-com-Neutro" <?php if($conector == "BT-Trifásico-com-Neutro"){ printf("selected"); }; ?>>BT - Trifásico com o Neutro (Fase + Fase + Fase + Neutro)</option>
                          </select>
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#conector').select2();
                        });
                      </script>
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de Registro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="registro"
                            id="registro" >
                            <option value="">Selecione uma opção</option>
                              <option value="Unidirecional" <?php if($registro == "Unidirecional"){ printf("selected"); }; ?>>Unidirecional</option>
                              <option value="Trifásico" <?php if($registro == "Trifásico"){ printf("selected"); }; ?>>Trifásico</option>
                              <option value="Trifásico-Unidirecional" <?php if($registro == "Trifásico-Unidirecional"){ printf("selected"); }; ?>>Unidirecional Trifásico</option>
                            </select>
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#registro').select2();
                        });
                      </script>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de Fase</label>
                        <div class="col-md-6 col-sm-6 ">
                          <select type="text" class="form-control has-feedback-right" name="fase"
                            id="fase" >
                            <option value="">Selecione uma opção</option>
                              <option value="ABC" <?php if($fase == "ABC"){ printf("selected"); }; ?>>ABC</option>
                              <option value="ACB" <?php if($fase == "ACB"){ printf("selected"); }; ?>>ACB</option>
                              <option value="BAC" <?php if($fase == "BAC"){ printf("selected"); }; ?>>BAC</option>
                              <option value="BCA" <?php if($fase == "BCA"){ printf("selected"); }; ?>>BCA</option>
                              <option value="CAB" <?php if($fase == "CAB"){ printf("selected"); }; ?>>CAB</option>
                              <option value="CBA" <?php if($fase == "CBA"){ printf("selected"); }; ?>>CBA</option>
                            </select>
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#fase').select2();
                        });
                      </script>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Ativo</label>
                        <div class="col-md-6 col-sm-6 ">
                          <div class="">
                            <label>
                              <input name="ativo" type="checkbox" class="js-switch"
                                <?php if($ativo == "0"){printf("checked"); }?> />
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      
                      
                      <label for="middle-name">Observação</label>
                      
                      <textarea id="obs" class="form-control" name="obs"
                        data-parsley-trigger="keyup" data-parsley-validation-threshold="10"
                        placeholder="<?php printf($obs); ?>"
                        value="<?php printf($obs); ?>" ><?php printf($obs); ?></textarea>
                      <div class="ln_solid"></div>

                      <div class="item form-group">
                        <label for="middle-name"
                          class="col-form-label col-md-3 col-sm-3 label-align">Cadastro</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input readonly="readonly" type="text" class="form-control"
                            value="<?php printf($reg_date); ?> ">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name"
                          class="col-form-label col-md-3 col-sm-3 label-align">Atualização</label>
                        <div class="col-md-6 col-sm-6 ">
                          <input readonly="readonly" type="text" class="form-control"
                            value="<?php printf($upgrade); ?> ">
                        </div>
                      </div>

                      
                      <div class="ln_solid"></div>
                      <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 "></label>
                        <div class="col-md-9 col-sm-9 ">
                          <center>
                            
                            <button type="reset" onclick="clean()" class="btn btn-primary" onclick="new PNotify({
                              title: 'Limpado',
                              text: 'Todos os Campos Limpos',
                              type: 'info',
                              styling: 'bootstrap3'
                            });" />Limpar </button>
                            <input type="submit" name="submit1" class="btn btn-primary" onclick="new PNotify({
                              title: 'Registrado',
                              text: 'Informações registrada!',
                              type: 'success',
                              styling: 'bootstrap3'
                            });" value="Salvar" />
                       
                                                     </center>
                        </div>
                      </div>
                      
                      </div>
                    </form>                    
                    
                    
                    
                    
                    
                    
                     
                    
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Imagem <small> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                          aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <div class="row">
                      <div class="col-md-55">
                        <?php if($file ==! ""){ ?>
                        
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;"
                              src="dropzone/utilities/<?php printf($file); ?>" alt="image" />
                            <div class="mask">
                              <p>Imagem</p>
                              <div class="tools tools-bottom">
                                <a href="dropzone/utilities/<?php printf($file); ?>" download><i
                                  class="fa fa-download"></i></a>
                                
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>Imagem</p>
                          </div>
                        </div>
                        <?php } ?>
                        
                      </div>
                      
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
            
            <div class="x_panel">
              <div class="x_title">
                <h2>Inserir/Substituir</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                      aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                
                <form class="dropzone"
                  action="backend/utilities-upload-backend.php?id=<?php printf($id); ?>"
                  method="post">
                </form>
                
                <form action="backend/utilities-upload-energy-backend.php?id=<?php printf($codigoget); ?>"
                  method="post">
                  <center>
                    <button class="btn btn-sm btn-success" type="submit">Atualizar Imagem</button>
                  </center>
                </form>
                
              </div>
            </div>
            

            
          </div>
          
          
        </div>

<?php 
  
  
  $query = "SELECT menu FROM tools";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($menu);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }
  if($menu == "0"){ ?>
<script type="text/javascript">
  window.onload = function()
  {
    document.getElementById("menu_toggle").click();
  }
</script>
<?php  } ?>