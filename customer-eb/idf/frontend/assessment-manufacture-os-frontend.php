

<?php
  
  
  
  $codigoget= $_GET['id'];
  $assessment= $_GET['assessment'];
  //$id_fornecedor= $_GET['id_fornecedor'];
  $id_fornecedor=$_COOKIE['assessment'];
  
  
  
?>   
   <div class="right_col" role="main">
       
          <div class="">
              
            <div class="page-title">
              <div class="title_left">
                <h3>Avaliação Fornecedor</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                   <a class="btn btn-app"  href="os-progress-upgrade?os=<?php printf($codigoget); ?>">
                    <i class="glyphicon glyphicon-list-alt"></i> Voltar
                  </a>
                     
                 

                </div>
              </div>






            <div class="clearfix"></div>
       
       
         
            
              <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Avaliação <small>de Fornecedor</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    
                    
                    <form action="backend/assessment-manufacture-os-backend.php" method="post">
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Avaliação</label>
                        <div class="col-md-4 col-sm-4 ">
                          <select type="text" class="form-control has-feedback-left" name="assessment" id="assessment" readonly="readonly"  >
                            <option value="	">Selecione a Avaliação</option>
                            <option value="1"<?php if ($assessment==1) { printf("selected");} ?>>Ordem de Serviço</option>
                            <option value="2"<?php if ($assessment==2) { printf("selected");} ?>>Manutenção Preventiva</option>
                            <option value="3"<?php if ($assessment==3) { printf("selected");} ?>>Calibração</option>
                            
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Avaliação "></span>
                          
                          
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Numero</label>
                        <div class="col-md-4 col-sm-4 ">
                          <input id="number_assessment" class="form-control" type="text" name="number_assessment" value="<?php printf($codigoget); ?>" readonly="readonly" >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Data</label>
                        <div class="col-md-4 col-sm-4 ">
                          <input id="data_assessment" class="form-control" type="date" name="data_assessment"  >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Empresa</label>
                        <div class="col-md-4 col-sm-4 ">
                          <select type="text" class="form-control has-feedback-left" name="id_manufacture" id="id_manufacture"  readonly="readonly" >
                            
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, empresa FROM fornecedor WHERE id = $id_fornecedor ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	" selected ><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da mem贸ria
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione a Empresa "></span>
                          
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#id_manufacture').select2();
                        });
                      </script>
                      <div class="item form-group">
                        <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tipo de Fornecimento</label>
                        <div class="col-md-4 col-sm-4 ">
                          <select type="text" class="form-control has-feedback-left" name="fornecedor_service" id="fornecedor_service"  >
                            <option value="	">Selecione o tipo</option>
                            <?php
                              
                              
                              
                              $sql = "SELECT  id, service FROM fornecedor_service ";
                              
                              
                              if ($stmt = $conn->prepare($sql)) {
                                $stmt->execute();
                                $stmt->bind_result($id,$nome);
                                while ($stmt->fetch()) {
                            ?>
                            <option value="<?php printf($id);?>	"><?php printf($nome);?>	</option>
                            <?php
                              // tira o resultado da busca da mem贸ria
                              }
                              
                              }
                              $stmt->close();
                              
                            ?>
                          </select>
                          <span class="fa fa-building form-control-feedback right" aria-hidden="true"class="docs-tooltip" data-toggle="tooltip" title="Selecione o tipo "></span>
                          
                          
                        </div>
                      </div>
                      
                      <script>
                        $(document).ready(function() {
                          $('#fornecedor_service').select2();
                        });
                      </script>
                      
                      <!--  <div class="col-md-12 col-sm-12  ">
                      <div class="x_panel">
                      <div class="x_title">
                      <h2>Avaliação <small>Fornecedor</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Settings 1</a>
                      <a class="dropdown-item" href="#">Settings 2</a>
                      </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                      </ul>
                      <div class="clearfix"></div>
                      </div>
                      
                      <div class="x_content">
                      
                      <p>Questionário <code>Avaliação</code> do fornecedor</p>
                      
                      <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                      <thead>
                      <tr class="headings">
                      <th>
                      <input type="checkbox" id="check-all" class="flat">
                      </th>
                      <th class="column-title">Pergunta </th>
                      <th class="column-title">Avaliação</th>
                      
                      <th class="column-title no-link last"><span class="nobr">Action</span>
                      </th>
                      <th class="bulk-actions" colspan="7">
                      <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                      </th>
                      </tr>
                      </thead>
                      
                      <tbody id="fornecedor_service_question">
                      <tr class="even pointer">  <td class="a-center ">
                      <input type="checkbox" class="flat" name="table_records">
                      </td>
                      <td class=" "></td>
                      <td class=" "></td>
                      <td class=" last"><a href="#"></a>
                      </td> </tr>
                      
                      
                      
                      </tbody>
                      </table>
                      </div>
                      </div>
                      </div>
                      </div> -->
                      
                      <div class="item form-group" >
                        
                        <div class="col-md-12 col-sm-12 " >
                          <span id="fornecedor_service_question"></span>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="item form-group" >
                        <label for="message_mp">Observação:</label>
                        <textarea id="obs" class="form-control" name="obs" data-parsley-trigger="keyup"
                          data-parsley-validation-threshold="10" placeholder=""></textarea>
                        <br>
                        
                      </div>
                      
                      <div class="modal-footer">
                        
                        <button type="submit" class="btn btn-primary" onclick="new PNotify({
                          title: 'Registrado',
                          text: 'Informações registrada!',
                          type: 'success',
                          styling: 'bootstrap3'
                        });" >Salvar Informações</button>
                      </div>
                      
                    </form>
                    
                    <script type="text/javascript">
                      $(document).ready(function(){
                        $('#fornecedor_service').change(function(){
                          $('#fornecedor_service_question').load('sub_categorias_post_fornecedor_service.php?id='+$('#fornecedor_service').val());
                        });
                      });
                    </script>
                    

                  
                      
                  
                    
                   
                  </div>
                </div>
              </div>
            </div>
         
            
            
           

            

            
        </div>
            </div>
        <!-- /page content -->
       
    <!-- /compose -->
    

 <script src="../../framework/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
 
