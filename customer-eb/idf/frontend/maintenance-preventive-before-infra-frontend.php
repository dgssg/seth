<?php
$codigoget = ($_GET["equipamento"]);

// Create connection

include("database/database.php");

?>

 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <h3>  Manutenção <small>Preventiva Infraestrutura</small></h3>
              </div>


            </div>

            <div class="clearfix"></div>





            <div class="clearfix"></div>




              <!-- Posicionamento -->



            <div class="x_panel">
                <div class="x_title">
                  <h2>Menu</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <a class="btn btn-app"  href="maintenance-preventive-infra">
                    <i class="glyphicon glyphicon-open"></i> Aberta
                      <span class="badge bg-green">  <h3> <?php
                        $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive where mp = 1 and id_status = 1 ");
                        $stmt->execute();
                        $stmt->bind_result($result);
                        while ( $stmt->fetch()) {
                        echo "$result" ;
                      }?> </h3></span>

                  </a>

                  <a class="btn btn-app"  href="maintenance-preventive-before-infra">
                    <i class="glyphicon glyphicon-export"></i> Atrasada
                    <span class="badge bg-red">  <h3> <?php
                      $stmt = $conn->prepare(" SELECT COUNT(id) FROM maintenance_preventive where mp = 1 and id_status = 2 ");
                      $stmt->execute();
                      $stmt->bind_result($result);
                      while ( $stmt->fetch()) {
                      echo "$result" ;
                    }?> </h3></span>
                  </a>

                 <a class="btn btn-app"  href="maintenance-preventive-close-infra">
                    <i class="glyphicon glyphicon-save"></i> Fechada
                  </a>

                   <a class="btn btn-app"  href="maintenance-preventive-print-infra">
                    <i class="glyphicon glyphicon-print"></i> Impressão
                  </a>

                  <a class="btn btn-app"  href="maintenance-preventive-print-register-infra">
                    <i class="glyphicon glyphicon-folder-open"></i> Registro de Impressão
                  </a>

                  <a class="btn btn-app"  href="maintenance-preventive-control-infra">
                    <i class="fa fa-check-square-o"></i> Controle
                  </a>

                </div>
              </div>
<div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="userstable_filter" style="column-count:3; -moz-column-count:3; -webkit-column-count:3; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>        
                   
                  </div>
                </div>
              </div>
            </div>
               <div class="x_panel">
                <div class="x_title">
                  <h2>Lista MP Atradasa</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <?php include 'frontend/maintenance-preventive-before-list-infra-frontend.php';?>

                </div>
              </div>




                </div>
              </div>
               <script type="text/javascript">
      $(document).ready(function () {
    $('#datatable').DataTable({
      "processing": true,
                "stateSave": true,
                responsive: true,
            
             
   
    "language": {
      "loadingRecords": "Carregando dados...",
      "processing": "Processando  dados...",
      "infoEmpty": "Nenhum dado a mostrar",
      "emptyTable": "Sem dados disponíveis na tabela",
      "zeroRecords": "Não há registros a serem exibidos",
      "search": "Filtrar registros:",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoFiltered": " - filtragem de _MAX_ registros",
      "lengthMenu": "Mostrar _MENU_ registros",
      
      "paginate": {
        "previous": "Página anterior",
        "next": "Próxima página",
        "last": "Última página",
        "first": "Primeira página",
     

        
      }
    },
        initComplete: function () {
            this.api()
                .columns([0,2,3,4,5,6,7,8,9,10,11,12])
                .every(function (d) {
                    var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
        // Container para o título, o select e o alerta de filtro
        var container = $('<div class="filter-title" style="margin-bottom: 10px;"></div>').appendTo('#userstable_filter');
        
        // Título acima do select
        var title = $('<label>' + theadname + '</label>').appendTo(container);
        
        // Container para o select
        var selectContainer = $('<div></div>').appendTo(container);
        
        var select = $('<select class="form-control my-1"><option value="">' +
          theadname + '</option></select>').appendTo(selectContainer).select2()
        .on('change', function() {
          var val = $.fn.dataTable.util.escapeRegex($(this).val());
          column.search(val ? '^' + val + '$' : '', true, false).draw();
          
          // Remove qualquer alerta existente
          container.find('.filter-alert').remove();
          
          // Se um valor for selecionado, adicionar o alerta de filtro
          if (val) {
            $('<div class="filter-alert">' +
              '<span class="filter-active-indicator">&#x25CF;</span>' +
              '<span class="filter-active-message">Filtro ativo</span>' +
              '</div>').appendTo(container);
          }
          
          // Remove o indicador do título da coluna
          $("#datatable th").eq([d]).find('.filter-active-indicator').remove();
          
          // Se um valor for selecionado, adicionar o indicador no título da coluna
          if (val) {
            $("#datatable th").eq([d]).append('<span class="filter-active-indicator">&#x25CF;</span>');
          }
        });
        
        column.data().unique().sort().each(function(d, j) {
          select.append('<option value="' + d + '">' + d + '</option>');
        });
        // Verificar se há filtros aplicados ao carregar a página
        var filterValue = column.search();
        if (filterValue) {
          select.val(filterValue.replace(/^\^|\$$/g, '')).trigger('change');
        }
      });
      
      // Adicionar a configuração inicial da DataTable
      table.on('init.dt', function() {
        // Verifica se há filtros aplicados ao carregar a página
        table.columns().every(function() {
          var column = this;
          var searchValue = column.search();
          if (searchValue) {
            var select = $('#userstable_filter select').eq(column.index());
            select.val(searchValue.replace(/^\^|\$$/g, '')).trigger('change');
          }
        });
      });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                

                
        },
    });
});

</script>

