<?php

date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");

// Conectando ao banco de dados
$conn = mysqli_connect("localhost", "cvheal47_root", "cvheal47_root", "cvheal47_building_idf");



$key = "mks";
$mod = "eb";
$status2 = 1;
 
$query = "
SELECT 
  id,v_nf
FROM 
  unidade_energia_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";

$dados = 0;
$result = 0;
$read_open = 1;
$t1 = "";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];


    while ($stmt->fetch()) {
        $values[] = $vlf;
        $ids[] = $id;
        $counter++;
    }
    
    if ($counter == 2) {
        // Calcular a diferença percentual entre os dois últimos valores
        $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  

    $stmt->close();
} else {
  //  echo "Erro na preparação da consulta: " . $conn->error;
}
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_energia_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_energia_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
  
  //agua
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_agua_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_agua_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_agua_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
//disel
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_disel_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_disel_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_disel_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
  //gas_glp
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_gas_glp_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_gas_glp_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_gas_glp_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
  //gas_medicina
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_gas_medicina_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_gas_medicina_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_gas_medicina_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
  // gas_natural
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_gas_natural_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_gas_natural_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_gas_natural_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
  //seee
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_seee_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_seee_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_seee_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
  //seei
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_seei_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_seei_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_seei_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
  //telephony
  $query = "
SELECT 
  id,v_nf
FROM 
  unidade_telephony_fatura
ORDER BY 
  reg_date DESC
LIMIT 2;
";
  
  $dados = 0;
  $result = 0;
  $read_open = 1;
  $t1 = "";
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id,$vlf);
    $counter = 0;
    $values = [];
    $ids = [];
    
    
    while ($stmt->fetch()) {
      $values[] = $vlf;
      $ids[] = $id;
      $counter++;
    }
    
    if ($counter == 2) {
      // Calcular a diferença percentual entre os dois últimos valores
      $result = (($values[0] - $values[1]) / $values[1]) * 100;
      $result;
    }  
    
    $stmt->close();
  } else {
    //  echo "Erro na preparação da consulta: " . $conn->error;
  }
  $result=abs($result);
  if($result > 20) {
    // Primeiro, verificar se já existe um alerta para o id_dados
    $query = "
    SELECT 
      id
    FROM 
      unidade_telephony_alert
    WHERE 
      id_dados = ?;
    ";
    
    if ($stmt = $conn->prepare($query)) {
      $stmt->bind_param("i", $ids[0]);  // Bind the id_dados value to the query
      $stmt->execute();
      $stmt->bind_result($id);
      $stmt->fetch();
      $stmt->close(); // Feche a declaração preparada após buscar os dados
      
      // Se não existe nenhum registro, $id será null
      if(is_null($id)) {
        // Prepare a declaração para inserir um novo alerta
        $stmt = $conn->prepare("INSERT INTO unidade_telephony_alert (tipo, dados, id_dados, read_open) VALUES (?, ?, ?, ?)");
        
        $t1 = 'Alerta de Diferença Percentual'; // Exemplo de valor para 'tipo'
        $read_open = 0; // Exemplo de valor para 'read_open', pode ser alterado conforme necessário
        
        // Vincular os parâmetros à declaração preparada
        $stmt->bind_param("sssi", $t1, $values[0], $ids[0], $read_open);
        
        // Execute a declaração preparada para inserir os dados
        $stmt->execute(); 
        
        // Fechar a declaração preparada
        $stmt->close(); 
      }
    }
  }
// Fechar a conexão com o banco de dados
mysqli_close($conn);


?>
