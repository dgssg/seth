<?php
include("../../database/database.php");
include("../../database/database_cal.php");
session_start();	

$usuario = $_SESSION['id_usuario'];
$setor = $_SESSION['setor'] ;

  $query = "SELECT id_colaborador,id_category FROM usuario where id = $usuario ";
  
  
  if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_colaborador,$id_category);
    while ($stmt->fetch()) {
      //printf("%s, %s\n", $solicitante, $equipamento);
    }
  }


 
 

?>



        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Laudos Calibração</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">

                    <span class="input-group-btn">

                    </span>
                  </div>
                </div>
              </div>
            </div>
               <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filtro</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Settings 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                                    <div id="userstable_filter" style="column-count:4; -moz-column-count:4; -webkit-column-count:4; column-rule:1px dashed #fc0; -moz-column-rule:1px dashed #fc0; -webkit-column-rule:1px dashed #fc0; –moz-column-gap:20px; –webkit-column-gap:20px;"></div>
                 
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Laudos  <small> Calibração</small> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="#">Parametro 1</a>
                          </li>
                          <li><a class="dropdown-item" href="#">Parametro 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                       

				

						

     
<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                  
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Laudo</th>
                          <th>Codigo</th>
                          <th>Equipamento</th>
                          <th>Modelo</th>
                          <th>Fabricante</th>
                          
                          <th>N/S</th>
                          
                          <th>Realização</th>
                          <th>Vencimento</th>
                          <th>Validade</th>
                          <th>vencido</th>
                          <th>Status</th>
                          <th>Unidade</th>
                          <th>Setor</th>
                          <th>Area</th>
                          
                          <th>Ação</th>
                          
                        </tr>
                      </thead>


                      <tbody>
                         
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
                </div>
              </div>


							</div>
						</div>
					</div>
				</div>
					</div>


             </div>
<script language="JavaScript">
  
  $(document).ready(function() {
    
    $('#datatable').dataTable( {
      "processing": true,
      "stateSave": true,
      responsive: true,
      
      
      
      "language": {
        "loadingRecords": "Carregando dados...",
        "processing": "Processando  dados...",
        "infoEmpty": "Nenhum dado a mostrar",
        "emptyTable": "Sem dados disponíveis na tabela",
        "zeroRecords": "Não há registros a serem exibidos",
        "search": "Filtrar registros:",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoFiltered": " - filtragem de _MAX_ registros",
        "lengthMenu": "Mostrar _MENU_ registros",
        
        "paginate": {
          "previous": "Página anterior",
          "next": "Próxima página",
          "last": "Última página",
          "first": "Primeira página",
          
          
          
        }
      }
      
      
      
    } );
    // Define a URL da API que retorna os dados da query em formato JSON
    const apiUrl = '../../table/table-search-calibration-report.php';
    
    
    // Usa a função fetch() para obter os dados da API em formato JSON
    fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
      // Mapeia os dados para o formato esperado pelo DataTables
      // Mapeia os dados para o formato esperado pelo DataTables
      const novosDados = data.map(item => {
        // Define a data atual em JavaScript
        const dataAtual = new Date();
        
        // Converte a data de início do item em JavaScript
        const dataStart = new Date(item.data_start);
        
        // Calcula a nova data com base no valor em meses do item
        const novaData = new Date(dataStart.setMonth(dataStart.getMonth() + parseInt(item.val)));
        
        // Define as variáveis $data_now e $new_date em formato de data string
        const dataNow = dataStart.toLocaleDateString("pt-BR");
        const newData = novaData.toLocaleDateString("pt-BR");
        
        // Define o valor do campo "ven" com base na condição da data
        const ven = (novaData < dataAtual) ? "Sim" : "Não";
        
        // Retorna os dados no formato esperado pelo DataTables
        return [
          ``,
          item.id,
          item.codigo,
          item.equipamento,
          item.modelo,
          item.fabricante,
          item.serie,
          item.programadaFormatada = item.data_start ? new Date(item.data_start).toLocaleDateString("pt-BR") : "",
          dataNow,
          item.val,
          ven,
          item.status == "0" ? "Aprovado" : "Reprovado",
          item.instituicao,
          item.setor,
          item.area,
          
          
          
          `   
  
  
                  <a  class="btn btn-app" href="../../calibration-report-viewer-copy?laudo=${item.id} "target="_blank" onclick="new PNotify({
                                        title: 'Visualizar',
                                        text: 'Visualizar Laudo!',
                                        type: 'info',
                                        styling: 'bootstrap3'
                                    });" >
                            <i class="fa fa-file-pdf-o"></i> PDF
                          </a>
                          <a  class="btn btn-app" href="../../calibration-report-viewer-copy-print?laudo=${item.id} "target="_blank" onclick="new PNotify({
                                        title: 'Visualizar',
                                        text: 'Visualizar Laudo!',
                                        type: 'info',
                                        styling: 'bootstrap3'
                                    });" >
                            <i class="fa fa-print"></i> Imprimir
                          </a>
                  `
        ];
      });
      
      // Adiciona as novas linhas ao DataTables e desenha a tabela
      $('#datatable').DataTable().rows.add(novosDados).draw();
      
      
      // Cria os filtros após a tabela ser populada
      $('#datatable').DataTable().columns([1,2,3,4,5,6,7,8,9,10,11,12,13,14]).every(function (d) {
        var column = this;
                    var theadname = $("#datatable th").eq([d]).text();
                    var container = $('<div class="filter-title"></div>')
                                    .appendTo('#userstable_filter');
                    var label = $('<label>'+theadname+': </label>')
                                .appendTo(container); 
                    var select = $('<select  class="form-control my-1"><option value="">' +
        theadname +'</option></select>')
                        .appendTo( '#userstable_filter'  ).select2()
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
 
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                        });
 
                    column
                        .data()
                        .unique()
                        .sort()
                        .each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        });
                });

                
        },
    });
});
$(document).ready(function () {
  var table = $('#datatable').DataTable();

  var clearButton = $('<button class="form-control my-1">Limpar Filtros</button>')
    .appendTo('#userstable_filter')
    .on('click', function () {
      table.search('').columns().search('').draw();
      clearSearchState();
    });

  function clearSearchState() {
    var api = table.api();
    var settings = api.settings()[0];
    settings.oPreviousSearch.sSearch = "";
    settings.aoPreSearchCols.forEach(function (column) {
      column.sSearch = "";
    });
    api.draw();
  }
});
</script>
           
   

  
 
  
   




