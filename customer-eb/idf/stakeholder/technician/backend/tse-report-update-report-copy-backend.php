<?php

include("../../../database/database.php");

$codigoget = ($_GET["laudo"]);

$codigo= $_POST['codigo'];
$id_equipamento= $_POST['equipamento'];
$data_start= $_POST['data_start'];
$val= $_POST['date_validation'];
$id_manufacture= $_POST['id_manufacture'];
$id_colaborador= $_POST['id_colaborador'];
$id_responsavel= $_POST['id_responsavel'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];
$obs= $_POST['obs'];



$stmt = $conn->prepare("UPDATE tse SET val = ? WHERE id= ?");
$stmt->bind_param("ss",$val,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE tse SET id_manufacture = ? WHERE id= ?");
$stmt->bind_param("ss",$id_manufacture,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE tse SET id_colaborador = ? WHERE id= ?");
$stmt->bind_param("ss",$id_colaborador,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE tse SET id_responsavel = ? WHERE id= ?");
$stmt->bind_param("ss",$id_responsavel,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE tse SET codigo = ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();
$stmt->close();

echo "<script>alert('Atualizado!');document.location='../calibration-equipament-edit?laudo=$codigoget'</script>";

?>
