<?php
include("../database/database.php");
    $query = "SELECT unidade_agua_fatura.id, unidade_agua.unidade,unidade_agua.cod,month.month, year.yr, unidade_agua_fatura.v_nf,unidade_agua.medidor,unidade_agua_fatura.kwh,unidade_agua_fatura.obs,unidade_agua_fatura.reg_date,unidade_agua_fatura.upgrade FROM unidade_agua_fatura INNER JOIN unidade_agua ON unidade_agua.id = unidade_agua_fatura.id_unidade_agua INNER JOIN month
    ON month.id = unidade_agua_fatura.id_mouth INNER JOIN year ON year.id = unidade_agua_fatura.id_year WHERE unidade_agua_fatura.trash != 0 order by unidade_agua_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
