<?php
include("../database/database.php");
    $query = "SELECT unidade_telephony_fatura.id, unidade_telephony.unidade,unidade_telephony.cod,month.month, year.yr, unidade_telephony_fatura.v_nf,unidade_telephony.medidor,unidade_telephony_fatura.kwh,unidade_telephony_fatura.obs,unidade_telephony_fatura.reg_date,unidade_telephony_fatura.upgrade FROM unidade_telephony_fatura INNER JOIN unidade_telephony ON unidade_telephony.id = unidade_telephony_fatura.id_unidade_telephony INNER JOIN month
    ON month.id = unidade_telephony_fatura.id_mouth INNER JOIN year ON year.id = unidade_telephony_fatura.id_year WHERE unidade_telephony_fatura.trash != 0 order by unidade_telephony_fatura.id DESC";

// Execute a query e retorne os resultados como JSON
$resultados = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($resultados)) {
    $rows[] = $r;
}
print json_encode($rows);
?>
