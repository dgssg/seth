<?php
// Faça a conexão com o banco de dados aqui
	include("../database/database.php");

$stmt = $conn->prepare("SELECT sensor.nome, notif.dados, notif.data FROM notif LEFT JOIN sensor ON sensor.id = notif.id_sensor ORDER BY notif.data DESC LIMIT 1");
$stmt->execute();
$stmt->bind_result($title, $notif_msg, $data);
$stmt->fetch();

// Retornar os dados da notificação em JSON
echo json_encode(array('title' => $title, 'notif_msg' => $notif_msg, 'data' => $data));
?>
