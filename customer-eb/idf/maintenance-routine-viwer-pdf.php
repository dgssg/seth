<?php
  //============================================================+
  // File name   : example_001.php
  // Begin       : 2008-03-04
  // Last Update : 2013-05-14
  //
  // Description : Example 001 for TCPDF class
  //               Default Header and Footer
  //
  // Author: Nicola Asuni
  //
  // (c) Copyright:
  //               Nicola Asuni
  //               Tecnick.com LTD
  //               www.tecnick.com
  //               info@tecnick.com
  //============================================================+
  
  /**
  * Creates an example PDF TEST document using TCPDF
  * @package com.tecnick.tcpdf
  * @abstract TCPDF - Example: Default Header and Footer
  * @author Nicola Asuni
  * @since 2008-03-04
  */
  
  // Include the main TCPDF library (search for installation path).
  require_once('../../framework/TCPDF/tcpdf.php');
  
  // create new PDF document
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('MK Sistemas Biomedicos');
  $pdf->SetTitle('Sistema SETH');
  $pdf->SetSubject('Ordem de Serviço');
  $pdf->SetKeywords('Ordem de Serviço');
  
  // set default header data
  $pdf->SetHeaderData('MK Sistemas Biomedicos', PDF_HEADER_LOGO_WIDTH,'MK Sistemas Biomedicos', 'Sistema SETH', array(0,64,255), array(0,64,128));
  $pdf->setFooterData(array(0,64,0), array(0,64,128));
  
  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
  
  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
  
  // set margins
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  
  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  
  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
  }
  
  // ---------------------------------------------------------
  
  // set default font subsetting mode
  $pdf->setFontSubsetting(true);
  
  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('dejavusans', '', 14, '', true);
  
  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage();
  
  // set text shadow effect
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
  
$codigoget = ($_GET["routine"]);
date_default_timezone_set('America/Sao_Paulo');



 

$print= date(DATE_RFC2822);
// Create connection
 

include("database/database.php");

$query = "SELECT equipamento.id,maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_before WHERE maintenance_routine.id = '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_equipamento,$id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo_equipamento,$nome_equipamento,$modelo_equipamento);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }
}





$query = "SELECT instituicao.logo,instituicao_localizacao.nome,instituicao_area.nome,instituicao.instituicao,equipamento.data_val,equipamento.nf,equipamento.data_fab,equipamento.data_instal,equipamento_familia.anvisa,equipamento_familia.fabricante,equipamento_familia.img,equipamento.obs,equipamento.data_instal,equipamento.data_fab,equipamento.patrimonio,equipamento.serie,equipamento.id, equipamento_familia.nome, equipamento_familia.modelo,equipamento_familia.fabricante, equipamento.codigo, instituicao_localizacao.nome FROM equipamento  INNER JOIN equipamento_familia ON equipamento_familia.id = equipamento.id_equipamento_familia INNER JOIN instituicao_localizacao ON instituicao_localizacao.id = equipamento.id_instituicao_localizacao INNER JOIN instituicao ON instituicao.id = instituicao_localizacao.id_unidade INNER JOIN instituicao_area ON instituicao_area.id = instituicao_localizacao.id_area WHERE equipamento.id like '$id_equipamento'";

//$query = "SELECT id,id_colaborador,id_equipamento,id_setor,id_solicitacao,reg_date FROM os WHERE id_status like '1'  ";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($logo,$setor,$area,$instituicao,$data_val,$nf,$data_fab,$data_instal,$anvisa,$fab,$img,$obs,$data_fab,$data_fab,$patrimonio,$serie,$id, $nome, $modelo, $fabricante, $codigo, $localizacao);
  while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
    }
}

$query = "SELECT maintenance_preventive.id,maintenance_preventive.id_mp,maintenance_preventive.id_fornecedor,maintenance_preventive.date_end,maintenance_preventive.file,maintenance_preventive.time_mp,maintenance_preventive.obs_tc,maintenance_preventive.obs_mp,maintenance_preventive.date_start,maintenance_preventive.upgrade,maintenance_preventive.id_status,maintenance_preventive.date_mp_end,maintenance_preventive.date_mp_start,maintenance_routine.time_ms,maintenance_preventive.id_routine ,maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_preventive ON maintenance_preventive.id_routine = maintenance_routine.id WHERE maintenance_preventive.id_routine like '$codigoget'";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($id_maintenance_preventive,$id_tecnico,$id_fornecedor,$date_end,$id_anexo,$time_mp,$obs_tc,$obs_mp,$programada,$ms_upgrade,$status,$date_end_ms,$date_start_ms,$time_ms,$rotina_id,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo);
   while ($stmt->fetch()) {
//printf("%s, %s\n", $solicitante, $equipamento);
  //  }
}
}
$query = "SELECT maintenance_routine.periodicidade_after,maintenance_routine.data_after,maintenance_routine.time_after,maintenance_routine.id_pop,maintenance_procedures.id,maintenance_procedures.codigo,maintenance_procedures.name, maintenance_routine.id,maintenance_routine.data_start, maintenance_routine.periodicidade,maintenance_routine.reg_date,maintenance_routine.upgrade,equipamento.codigo,equipamento_familia.nome,equipamento_familia.modelo,maintenance_procedures.name,maintenance_procedures.codigo FROM maintenance_routine INNER JOIN equipamento ON equipamento.id = maintenance_routine.id_equipamento  INNER JOIN equipamento_familia ON equipamento.id_equipamento_familia = equipamento_familia.id INNER JOIN maintenance_procedures ON maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_before OR maintenance_procedures.id =  maintenance_routine.id_maintenance_procedures_after WHERE maintenance_routine.id = $codigoget";
if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($periodicidade_after,$data_after,$time_after,$id_pop,$id_maintenance_procedures,$codigo_p,$procedimento_1,$rotina, $data_start,$periodicidade,$reg_date,$upgrade,$codigo,$nome,$modelo,$procedimento_2,$codigo_p2);
    while ($stmt->fetch()) {
        //printf("%s, %s\n", $solicitante, $equipamento);
          //  }
        }
        }

  $html = <<<EOD


<div class="wraper">
 
  <br>
<!-- Início do Cabeçalho -->
 
 
<hr style="border-top: dashed 1px; width: 900px; color: gray; clear: both;">
<!-- Fim do Cabeçalho -->
<!-- Início do Título -->
 <br>
<center>
<h2 class="titulo-relatorio">ROTINA Nº $codigoget</h2>
</center>
<br>
<!-- Fim do Título -->
<main>
<div class="container">
  <section class="content-block">

    <header>
      <h4 class="content-block-title">Dados do equipamento</h4>
    </header>
    <table class="table-documento">
      <tbody>
        <tr>
          <td >
          Nome:
            <span class="uppercase weight-bold"> $nome_equipamento</span>
          </td>

          <td >
            Modelo:
            <span class="uppercase weight-bold"> $modelo_equipamento</span>
          </td>

          <td >
          N&ordm; Serie::
            <span class="uppercase weight-bold"> $serie</span>
          </td>

          <td>
            Patrimonio:
            <span class="uppercase weight-bold"> $patrimonio </span>
          </td>

          <td >
            Codigo:
            <span class="uppercase weight-bold"> $codigo_equipamento</span>
          </td>

        


        </tr>
      </tbody>
    </table>
  </section>
  <!--./content-block-->
  <section class="content-block">
  <header>
    <h4 class="content-block-title">Localiza&ccedil;&atilde;o Atual:</h4>
  </header>

   <table class="table-documento">
    <tbody>
      <tr>
        <td >
        Data instalação:
          <span class="uppercase weight-bold"> $data_instal</span>
        </td>

        <td >
        Instituição:
          <span class="uppercase weight-bold"> $instituicao</span>
        </td>

        <td >
          Setor:
          <span class="uppercase weight-bold"> $area</span>
        </td>

        <td>
          Area:
          <span class="uppercase weight-bold"> $setor </span>
        </td>

     
      </tr>






    </tbody>
  </table>
</section>
 
<section class="content-block">
<header>
  <h4 class="content-block-title">Rotina do Equipamento</h4>
</header>
<table class="table-documento lista-atividades">
  <thead>
    <tr>
      <th class="width-50-percent text-left">Rotina: $rotina</th>
         <th class="width-50-percent text-left">Procedimento: $codigo_p -  $procedimento_1</th>
       
         
         

    </tr>
   
  </thead>



</table>
<table class="table-documento lista-atividades">
  <thead>
  
    <tr>
    <td class="width-50-percent text-left">* inicio $data_start </td>
    <td class="width-50-percent text-left">* Periodicidade $periodicidade (dias)</td>
    </tr>
  </thead>



</table>
<table class="table-documento lista-atividades">
<thead>
<tr>
<th class="width-100-percent text-left">
Cadastro:
<span class="uppercase weight-bold"  > $reg_date</span>  
<br>
Atualização:
<span class="uppercase weight-bold" > $upgrade</span> 
</th>

</tr>
</thead>
</table>

</section>




<br>
<section class="content-block">


         <div class="center-side">

           <div class="center-side-text">


               <small>ROTINA Nº $codigoget</small>
             <br>

           </div>
         </div>

</section>

<!--./content-block-->
</div>
<!--/.container-->
</main>

<footer class="footer">
<div class="">
<center>
Copyright ©2022 MK Sistemas Biomédicos- by MK Sistemas Biomédicos.
</center>

</br>
</div>
<table>
<tr>
  <td class="footer-info base-background">


  </td>

</tr>

</table>
</footer>
					<div class="compose col-md-6  ">
						<div class="compose-header">
							
							Lista de Alertas
							<button type="button" class="close compose-close">
								<span>×</span>
							</button>
						</div>
						<div class="compose-body">
							<div id="alerts"></div>
							<div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
								
							
								
							</div>
						<h3><?php include 'api/api-utilities-list.php';?></h3>
						</div>
						<div class="compose-footer">
							
						</div>
					</div>

</div>


EOD;
  // Print text using writeHTMLCell()
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  
  // ---------------------------------------------------------
  
  // Close and output PDF document
  // This method has several options, check the source code documentation for more information.
  $pdf->Output('Rotina.pdf', 'I');
