 <?php

include("../database/database.php");

$codigoget= $_GET['routine'];
$time_ms= $_POST['time_ms'];
$periodicidade= $_POST['periodicidade'];
$habilitado= $_POST['habilitado'];
$categoria= $_POST['categoria'];
$colaborador= $_POST['colaborador'];
$fornecedor= $_POST['fornecedor'];
$data_after= $_POST['data_after'];
$procedimento_1= $_POST['procedimento_1'];
$procedimento_2= $_POST['procedimento_2'];
$pop= $_POST['pop'];
$digital= $_POST['digital'];
$signature_digital= $_POST['signature_digital'];
$app= $_POST['app'];
$return_user= $_POST['return_user'];
$group_routine= $_POST['group_routine'];

if($procedimento_2 == ""){
$procedimento_2 = $procedimento_1;
  }
  $stmt = $conn->prepare("UPDATE maintenance_routine SET group_routine = ? WHERE id= ?");
$stmt->bind_param("ss",$group_routine,$codigoget);
$execval = $stmt->execute();
    $stmt = $conn->prepare("UPDATE maintenance_routine SET app = ? WHERE id= ?");
    $stmt->bind_param("ss",$app,$codigoget);
    $execval = $stmt->execute();
    
    $stmt = $conn->prepare("UPDATE maintenance_routine SET return_user = ? WHERE id= ?");
    $stmt->bind_param("ss",$return_user,$codigoget);
    $execval = $stmt->execute();
    
$stmt = $conn->prepare("UPDATE maintenance_routine SET data_after = ? WHERE id= ?");
$stmt->bind_param("ss",$data_after,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET time_ms = ? WHERE id= ?");
$stmt->bind_param("ss",$time_ms,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET periodicidade = ? WHERE id= ?");
$stmt->bind_param("ss",$periodicidade,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET habilitado = ? WHERE id= ?");
$stmt->bind_param("ss",$habilitado,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET digital = ? WHERE id= ?");
$stmt->bind_param("ss",$digital,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET signature_digital = ? WHERE id= ?");
$stmt->bind_param("ss",$signature_digital,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET id_category = ? WHERE id= ?");
$stmt->bind_param("ss",$categoria,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET id_colaborador = ? WHERE id= ?");
$stmt->bind_param("ss",$colaborador,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET id_fornecedor = ? WHERE id= ?");
$stmt->bind_param("ss",$fornecedor,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET id_maintenance_procedures_before = ? WHERE id= ?");
$stmt->bind_param("ss",$procedimento_1,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET id_maintenance_procedures_after = ? WHERE id= ?");
$stmt->bind_param("ss",$procedimento_2,$codigoget);
$execval = $stmt->execute();

$stmt = $conn->prepare("UPDATE maintenance_routine SET id_pop= ? WHERE id= ?");
$stmt->bind_param("ss",$pop,$codigoget);
$execval = $stmt->execute();
echo "<script>alert('Alteração Registrado!');document.location='../maintenance-routine-edit?routine=$codigoget'</script>";

header('Location: ../maintenance-routine-edit?routine='.$codigoget);

?>
