<?php
include("../database/database.php");
date_default_timezone_set('America/Sao_Paulo');
$today = date("Y-m-d");
$codigoget = ($_GET["tools"]);
$certificado = $_POST['certificado'];
$date_calibration = $_POST['date_calibration'];
$date_validation = $_POST['date_validation'];
$anexo=$_COOKIE['anexo'];
$status=0;




$stmt = $conn->prepare("INSERT INTO calibration_parameter_tools_laudos (id_calibration_parameter_tools, certificado, date_calibration, date_validation, status, file) VALUES (?, ?,?, ?, ?, ?)");
$stmt->bind_param("ssssss",$codigoget,$certificado,$date_calibration,$date_validation,$status,$anexo);
$execval = $stmt->execute();
$last_id = $conn->insert_id;
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET date_calibration= ? WHERE id= ?");
$stmt->bind_param("ss",$date_calibration,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET date_validation= ? WHERE id= ?");
$stmt->bind_param("ss",$date_validation,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_parameter_tools SET calibration= ? WHERE id= ?");
$stmt->bind_param("ss",$certificado,$codigoget);
$execval = $stmt->execute();
$stmt->close();

setcookie('anexo', null, -1);
header('Location: ../calibration-parameter-equipament');
 ?>
