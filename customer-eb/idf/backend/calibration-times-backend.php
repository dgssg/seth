<?php

include("../database/database.php");

$codigoget = ($_GET["id"]);

$message_cl = $_POST['message_cl'];

$date_mp_end = date('Y-m-d');

$status="4";

$stmt = $conn->prepare("UPDATE calibration_preventive SET id_status= ? WHERE id= ?");
$stmt->bind_param("ss",$status,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE calibration_preventive SET obs_cl= ? WHERE id= ?");
$stmt->bind_param("ss",$message_cl,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_preventive SET date_end= ? WHERE id= ?");
$stmt->bind_param("ss",$date_mp_end,$codigoget);
$execval = $stmt->execute();
$stmt->close();


setcookie('anexo', null, -1);
header('Location: ../flow-calibration');
?>
