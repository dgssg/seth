<?php

include("../database/database.php");

$codigoget = ($_GET["laudo"]);

$codigo= $_POST['codigo'];
$id_equipamento= $_POST['equipamento'];
$data_start= $_POST['data_start'];
$val= $_POST['date_validation'];
$id_manufacture= $_POST['id_manufacture'];
$id_colaborador= $_POST['id_colaborador'];
$id_responsavel= $_POST['id_responsavel'];
$customer= $_POST['customer'];
$temp= $_POST['temp'];
$hum= $_POST['hum'];
$obs= $_POST['obs'];
$documentation= $_POST['documentation'];
$equipament= $_POST['equipament'];
$serial_number= $_POST['serial_number'];
$procedure= $_POST['procedure'];

$stmt = $conn->prepare("UPDATE calibration_single SET procedure_pop = ? WHERE id= ?");
$stmt->bind_param("ss",$procedure,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE calibration_single SET serie_number = ? WHERE id= ?");
$stmt->bind_param("ss",$serial_number,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET equipament = ? WHERE id= ?");
$stmt->bind_param("ss",$equipament,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET documentation = ? WHERE id= ?");
$stmt->bind_param("ss",$documentation,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET customer = ? WHERE id= ?");
$stmt->bind_param("ss",$customer,$codigoget);
$execval = $stmt->execute();
$stmt->close();


$stmt = $conn->prepare("UPDATE calibration_single SET id_equipamento = ? WHERE id= ?");
$stmt->bind_param("ss",$id_equipamento,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET data_start = ? WHERE id= ?");
$stmt->bind_param("ss",$data_start,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET val = ? WHERE id= ?");
$stmt->bind_param("ss",$val,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET id_manufacture = ? WHERE id= ?");
$stmt->bind_param("ss",$id_manufacture,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET id_colaborador = ? WHERE id= ?");
$stmt->bind_param("ss",$id_colaborador,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET id_responsavel = ? WHERE id= ?");
$stmt->bind_param("ss",$id_responsavel,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET temp = ? WHERE id= ?");
$stmt->bind_param("ss",$temp,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET hum = ? WHERE id= ?");
$stmt->bind_param("ss",$hum,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET obs = ? WHERE id= ?");
$stmt->bind_param("ss",$obs,$codigoget);
$execval = $stmt->execute();
$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_single SET codigo = ? WHERE id= ?");
$stmt->bind_param("ss",$codigo,$codigoget);
$execval = $stmt->execute();
$stmt->close();


echo "<script>alert('Atualizado!');document.location='../calibration-report-edit-single-copy?laudo=$codigoget'</script>";

?>
