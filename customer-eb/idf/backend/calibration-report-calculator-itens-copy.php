<?php
$conn = null;
$conn_cal = null;
$stmt = null;
$stmt_cal = null;

include("../database/database.php");
include("../database/database_cal.php");

$loop="0";
$id_query= ($_GET['id']);

$query="SELECT calibration_report.erro_ac,calibration_parameter_dados_parameter.d_1,calibration_parameter_dados_parameter.d_2,calibration_parameter_dados_parameter.erro,calibration_parameter_tools_dados.i_h,calibration_report.id, calibration_report.id_calibration, calibration_report.id_calibration_parameter_tools, calibration_report.id_calibration_parameter_tools_dados, calibration_report.id_calibration_parameter_dados_parameter, calibration_report.ve, calibration_report.vl_1, calibration_report.vl_2, calibration_report.vl_3, calibration_report.vl_avg, calibration_report.erro, calibration_report.dp, calibration_report.ie, calibration_report.ia, calibration_report.k, calibration_report.ib, calibration_report.ic, calibration_report.status, calibration_report.upgrade, calibration_report.reg_date, calibration_report.trash FROM calibration_report INNER JOIN calibration_parameter_tools_dados ON calibration_parameter_tools_dados.id = calibration_report.id_calibration_parameter_tools_dados INNER JOIN calibration_parameter_dados_parameter ON calibration_parameter_dados_parameter.id = calibration_report.id_calibration_parameter_dados_parameter WHERE calibration_report.id = $id_query ";

if ($stmt = $conn->prepare($query)) {
    $stmt->execute();
    $stmt->bind_result($erro_ac,$d_1,$d_2,$d_erro,$i_h,$id,$id_calibration,$id_calibration_parameter_tools,$id_calibration_parameter_tools_dados,$id_calibration_parameter_dados_parameter,$ve,$vl_1,$vl_2,$vl_3,$vl_avg,$erro,$dp,$ie,$ia,$k,$ib,$ic,$status,$upgrade,$reg_date,$trash);
		 while ($stmt->fetch()) {
//printf("%s\n, %s\n,%s\n, %s\n,%s\n, %s\n", $i_h, $id,$ve,$vl_1,$vl_2,$vl_3);
$laudo=$id_calibration;

$ibn=$i_h;  //Esse cálculo só se aplica se a Incerteza for do tipo B, e o tipo for, (resolução ou outras), então é a estimativa convertida dividida pela distribuição

$vl_avg= ($vl_1 + $vl_2 + $vl_3)/3; //Valor medio

$erro=abs($ve-$vl_avg); //Erro

$dp=sqrt((pow(($vl_1-$vl_avg),2)+pow(($vl_2-$vl_avg),2)+pow(($vl_3-$vl_avg),2))/3); // Desvio Padrão

$ia=$dp/(sqrt(3)); // Incerteza do Tipo A / Repetitividade


$ib= $ibn; //É a raiz quadrada das somas das incertezas do tipo B ao quadrado.

$ic=sqrt((pow($ib,2)+pow($ia,2))); //É a raiz quadrada da soma da incerteza do tipo B ao quadrado mais a incerteza do tipo A ao quadrado.

$ie=$ic*$k;//É a Incerteza Combinada vezes o fator de Abrangência.
$erro_ld=trim($erro_ld);
//if($erro_ld=="0"){
$ap= (abs($ie) + abs($vl_avg));
if($d_erro =="0"){
  if($vl_avg > $ve){
   $ac=(abs($ve) + abs($d_1));

    if($vl_avg < $ac){
     $status="0";
   }
    if($vl_avg > $ac){
       $status="1";
     }
 }
 if($vl_avg < $ve){
  $ac=(abs($ve) - abs($d_1));

   if($vl_avg > $ac){
    $status="0";
  }
   if($vl_avg < $ac){
      $status="1";
    }
}

}

if($d_erro =="1"){
 if($vl_avg > $ve){
      $ac=(abs($ve))+(abs($ve) * abs($d_2));

   if($vl_avg < $ac){
    $status="0";
  }
   if($vl_avg > $ac){
      $status="1";
    }
}
if($vl_avg < $ve){
 $ac=(abs($ve))+(abs($ve) * abs($d_2));

  if($vl_avg > $ac){
   $status="0";
 }
  if($vl_avg < $ac){
     $status="1";
   }
}

}

 if($erro_ac != "0"){
   if($d_erro =="0"){
     if($vl_avg > $ve){
      $ac=(abs($ve) + abs($erro_ac));

       if($vl_avg < $ac){
        $status="0";
      }
       if($vl_avg > $ac){
          $status="1";
        }
    }
    if($vl_avg < $ve){
     $ac=(abs($ve) - abs($erro_ac));

      if($vl_avg > $ac){
       $status="0";
     }
      if($vl_avg < $ac){
         $status="1";
       }
   }

   }

   if($d_erro =="1"){
    if($vl_avg > $ve){
         $ac=(abs($ve))+(abs($ve) * abs($erro_ac));

      if($vl_avg < $ac){
       $status="0";
     }
      if($vl_avg > $ac){
         $status="1";
       }
   }
   if($vl_avg < $ve){
    $ac=(abs($ve))+(abs($ve) * abs($erro_ac));

     if($vl_avg > $ac){
      $status="0";
    }
     if($vl_avg < $ac){
        $status="1";
      }
   }

   }
 }
}
}
if( $status == ""){
   $status = "0";
}

$stmt= $conn->prepare("UPDATE calibration_report SET vl_avg = ? WHERE id= ?");
//if(!$req){
  //    echo "Prepare failed: (". $conn_cal->errno.") ".$conn_cal->error."<br>";
   //}
$stmt->bind_param("ss",$vl_avg,$id_query);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET erro = ? WHERE id= ?");
$stmt->bind_param("ss",$erro,$id_query);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET dp = ? WHERE id= ?");
$stmt->bind_param("ss",$dp,$id_query);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ia = ? WHERE id= ?");
$stmt->bind_param("ss",$ia,$id_query);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ie = ? WHERE id= ?");
$stmt->bind_param("ss",$ie,$id_query);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET k = ? WHERE id= ?");
$stmt->bind_param("ss",$k,$id_query);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ib = ? WHERE id= ?");
$stmt->bind_param("ss",$ib,$id_query);
$execval = $stmt->execute();
$stmt->close();
$stmt = $conn->prepare("UPDATE calibration_report SET ic = ? WHERE id= ?");
$stmt->bind_param("ss",$ic,$id_query);
$execval = $stmt->execute();
$stmt->close();
//$stmt = $conn->prepare("UPDATE calibration_report SET id_calibration_parameter_tools = ? WHERE id= ?");
//$stmt->bind_param("si",$id_calibration_parameter_tools,$id);
//$execval = $stmt->execute();
//$stmt->close();
//$stmt = $conn->prepare("UPDATE calibration_report SET id_calibration_parameter_tools_dados = ? WHERE id= ?");
//$stmt->bind_param("si",$tools,$id);
//$execval = $stmt->execute();
//$stmt->close();

$stmt = $conn->prepare("UPDATE calibration_report SET status = ? WHERE id= ?");
$stmt->bind_param("ss",$status,$id_query);
$execval = $stmt->execute();
$stmt->close();

// printf("%s\n, %s\n", $ap, $ac);
//printf("%s\n, %s\n,%s\n, %s\n,%s\n, %s\n", $ap, $erro,$dp,$ia,$ie,$k);

echo "<script>alert('Calculado!');document.location='../calibration-report-edit-copy?laudo=$laudo'</script>";


//header("location:javascript:alert(\"Calculo Processado!\");location.href=\"../calibration-report-edit-copy?laudo='.$laudo\";")
header('Location: ../calibration-report-edit-copy?laudo='.$laudo);
?>
