<?php
include("../database/database.php");

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Obtém os dados enviados pelo formulário
    $id_unidade_energia = $_POST["codigoget"];
    $item = $_POST["item"];
    $id_type = $_POST["id_type"];
    $qtd = $_POST["qtd"];
    $vlr_u = $_POST["vlr_u"];
    $vlr_t = $_POST["vlr_t"];
    
    // Prepara e executa a consulta SQL
    $stmt = $conn->prepare("INSERT INTO unidade_telephony_fatura_itens (id_unidade_telephony_fatura, id_type, item, qtd, vlr_u, vlr_t) VALUES (?, ?, ?, ?, ?, ?)");
    
    if ($stmt) {
        $stmt->bind_param("ssssss", $id_unidade_energia, $id_type, $item, $qtd, $vlr_u, $vlr_t);
        $execval = $stmt->execute();
        $stmt->close();
        
        // Verifica se a execução foi bem-sucedida e redireciona
        if ($execval) {
            header("Location: ../utilities-telephony-supply-search");
            exit();
        } else {
            echo "Erro ao inserir a linha.";
        }
    } else {
        echo "Erro na preparação da consulta: " . $conn->error;
    }
} else {
    echo "Método de requisição inválido.";
}
?>
