<?php

include("../database/database.php");

 
$codigoget= $_POST['id'];
$unidade= $_POST['unidade'];
$cod= $_POST['cod'];
$medidor= $_POST['medidor'];
$fornecimento= $_POST['fornecimento'];
$obs= $_POST['obs'];
$fabricante= $_POST['fabricante'];
$modelo= $_POST['modelo'];
$volt= $_POST['volt'];
$amper= $_POST['amper'];
$type= $_POST['type'];
$conector= $_POST['conector'];
$registro= $_POST['registro'];
$fase= $_POST['fase'];
$serie= $_POST['serie'];
$ativo= $_POST['ativo'];

	$stmt = $conn->prepare("UPDATE unidade_energia SET unidade= ? WHERE id= ?");
	$stmt->bind_param("ss",$unidade,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET cod= ? WHERE id= ?");
	$stmt->bind_param("ss",$cod,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET medidor= ? WHERE id= ?");
	$stmt->bind_param("ss",$medidor,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET fornecimento= ? WHERE id= ?");
	$stmt->bind_param("ss",$fornecimento,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET obs= ? WHERE id= ?");
	$stmt->bind_param("ss",$obs,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET fabricante= ? WHERE id= ?");
	$stmt->bind_param("ss",$fabricante,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET modelo= ? WHERE id= ?");
	$stmt->bind_param("ss",$modelo,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET volt= ? WHERE id= ?");
	$stmt->bind_param("ss",$volt,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET amper= ? WHERE id= ?");
	$stmt->bind_param("ss",$amper,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET conector= ? WHERE id= ?");
	$stmt->bind_param("ss",$conector,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET type= ? WHERE id= ?");
	$stmt->bind_param("ss",$type,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET registro= ? WHERE id= ?");
	$stmt->bind_param("ss",$registro,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET ativo= ? WHERE id= ?");
	$stmt->bind_param("ss",$ativo,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET fase= ? WHERE id= ?");
	$stmt->bind_param("ss",$fase,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	$stmt = $conn->prepare("UPDATE unidade_energia SET serie= ? WHERE id= ?");
	$stmt->bind_param("ss",$serie,$codigoget);
	$execval = $stmt->execute();
	$stmt->close();
	
	echo "<script>document.location='../utilities-energy-unit-search?sweet_salve=1'</script>";
?>